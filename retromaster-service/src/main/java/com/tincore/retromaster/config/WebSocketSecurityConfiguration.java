package com.tincore.retromaster.config;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;

import static com.tincore.retromaster.config.WebSocketConfiguration.*;

@Configuration
public class WebSocketSecurityConfiguration extends AbstractSecurityWebSocketMessageBrokerConfigurer {

    @Override
    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages.simpDestMatchers(MESSAGE_TOPIC_CONTENT_EXECUTION)
            .hasAnyAuthority("CONTENT_EXECUTE")
            .simpDestMatchers(MESSAGE_TOPIC_CONTENT_SCAN)
            .hasAnyAuthority("CONTENT_IMPORT")
            .simpDestMatchers(MESSAGE_TOPIC_CONTENT_IMPORT)
            .hasAnyAuthority("CONTENT_IMPORT")
            .simpDestMatchers(MESSAGE_TOPIC_CONTENT_INDEX)
            .hasAnyAuthority("CONTENT_IMPORT")
            .simpDestMatchers(MESSAGE_TOPIC_CONTENT_METADATA_SET_MANAGE)
            .hasAnyAuthority("CONTENTMETADATASET_READ")
            .simpDestMatchers(MESSAGE_PING, MESSAGE_TOPIC_ECHO)
            .authenticated()
            .anyMessage()
            .authenticated(); // All needs to be declared here explicitly
    }

    @Override
    protected boolean sameOriginDisabled() {
        return true;
    }
}


// IT STILL DOES NOT OFFER ANYTHING TO DISABLE SAME ORIGIN!!!!!!!
//@Configuration
//@EnableWebSocketSecurity
//public class WebSocketSecurityConfiguration {
//
//    @Bean
//    AuthorizationManager<Message<?>> authorizationManager(MessageMatcherDelegatingAuthorizationManager.Builder messages) {
//
//        messages.simpDestMatchers(MESSAGE_TOPIC_CONTENT_EXECUTION)
//            .hasAnyAuthority("CONTENT_EXECUTE")
//            .simpDestMatchers(MESSAGE_TOPIC_CONTENT_SCAN)
//            .hasAnyAuthority("CONTENT_IMPORT")
//            .simpDestMatchers(MESSAGE_TOPIC_CONTENT_IMPORT)
//            .hasAnyAuthority("CONTENT_IMPORT")
//            .simpDestMatchers(MESSAGE_TOPIC_CONTENT_INDEX)
//            .hasAnyAuthority("CONTENT_IMPORT")
//            .simpDestMatchers(MESSAGE_TOPIC_CONTENT_METADATA_SET_MANAGE)
//            .hasAnyAuthority("CONTENTMETADATASET_READ")
//            .simpDestMatchers(MESSAGE_PING, MESSAGE_TOPIC_ECHO)
//            .authenticated()
//            .anyMessage()
//            .authenticated(); // All needs to be declared here explicitly
//
//        return messages.build();
//    }
//}
