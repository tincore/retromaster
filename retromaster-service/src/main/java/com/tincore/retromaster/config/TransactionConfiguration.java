package com.tincore.retromaster.config;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.support.TransactionTemplate;

@Configuration
@EnableTransactionManagement
public class TransactionConfiguration {

    @Bean
    @Qualifier("requiresNew")
    public TransactionNewTemplate transactionNewTemplate(PlatformTransactionManager platformTransactionManager) {
        return new TransactionNewTemplate(platformTransactionManager);
    }

    @Bean
    @Qualifier("readOnly")
    public TransactionReadTemplate transactionReadTemplate(PlatformTransactionManager platformTransactionManager) {
        return new TransactionReadTemplate(platformTransactionManager);
    }

    @Bean
    @Primary
    public TransactionTemplate transactionTemplate(PlatformTransactionManager platformTransactionManager) {
        return new TransactionTemplate(platformTransactionManager);
    }

    public static class TransactionNewTemplate extends TransactionTemplate {

        public TransactionNewTemplate(PlatformTransactionManager platformTransactionManager) {
            super(platformTransactionManager);
            // setReadOnly(true);
            setPropagationBehavior(Propagation.REQUIRES_NEW.value());
        }
    }

    public static class TransactionReadTemplate extends TransactionTemplate {

        public TransactionReadTemplate(PlatformTransactionManager platformTransactionManager) {
            super(platformTransactionManager);
            setReadOnly(true);
            // setPropagationBehavior(Propagation.REQUIRES_NEW.value());
        }
    }
}
