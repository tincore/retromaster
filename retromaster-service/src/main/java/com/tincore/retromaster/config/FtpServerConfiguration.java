package com.tincore.retromaster.config;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.RetromasterUser;
import com.tincore.retromaster.service.UserService;
import lombok.Data;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.*;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;
import org.apache.ftpserver.usermanager.impl.AbstractUserManager;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import java.util.ArrayList;
import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "tincore.ftp")
@ConditionalOnProperty(value = "tincore.ftp.enabled", havingValue = "true")
public class FtpServerConfiguration {

    private int port;

    private List<String> ipWhitelist = new ArrayList<>();

    public UserManager createUserManager(final UserService userService) {
        var matchers = ipWhitelist.stream().map(IpAddressMatcher::new).toList();

        return new AbstractUserManager() {
            @Override
            public User authenticate(Authentication authentication) throws AuthenticationFailedException {
                if (!(authentication instanceof UsernamePasswordAuthentication)) {
                    throw new AuthenticationFailedException("Unsupported authentication class " + authentication.getClass());
                }

                if (!matchers.isEmpty()) {
                    var inetAddress = ((UsernamePasswordAuthentication) authentication).getUserMetadata().getInetAddress().getHostAddress();
                    if (matchers.stream().noneMatch(m -> m.matches(inetAddress))) {
                        throw new AuthenticationFailedException("Could not authenticate. Invalid IP Address");
                    }
                }

                var usernamePasswordAuthentication = (UsernamePasswordAuthentication) authentication;
                return userService.getUsers()
                    .stream()
                    .filter(RetromasterUser::isFtp)
                    .filter(u -> u.getUsername().equals(usernamePasswordAuthentication.getUsername()))
                    .filter(u -> u.getPassword().equals(usernamePasswordAuthentication.getPassword()))
                    .findFirst()
                    .map(u -> toUser(u))
                    .orElseThrow(() -> new AuthenticationFailedException("Could not authenticate"));
            }

            @Override
            public void delete(String username) {
                // Unimplemented
            }

            @Override
            public boolean doesExist(String username) {
                return userService.getUsers().stream().anyMatch(u -> u.getUsername().equals(username));
            }

            @Override
            public String[] getAllUserNames() {
                return userService.getUsers().stream().filter(RetromasterUser::isFtp).map(RetromasterUser::getUsername).toArray(String[]::new);
            }

            @Override
            public User getUserByName(String username) throws FtpException {
                var user = userService.getUsers().stream().filter(RetromasterUser::isFtp).filter(u -> u.getUsername().equals(username)).findFirst().orElseThrow(() -> new FtpException("No user with name " + username));
                return toUser(user);
            }

            @Override
            public void save(User user) {
                // Do nothing (RO)
            }
        };
    }

    @Bean
    public FtpServerFactory ftpServerFactory(UserService userService) {
        var serverFactory = new FtpServerFactory();
        serverFactory.setUserManager(createUserManager(userService));

        var factory = new ListenerFactory();
        factory.setPort(port);

        // SslConfigurationFactory ssl = new SslConfigurationFactory();
        // ssl.setKeystoreFile(new File("src/test/resources/ftpserver.jks"));
        // ssl.setKeystorePassword("password");
        //// set the SSL configuration for the listener
        // factory.setSslConfiguration(ssl.createSslConfiguration());
        // factory.setImplicitSsl(true);

        serverFactory.addListener("default", factory.createListener());

        return serverFactory;
    }

    private User toUser(RetromasterUser user) {
        return new User() {
            @Override
            public AuthorizationRequest authorize(AuthorizationRequest request) {
                // To restrict multiple logins for instance
                return request;
            }

            @Override
            public List<? extends Authority> getAuthorities(Class<? extends Authority> clazz) {
                return getAuthorities();
            }

            @Override
            public List<? extends Authority> getAuthorities() {
                return new ArrayList<>();
            }

            @Override
            public boolean getEnabled() {
                return true;
            }

            @Override
            public String getHomeDirectory() {
                return "/";
            }

            @Override
            public int getMaxIdleTime() {
                return 0;
            }

            @Override
            public String getName() {
                return user.getUsername();
            }

            @Override
            public String getPassword() {
                return user.getPassword();
            }
        };
    }
}
