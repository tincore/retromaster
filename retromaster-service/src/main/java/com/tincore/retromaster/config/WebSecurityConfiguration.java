package com.tincore.retromaster.config;

/*-
 * #%L
 * spring-boot3x-lib-common
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.util.security.AuthenticationExceptionEntryPoint;
import com.tincore.boot.util.security.jwt.JwtAuthenticationTokenFilter;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.h2.H2ConsoleProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

@Data
@Configuration
@ConfigurationProperties("tincore.web.sec")
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration {

    private boolean securePathRoot = true;
    private boolean securePathDev = true;

    private String redirectLocationTemplate;
    private String healthPath = "/health";

    private List<String> authPaths = new ArrayList<>();
    private List<String> ipWhitelist = new ArrayList<>();

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired(required = false)
    private H2ConsoleProperties h2ConsoleProperties;

    @Autowired(required = false)
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    @Bean
    public AuthenticationEntryPoint authenticationExceptionEntryPoint() {
        return new AuthenticationExceptionEntryPoint(redirectLocationTemplate);
    }

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(this.userDetailsService);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity, MvcRequestMatcher.Builder mvc) throws Exception {
        httpSecurity.csrf().disable().exceptionHandling().authenticationEntryPoint(authenticationExceptionEntryPoint()).and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        httpSecurity.authorizeRequests(a -> {
            a.requestMatchers(mvc.pattern(HttpMethod.GET, healthPath)).permitAll();

            a.requestMatchers(antMatcher("/api/auth")).permitAll();

            a.requestMatchers(mvc.pattern(HttpMethod.GET, "/retromaster/auth")).permitAll();
//            authPaths.forEach(p -> a.requestMatchers(p).permitAll());

            if (!securePathRoot) {
                a.requestMatchers(
                    mvc.pattern(HttpMethod.GET, "/"),
                    mvc.pattern(HttpMethod.GET, "/*.html"),
                    mvc.pattern(HttpMethod.GET, "/*/*.html")
                ).permitAll();
            }

            if (!securePathDev) {
                a.requestMatchers(
                    mvc.pattern("/dev/**"),
                    mvc.pattern("/stub/**")
                ).permitAll();
            }

            if (!securePathRoot || !securePathDev) {
                a.requestMatchers(
                    mvc.pattern(HttpMethod.GET, "/favicon.ico")
                ).permitAll();

                Stream.of("", "/*", "/*/*", "/*/*/*", "/*/*/*/*", "/*/*/*/*/*")
                    .forEach(p -> a.requestMatchers(
                        mvc.pattern(HttpMethod.GET, p + "/*.css"),
                        mvc.pattern(HttpMethod.GET, p + "/*.js"),
                        mvc.pattern(HttpMethod.GET, p + "/*.woff"),
                        mvc.pattern(HttpMethod.GET, p + "/*.woff2"),
                        mvc.pattern(HttpMethod.GET, p + "/*.ttf"),
                        mvc.pattern(HttpMethod.GET, p + "/*.eot"),
                        mvc.pattern(HttpMethod.GET, p + "/*.png"),
                        mvc.pattern(HttpMethod.GET, p + "/*.xpg"),
                        mvc.pattern(HttpMethod.GET, p + "/*.jpeg"),
                        mvc.pattern(HttpMethod.GET, p + "/*.svg")
                    ).permitAll());
            }

            var ipExpression = ipWhitelist.stream().map(s -> "hasIpAddress('" + s.trim() + "')").collect(Collectors.joining(" or "));
            a.anyRequest().access("isAuthenticated()" + (StringUtils.isNotBlank(ipExpression) ? " and (" + ipExpression + ")" : ""));

        });

        if (jwtAuthenticationTokenFilter != null) {
            httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
        }

        if (h2ConsoleProperties != null && h2ConsoleProperties.getEnabled()) {
            // Need this for h2 console
            httpSecurity.headers().frameOptions().disable();
        }

        httpSecurity.headers().cacheControl();
        return httpSecurity.build();
    }

    @Bean
    MvcRequestMatcher.Builder mvc(HandlerMappingIntrospector introspector) {
        return new MvcRequestMatcher.Builder(introspector);
    }
}
