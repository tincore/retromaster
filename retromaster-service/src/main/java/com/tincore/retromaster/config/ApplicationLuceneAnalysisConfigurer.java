package com.tincore.retromaster.config;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilterFactory;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterGraphFilterFactory;
import org.apache.lucene.analysis.ngram.EdgeNGramFilterFactory;
import org.apache.lucene.analysis.pattern.PatternReplaceFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurationContext;
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurer;

public class ApplicationLuceneAnalysisConfigurer implements LuceneAnalysisConfigurer {

    public static final String NORMALIZER_LOWERCASE = "lowercase";
    public static final String ANALYZER_STANDARD = "standardAnalyzer";
    public static final String ANALYZER_STANDARD_EDGE_N_GRAM = "standardEdgeNGram";

    // public static final String ANALYZER_STANDARD_N_GRAM = "standardNGram";
    // public static final String ANALYZER_KEYWORD_EDGE_NGRAM = "keywordEdgeNgram";

    @Override
    public void configure(LuceneAnalysisConfigurationContext context) {
        context.normalizer(NORMALIZER_LOWERCASE).custom().tokenFilter(LowerCaseFilterFactory.class).tokenFilter(ASCIIFoldingFilterFactory.class);

        context.analyzer(ANALYZER_STANDARD_EDGE_N_GRAM)
            .custom()
            .tokenizer(StandardTokenizerFactory.class)
            .tokenFilter(LowerCaseFilterFactory.class)
            .tokenFilter(ASCIIFoldingFilterFactory.class)
            .tokenFilter(EdgeNGramFilterFactory.class)
            .param("minGramSize", "1")
            .param("maxGramSize", "20")
            .param("preserveOriginal", "true")
            .tokenFilter(PatternReplaceFilterFactory.class)
            .param("pattern", "([^a-zA-Z0-9\\.])")
            .param("replacement", " ")
            .param("replace", "all");

        // Should use this to tokenize searches so it does not split query
        context.analyzer(ANALYZER_STANDARD)
            .custom()
            .tokenizer(StandardTokenizerFactory.class)
            .tokenFilter(LowerCaseFilterFactory.class)
            .tokenFilter(ASCIIFoldingFilterFactory.class)
            .tokenFilter(WordDelimiterGraphFilterFactory.class)
            .tokenFilter(PatternReplaceFilterFactory.class)
            .param("pattern", "([^a-zA-Z0-9\\.])")
            .param("replacement", " ")
            .param("replace", "all");

        // context.analyzer(ANALYZER_KEYWORD_EDGE_NGRAM)
        // .custom()
        // .tokenizer(StandardTokenizerFactory.class)
        // .tokenFilter(LowerCaseFilterFactory.class)
        // .tokenFilter(StopFilterFactory.class) // Stopwords file??
        // .tokenFilter(EdgeNGramFilterFactory.class) // Index partial words starting at the front, so we can provide Autocomplete functionality
        // .param("minGramSize", "1")
        // .param("maxGramSize", "40")
        // .tokenFilter(PatternReplaceFilterFactory.class)
        // .param("pattern", "([^a-zA-Z0-9\\.])")
        // .param("replacement", " ")
        // .param("replace", "all");
        //
        // context.analyzer(ANALYZER_STANDARD_N_GRAM)
        // .custom()
        // .tokenizer(StandardTokenizerFactory.class)
        // .tokenFilter(LowerCaseFilterFactory.class)
        // .tokenFilter(WordDelimiterGraphFilterFactory.class)
        // .tokenFilter(NGramFilterFactory.class) // Index partial words starting at the front, so we can provide Autocomplete functionality
        // .param("minGramSize", "3")
        // .param("maxGramSize", "5")
        // .tokenFilter(PatternReplaceFilterFactory.class)
        // .param("pattern", "([^a-zA-Z0-9\\.])")
        // .param("replacement", " ")
        // .param("replace", "all");
    }
}
