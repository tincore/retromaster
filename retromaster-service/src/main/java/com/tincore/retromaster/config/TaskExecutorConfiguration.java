package com.tincore.retromaster.config;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

@Data
@Configuration
@ConfigurationProperties(prefix = "tincore.task")
public class TaskExecutorConfiguration {

    private static final int CORE_POOL_SIZE = 5;
    private static final int MAX_POOL_SIZE = 10;
    private static final int QUEUE_CAPACITY = 25;

    private int forkJoinPoolParallelism;

    @Bean
    public ForkJoinPoolProvider forkJoinPoolProvider() {
        return new ForkJoinPoolProvider(forkJoinPoolParallelism);
    }

    @Bean
    @Qualifier("pool")
    public TaskExecutor poolTaskExecutor() {
        var threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(CORE_POOL_SIZE);
        threadPoolTaskExecutor.setMaxPoolSize(MAX_POOL_SIZE);
        threadPoolTaskExecutor.setQueueCapacity(QUEUE_CAPACITY);
        return threadPoolTaskExecutor;
    }

    public static class ForkJoinPoolProvider {
        private final ForkJoinPool forkJoinPool;

        public ForkJoinPoolProvider(int parallelism) {
            forkJoinPool = new ForkJoinPool(parallelism);
        }

        @PreDestroy
        public void preDestroy() {
            forkJoinPool.shutdown();
        }

        public void submitNow(Runnable r) throws ExecutionException, InterruptedException {
            forkJoinPool.submit(r).get();
        }
    }
}
