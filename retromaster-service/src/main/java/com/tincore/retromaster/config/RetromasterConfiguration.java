package com.tincore.retromaster.config;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.domain.RetromasterUser;
import com.tincore.retromaster.service.archive.ArchiverFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.service.archive.ArchiverFormat.TAR_XZ;
import static com.tincore.retromaster.service.archive.ArchiverFormat.ZIP;

@Data
@Configuration
@ConfigurationProperties(prefix = "tincore.retromaster")
public class RetromasterConfiguration {
    public static final int DEFAULT_CONTENT_SIGNATURE_PROCESSOR_BATCH_SIZE = 100;
    public static final int DEFAULT_CONTENT_SIGNATURE_PROCESSOR_FETCH_SIZE = 50_000; // Bigger number less dupes but more risk of too large transaction
    public static final int DEFAULT_METADATA_SET_IMPORT_METADATA_BATCH_SIZE = 100;
    public static final int DEFAULT_METADATA_SET_IMPORT_SIGNATURE_BATCH_SIZE = 1000;
    public static final int DEFAULT_METADATA_SET_DELETE_METADATA_BATCH_SIZE = 250;
    public static final long DEFAULT_LARGE_FILE_THRESHOLD = 65_000_000L;

    private static final int PRE_TOUCH_LIMIT_FACTOR = 50;
    private static final int PRE_LIMIT_FACTOR = 20;
    private static final Set<String> DEFAULT_METADATA_EXTENSIONS = Stream.of("dat", "xml").collect(Collectors.toSet());
    private static final int DEFAULT_CONTENT_VOLUME_IMPORT_PARALLEL_COUNT = 4;
    private static final int DEFAULT_CONTENT_VOLUME_IMPORT_BATCH_FILE_COUNT_LIMIT = 1000;
    private static final long DEFAULT_CONTENT_VOLUME_IMPORT_BATCH_FILE_SIZE_LIMIT = 500_000_000L;
    private static final int DEFAULT_INDEX_THREAD_COUNT = 8;
    private static final int DEFAULT_INDEX_PUBLISH_EVENT_ENTITIES_LOAD_INTERVAL = 1000;
    private static final int DEFAULT_INDEX_ID_FETCH_SIZE = 15;
    private static final int DEFAULT_INDEX_BATCH_SIZE = 5;
    // private static final int ARCHIVER_BUFFER_SIZE_DEFAULT = 1024 * 4;
    private static final int ARCHIVER_BUFFER_SIZE_DEFAULT = 1024 * 64;
    private static final Duration DEFAULT_ORCHESTRATION_EXECUTION_DELAY = Duration.ofSeconds(15);
    private Set<RetromasterUser> users = new HashSet<>();

    private Set<String> metadataExtensions = DEFAULT_METADATA_EXTENSIONS;

    private int contentVolumeImportParallelCount = DEFAULT_CONTENT_VOLUME_IMPORT_PARALLEL_COUNT;
    private boolean contentVolumeImportPreTouchFiles = true;
    private long contentVolumeImportBatchFilePreTouchLimit = DEFAULT_CONTENT_VOLUME_IMPORT_BATCH_FILE_COUNT_LIMIT * PRE_TOUCH_LIMIT_FACTOR;
    private long contentVolumeImportBatchFileCountLimit = DEFAULT_CONTENT_VOLUME_IMPORT_BATCH_FILE_COUNT_LIMIT;
    private long contentVolumeImportBatchFileCountPreLimit = DEFAULT_CONTENT_VOLUME_IMPORT_BATCH_FILE_COUNT_LIMIT * PRE_LIMIT_FACTOR;
    private long contentVolumeImportBatchFileSizeLimit = DEFAULT_CONTENT_VOLUME_IMPORT_BATCH_FILE_SIZE_LIMIT;

    private Path configPath;
    private Path homePath;
    private Path indexPath;

    private List<Path> dataImportPaths;
    private List<Path> dataStagePaths;
    private List<Path> dataProdPaths;

    private Path dataExportPath;
    private String dataExportUnknownDirectoryName = "zzz_unk";
    private String dataExportErrorDirectoryName = "zzz_err";

    private List<Path> metadataImportPaths;
    private Path metadataExportPath;
    private Path metadataExportUnknownPath;
    private Path metadataExportLegacyPath;

    private String metadataExportLegacyDirectoryName = "zzz_legacy";
    private String metadataExportErrorDirectoryName = "zzz_err";
    private String metadataExportDuplicateDirectoryName = "zzz_dupe";

    private boolean metadataSetAutoCleanOlderVersions = true;
    private int metadataSetImportMetadataBatchSize = DEFAULT_METADATA_SET_IMPORT_METADATA_BATCH_SIZE;
    private int metadataSetImportSignatureBatchSize = DEFAULT_METADATA_SET_IMPORT_SIGNATURE_BATCH_SIZE;
    private int metadataSetDeleteMetadataBatchSize = DEFAULT_METADATA_SET_DELETE_METADATA_BATCH_SIZE;

    private Path metadataProdPath;

    private long contentSignatureProcessorStageLargeFileThreshold = DEFAULT_LARGE_FILE_THRESHOLD;
    private int contentSignatureProcessorFetchSize = DEFAULT_CONTENT_SIGNATURE_PROCESSOR_FETCH_SIZE;
    private int contentSignatureProcessorBatchSize = DEFAULT_CONTENT_SIGNATURE_PROCESSOR_BATCH_SIZE;

    private int indexThreadCount = DEFAULT_INDEX_THREAD_COUNT;
    private int indexIdFetchSize = DEFAULT_INDEX_ID_FETCH_SIZE;
    private int indexBatchSize = DEFAULT_INDEX_BATCH_SIZE;
    private int indexPublishEventEntitiesLoadedInterval = DEFAULT_INDEX_PUBLISH_EVENT_ENTITIES_LOAD_INTERVAL; // 10000

    private OrchestrationConfiguration orchestration = new OrchestrationConfiguration();
    private EnrichConfiguration enrich = new EnrichConfiguration();
    private ExecuteConfiguration execute = new ExecuteConfiguration();
    private ArchiverConfiguration archiver = new ArchiverConfiguration();

    private ContentsConfiguration content = new ContentsConfiguration();

    @Bean
    ArchiverConfiguration archiverConfiguration() {
        return archiver;
    }

    @Bean
    ContentsConfiguration contentsConfiguration() {
        return content;
    }

    @Bean
    RetroarchExecuteConfiguration retroarchExecuteConfiguration() {
        return execute.getRetroarch();
    }

    public interface ArchiverFormatContentSystemConfiguration {
        long getQuickArchiverFormatOnSizeGreaterThan();

        ArchiverFormat getQuickArchiverFormat();

        ArchiverFormat getSolidArchiverFormat();

        boolean isDisableArchiver();

        boolean isQuickArchiverFormatOnContentExtensionDisc();
    }

    @Data
    public static class ContentSystemConfiguration implements ArchiverFormatContentSystemConfiguration {
        private ContentSystem contentSystem;

        private boolean disableArchiver;
        private long quickArchiverFormatOnSizeGreaterThan = DEFAULT_LARGE_FILE_THRESHOLD;
        private boolean quickArchiverFormatOnContentExtensionDisc = true;
        private ArchiverFormat quickArchiverFormat = ZIP;
        private ArchiverFormat solidArchiverFormat = TAR_XZ;
    }

    @Data
    public class OrchestrationConfiguration {
        private boolean enabled = true;

        private boolean contentMetadataSetImportAuto = true;

        private boolean onStartMetadataSetReimport;
        private boolean onStartMetadataSetReimportRenormalizeSets = true;
        private boolean onStartMetadataSetReimportRenormalizeDatas;
        private boolean onStartMetadataSetReorganize = true;

        private boolean contentImportAuto = true;
        private boolean onStartContentReindex;
        private boolean onStartContentReorganize;
        private boolean onStartContentCleanOrphan = true;

        private Duration delay = DEFAULT_ORCHESTRATION_EXECUTION_DELAY;
    }

    @Data
    public class ArchiverConfiguration {
        private int archiveBufferSize = ARCHIVER_BUFFER_SIZE_DEFAULT;
    }

    @Data
    public class ContentsConfiguration implements ArchiverFormatContentSystemConfiguration {

        private boolean disableArchiver;
        private long quickArchiverFormatOnSizeGreaterThan = DEFAULT_LARGE_FILE_THRESHOLD;
        private boolean quickArchiverFormatOnContentExtensionDisc = true;
        private ArchiverFormat quickArchiverFormat = ZIP;
        private ArchiverFormat solidArchiverFormat = TAR_XZ;

        private List<ContentSystemConfiguration> systems = new ArrayList<>();
    }

    @Data
    public class ExecuteConfiguration {

        private CitraExecuteConfiguration citra = new CitraExecuteConfiguration();
        private DolphinExecuteConfiguration dolphin = new DolphinExecuteConfiguration();
        private DosboxExecuteConfiguration dosbox = new DosboxExecuteConfiguration();
        private DosboxExecuteConfiguration dosboxX = new DosboxExecuteConfiguration();
        private FsUaeExecuteConfiguration fsUae = new FsUaeExecuteConfiguration();
        private HatariExecuteConfiguration hatari = new HatariExecuteConfiguration();
        private Pcsx2ExecuteConfiguration pcsx2 = new Pcsx2ExecuteConfiguration();
        private PpssppExecuteConfiguration ppsspp = new PpssppExecuteConfiguration();
        private DuckstationExecuteConfiguration duckstation = new DuckstationExecuteConfiguration();

        private RedreamExecuteConfiguration redream = new RedreamExecuteConfiguration();
        private ReicastExecuteConfiguration reicast = new ReicastExecuteConfiguration();
        private RetroarchExecuteConfiguration retroarch = new RetroarchExecuteConfiguration();
        private YabauseExecuteConfiguration yabause = new YabauseExecuteConfiguration();
    }

    @Data
    public class GenericExecuteConfiguration {
        private List<String> executablePaths;
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    public class RetroarchExecuteConfiguration extends GenericExecuteConfiguration {
        private boolean fullscreen = true;
        private Path configurationPath;
        private List<String> corePaths;
        private boolean verbose;
    }

    @Data
    public class DosboxExecuteConfiguration extends GenericExecuteConfiguration {
        private boolean fullscreen = true;
        private boolean fulldouble;
        private String fullResolution = "original";
        private String windowResolution = "original";
        private String output = "opengl";
    }

    @Data
    public class YabauseExecuteConfiguration extends GenericExecuteConfiguration {
    }

    @Data
    public class HatariExecuteConfiguration extends GenericExecuteConfiguration {
    }

    @Data
    public class CitraExecuteConfiguration extends GenericExecuteConfiguration {
    }

    @Data
    public class RedreamExecuteConfiguration extends GenericExecuteConfiguration {
    }

    @Data
    public class FsUaeExecuteConfiguration extends GenericExecuteConfiguration {
        private static final int FLOPPY_COUNT = 4;
        private static final int FSAA_DEFAULT = 2;
        private static final int FLOPPY_SPEED_DEFAULT = 0;

        private int fsaa = FSAA_DEFAULT;

        private int floppySpeed = FLOPPY_SPEED_DEFAULT;
        private int floppyCount = FLOPPY_COUNT;
        private boolean fullscreen = true;
    }

    @Data
    public class PpssppExecuteConfiguration extends GenericExecuteConfiguration {
    }

    @Data
    public class DuckstationExecuteConfiguration extends GenericExecuteConfiguration {
    }

    @Data
    public class ReicastExecuteConfiguration extends GenericExecuteConfiguration {
    }

    @Data
    public class Pcsx2ExecuteConfiguration extends GenericExecuteConfiguration {
    }

    @Data
    public class EnrichConfiguration {
        private Set<String> sabretoolsMergedOriginExclude = new HashSet<>();
    }

    @Data
    public class DolphinExecuteConfiguration extends GenericExecuteConfiguration {
    }
}
