package com.tincore.retromaster.config;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.util.monitor.SimpleExecutionTimeInterceptor;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.PointcutAdvisor;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

@Configuration
@EnableAspectJAutoProxy
public class MonitorConfiguration {

    @Bean
    public PointcutAdvisor getMonitorPointcutAdvisor() {
        return new AbstractPointcutAdvisor() {

            private final StaticMethodMatcherPointcut pointcut = new StaticMethodMatcherPointcut() {
                public boolean isClassAnnotated(Class<?> targetClass) {
                    return targetClass.isAnnotationPresent(Monitored.class)
                        // || targetClass.isAnnotationPresent(Configuration.class)
                        // || targetClass.isAnnotationPresent(RestController.class)
                        // || targetClass.isAnnotationPresent(Service.class)
                        || targetClass.isAnnotationPresent(ControllerAdvice.class);
                }

                public boolean isMethodAnnotated(Method method) {
                    return method.isAnnotationPresent(Monitored.class);
                }

                @Override
                public boolean matches(Method method, Class<?> targetClass) {
                    return isClassAnnotated(targetClass) || isMethodAnnotated(method);
                }
            };

            @Override
            public Advice getAdvice() {
                return new SimpleExecutionTimeInterceptor();
            }

            @Override
            public Pointcut getPointcut() {
                return this.pointcut;
            }
        };
    }

    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Monitored {}
}
