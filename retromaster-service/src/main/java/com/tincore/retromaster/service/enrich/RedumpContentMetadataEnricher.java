package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.*;
import com.tincore.util.StreamTrait;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.TreeSet;

@Slf4j
@Service
public class RedumpContentMetadataEnricher implements ContentMetadataEnricher, StreamTrait {

    @Override
    public void enrich(String title, ContentMetadata contentMetadata) {
        contentMetadata.setTitle(title);

        Set<String> regions = new TreeSet<>();
        Set<String> languages = new TreeSet<>();
        contentMetadata.getItems()
            .stream()
            .map(ContentMetadataItem::getName)
            .forEach(
                n -> {
                    if (!StringUtils.startsWithIgnoreCase(n, title)) {
                        return;
                    }

                    var tokens = StringUtils.substringsBetween(n, "(", ")");
                    if (tokens == null) {
                        return;
                    }

                    for (var token : tokens) {
                        var region = ContentRegion.MAP.getOrDefault(token, null);
                        var regionEmpty = region != null;
                        if (regionEmpty) {
                            regions.add(region);
                        } else if (token.startsWith("Track ")) {
                            log.trace("Import {}. Track token ignored", title);
                        } else if (token.startsWith("Disc")) {
                            contentMetadata.setMediaOrder(token.substring("Disc".length()).trim());
                        } else {
                            var langs = toStream(token.split(",")).map(String::toLowerCase).filter(ContentLanguage.SET::contains).toList();
                            if (langs.isEmpty()) {
                                contentMetadata.setTagsExtra(token);
                            } else {
                                languages.addAll(langs);
                            }
                        }
                    }
                });

        contentMetadata.setRegion(String.join(",", regions));
        contentMetadata.setLanguage(String.join(",", languages));
        contentMetadata.setSource(ContentMetadataSource.REDUMP.name());
    }
}
