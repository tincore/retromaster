package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.tincore.retromaster.domain.ContentMetadataSet.AUTHOR_LENGTH;

public interface ContentMetadataSetEnricher {
    Logger LOG = LoggerFactory.getLogger(ContentMetadataSetEnricher.class);

    boolean canEnrich(ContentMetadataSet contentMetadataSet);

    ContentMetadata enrichContentMetadata(ContentMetadata contentMetadata, String systemDefault);

    ContentMetadataSet enrichContentMetadataSet(ContentMetadataSet contentMetadataSet);

    List<ContentMetadata> enrichContentMetadatas(List<ContentMetadata> contentMetadatas, ContentMetadataSet enrichedContentMetadataSet, String systemDefault);

    String extractSystem(ContentMetadataSet enrichedContentMetadataSet);

    ContentMetadataSource getContentMetadataSource();

    String getRepositoryPath(ContentMetadataSet contentMetadataSet);

    default ContentMetadataSet reenrichContentMetadataSet(ContentMetadataSet contentMetadataSet) {
        contentMetadataSet.setAuthor(StringUtils.left(contentMetadataSet.getAuthor(), AUTHOR_LENGTH));
        var enrichedContentMetadataSet = enrichContentMetadataSet(contentMetadataSet);
        var currentRepositoryPath = enrichedContentMetadataSet.getRepositoryPath();
        var recalculatedRepositoryPath = getRepositoryPath(enrichedContentMetadataSet);
        if (!currentRepositoryPath.equals(recalculatedRepositoryPath)) {
            LOG.info("Metadata path recalculated {} => {}, cms={}", currentRepositoryPath, recalculatedRepositoryPath, enrichedContentMetadataSet);
            enrichedContentMetadataSet.setRepositoryPath(recalculatedRepositoryPath);
        }
        return enrichedContentMetadataSet;
    }
}
