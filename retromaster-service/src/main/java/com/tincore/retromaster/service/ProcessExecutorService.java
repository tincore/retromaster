package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentExecuteProcess;
import com.tincore.util.ProcessHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProcessExecutorService {

    private final Map<UUID, ContentExecuteProcess> contentExecuteProcesses = new HashMap<>();

    public int exec(String command, UUID executionId) throws IOException, InterruptedException {
        return exec(command, executionId, null);
    }

    public int exec(String command, UUID executionId, Consumer<String> onConsoleLine) throws IOException, InterruptedException {
        try {
            contentExecuteProcesses.put(executionId, new ContentExecuteProcess(executionId, command));
            return ProcessHelper.exec(command, onConsoleLine);
        } finally {
            contentExecuteProcesses.remove(executionId);
        }
    }

    public Collection<ContentExecuteProcess> findContentExecuteProcesses() {
        return contentExecuteProcesses.values();
    }
}
