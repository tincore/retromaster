package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.tincore.util.lang.ThrowingConsumer.uConsumer;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentMetadataVolumeService implements StreamTrait {

    private final RetromasterConfiguration retromasterConfiguration;

    private final AtomicBoolean initialized = new AtomicBoolean();

    public Path getDuplicatedPath() {
        return getExportPath().resolve(retromasterConfiguration.getMetadataExportDuplicateDirectoryName());
    }

    public Path getErrorPath() {
        return getExportPath().resolve(retromasterConfiguration.getMetadataExportErrorDirectoryName());
    }

    public Path getExportPath() {
        return retromasterConfiguration.getMetadataExportPath();
    }

    public Path getImportPath() {
        return retromasterConfiguration.getMetadataImportPaths().get(0);
    }

    public List<Path> getImportPaths() {
        return retromasterConfiguration.getMetadataImportPaths();
    }

    public Path getLegacyPath() {
        return getExportPath().resolve(retromasterConfiguration.getMetadataExportLegacyDirectoryName());
    }

    public Path getProdPath() {
        return retromasterConfiguration.getMetadataProdPath();
    }

    public Path getProdPath(String filename) {
        return getProdPath().resolve(filename);
    }

    @Transactional
    public void initialize() throws IOException {
        if (initialized.compareAndSet(false, true)) {
            log.info("initialize");
            retromasterConfiguration.getMetadataImportPaths().forEach(uConsumer(Files::createDirectories));
            Files.createDirectories(retromasterConfiguration.getMetadataExportPath());
            Files.createDirectories(retromasterConfiguration.getMetadataProdPath());
        } else {
            log.warn("initialize called more than once. Ignore");
        }
    }
}
