package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.service.archive.Archiver;
import com.tincore.retromaster.service.archive.ArchiverFormat;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryArchiveEntryProducer;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryProducer;
import com.tincore.retromaster.service.archive.producer.LazyExtractingArchiveEntryProducer;
import com.tincore.retromaster.service.archive.producer.PathArchiveEntryProducer;
import com.tincore.util.FileSystemTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.tincore.util.lang.ThrowingFunction.uFunction;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

@Slf4j
@Service
@RequiredArgsConstructor
public class ArchiveService implements FileSystemTrait {

    private final List<Archiver> archivers;

    public Path archive(Path targetPath, Path... paths) throws IOException {
        var archiveEntries = Stream.of(paths).map(uFunction(this::toArchiveEntryProducerStream)).flatMap(Collection::stream).toList();
        return archive(targetPath, archiveEntries);
    }

    public Path archive(Path targetPath, List<? extends ArchiveEntryProducer> archiveEntries) throws IOException {
        createDirectoriesParent(targetPath);
        try (var channel = Files.newByteChannel(targetPath, CREATE, WRITE)) {
            archive(toArchiverFormat(targetPath), channel, archiveEntries);
        }
        return targetPath;
    }

    public void archive(ArchiverFormat archiverFormat, SeekableByteChannel channel, List<? extends ArchiveEntryProducer> archiveEntries) throws IOException {
        getArchiver(archiverFormat).archive(channel, archiveEntries);
    }

    public ArchiveInputStream createArchiveInputStream(ArchiverFormat archiverFormat, SeekableByteChannel channel) throws IOException {
        return getArchiver(archiverFormat).createArchiveInputStream(channel);
    }

    public void extract(ArchiverFormat archiverFormat, Path targetPath, SeekableByteChannel channel, LocalDateTime lastModifiedTime) throws IOException {
        Files.createDirectories(targetPath);
        Function<ArchiveEntryProducer, Object> archiveExtractEntryObjectFunction = uFunction(
            p -> {
                var entryName = p.getName();
                var outEntryPath = Paths.get(targetPath.toString(), entryName);
                if (p.isDirectory()) {
                    Files.createDirectories(outEntryPath);
                } else {
                    copyToPath(outEntryPath, p.getInputStream());
                }
                return null;
            });
        extract(archiverFormat, channel, lastModifiedTime, archiveExtractEntryObjectFunction);
    }

    public <T> List<T> extract(ArchiverFormat archiverFormat, SeekableByteChannel channel, LocalDateTime lastModifiedTime, Function<ArchiveEntryProducer, T> consumer) throws IOException {
        try (var is = createArchiveInputStream(archiverFormat, channel)) {
            List<T> results = new ArrayList<>();
            ArchiveEntry entry;
            while ((entry = is.getNextEntry()) != null) {
                if (!is.canReadEntryData(entry)) {
                    log.warn("extract: Unreadable, entry={}", entry);
                    continue;
                }

                var v = consumer.apply(new ArchiveEntryArchiveEntryProducer(entry, is, lastModifiedTime)); // NOPMD absurd
                if (v != null) {
                    results.add(v);
                }
            }
            return results;
        }
    }

    public <T> List<T> extract(Path inputPath, Function<ArchiveEntryProducer, T> consumer) throws IOException {
        var archiverFormat = toArchiverFormat(inputPath);
        try (var channel = Files.newByteChannel(inputPath)) {
            return extract(archiverFormat, channel, getLastModifiedTime(inputPath), consumer);
        }
    }

    public void extract(ArchiverFormat archiverFormat, Path inputPath, Path targetPath) throws IOException {
        try (var channel = Files.newByteChannel(inputPath)) {
            extract(archiverFormat, targetPath, channel, getLastModifiedTime(inputPath));
        }
    }

    public List<byte[]> extractEntry(Path inputPath, String fileEntry, int blockSize) throws IOException {
        var archiverFormat = toArchiverFormat(inputPath);

        try (var channel = Files.newByteChannel(inputPath)) {
            List<List<byte[]>> cont = extract(
                archiverFormat,
                channel,
                null,
                a -> {
                    try {
                        if (a.isDirectory()) {
                            return null;
                        }
                        if (!fileEntry.equals(a.getName())) {
                            return null;
                        }

                        var fileContentSize = a.getSize();

                        var inputStream = a.getInputStream();
                        var blocks = new ArrayList<byte[]>();
                        var fileContentBytesToRead = fileContentSize;
                        do {
                            var len = (int) Math.min(blockSize, fileContentBytesToRead);
                            var block = new byte[len]; // NOPMD
                            IOUtils.read(inputStream, block, 0, len);
                            blocks.add(block);
                            fileContentBytesToRead -= blockSize;
                        } while (fileContentBytesToRead > 0);

                        return blocks;
                    } catch (IOException e) {
                        log.error("Could not get block", e);
                        return null;
                    }
                });

            if (cont.isEmpty()) {
                throw new IOException("Could not extract content path=" + inputPath);
            }
            return cont.get(0);
        }
    }

    public ArchiveEntryProducer extractToLazyArchiveEntryProducerByName(Path inputPath, String nameEquals, String targetName, long offset) throws IOException {
        return new LazyExtractingArchiveEntryProducer(inputPath, nameEquals, targetName, offset, getArchiver(toArchiverFormat(inputPath)));
    }

    public Path extractToPathByName(Path inputPath, String nameEquals, Path targetFile) throws IOException {
        var archiverFormat = toArchiverFormat(inputPath);
        try (var channel = Files.newByteChannel(inputPath)) {
            try (var is = createArchiveInputStream(archiverFormat, channel)) {
                ArchiveEntry entry;
                while ((entry = is.getNextEntry()) != null) {
                    if (!is.canReadEntryData(entry)) {
                        log.warn("extractToPathByName: Unreadable, entry={}", entry);
                    }

                    if (nameEquals.equals(entry.getName())) {
                        copyToPath(targetFile, is);
                        return targetFile;
                    }
                }
            }
        }
        throw new WrappedException("Could not find expected entry, name=" + nameEquals + ", inputpath= " + inputPath);
    }

    private Archiver getArchiver(ArchiverFormat archiverFormat) throws IOException {
        return archivers.stream().filter(a -> a.isHandlerOf(archiverFormat)).findFirst().orElseThrow(() -> new IOException("Unsupported format for Archive input findPath, format=" + archiverFormat));
    }

    public Optional<ArchiverFormat> getArchiverFormat(Path targetPath) {
        return ArchiverFormat.byPath(targetPath);
    }

    public Path rearchive(Path filePath, List<String> entryNamesToKeep) throws IOException {
        var tempFilePath = filePath.resolveSibling("TMP_" + filePath.getFileName());
        try {
            archive(tempFilePath, entryNamesToKeep.stream().map(uFunction(e -> extractToLazyArchiveEntryProducerByName(filePath, e, e, 0))).toList());
            return move(tempFilePath, filePath);
        } finally {
            Files.deleteIfExists(tempFilePath);
        }
    }

    public Path rearchive(Path originFilePath, List<String> entryNamesToKeep, Path targetFilePath) throws IOException {
        archive(targetFilePath, entryNamesToKeep.stream().map(uFunction(e -> extractToLazyArchiveEntryProducerByName(originFilePath, e, e, 0))).toList());
        return targetFilePath;
    }

    private List<PathArchiveEntryProducer> toArchiveEntryProducerStream(Path basePath) throws IOException {
        try (var s = findPaths(basePath)) {
            return s.map(uFunction(p -> new PathArchiveEntryProducer(p, basePath, null))).toList();
        }
    }

    public ArchiverFormat toArchiverFormat(Path targetPath) {
        return getArchiverFormat(targetPath).orElseThrow(() -> new IllegalArgumentException("Valid archiver format not found, path=" + targetPath));
    }
}
