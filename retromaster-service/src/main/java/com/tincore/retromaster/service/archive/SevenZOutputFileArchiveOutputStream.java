package com.tincore.retromaster.service.archive;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;

import java.io.File;
import java.io.IOException;

public class SevenZOutputFileArchiveOutputStream extends ArchiveOutputStream {

    private final SevenZOutputFile file;

    public SevenZOutputFileArchiveOutputStream(SevenZOutputFile file) {
        super();
        this.file = file;
    }

    @Override
    public void close() throws IOException {
        file.close();
    }

    @Override
    public void closeArchiveEntry() throws IOException {
        file.closeArchiveEntry();
    }

    @Override
    public ArchiveEntry createArchiveEntry(File inputFile, String entryName) {
        return file.createArchiveEntry(inputFile, entryName);
    }

    @Override
    public void finish() throws IOException {
        file.finish();
    }

    public SevenZOutputFile getSevenZOutputFile() {
        return file;
    }

    @Override
    public void putArchiveEntry(ArchiveEntry entry) {
        file.putArchiveEntry(entry);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        file.write(b, off, len);
    }

    @Override
    public void write(int b) throws IOException {
        file.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        file.write(b);
    }
}
