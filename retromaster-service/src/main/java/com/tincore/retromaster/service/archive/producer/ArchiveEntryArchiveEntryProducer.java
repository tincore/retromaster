package com.tincore.retromaster.service.archive.producer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

public class ArchiveEntryArchiveEntryProducer implements ArchiveEntryProducer {

    private final ArchiveEntry entry;
    private final ArchiveInputStream archiveInputStream;
    private final LocalDateTime modifiedTime;

    public ArchiveEntryArchiveEntryProducer(ArchiveEntry entry, ArchiveInputStream archiveInputStream, LocalDateTime modifiedTime) {
        this.entry = entry;
        this.archiveInputStream = archiveInputStream;
        this.modifiedTime = modifiedTime;
    }

    @Override
    public void close() throws IOException {
        if (archiveInputStream != null) {
            archiveInputStream.close();
        }
    }

    @Override
    public InputStream getInputStream() {
        return archiveInputStream;
    }

    @Override
    public LocalDateTime getModifiedTime() {
        return modifiedTime;
    }

    @Override
    public String getName() {
        return entry.getName();
    }

    @Override
    public long getSize() {
        return entry.getSize();
    }

    @Override
    public boolean isDirectory() {
        return entry.isDirectory();
    }

    @Override
    public boolean isRegularFile() {
        return !isDirectory();
    }
}
