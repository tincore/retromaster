package com.tincore.retromaster.service.contentsystem;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentSystem;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public interface ContentSystemExtractor {
    static String normalizeSystemLabel(String label) {
        if (StringUtils.isBlank(label)) {
            return "UNKNOWN";
        }

        var systemNormalized = StringUtils.replace(label.toLowerCase().trim(), " - ", "");
        systemNormalized = StringUtils.replace(systemNormalized, "-", "");
        systemNormalized = StringUtils.replace(systemNormalized, ".", "");
        systemNormalized = StringUtils.replace(systemNormalized, " & ", "");
        systemNormalized = StringUtils.replace(systemNormalized, "_", "");
        systemNormalized = StringUtils.replace(systemNormalized, " ", "");
        return systemNormalized;
    }

    Optional<ContentSystem> extract(ContentMetadata contentMetadata);

    ContentSystem getContentSystem();

    boolean isMatch(ContentMetadata contentMetadata);

    boolean isMatchBySystemLabel(String system);
}
