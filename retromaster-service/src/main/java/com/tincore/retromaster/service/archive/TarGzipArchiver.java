package com.tincore.retromaster.service.archive;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.SeekableByteChannel;
import java.util.List;

import static com.tincore.retromaster.service.archive.CompressorFormat.GZIP;

@Slf4j
@Service
public class TarGzipArchiver extends AbstractTarArchiver {

    public TarGzipArchiver(RetromasterConfiguration.ArchiverConfiguration archiverConfiguration) {
        super(archiverConfiguration);
    }

    @Override
    public void archive(SeekableByteChannel channel, List<? extends ArchiveEntryProducer> archiveEntries) throws IOException {
        super.archive(createCompressorOutputStream(GZIP, Channels.newOutputStream(channel)), archiveEntries);
    }

    @Override
    public ArchiveInputStream createArchiveInputStream(SeekableByteChannel channel) throws IOException {
        return new TarArchiveInputStream(new GzipCompressorInputStream(Channels.newInputStream(channel)));
    }

    @Override
    public boolean isHandlerOf(ArchiverFormat archiverFormat) {
        return ArchiverFormat.TAR.equals(archiverFormat);
    }
}
