package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import static com.tincore.retromaster.domain.ContentExtension.*;
import static com.tincore.retromaster.domain.ContentSystem.*;

@Component
@AllArgsConstructor
class Retroarch1ContentExecutorFactory { // NOPMD Cyclomatic BS

    private final RetromasterConfiguration.RetroarchExecuteConfiguration retroarchExecuteConfiguration;

    @Bean
    public RetroarchContentExecutor retroarch4DOContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("4do").contentSystems(PANASONIC_3DO).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarch81ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("81").contentSystems(SINCLAIR_ZX81).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchAtari800ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("atari800").contentSystems(ATARI_8BIT, ATARI_5200).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBeetleDcContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("beetledc").contentSystems(SEGA_DREAMCAST, SEGA_NAOMI).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBeetleDcWinCeContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("beetledc_wince").contentSystems(SEGA_DREAMCAST).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBkContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bk").contentSystems(ELEKTRONIKA_BK_0010, ELEKTRONIKA_BK_0011).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBlastemContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("blastem").contentSystems(SEGA_MEGADRIVE).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBlueMsxContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("bluemsx")
                .contentSystems(MICROSOFT_MSX_1, MICROSOFT_MSX_2, MICROSOFT_MSX_2P, SEGA_GAME_1000, COLECO_COLECOVISION)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBnesContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bnes").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnes2014AccuracyContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes2014_accuracy").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnes2014BalancedContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes2014_balanced").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnes2014PerformanceContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes2014_performance").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnesAccuracyContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes_accuracy").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnesBalancedContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes_balanced").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnesCplusplus98ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes_cplusplus98").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnesHdBetaContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes_hd_beta").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnesMercuryAccuracyContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes_mercury_accuracy").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnesMercuryBalancedContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes_mercury_balanced").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnesMercuryPerformanceContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes_mercury_performance").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchBsnesPerformanceContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("bsnes_performance").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    /** https://docs.libretro.com/library/caprice32/ */
    @Bean
    public RetroarchContentExecutor retroarchCap32ContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("cap32")
                .contentSystems(AMSTRAD_CPC)
                .contentExtensions(dsk, sna, tap, cdt, m3u, cpr)
                .multipartCapable(true)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    /** https://docs.libretro.com/library/caprice32/ */
    @Bean
    public RetroarchContentExecutor retroarchCap3PlusContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreLabel("Cap32 CPC 6128+")
                .coreName("cap32")
                .coreVariant("plus")
                .coreOptions("cap32_model = \"6128+\"")
                .contentSystems(AMSTRAD_CPC)
                .contentExtensions(dsk, sna, tap, cdt, m3u, cpr)
                .multipartCapable(true)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchCitraCanaryContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("citra_canary").contentSystems(NINTENDO_3DS).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchCitraContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("citra").contentSystems(NINTENDO_3DS).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchCrocodsContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("crocods").contentSystems(AMSTRAD_CPC).contentExtensions(dsk, sna, kcr).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchDesmume2015ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("desmume2015").contentSystems(NINTENDO_DS).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchDesmumeContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("desmume").contentSystems(NINTENDO_DS).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchDolphinContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("dolphin").contentSystems(NINTENDO_GAMECUBE, NINTENDO_WII).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchDosboxContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("dosbox_core").contentSystems(IBM_PC).contentExtensions(exe, com, bat).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchDosboxPureContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("dosbox_pure").contentSystems(IBM_PC).contentExtensions(exe, com, bat).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchDosboxSvnContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("dosbox_svn").contentSystems(IBM_PC).contentExtensions(exe, com, bat).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchDuckstationContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("duckstation").contentSystems(SONY_PLAYSTATION).contentExtensions(cue, img, chd, m3u).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchEmuxChip8ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("emux_chip8").contentSystems(RCA_CHIP_8).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchEmuxGbContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("emux_gb").contentSystems(NINTENDO_GAMEBOY, NINTENDO_GAMEBOY_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchEmuxNesContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("emux_nes").contentSystems(NINTENDO_NES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchEmuxSmsContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("emux_sms").contentSystems(SEGA_MASTERSYSTEM).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFbAlphaNeoGeoCdContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("fbalpha").contentSystems(SNK_NEOGEO_CD).paramsExtra("--subsystem neocd").retroarchExecuteConfiguration(retroarchExecuteConfiguration).build()) {
            @Override
            public boolean isReadOnlyExecutionCapable() {
                return false;
            }
        };
    }

    @Bean
    public RetroarchContentExecutor retroarchFceummContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("fceumm").contentSystems(NINTENDO_NES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFixGbContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("fixgb").contentSystems(NINTENDO_GAMEBOY, NINTENDO_GAMEBOY_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFixNesContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("fixnes").contentSystems(NINTENDO_NES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFlycastContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("flycast")
                .contentSystems(SEGA_DREAMCAST, SEGA_NAOMI)
                .contentExtensions(chd, cdi, m3u, cue, gdi)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFlycastGles2ContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("flycast_gles2")
                .contentSystems(SEGA_DREAMCAST, SEGA_NAOMI)
                .contentExtensions(chd, cdi, m3u, cue, gdi)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFmsxContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("fmsx").contentSystems(MICROSOFT_MSX_1, MICROSOFT_MSX_2).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFreeChafContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("freechaf").contentSystems(FAIRCHILD_CHANNEL_F).contentExtensions(bin, chf).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFreeIntvContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("freeintv").contentSystems(MATTEL_INTELLIVISION).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFrodoContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("frodo").contentSystems(COMMODORE_C64).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFsuaeContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("fsuae").contentSystems(COMMODORE_AMIGA).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchFuseContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("fuse").contentSystems(SINCLAIR_ZX_SPECTRUM, SINCLAIR_ZX_SPECTRUM_128).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchGambatteContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("gambatte").contentSystems(NINTENDO_GAMEBOY, NINTENDO_GAMEBOY_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchGearboyContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("gearboy").contentSystems(NINTENDO_GAMEBOY, NINTENDO_GAMEBOY_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchGearsystemContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("gearsystem").contentSystems(SEGA_MASTERSYSTEM, SEGA_GAMEGEAR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchGenesisPlusGxContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("genesis_plus_gx")
                .contentSystems(SEGA_GAMEGEAR, SEGA_MEGADRIVE, SEGA_MASTERSYSTEM, SEGA_CD, SEGA_PICO, SEGA_GAME_1000)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchGpSpContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("gpsp").contentSystems(NINTENDO_GAMEBOY_ADVANCE).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchHandyContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("handy").contentSystems(ATARI_LYNX).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchHatariContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("hatari").contentSystems(ATARI_ST).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchHiganSfcBalancedContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("higan_sfc_balanced").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchHiganSfcContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("higan_sfc").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchIshiirukaContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("ishiiruka").contentSystems(NINTENDO_WII, NINTENDO_GAMECUBE).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchKronosContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("kronos").contentSystems(SEGA_SATURN).contentExtensions(bin, cue, iso, mds, ccd).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }
}
