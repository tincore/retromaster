package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.util.StreamTrait;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static com.tincore.retromaster.domain.ContentMetadata.TAGS_EXTRA_LENGTH;

@Service
public class ComputerEmuZoneContentMetadataEnricher implements ContentMetadataEnricher, StreamTrait {

    public static final int MAX_PUBLISHERS = 3;

    @Override
    public void enrich(String title, ContentMetadata contentMetadata) {
        contentMetadata.setTitle(title);

        Set<String> tagExtra = new TreeSet<>();
        Set<String> regions = new TreeSet<>();
        Set<String> dates = new TreeSet<>();
        List<String> publishers = new ArrayList<>();
        contentMetadata.getItems()
            .stream()
            .map(ContentMetadataItem::getName)
            .forEach(
                n -> {
                    if (!StringUtils.startsWithIgnoreCase(n, title)) {
                        return;
                    }

                    var tokens = StringUtils.substringsBetween(n, "(", ")");
                    if (tokens != null) {
                        for (var token : tokens) {
                            var possibleYear = StringUtils.substringAfterLast(token, " ").trim();
                            if (StringUtils.isNumeric(possibleYear)) {
                                publishers.add(StringUtils.substringBeforeLast(token, " ").trim());
                                dates.add(possibleYear);
                            } else {
                                publishers.add(token.trim());
                            }
                        }
                    }
                });
        contentMetadata.setSource(ContentMetadataSource.COMPUTER_EMUZONE.name());

        contentMetadata.setRegion(String.join(",", regions));
        contentMetadata.setDate(String.join(",", dates));
        contentMetadata.setPublisher(publishers.stream().distinct().limit(MAX_PUBLISHERS).collect(Collectors.joining(",")));
        contentMetadata.setTagsExtra(StringUtils.left(String.join(",", tagExtra), TAGS_EXTRA_LENGTH));
    }
}
