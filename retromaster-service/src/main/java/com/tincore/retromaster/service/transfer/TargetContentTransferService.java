package com.tincore.retromaster.service.transfer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.transfer.TargetContentTransfer;
import com.tincore.retromaster.service.ContentMetadataItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TargetContentTransferService {
    private final ContentMetadataItemService contentMetadataItemService;

    public TargetContentTransfer createTargetContentTransfer(Content content) {
        return new TargetContentTransfer(content, contentMetadataItemService.getContentMetadataItemByContentAndNotStagingSortedByPrecedence(content));
    }
}
