package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.FilesystemAvFsConfiguration;
import com.tincore.retromaster.config.FilesystemUnionFsConfiguration;
import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.domain.event.*;
import com.tincore.retromaster.service.fs.AvFsService;
import com.tincore.retromaster.service.fs.UnionFsService;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class RetromasterOrchestratorServiceImpl implements StreamTrait, FileSystemTrait, RetromasterOrchestratorService, RetromasterContentEventHandler { // NOPMD No God

    // Should eventually: Should report phase of what he is doing. Add this state to API
    private final RetromasterConfiguration retromasterConfiguration;

    private final FilesystemAvFsConfiguration filesystemAvFsConfiguration;
    private final FilesystemUnionFsConfiguration filesystemUnionFsConfiguration;

    private final ContentMetadataVolumeService contentMetadataVolumeService;
    private final ContentVolumeService contentVolumeService;

    private final ContentVolumeRepositoryService contentVolumeRepositoryService;

    private final ContentMetadataSetService contentMetadataSetService;
    private final ContentMetadataSetImportService contentMetadataSetImportService;

    private final ContentSignatureChangeService contentSignatureChangeService;

    private final ContentUpdateAutometadataService contentUpdateAutometadataService;
    private final ContentUpdateService contentUpdateService;

    private final ContentMetadataSearchService contentMetadataSearchService;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final UnionFsService unionFsService;
    private final AvFsService avFsService;

    private final ContentService contentService;

    private final AtomicBoolean contentMetadataSetImportRequested = new AtomicBoolean();
    private final AtomicBoolean contentMetadataSetReimportRequested = new AtomicBoolean();
    private final AtomicBoolean contentMetadataSetReorganizeRequested = new AtomicBoolean();

    private final AtomicBoolean contentImportRequested = new AtomicBoolean();
    private final AtomicBoolean contentReindexRequested = new AtomicBoolean();
    private final AtomicBoolean contentStagingScanRequested = new AtomicBoolean();
    private final AtomicBoolean contentReorganizeRequested = new AtomicBoolean();
    private final AtomicBoolean contentCleanOrphanRequested = new AtomicBoolean();

    private final Queue<ContentMetadata> contentScanByContentMetadataRequest = new ConcurrentLinkedQueue<>();
    private final Queue<ContentMetadataSet> contentScanByContentMetadataSetRequest = new ConcurrentLinkedQueue<>();
    private final Queue<Pair<ContentVolume, String>> contentScanByVolumeAndDirectory = new ConcurrentLinkedQueue<>();

    private final AtomicBoolean initialized = new AtomicBoolean();

    private boolean contentMetadataSetImportAuto = true;

    private boolean contentImportAuto = true;

    @Override
    public void doContentReorganizeRequest() {
        contentReorganizeRequested.set(true);
        contentCleanOrphanRequested.set(true);
    }

    @Override
    public void doContentScanByContentMetadataRequest(ContentMetadata contentMetadata) {
        contentScanByContentMetadataRequest.add(contentMetadata);
    }

    @Override
    public void doContentScanByContentMetadataSetRequest(ContentMetadataSet contentMetadataSet) {
        contentScanByContentMetadataSetRequest.add(contentMetadataSet);
    }

    @Override
    public void doContentScanByContentVolumeStagingRequest() {
        contentStagingScanRequested.set(true);
    }

    @Override
    public void doContentScanByVolumeAndDirectoryRequest(Pair<ContentVolume, String> pair) {
        contentScanByVolumeAndDirectory.add(pair);
    }

    @Override
    public void doReindexRequest() {
        contentReindexRequested.set(true);
    }

    @Override
    @Scheduled(fixedDelayString = "${tincore.retromaster.orchestration.delay}")
    public void execute() { // NOPMD BS
        if (!retromasterConfiguration.getOrchestration().isEnabled() || !initialized.get()) {
            return;
        }
        log.trace("execute");
        try {

            if (contentMetadataSetReimportRequested.compareAndSet(true, false)) {
                contentMetadataSetImportService.doContentMetadataSetsReimport(
                    retromasterConfiguration.getOrchestration().isOnStartMetadataSetReimportRenormalizeSets(),
                    retromasterConfiguration.getOrchestration().isOnStartMetadataSetReimportRenormalizeDatas());
            }
            if (contentMetadataSetReorganizeRequested.compareAndSet(true, false)) {
                var movedCount = contentMetadataSetService.deleteContentMetadataSetsByObsoleteVersion();
                var deletedCount = contentMetadataSetService.deleteContentMetadataSetsByMissingDatFiles();
                var stagingDeletedCount = contentUpdateAutometadataService.deleteContentMetadataSetsByStageContentVolumesAndMissingContentDirectory();
                if (movedCount > 0 || deletedCount > 0 || stagingDeletedCount > 0) {
                    applicationEventPublisher.publishEvent(new ContentMetadataSetManageEvent(ContentMetadataSetManageEventType.METADATAS_MODIFIED, "MetadataSets reorganization modified", movedCount + deletedCount + stagingDeletedCount));
                }
            }

            if (contentMetadataSetImportRequested.compareAndSet(true, false)) {
                contentMetadataVolumeService.getImportPaths().forEach(contentMetadataSetImportService::doContentMetadataSetImportByDirectory);
                contentMetadataSetImportRequested.set(contentMetadataSetImportAuto);
            }

            if (contentReorganizeRequested.compareAndSet(true, false)) {
                for (var contentVolume : contentVolumeRepositoryService.findByEnvironment(ContentVolumeEnvironment.PROD)) {
                    contentUpdateService.updateContentsByContentVolume(contentVolume, false);
                }
            }

            while (!contentScanByVolumeAndDirectory.isEmpty()) {
                var pair = contentScanByVolumeAndDirectory.poll();
                if (pair != null) {
                    var contentVolume = pair.getLeft();
                    var subPath = pair.getRight();
                    if (contentVolume.isStage()) {
                        contentUpdateAutometadataService.updateContentsByContentVolumeAndSubPath(contentVolume, subPath, true);
                    } else {
                        contentUpdateService.updateContentsByContentVolumeAndDirectory(contentVolume, subPath, false);
                    }
                }
            }
            while (!contentScanByContentMetadataRequest.isEmpty()) {
                var head = contentScanByContentMetadataRequest.poll();
                if (head != null) {
                    contentService.deleteContentsByMetadataIdAndMissingFiles(head.getId());
                }
            }
            while (!contentScanByContentMetadataSetRequest.isEmpty()) {
                var contentMetadataSet = contentScanByContentMetadataSetRequest.poll();
                if (contentMetadataSet != null) {
                    if (contentMetadataSet.isStaging()) {
                        contentUpdateAutometadataService.updateContentsByContentMetadataSet(contentMetadataSet);
                    } else {
                        contentUpdateService.updateContentsByContentMetadataSet(contentMetadataSet);
                    }
                }
            }
            if (contentCleanOrphanRequested.compareAndSet(true, false)) {
                for (var contentVolume : contentVolumeRepositoryService.findByEnvironment(ContentVolumeEnvironment.STAGE)) {
                    contentService.deleteContentsByVolumeAndMissingFiles(contentVolume);
                }
                for (var contentVolume : contentVolumeRepositoryService.findByEnvironment(ContentVolumeEnvironment.PROD)) {
                    contentService.deleteContentsByVolumeAndMissingFiles(contentVolume);
                }
            }

            if (contentImportRequested.compareAndSet(true, false)) {
                for (var contentVolume : contentVolumeRepositoryService.findByEnvironment(ContentVolumeEnvironment.IMPORT)) {
                    try {
                        contentUpdateService.updateContentsFromImportVolume(contentVolume);
                    } catch (ExecutionException | InterruptedException | IOException e) {
                        log.error("updateContentsFromImportVolume failed", e);
                    } finally {
                        contentImportRequested.set(false); // Debounce
                    }
                }
                contentImportRequested.set(contentImportAuto);
            }

            if (contentStagingScanRequested.compareAndSet(true, false)) {
                contentUpdateAutometadataService.deleteContentMetadataSetsByStageContentVolumesAndMissingContentDirectory();
                contentUpdateAutometadataService.updateContentsByStageContentVolumes();
            }

            if (contentReindexRequested.compareAndSet(true, false)) {
                try {
                    contentMetadataSearchService.reindex(ContentMetadata.class);
                    contentSignatureChangeService.reset();
                } catch (InterruptedException e) {
                    log.error("mass context reindex failed", e);
                } finally {
                    contentReindexRequested.set(false); // Debounce
                }
            } else {
                contentSignatureChangeService.reindexContentSignatureChanges();
            }

        } catch (IOException | ExecutionException | InterruptedException e) {
            log.error("Execute interrupted with exception", e);
        }
    }

    @Async
    @EventListener
    @Override
    public void onEvent(ContentExecutionEvent event) {
        log.debug("onEvent {}", event);
    }

    @Async
    @EventListener
    @Override
    public void onEvent(ContentImportEvent event) {
        log.debug("onEvent {}", event);
        if (ContentImportEventType.SUCCESS.equals(event.getType()) || ContentImportEventType.FAIL.equals(event.getType())) {
            contentMetadataSearchService.reset();
        }
    }

    @Async
    @EventListener
    @Override
    public void onEvent(ContentMetadataSetManageEvent event) {
        log.debug("onEvent {}", event);
        if (ContentMetadataSetManageEventType.METADATAS_MODIFIED.equals(event.getType())) {
            contentMetadataSearchService.reset();
        } else if (ContentMetadataSetManageEventType.IMPORT_SUCCESS.equals(event.getType())) {
            contentReorganizeRequested.set(true);
        }
    }

    @Async
    @EventListener
    @Override
    public void onEvent(ContentVolumeCreatedEvent event) {
        log.debug("onEvent {}", event);
    }

    @Async
    @EventListener
    @Override
    public void onEvent(ContentIndexEvent event) {
        log.debug("onEvent {}", event);
        if (ContentIndexEventType.SUCCESS.equals(event.getType()) || ContentIndexEventType.FAIL.equals(event.getType())) {
            contentMetadataSearchService.reset();
        }
    }

    @Async
    @EventListener
    @Override
    public void onEvent(ContentScanEvent event) {
        log.debug("onEvent {}", event);
    }

    @PostConstruct
    public void postConstruct() throws Exception {
        contentMetadataSetImportAuto = retromasterConfiguration.getOrchestration().isContentMetadataSetImportAuto();
        contentMetadataSetImportRequested.set(contentMetadataSetImportAuto);
        contentMetadataSetReimportRequested.set(retromasterConfiguration.getOrchestration().isOnStartMetadataSetReimport());
        contentMetadataSetReorganizeRequested.set(retromasterConfiguration.getOrchestration().isOnStartMetadataSetReorganize());

        contentImportAuto = retromasterConfiguration.getOrchestration().isContentImportAuto();
        contentImportRequested.set(contentImportAuto);
        contentReorganizeRequested.set(retromasterConfiguration.getOrchestration().isOnStartContentReorganize());
        contentReindexRequested.set(retromasterConfiguration.getOrchestration().isOnStartContentReindex());

        contentStagingScanRequested.set(false);
        contentCleanOrphanRequested.set(retromasterConfiguration.getOrchestration().isOnStartContentCleanOrphan());

        contentMetadataVolumeService.initialize();
        contentVolumeService.initialize();

        setUpVirtualExternalFilesystems();

        initialized.set(true);

        log.debug("Orchestrator initialized, path={}", Path.of("").toAbsolutePath());

    }

    private void setUpVirtualAvfsFilesystems() throws IOException {
        if (!avFsService.isActive() && filesystemAvFsConfiguration.isAutocreate()) {
            try {
                avFsService.mount();
            } catch (InterruptedException e) {
                log.error("Mounting AVFS virtual filesystems", e);
            }
        }
    }

    private void setUpVirtualExternalFilesystems() throws IOException {
        if (filesystemAvFsConfiguration.isEnabled()) {
            setUpVirtualAvfsFilesystems();
            setUpVirtualUnionFilesystems();
        }
    }

    private void setUpVirtualUnionFilesystems() throws IOException {
        if (avFsService.isActive() && filesystemUnionFsConfiguration.isEnabled() && filesystemUnionFsConfiguration.isAutocreate()) {
            try {
                var prodVolumes = contentVolumeService.getContentVolumesByEnvironment(ContentVolumeEnvironment.PROD);
                var stageVolumes = contentVolumeService.getContentVolumesByEnvironment(ContentVolumeEnvironment.STAGE);
                var volumes = Stream.concat(prodVolumes.stream(), stageVolumes.stream()).toList();

                for (var volume : volumes) {
                    unionFsService.mount(volume, avFsService.getPath(volume));
                }
            } catch (InterruptedException e) {
                log.error("Mounting virtual filesystems", e);
            }
        }
    }
}

