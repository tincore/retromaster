package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMediaType;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.retromaster.service.ContentSystemService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DummyContentMetadataSetEnricher implements ContentMetadataSetEnricher {

    private final ContentSystemService contentSystemService;

    @Override
    public boolean canEnrich(ContentMetadataSet contentMetadataSet) {
        return false;
    }

    @Override
    public ContentMetadata enrichContentMetadata(ContentMetadata contentMetadata, String systemDefault) {
        contentMetadata.setMediaType(Optional.ofNullable(contentMetadata.getMediaType()).orElse(ContentMediaType.UNKNOWN));
        contentMetadata.setSystem(systemDefault);
        return contentMetadata;
    }

    @Override
    public ContentMetadataSet enrichContentMetadataSet(ContentMetadataSet contentMetadataSet) {
        if (StringUtils.isBlank(contentMetadataSet.getCategory())) {
            contentMetadataSet.setCategory("UNKNOWN");
        }
        return contentMetadataSet;
    }

    @Override
    public List<ContentMetadata> enrichContentMetadatas(List<ContentMetadata> contentMetadatas, ContentMetadataSet enrichedContentMetadataSet, String systemDefault) {
        return contentMetadatas;
    }

    @Override
    public String extractSystem(ContentMetadataSet enrichedContentMetadataSet) {
        return contentSystemService.findSystemNormalized(enrichedContentMetadataSet.getName());
    }

    @Override
    public ContentMetadataSource getContentMetadataSource() {
        return ContentMetadataSource.UNKNOWN;
    }

    @Override
    public String getRepositoryPath(ContentMetadataSet contentMetadataSet) {
        return "dummy - " + contentMetadataSet.getName();
    }
}
