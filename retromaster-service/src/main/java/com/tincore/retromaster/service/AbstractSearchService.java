package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.event.ContentIndexEvent;
import com.tincore.retromaster.domain.event.ContentIndexEventType;
import com.tincore.util.StreamTrait;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.SortField;
import org.hibernate.CacheMode;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.massindexing.MassIndexingMonitor;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public abstract class AbstractSearchService<T> implements StreamTrait {
    public static final int EXPLANATION_INDEX = 1;
    public static final int DOCUMENT_INDEX = 4;
    public static final int SCORE_INDEX = 3;
    // Move this to config...
    private static final int TIMEOUT_MILLIS_MIN = 1000;
    private static final int TIMEOUT_MILLIS_UNIT = 500;
    private static final int TIMEOUT_MAX = 60_000_000;

    private final Class<T> type;

    @Autowired
    private RetromasterConfiguration retromasterConfiguration;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @PersistenceContext
    private EntityManager entityManager;

    public AbstractSearchService(Class<T> type) {
        this.type = type;
    }

    protected SearchSession getSearchSession() {
        return Search.session(entityManager);
    }

    // Lucene does not really accept long!
    public long getTimeoutMillisByExpectedMaxElementCount(long count) {
        return (long) Math.min((double) (TIMEOUT_MILLIS_MIN + TIMEOUT_MILLIS_UNIT * count), TIMEOUT_MAX);
    }

    public long getTimeoutMillisByMax() {
        return TIMEOUT_MAX;
    }

    public Class<T> getType() {
        return type;
    }

    @Transactional
    public void reindex(Class... types) throws InterruptedException {
        var label = Stream.of(types).map(Class::getName).collect(Collectors.joining(","));
        var monitor = new MassIndexingMonitor() {
            private final AtomicLong entitiesLoadedCount = new AtomicLong();
            private final AtomicLong totalCount = new AtomicLong();
            private final AtomicLong lastEntitiesLoadedCount = new AtomicLong();

            @Override
            public void addToTotalCount(long count) {
                totalCount.addAndGet(count);
                applicationEventPublisher.publishEvent(new ContentIndexEvent(ContentIndexEventType.PROGRESS, label, entitiesLoadedCount.get(), totalCount.get()));
            }

            @Override
            public void documentsAdded(long increment) {
                // Do nothing
            }

            @Override
            public void documentsBuilt(long number) {
                // Do nothing
            }

            @Override
            public void entitiesLoaded(long size) {
                entitiesLoadedCount.addAndGet(size);
                if (entitiesLoadedCount.longValue() - lastEntitiesLoadedCount.longValue() > retromasterConfiguration.getIndexPublishEventEntitiesLoadedInterval()) {
                    lastEntitiesLoadedCount.set(entitiesLoadedCount.get());
                    applicationEventPublisher.publishEvent(new ContentIndexEvent(ContentIndexEventType.PROGRESS, label, entitiesLoadedCount.get(), totalCount.get()));
                }
            }

            @Override
            public void indexingCompleted() {
                applicationEventPublisher.publishEvent(new ContentIndexEvent(ContentIndexEventType.SUCCESS, label, entitiesLoadedCount.get(), totalCount.get()));
            }
        };

        applicationEventPublisher.publishEvent(new ContentIndexEvent(ContentIndexEventType.START, label, 0, 0));
        Search.session(entityManager)
            .massIndexer(types)
            .monitor(monitor)
            .threadsToLoadObjects(retromasterConfiguration.getIndexThreadCount())
            .batchSizeToLoadObjects(retromasterConfiguration.getIndexBatchSize())
            .idFetchSize(retromasterConfiguration.getIndexIdFetchSize())
            // .cacheMode(CacheMode.NORMAL)
            .cacheMode(CacheMode.IGNORE)
            // .transactionTimeout(10)
            .startAndWait();
    }

    // @Transactional
    // void reindexAll(Class message){
    // QueryBuilder builder = getEntityContext().get();
    // Query query = builder.all().createQuery();
    // FullTextQuery toFullTextQuery = toFullTextQuery(query);
    //
    // toFullTextQuery.setFlushMode(FlushMode.MANUAL);
    //
    //
    // FullTextSession fullTextSession = getSearchSession().getFullTextSession(session);
    // Transaction tx = fullTextSession.beginTransaction();
    // fullTextSession.setHibernateFlushMode(FlushMode.MANUAL);
    // fullTextSession.setCacheMode(CacheMode.IGNORE);
    //// Scrollable results will avoid loading too many objects in memory
    // int fetchSize = 100;
    // ScrollableResults results = fullTextSession.createCriteria( message )
    // .setFetchSize(fetchSize)
    // .scroll(ScrollMode.FORWARD_ONLY);
    // int index = 0;
    // while(results.next()) {
    // index++;
    // fullTextSession.index(results.get(0)); //index each element
    // if (index % fetchSize == 0) {
    // fullTextSession.flushToIndexes(); //apply changes to indexes
    // fullTextSession.clear(); //free memory since the queue is processed
    // }
    // }
    // tx.commit();
    // }

    SortField toSortField(String field, Sort.Direction direction) {
        return new SortField(field, SortField.Type.STRING, Sort.Direction.DESC.equals(direction));
    }
}
