package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.domain.event.ContentVolumeCreatedEvent;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.StreamSupport;

import static com.tincore.util.lang.ThrowingConsumer.uConsumer;
import static com.tincore.util.lang.ThrowingSupplier.uSupplier;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentVolumeService implements StreamTrait, FileSystemTrait {

    private final ApplicationEventPublisher applicationEventPublisher;

    private final ContentVolumeRepositoryService contentVolumeRepositoryService;

    private final RetromasterConfiguration retromasterConfiguration;

    private final AtomicBoolean initialized = new AtomicBoolean();

    private final Map<Path, Pair<LocalDateTime, List<Path>>> directoriesByVolumeRepository = new ConcurrentHashMap<>();

    @Cacheable("contentVolume")
    public ContentVolume getContentVolumeByEnvironment(ContentVolumeEnvironment environment) {
        // It should look for priority and closest in device to staging volume so moves are quicker
        // can also take filesystem space in consideration
        return contentVolumeRepositoryService.findByEnvironment(environment).stream().findFirst().orElseThrow(() -> new WrappedException(String.format("Could not find volume for environment %s", environment)));
    }

    public List<ContentVolume> getContentVolumesByEnvironment(ContentVolumeEnvironment environment) {
        return contentVolumeRepositoryService.findByEnvironment(environment);
    }

    @Cacheable("contentVolumeDirectoryPaths")
    public List<Path> getDirectoryPaths(ContentVolume volume) throws IOException {
        var volumePath = volume.toPath();

        if (!Files.exists(volumePath)) {
            return Collections.emptyList();
        }

        var volumePathLastModifiedTime = getLastModifiedTime(volumePath);
        Pair<LocalDateTime, List<Path>> entry = directoriesByVolumeRepository.get(volumePath);
        if (entry != null && entry.getKey().equals(volumePathLastModifiedTime)) {
            return entry.getValue();
        }

        try (var directoryStream = Files.newDirectoryStream(volumePath)) {
            List<Path> directoryPaths = StreamSupport.stream(directoryStream.spliterator(), false).filter(Files::isDirectory).map(volumePath::relativize).sorted().toList();
            directoriesByVolumeRepository.put(volumePath, Pair.of(volumePathLastModifiedTime, directoryPaths));
            return directoryPaths;
        }
    }

    public Path getExportErrorPath() {
        return retromasterConfiguration.getDataExportPath().resolve(retromasterConfiguration.getDataExportErrorDirectoryName());
    }

    public Path getExportUnknownPath() {
        return retromasterConfiguration.getDataExportPath().resolve(retromasterConfiguration.getDataExportUnknownDirectoryName());
    }

    @Transactional
    public void initialize() throws IOException {
        if (initialized.compareAndSet(false, true)) {
            log.info("initialize");

            Files.createDirectories(getExportErrorPath());
            Files.createDirectories(getExportUnknownPath());

            initialize(retromasterConfiguration.getDataImportPaths(), ContentVolumeEnvironment.IMPORT);
            initialize(retromasterConfiguration.getDataProdPaths(), ContentVolumeEnvironment.PROD);
            initialize(retromasterConfiguration.getDataStagePaths(), ContentVolumeEnvironment.STAGE);

            for (var volume : contentVolumeRepositoryService.findAll()) {
                Files.createDirectories(volume.toPath());
            }
        } else {
            log.warn("initialize called more than once. Ignore");
        }
    }

    private void initialize(List<Path> paths, ContentVolumeEnvironment environment) {
        var existingVolumes = contentVolumeRepositoryService.findByEnvironment(environment);
        existingVolumes.forEach(
            v -> {
                if (!Files.exists(v.toPath())) {
                    var existingPath = paths.stream().filter(Files::exists).findFirst();
                    if (existingPath.isPresent() && existingVolumes.size() == 1) {
                        v.setPath(existingPath.get().toString());
                        contentVolumeRepositoryService.save(v);
                    }
                }
            });

        toStream(paths)
            .forEach(
                uConsumer(
                    p -> {
                        var contentVolume = contentVolumeRepositoryService.findByPath(p.toString()).orElseGet(uSupplier(() -> save(new ContentVolume(environment.name(), p, environment))));
                        if (!Files.exists(contentVolume.toPath())) {
                            Files.createDirectories(contentVolume.toPath());
                        }
                    }));
    }

    @Transactional
    public ContentVolume save(ContentVolume contentVolume) throws IOException {
        validatePath(contentVolume.toPath());

        var volume = contentVolumeRepositoryService.findByPath(contentVolume.getPath()).orElse(null);
        if (volume != null) {
            return volume;
        }

        var savedVolume = contentVolumeRepositoryService.save(contentVolume);
        Files.createDirectories(savedVolume.toPath());
        applicationEventPublisher.publishEvent(new ContentVolumeCreatedEvent(savedVolume));
        return savedVolume;
    }

    private void validatePath(Path path) {
        if (Files.exists(path) && !Files.isDirectory(path)) {
            throw new IllegalArgumentException(String.format("Volume parent path should be directory and it is a file. Path=%s", path));
        }
    }
}
