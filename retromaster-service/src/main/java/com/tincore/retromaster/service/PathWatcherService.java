package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.StreamTrait;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

@Slf4j
@Service
public class PathWatcherService implements StreamTrait {

    private final AtomicBoolean initialized = new AtomicBoolean();
    private WatchService watchService;
    private Map<WatchKey, WatchServiceRegister> watchServiceRegisters;

    @Autowired
    @Qualifier("pool")
    private TaskExecutor taskExecutor; // NOPMD

    public WatchKey add(Path path, Consumer<Path> consumer, WatchEvent.Kind<?>... events) throws IOException {
        initialize();

        var watchKey = path.register(watchService, events);
        watchServiceRegisters.put(watchKey, new WatchServiceRegister(path, consumer));
        return watchKey;
    }

    private void initialize() throws IOException {
        if (initialized.compareAndSet(false, true)) {
            this.watchService = FileSystems.getDefault().newWatchService();
            this.watchServiceRegisters = new ConcurrentHashMap<>();

            taskExecutor.execute(
                () -> {
                    log.info("PathWatcherService - start");
                    runFileWatcher();
                });
        }
    }

    @PreDestroy
    public void preDestroy() throws IOException {
        if (initialized.compareAndSet(true, false)) {
            this.watchServiceRegisters.keySet().forEach(WatchKey::cancel);
            this.watchService.close();
        }
    }

    // private void remove(WatchKey watchKey) {
    // watchServiceRegisters.entrySet().stream().filter(e -> e.getKey().equals(watchKey)).forEach(e -> e.getKey().cancel());
    // }
    //
    // private void remove(Path path) {
    // watchServiceRegisters.entrySet().stream().filter(e -> e.getValue().getPath().equals(path)).forEach(e -> e.getKey().cancel());
    // }

    private void runFileWatcher() {
        try {
            WatchKey key;
            while ((key = watchService.take()) != null) {
                var register = watchServiceRegisters.get(key);
                if (register == null) {
                    log.warn("PathWatcherService unrecognized key");
                    continue;
                }

                if (log.isDebugEnabled()) {
                    for (var event : key.pollEvents()) {
                        log.debug(String.format("PathWatcherService [%s] %s.", event.kind(), event.context()));
                    }
                } else {
                    key.pollEvents();
                }

                register.getConsumer().accept(register.getPath());
                key.reset();
            }
        } catch (ClosedWatchServiceException | InterruptedException e) {
            log.warn("PathWatcherService interrupted", e);
        }
    }

    @Data
    @AllArgsConstructor
    private class WatchServiceRegister {
        private Path path;
        private Consumer<Path> consumer;
    }
}
