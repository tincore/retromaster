package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentLocation;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentVolume;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface ContentRepositoryService extends JpaRepository<Content, UUID>, QuerydslPredicateExecutor<Content>, ContentRepositoryServiceExtension {

    long countAllBySignature(String signature);

    long countAllByVolume(ContentVolume volume);

    long countAllByVolumeAndDirectoryPath(ContentVolume volume, String directoryPath);

    long countAllByVolumeAndDirectoryPathStartingWith(ContentVolume volume, String directoryPathStartingWith);

    @Query(nativeQuery = true, value = "SELECT COUNT(DISTINCT c.signature) FROM Content c WHERE c.signature IN (:signatures)")
    long countDistinctSignaturesBySignatureIn(Collection<String> signatures);

    @Override
    default void delete(Content content) {
        deleteAndCreateChange(content);
    }

    @Override
    default void deleteAll(Iterable<? extends Content> contents) {
        contents.forEach(this::delete);
    }

    default void deleteAllByContentLocation(ContentVolume volume, ContentLocation contentLocation) {
        deleteAll(findDistinctByVolumeAndDirectoryPathAndFileName(volume, contentLocation.getDirectoryPath(), contentLocation.getFileName()));
    }

    default void deleteAllByVolume(ContentVolume volume) {
        findByVolume(volume).forEach(this::delete);
    }

    default void deleteAllByVolumeAndDirectoryPathAndFileName(ContentVolume volume, String directoryPath, String fileName) {
        findByVolumeAndDirectoryPathAndFileName(volume, directoryPath, fileName).forEach(this::delete);
    }

    default void deleteAllByVolumeAndFileAbsolutePath(ContentVolume volume, Path fileAbsolutePath) {
        var directoryPath = volume.toPath().relativize(fileAbsolutePath.getParent()).toString();
        var fileName = fileAbsolutePath.getFileName().toString();
        deleteAllByVolumeAndDirectoryPathAndFileName(volume, directoryPath, fileName);
    }

    @Override
    default void deleteById(UUID uuid) {
        deleteAndCreateChange(findById(uuid).orElseThrow(() -> new EmptyResultDataAccessException(String.format("No %s entity with id %s exists!", Content.class, uuid), 1)));
    }

    Page<Content> findAllByVolume(ContentVolume volume, Pageable pageable);

    List<Content> findBySignature(String signature);

    List<Content> findBySignatureAndVolume(String signature, ContentVolume contentVolume);

    List<Content> findBySignatureIn(Collection<String> signatures);

    Stream<Content> findByVolume(ContentVolume volume);

    Page<Content> findByVolumeAndDirectoryPath(ContentVolume volume, String directoryPath, Pageable pageable);

    List<Content> findByVolumeAndDirectoryPath(ContentVolume volume, String directoryPath);

    List<Content> findByVolumeAndDirectoryPathAndFileName(ContentVolume volume, String directoryPath, String fileName);

    Optional<Content> findByVolumeAndDirectoryPathAndFileNameAndFileEntry(ContentVolume volume, String directoryPath, String fileName, String fileEntry);

    Page<Content> findByVolumeAndDirectoryPathStartingWith(ContentVolume volume, String directoryPathStartingWith, Pageable pageable);

    List<Content> findByVolumeAndDirectoryPathStartingWith(ContentVolume volume, String directoryPathStartingWith);

    @Query(
           nativeQuery = true,
           value = "SELECT subt, COUNT(*) "
               + "  FROM ( "
               + "     SELECT cmi.id, (CASE WHEN COUNT(c.id) > 0 THEN 1 ELSE 0 END) as subt "
               + "       FROM content_metadata_item cmi "
               + "           LEFT OUTER JOIN content c ON (c.signature = cmi.signature) "
               + "       WHERE cmi.content_metadata_id = :contentMetadataId GROUP BY cmi.id"
               + "   )"
               + "  GROUP BY subt ORDER BY subt")
    int[][] findContentMetadataItemContentCountsGroupedByCount(@Param("contentMetadataId") UUID contentMetadataId);

    default Map<UUID, UUID> findContentMetadataItemIdContentIdPairs(Collection<ContentMetadataItem> contentMetadataItems) {
        return contentMetadataItems.stream().map(i -> Pair.of(i.getId(), findFirstBySignature(i.getSignature()).map(Content::getId).orElse(null))).filter(p -> p.getValue() != null).collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    List<Content> findDistinctByVolumeAndDirectoryPathAndFileName(ContentVolume volume, String directoryPath, String fileName);

    default List<Content> findDistinctByVolumeAndFileAbsolutePath(ContentVolume volume, Path fileAbsolutePath) {
        var directoryPath = volume.toPath().relativize(fileAbsolutePath.getParent()).toString();
        var fileName = fileAbsolutePath.getFileName().toString();
        return findDistinctByVolumeAndDirectoryPathAndFileName(volume, directoryPath, fileName);
    }

    @Query("SELECT DISTINCT c.directoryPath FROM Content c")
    Page<String> findDistinctContentDirectoryPaths(Pageable pageable);

    @Query("SELECT DISTINCT c.directoryPath FROM Content c WHERE c.volume = :volume")
    List<String> findDistinctContentDirectoryPathsByContentVolume(@Param("volume") ContentVolume volume);

    @Query("SELECT DISTINCT c.modifiedTime FROM Content c WHERE c.volume = :volume AND c.directoryPath = :directoryPath")
    List<LocalDateTime> findDistinctContentLocalDateTimeByVolumeAndDirectoryPath(@Param("volume") ContentVolume volume, @Param("directoryPath") String directoryPath);

    @Query("SELECT DISTINCT c.modifiedTime FROM Content c WHERE c.volume = :volume AND c.directoryPath = :directoryPath AND c.fileName = :fileName")
    List<LocalDateTime> findDistinctContentLocalDateTimeByVolumeAndDirectoryPathAndFileName(@Param("volume") ContentVolume volume, @Param("directoryPath") String directoryPath, @Param("fileName") String fileName);

    @Query("SELECT DISTINCT new com.tincore.retromaster.domain.ContentLocation(c.directoryPath, c.fileName, c.modifiedTime, c.fileSize) FROM Content c WHERE c.volume = :volume")
    List<ContentLocation> findDistinctContentLocationsByContentVolume(@Param("volume") ContentVolume volume);

    @Query("SELECT DISTINCT new com.tincore.retromaster.domain.ContentLocation(c.directoryPath, c.fileName, c.modifiedTime, c.fileSize) FROM Content c WHERE c.volume = :volume AND c.directoryPath = :directoryPath")
    List<ContentLocation> findDistinctContentLocationsByContentVolumeAndDirectoryPath(@Param("volume") ContentVolume volume, @Param("directoryPath") String directoryPath);

    @Query("SELECT DISTINCT new com.tincore.retromaster.domain.ContentLocation(c.directoryPath, c.fileName, c.modifiedTime, c.fileSize) FROM Content c WHERE c.volume = :volume AND c.directoryPath LIKE :directoryPath%")
    List<ContentLocation> findDistinctContentLocationsByContentVolumeAndDirectoryPathStartingWith(@Param("volume") ContentVolume volume, @Param("directoryPath") String directoryPathStartingWith);

    @Query(nativeQuery = true, value = "SELECT DISTINCT c.signature FROM Content c WHERE c.signature IN (:signatures)")
    List<String> findDistinctSignaturesBySignatureIn(Collection<String> signatures);

    Optional<Content> findFirstBySignature(String signature);

    @Override
    default Content save(Content content) {
        return saveAndCreateChange(content);
    }

    @Override
    default <S extends Content> List<S> saveAll(Iterable<S> contents) {
        return saveAllAndCreateChange(contents);
    }
}
