package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.util.StreamTrait;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public abstract class AbstractContentMetadataSetEnricher implements ContentMetadataSetEnricher, StreamTrait {

    public static String substringBeforeAll(String string, String... suffixes) {
        var lastIndex = string.length();
        for (var suffix : suffixes) {
            var p1 = string.indexOf(suffix);
            if (p1 > 0) {
                lastIndex = Math.min(p1, lastIndex);
            }
        }
        return string.substring(0, lastIndex).trim();
    }

    public ContentMetadataItem enrichContentMetadataItem(ContentMetadataItem contentMetadataItem) {
        return contentMetadataItem;
    }

    @Override
    public List<ContentMetadata> enrichContentMetadatas(List<ContentMetadata> contentMetadatas, ContentMetadataSet enrichedContentMetadataSet, String systemDefault) {
        return toStream(contentMetadatas)
            .map(
                cmd -> {
                    if (cmd.getContentMetadataSet() == null) { // Should not be needed with enricher but intellij is not using atm
                        cmd.setContentMetadataSet(enrichedContentMetadataSet);
                    }
                    try {
                        var enrichedContentMetadata = enrichContentMetadata(cmd, systemDefault);
                        if (enrichedContentMetadata == null) {
                            return null;
                        }

                        toStream(cmd.getItems())
                            .forEach(
                                i -> {
                                    if (i.getContentMetadata() == null) { // Should not be needed with enricher but intellij is not using atm
                                        i.setContentMetadata(cmd);
                                    }
                                    enrichContentMetadataItem(i);
                                });
                        return enrichedContentMetadata;
                    } catch (Exception e) { // NOPMD
                        log.error("Enrich failed for " + cmd, e);
                        return null;
                    }
                })
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }
}
