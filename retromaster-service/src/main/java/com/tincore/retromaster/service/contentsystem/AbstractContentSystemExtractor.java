package com.tincore.retromaster.service.contentsystem;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentSystem;

import java.util.Optional;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public abstract class AbstractContentSystemExtractor implements ContentSystemExtractor {

    private final ContentSystem contentSystem;

    AbstractContentSystemExtractor(ContentSystem contentSystem) {
        this.contentSystem = contentSystem;
    }

    @Override
    public Optional<ContentSystem> extract(ContentMetadata contentMetadata) {
        return isMatch(contentMetadata) ? Optional.of(contentSystem) : Optional.empty();
    }

    @Override
    public ContentSystem getContentSystem() {
        return contentSystem;
    }

    public boolean matchSystemContains(String system, Set<String> systemContains) {
        return systemContains.stream().anyMatch(sc -> containsIgnoreCase(system, sc));
    }
}
