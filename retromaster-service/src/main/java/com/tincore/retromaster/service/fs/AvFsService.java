package com.tincore.retromaster.service.fs;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.FilesystemAvFsConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.service.ProcessExecutorService;
import com.tincore.util.FileSystemTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@RequiredArgsConstructor
@Service
public class AvFsService implements FileSystemTrait {

    private final FilesystemAvFsConfiguration filesystemAvFsConfiguration;
    private final ProcessExecutorService processExecutorService;

    private final AtomicBoolean avfsMounted = new AtomicBoolean();

    public Path getPath(ContentVolume volume) {
        var absoluteRootRelativizedVolumeRootPath = Paths.get("/").relativize(volume.toPath());
        return filesystemAvFsConfiguration.getPath().resolve(absoluteRootRelativizedVolumeRootPath);
    }

    public Path getPath(Content content) {
        return getPath(content.getVolume()).resolve(content.getPathContentRelative());
    }

    public boolean isActive() {
        var path = filesystemAvFsConfiguration.getPath();
        try {
            return Files.exists(path) && !isEmpty(path);
        } catch (IOException e) {
            log.warn("isAvfsPathPresent failed", e);
            return false;
        }
    }

    public boolean mount() throws IOException, InterruptedException {
        synchronized (this) {
            if (avfsMounted.compareAndSet(false, true)) {
                var result = processExecutorService.exec("mountavfs", UUID.randomUUID());
                log.info("mount avfs. result={}", result);
                if (result != 0) {
                    log.error("mount avfs failed. result={}", result);
                    avfsMounted.set(false);
                    return false;
                }
            }
            return true;
        }
    }

    @PreDestroy
    public void preDestroy() {
        umount();
    }

    private void umount() {
        if (avfsMounted.compareAndSet(true, false)) {
            try {
                var result = processExecutorService.exec("umountavfs", UUID.randomUUID());
                log.info("umount avfs. result={}", result);
            } catch (IOException | InterruptedException e) {
                log.error("umount avfs failed", e);
            }
        }
    }
}
