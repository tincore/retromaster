package com.tincore.retromaster.service.archive;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

@Slf4j
public class ZipFileArchiveInputStream extends ArchiveInputStream {
    private final ZipFile zipFile;
    private Enumeration<ZipArchiveEntry> entries;
    private ZipArchiveEntry currentEntry;
    private InputStream currentEntryStream;

    public ZipFileArchiveInputStream(ZipFile zipFile) {
        super();
        this.zipFile = zipFile;
    }

    public boolean canReadEntryData(ArchiveEntry archiveEntry) {
        return archiveEntry.equals(this.getCurrentEntry());
    }

    public void close() throws IOException {
        this.closeCurrentEntryStream();
        try {
            this.zipFile.close();
        } catch (IOException e) {
            log.warn("closeFile", e);
        }
        super.close();
    }

    private void closeCurrentEntryStream() {
        var stream = this.getCurrentEntryStream(); // NOPMD
        IOUtils.closeQuietly(stream);
        this.currentEntryStream = null;
    }

    public ZipArchiveEntry getCurrentEntry() {
        return this.currentEntry;
    }

    public InputStream getCurrentEntryStream() {
        return this.currentEntryStream;
    }

    private Enumeration<ZipArchiveEntry> getEntries() {
        if (this.entries == null) {
            this.entries = this.zipFile.getEntriesInPhysicalOrder();
        }

        return this.entries;
    }

    public ZipArchiveEntry getNextEntry() throws IOException {
        var entries = this.getEntries();
        this.closeCurrentEntryStream();
        this.currentEntry = entries.hasMoreElements() ? entries.nextElement() : null;
        this.currentEntryStream = this.currentEntry == null ? null : this.zipFile.getInputStream(this.currentEntry);
        return this.currentEntry;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        var read = this.getCurrentEntryStream().read(b, off, len);
        if (read == -1) {
            IOUtils.closeQuietly(this.getCurrentEntryStream());
        }

        this.count(read);
        return read;
    }
}
