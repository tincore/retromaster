package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import static com.tincore.retromaster.domain.ContentSystem.*;

@Component
public class HatariContentExecutorFactory {

    private final RetromasterConfiguration.HatariExecuteConfiguration hatariExecuteConfiguration;

    @Autowired
    public HatariContentExecutorFactory(RetromasterConfiguration retromasterConfiguration) {
        this.hatariExecuteConfiguration = retromasterConfiguration.getExecute().getHatari();
    }

    @Bean
    public HatariContentExecutor hatariFalconContentExecutor() {
        return new HatariContentExecutor(hatariExecuteConfiguration, ATARI_FALCON, ATARI_ST);
    }

    @Bean
    public HatariContentExecutor hatariMegaStContentExecutor() {
        return new HatariContentExecutor(hatariExecuteConfiguration, ATARI_MEGAST, ATARI_ST);
    }

    @Bean
    public HatariContentExecutor hatariMegaSteContentExecutor() {
        return new HatariContentExecutor(hatariExecuteConfiguration, ATARI_MEGASTE, ATARI_ST);
    }

    @Bean
    public HatariContentExecutor hatariStContentExecutor() {
        return new HatariContentExecutor(hatariExecuteConfiguration, ATARI_ST);
    }

    @Bean
    public HatariContentExecutor hatariSteContentExecutor() {
        return new HatariContentExecutor(hatariExecuteConfiguration, ATARI_STE, ATARI_ST);
    }

    @Bean
    public HatariContentExecutor hatariTtContentExecutor() {
        return new HatariContentExecutor(hatariExecuteConfiguration, ATARI_TT, ATARI_ST);
    }
}
