package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.retromaster.service.ContentSystemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.tincore.retromaster.domain.ContentMediaType.UNKNOWN;
import static com.tincore.retromaster.domain.ContentMetadataSource.OPENGOOD;
import static com.tincore.retromaster.service.enrich.ContentMetadataEnricher.PATTERN_PARENTHESES;

@Slf4j
@Service
@RequiredArgsConstructor
public class OpenGoodContentMetadataSetEnricher extends AbstractContentMetadataSetEnricher {

    private final OpenGoodContentMetadataEnricher contentMetadataEnricher;
    private final ContentSystemService contentSystemService;

    private static String getContentMetadataSetIdentifier(ContentMetadataSet contentMetadataSet) {
        return StringUtils.substringAfter(contentMetadataSet.getDescription(), "OpenGood - ").trim();
    }

    @Override
    public boolean canEnrich(ContentMetadataSet contentMetadataSet) {
        return OPENGOOD.isMatch(contentMetadataSet);
    }

    @Override
    public ContentMetadata enrichContentMetadata(ContentMetadata contentMetadata, String systemDefault) {
        contentMetadata.setMediaType(UNKNOWN); // Should eventually: Default from set
        contentMetadata.setSource(OPENGOOD.name());
        contentMetadata.setSystem(systemDefault);

        var title = substringBeforeAll(contentMetadata.getName(), "[", "(");
        contentMetadataEnricher.enrich(title, contentMetadata);
        return contentMetadata;
    }

    @Override
    public ContentMetadataSet enrichContentMetadataSet(ContentMetadataSet contentMetadataSet) {
        contentMetadataSet.setCategory(getContentMetadataSource().toString());
        if (!contentMetadataSet.isStaging() || StringUtils.isBlank(contentMetadataSet.getRepositoryPath())) {
            contentMetadataSet.setRepositoryPath(getRepositoryPath(contentMetadataSet));
        }
        return contentMetadataSet;
    }

    @Override
    public String extractSystem(ContentMetadataSet contentMetadataSet) {
        var identifier = getContentMetadataSetIdentifier(contentMetadataSet);
        var system = StringUtils.substringBefore(identifier, "(");
        return contentSystemService.findSystemNormalized(system);
    }

    @Override
    public ContentMetadataSource getContentMetadataSource() {
        return OPENGOOD;
    }

    @Override
    public String getRepositoryPath(ContentMetadataSet contentMetadataSet) {
        var systemNormalized = extractSystem(contentMetadataSet);

        var identifier = getContentMetadataSetIdentifier(contentMetadataSet);
        List<String> tokensExtra = new ArrayList<>();
        var m = PATTERN_PARENTHESES.matcher(identifier);
        while (m.find()) {
            tokensExtra.add(m.group(1));
        }
        var discriminator = StringUtils.substringAfter(contentMetadataSet.getName(), ".").trim();
        if (StringUtils.isNotBlank(discriminator)) {
            tokensExtra.add(discriminator);
        }

        return systemNormalized + " - "
            + (tokensExtra.isEmpty() ? "" : String.join(" ", tokensExtra) + " ")
            + contentMetadataSet.getVersion().trim()
            + " "
            + contentMetadataSet.getCategory();
    }
}