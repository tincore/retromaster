package com.tincore.retromaster.service.archive;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Optional;

public enum ArchiverFormat {
    SEVEN_ZIP("7z"),
    TAR("tar"),
    TAR_BZIP("tar.bz2"),
    TAR_GZIP("tar.gz"),
    TAR_LZMA("tar.lzma"),
    TAR_XZ("tar.xz"),
    ZIP("zip");

    private final String fileExtension;

    ArchiverFormat(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public static Optional<ArchiverFormat> byFilename(String filename) {
        return Arrays.stream(ArchiverFormat.values()).filter(af -> StringUtils.endsWith(filename, "." + af.getFileExtension())).findFirst();
    }

    public static Optional<ArchiverFormat> byPath(Path path) {
        return byFilename(path.getFileName().toString().toLowerCase());
    }

    public String appendExtension(String name) {
        return StringUtils.appendIfMissing(name, "." + fileExtension);
    }

    public String getFileExtension() {
        return fileExtension;
    }
}
