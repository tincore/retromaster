package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ExecutionContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentExtension.img;
import static com.tincore.retromaster.domain.ContentExtension.iso;
import static com.tincore.retromaster.service.execute.executor.DosFilesUtil.toDosName;

@Service
public class DosboxBootContentExecutor extends AbstractContentExecutor {
    private static final Set<String> CONTENT_EXTENSIONS = Stream.of(iso, img).map(Enum::name).collect(Collectors.toSet());

    private final RetromasterConfiguration.DosboxExecuteConfiguration dosboxExecuteConfiguration;

    public DosboxBootContentExecutor(RetromasterConfiguration retromasterConfiguration) {
        super("DOSBOX_Boot", "Dosbox Boot", false, ContentSystem.IBM_PC);
        this.dosboxExecuteConfiguration = retromasterConfiguration.getExecute().getDosbox();
    }

    public ExecutionContext createExecutionContext(
        ContentMetadataItem contentMetadataItem,
        UUID executionId,
        List<ContentMetadataItem> associatedItems,
        List<Pair<ContentMetadataItem, Content>> contentsMain,
        List<Pair<ContentMetadataItem, Content>> contentsAssociated) {
        return new ExecutionContext(executionId, contentMetadataItem, associatedItems, contentsMain, contentsAssociated) {
            private final Set<String> fileNames = new HashSet<>(); // To convert files to 8:3

            @Override
            public String formatFileName(String name) {
                var dosName = toDosName(name, fileNames);
                fileNames.add(dosName);
                return dosName;
            }
        };
    }

    @Override
    public void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException {
        var contentArgument = executionContext.getMainItemPath();
        var args = "-c boot " + StringUtils.wrapIfMissing(contentArgument.toString(), "\"");
        exec(String.format("%s %s", getExecutablePath(), args), executionContext.getExecutionId());
    }

    @Override
    public Set<String> getContentExtensions() {
        return CONTENT_EXTENSIONS;
    }

    public Stream<String> getExecutablePaths() {
        return dosboxExecuteConfiguration.getExecutablePaths().stream();
    }

    @Override
    public boolean isReadOnlyExecutionCapable() {
        return false;
    }
}
