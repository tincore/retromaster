package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentExtension;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ExecutionContext;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FsUaeContentExecutor extends AbstractContentExecutor {

    private final RetromasterConfiguration.FsUaeExecuteConfiguration fsUaeExecuteConfiguration;
    private final ContentSystem mainContentSystem;

    public FsUaeContentExecutor(RetromasterConfiguration.FsUaeExecuteConfiguration fsUaeExecuteConfiguration, ContentSystem... contentSystems) {
        super("FS-UAE_" + contentSystems[0].name(), "FsUae - " + contentSystems[0].getLabel(), true, contentSystems);
        this.fsUaeExecuteConfiguration = fsUaeExecuteConfiguration;
        this.mainContentSystem = contentSystems[0];
    }

    // fs-uae --amiga-model=cdtv --cdrom-drive-0='Case of the Cautious Condor, The (1990)(Tiger Media)[!].cue'

    @Override
    public void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException {
        List<String> params = new ArrayList<>();
        var contentArgument = executionContext.getMainItemPath();
        params.add(getContentArgument(contentArgument));

        Optional.ofNullable(getModelParam()).ifPresent(p -> params.add("amiga-model=" + p));

        params.add("floppy_drive_count=" + fsUaeExecuteConfiguration.getFloppyCount()); // 0 - 4
        params.add("floppy_drive_speed=" + fsUaeExecuteConfiguration.getFloppySpeed()); // 0 100 800

        if (fsUaeExecuteConfiguration.isFullscreen()) {
            params.add("fullscreen=1");
        }
        if (fsUaeExecuteConfiguration.getFsaa() > 0) {
            params.add("fsaa=" + fsUaeExecuteConfiguration.getFsaa()); // 0 2 4 8
        }

        var extraItemsPaths = executionContext.getOtherItemPaths().stream().sorted().toList();
        if (!extraItemsPaths.isEmpty()) {
            params.add("floppy-image-0=" + StringUtils.wrapIfMissing(contentArgument.toString(), "\""));
            var index = 1;
            for (var path : extraItemsPaths) {
                params.add("floppy-image-" + index + "=" + StringUtils.wrapIfMissing(path.toString(), "\""));
                index++;
            }
        }

        var cmd = String.format("%s %s", getExecutablePath(), params.stream().map(p -> "--" + p).collect(Collectors.joining(" ")));
        exec(cmd, executionContext.getExecutionId());
    }

    private String getContentArgument(Path contentArgument) {
        if (ContentExtension.isDisc(contentArgument.getFileName().toString())) {
            return "cdrom-drive-0=" + StringUtils.wrapIfMissing(contentArgument.toString(), "\"");
        }
        return "floppy-drive-0=" + StringUtils.wrapIfMissing(contentArgument.toString(), "\"");
    }

    @Override
    public Set<String> getContentExtensions() {
        return Collections.emptySet();
    }

    public Stream<String> getExecutablePaths() {
        return fsUaeExecuteConfiguration.getExecutablePaths().stream();
    }

    private String getModelParam() {
        if (ContentSystem.COMMODORE_CDTV.equals(mainContentSystem)) {
            return "cdtv";
        }
        if (ContentSystem.COMMODORE_CD32.equals(mainContentSystem)) {
            return "cd32";
        }
        if (ContentSystem.COMMODORE_AMIGA_1200.equals(mainContentSystem)) {
            return "a1200";
        }

        return null;
    }

}
