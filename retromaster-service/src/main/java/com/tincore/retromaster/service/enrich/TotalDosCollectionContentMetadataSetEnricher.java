package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.tincore.retromaster.domain.ContentMediaType.UNKNOWN;
import static com.tincore.retromaster.domain.ContentMetadataSource.TDC;
import static com.tincore.retromaster.domain.ContentSystem.IBM_PC;

@Slf4j
@Service
@RequiredArgsConstructor
public class TotalDosCollectionContentMetadataSetEnricher extends AbstractContentMetadataSetEnricher {

    private final TotalDosCollectionContentMetadataEnricher contentMetadataEnricher;

    @Override
    public boolean canEnrich(ContentMetadataSet contentMetadataSet) {
        return TDC.isMatch(contentMetadataSet);
    }

    @Override
    public ContentMetadata enrichContentMetadata(ContentMetadata contentMetadata, String systemDefault) {
        contentMetadata.setMediaType(UNKNOWN); // Should eventually: Default from set
        contentMetadata.setSource(TDC.name());
        contentMetadata.setSystem(systemDefault);

        var lastIndex = contentMetadata.getName().length();
        var p1 = contentMetadata.getName().indexOf("[");
        if (p1 > 0) {
            lastIndex = Math.min(p1, lastIndex);
        }
        var p2 = contentMetadata.getName().indexOf("(");
        if (p2 > 0) {
            lastIndex = Math.min(p2, lastIndex);
        }

        var title = contentMetadata.getName().substring(0, lastIndex).trim();

        contentMetadataEnricher.enrich(title, contentMetadata);

        return contentMetadata;
    }

    @Override
    public ContentMetadataSet enrichContentMetadataSet(ContentMetadataSet contentMetadataSet) {
        contentMetadataSet.setCategory(getContentMetadataSource().toString());
        if (!contentMetadataSet.isStaging() || StringUtils.isBlank(contentMetadataSet.getRepositoryPath())) {
            contentMetadataSet.setRepositoryPath(getRepositoryPath(contentMetadataSet));
        }
        return contentMetadataSet;
    }

    @Override
    public String extractSystem(ContentMetadataSet contentMetadataSet) {
        return IBM_PC.name();
    }

    @Override
    public ContentMetadataSource getContentMetadataSource() {
        return TDC;
    }

    @Override
    public String getRepositoryPath(ContentMetadataSet contentMetadataSet) {
        return extractSystem(contentMetadataSet) + " - " + contentMetadataSet.getVersion().trim() + " " + contentMetadataSet.getCategory();
    }
}
