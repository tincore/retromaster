package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.ApplicationLuceneAnalysisConfigurer;
import com.tincore.retromaster.domain.ContentMediaType;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.search.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.search.engine.backend.common.DocumentReference;
import org.hibernate.search.engine.search.aggregation.AggregationKey;
import org.hibernate.search.engine.search.common.BooleanOperator;
import org.hibernate.search.engine.search.query.SearchResult;
import org.hibernate.search.engine.search.query.dsl.SearchQueryOptionsStep;
import org.hibernate.search.engine.search.sort.dsl.SearchSortFactory;
import org.hibernate.search.mapper.orm.search.loading.dsl.SearchLoadingOptionsStep;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentMetadata.*;

@Slf4j
@Transactional(readOnly = true)
@Repository
public class ContentMetadataSearchService extends AbstractSearchService<ContentMetadata> {

    public static final int FACET_VALUES_COUNT_MED = 99;
    public static final int FACET_VALUES_COUNT_MAX = 999;
    public static final int VALUES_MAX = 9999;

    public static final String FACET_YEAR = "year";
    public static final String FACET_PUBLISHER = "publisher";
    public static final String FACET_COMPLETE = "complete";
    public static final String FACET_SOURCE = "source";
    public static final String FACET_SYSTEM = "system";
    public static final String FACET_PREFIX = "prefix";

    private static final int PR3 = 3;
    private static final int PR4 = 4;
    private static final List<QueryFacetDefinition> FACET_DEFINITIONS = Stream.of(
            new QueryFacetDefinition(FACET_SYSTEM, FIELD_SYSTEM_RAW, FACET_VALUES_COUNT_MAX, 0),
            new QueryFacetDefinition(FACET_COMPLETE, FIELD_COMPLETE, FACET_VALUES_COUNT_MAX, 1),
            new QueryFacetDefinition(FACET_PUBLISHER, FIELD_PUBLISHER_RAW, FACET_VALUES_COUNT_MED, 2),
            new QueryFacetDefinition(FACET_YEAR, FIELD_YEAR, FACET_VALUES_COUNT_MED, PR3),
            new QueryFacetDefinition(FACET_SOURCE, FIELD_SOURCE, FACET_VALUES_COUNT_MAX, PR4))
        .toList();
    protected static final List<QueryFacetFilter> BY_TEXT_DEFAULT_QUERY_FACET_FILTERS = FACET_DEFINITIONS.stream().map(fd -> new QueryFacetFilter(Collections.emptySet(), false, fd.getField(), fd.getMax())).collect(Collectors.toList());
    private static final Map<String, QueryFacetDefinition> FACET_DEFINITIONS_BY_NAME = FACET_DEFINITIONS.stream().collect(Collectors.toMap(QueryFacetDefinition::getField, Function.identity()));

    public ContentMetadataSearchService() {
        super(ContentMetadata.class);
    }

    private AggregationKey<Map<String, Long>> addSearchQueryAggregation(QueryFacetFilter queryFacetFilter, SearchQueryOptionsStep<?, ContentMetadata, SearchLoadingOptionsStep, ?, ?> baseQuery) {
        var facetDefinition = FACET_DEFINITIONS_BY_NAME.get(queryFacetFilter.getFacetName());
        AggregationKey<Map<String, Long>> aggregationKey = AggregationKey.of(facetDefinition.getField());
        baseQuery.aggregation(
            aggregationKey,
            f -> f.terms()
                .field(facetDefinition.getSearchField(), String.class)
                .orderByTermAscending()
                .minDocumentCount(queryFacetFilter.isIncludeZero() ? 0 : 1)
                .maxTermCount(queryFacetFilter.getValuesMax() > 0 ? queryFacetFilter.getValuesMax() : facetDefinition.getMax()));
        return aggregationKey;
    }

    public QueryResult<ContentMetadata> createQueryResult(SearchResult<ContentMetadata> searchResult, Pageable pageable) {
        return new QueryResult<>(searchResult.hits(), pageable, searchResult.total().hitCount());
    }

    @CacheEvict(
        cacheNames = {"contentMetadataSearch", "searchByMetadataSetIdAndPrefixAndCompleteNotEmpty", "searchBySystemAndPrefixAndCompleteNotEmpty"},
        allEntries = true)
    public void reset() {
        log.info("contentMetadataSearch cache reset");
    }

    public QueryResult<ContentMetadata> searchByMetadataSetIdAndComplete(UUID metadataSetId, String complete, Pageable pageable) {
        var result = getSearchSession()
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_CONTENT_METADATA_SET_ID).matching(metadataSetId));
                        b.must(f.match().field(FIELD_COMPLETE).matching(complete));
                    }))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByMax(), TimeUnit.MILLISECONDS)
            .fetch((int) pageable.getOffset(), pageable.getPageSize());

        return createQueryResult(result, pageable);
    }

    @Cacheable(cacheNames = "searchByMetadataSetIdAndPrefixAndCompleteNotEmpty")
    public QueryResult<ContentMetadata> searchByMetadataSetIdAndPrefixAndCompleteNotEmpty(UUID metadataSetId, String prefix, Pageable pageable) {
        var result = getSearchSession()
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_CONTENT_METADATA_SET_ID).matching(metadataSetId));
                        b.must(f.match().field(FIELD_PREFIX).matching(prefix));
                        b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY));
                    }))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByMax(), TimeUnit.MILLISECONDS)
            .fetch((int) pageable.getOffset(), pageable.getPageSize());

        return createQueryResult(result, pageable);
    }

    public QueryResult<ContentMetadata> searchBySystemAndPrefix(String system, String prefix, Pageable pageable) {
        var result = getSearchSession()
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_SYSTEM).matching(system));
                        b.must(f.match().field(FIELD_PREFIX).matching(prefix));
                    }))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByMax(), TimeUnit.MILLISECONDS)
            .fetch((int) pageable.getOffset(), pageable.getPageSize());

        return createQueryResult(result, pageable);
    }

    @Cacheable(cacheNames = "searchBySystemAndPrefixAndCompleteNotEmpty")
    public QueryResult<ContentMetadata> searchBySystemAndPrefixAndCompleteNotEmpty(String system, String prefix, Pageable pageable) {
        var result = getSearchSession()
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_SYSTEM).matching(system));
                        b.must(f.match().field(FIELD_PREFIX).matching(prefix));
                        b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY));
                    }))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByMax(), TimeUnit.MILLISECONDS)
            .fetch((int) pageable.getOffset(), pageable.getPageSize());

        return createQueryResult(result, pageable);
    }

    @Cacheable(cacheNames = "searchBySystemAndTitleAndMediaImageAndCompleteNotEmpty")
    public QueryResult<ContentMetadata> searchBySystemAndTitleAndMediaImageAndCompleteNotEmpty(String system, String title, Pageable pageable) {
        var result = getSearchSession()
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_SYSTEM).matching(system));
                        b.must(f.match().field(FIELD_TITLE_RAW).matching(title));
                        b.must(f.bool(b2 -> {
                            b2.should(f.match().field(FIELD_MEDIA_TYPE).matching(ContentMediaType.PIC_TITLE));
                            b2.should(f.match().field(FIELD_MEDIA_TYPE).matching(ContentMediaType.PIC_BOXART));
                            b2.should(f.match().field(FIELD_MEDIA_TYPE).matching(ContentMediaType.PIC_SNAPSHOT));
                        }));
                        b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY));
                    }))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByMax(), TimeUnit.MILLISECONDS)
            .fetch((int) pageable.getOffset(), pageable.getPageSize());

        return createQueryResult(result, pageable);
    }

    @Cacheable(cacheNames = "searchBySystemAndTitleAndMediaTypeAndCompleteNotEmpty")
    public QueryResult<ContentMetadata> searchBySystemAndTitleAndMediaTypeAndCompleteNotEmpty(String system, String title, ContentMediaType contentMediaType, Pageable pageable) {
        var result = getSearchSession()
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_SYSTEM).matching(system));
                        b.must(f.match().field(FIELD_TITLE_RAW).matching(title));
                        b.must(f.match().field(FIELD_MEDIA_TYPE).matching(contentMediaType));
                        b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY));
                    }))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByMax(), TimeUnit.MILLISECONDS)
            .fetch((int) pageable.getOffset(), pageable.getPageSize());

        return createQueryResult(result, pageable);
    }

    /**
     * https://hibernate.atlassian.net/browse/HSEARCH-3666 Lucene terms aggregations (discrete facets) may return wrong results for any sort other than the default one This means that I may need to sort anyway!
     *
     * @param text                     Text to search for
     * @param queryFacetFilters
     * @param excludeContentMediaTypes
     * @param pageable
     * @return
     */
    @Transactional(readOnly = true)
    @Cacheable(cacheNames = "contentMetadataSearch")
    public QueryResult<ContentMetadata> searchByTextPaged(String text, List<QueryFacetFilter> queryFacetFilters, List<ContentMediaType> excludeContentMediaTypes, Pageable pageable) {
        var searchSession = getSearchSession();

        var facetFilters = queryFacetFilters.isEmpty() ? BY_TEXT_DEFAULT_QUERY_FACET_FILTERS : queryFacetFilters;
        var queryFacetFilterValuesByFacetName = queryFacetFilters.stream().collect(Collectors.toMap(QueryFacetFilter::getFacetName, QueryFacetFilter::getValues));

        var textFilterPresent = StringUtils.isNotBlank(text);

        var facetFilteredQuery = searchSession
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        excludeContentMediaTypes.forEach(m -> b.mustNot(f.match().field(FIELD_MEDIA_TYPE).matching(m)));

                        b.must(textFilterPresent ? f.simpleQueryString().field(FIELD_TITLE).matching(text).analyzer(ApplicationLuceneAnalysisConfigurer.ANALYZER_STANDARD).defaultOperator(BooleanOperator.AND) : f.matchAll());
                        facetFilters.stream()
                            .filter(ff -> !ff.getValues().isEmpty())
                            .forEach(
                                ff -> {
                                    var searchField = FACET_DEFINITIONS_BY_NAME.get(ff.getFacetName()).getSearchField();
                                    b.filter(f.bool(b2 -> ff.getValues().forEach(v -> b2.should(f.match().field(searchField).matching(v)))));
                                });
                    }))
            .sort(f -> textFilterPresent ? f.score() : f.field(FIELD_TITLE_RAW).asc());

        var nonFilteringQueryAggregationKeys = facetFilters.stream().filter(ff -> ff.getValues().isEmpty()).map(ff -> addSearchQueryAggregation(ff, facetFilteredQuery)).toList();

        var filteredResult = facetFilteredQuery.failAfter(getTimeoutMillisByExpectedMaxElementCount(pageable.getPageSize()), TimeUnit.MILLISECONDS).fetch((int) pageable.getOffset(), pageable.getPageSize());

        var nonFilteringQueryResultFacets = nonFilteringQueryAggregationKeys.stream().map(ak -> toQueryResultFacet(ak, filteredResult, queryFacetFilterValuesByFacetName.get(ak.name())));

        var filteringQueryResultFacets = facetFilters.stream()
            .filter(ff -> !ff.getValues().isEmpty())
            .map(
                ff -> {
                    var aggregateQuery = searchSession
                        .search(getType())
                        .where(
                            f -> f.bool(
                                b -> {
                                    excludeContentMediaTypes.forEach(m -> b.mustNot(f.match().field(FIELD_MEDIA_TYPE).matching(m)));
                                    b.must(textFilterPresent ? f.simpleQueryString().field(FIELD_TITLE).matching(text.toLowerCase()).defaultOperator(BooleanOperator.AND) : f.matchAll());
                                    facetFilters.stream()
                                        .filter(sff -> !sff.getValues().isEmpty() && !sff.equals(ff))
                                        .forEach(
                                            sff -> {
                                                var searchField = FACET_DEFINITIONS_BY_NAME.get(sff.getFacetName()).getSearchField();
                                                b.filter(f.bool(b2 -> sff.getValues().forEach(v -> b2.should(f.match().field(searchField).matching(v)))));
                                            });
                                }))
                        .sort(f -> textFilterPresent ? f.score() : f.field(FIELD_TITLE_RAW).asc());
                    var ak = addSearchQueryAggregation(ff, aggregateQuery);
                    var aggregateQueryResult = aggregateQuery.failAfter(getTimeoutMillisByExpectedMaxElementCount(pageable.getPageSize()), TimeUnit.MILLISECONDS).fetch(0, 0);
                    return toQueryResultFacet(ak, aggregateQueryResult, queryFacetFilterValuesByFacetName.get(ak.name()));
                });
        var queryResultFacets
            = Stream.concat(filteringQueryResultFacets, nonFilteringQueryResultFacets).sorted(Comparator.comparingInt(a -> FACET_DEFINITIONS_BY_NAME.get(a.getFacetName()).getPrecedence())).collect(Collectors.toList());

        return new QueryResult<>(filteredResult.hits(), pageable, filteredResult.total().hitCount(), queryResultFacets);
    }

    public List<String> searchContentMetadataPrefixByContentMetadataSetIdAndCompleteNotEmpty(UUID contentMetadataSetId) {
        AggregationKey<Map<String, Long>> countsByMetadataSetIdKey = AggregationKey.of("countsByMetadataSetId");
        var result = getSearchSession()
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_CONTENT_METADATA_SET_ID).matching(contentMetadataSetId));
                        b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY));
                    }))
            .aggregation(
                countsByMetadataSetIdKey,
                f -> f.terms()
                    .field(FIELD_PREFIX, String.class)
                    .orderByTermAscending()
                    .minDocumentCount(1) // Not empty
                    .maxTermCount(VALUES_MAX))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByMax(), TimeUnit.MILLISECONDS)
            .fetch(0, 1);
        var counts = result.aggregation(countsByMetadataSetIdKey); // map has order...
        return new ArrayList<>(counts.keySet());
    }

    public List<String> searchContentMetadataPrefixBySystemAndContentNotEmpty(String system) {
        AggregationKey<Map<String, Long>> countsByPrefixKey = AggregationKey.of("countsByPrefix");
        var result = getSearchSession()
            .search(getType())
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_SYSTEM_RAW).matching(system));
                        b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY));
                    }))
            .aggregation(
                countsByPrefixKey,
                f -> f.terms()
                    .field(FIELD_PREFIX, String.class)
                    .orderByTermAscending()
                    .minDocumentCount(1) // Not empty
                    .maxTermCount(VALUES_MAX))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByExpectedMaxElementCount(Integer.MAX_VALUE), TimeUnit.MILLISECONDS)
            .fetch(0, 1);
        var counts = result.aggregation(countsByPrefixKey);
        return new ArrayList<>(counts.keySet());
    }

    public List<String> searchContentMetadataSystemsByContentNotEmpty() {
        AggregationKey<Map<String, Long>> countsBySystemKey = AggregationKey.of("countsBySystem");
        var result = getSearchSession()
            .search(getType())
            .where(f -> f.bool(b -> b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY))))
            .aggregation(
                countsBySystemKey,
                f -> f.terms()
                    .field(FIELD_SYSTEM_RAW, String.class)
                    .minDocumentCount(1) // Not empty??
                    // .minDocumentCount(0)
                    .maxTermCount(VALUES_MAX))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByExpectedMaxElementCount(Integer.MAX_VALUE), TimeUnit.MILLISECONDS)
            .fetch(0, 1);
        var counts = result.aggregation(countsBySystemKey);
        return new ArrayList<>(counts.keySet());
    }

    public Stream<UUID> searchIdsByMetadataSetIdAndPrefixAndCompleteNotEmpty(UUID metadataSetId, String prefix) {
        var result = getSearchSession()
            .search(getType())
            .select(f -> f.composite(DocumentReference::id, f.documentReference()))
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_CONTENT_METADATA_SET_ID).matching(metadataSetId));
                        b.must(f.match().field(FIELD_PREFIX).matching(prefix));
                        b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY));
                    }))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByExpectedMaxElementCount(Integer.MAX_VALUE), TimeUnit.MILLISECONDS)
            .fetch(0, Integer.MAX_VALUE);

        return result.hits().stream().map(UUID::fromString);
    }

    public Stream<UUID> searchIdsBySystemAndPrefixAndCompleteNotEmpty(String system, String prefix) {
        var result = getSearchSession()
            .search(getType())
            .select(f -> f.composite(DocumentReference::id, f.documentReference()))
            .where(
                f -> f.bool(
                    b -> {
                        b.must(f.match().field(FIELD_SYSTEM).matching(system));
                        b.must(f.match().field(FIELD_PREFIX).matching(prefix));
                        b.mustNot(f.match().field(FIELD_COMPLETE).matching(COMPLETE_EMPTY));
                    }))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByExpectedMaxElementCount(Integer.MAX_VALUE), TimeUnit.MILLISECONDS)
            .fetch(0, Integer.MAX_VALUE);

        return result.hits().stream().map(UUID::fromString);
    }

    public QueryResultFacet toQueryResultFacet(AggregationKey<Map<String, Long>> ak, SearchResult<ContentMetadata> searchResult, Set<String> selectedValues) {
        var aggregationCounts = searchResult.aggregation(ak);
        var queryResultFacetValues = aggregationCounts.entrySet()
            .stream()
            .map(e -> toQueryResultFacetValue(e, selectedValues))
            .toList();
        return new QueryResultFacet(ak.name(), queryResultFacetValues);
    }

    private QueryResultFacetValue toQueryResultFacetValue(Map.Entry<String, Long> agregationCount, Set<String> selectedValues) {
        return new QueryResultFacetValue(agregationCount.getKey(), agregationCount.getValue().intValue(), selectedValues != null && selectedValues.contains(agregationCount.getKey()));
    }
}
