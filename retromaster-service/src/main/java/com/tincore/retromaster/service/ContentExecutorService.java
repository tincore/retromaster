package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ContentExecutor;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ContentExecutorService {

    private final List<ContentExecutor> contentExecutors;

    public ContentExecutor findById(String id) {
        return contentExecutors.stream().filter(s -> s.getId().equals(id)).findFirst().orElseThrow(() -> new RuntimeException("Could not find executor with id= " + id));
    }

    public List<ContentExecutor> findContentExecutorsByContentMetadataItem(ContentMetadataItem contentMetadataItem, List<ContentSystem> contentSystems) {
        return contentExecutors.stream().filter(s -> s.canExecute(contentMetadataItem.getName(), contentSystems)).toList();
    }

    public Stream<ContentExecutor> findContentExecutorsByContentSystem(List<ContentSystem> contentSystems) {
        return contentExecutors.stream().filter(s -> s.canExecute(null, contentSystems));
    }

    @Cacheable("contentExecutors")
    public List<ContentExecutor> findContentExecutorsByFilter(Boolean enabledEquals, Boolean foundEquals) {
        return contentExecutors.stream().filter(s -> (enabledEquals == null || s.isEnabled() == enabledEquals) && (foundEquals == null || s.isFound() == foundEquals)).collect(Collectors.toList());
    }
}
