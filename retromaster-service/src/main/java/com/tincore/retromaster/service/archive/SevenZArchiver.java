package com.tincore.retromaster.service.archive;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.service.WrappedException;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryProducer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.channels.SeekableByteChannel;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class SevenZArchiver implements Archiver {

    private final RetromasterConfiguration.ArchiverConfiguration archiverConfiguration;

    @Override
    public void archive(SeekableByteChannel targetChannel, List<? extends ArchiveEntryProducer> archiveEntries) throws IOException {
        try (var out = new SevenZOutputFileArchiveOutputStream(new SevenZOutputFile(targetChannel))) {
            archiveEntries.forEach(
                ae -> {
                    try {
                        if (ae.isRegularFile()) {
                            var entry = new SevenZArchiveEntry();
                            entry.setDirectory(false);
                            entry.setName(ae.getName());
                            entry.setLastModifiedDate(new Date(ae.getModifiedTime().toInstant(ZoneOffset.UTC).toEpochMilli()));
                            out.putArchiveEntry(entry);

                            try (var in = ae.getInputStream()) {
                                IOUtils.copyLarge(in, out, new byte[archiverConfiguration.getArchiveBufferSize()]);
                            }
                            out.closeArchiveEntry();
                        } else if (ae.isDirectory()) {
                            var name = ae.getName();
                            if (StringUtils.isNotBlank(name)) {
                                var entry = new SevenZArchiveEntry();
                                entry.setName(name);
                                entry.setDirectory(true);
                                entry.setLastModifiedDate(new Date(ae.getModifiedTime().toInstant(ZoneOffset.UTC).toEpochMilli()));

                                out.putArchiveEntry(entry);
                            }
                        } else {
                            log.warn("Not supported {}", ae);
                        }
                    } catch (IOException e) {
                        throw new WrappedException(e);
                    } finally {
                        try {
                            ae.close();
                        } catch (IOException e) {
                            log.error("closing " + ae, e);
                        }
                    }
                });
        }
    }

    @Override
    public SevenZFileArchiveInputStream createArchiveInputStream(SeekableByteChannel channel) throws IOException {
        return new SevenZFileArchiveInputStream(new SevenZFile(channel));
    }

    @Override
    public boolean isHandlerOf(ArchiverFormat archiverFormat) {
        return ArchiverFormat.SEVEN_ZIP.equals(archiverFormat);
    }
}
