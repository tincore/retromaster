package com.tincore.retromaster.service.execute.prepare;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.service.execute.ContentExecutor;
import com.tincore.retromaster.service.execute.ExecutionContext;
import com.tincore.retromaster.service.fs.AvFsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.util.Comparator;

@Slf4j
@Service
@RequiredArgsConstructor
public class AvFsExecutionPrepareStrategy implements ExecutionPrepareStrategy {

    private static final int PRECEDENCE = 4;
    private final AvFsService avFsService;

    @Override
    public void doBeforeExecution(ExecutionContext executionContext, boolean readOnly) {
        executionContext.setMainItemPath(avFsService.getPath(executionContext.getMainContent()));
        executionContext.getContentsAssociated().stream().sorted(Comparator.comparing(p1 -> p1.getLeft().getName())).map(Pair::getRight).map(avFsService::getPath).forEach(p -> executionContext.getOtherItemPaths().add(p));
    }

    @Override
    public int getOrder() {
        return PRECEDENCE;
    }

    @Override
    public boolean isApplicable(ExecutionContext executionContext, ContentExecutor contentExecutor) {
        return executionContext.getContentSourceLocationsCount() == 1 && avFsService.isActive() && contentExecutor.isReadOnlyExecutionCapable();
    }
}
