package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentSystem;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
class ViceContentExecutorFactory {

    @Bean
    public ViceContentExecutor viceX128ContentExecutor() {
        return new ViceContentExecutor("x128", ContentSystem.COMMODORE_C128);
    }

    @Bean
    public ViceContentExecutor viceX64ContentExecutor() {
        return new ViceContentExecutor("x64", ContentSystem.COMMODORE_C64);
    }

    @Bean
    public ViceContentExecutor viceX64DtvContentExecutor() {
        return new ViceContentExecutor("x64dtv", ContentSystem.COMMODORE_C64DTV);
    }

    @Bean
    public ViceContentExecutor viceX64ScContentExecutor() {
        return new ViceContentExecutor("x64sc", ContentSystem.COMMODORE_C64);
    }

    @Bean
    public ViceContentExecutor viceXCbm25x0ContentExecutor() {
        return new ViceContentExecutor("xcbm25x0", ContentSystem.COMMODORE_CBM_II);
    }

    @Bean
    public ViceContentExecutor viceXCbm2ContentExecutor() {
        return new ViceContentExecutor("xcbm2", ContentSystem.COMMODORE_CBM_II);
    }

    @Bean
    public ViceContentExecutor viceXPetContentExecutor() {
        return new ViceContentExecutor("xpet", ContentSystem.COMMODORE_PET);
    }

    @Bean
    public ViceContentExecutor viceXPlus4ContentExecutor() {
        return new ViceContentExecutor("xplus4", ContentSystem.COMMODORE_C16_C116_PLUS_4);
    }
}
