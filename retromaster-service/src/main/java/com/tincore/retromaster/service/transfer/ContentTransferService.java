package com.tincore.retromaster.service.transfer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.TaskExecutorConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadataSourceStorageStrategy;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.transfer.ContentTransfer;
import com.tincore.retromaster.domain.transfer.OriginContentTransfer;
import com.tincore.retromaster.domain.transfer.TargetContentTransfer;
import com.tincore.retromaster.service.*;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryProducer;
import com.tincore.retromaster.service.archive.producer.PathArchiveEntryProducer;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.transfer.ContentTransfer.*;
import static com.tincore.util.lang.ThrowingConsumer.uConsumer;
import static com.tincore.util.lang.ThrowingFunction.uFunction;
import static java.util.stream.Collectors.*;


@Slf4j
@Service
@RequiredArgsConstructor
public class ContentTransferService implements StreamTrait, FileSystemTrait {

    private final TargetContentTransferService targetContentTransferService;
    private final ContentVolumeService contentVolumeService;
    private final ContentVolumeRepositoryService contentVolumeRepositoryService;
    private final ContentEnrichService contentEnrichService;
    private final ContentRepositoryService contentRepositoryService;
    private final ContentErrorService contentErrorService;
    private final ArchiveService archiveService;


    private final TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;
    private final TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider;

    private boolean isOriginContentsAllCompressedAndIdentifiedAndHaveTargetExpectedNames(List<OriginContentTransfer> originContentTransfers) {
        return originContentTransfers.stream().allMatch(ct -> ct.isIdentified() && ct.getContent().isCompressedEntry() && ct.getContent().getFileEntry().equals(ct.getContentMetadataItem().getName()));
    }

    private boolean isOriginContentsAllFromSingleFileWithNameEqualsTargetFileName(List<OriginContentTransfer> originContentTransfers, Path targetFileAbsolutePath) {
        var originContentTransferFilePaths = originContentTransfers.stream().map(t -> t.getContent().getPathFileAbsolute()).distinct().toList();
        var singleSource = originContentTransferFilePaths.size() != 1;
        return !singleSource && targetFileAbsolutePath.getFileName().equals(originContentTransferFilePaths.get(0).getFileName());
    }

    private void merge(ContentVolume targetVolume, Path targetFileAbsolutePath, List<OriginContentTransfer> knownUniqueOrigins, List<TargetContentTransfer> knownUniqueTargets) throws IOException { // NOPMD Complex
        var destinationRootPath = targetVolume.toPath();

        var lastModifiedTime = getLastModifiedTime(targetFileAbsolutePath);
        var fileSize = Files.size(targetFileAbsolutePath);
        var directoryPath = destinationRootPath.relativize(targetFileAbsolutePath.getParent()).toString();
        var fileName = targetFileAbsolutePath.getFileName().toString();

        var distinctByVolumeAndFileAbsolutePath = contentRepositoryService.findDistinctByVolumeAndFileAbsolutePath(targetVolume, targetFileAbsolutePath);
        var contentsCurrentById = distinctByVolumeAndFileAbsolutePath.stream().collect(toMap(Content::getId, c -> c));
        var contentsCurrentByPathContentRelative = distinctByVolumeAndFileAbsolutePath.stream().collect(toMap(Content::getPathContentRelative, c -> c));
        var updatedContents = Stream.concat(
                knownUniqueOrigins.stream()
                    .map(
                        oct -> {
                            var originContentCopy = oct.getContent().toBuilder().build();
                            Content stored = null;
                            if (originContentCopy.getId() != null) {
                                stored = contentsCurrentById.get(originContentCopy.getId());
                                if (stored != null) {
                                    // same Id so cleanUp should NOT delete content
                                    log.error("STRANGE CASE1. FIND OUT HOW contentToSave={}, contentStored={}", originContentCopy, stored);
                                }
                            }
                            if (stored == null) {
                                var pathContentRelative = Content.toPathContentRelative(directoryPath, fileName, oct.getTargetFileEntry());
                                stored = contentsCurrentByPathContentRelative.get(pathContentRelative);
                                if (stored != null) {
                                    // Strange case
                                    log.warn("Transfer corner case. Possible manipulation of target file out of application. Transfer content to saved content that does not exist in filesystem. content={}", stored);
                                }
                            }

                            Content content;
                            if (stored == null) {
                                content = originContentCopy;
                            } else {
                                contentsCurrentById.remove(stored.getId());
                                contentsCurrentByPathContentRelative.remove(stored.getPathContentRelative());
                                content = stored;

                                // Reapply signature in case of modification
                                content.setSignature(originContentCopy.getCrc(), originContentCopy.getMd5(), originContentCopy.getSha1(), originContentCopy.getSize());
                            }

                            // Apply all attributes. There are renaming cases
                            content.setVolume(targetVolume);
                            content.setDirectoryPath(directoryPath);
                            content.setFileName(fileName);
                            content.setFileEntry(oct.getTargetFileEntry());

                            if (originContentCopy.getId() != null && !originContentCopy.getId().equals(content.getId())) {
                                // Cleanup should remove this content!
                                oct.addDeleteContentFlag();
                            }

                            oct.addImportedFlag();
                            return content;
                        }),
                knownUniqueTargets.stream()
                    .map(
                        tct -> {
                            var targetContent = tct.getContent();

                            Content stored = null;
                            if (targetContent.getId() != null) {
                                stored = contentsCurrentById.get(targetContent.getId());
                            }
                            if (stored == null) {
                                stored = contentsCurrentByPathContentRelative.get(targetContent.getPathContentRelative());
                            }

                            Content content;
                            if (stored == null) {
                                content = targetContent;
                            } else {
                                contentsCurrentById.remove(stored.getId());
                                contentsCurrentByPathContentRelative.remove(stored.getPathContentRelative());

                                // Apply all attributes. There are renaming cases
                                stored.setVolume(targetContent.getVolume());
                                stored.setDirectoryPath(targetContent.getDirectoryPath());
                                stored.setFileName(targetContent.getFileName());
                                // Reapply signature in case of modification
                                stored.setSignature(targetContent.getCrc(), targetContent.getMd5(), targetContent.getSha1(), targetContent.getSize());
                                content = stored;
                            }

                            content.setFileEntry(tct.getTargetFileEntry());
                            return targetContent;
                        }))
            .peek(
                c -> {
                    // Update file data for modification checks
                    c.setFileSize(fileSize);
                    c.setEnrichTime(lastModifiedTime);
                    c.setModifiedTime(lastModifiedTime);
                    c.setCheckTime(lastModifiedTime);
                })
            .toList();
        var deleteContents = contentsCurrentById.values();
        deleteContents.forEach(contentRepositoryService::delete); // 1st delete to avoid Constraint errors
        updatedContents.forEach(contentRepositoryService::save);
    }

    private ArchiveEntryProducer toArchiveEntryProducer(Content content, String targetName) throws IOException {
        var volumeRootPath = content.getVolume().toPath();
        var filePath = volumeRootPath.resolve(content.getPathFileRelative());
        if (content.isCompressedEntry()) { // Archive
            return archiveService.extractToLazyArchiveEntryProducerByName(filePath, content.getFileEntry(), targetName, 0);
        } else { // Regular File
            return new PathArchiveEntryProducer(filePath, volumeRootPath, targetName);
        }
    }

    /**
     * @param origins                  Collection of content origins
     * @param targetFilePath           Desired destination
     * @param targetVolume
     * @param unknownOriginsByFilePath
     * @throws IOException
     */
    public void transferContents(List<OriginContentTransfer> origins, Path targetFilePath, ContentVolume targetVolume, Map<Path, List<OriginContentTransfer>> unknownOriginsByFilePath) throws IOException { // NOPMD complexity justified
        var destinationRootPath = targetVolume.toPath();

        var targetAlreadyExists = Files.exists(targetFilePath);
        List<TargetContentTransfer> targetContentTransfers;
        if (targetAlreadyExists) {
            // if all origins are identified and have contentId = null and all same target and target same time and same size then no need to enrich again.
            var sameOriginAndTargetAndSameExactContent
                = origins.stream().allMatch(c -> c.isIdentified() && c.getContent().getId() == null && c.getContent().getPathFileAbsolute().equals(targetFilePath) && c.getContent().isFileExistsAndValid());

            Stream<TargetContentTransfer> stream;
            if (sameOriginAndTargetAndSameExactContent) {
                log.debug("transferContents: Target and origin are same and new, file={}", targetFilePath);
                stream = origins.stream().map(o -> new TargetContentTransfer(o.getContent(), o.getContentMetadataItem()));
            } else {
                log.debug("transferContents: Target exists so compare, file={}", targetFilePath);
                stream = contentEnrichService.getContentsEnriched(targetVolume, targetFilePath, p -> contentErrorService.doRejectBroken(targetVolume, p)).stream().map(targetContentTransferService::createTargetContentTransfer);
            }

            targetContentTransfers = stream.peek(
                    tcs -> {
                        if (tcs.isIdentified() && tcs.getContent().getId() == null) {
                            // Corner case when not prev scanned target!! Happens if directories are manipulated or if database is wiped while contents are left
                            log.warn("transferContents: Found unexpected identified content in target so auto-adding, content={}", tcs.getContent());
                            var content = contentRepositoryService.saveAndCreateChange(tcs.getContent());
                            origins.stream()
                                .filter(o -> o.getContent().isSameSignatureAs(content) && o.getContent().isSameLocationAs(content))
                                .forEach(
                                    o -> {
                                        o.addImportedFlag();
                                        o.addManagedByTransferFlag();
                                    });
                        }
                    })
                .toList();

        } else {
            targetContentTransfers = Collections.emptyList();
        }

        // Marking origin transfers that already exist in target. May happen if date changes. Needed to avoid unwanted cleanup
        origins.stream().filter(o -> targetContentTransfers.stream().anyMatch(tcs -> tcs.isSameContentAs(o))).forEach(OriginContentTransfer::addManagedByTransferFlag);

        List<TargetContentTransfer> knownUniqueTargets = new ArrayList<>();
        List<TargetContentTransfer> targetContentTransfersThatAreUnknown = new ArrayList<>();
        targetContentTransfers.forEach(
            tcs -> {
                if (tcs.isIdentified()) {
                    knownUniqueTargets.add(tcs);
                } else {
                    targetContentTransfersThatAreUnknown.add(tcs);
                }
            });

        List<OriginContentTransfer> knownUniqueOrigins = new ArrayList<>();
        origins.forEach(
            o -> {
                if (o.isContentSignatureSameAsAnyOf(knownUniqueTargets) || o.isContentSignatureSameAsAnyOf(knownUniqueOrigins)) {
                    o.addImportedFlag(); // Duplicate in any case
                    o.addDeleteContentFlag();
                } else {
                    knownUniqueOrigins.add(o);
                }
            });

        if (!targetContentTransfersThatAreUnknown.isEmpty()) {
            var unknownContentVolumeUniquePath = getPathUniqueFile(contentVolumeService.getExportUnknownPath().resolve(destinationRootPath.relativize(targetFilePath)));

            List<TargetContentTransfer> targetContentTransfersThatAreUnknownAndDuplicated = new ArrayList<>();
            List<TargetContentTransfer> targetContentTransfersThatAreUnknownAndUnique = new ArrayList<>();
            targetContentTransfersThatAreUnknown.forEach(
                tcs -> {
                    if (tcs.isContentSignatureSameAsAnyOf(targetContentTransfersThatAreUnknownAndUnique)) {
                        targetContentTransfersThatAreUnknownAndDuplicated.add(tcs);
                    } else {
                        targetContentTransfersThatAreUnknownAndUnique.add(tcs);
                    }
                });

            log.warn("transferContents: Target contains unknown files so moving to unknown, path={}, target={}", targetFilePath, unknownContentVolumeUniquePath);

            // Improve extreme case. All target is unknown.
            archiveService.archive(unknownContentVolumeUniquePath, targetContentTransfersThatAreUnknownAndUnique.stream().map(uFunction(tcs -> toArchiveEntryProducer(tcs.getContent(), tcs.getContent().getFileEntry()))).toList());

            var signatures = Stream.concat(targetContentTransfersThatAreUnknownAndUnique.stream(), targetContentTransfersThatAreUnknownAndDuplicated.stream()).map(tct -> tct.getContent().getSignature()).collect(toSet());
            Stream.concat(targetContentTransfersThatAreUnknownAndUnique.stream(), targetContentTransfersThatAreUnknownAndDuplicated.stream())
                .peek(
                    tct -> {
                        var absolutePath = tct.getContent().getPathFileAbsolute();
                        toStream(unknownOriginsByFilePath.get(absolutePath)).filter(oct -> signatures.contains(oct.getContent().getSignature())).forEach(OriginContentTransfer::addManagedByTransferFlag);
                    })
                .map(ContentTransfer::getContent)
                .filter(c -> c.getId() != null)
                .forEach(contentRepositoryService::delete);
        }

        var originContainsKnownContents = !knownUniqueOrigins.isEmpty();
        if (!originContainsKnownContents && knownUniqueTargets.isEmpty()) {
            // nothing so I should be able to delete file if exists...
            log.warn("transferContents: Origin and target do not have any known contents TODO Move destination to unknown?, path={}", targetFilePath);
            return;
        }

        List<ContentTransfer> contentTransfersThatAreKnownAndUnique = new ArrayList<>();
        contentTransfersThatAreKnownAndUnique.addAll(knownUniqueOrigins);
        contentTransfersThatAreKnownAndUnique.addAll(knownUniqueTargets);

        // Adjust names if there would be entry name collisions
        var targetFileEntriesWouldContainDuplicates = contentTransfersThatAreKnownAndUnique.stream().map(ContentTransfer::getTargetFileEntryByDefault).distinct().count() < contentTransfersThatAreKnownAndUnique.size();
        if (targetFileEntriesWouldContainDuplicates) {
            // Still is possible that even duplicate cm names!
            var targetFileEntriesWouldStillContainDuplicates
                = contentTransfersThatAreKnownAndUnique.stream().map(ContentTransfer::getTargetFileEntryByContentMetadataItemAndContentMetadata).distinct().count() < contentTransfersThatAreKnownAndUnique.size();
            if (targetFileEntriesWouldStillContainDuplicates) {
                var targetFileEntriesWouldStillStillContainDuplicates
                    = contentTransfersThatAreKnownAndUnique.stream().map(ContentTransfer::getTargetFileEntryByContentMetadataItemAndContentMetadataAndContentMetadataId).distinct().count() < contentTransfersThatAreKnownAndUnique.size();
                if (targetFileEntriesWouldStillStillContainDuplicates) {
                    contentTransfersThatAreKnownAndUnique.forEach(t -> t.setTargetFileEntry(getTargetFileEntryByContentMetadataItemAndContentMetadataAndContentMetadataIdAndCrc(t)));
                } else {
                    contentTransfersThatAreKnownAndUnique.forEach(t -> t.setTargetFileEntry(getTargetFileEntryByContentMetadataItemAndContentMetadataAndContentMetadataId(t)));
                }
            } else {
                contentTransfersThatAreKnownAndUnique.forEach(t -> t.setTargetFileEntry(getTargetFileEntryByContentMetadataItemAndContentMetadata(t)));
            }
        } else if (origins.size() == 1 && origins.get(0).getContentMetadataSourceStorageStrategy().equals(ContentMetadataSourceStorageStrategy.FLAT)) {
            contentTransfersThatAreKnownAndUnique.forEach(t -> t.setTargetFileEntry(null));
            log.debug("transferContents: content marked for flat transfer, target={}", targetFilePath);
        } else {
            contentTransfersThatAreKnownAndUnique.forEach(t -> t.setTargetFileEntry(getTargetFileEntryByDefault(t)));
        }

        if (targetContentTransfersThatAreUnknown.isEmpty() && !originContainsKnownContents) {
            log.debug("transferContents: Ignoring already exists, target={}", targetFilePath);
            knownUniqueOrigins.forEach(OriginContentTransfer::addImportedFlag);
            return;
        }

        var archiveEntryProducers = contentTransfersThatAreKnownAndUnique.stream()
            .sorted(Comparator.comparing(x -> x.getContent().getPathContentRelative()))
            .map(uFunction(ct -> toArchiveEntryProducer(ct.getContent(), ct.getTargetFileEntry()))).toList();
        var sMillis = System.currentTimeMillis();
        var pathFileAbsolute = origins.get(0).getContent().getPathFileAbsolute();
        if (targetAlreadyExists) {
            if (isOriginContentsAllFromSingleFileWithNameEqualsTargetFileName(origins, targetFilePath)
                && isOriginContentsAllCompressedAndIdentifiedAndHaveTargetExpectedNames(origins)
                && origins.size() == origins.get(0).getOriginContentCount() // all files in origin are used
                && knownUniqueTargets.stream().allMatch(cs -> cs.isContentSignatureSameAsAnyOf(origins))) { // allTargetKnownContentsAreInOriginToo
                log.debug("transferContents: By replacing archive, target={}", targetFilePath);
                move(pathFileAbsolute, targetFilePath);
            } else {
                log.debug("transferContents: By rearchiving, target={}", targetFilePath);
                var tempTarget = targetFilePath.resolveSibling("000TMP_" + targetFilePath.getFileName());
                archiveService.archive(tempTarget, archiveEntryProducers);
                move(tempTarget, targetFilePath);
                if (log.isDebugEnabled()) {
                    log.debug("transferContents: Rearchived, target={} ({} ms)", targetFilePath, System.currentTimeMillis() - sMillis);
                }
            }
        } else { // Target does not exist so no merge needed
            if (isOriginContentsAllFromSingleFileWithNameEqualsTargetFileName(origins, targetFilePath)
                && isOriginContentsAllCompressedAndIdentifiedAndHaveTargetExpectedNames(origins)
                && knownUniqueOrigins.size() == origins.get(0).getOriginContentCount()) { // all files in origin are used
                log.debug("transferContents: By moving archive, target={}", targetFilePath);
                move(pathFileAbsolute, targetFilePath);
            } else if (archiveService.getArchiverFormat(targetFilePath).isEmpty()) { // Target not compressed. Then its single file!
                var compressedOriginCount = origins.stream().filter(ct -> ct.getContent().isCompressedEntry()).count();
                if (compressedOriginCount > 0 && compressedOriginCount == origins.size()) { // All origins are compressed
                    origins
                        .stream()
                        .filter(ct -> ct.getContent().isCompressedEntry())
                        .forEach(uConsumer(
                            o -> {
                                log.debug("transferContents: By extracting to plainFile, target={}", targetFilePath);
                                archiveService.extractToPathByName(pathFileAbsolute, o.getContent().getFileEntry(), targetFilePath);
                            }));
                } else { // At least one origin is not compressed
                    origins
                        .stream()
                        .filter(ct -> !ct.getContent().isCompressedEntry())
                        .forEach(uConsumer(
                            o -> {
                                log.debug("transferContents: By moving plainFile, target={}", targetFilePath);
                                move(o.getContent().getPathFileAbsolute(), targetFilePath);
                            }));
                }
            } else {
                log.debug("transferContents: By archiving, target={}", targetFilePath);
                archiveService.archive(targetFilePath, archiveEntryProducers);
                if (log.isDebugEnabled()) {
                    log.debug("transferContents: Archived, target={} ({} ms)", targetFilePath, System.currentTimeMillis() - sMillis);
                }
            }
        }

        merge(targetVolume, targetFilePath, knownUniqueOrigins, knownUniqueTargets);
    }

    @SuppressWarnings("unchecked")
    public int transferContents(Map<Path, List<OriginContentTransfer>> originContentTransfersByTargetFilePath, ContentVolume targetVolume, boolean parallel) throws ExecutionException, InterruptedException {
        Map<Path, List<OriginContentTransfer>> unknownOriginContentTransfersByOriginFilePath
            = Optional.ofNullable(originContentTransfersByTargetFilePath.get(UNKOWN_PATH)).map(l -> l.stream().collect(groupingBy(OriginContentTransfer::getOriginFileAbsolutePath))).orElse(Collections.EMPTY_MAP);

        var count = new AtomicInteger();
        var errorCount = new AtomicInteger();

        BiConsumer<Path, List<OriginContentTransfer>> action = (p, ts) -> {
            if (p.equals(UNKOWN_PATH)) {
                return;
            }
            count.incrementAndGet();
            log.debug("transferContents: Importing, i={}/{}, target={}", count.get(), originContentTransfersByTargetFilePath.size(), p);
            transactionRequiresNewTemplate.executeWithoutResult(
                t -> {
                    try {
                        var contentVolume = targetVolume;
// Will need to refactor if enable hibernate collection management enhancements
//                        var contentVolume = contentVolumeRepositoryService.getReferenceById(targetVolume.getId());
                        this.transferContents(ts, p, contentVolume, unknownOriginContentTransfersByOriginFilePath);
                    } catch (IOException ioException) {
                        errorCount.incrementAndGet();
                        log.error("transferContents: Importing failed, i={}/{}, target={}", count.get(), originContentTransfersByTargetFilePath.size(), p, ioException);
                    }
                });
        };

        if (parallel) {
            forkJoinPoolProvider.submitNow(() -> originContentTransfersByTargetFilePath.entrySet().parallelStream().forEach(e -> action.accept(e.getKey(), e.getValue())));
        } else {
            originContentTransfersByTargetFilePath.forEach(action);
        }
        return errorCount.get();
    }
}
