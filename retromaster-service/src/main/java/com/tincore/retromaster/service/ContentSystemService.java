package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.contentsystem.ContentSystemExtractor;
import com.tincore.retromaster.service.contentsystem.ContentSystemExtractors;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.tincore.retromaster.service.contentsystem.ContentSystemExtractor.normalizeSystemLabel;

@Service
public class ContentSystemService {

    private final ContentSystemExtractors extractors = new ContentSystemExtractors();

    public List<ContentSystem> findByContentMetadata(ContentMetadata contentMetadata) {
        return extractors.stream()
            .map(e -> e.extract(contentMetadata))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .toList();
    }

    @Cacheable("contentSystems")
    public Optional<ContentSystem> findBySystemLabel(String label) {
        var normalizedSystemLabel = normalizeSystemLabel(label);
        return extractors.stream()
            .filter(e -> e.isMatchBySystemLabel(normalizedSystemLabel))
            .map(ContentSystemExtractor::getContentSystem)
            .findFirst();
    }

    public String findSystemNormalized(String system) {
        return findBySystemLabel(system).map(Enum::name).orElse(system);
    }
}
