package com.tincore.retromaster.service.transfer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.domain.transfer.OriginContentTransfer;
import com.tincore.retromaster.service.*;
import com.tincore.retromaster.service.archive.ArchiverFormat;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.LRUCache;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentMetadataSourceStorageStrategy.FLAT;
import static com.tincore.retromaster.domain.transfer.ContentTransfer.UNKOWN_PATH;
import static com.tincore.util.lang.ThrowingFunction.uFunction;


@Slf4j
@Service
@RequiredArgsConstructor
public class OriginContentTransferService implements StreamTrait, FileSystemTrait {

    public static final int NAX_ALLOWED_DIRECTORIES_FOR_BFS = 100;
    private static final boolean MERGE_INCLUDE_TITLE = true;
    private static final boolean MERGE_INCLUDE_PUBLISHER = true;
    private static final boolean MERGE_INCLUDE_DATE = true;
    private static final int CACHE_SIZE = 50;

    private final ContentEnrichService contentEnrichService;
    private final ContentMetadataSourceService contentMetadataSourceService;
    private final ContentMetadataItemService contentMetadataItemService;
    private final ArchiverFormatContentTransferService archiverFormatContentTransferService;

    private final ContentVolumeRepositoryService contentVolumeRepositoryService;

    private final RetromasterConfiguration retromasterConfiguration;

    private final TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;

    private final LRUCache<FileTime, FileTime> recentPathFileTimes = new LRUCache<>(CACHE_SIZE);

    private static Path getTargetFilePathAsFlattenedFile(Path targetVolumePath, ContentMetadataItem contentMetadataItem) {
        return targetVolumePath
            .resolve(contentMetadataItem.getContentMetadata().getContentMetadataSet().getRepositoryPath())
            .resolve(contentMetadataItem.getName());
    }

    private OriginContentTransfer createOriginContentTransfer(Content content, Path targetVolumePath, int originContentCount) {
        var contentMetadataItem = contentMetadataItemService.getContentMetadataItemByContentAndNotStagingSortedByPrecedence(content);

        if (contentMetadataItem == null) {
            return new OriginContentTransfer(content, contentMetadataItem, originContentCount, UNKOWN_PATH, true, FLAT);
        }

        var contentMetadata = contentMetadataItem.getContentMetadata();
        var contentMetadataSource = contentMetadataSourceService.getContentMetadataSource(contentMetadataItem.getContentMetadata().getContentMetadataSet());
        var contentMetadataSourceStorageStrategy = contentMetadataSource.getContentMetadataSourceStorageStrategy();
        var targetFilePath = switch (contentMetadataSourceStorageStrategy) {
            case ARCHIVE_DEFAULT ->
                archiverFormatContentTransferService.getArchiverFormatByContentMetadata(contentMetadata)
                    .map(af -> getTargetFilePathAsCompressedFile(targetVolumePath, contentMetadataItem, contentMetadataSource, af))
                    .orElseGet(() -> getTargetFilePathAsSubDirectory(targetVolumePath, contentMetadataItem));
            case ARCHIVE_QUICK -> archiverFormatContentTransferService.getArchiverFormatByQuick()
                .map(af -> getTargetFilePathAsCompressedFile(targetVolumePath, contentMetadataItem, contentMetadataSource, af))
                .orElseGet(() -> getTargetFilePathAsSubDirectory(targetVolumePath, contentMetadataItem));
            case DIRECTORY -> getTargetFilePathAsSubDirectory(targetVolumePath, contentMetadataItem);
            case FLAT -> getTargetFilePathAsFlattenedFile(targetVolumePath, contentMetadataItem);
        };

        return new OriginContentTransfer(content, contentMetadataItem, originContentCount, targetFilePath, contentMetadataSourceStorageStrategy);
    }

    private List<Path> findFilePaths(Path originVolumeAbsolutePath, long fileLimit) throws IOException {
        return findFilePathsBFS(originVolumeAbsolutePath, fileLimit);
    }

    private List<Path> findFilePathsBFS(Path originVolumeAbsolutePath, long fileLimit) throws IOException {
        Queue<Path> queue = new LinkedList<>();
        queue.add(originVolumeAbsolutePath);
        return findFilePathsBFS(fileLimit, queue);
    }

    private List<Path> findFilePathsBFS(long fileLimit, Queue<Path> queue) throws IOException {
        List<Path> result = new ArrayList<>();
        while (!queue.isEmpty() && result.size() < fileLimit) {
            var current = queue.poll();
            List<Path> subPaths;
            try (var list = Files.list(current)) {
                subPaths = list
                    .sorted(Comparator.comparing(p -> p.toAbsolutePath().toString()))
                    .limit(fileLimit)
                    .toList();
            }
            var directoryCount = subPaths.stream().filter(Files::isDirectory).count();
            var regularFileCount = subPaths.size() - directoryCount;

            var childrenBFS = regularFileCount == 0 || directoryCount > NAX_ALLOWED_DIRECTORIES_FOR_BFS;
            for (var subPath : subPaths) {
                if (Files.isDirectory(subPath)) {
                    if (childrenBFS) {
                        queue.add(subPath);
                    } else {
                        try (var s = findPaths(subPath)) {
                            s.limit(fileLimit - result.size()).forEach(result::add);
                        }
                    }
                } else {
                    result.add(subPath);
                }
            }

        }
        return result;
    }

    private String getTargetFileName(List<String> tokens) {
        var nameBuilder = new StringBuilder();
        nameBuilder.append(tokens.get(0));
        var multipleTokens = tokens.size() > 1;
        if (multipleTokens) {
            nameBuilder.append(' ');
            for (var i = 1; i < tokens.size(); i++) {
                nameBuilder.append('(');
                nameBuilder.append(tokens.get(i));
                nameBuilder.append(')');
            }
        }

        return nameBuilder.toString();
    }

    public String getTargetFileName(ContentMetadata contentMetadata, boolean mergeSimilarContentMetadatas) {
        if (!mergeSimilarContentMetadatas) {
            return contentMetadata.getName();
        }

        List<String> tokens = new ArrayList<>();
        if (MERGE_INCLUDE_TITLE) {
            tokens.add(contentMetadata.getTitle());
        }
        if (MERGE_INCLUDE_DATE) {
            var date = contentMetadata.getDate();
            if (StringUtils.isNotBlank(date)) {
                tokens.add(date);
            }
        }
        if (MERGE_INCLUDE_PUBLISHER) {
            var publisher = contentMetadata.getPublisher();
            if (StringUtils.isNotBlank(publisher)) {
                tokens.add(publisher);
            }
        }
        if (tokens.isEmpty()) {
            tokens.add(contentMetadata.getName());
        }

        return getTargetFileName(tokens);
    }

    private Path getTargetFilePathAsCompressedFile(Path targetVolumePath, ContentMetadataItem contentMetadataItem, ContentMetadataSource contentMetadataSource, ArchiverFormat af) {
        return targetVolumePath.resolve(contentMetadataItem.getContentMetadata().getContentMetadataSet().getRepositoryPath()).resolve(af.appendExtension(getTargetFileName(contentMetadataItem.getContentMetadata(), contentMetadataSource.isMultipleContentMetadatasCanBelongToSameTitle())));
    }

    private Path getTargetFilePathAsSubDirectory(Path targetVolumePath, ContentMetadataItem contentMetadataItem) {
        return targetVolumePath.resolve(contentMetadataItem.getContentMetadata().getContentMetadataSet().getRepositoryPath()).resolve(contentMetadataItem.getContentMetadata().getName()).resolve(contentMetadataItem.getName());
    }

    private void lockOriginFiles(ContentVolume originVolume, Path originVolumeAbsolutePath, boolean touchFileTimestamp, FileTime fileTime) throws IOException {
        recentPathFileTimes.put(fileTime, fileTime);
        var count = new AtomicInteger();
        // If amount of files is really massive then this can end being relatively slow!!
        var preTouchLimit = retromasterConfiguration.getContentVolumeImportBatchFilePreTouchLimit();
        findFilePaths(originVolumeAbsolutePath, preTouchLimit > 0 ? preTouchLimit : Long.MAX_VALUE)
            .forEach(
                p -> {
                    try {
                        var pathFileTime = Files.getLastModifiedTime(p);
                        if (!recentPathFileTimes.containsKey(pathFileTime)) {
                            count.incrementAndGet();
                            if (touchFileTimestamp) {
                                touch(p, fileTime);
                            }
                        }
                    } catch (IOException e) {
                        log.warn("lockOriginFiles: Failed, path={}", p, e);
                    }
                });
        log.debug("lockOriginFiles: Completed, count={}, vol={}", count.get(), originVolume.getName());
    }

    public Stream<OriginContentTransfer> streamOriginContentTransfers(ContentVolume originVolume, List<Path> analyzePaths, Path targetVolumePath, Consumer<Path> onError) {
        var count = new AtomicInteger();
        count.set(0);
        return analyzePaths.stream()
            .parallel()
            .flatMap(
                uFunction(
                    p -> {
                        if (log.isDebugEnabled()) {
                            log.debug("streamOriginContentTransfers, i={}/{}, path={}", 1 + count.getAndIncrement(), analyzePaths.size(), p);
                        }
                        return transactionRequiresNewTemplate.execute(
                            t -> {
                                try {
                                    // Will need to refactor if enable hibernate collection management enhancements
//                                    var contentVolume = contentVolumeRepositoryService.getReferenceById(originVolume.getId());
                                    var contentVolume = originVolume; // Workaround for now
                                    var contentsEnriched = contentEnrichService.getContentsEnriched(contentVolume, p, onError);
                                    return contentsEnriched.stream().map(c -> createOriginContentTransfer(c, targetVolumePath, contentsEnriched.size())).toList().stream();
                                } catch (Exception e) { // NOPMD
                                    log.error("streamOriginContentTransfers: Failed, path={}", p, e);
                                    throw new WrappedException(e);
                                }
                            });
                    }));
    }

    /**
     * @param contentVolume
     * @param originVolumeAbsolutePath
     * @param targetVolumePath
     * @param onError
     * @param touchFileTimestamp       set to true when files are in an import volume or other volume where data is modified at any moment. Try to set it to false when streaming from a prod volume because if file is not touched then stored
     *                                 analyses can be reused an this is quicker than scanning the file contents again
     * @return
     * @throws IOException
     */
    public Stream<OriginContentTransfer> streamOriginContentTransfers(ContentVolume contentVolume, Path originVolumeAbsolutePath, Path targetVolumePath, Consumer<Path> onError, boolean touchFileTimestamp)
        throws IOException {
        var fileTime = getFileTimeNow();
        log.debug("streamOriginContentTransfers: Scanning, vol={}, path={}", contentVolume.getName(), originVolumeAbsolutePath);
        if (touchFileTimestamp) {
            lockOriginFiles(contentVolume, originVolumeAbsolutePath, touchFileTimestamp, fileTime);
        }

        var preLimit = retromasterConfiguration.getContentVolumeImportBatchFileCountPreLimit();
        var pathStream = findFilePaths(originVolumeAbsolutePath, preLimit > 0 ? preLimit : Long.MAX_VALUE)
            .stream()
            .limit(retromasterConfiguration.getContentVolumeImportBatchFileCountLimit()) // And then get batch
            .filter(
                p -> {
                    if (!touchFileTimestamp) {
                        return true;
                    }
                    try {
                        return recentPathFileTimes.containsKey(Files.getLastModifiedTime(p));
                    } catch (IOException e) {
                        log.warn("streamOriginContentTransfers: Item could not be locked skipping for next iteration, path={}", p);
                        return false;
                    }
                })
            .sorted(Comparator.comparing(p -> p.toAbsolutePath().toString()));

        // Try to avoid processing too many big sized files at a time...
        var maxSizePerBatch = retromasterConfiguration.getContentVolumeImportBatchFileSizeLimit();
        long accumulatedSize = 0;
        List<Path> analyzePaths = new ArrayList<>();
        for (var contentPath : pathStream.toList()) {
            analyzePaths.add(contentPath);
            accumulatedSize += Files.size(contentPath);
            if (accumulatedSize > maxSizePerBatch) {
                log.debug("streamOriginContentTransfers: Count reduction based found size, count={}, size={}", analyzePaths.size(), accumulatedSize);
                break;
            }
        }
        return streamOriginContentTransfers(contentVolume, analyzePaths, targetVolumePath, onError);
    }
}
