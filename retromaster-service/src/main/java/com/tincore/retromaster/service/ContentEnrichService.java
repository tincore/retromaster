package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.service.archive.ArchiverFormat;
import com.tincore.util.FileSystemTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import static com.tincore.retromaster.domain.Content.LONGFILE_SIGNATURE_PREFIX;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentEnrichService implements FileSystemTrait {

    public static final int BUFFER_SIZE = 4096;
    public static final int CRC_LENGTH = 8;

    private static final int HEX_MASK_UP = 240;
    private static final int HEX_MAX_LOW = 15;
    private static final int HEX_UP_SHIFT = 4;
    private static final char[] HEX_DIGITS_LOWER = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
        'e', 'f'};

    private final ArchiveService archiveService;
    private final ContentService contentService;
    private final ContentRepositoryService contentRepositoryService;
    private final RetromasterConfiguration retromasterConfiguration;

    protected Content enrichContent(Content content, LocalDateTime modifiedTime, InputStream inputStream, long fileSize) throws IOException {
        return enrichContent(content, modifiedTime, inputStream, fileSize, StringUtils.EMPTY, false);
    }

    protected Content enrichContent(Content content, LocalDateTime modifiedTime, InputStream inputStream, long fileSize, String enrichStrategyPrefix, boolean forceFileSize) throws IOException {
        try {

            var md5MessageDigest = MessageDigest.getInstance("MD5");
            var sha1MessageDigest = MessageDigest.getInstance("SHA1");
            var checkedInputStream = new CheckedInputStream(inputStream, new CRC32()); // NOPMD Closing stream is responsibility of parent
            var chain = new DigestInputStream(new DigestInputStream(checkedInputStream, md5MessageDigest), sha1MessageDigest); // NOPMD

            long size = 0;
            var buf = new byte[BUFFER_SIZE];
            int read;
            while ((read = chain.read(buf)) >= 0) {
                size += read;
            }

            var crc = StringUtils.leftPad(Long.toHexString(checkedInputStream.getChecksum().getValue()).toUpperCase(), CRC_LENGTH, "0");
            var md5 = toHexString(md5MessageDigest.digest()).toUpperCase();
            var sha1 = toHexString(sha1MessageDigest.digest()).toUpperCase();

            content.setSignature(enrichStrategyPrefix + crc, enrichStrategyPrefix + md5, enrichStrategyPrefix + sha1, forceFileSize ? fileSize : size);

            content.setEnrichTime(modifiedTime);
            content.setModifiedTime(modifiedTime);
            content.setFileSize(fileSize);

            return content;
        } catch (NoSuchAlgorithmException e) {
            throw new RetromasterFatalException("Could not construct digests! Java version problem?", e);
        }
    }

    public List<Content> getContentsEnriched(ContentVolume contentVolume, Path fileAbsolutePath, Consumer<Path> onError) throws IOException {
        var archiverFormat = ArchiverFormat.byPath(fileAbsolutePath);
        var lastModifiedTime = getLastModifiedTime(fileAbsolutePath);
        var fileSize = Files.size(fileAbsolutePath);
        // ADD CACHE HERE. THIS WOULD ALLOW FOR MUCH LARGER SCANS ON IMPORT. THEN TRIM ANYWAY BUT BY GROUPED FILE COUNT
        return archiverFormat
            .map(af -> getContentsEnrichedByArchive(contentVolume, fileAbsolutePath, onError, lastModifiedTime, fileSize, af))
            .orElseGet(
                () -> {
                    if (Files.isDirectory(fileAbsolutePath)) {
                        return getContentsEnrichedByDirectory(contentVolume, fileAbsolutePath, lastModifiedTime);
                    } else { // Single file
                        return getContentsEnrichedByFile(contentVolume, fileAbsolutePath, lastModifiedTime);
                    }
                });
    }

    private List<Content> getContentsEnrichedByArchive(ContentVolume contentVolume, Path path, Consumer<Path> onError, LocalDateTime lastModifiedTime, long fileSize, ArchiverFormat af) {
        // This optimization will only work if prod files are trusted, Will not work if they have unexpected content
        if (contentVolume.getEnvironment().isContainsImported()) {
            var directoryPath = contentVolume.toPath().relativize(path.getParent()).toString();
            var fileName = path.getFileName().toString();
            var contents = contentRepositoryService.findByVolumeAndDirectoryPathAndFileName(contentVolume, directoryPath, fileName);
            if (!contents.isEmpty() && contents.stream().allMatch(c -> c.isEnriched(lastModifiedTime))) {
                log.debug("getContentsEnrichedByArchive: Already known, contents={}", contents);
                return contents;
            }
        }
        var exceptionHolder = new AtomicReference<Exception>();
        try (var channel = Files.newByteChannel(path)) {
            var results = archiveService.extract(
                af,
                channel,
                lastModifiedTime,
                a -> {
                    try {
                        if (a.isDirectory()) {
                            return null;
                        }
                        var content = contentService.findOrCreateEmptyContent(contentVolume, path, a.getName(), true);
                        if (content.isEnriched(lastModifiedTime)) {
                            log.debug("getContentsEnrichedByArchive: Found fresh so skip hash, content={}#{}", content.getFileName(), content.getFileEntry());
                            return content;
                        }

                        var inputStream = a.getInputStream(); // Here it is closed by archiver so DO NOT CLOSE!!
                        log.debug("getContentsEnrichedByArchive: Hashing new or changed, content={}#{}", content.getFileName(), content.getFileEntry());
                        var enrichedContent = enrichContent(content, lastModifiedTime, inputStream, fileSize);
                        log.debug("getContentsEnrichedByArchive: Hashed, content={}#{}", content.getFileName(), content.getFileEntry());
                        return enrichedContent;
                    } catch (IOException e) {
                        exceptionHolder.set(e);
                        return null;
                    }
                });
            var success = exceptionHolder.get() == null;
            if (success) {
                return results;
            }
            log.error("getContentsEnrichedByArchive: Failure enriching, path={}", path, exceptionHolder.get());
            onError.accept(path);
            return Collections.emptyList();
        } catch (IOException e) {
            log.error("getContentsEnrichedByArchive: Failure enriching error, path={}", path, e);
            onError.accept(path);
            return Collections.emptyList();
        }
    }

    private List<Content> getContentsEnrichedByDirectory(ContentVolume volume, Path path, LocalDateTime lastModifiedTime) {
        try (var s = findPaths(path)) {
            return s.filter(BY_REGULAR_FILE).flatMap(p -> getContentsEnrichedByFile(volume, p, lastModifiedTime).stream()).toList();
        } catch (IOException e) {
            log.error("getContentsEnrichedByDirectory: Failure enriching so return empty, path={}", path, e);
            return Collections.emptyList();
        }
    }

    private List<Content> getContentsEnrichedByFile(ContentVolume volume, Path path, LocalDateTime lastModifiedTime) {
        var content = contentService.findOrCreateEmptyContent(volume, path, null, true);
        if (content.isEnriched(lastModifiedTime)) {
            log.debug("getContentsEnrichedByFile: Already known, content={}", content);
            return Stream.of(content).toList();
        }

        try {
            var fileSize = Files.size(path);
            var stageLargeFile = volume.isStage() && fileSize > retromasterConfiguration.getContentSignatureProcessorStageLargeFileThreshold();
            if (stageLargeFile) {
                log.debug("getContentsEnrichedByFile: Staging very large file detected so calculate signature based on fileName, content={}", content);
                try (var inputStream = new ByteArrayInputStream(content.getPathContentRelative().toString().getBytes(StandardCharsets.UTF_8))) {
                    var enrichedContent = enrichContent(content, lastModifiedTime, inputStream, fileSize, LONGFILE_SIGNATURE_PREFIX, true);
                    return Stream.of(enrichedContent).toList();
                }
            } else {
                log.trace("getContentsEnrichedByFile: New or changed calculating signature, content={}", content);
                try (var inputStream = Files.newInputStream(path)) {
                    var enrichedContent = enrichContent(content, lastModifiedTime, inputStream, fileSize);
                    return Stream.of(enrichedContent).toList();
                }

            }
        } catch (IOException e) {
            log.error("getContentsEnrichedByFile: Error so return empty, path={}", path, e);
            return Collections.emptyList();
        }
    }

    private String toHexString(byte[] digest) {
        var length = digest.length;
        var chars = new char[length * 2];
        var i = 0;
        var j = 0;
        while (i < length) {
            chars[j++] = HEX_DIGITS_LOWER[(HEX_MASK_UP & digest[i]) >>> HEX_UP_SHIFT];
            chars[j++] = HEX_DIGITS_LOWER[HEX_MAX_LOW & digest[i]];
            ++i;
        }

        return new String(chars);
    }
}
