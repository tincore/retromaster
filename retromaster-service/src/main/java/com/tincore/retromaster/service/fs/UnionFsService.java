package com.tincore.retromaster.service.fs;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.FilesystemConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.service.ProcessExecutorService;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.UuidUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UnionFsService implements FileSystemTrait {

    private static final int UNIONFS_ALREADY_MOUNTED = 1;
    private static final int SUCCESS = 0;
    private static final String DATA_SUFFIX = "_data";

    private final FilesystemConfiguration fileSystemConfiguration;
    private final ProcessExecutorService processExecutorService;

    private final Set<Path> mountedUnionFsPaths = new HashSet<>();

    private String getDirectoryName(ContentVolume volume) {
        return volume.getEnvironment() + "_" + UuidUtil.toString64(volume.getId());
    }

    public Path getPath(ContentVolume volume) {
        return fileSystemConfiguration.getMountRootPath().resolve(getDirectoryName(volume));
    }

    public Path getPath(Content content) {
        return getPath(content.getVolume()).resolve(content.getPathContentRelative());
    }

    public Path getUnionFsDataPath(ContentVolume volume) {
        return fileSystemConfiguration.getMountRootPath().resolve(getDirectoryName(volume) + DATA_SUFFIX);
    }

    public boolean isActive(ContentVolume volume) {
        var path = getPath(volume);
        try {
            return Files.exists(path) && !isEmpty(path);
        } catch (IOException e) {
            log.warn("error finding unionfs path", e);
            return false;
        }
    }

    public void mount(ContentVolume volume, Path readOnlyPath) throws IOException, InterruptedException {
        mount(getPath(volume), readOnlyPath, getUnionFsDataPath(volume));
    }

    public void mount(Path unionMountPath, Path readOnlyPath, Path mutatedDataPath) throws IOException, InterruptedException {
        synchronized (this) {
            if (mountedUnionFsPaths.contains(unionMountPath)) {
                log.warn("mount unionfs skipped. path={}", unionMountPath);
                return;
            }

            Files.createDirectories(unionMountPath);
            Files.createDirectories(mutatedDataPath);
            // contentExecuteProcessService.exec(String.format("fuse-overlayfs --debug -oworkdir=%s -olowerdir=%s -oupperdir=%s %s", unionTmpPath, readOnlyPath, mutatedDataPath, unionMountPath), executionContext);
            var command = String.format("unionfs -o cow %s=RW:%s=RO %s", mutatedDataPath, readOnlyPath, unionMountPath);
            var result = processExecutorService.exec(command, UUID.randomUUID());

            if (result != SUCCESS) {
                log.info("mount unionfs failed trying remount cmd={}", command);
                umount(unionMountPath, true);
                result = processExecutorService.exec(command, UUID.randomUUID());
            }
            if (result == SUCCESS || result == UNIONFS_ALREADY_MOUNTED) {
                log.info("mount unionfs. Path={}, Result={}", unionMountPath, result);
                mountedUnionFsPaths.add(unionMountPath);
            } else {
                log.error("mount unionfs failed. Are you missing unionfs? sudo apt install unionfs-fuse. cmd={}, result={}", command, result);
            }
        }
    }

    @PreDestroy
    public void preDestroy() {
        umountAll();
    }

    public void umount(Path path) throws IOException, InterruptedException {
        synchronized (this) {
            umount(path, false);
        }
    }

    public void umount(Path path, boolean force) throws IOException, InterruptedException {
        if (!force && !mountedUnionFsPaths.contains(path)) {
            log.warn("umount unionfs skipped. Path={}", path);
            return;
        }
        var result = processExecutorService.exec("fusermount -u " + path.toString(), UUID.randomUUID());
        log.info("umount unionfs. Path={}, Result={}", path, result);
        mountedUnionFsPaths.remove(path);
    }

    private void umountAll() {
        var paths = new ArrayList<>(mountedUnionFsPaths);
        paths.forEach(
            p -> {
                try {
                    umount(p);
                } catch (IOException | InterruptedException e) {
                    log.error(String.format("umount unionfs failed. Path=%s", p), e);
                }
            });
    }
}
