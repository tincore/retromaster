package com.tincore.retromaster.service.execute;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadataItem;
import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Data
public class ExecutionContext {
    private final UUID executionId;
    private final ContentMetadataItem mainItem;
    private final List<ContentMetadataItem> associatedItems;
    private final List<Pair<ContentMetadataItem, Content>> contentsMain;
    private final List<Pair<ContentMetadataItem, Content>> contentsAssociated;

    private final List<Path> otherItemPaths = new ArrayList<>();

    private Path targetProjectionPath;
    private Path mainItemPath;
    private LocalDateTime setUpCompletedTimestamp;

    public String formatFileName(String name) {
        return name;
    }

    public long getContentSourceLocationsCount() {
        return Stream.concat(contentsMain.stream(), contentsAssociated.stream()).filter(p -> p.getRight() != null).map(p -> p.getRight().getPathFileAbsolute()).distinct().count();
    }

    public Content getMainContent() {
        return contentsMain.stream().filter(p -> p.getLeft().getId().equals(mainItem.getId())).findFirst().map(Pair::getRight).orElseThrow(() -> new IllegalStateException("No valid content found for main item=" + mainItem));
    }
}
