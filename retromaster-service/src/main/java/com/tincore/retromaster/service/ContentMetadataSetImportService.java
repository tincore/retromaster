package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.config.TaskExecutorConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.event.ContentMetadataSetManageEvent;
import com.tincore.retromaster.domain.event.ContentMetadataSetManageEventType;
import com.tincore.retromaster.service.dat.DatFormMapper;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.Partition;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentLocation.isFileModifiedTimeEquals;
import static com.tincore.util.lang.ThrowingConsumer.uConsumer;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ContentMetadataSetImportService implements StreamTrait, FileSystemTrait {

    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentMetadataSetService contentMetadataSetService;
    private final ContentMetadataSetEnricherService contentMetadataSetEnricherService;
    private final ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    private final ContentMetadataVolumeService contentMetadataVolumeService;
    private final ContentSignatureChangeRepositoryService contentSignatureChangeRepositoryService;

    private final RetromasterConfiguration retromasterConfiguration;

    private final ApplicationEventPublisher applicationEventPublisher;
    private final TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider;
    private final TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;
    private final DatFormMapper datFormMapper;

    private ContentMetadataSet createContentMetadataSet(Path path, String filename) throws IOException {
        try (var is = Files.newInputStream(path)) {
            var xmlMapper = new XmlMapper();
            var datafile = xmlMapper.readValue(is, DatFormMapper.Datafile.class);
            return datFormMapper.toContentMetadataSet(datafile, filename);
        }
    }

    private ContentMetadataSet doContentMetadataSetImport(Path path) throws IOException, ExecutionException, InterruptedException { // NOPMD
        var filename = path.getFileName().toString();

        var storedContentMetadataSetFilePath = contentMetadataVolumeService.getProdPath(filename);

        var selfImport = path.equals(storedContentMetadataSetFilePath);

        var contentMetadataSetAlreadyStored = contentMetadataSetRepositoryService.findByFilename(filename).orElse(null);
        if (!selfImport
            && contentMetadataSetAlreadyStored != null
            && Files.exists(storedContentMetadataSetFilePath)
            && isFileModifiedTimeEquals(storedContentMetadataSetFilePath, contentMetadataSetAlreadyStored.getCheckTime())
            && Files.size(storedContentMetadataSetFilePath) == Files.size(path)) {
            log.debug("doContentMetadataSetImport: Trying to import duplicate. Moving to export, name={}", contentMetadataSetAlreadyStored.getName());
            moveContentMetadataSetDatFileToExportDuplicated(path);
            return contentMetadataSetAlreadyStored;
        }

        var parsedContentMetadataSet = createContentMetadataSet(path, filename);
        var contentMetadataSetEnricher = contentMetadataSetEnricherService.getByContentMetadataSet(parsedContentMetadataSet);
        var enrichedContentMetadataSet = contentMetadataSetEnricher.enrichContentMetadataSet(parsedContentMetadataSet);

        if (contentMetadataSetAlreadyStored == null) {
            // Check if old version
            if (retromasterConfiguration.isMetadataSetAutoCleanOlderVersions()) {
                var contentMetadataSetsWithSameNameAndCategory = contentMetadataSetRepositoryService.findByNameAndCategory(parsedContentMetadataSet.getName(), parsedContentMetadataSet.getCategory());
                if (!contentMetadataSetsWithSameNameAndCategory.isEmpty()) {
                    var mostRecent = Stream.concat(Stream.of(enrichedContentMetadataSet), contentMetadataSetsWithSameNameAndCategory.stream()).max(Comparator.comparing(ContentMetadataSet::getVersion)).get();
                    if (enrichedContentMetadataSet.equals(mostRecent)) {
                        contentMetadataSetsWithSameNameAndCategory.forEach(
                            c -> {
                                log.debug("doContentMetadataSetImport: Previous version found in import so moving to legacy, name={}", c.getName());
                                contentMetadataSetService.moveContentMetadataSetDatFileToLegacyAndDelete(c);
                            });
                    } else {
                        log.debug("doContentMetadataSetImport: Older than present so moving to legacy,. name={}, versions={}=>{}", enrichedContentMetadataSet.getName(), mostRecent.getVersion(), enrichedContentMetadataSet.getVersion());
                        contentMetadataSetService.moveContentMetadataSetDatFileToLegacy(path);
                        return mostRecent;
                    }
                }
            }
        } else {
            // Remove to replace with new
            contentMetadataSetService.delete(contentMetadataSetAlreadyStored, selfImport ? null : storedContentMetadataSetFilePath);
        }

        enrichedContentMetadataSet.setCheckTime(LocalDateTime.MIN);
        var contentMetadataSet = transactionRequiresNewTemplate.execute(ts -> contentMetadataSetRepositoryService.save(enrichedContentMetadataSet));
        log.info("doContentMetadataSetImport: Set Saved. Importing enriched ContentMetadatas, id={}, name={}", contentMetadataSet.getId(), contentMetadataSet.getName());

        var systemDefault = contentMetadataSetEnricher.extractSystem(contentMetadataSet);
        var enrichedContentMetadatas = contentMetadataSetEnricher.enrichContentMetadatas(contentMetadataSet.getContentMetadatas(), contentMetadataSet, systemDefault);
        enrichedContentMetadatas.forEach(
            c -> {
                if (c.getContentMetadataSet() == null) {
                    c.setContentMetadataSet(contentMetadataSet);
                }
                if (c.getItems() != null) {
                    c.getItems().stream().filter(i -> i.getContentMetadata() == null).forEach(i -> i.setContentMetadata(c));
                }
            }); // If Hibernate enhanced is this needed?

        contentMetadataSet.setFlattenable(contentMetadataSetService.isFlattenable(contentMetadataSet));

        var partition = Partition.ofSize(enrichedContentMetadatas, retromasterConfiguration.getMetadataSetImportMetadataBatchSize());
        var count = new AtomicInteger();

        Set<String> signatures = ConcurrentHashMap.newKeySet();
        forkJoinPoolProvider.submitNow(
            () -> IntStream.range(0, partition.size())
                .parallel()
                .forEach(
                    i -> {
                        var contentMetadataBatch = partition.get(i);
                        count.addAndGet(contentMetadataBatch.size());
                        log.info("doContentMetadataSetImport: Importing data, id={}, i={}/{}", contentMetadataSet.getId(), count.get(), enrichedContentMetadatas.size());
                        transactionRequiresNewTemplate.execute(
                            ts -> {
                                contentMetadataRepositoryService.saveAll(contentMetadataBatch);
                                contentMetadataBatch.stream().filter(cm -> cm.getItems() != null).flatMap(cm -> cm.getItems().stream()).map(ContentMetadataItem::getSignature).forEach(signatures::add);
                                return true;
                            });
                    }));

        log.info("doContentMetadataSetImport: Imported. Saving new signatures, id={}, signatureCount={}", contentMetadataSet.getId(), signatures.size());
        var signaturesPartition = Partition.ofSize(new ArrayList<>(signatures), retromasterConfiguration.getMetadataSetImportSignatureBatchSize()); // could be bigger
        forkJoinPoolProvider.submitNow(
            () -> IntStream.range(0, signaturesPartition.size())
                .parallel()
                .forEach(
                    i -> transactionRequiresNewTemplate.execute(
                        ts -> {
                            contentSignatureChangeRepositoryService.saveAll(signaturesPartition.get(i).stream());
                            return true;
                        })));

        if (!selfImport) {
            move(path, storedContentMetadataSetFilePath);
        }

        var lastModifiedTime = getLastModifiedTime(storedContentMetadataSetFilePath);
        contentMetadataSet.setCheckTime(lastModifiedTime);

        return transactionRequiresNewTemplate.execute(
            ts -> {
                var set = contentMetadataSetRepositoryService.getReferenceById(contentMetadataSet.getId());
                set.setCheckTime(lastModifiedTime);
                set.setFlattenable(contentMetadataSetService.isFlattenable(contentMetadataSet));
                return contentMetadataSetRepositoryService.save(set);
            });
    }

    public boolean doContentMetadataSetImportByDirectory(Path path) {
        var count = new AtomicInteger();
        try {
            List<Path> paths;
            try (var s = findPaths(path)) {
                paths = s.filter(contentMetadataSetService::isContentMetadataSetFileSupported).toList();
            }
            if (paths.isEmpty()) {
                log.trace("doContentMetadataSetImportByDirectory: Skip empty, path={}", path);
                return true;
            }

            log.trace("doContentMetadataSetImportByDirectory: Processing, path={}", path);

            applicationEventPublisher.publishEvent(new ContentMetadataSetManageEvent(ContentMetadataSetManageEventType.IMPORT_START, "", 0));

            var fileTime = getFileTimeNow();
            touch(paths, fileTime);

            List<Path> touchedPaths;
            try (var s = findPaths(path)) {
                touchedPaths = s.filter(contentMetadataSetService::isContentMetadataSetFileSupported).toList();
            }
            for (var touchedPath : touchedPaths) {
                try {
                    var isTouched = Files.getLastModifiedTime(touchedPath).equals(fileTime); // Same FS comparison is OK
                    if (isTouched) {
                        log.debug("doContentMetadataSetImportByDirectory: Importing, i={}/{}, path={}", count.incrementAndGet(), touchedPaths.size(), touchedPath);
                        doContentMetadataSetImport(touchedPath);
                    }
                } catch (Exception e) { // NOPMD
                    log.error("doContentMetadataSetImportByDirectory: Failed import, path={}", touchedPath, e);
                    moveContentMetadataSetDatFileToExportError(touchedPath);
                }
            }

            getDirectoriesByEmpty(path).stream().filter(d -> !d.equals(path)).forEach(uConsumer(this::deleteSilent));

            applicationEventPublisher.publishEvent(new ContentMetadataSetManageEvent(ContentMetadataSetManageEventType.IMPORT_SUCCESS, "", count.get()));
            return true;
        } catch (Exception e) { // NOPMD
            log.error("doContentMetadataSetImportByDirectory: Failed overall import, path={}", path, e);
            applicationEventPublisher.publishEvent(new ContentMetadataSetManageEvent(ContentMetadataSetManageEventType.IMPORT_FAIL, e.getMessage(), count.get()));
            return false;
        }
    }

    private ContentMetadataSet doContentMetadataSetRenormalizeData(ContentMetadataSet contentMetadataSet) {
        var enricher = contentMetadataSetEnricherService.getByContentMetadataSet(contentMetadataSet);
        var system = enricher.extractSystem(contentMetadataSet);
        var updatedContentMetadatas = contentMetadataSet.getContentMetadatas().stream().filter(c -> !system.equals(c.getSystem())).peek(d -> enricher.enrichContentMetadata(d, system)).toList();
        contentMetadataRepositoryService.saveAll(updatedContentMetadatas);
        contentSignatureChangeRepositoryService.saveAll(updatedContentMetadatas.stream().flatMap(cm -> cm.getItems().stream()).map(ContentMetadataItem::getSignature));
        return contentMetadataSet;
    }

    @Transactional(readOnly = true)
    public void doContentMetadataSetsReimport(boolean reNormalizeSet, boolean reNormalizeData) {
        applicationEventPublisher.publishEvent(new ContentMetadataSetManageEvent(ContentMetadataSetManageEventType.REIMPORT_START, "", 0));
        try {
            List<Path> paths;
            try (var s = findPaths(contentMetadataVolumeService.getProdPath())) {
                paths = s.filter(contentMetadataSetService::isContentMetadataSetFileSupported).toList();
            }
            var count = new AtomicInteger();
            paths.stream()
                .sorted()
                .forEach(p -> {
                    try {
                        var filename = p.getFileName().toString();
                        var storedContentMetadataSet = contentMetadataSetRepositoryService.findByFilename(filename);
                        var alreadyImported = storedContentMetadataSet.isPresent() && isFileModifiedTimeEquals(p, storedContentMetadataSet.get().getCheckTime());
                        if (alreadyImported) {
                            var contentMetadataSet = storedContentMetadataSet.get();
                            if (reNormalizeSet) {
                                log.debug("doContentMetadataSetsReimport: Re-normalizing set, i={}/{}, filename={}", count.get(), paths.size(), filename);
                                transactionRequiresNewTemplate.execute(
                                    ts -> {
                                        var updatedContentMetadataSet = contentMetadataSetEnricherService.getByContentMetadataSet(contentMetadataSet).reenrichContentMetadataSet(contentMetadataSet);
                                        updatedContentMetadataSet.setFlattenable(contentMetadataSetService.isFlattenable(updatedContentMetadataSet));
                                        return contentMetadataSetRepositoryService.save(updatedContentMetadataSet);
                                    });
                            }
                            if (reNormalizeData) {
                                log.debug("doContentMetadataSetsReimport: Re-normalizing items, i={}/{}, filename={}", count.get(), paths.size(), filename);
                                transactionRequiresNewTemplate.execute(ts -> doContentMetadataSetRenormalizeData(contentMetadataSet));
                            }
                            if (!reNormalizeData && !reNormalizeSet) {
                                log.debug("doContentMetadataSetsReimport: Skipped fresh, i={}/{}, filename={}", count.get(), paths.size(), filename);
                            }
                        } else {
                            log.debug("doContentMetadataSetsReimport: Importing new, i={}/{}, filename={}", count.get(), paths.size(), filename);
                            doContentMetadataSetImport(p);
                        }
                        count.incrementAndGet();
                    } catch (IOException
                             | DataIntegrityViolationException
                             | ExecutionException
                             | InterruptedException e) {
                        log.error("doContentMetadataSetsReimport: Failed re-normalize, filename={}", p, e);
                        try {
                            moveContentMetadataSetDatFileToExportError(p);
                        } catch (IOException e2) {
                            log.error("doContentMetadataSetsReimport: Failed re-normalize error cleanup, filename={}", p, e2);
                        }
                    }
                });
            applicationEventPublisher.publishEvent(new ContentMetadataSetManageEvent(ContentMetadataSetManageEventType.REIMPORT_SUCCESS, "", count.getAndIncrement()));
        } catch (IOException | DataIntegrityViolationException e) {
            applicationEventPublisher.publishEvent(new ContentMetadataSetManageEvent(ContentMetadataSetManageEventType.REIMPORT_FAIL, "", 0));
        }
    }

    private void moveContentMetadataSetDatFileToExportDuplicated(Path path) {
        try {
            if (Files.exists(path)) {
                move(path, contentMetadataVolumeService.getDuplicatedPath().resolve(path.getFileName()));
            }
        } catch (IOException e) {
            log.error("moveContentMetadataSetDatFileToExportDuplicated, path={}", path, e);
        }
    }

    private void moveContentMetadataSetDatFileToExportError(Path path) throws IOException {
        var errorPath = contentMetadataVolumeService.getErrorPath();
        var prodPath = contentMetadataVolumeService.getProdPath(path.getFileName().toString());
        var currentFilepath = Files.exists(path) ? path : prodPath;
        moveToUnique(currentFilepath, errorPath.resolve(path.getFileName()));
    }
}
