package com.tincore.retromaster.service.contentsystem;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.util.StreamTrait;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleContentSystemExtractor extends AbstractContentSystemExtractor implements StreamTrait {

    private final Set<String> systemContains;

    public SimpleContentSystemExtractor(ContentSystem contentSystem) {
        this(contentSystem, new String[]{});
    }

    public SimpleContentSystemExtractor(ContentSystem contentSystem, String... systemContains) {
        super(contentSystem);
        this.systemContains = normalizeSystemContains(Stream.concat(Stream.of(contentSystem.name(), contentSystem.getLabel()), toStream(systemContains)));
    }

    @Override
    public boolean isMatch(ContentMetadata contentMetadata) {
        return matchSystemContains(ContentSystemExtractor.normalizeSystemLabel(contentMetadata.getSystem()), systemContains);
    }

    public boolean isMatchBySystemLabel(String system) {
        return matchSystemContains(system, systemContains);
    }

    private Set<String> normalizeSystemContains(Stream<String> names) {
        return names.map(ContentSystemExtractor::normalizeSystemLabel).collect(Collectors.toSet());
    }
}
