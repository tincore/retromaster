package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentMetadataSet;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

public interface ContentMetadataItemRepositoryService extends JpaRepository<ContentMetadataItem, UUID>, ContentMetadataItemRepositoryServiceExtension {

    @Query("SELECT count(cmi) FROM ContentMetadataItem cmi JOIN ContentMetadata cm ON (cmi.contentMetadata.id = cm.id) WHERE cm.contentMetadataSet.id = :contentMetadataSetId")
    int countAllByContentMetadataSetId(@Param("contentMetadataSetId") UUID contentMetadataSetId);

    @Query("SELECT count(distinct (cmi.name)) FROM ContentMetadataItem cmi JOIN ContentMetadata cm ON (cmi.contentMetadata.id = cm.id) WHERE cm.contentMetadataSet.id = :contentMetadataSetId")
    int countAllByContentMetadataSetIdAndDistinctNames(@Param("contentMetadataSetId") UUID contentMetadataSetId);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM Content_Metadata_item cmi WHERE cmi.content_metadata_id = :id", nativeQuery = true)
    void deleteNativeByContentMetadataId(UUID id);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM Content_Metadata_item cmi WHERE cmi.id = :id", nativeQuery = true)
    void deleteNativeById(UUID id);

    List<ContentMetadataItem> findByContentMetadataId(UUID id);

    @Query("SELECT cmi FROM ContentMetadataItem cmi JOIN ContentMetadata cm ON (cmi.contentMetadata.id = cm.id) WHERE cm.contentMetadataSet.id = :contentMetadataSetId")
    List<ContentMetadataItem> findByContentMetadataSetId(@Param("contentMetadataSetId") UUID contentMetadataSetId);

    @Query("SELECT cmi FROM ContentMetadataItem cmi JOIN ContentMetadata cm ON (cmi.contentMetadata.id = cm.id) WHERE cm.contentMetadataSet.id = :contentMetadataSetId AND cm.complete = '" + ContentMetadata.COMPLETE_TOTAL + "'")
    List<ContentMetadataItem> findByContentMetadataSetIdAndComplete(@Param("contentMetadataSetId") UUID contentMetadataSetId);

    List<ContentMetadataItem> findBySignature(String value);

    List<ContentMetadataItem> findBySignatureIn(Collection<String> signatures);

    @Cacheable(cacheNames = "findContentMetadataItemByStringFileByContentMetadataSetId")
    default Map<String, ContentMetadataItem> findContentMetadataItemByStringFileByContentMetadataSetId(UUID contentMetadataSetId) {
        return findByContentMetadataSetId(contentMetadataSetId).stream().collect(Collectors.toMap(ContentMetadataItem::toStringFile, s -> s));
    }

    @Cacheable(cacheNames = "findContentMetadataItemByStringFileByContentMetadataSetIdAndCompleteTotal")
    default Map<String, ContentMetadataItem> findContentMetadataItemByStringFileByContentMetadataSetIdAndCompleteTotal(UUID contentMetadataSetId) {
        return findByContentMetadataSetIdAndComplete(contentMetadataSetId).stream().collect(Collectors.toMap(ContentMetadataItem::toStringFile, s -> s));
    }

    @Query("SELECT cmi.id FROM ContentMetadataItem cmi JOIN ContentMetadata cm ON (cmi.contentMetadata.id = cm.id) WHERE cm.contentMetadataSet = :contentMetadataSet")
    List<UUID> findContentMetadataItemIdsByContentMetadataSet(@Param("contentMetadataSet") ContentMetadataSet contentMetadataSet);

    Optional<ContentMetadataItem> findFirstByContentMetadataId(UUID id);

    Optional<ContentMetadataItem> findFirstByContentMetadataIdAndName(UUID id, String name);

    int countAllByContentMetadataIdAndNameStartsWith(UUID id, String name);

}
