package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class PackageSnapService {

    private static final int CACHE_MINUTES = 10;

    private final ProcessExecutorService processExecutorService;

    private String listOutput;
    private LocalDateTime listOutputTimestamp;

    public boolean isInstalled(String applicationId) {
        if (listOutputTimestamp == null || listOutputTimestamp.isBefore(LocalDateTime.now().minusMinutes(CACHE_MINUTES))) {
            var sb = new StringBuilder();
            try {
                processExecutorService.exec("snap list", UUID.randomUUID(), sb::append);

                listOutput = sb.toString();
                listOutputTimestamp = LocalDateTime.now();
            } catch (IOException | InterruptedException e) {
                log.error("Error running snap. Is it installed?", e);
                listOutputTimestamp = LocalDateTime.now();
                return false;
            }
        }

        try (var scan = new Scanner(listOutput)) {
            while (scan.hasNextLine()) {
                var line = scan.nextLine();
                if (line.contains(applicationId)) {
                    return true;
                }
            }
        }
        return false;
    }
}
