package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import static com.tincore.retromaster.domain.ContentExtension.*;
import static com.tincore.retromaster.domain.ContentSystem.*;

@Component
class Retroarch2ContentExecutorFactory { // NOPMD Cyclomatic BS

    private final RetromasterConfiguration.RetroarchExecuteConfiguration retroarchExecuteConfiguration;

    @Autowired
    Retroarch2ContentExecutorFactory(RetromasterConfiguration retromasterConfiguration) {
        this.retroarchExecuteConfiguration = retromasterConfiguration.getExecute().getRetroarch();
    }

    @Bean
    public RetroarchContentExecutor retroarchMGbaContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mgba").contentSystems(NINTENDO_GAMEBOY_ADVANCE).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenGbaContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mednafen_gba").contentSystems(NINTENDO_GAMEBOY_ADVANCE).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenLynxContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mednafen_lynx").contentSystems(ATARI_LYNX).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenNgpContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("mednafen_ngp").contentSystems(SNK_NEOGEO_POCKET, SNK_NEOGEO_POCKET_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenPceFastContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mednafen_pce_fast").contentSystems(NEC_TURBOGRAFX).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenPcfxContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mednafen_pcfx").contentSystems(NEC_PCFX).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenPsxContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("mednafen_psx")
                .contentSystems(SONY_PLAYSTATION)
                .contentExtensions(cue, toc, m3u, ccd, exe, pbp, chd)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenPsxHwContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("mednafen_psx_hw")
                .contentSystems(SONY_PLAYSTATION)
                .contentExtensions(cue, toc, m3u, ccd, exe, pbp, chd)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenSaturnContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("mednafen_saturn").contentSystems(SEGA_SATURN).contentExtensions(cue, toc, m3u, ccd, chd).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenSnesContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mednafen_snes").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenSupergrafxContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mednafen_supergrafx").contentSystems(NEC_SUPERGRAFX).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenVBContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mednafen_vb").contentSystems(NINTENDO_VIRTUAL_BOY).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMednafenWonderSwanContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("mednafen_wswan").contentSystems(BANDAI_WONDERSWAN, BANDAI_WONDERSWAN_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMelonDSContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("melonds").contentSystems(NINTENDO_DS).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMesenContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mesen").contentSystems(NINTENDO_NES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMesenSContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("mesen-s").contentSystems(NINTENDO_SNES, NINTENDO_GAMEBOY, NINTENDO_GAMEBOY_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMeteorContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("meteor").contentSystems(NINTENDO_GAMEBOY_ADVANCE).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMupen64PlusContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mupen64plus").contentSystems(NINTENDO_64).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchMupen64PlusNextContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("mupen64plus_next").contentSystems(NINTENDO_64).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchNekoP2ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("nekop2").contentSystems(NEC_PC9801, NEC_PC9821).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchNeoCdContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("neocd").contentSystems(SNK_NEOGEO_CD).contentExtensions(cue, chd).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchNestopiaContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("nestopia").contentSystems(NINTENDO_NES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchNp2kaiContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("np2kai").contentSystems(NEC_PC9801, NEC_PC9821).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchO2EmContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("o2em").contentSystems(PHILIPS_VIDEOPAC, MAGNAVOX_ODYSSEY_2).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchOperaontentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("opera").contentSystems(PANASONIC_3DO).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchParallelN64ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("parrallel_n64").contentSystems(NINTENDO_64).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchPcemRearmedContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("pcem").contentSystems(IBM_PC).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchPcsx1ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("pcsx1").contentSystems(SONY_PLAYSTATION).multipartCapable(true).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchPcsxRearmedContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("pcsx_rearmed")
                .contentSystems(SONY_PLAYSTATION)
                .contentExtensions(bin, cue, img, mdf, toc, m3u, cbn, ccd, exe, pbp, chd)
                .multipartCapable(true)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchPicodiveContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("picodrive").contentSystems(SEGA_MEGADRIVE, SEGA_MASTERSYSTEM, SEGA_32X, SEGA_CD, SEGA_PICO).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchPlayContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("play").contentSystems(SONY_PLAYSTATION_2).contentExtensions(iso, isz, cso, bin, elf).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchPpssppContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("ppsspp")
                .contentSystems(SONY_PLAYSTATION_PORTABLE)
                .contentExtensions(iso, cso, ciso, pbp, prx, elf)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchProsystemContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("prosystem").contentSystems(ATARI_7800).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchPuaeContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("puae").contentSystems(COMMODORE_AMIGA, COMMODORE_AMIGA_1200, COMMODORE_CDTV, COMMODORE_CD32).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchPx68ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("px68k").contentSystems(SHARP_X68000).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchQuasi88ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("quasi88").contentSystems(NEC_PC8001).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchQuickNesContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("quicknes").contentSystems(NINTENDO_NES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchRaceContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("race").contentSystems(SNK_NEOGEO_POCKET, SNK_NEOGEO_POCKET_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchRedreamContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("redream").contentSystems(SEGA_DREAMCAST, SEGA_NAOMI).contentExtensions(chd, cdi, gdi).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchReicastContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("reicast")
                .contentSystems(SEGA_DREAMCAST, SEGA_NAOMI)
                .contentExtensions(chd, cdi, iso, elf, bin, cue, gdi)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchReicastWinCeContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder()
                .coreName("reicast_wince")
                .contentSystems(SEGA_DREAMCAST)
                .contentExtensions(chd, cdi, iso, elf, bin, cue, gdi)
                .retroarchExecuteConfiguration(retroarchExecuteConfiguration)
                .build());
    }

    @Bean
    public RetroarchContentExecutor retroarchSameboyContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("sameboy").contentSystems(NINTENDO_GAMEBOY, NINTENDO_GAMEBOY_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchSmsPlusContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("smsplus").contentSystems(SEGA_MASTERSYSTEM, SEGA_GAMEGEAR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchSnes9x2002ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("snes9x2002").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchSnes9x2005ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("snes9x2005").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchSnes9x2005PlusContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("snes9x2005_plus").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchSnes9x2010ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("snes9x2010").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchSnes9xContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("snes9x").contentSystems(NINTENDO_SNES).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchStella2014ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("stella2014").contentSystems(ATARI_2600).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchStellaContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("stella").contentSystems(ATARI_2600).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchTGBDualContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("tgbdual").contentSystems(NINTENDO_GAMEBOY, NINTENDO_GAMEBOY_COLOR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchTheodoreContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("theodore").contentSystems(THOMSON_MO5, THOMSON_MO6, THOMSON_TO7, THOMSON_TO8, THOMSON_TO9).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchUzemContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("uzem").contentSystems(UZEBOX).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchVbaMContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vbam").contentSystems(NINTENDO_GAMEBOY_ADVANCE).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchVbaNextContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vba_next").contentSystems(NINTENDO_GAMEBOY_ADVANCE).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchVecXContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vecx").contentSystems(GCE_VECTREX).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchVemulatorContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vemulator").contentSystems(SEGA_VMS).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchViceX128ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vice_x128").contentSystems(COMMODORE_C128).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchViceX64ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vice_x64").contentSystems(COMMODORE_C64).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchViceX64ScContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vice_x64sc").contentSystems(COMMODORE_C64).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchViceXCbm2ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vice_xcbm2").contentSystems(COMMODORE_CBM_II).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchViceXPetContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vice_xpet").contentSystems(COMMODORE_PET).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchViceXPlus4ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vice_xplus4").contentSystems(COMMODORE_C16_C116_PLUS_4).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchViceXVicContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("vice_xvic").contentSystems(COMMODORE_VIC20).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchVirtualJaguarContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("virtualjaguar").contentSystems(ATARI_JAGUAR).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchX1ContentExecutor() {
        return new RetroarchContentExecutor(RetroarchContentExecutor.Configuration.builder().coreName("x1").contentSystems(SHARP_X1).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchYabasanshiroContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("yabasanshiro").contentSystems(SEGA_SATURN).contentExtensions(cue, iso, mds, ccd).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }

    @Bean
    public RetroarchContentExecutor retroarchYabauseContentExecutor() {
        return new RetroarchContentExecutor(
            RetroarchContentExecutor.Configuration.builder().coreName("yabause").contentSystems(SEGA_SATURN).contentExtensions(cue, iso, bin).retroarchExecuteConfiguration(retroarchExecuteConfiguration).build());
    }
}
