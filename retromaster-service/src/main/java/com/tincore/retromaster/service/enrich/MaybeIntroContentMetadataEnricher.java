package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.retromaster.domain.ContentRegion;
import com.tincore.util.StreamTrait;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.TreeSet;

@Service
public class MaybeIntroContentMetadataEnricher implements ContentMetadataEnricher, StreamTrait {
    public static final String TAG_UNLICENSED = "Unlicensed";
    private static final int LENGTH_MAX = 254;

    @Override
    public void enrich(String title, ContentMetadata contentMetadata) {
        contentMetadata.setTitle(title);

        Set<String> tagExtra = new TreeSet<>();
        Set<String> regions = new TreeSet<>();
        Set<String> translatedDatas = new TreeSet<>();
        contentMetadata.getItems()
            .stream()
            .map(ContentMetadataItem::getName)
            .forEach(
                n -> {
                    if (!StringUtils.startsWithIgnoreCase(n, contentMetadata.getTitle())) {
                        return;
                    }

                    var tokens = StringUtils.substringsBetween(n, "(", ")");
                    if (tokens != null) {
                        for (var token : tokens) {
                            var region = ContentRegion.MAP.getOrDefault(token, null);
                            if (region == null) {
                                if ("Unl".equals(token)) {
                                    tagExtra.add(TAG_UNLICENSED);
                                    // tagExtra.add(token);
                                }
                            } else {
                                regions.add(region);
                            }
                        }
                    }

                    var tokens2 = StringUtils.substringsBetween(n, "[", "]");
                    if (tokens2 != null) {
                        for (var token : tokens2) {
                            if (StringUtils.startsWithIgnoreCase(token, "T-")) {
                                translatedDatas.add(token.substring(2));
                            } else {
                                tagExtra.add(token);
                            }
                        }
                    }
                });
        contentMetadata.setSource(ContentMetadataSource.MAYBEINTRO.name());
        contentMetadata.setRegion(String.join(",", regions));
        contentMetadata.setTagsExtra(StringUtils.left(String.join(",", tagExtra), LENGTH_MAX));
        contentMetadata.setTranslated(!translatedDatas.isEmpty());
        contentMetadata.setTranslatedData(StringUtils.left(String.join(",", translatedDatas), LENGTH_MAX));
    }
}
