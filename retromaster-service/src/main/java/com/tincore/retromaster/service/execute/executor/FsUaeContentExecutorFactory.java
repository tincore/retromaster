package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import static com.tincore.retromaster.domain.ContentSystem.*;

@Component
public class FsUaeContentExecutorFactory {

    private final RetromasterConfiguration.FsUaeExecuteConfiguration fsUaeExecuteConfiguration;

    @Autowired
    public FsUaeContentExecutorFactory(RetromasterConfiguration retromasterConfiguration) {
        this.fsUaeExecuteConfiguration = retromasterConfiguration.getExecute().getFsUae();
    }

    @Bean
    public FsUaeContentExecutor fsUaeA1200ContentExecutor() {
        return new FsUaeContentExecutor(fsUaeExecuteConfiguration, COMMODORE_AMIGA_1200, COMMODORE_AMIGA);
    }

    @Bean
    public FsUaeContentExecutor fsUaeA500ContentExecutor() {
        return new FsUaeContentExecutor(fsUaeExecuteConfiguration, COMMODORE_AMIGA);
    }

    @Bean
    public FsUaeContentExecutor fsUaeCd32ContentExecutor() {
        return new FsUaeContentExecutor(fsUaeExecuteConfiguration, COMMODORE_CD32);
    }

    @Bean
    public FsUaeContentExecutor fsUaeCdtvContentExecutor() {
        return new FsUaeContentExecutor(fsUaeExecuteConfiguration, COMMODORE_CDTV);
    }
}
