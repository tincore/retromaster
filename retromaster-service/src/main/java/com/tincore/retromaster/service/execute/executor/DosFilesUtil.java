package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.FilenameUtils;

import java.util.Set;

public class DosFilesUtil {

    public static final int DOSFILENAME_LENGTH = 8;
    public static final int DOS_FILENAME_SHORTENED_LENGTH = 6;

    public static String toDosName(String fileName, Set<String> fileNames) {
        var baseName = FilenameUtils.getBaseName(fileName);
        if (baseName.length() <= DOSFILENAME_LENGTH) {
            return fileName;
        }

        var shortName = baseName.replaceAll(" ", "").substring(0, DOSFILENAME_LENGTH);
        var count = 0;
        while (fileNames.contains(shortName)) {
            count++;
            shortName = baseName.substring(0, DOS_FILENAME_SHORTENED_LENGTH) + "~" + count;
        }

        var extension = FilenameUtils.getExtension(fileName);
        var fullPath = FilenameUtils.getFullPath(fileName);
        // Not considering subpaths
        return (fullPath + shortName + FilenameUtils.EXTENSION_SEPARATOR + extension).toUpperCase();
    }
}
