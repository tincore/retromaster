package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataIdSignature;
import com.tincore.retromaster.domain.ContentMetadataSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ContentMetadataRepositoryService extends JpaRepository<ContentMetadata, UUID>, QuerydslPredicateExecutor<ContentMetadata> {
    int countAllByContentMetadataSetId(UUID contentMetadataSetId);

    // TOO SLOW APPROACH FOR MASSIVE DELETION
    // @Modifying
    // @Transactional
    // @Query("DELETE FROM ContentMetadata c WHERE c.contentMetadataSet = ?1")
    // int deleteByContentMetadataSetEquals(ContentMetadataSet contentMetadataSet);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM Content_Metadata cm WHERE cm.id = :id", nativeQuery = true)
    void deleteNativeById(UUID id);

    @Query("SELECT DISTINCT new com.tincore.retromaster.domain.ContentMetadataIdSignature(cm.id, cmi.signature) FROM ContentMetadataItem cmi JOIN ContentMetadata cm ON (cmi.contentMetadata.id = cm.id)  WHERE cmi.signature IN :signatures")
    List<ContentMetadataIdSignature> findByContentMetadataIdSingaturesBySignatureIn(Collection<String> signatures);

    Optional<ContentMetadata> findByContentMetadataSetAndName(ContentMetadataSet contentMetadataSet, String name);

    Optional<ContentMetadata> findByContentMetadataSetIdAndName(UUID contentMetadataSetId, String name);

    List<ContentMetadata> findByContentMetadataSetEquals(ContentMetadataSet contentMetadataSet);

    List<ContentMetadata> findByContentMetadataSet_IdEquals(UUID id);

    List<ContentMetadata> findByTitleAndPublisherAndSystemAndSource(String title, String publisher, String system, String source);

    @Query("SELECT cm.id FROM ContentMetadata cm WHERE cm.contentMetadataSet = :contentMetadataSet")
    List<UUID> findContentMetadataIdsByContentMetadataSet(ContentMetadataSet contentMetadataSet);
}
