package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.engine.search.common.BooleanOperator;
import org.hibernate.search.engine.search.sort.dsl.SearchSortFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tincore.retromaster.domain.Content.*;

@Slf4j
@Transactional(readOnly = true)
@Repository
public class ContentSearchService extends AbstractSearchService<Content> {

    public ContentSearchService() {
        super(Content.class);
    }

    public List<Content> searchByFieldsSimpleQueryString(String text, long top, long limit) {
        // Explanation a = getSearchSession().search(getType())
        // .extension(LuceneExtension.get())
        // .where(f -> f.simpleQueryString().fields("directoryPath", "fileName", "fileEntry").matching(text))
        // .failAfter(getTimeoutMillisByExpectedMaxElementCount(limit), TimeUnit.MILLISECONDS)
        // .toQuery().explain(EXPLANATION_INDEX);

        var result = getSearchSession()
            .search(getType())
            .where(f -> f.simpleQueryString().fields(FIELD_DIRECTORY_PATH, FIELD_FILE_NAME, FIELD_FILE_ENTRY).matching(text).defaultOperator(BooleanOperator.AND))
            .sort(SearchSortFactory::score)
            .failAfter(getTimeoutMillisByExpectedMaxElementCount(limit), TimeUnit.MILLISECONDS)
            .fetch((int) top, (int) limit);
        // int totalResultCount = result.total().hitCount();
        return result.hits();
        //
        // FullTextQuery query =
        // getSearchSession()
        // .createFullTextQuery(
        // b.simpleQueryString()
        // .onFields("directoryPath", "fileName", "fileEntry")
        // .withAndAsDefaultOperator() //
        // .matching(text)
        // .createQuery(),
        // getType())
        // .setTimeout(getTimeoutMillisByExpectedMaxElementCount(limit), TimeUnit.MILLISECONDS)
        // .setFirstResult((int) top)
        // .setMaxResults((int) limit)
        // .setSort(new Sort(SortField.FIELD_SCORE));
        //
        // @SuppressWarnings("unchecked")
        // List<Content> result = (List<Content>) query.getResultList();
        // // int totalResultCount = fullTextQuery.getResultSize();
        // // doExplained(query, top, limit);
        // return result;
    }
}
