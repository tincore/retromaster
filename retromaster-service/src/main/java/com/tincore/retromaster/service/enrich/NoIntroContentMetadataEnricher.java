package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentRegion;
import com.tincore.util.StreamTrait;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class NoIntroContentMetadataEnricher implements ContentMetadataEnricher, StreamTrait {

    @Override
    public void enrich(String title, ContentMetadata contentMetadata) { // NOPMD Complex
        contentMetadata.setTitle(title);

        var name = contentMetadata.getItems().stream()
            .map(ContentMetadataItem::getName)
            .filter(n -> StringUtils.startsWithIgnoreCase(n, title))
            .findFirst()
            .orElse(contentMetadata.getName());

        if (name.equals(contentMetadata.getDescription())) {
            contentMetadata.setDescription("");
        }

        List<String> tokensExtra = new ArrayList<>();

        if (name.indexOf('(') != -1) {
            var m = PATTERN_PARENTHESES.matcher(name);
            if (m.find()) {
                var region = m.group(1);
                var regionMapped = toStream(region.split(",")).map(r -> ContentRegion.MAP.getOrDefault(r.trim(), r)).collect(Collectors.joining(","));
                contentMetadata.setRegion(regionMapped);
            }
            Set<String> parenthesesAttributes = new HashSet<>();
            while (m.find()) {
                var value = m.group(1);
                if ("Alt".equals(value)) {
                    contentMetadata.setAlternate(true);
                } else if ("Pirate".equals(value)) {
                    contentMetadata.setPirated(true);
                } else {
                    parenthesesAttributes.add(value);
                }
            }
            tokensExtra.addAll(parenthesesAttributes);
        }

        if (name.indexOf('[') != -1) {
            var m = PATTERN_BRACKETS.matcher(name);
            Set<String> bracketsAttributes = new HashSet<>();
            while (m.find()) {
                var group = m.group(1);
                if (StringUtils.startsWith(group, "T-")) {
                    contentMetadata.setTranslated(true);
                    contentMetadata.setTranslatedData(group);
                } else {
                    bracketsAttributes.add(group);
                }
            }
            tokensExtra.addAll(bracketsAttributes);
        }

        if (!tokensExtra.isEmpty()) {
            contentMetadata.setTagsExtra(String.join(",", tokensExtra));
        }
    }
}
