package com.tincore.retromaster.service.contentsystem;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.util.StreamTrait;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentSystem.*;

public class ContentSystemExtractors implements StreamTrait {

    private final List<ContentSystemExtractor> extractors;

    @SuppressWarnings("checkstyle:methodlength")
    public ContentSystemExtractors() {

        // ORDER MATTERS. More specific after
        var extractorsPre = Stream.of(
                new SimpleContentSystemExtractor(ADC_SUPER_SIX),

                new SimpleContentSystemExtractor(APF_IMAGINATION_MACHINE),

                new SimpleContentSystemExtractor(ACORN_SYSTEM_1, "Acorn - System 1"),
                new SimpleContentSystemExtractor(ACORN_ARCHIMEDES, "Archimedes"),
                new SimpleContentSystemExtractor(ACORN_ATOM),
                new SimpleContentSystemExtractor(ACORN_BBC, "BBC"),
                new SimpleContentSystemExtractor(ACORN_ELECTRON, "Electron"),
                new SimpleContentSystemExtractor(ACORN_FILESTORE_E01),
                new SimpleContentSystemExtractor(ACORN_RISC_PC),
                new SimpleContentSystemExtractor(ACORN_RISCOS_FLASH_MEDIA),
                new SimpleContentSystemExtractor(ACORN),

                new SimpleContentSystemExtractor(ALTOS_ACS_X86, "Altos Computer Systems ACS-186, 586, 686 & 986", "ACS-186"),
                new SimpleContentSystemExtractor(AMPRO_LITTLE_BOARD, "Ampro Computers Little Board", "AMPRO Computers LittleBoard Z80"),
                new SimpleContentSystemExtractor(AMSTRAD_CPCP),
                new SimpleContentSystemExtractor(AMSTRAD_NC100),
                new SimpleContentSystemExtractor(AMSTRAD_NC200),
                new SimpleContentSystemExtractor(AMSTRAD_CPC, "Amstrad CPC"),
                new SimpleContentSystemExtractor(AMSTRAD_PCW),
                new SimpleContentSystemExtractor(AMSTRAD_PC1512, "PC1512"),
                new SimpleContentSystemExtractor(AMSTRAD_PC1640, "PC1640"),
                new SimpleContentSystemExtractor(AMSTRAD_GX4000, "GX4000"),
                new SimpleContentSystemExtractor(AMSTRAD_MEGA_PC),

                new SimpleContentSystemExtractor(ATARI_2600),
                new SimpleContentSystemExtractor(ATARI_5200),
                new SimpleContentSystemExtractor(ATARI_7800),
                new SimpleContentSystemExtractor(ATARI_8BIT, "Atari 400/800/XE/XL/XEGS", "Atari 8Bit", "Atari - 8-Bit Family", "Atari 400", "Atari 800"),
                new SimpleContentSystemExtractor(ATARI_JAGUAR_CD),
                new SimpleContentSystemExtractor(ATARI_JAGUAR),
                new SimpleContentSystemExtractor(ATARI_LYNX),
                new SimpleContentSystemExtractor(ATARI_MEGASTE),
                new SimpleContentSystemExtractor(ATARI_MEGAST),
                new SimpleContentSystemExtractor(ATARI_STE),
                new SimpleContentSystemExtractor(ATARI_ST),
                new SimpleContentSystemExtractor(ATARI_TT),
                new SimpleContentSystemExtractor(ATARI_FALCON),
                new SimpleContentSystemExtractor(ATARI),

                new SimpleContentSystemExtractor(BANDAI_WONDERSWAN_COLOR, "WonderSwan Color"),
                new SimpleContentSystemExtractor(BANDAI_WONDERSWAN, "WonderSwan"),
                new SimpleContentSystemExtractor(BANDAI_SUPER_VISION_8000),
                new SimpleContentSystemExtractor(BANDAI_PIPPIN),
                new SimpleContentSystemExtractor(BANDAI_PLAYDIA),
                new SimpleContentSystemExtractor(BANDAI_SUPER_NOTE_CLUB),
                new SimpleContentSystemExtractor(BANDAI_DESIGN_MASTER_DENSHI_MANGAJUKU),

                new SimpleContentSystemExtractor(BALLY_ASTROCADE, "Bally Professional Arcade and Astrocade", "Bally Professional", "Astrocade"),
                new SimpleContentSystemExtractor(CAPCOM_CPS_III, "Capcom CPS III", "Capcom CP System III"),
                new SimpleContentSystemExtractor(COLECO_COLECOVISION_ADAM, "ColecoVision ADAM"),
                new SimpleContentSystemExtractor(COLECO_COLECOVISION, "ColecoVision"),

                new SimpleContentSystemExtractor(COMMODORE_C64DTV, "C64DTV"),
                new SimpleContentSystemExtractor(COMMODORE_C64, "Commodore 64", "Commodore C64", "C64"),
                new SimpleContentSystemExtractor(COMMODORE_C65),
                new SimpleContentSystemExtractor(COMMODORE_C128, "Commodore 128", "Commodore C128"),
                new SimpleContentSystemExtractor(COMMODORE_C16_C116_PLUS_4, "Commodore C16, C116 & Plus-4", "Plus-4", "Plus4"),
                new SimpleContentSystemExtractor(COMMODORE_VIC20, "Commodore VIC20", "Vic20"),
                new SimpleContentSystemExtractor(COMMODORE_VIC10),
                new SimpleContentSystemExtractor(COMMODORE_CD32, "Commodore Amiga CD32"),
                new SimpleContentSystemExtractor(COMMODORE_CDTV, "Commodore Amiga CDTV", "CDTV"),
                new SimpleContentSystemExtractor(COMMODORE_AMIGA_1200),
                new SimpleContentSystemExtractor(COMMODORE_AMIGA, "Commodore Amiga", "AMIGA"),
                new SimpleContentSystemExtractor(COMMODORE_VIC20),
                new SimpleContentSystemExtractor(COMMODORE_CBM_II),
                new SimpleContentSystemExtractor(COMMODORE_MAX),
                new SimpleContentSystemExtractor(COMMODORE_SUPERPET),
                new SimpleContentSystemExtractor(COMMODORE_PET),
                new SimpleContentSystemExtractor(COMMODORE_8096),
                new SimpleContentSystemExtractor(COMMODORE_8296),
                new SimpleContentSystemExtractor(COMMODORE_BX256_80HP),
                new SimpleContentSystemExtractor(COMMODORE_P500),
                new SimpleContentSystemExtractor(COMMODORE),

                new SimpleContentSystemExtractor(CYBIKO_XTREME),
                new SimpleContentSystemExtractor(CYBIKO_CYBIKO),

                new SimpleContentSystemExtractor(CASIO_LOOPY, "Loopy"),
                new SimpleContentSystemExtractor(CASIO_PB_1000),
                new SimpleContentSystemExtractor(CASIO_PB_2000C),
                new SimpleContentSystemExtractor(CASIO_PV_1000),
                new SimpleContentSystemExtractor(CASIO_PV_2000),
                new SimpleContentSystemExtractor(CASIO_CFX_9850),
                new SimpleContentSystemExtractor(CASIO_FP_1X00),

                new SimpleContentSystemExtractor(DRAGON_DATA_ALPHA, "Dragon Data Dragon Alpha"),
                new SimpleContentSystemExtractor(DRAGON_DATA_BETA, "Dragon Data Dragon Beta"),
                new SimpleContentSystemExtractor(ENTEX_ADVENTURE_VISION, "Entex Adventure Vision", "Adventure Vision", "AdventureVision"),
                new SimpleContentSystemExtractor(EXELVISION_MULTI),
                new SimpleContentSystemExtractor(EXELVISION_EXETEL),
                new SimpleContentSystemExtractor(EXELVISION_EXL100),
                new SimpleContentSystemExtractor(FAIRCHILD_CHANNEL_F, "Fairchild Channel F", "Channel-F", "Channel F", "ChannelF"),

                new SimpleContentSystemExtractor(FRANKLIN_ACE_500),
                new SimpleContentSystemExtractor(FRANKLIN_ACE_2000),
                new SimpleContentSystemExtractor(FRANKLIN_ACE_1000),
                new SimpleContentSystemExtractor(FRANKLIN_ACE_100),

                new SimpleContentSystemExtractor(FUJITSU_FMR50, "Fujitsu FM-R50"),
                new SimpleContentSystemExtractor(FUJITSU_FMTOWNS),
                new SimpleContentSystemExtractor(FUJITSU_FM77AV, "Fujitsu FM77-AV", "Fujitsu FM-77AV", "Fujitsu - FM-77AV", "Fujitsu - FM77AV"),
                new SimpleContentSystemExtractor(FUJITSU_FM7, "Fujitsu FM-7", "Fujitsu - FM-7", "Fujitsu - FM7"),
                new SimpleContentSystemExtractor(FUJITSU_FACOM_9450, "Fujitsu - Facom 9450", "Fujitsu Facom 9450", "Facom 9450"),
                new SimpleContentSystemExtractor(FUJITSU_FACOM),

                new SimpleContentSystemExtractor(FUNWORLD_PHOTOPLAY, "funworld - Photo Play"),
                new SimpleContentSystemExtractor(FUNTECH_SUPERACAN, "Funtech"),

                new SimpleContentSystemExtractor(GALAKSIJA_PLUS, "Galaksija Galaksija Plus"),
                new SimpleContentSystemExtractor(GALAKSIJA_GALAKSIJA, "Galaksija Galaksija"),

                new SimpleContentSystemExtractor(GCE_VECTREX, "GCE Vectrex", "Vectrex"),

                new SimpleContentSystemExtractor(GAMEPARK_GP32, "Game Park", "Ganepark"),

                new SimpleContentSystemExtractor(HAWTHORNE_TECHNOLOGY_HT68K, "Hawthorne Technologies HT68K"),
                new SimpleContentSystemExtractor(HAWTHORNE_TECHNOLOGY_TINYGIANT, "Hawthorne Technologies TinyGiant", "Hawthorne Technologies Tiny Giant"),

                new SimpleContentSystemExtractor(HITACHI_S1),
                new SimpleContentSystemExtractor(HITACHI_BASIC_MASTER_JR_LEVEL3, "Hitachi Basic Master Jr. Level 3"),
                new SimpleContentSystemExtractor(HITACHI_BASIC_MASTER_JR_LEVEL2, "Hitachi Basic Master Jr. Level 2"),
                new SimpleContentSystemExtractor(HITACHI_BASIC_MASTER_JR, "Hitachi Basic Master Jr."),

                new SimpleContentSystemExtractor(MICRONIQUE_HECTOR),

                new SimpleContentSystemExtractor(IQUE_PLAYER),
                new SimpleContentSystemExtractor(IQUE),

                new SimpleContentSystemExtractor(JUPITER_CANTAB_ACE, "Jupiter Cantab Ace", "Jupiter"),

                new SimpleContentSystemExtractor(LEAPFROG_LEAPSTER_LEARNING_GAMING_SYSTEM),
                new SimpleContentSystemExtractor(LEAPFROG_LEAPSTER),

                new SimpleContentSystemExtractor(LIBRASCOPE_LGP30, "Librascope LGP-30"),

                new SimpleContentSystemExtractor(MAGNAVOX_ODYSSEY_2, "Magnavox Odyssey 2", "Odyssey2"),

                new SimpleContentSystemExtractor(MATTEL_AQUARIUS, "Mattel Aquarius", "Aquarius"),
                new SimpleContentSystemExtractor(MATTEL_INTELLIVISION_ECS, "Mattel Intellivision ECS"),
                new SimpleContentSystemExtractor(MATTEL_INTELLIVISION, "Mattel Intellivision", "Intellivision"),

                new SimpleContentSystemExtractor(MATSUSHITA_JR100, "Matsushita JR-100", "Matsushita JR100"),
                new SimpleContentSystemExtractor(MATSUSHITA_JR200, "Matsushita JR-200", "Matsushita JR200"),
                new SimpleContentSystemExtractor(MATSUSHITA_NATIONAL_JR200, "Matsushita National JR 200", "Matsushita National JR200"),

                new SimpleContentSystemExtractor(MICRONIQUE_HECTOR),

                new SimpleContentSystemExtractor(NAMCO_TRIFORCE, "Namco Triforce", "Arcade Namco / Sega / Nintendo Triforce", "Arcade Namco Sega Nintendo Triforce"),

                new SimpleContentSystemExtractor(MGT_SAM_COUPE, "Miles Gordon Technology SAM", "SAM Coup"),

                new SimpleContentSystemExtractor(MICROSOFT_XBOX_360),
                new SimpleContentSystemExtractor(MICROSOFT_XBOX),
                new SimpleContentSystemExtractor(MICROSOFT_MSX_2P, "MSX 2+", "MSX2+"),
                new SimpleContentSystemExtractor(MICROSOFT_MSX_2, "MSX 2", "MSX2"),
                new SimpleContentSystemExtractor(MICROSOFT_MSX_TURBO_R, "MSX Turbo R", "TurboR"),
                new SimpleContentSystemExtractor(MICROSOFT_MSX_1, "MSX"),

                new SimpleContentSystemExtractor(NEC_PCFXGA, "FXGA"),
                new SimpleContentSystemExtractor(NEC_PCFX, "NEC PC FX"),
                new SimpleContentSystemExtractor(NEC_PC8001, "NEC PC-8001", "PC-8001"),
                new SimpleContentSystemExtractor(NEC_PC88VA, "NEC PC-88VA", "PC-88VA"),
                new SimpleContentSystemExtractor(NEC_PC6001, "NEC PC-6001", "PC-6001"),
                new SimpleContentSystemExtractor(NEC_PC8201, "NEC PC-8201", "PC-8201"),
                new SimpleContentSystemExtractor(NEC_PC8801, "NEC PC-88 SERIES", "PC-8801"),
                new SimpleContentSystemExtractor(NEC_PC9821, "NEC PC-9821", "PC-9821"),
                new SimpleContentSystemExtractor(NEC_PC9801, "NEC PC-98 SERIES", "NEC PC-98", "PC-9801"),
//
                new SimpleContentSystemExtractor(NINTENDO_KIOSK_VIDEO_COMPACT_FLASH),
                new SimpleContentSystemExtractor(NINTENDO_MARIO_NO_PHOTOPI_SMARTMEDIA),
                new SimpleContentSystemExtractor(NINTENDO_WALLPAPERS),

                new SimpleContentSystemExtractor(NINTENDO_POKEMON_MINI),
                new SimpleContentSystemExtractor(NINTENDO_SUFAMI_TURBO),
                new SimpleContentSystemExtractor(NINTENDO_GAMECUBE),
                new SimpleContentSystemExtractor(NINTENDO_SATELLAVIEW, "Satellaview"),
                new SimpleContentSystemExtractor(NINTENDO_PLAY_YAN),
                new SimpleContentSystemExtractor(NINTENDO_FAMILY_COMPUTER_DISK_SYSTEM, "Nintendo Family Disk System", "Nintendo Famicom Disk System", "Nintendo FDS"),
                new SimpleContentSystemExtractor(NINTENDO_WIIU, "Nintendo Wii U"),
                new SimpleContentSystemExtractor(NINTENDO_WII),
                new SimpleContentSystemExtractor(NINTENDO_64DD, "Nintendo 64DD"),
                new SimpleContentSystemExtractor(NINTENDO_64),
                new SimpleContentSystemExtractor(NINTENDO_DSI, "Nintendo DSI"),
                new SimpleContentSystemExtractor(NINTENDO_DS, "Nintendo DS"),
                new SimpleContentSystemExtractor(NINTENDO_3DS_NEW),
                new SimpleContentSystemExtractor(NINTENDO_3DS),
                new SimpleContentSystemExtractor(NINTENDO_GAMEBOY_POCKET, "Game Boy Pocket"),
                new SimpleContentSystemExtractor(NINTENDO_GAMEBOY_ADVANCE, "Game Boy Advance"),
                new SimpleContentSystemExtractor(NINTENDO_GAMEBOY_COLOR, "Game Boy Color"),
                new SimpleContentSystemExtractor(NINTENDO_GAMEBOY, "Nintendo GB", "Game Boy"),
                new SimpleContentSystemExtractor(NINTENDO_SNES, "Super Famicom", "Super Nintendo Entertainment System", "SNES"),
                new SimpleContentSystemExtractor(NINTENDO_NES, "Famicom", "Nintendo Entertainment System"),
                new SimpleContentSystemExtractor(NINTENDO_VIRTUAL_BOY, "Virtual Boy", "VirtualBoy"),
                new SimpleContentSystemExtractor(NINTENDO_EREADER, "Nintendo e-Reader"),
                //
                new SimpleContentSystemExtractor(NEC_SUPERGRAFX, "SuperGrafx", "Super Grafx"),
                new SimpleContentSystemExtractor(NEC_TURBOGRAFX_CD, "PC-Engine CD", "TurboGrafx 16 CD", "TurboGrafx CD"),
                new SimpleContentSystemExtractor(NEC_TURBOGRAFX, "Turbografx", "PC-Engine"),

                new SimpleContentSystemExtractor(SNK_NEOGEO_CD),
                new SimpleContentSystemExtractor(SNK_NEOGEO_POCKET_COLOR, "Neo-Geo Pocket Color", "Neogeo Pocket Color"),
                new SimpleContentSystemExtractor(SNK_NEOGEO_POCKET, "Neo-Geo Pocket", "Neogeo Pocket"),

                new SimpleContentSystemExtractor(PANASONIC_3DO, "3DO"),

                new SimpleContentSystemExtractor(PHILIPS_VIDEOPAC, "Videopac"),
                new SimpleContentSystemExtractor(PHILIPS_P2000),
                new SimpleContentSystemExtractor(PHILIPS_VG5000),
                new SimpleContentSystemExtractor(PHILIPS_CD_I),

                new SimpleContentSystemExtractor(RADIO_86RK_APOGEJ_BK01, "Radio-86RK Apogej BK-01"),
                new SimpleContentSystemExtractor(RADIO_86RK_MIKRO80, "Radio-86RK Mikro-80"),
                new SimpleContentSystemExtractor(RADIO_86RK_MIKROSHA, "Radio-86RK Mikrosha"),
                new SimpleContentSystemExtractor(RADIO_86RK_YUT_88, "Radio 86RK - Yut88"),
                new SimpleContentSystemExtractor(RADIO_86RK_PARTNER_0101, "Radio-86RK Partner-01.01", "Radio-86RK - Partner-01.01"),
                new SimpleContentSystemExtractor(RADIO_86RK, "Radio 86RK", "Radio 86rk"),

                new SimpleContentSystemExtractor(SEGA_CD, "Sega CD", "Sega Mega-CD", "Sega Mega CD"),
                new SimpleContentSystemExtractor(SEGA_32X, "32X"),
                new SimpleContentSystemExtractor(SEGA_GAME_1000, "SG-1000", "Sega SG1000", "SG1000"),
                new SimpleContentSystemExtractor(SEGA_COMPUTER_3000, "Computer 3000"),
                new SimpleContentSystemExtractor(SEGA_GAMEGEAR, "Game Gear", "GameGear"),
                new SimpleContentSystemExtractor(SEGA_MASTERSYSTEM, "Master System", "MasterSystem", "Mark III", "Mark 3"),
                new SimpleContentSystemExtractor(SEGA_MEGADRIVE, "Genesis", "Megadrive", "Mega Drive"),
                new SimpleContentSystemExtractor(SEGA_ADVANCED_PICO_BEENA),
                new SimpleContentSystemExtractor(SEGA_SATURN),
                new SimpleContentSystemExtractor(SEGA_DREAMCAST),
                new SimpleContentSystemExtractor(SEGA_SUPERCONTROLSTATION),
                new SimpleContentSystemExtractor(SEGA_VMS),
                new SimpleContentSystemExtractor(SEGA_WONDERMEGA),
                new SimpleContentSystemExtractor(SEGA_BEENA),
                new SimpleContentSystemExtractor(SEGA_PICO),
                new SimpleContentSystemExtractor(SEGA_MODEL_2B),
                new SimpleContentSystemExtractor(SEGA_SYSTEM_32),
                new SimpleContentSystemExtractor(SEGA_CHIHIRO),
                new SimpleContentSystemExtractor(SEGA_LINDBERGH),
                new SimpleContentSystemExtractor(SEGA_RINGWIDE),
                new SimpleContentSystemExtractor(SEGA_TITAN_VIDEO),
                new SimpleContentSystemExtractor(SEGA_SYSTEM_32),
                new SimpleContentSystemExtractor(SEGA_NAOMI),
                new SimpleContentSystemExtractor(SEGA_MEGAPLAY),

                new SimpleContentSystemExtractor(SHARP_X68000, "X68000"),
                new SimpleContentSystemExtractor(SHARP_MZ80B),
                new SimpleContentSystemExtractor(SHARP_MZ700),
                new SimpleContentSystemExtractor(SHARP_MZ800),
                new SimpleContentSystemExtractor(SHARP_MZ2000),
                new SimpleContentSystemExtractor(SHARP_MZ2500),
                new SimpleContentSystemExtractor(SHARP_PCG850, "Sharp PC-G850, PC-G815 & E200", "Sharp PC-G850", "Sharp PCG850"),
                new SimpleContentSystemExtractor(SHARP_MZ2500),
                new SimpleContentSystemExtractor(SHARP_X1TURBO),
                new SimpleContentSystemExtractor(SHARP_X1),
                new SimpleContentSystemExtractor(SHARP_X68000),
                new SimpleContentSystemExtractor(SHARP_WIZARD_OZ_700, "Sharp Wizard OZ-700"),

                new SimpleContentSystemExtractor(SORD_M5, "Sord CGL M 5"),

                new SimpleContentSystemExtractor(SONY_SMC777, "Sony SMC-777", "Sony SMC777", "SMC777", "SMC-777"),
                new SimpleContentSystemExtractor(SONY_PLAYSTATION_PORTABLE, "PSP"),
                new SimpleContentSystemExtractor(SONY_PLAYSTATION_MOBILE, "PSM"),
                new SimpleContentSystemExtractor(SONY_PLAYSTATION_VITA, "PSV"),
                new SimpleContentSystemExtractor(SONY_PLAYSTATION_5, "PS5"),
                new SimpleContentSystemExtractor(SONY_PLAYSTATION_4, "PS4"),
                new SimpleContentSystemExtractor(SONY_PLAYSTATION_3, "PS3"),
                new SimpleContentSystemExtractor(SONY_PLAYSTATION_2, "PS2"),
                new SimpleContentSystemExtractor(SONY_PLAYSTATION, "PS1", "PSX"),
                new SimpleContentSystemExtractor(SONY_POCKETSTATION),
                new SimpleContentSystemExtractor(SONY_NEWS),

                new SimpleContentSystemExtractor(SINCLAIR_ZX_SPECTRUM_128, "ZX Spectrum 128K", "Spectrum 128K"),
                new SimpleContentSystemExtractor(SINCLAIR_ZX_SPECTRUM, "ZX Spectrum"),

                new SimpleContentSystemExtractor(TANDY_TRS_80_3),
                new SimpleContentSystemExtractor(TANDY_TRS_80_4),
                new SimpleContentSystemExtractor(TANDY_TRS_2000),
                new SimpleContentSystemExtractor(TANDY_TRS_6000),
                new SimpleContentSystemExtractor(TANDY_TRS_80_COCO),
                new SimpleContentSystemExtractor(TANDY_TRS_80_MC_10),
                new SimpleContentSystemExtractor(TANDY_TRS_80_100),
                new SimpleContentSystemExtractor(TANDY_TRS_80_1),
                new SimpleContentSystemExtractor(TANDY_TRS_80_200),

                new SimpleContentSystemExtractor(TANGERINE_ORIC_TELESTRAT),
                new SimpleContentSystemExtractor(TANGERINE_MICROTAN_65),
                new SimpleContentSystemExtractor(TANGERINE_ORIC_MULTI),
                new SimpleContentSystemExtractor(TANGERINE_ORIC_1),
                new SimpleContentSystemExtractor(TANGERINE_ORIC_ATMOS),

                new SimpleContentSystemExtractor(TOSHIBA_PASOPIA_7, "Pasopia 7"),
                new SimpleContentSystemExtractor(TOSHIBA_PASOPIA, "Pasopia"),
                new SimpleContentSystemExtractor(TOSHIBA_VISICOM),

                new SimpleContentSystemExtractor(THOMSON_MO5),
                new SimpleContentSystemExtractor(THOMSON_MO6),
                new SimpleContentSystemExtractor(THOMSON_TO7),
                new SimpleContentSystemExtractor(THOMSON_TO8_MULTI),
                new SimpleContentSystemExtractor(THOMSON_TO8D),
                new SimpleContentSystemExtractor(THOMSON_TO8),
                new SimpleContentSystemExtractor(THOMSON_TO9_PLUS),
                new SimpleContentSystemExtractor(THOMSON_TO9),

                new SimpleContentSystemExtractor(XEROX_820_II),
                new SimpleContentSystemExtractor(XEROX_820),
                new SimpleContentSystemExtractor(XEROX_860),
                new SimpleContentSystemExtractor(XEROX_1108),
                new SimpleContentSystemExtractor(XEROX_1186),
                new SimpleContentSystemExtractor(XEROX_6085),
                new SimpleContentSystemExtractor(XEROX_DAYBREAK)
            )
            .toList();

        var extractorsPost = Stream.of(
                new SimpleContentSystemExtractor(AMSTRAD),
                new SimpleContentSystemExtractor(ACT_APRICOT),
                new SimpleContentSystemExtractor(APPLE_LISA_2),
                new SimpleContentSystemExtractor(APPLE_LISA),
                new SimpleContentSystemExtractor(APPLE_3, "Apple 3"),
                new SimpleContentSystemExtractor(APPLE_2GS, "Apple IIGS", "Apple 2GS"),
                new SimpleContentSystemExtractor(APPLE_2, "Apple II", "Apple 2"),
                new SimpleContentSystemExtractor(APPLE_1, "Apple I", "Apple 1"),
                new SimpleContentSystemExtractor(APPLE_PIPPIN, "Pippin"),
                new SimpleContentSystemExtractor(BENESSE_POCKET_CHALLENGE_V),
                new SimpleContentSystemExtractor(CYBIKO),
                new SimpleContentSystemExtractor(DEC_ALPHA),
                new SimpleContentSystemExtractor(DEC_PDP_1),
                new SimpleContentSystemExtractor(ETL_MARK_4),
                new SimpleContentSystemExtractor(EXELVISION),
                new SimpleContentSystemExtractor(FUJITSU),
                new SimpleContentSystemExtractor(FUNWORLD_PHOTOPLAY),
                new SimpleContentSystemExtractor(GALAKSIJA),
                new SimpleContentSystemExtractor(IBM_PCJR, "IBM PCjr"),
                new SimpleContentSystemExtractor(IBM_PC, "IBM PC Compatibles"),
                new SimpleContentSystemExtractor(LUXOR_ABC_80),
                new SimpleContentSystemExtractor(MEMOTECH_MTX),

                new SimpleContentSystemExtractor(NINTENDO, "Nintendo"),

                new SimpleContentSystemExtractor(SNK_NEOGEO),
                new SimpleContentSystemExtractor(SONY),
                new SimpleContentSystemExtractor(TESLA_ONDRA, "Tesla Ondra", "Ondra Vili"),
                new SimpleContentSystemExtractor(TIMETOP_GAMEKING),
                new SimpleContentSystemExtractor(TIMEX_TS_2068, "Timex Sinclair", "Timex Sinclair TS-2068"),
                new SimpleContentSystemExtractor(VTECH_VSMILE, "V.Smile"),
                new SimpleContentSystemExtractor(VTECH_VFLASH, "V.Flash"),
                new SimpleContentSystemExtractor(VTECH_GENIUS),
                new SimpleContentSystemExtractor(VTECH_LASER_200),
                new SimpleContentSystemExtractor(VTECH_LASER),
                new SimpleContentSystemExtractor(VTECH_MOBIGO),
                new SimpleContentSystemExtractor(VTECH_PRECOMPUTER_1000),
                new SimpleContentSystemExtractor(XEROX)
            )
            .toList();

        var specificExtractorsSystems = Stream.concat(extractorsPre.stream(), extractorsPost.stream()).map(ContentSystemExtractor::getContentSystem).collect(Collectors.toSet());

        // Add default extractors for not explicitly defined defined systems
        var standardExtractors = Stream.of(values()).filter(s -> !specificExtractorsSystems.contains(s)).sorted().map(SimpleContentSystemExtractor::new).toList();

        extractors = new ArrayList<>();
        extractors.addAll(extractorsPre);
        extractors.addAll(standardExtractors);
        extractors.addAll(extractorsPost);
    }

    public Stream<ContentSystemExtractor> stream() {
        return extractors.stream();
    }
}
