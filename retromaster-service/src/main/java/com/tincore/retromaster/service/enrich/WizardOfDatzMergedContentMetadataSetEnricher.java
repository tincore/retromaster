package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentMediaType;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.retromaster.service.ContentSystemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.tincore.retromaster.domain.ContentMetadataSet.AUTHOR_LENGTH;
import static com.tincore.retromaster.domain.ContentMetadataSource.WIZARD_OF_DATZ_MERGED;
// <header>
// <name>CCE - MC-1000 (merged)</name>
// <description>CCE - MC-1000 (merged 20160515133525)</description>
// <category>SabreTools</category>
// <version></version>
// <date>20160515133525</date>
// <author>SabreTools</author>
// </header>
// <header>
// <name>Acorn - Archimedes (merged 20160317085507)</name>
// <description>Acorn - Archimedes (merged 20160317085507)</description>
// <category>The Wizard of DATz</category>
// <version>20160317085507</version>
// <date>20160317085507</date>
// <author>The Wizard of DATz</author>
// <clrmamepro/>

@Slf4j
@Service
@RequiredArgsConstructor
public class WizardOfDatzMergedContentMetadataSetEnricher extends AbstractContentMetadataSetEnricher implements ContentMetadataSetEnricher {

    private final ContentSystemService contentSystemService;

    private final DemobaseGameboyContentMetadataEnricher demobaseGameboyContentMetadataEnricher;
    private final DemobasePlus4ContentMetadataEnricher demobasePlus4ContentMetadataEnricher;
    private final Ep128HuContentMetadataEnricher ep128HuContentMetadataEnricher;
    private final GoodsetContentMetadataEnricher goodsetContentMetadataEnricher;
    private final MameContentMetadataEnricher mameContentMetadataEnricher;
    private final MaybeIntroContentMetadataEnricher maybeIntroContentMetadataEnricher;
    private final NoIntroContentMetadataEnricher noIntroContentMetadataEnricher;
    private final NonGoodContentMetadataEnricher nonGoodContentMetadataEnricher;
    private final RedumpContentMetadataEnricher redumpContentMetadataEnricher;
    private final TosecContentMetadataEnricher tosecContentMetadataEnricher;
    private final TruripContentMetadataEnricher truripContentMetadataEnricher;
    private final RetromasterConfiguration retromasterConfiguration;
    private final AlexVampireContentMetadataEnricher alexVampireContentMetadataEnricher;
    private final ComputerEmuZoneContentMetadataEnricher computeremuzoneContentMetadataEnricher;
    private final VizzedContentMetadataEnricher vizzedContentMetadataEnricher;

    @Override
    public boolean canEnrich(ContentMetadataSet contentMetadataSet) {
        return WIZARD_OF_DATZ_MERGED.isMatch(contentMetadataSet);
    }

    @Override
    public ContentMetadata enrichContentMetadata(ContentMetadata contentMetadata, String systemDefault) { // NOPMD
        var name = contentMetadata.getName();

        if (name.equals(contentMetadata.getDescription())) {
            contentMetadata.setDescription("");
        }

        var tokens = StringUtils.substringsBetween(name, "[", "]");
        var source = tokens != null && tokens.length > 0 ? tokens[tokens.length - 1] : null;
        if (retromasterConfiguration.getEnrich().getSabretoolsMergedOriginExclude().contains(source)) {
            log.trace("Filter out entry {}", name);
            return null;
        }

        contentMetadata.setMediaType(ContentMediaType.UNKNOWN); // Should eventually: Default from set
        contentMetadata.setSystem(systemDefault);

        var title = StringUtils.substringBefore(name, "[").trim();
        switch (source) {
            case "AlexVampire" -> alexVampireContentMetadataEnricher.enrich(title, contentMetadata);
            case "DemobaseGameboy" -> demobasePlus4ContentMetadataEnricher.enrich(title, contentMetadata);
            case "DemobasePlus4" -> demobaseGameboyContentMetadataEnricher.enrich(title, contentMetadata);
            case "Computeremuzone" -> computeremuzoneContentMetadataEnricher.enrich(title, contentMetadata);
            case "ep128hu" -> ep128HuContentMetadataEnricher.enrich(title, contentMetadata);
            case "Good" -> goodsetContentMetadataEnricher.enrich(title, contentMetadata);
            case "MAME" -> mameContentMetadataEnricher.enrich(title, contentMetadata);
            case "Maybe-Intro" -> maybeIntroContentMetadataEnricher.enrich(title, contentMetadata);
            case "no-Intro" -> noIntroContentMetadataEnricher.enrich(title, contentMetadata);
            case "NonGood" -> nonGoodContentMetadataEnricher.enrich(title, contentMetadata);
            case "TOSEC" -> tosecContentMetadataEnricher.enrich(title, contentMetadata);
            case "Trurip" -> truripContentMetadataEnricher.enrich(title, contentMetadata);
            case "Redump" -> redumpContentMetadataEnricher.enrich(title, contentMetadata);
            case "Vizzed" -> vizzedContentMetadataEnricher.enrich(title, contentMetadata);
            default -> {
                contentMetadata.setTitle(title);
                contentMetadata.setSource(source);
            }
        }

        if (StringUtils.isBlank(contentMetadata.getTitle())) {
            log.error("No title!! " + contentMetadata);
        }

        return contentMetadata;
    }

    @Override
    public ContentMetadataSet enrichContentMetadataSet(ContentMetadataSet contentMetadataSet) {
        contentMetadataSet.setCategory(getContentMetadataSource().toString());
        if (!contentMetadataSet.isStaging() || StringUtils.isBlank(contentMetadataSet.getRepositoryPath())) {
            contentMetadataSet.setRepositoryPath(getRepositoryPath(contentMetadataSet));
        }
        contentMetadataSet.setAuthor(StringUtils.left(contentMetadataSet.getAuthor(), AUTHOR_LENGTH));
        return contentMetadataSet;
    }

    @Override
    public String extractSystem(ContentMetadataSet contentMetadataSet) {
        var system = contentMetadataSet.getName().replaceAll(" - ", " ");
        return contentSystemService.findSystemNormalized(StringUtils.substringBefore(system, "(").trim());
    }

    @Override
    public ContentMetadataSource getContentMetadataSource() {
        return WIZARD_OF_DATZ_MERGED;
    }

    public String getRepositoryPath(ContentMetadataSet contentMetadataSet) {
        return extractSystem(contentMetadataSet) + " - " + contentMetadataSet.getVersion().trim() + " " + contentMetadataSet.getCategory();
    }
}
