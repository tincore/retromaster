package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.util.FileSystemTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentErrorService implements FileSystemTrait {

    private final ContentVolumeService contentVolumeService;
    private final ContentRepositoryService contentRepositoryService;
    private final RetromasterConfiguration retromasterConfiguration;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean doRejectBroken(ContentVolume volume, Path fileAbsolutePath) {
        log.error("doRejectBroken {}", fileAbsolutePath);
        var errorContentVolumeRootPath = contentVolumeService.getExportErrorPath();
        try {
            if (Files.exists(fileAbsolutePath)) {
                moveToUnique(fileAbsolutePath, errorContentVolumeRootPath.resolve(volume.toPath().relativize(fileAbsolutePath)));
            }
            contentRepositoryService.deleteAllByVolumeAndFileAbsolutePath(volume, fileAbsolutePath);
            return true;
        } catch (IOException e) {
            log.error("doRejectBroken Failed " + fileAbsolutePath, e);
            return false;
        }
    }
}
