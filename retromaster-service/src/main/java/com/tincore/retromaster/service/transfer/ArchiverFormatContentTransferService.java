package com.tincore.retromaster.service.transfer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentExtension;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.ContentSystemService;
import com.tincore.retromaster.service.archive.ArchiverFormat;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.StreamTrait;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ArchiverFormatContentTransferService implements StreamTrait, FileSystemTrait {

    private final ContentSystemService contentSystemService;
    private final RetromasterConfiguration.ContentsConfiguration contentsConfiguration;
    private final Map<ContentSystem, RetromasterConfiguration.ArchiverFormatContentSystemConfiguration> contentSystemsConfigurationsByContentSystem;

    public ArchiverFormatContentTransferService(ContentSystemService contentSystemService, RetromasterConfiguration.ContentsConfiguration contentsConfiguration) {
        this.contentSystemService = contentSystemService;
        this.contentsConfiguration = contentsConfiguration;

        this.contentSystemsConfigurationsByContentSystem = contentsConfiguration.getSystems().stream().collect(Collectors.toMap(RetromasterConfiguration.ContentSystemConfiguration::getContentSystem, s -> s));
    }

    public Optional<ArchiverFormat> getArchiverFormatByContentMetadata(ContentMetadata contentMetadata) {
        return Optional.of(getArchiverFormatContentSystemConfiguration(contentMetadata))
            .filter(c -> !c.isDisableArchiver())
            .map(c -> isQuickArchiverFormatRequired(contentMetadata, c))
            .map(b -> b ? contentsConfiguration.getQuickArchiverFormat() : contentsConfiguration.getSolidArchiverFormat());
    }

    public Optional<ArchiverFormat> getArchiverFormatByQuick() {
        return Optional.of(contentsConfiguration.getQuickArchiverFormat());
    }

    public RetromasterConfiguration.ArchiverFormatContentSystemConfiguration getArchiverFormatContentSystemConfiguration(ContentMetadata contentMetadata) {
        return contentSystemService.findBySystemLabel(contentMetadata.getSystem())
            .flatMap(s -> Optional.ofNullable(contentSystemsConfigurationsByContentSystem.get(s)))
            .orElse(contentsConfiguration);
    }

    private boolean isQuickArchiverFormatRequired(ContentMetadata contentMetadata, RetromasterConfiguration.ArchiverFormatContentSystemConfiguration archiverFormatContentSystemConfiguration) {
        return contentMetadata.getItems().stream().anyMatch(i -> isQuickArchiverFormatRequired(i, archiverFormatContentSystemConfiguration));
    }

    private boolean isQuickArchiverFormatRequired(ContentMetadataItem contentMetadataItem, RetromasterConfiguration.ArchiverFormatContentSystemConfiguration archiverFormatContentSystemConfiguration) {
        return archiverFormatContentSystemConfiguration.isQuickArchiverFormatOnContentExtensionDisc() && ContentExtension.isDisc(contentMetadataItem.getName())
               || contentMetadataItem.getSize() > archiverFormatContentSystemConfiguration.getQuickArchiverFormatOnSizeGreaterThan();
    }
}
