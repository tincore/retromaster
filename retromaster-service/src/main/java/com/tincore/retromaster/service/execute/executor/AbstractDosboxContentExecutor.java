package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ExecutionContext;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentExtension.*;

public abstract class AbstractDosboxContentExecutor extends AbstractContentExecutor {
    private static final Set<String> CONTENT_EXTENSIONS = Stream.of(bat, com, exe).map(Enum::name).collect(Collectors.toSet());

    private final RetromasterConfiguration.DosboxExecuteConfiguration dosboxExecuteConfiguration;

    public AbstractDosboxContentExecutor(String id, String name, RetromasterConfiguration.DosboxExecuteConfiguration dosboxExecuteConfiguration) {
        super(id, name, false, ContentSystem.IBM_PC);
        this.dosboxExecuteConfiguration = dosboxExecuteConfiguration;
    }

    @Override
    public void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException {
        Path cfgPath = null;
        var contentArgument = executionContext.getMainItemPath();
        try {
            var contentArgumentString = contentArgument.toString();
            cfgPath = Files.createTempFile("dosbox.retromaster", ".cfg");
            var config = new StringBuilder()
                .append("[sdl]")
                .append(EOL)
                .append("fullscreen=")
                .append(dosboxExecuteConfiguration.isFullscreen())
                .append(EOL)
                .append("fulldouble=")
                .append(dosboxExecuteConfiguration.isFulldouble())
                .append(EOL)
                .append("fullresolution=")
                .append(dosboxExecuteConfiguration.getFullResolution())
                .append(EOL)
                .append("windowresolution=")
                .append(dosboxExecuteConfiguration.getWindowResolution())
                .append(EOL)
                .append("output=")
                .append(dosboxExecuteConfiguration.getOutput())
                .append(EOL)
                .append("[autoexec]")
                .append(EOL);
            if (isDisc(contentArgumentString)) {
                config.append("imgmount D ").append(StringUtils.wrapIfMissing(contentArgumentString, "\"")).append(" -t iso").append(EOL);
            } else if (isDisk(contentArgumentString)) {
                // config.append("imgmount B ").append(StringUtils.wrapIfMissing(contentArgument.toString64(), "\"")).append("\n");
                config.append("mount C ")
                    .append(StringUtils.wrapIfMissing(contentArgument.getParent().toString(), "\""))
                    .append(EOL)
                    .append("C:")
                    .append(EOL)
                    .append("imgmount A ")
                    .append(StringUtils.wrapIfMissing(contentArgument.getFileName().toString(), "\""))
                    .append(" -t floppy")
                    .append(EOL);
            } else {
                // config.append("imgmount B ").append(StringUtils.wrapIfMissing(contentArgument.toString64(), "\"")).append("\n");
                config.append("mount C ").append(StringUtils.wrapIfMissing(contentArgument.getParent().toString(), "\"")).append(EOL).append("C:").append(EOL).append(contentArgument.getFileName().toString()).append(EOL);
            }
            Files.write(cfgPath, config.toString().getBytes());

            List<String> params = new ArrayList<>();
            params.add("userconf");
            params.add("conf " + StringUtils.wrapIfMissing(cfgPath.toString(), "\""));

            if (dosboxExecuteConfiguration.isFullscreen()) {
                params.add("fullscreen");
            }

            // name
            // If name is a directory, DOSBox will mount the specified directory as the C drive.
            // If name is an executable, DOSBox will mount the directory of name as the C drive, and start executing name.
            // -exit
            // DOSBox will close itself when the DOS application name ends.
            // -c command
            // Runs the specified command before running name. Multiple commands can be specified. Each command should start with -c though. A command can be: an Internal Program, a DOS command or an executable on a mounted drive.
            // -fullscreen
            // Starts DOSBox in fullscreen mode.
            // -userconf[1]
            // Load the configuration from the user's profile or home directory. This is the default behavior, so this switch is useful when using -conf, below.
            // -conf configfilelocation
            // Load the config file specified in configfilelocation. Useful for specifying particular options for specific games. If used after -userconf, or if you use multiple -conf switches, options present in multiple configs will be
            // overwritten by the last, except for autoexec entries, which will be appended in order.
            // -lang languagefilelocation
            // Start DOSBox using the language string specified in languagefilelocation.
            // -machine machinetype
            // Setup DOSBox to emulate a specific type of machine. Valid choices are: hercules, cga, ega, pcjr, tandy, svga_s3 (default) as well as the additional svga chipsets listed in the DOSBox configuration file. The machinetype
            // affects the video card and the available sound cards.
            // -noconsole (Windows Only)
            // Start DOSBox without showing the console window, output will be redirected to stdout.txt and stderr.txt. This is useful if DOSBox crashes, since the error messages stored in stdout.txt and stderr.txt may help the developers
            // fixing the problem.
            // -startmapper
            // Enter the mapper directly on startup. Useful for people with keyboard or joystick problems.
            // -noautoexec
            // Skips the [autoexec] section of the loaded configuration file.
            // -securemode
            // Same as -noautoexec, but adds config.com -securemode at the bottom of AUTOEXEC.BAT (which in turn disables any changes to how the drives are mounted inside DOSBox).
            // -scaler
            // Uses the scaler specified by "scaler". See the DOSBox configuration file for the available scalers.
            // -forcescaler
            // Similar to the -scaler parameter, but tries to force usage of the specified scaler even if it might not fit.
            // -version
            // output version information and exit. (see stdout.txt) Useful for frontends.
            // -editconf program
            // calls program with as first parameter the configuration file. You can specify this command more than once. In this case it will move to second program if the first one fails to start.
            // -opencaptures program
            // calls program with as first parameter the location of the captures folder.
            // -printconf
            // prints the location of the default configuration file.
            // -resetconf
            // removes the default configuration file.
            // -resetmapper
            // removes the mapperfile used by the default clean configuration file.
            // -socket
            // passes the socket number to the nullmodem emulation.
            // Notes
            var cmd = String.format("%s %s", getExecutablePath(), params.stream().map(p -> "-" + p).collect(Collectors.joining(" ")));
            exec(cmd, executionContext.getExecutionId());
        } finally {
            if (cfgPath != null) {
                Files.deleteIfExists(cfgPath);
            }
        }
    }

    @Override
    public Set<String> getContentExtensions() {
        return CONTENT_EXTENSIONS;
    }

    public Stream<String> getExecutablePaths() {
        return dosboxExecuteConfiguration.getExecutablePaths().stream();
    }

    @Override
    public boolean isReadOnlyExecutionCapable() {
        return false;
    }
}
