package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentLanguage;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentRegion;
import com.tincore.util.StreamTrait;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.tincore.retromaster.domain.ContentMediaType.*;

@Service
public class TosecContentMetadataEnricher implements ContentMetadataEnricher, StreamTrait {
    public static final String UNIDENTIFIED_PREFIX = "ZZZ-UNK-";

    @Override
    public void enrich(String title, ContentMetadata contentMetadata) { // NOPMD
        if (contentMetadata.getName().equals(contentMetadata.getDescription())) {
            contentMetadata.setDescription("");
        }

        contentMetadata.setTitle(title);

        var name = contentMetadata.getItems().stream().map(ContentMetadataItem::getName).filter(n -> StringUtils.startsWithIgnoreCase(n, title)).findFirst().orElse(contentMetadata.getName());

        var unidentified = name.startsWith(UNIDENTIFIED_PREFIX);
        contentMetadata.setUnidentified(unidentified);

        var parenthesesIndex = name.indexOf('(', 1);
        var demoCloseParentheses = name.indexOf(") ", parenthesesIndex);
        if (demoCloseParentheses != -1) {
            // title (demo) (date...)
            parenthesesIndex = name.indexOf('(', demoCloseParentheses);
        }

        var bracketIndex = unidentified ? name.indexOf('[', UNIDENTIFIED_PREFIX.length() + 1) : name.indexOf('[', 1);

        List<String> tokensExtra = null;
        if (parenthesesIndex != -1) {
            var m1 = PATTERN_PARENTHESES.matcher(name.substring(parenthesesIndex));
            if (!unidentified) {
                m1.find();
                var date = m1.group(1);
                contentMetadata.setDate(date);
                var foundPublisher = m1.find();
                if (foundPublisher) { // NOPMD
                    var publisher = m1.group(1);
                    contentMetadata.setPublisher(publisher);
                }
            }

            Set<String> parenthesesAttributes = new HashSet<>();
            while (m1.find()) {
                parenthesesAttributes.add(m1.group(1));
            }
            enrichParenthesesAttributes(contentMetadata, parenthesesAttributes);

            parenthesesAttributes.remove("-"); // Sometimes is dangling in UNK

            tokensExtra = new ArrayList<>(parenthesesAttributes);
        }

        if (bracketIndex != -1) {
            Set<String> bracketsAttributes = new HashSet<>();
            var m = PATTERN_BRACKETS.matcher(name.substring(bracketIndex));
            while (m.find()) {
                bracketsAttributes.add(m.group(1));
            }
            enrichBracketsAttributes(contentMetadata, bracketsAttributes);

            if (tokensExtra == null) {
                tokensExtra = new ArrayList<>();
            }
            tokensExtra.addAll(bracketsAttributes);
        }

        if (tokensExtra != null) {
            contentMetadata.setTagsExtra(String.join(",", tokensExtra));
        }
    }

    public void enrichBracketsAttributes(ContentMetadata contentMetadata, Set<String> bracketsAttributes) {
        applyOnPrefixFound(bracketsAttributes, "!", data -> contentMetadata.setVerified(true));
        applyOnPrefixFound(
            bracketsAttributes,
            "a",
            data -> {
                contentMetadata.setAlternate(true);
                contentMetadata.setAlternateData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "b",
            data -> {
                contentMetadata.setBadDump(true);
                contentMetadata.setBadDumpData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "cr",
            data -> {
                contentMetadata.setCracked(true);
                contentMetadata.setCrackedData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "f",
            data -> {
                contentMetadata.setFixed(true);
                contentMetadata.setFixedData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "h",
            data -> {
                contentMetadata.setHacked(true);
                contentMetadata.setHackedData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "m",
            data -> {
                contentMetadata.setModified(true);
                contentMetadata.setModifiedData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "p",
            data -> {
                contentMetadata.setPirated(true);
                contentMetadata.setPiratedData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "v",
            data -> {
                contentMetadata.setVirus(true);
                contentMetadata.setVirusData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "tr",
            data -> {
                contentMetadata.setTranslated(true);
                contentMetadata.setTranslatedData(data);
            });
        applyOnPrefixFound(
            bracketsAttributes,
            "t",
            data -> {
                contentMetadata.setTrained(true);
                contentMetadata.setTrainedData(data);
            });
    }

    public void enrichParenthesesAttributes(ContentMetadata contentMetadata, Set<String> parenthesesAttributes) {
        applyOnPrefixFound(
            parenthesesAttributes,
            "Disc",
            data -> {
                contentMetadata.setMediaType(DISC);
                contentMetadata.setMediaOrder(data);
            });
        applyOnPrefixFound(
            parenthesesAttributes,
            "Disk",
            data -> {
                contentMetadata.setMediaType(DISK);
                contentMetadata.setMediaOrder(data);
            });
        applyOnPrefixFound(
            parenthesesAttributes,
            "File",
            data -> {
                contentMetadata.setMediaType(FILE);
                contentMetadata.setMediaOrder(data);
            });
        applyOnPrefixFound(
            parenthesesAttributes,
            "Part",
            data -> {
                contentMetadata.setMediaType(PART);
                contentMetadata.setMediaOrder(data);
            });
        applyOnPrefixFound(
            parenthesesAttributes,
            "Side",
            data -> {
                contentMetadata.setMediaType(SIDE);
                contentMetadata.setMediaOrder(data);
            });
        applyOnPrefixFound(
            parenthesesAttributes,
            "Tape",
            data -> {
                contentMetadata.setMediaType(TAPE);
                contentMetadata.setMediaOrder(data);
            });

        // search if region
        parenthesesAttributes.stream()
            .filter(
                pA -> {
                    var tokens = pA.split("-");
                    return toStream(tokens).anyMatch(ContentRegion.SET::contains);
                })
            .findFirst()
            .ifPresent(
                pA -> {
                    contentMetadata.setRegion(pA);
                    parenthesesAttributes.remove(pA);
                });

        parenthesesAttributes.stream()
            .filter(
                pA -> {
                    var tokens = pA.split("-");
                    return toStream(tokens).anyMatch(ContentLanguage.SET::contains);
                })
            .findFirst()
            .ifPresent(
                pA -> {
                    contentMetadata.setLanguage(pA);
                    parenthesesAttributes.remove(pA);
                });
    }
}
