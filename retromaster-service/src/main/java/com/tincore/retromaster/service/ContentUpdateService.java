package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.domain.transfer.OriginContentTransfer;
import com.tincore.retromaster.service.transfer.ContentTransferCleanupService;
import com.tincore.retromaster.service.transfer.ContentTransferService;
import com.tincore.retromaster.service.transfer.OriginContentTransferService;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.Partition;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.tincore.util.lang.ThrowingConsumer.uConsumer;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentUpdateService implements FileSystemTrait {

    private static final int UPDATE_ITERATIONS_MAX = 50;
    private static final int PARTITION_SIZE = 50;

    private final ContentService contentService;
    private final ContentVolumeService contentVolumeService;
    private final ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    private final OriginContentTransferService originContentTransferService;
    private final ContentTransferService contentTransferService;
    private final ContentTransferCleanupService contentTransferCleanupService;
    private final ContentErrorService contentErrorService;
    private final RetromasterConfiguration retromasterConfiguration;

    public void updateContentsByContentMetadataSet(ContentMetadataSet contentMetadataSet) throws IOException, ExecutionException, InterruptedException {
        for (var contentVolume : contentVolumeService.getContentVolumesByEnvironment(ContentVolumeEnvironment.PROD)) {
            updateContentsByContentVolumeAndDirectory(contentVolume, contentMetadataSet.getRepositoryPath(), false);
        }
    }

    public void updateContentsByContentVolume(ContentVolume contentVolume) throws IOException {
        updateContentsByContentVolume(contentVolume, false);
    }

    public void updateContentsByContentVolume(ContentVolume contentVolume, boolean orphanOnly) throws IOException {
        if (contentVolume.isStage()) {
            throw new IllegalArgumentException("updateContentsByContentVolume: Ignoring Stage volume because not supported by this method. Use ContentImportAutometadataService methods, vol=" + contentVolume);
        }
        if (!contentVolume.getEnvironment().isContainsImported()) {
            log.warn("updateContentsByContentVolume: Skipped volume that does not allow contents, vol={}", contentVolume.getName());
            return;
        }

        log.debug("updateContentsByContentVolume: Processing, vol={}", contentVolume.getName());
        var contentVolumePath = contentVolume.toPath();


        List<Path> directories;
        try (var stream = Files.walk(contentVolumePath, 1)) {
            directories = stream.filter(p -> !p.equals(contentVolumePath) && Files.isDirectory(p))
                .sorted().toList();
        }
        directories.forEach(uConsumer(p -> updateContentsByContentVolumeAndDirectory(contentVolume, contentVolumePath.relativize(p).toString(), orphanOnly)));
        log.debug("updateContentsByContentVolume: Deleting empty dirs, vol={}", contentVolume.getName());
        deleteEmptyDirectories(contentVolumePath);
        log.debug("updateContentsByContentVolume: Deleted empty dirs, vol={}", contentVolume.getName());
    }

    public void updateContentsByContentVolumeAndDirectory(ContentVolume contentVolume, String subPath, boolean orphanOnly) throws IOException, ExecutionException, InterruptedException { // NOPMD BS
        if (!contentVolume.getEnvironment().isContainsImported()) {
            throw new IllegalArgumentException("updateContentsByContentVolumeAndDirectory: Volume environment does not contain contents, vol=%s".formatted(contentVolume));
        }

        var rootPath = contentVolume.toPath().resolve(subPath);
        if (!Files.exists(rootPath)) {
            log.debug("updateContentsByContentVolumeAndDirectory: Skipped non existing path, vol={}, subPath={}", contentVolume.getName(), subPath);
            return;
        }

        var contentMetadataSets = contentMetadataSetRepositoryService.findByRepositoryPath(subPath);
        if (contentMetadataSets.isEmpty()) {
            var count = 0;
            while (count < UPDATE_ITERATIONS_MAX && !isEmpty(rootPath)) {
                log.debug("updateContentsByContentVolumeAndDirectory: New non empty directory so import all files, iteration={}, path={}", count, subPath);
                updateContentsFromProdVolumeAndSubPathAllFiles(contentVolume, subPath);
                count++;
            }
            if (isEmpty(rootPath)) {
                deleteSilent(rootPath);
            }
            return;
        }

        if (orphanOnly) {
            log.debug("updateContentsByContentVolumeAndDirectory: Completed, orphanOnly={}, path={}", orphanOnly, subPath);
            return;
        }


        var abnormalContentMetadataSetCount = contentMetadataSets.size() != 1;
        if (abnormalContentMetadataSetCount) {
            log.warn("updateContentsByContentVolumeAndDirectory: Update contents DETECTED 1 PATH SERVING MULTIPLE DATA SETS!!! NEEDS FIX IN CONTENT METADATA SET ENRICHERS!!! path={}, contentMetadataSets={}", subPath, contentMetadataSets);
            return;
        }

        var contentMetadataSet = contentMetadataSets.get(0);
        log.debug("updateContentsByContentVolumeAndDirectory: Checking, path={}, contentMetadataSet={}", subPath, contentMetadataSet.getName());
        var modifiedPaths = contentService.findContentPathsByModified(contentVolume, subPath);
        if (modifiedPaths.isEmpty()) {
            log.debug("updateContentsByContentVolumeAndDirectory: No changes found, path={}, contentMetadataSet={}", subPath, contentMetadataSet.getName());
        } else {
            var partition = Partition.ofSize(modifiedPaths, PARTITION_SIZE);
            log.debug("updateContentsByContentVolumeAndDirectory: Updating, path={}, contentMetadataSet={}", subPath, contentMetadataSet.getName());
            for (var part : partition) {
                updateContentsFromPaths(contentVolume, part);
            }
            contentService.deleteContentsByVolumeAndMetadataSetAndMissingFiles(contentVolume, contentMetadataSet);
            if (isEmpty(rootPath)) {
                deleteSilent(rootPath);
            }
        }

        contentMetadataSet.setCheckContentTime(Files.exists(rootPath) ? getLastModifiedTime(rootPath) : null);
        contentMetadataSetRepositoryService.save(contentMetadataSet);
    }

    public void updateContentsFromImportVolume(ContentVolume contentVolume) throws ExecutionException, InterruptedException, IOException {
        if (!contentVolume.getEnvironment().equals(ContentVolumeEnvironment.IMPORT)) {
            throw new IllegalArgumentException("updateContentsFromImportVolume: Not import type invalid volume, vol=" + contentVolume);
        }

        var originVolumeAbsolutePath = contentVolume.toPath();
        if (isEmpty(originVolumeAbsolutePath)) {
            log.trace("updateContentsFromImportVolume: Skipping empty origin, vol={}", contentVolume.getName());
            return;
        }

        log.debug("updateContentsFromImportVolume: Updating, vol={}", contentVolume.getName());
        var targetVolume = contentVolumeService.getContentVolumeByEnvironment(ContentVolumeEnvironment.PROD);
        var touchFileTimestamp = retromasterConfiguration.isContentVolumeImportPreTouchFiles();
        var originContentTransfersByTargetFilePath = originContentTransferService
            .streamOriginContentTransfers(contentVolume, originVolumeAbsolutePath, targetVolume.toPath(), p -> contentErrorService.doRejectBroken(contentVolume, p), touchFileTimestamp)
            .collect(Collectors.groupingBy(OriginContentTransfer::getTargetFileAbsolutePath, Collectors.toList()));

        // If initial path limit is much higher (and I use in memory cache to prevent scanning import  directory for content twice_
        // I can limit then limit here the amount of processed by batch

        var errorCount = contentTransferService.transferContents(originContentTransfersByTargetFilePath, targetVolume, true);

        contentTransferCleanupService.cleanUpDisposableContents(originContentTransfersByTargetFilePath);

        deleteEmptyDirectories(originVolumeAbsolutePath);

        log.debug("updateContentsFromImportVolume: Completed, err={}, vol={}", errorCount, contentVolume.getName());
    }

    public void updateContentsFromPaths(ContentVolume contentVolume, List<Path> paths) throws ExecutionException, InterruptedException {
        var contentVolumePath = contentVolume.toPath();
        var originContentTransfersByTargetFilePath = originContentTransferService
            .streamOriginContentTransfers(contentVolume, paths, contentVolumePath, p -> contentErrorService.doRejectBroken(contentVolume, p))
            .collect(Collectors.groupingBy(OriginContentTransfer::getTargetFileAbsolutePath, Collectors.toList()));

        contentTransferService.transferContents(originContentTransfersByTargetFilePath, contentVolume, false);

        contentTransferCleanupService.cleanUpDisposableContents(originContentTransfersByTargetFilePath);
    }

    /**
     * Use for orphan directories
     *
     * <p>
     * has batch size limit
     */
    public boolean updateContentsFromProdVolumeAndSubPathAllFiles(ContentVolume contentVolume, String subPath) throws ExecutionException, InterruptedException, IOException {
        if (!contentVolume.getEnvironment().equals(ContentVolumeEnvironment.PROD)) {
            throw new IllegalArgumentException("updateContentsFromProdVolumeAndSubPathAllFiles: Not prod volume error, vol=%s".formatted(contentVolume));
        }

        var originVolumeAbsolutePath = contentVolume.toPath().resolve(subPath);
        if (isEmpty(originVolumeAbsolutePath)) {
            log.trace("updateContentsFromProdVolumeAndSubPathAllFiles: Skipping empty, vol={}, path={}", contentVolume.getName(), originVolumeAbsolutePath);
            return true;
        }

        log.debug("updateContentsFromProdVolumeAndSubPathAllFiles: Processing, vol={}, path={}", contentVolume.getName(), originVolumeAbsolutePath);

        var targetVolume = contentVolumeService.getContentVolumeByEnvironment(ContentVolumeEnvironment.PROD);
        var originContentTransfersByTargetFilePath = originContentTransferService
            .streamOriginContentTransfers(contentVolume, originVolumeAbsolutePath, targetVolume.toPath(), p -> contentErrorService.doRejectBroken(contentVolume, p), false)
            .collect(Collectors.groupingBy(OriginContentTransfer::getTargetFileAbsolutePath, Collectors.toList()));

        var errorCount = contentTransferService.transferContents(originContentTransfersByTargetFilePath, targetVolume, true);

        contentTransferCleanupService.cleanUpDisposableContents(originContentTransfersByTargetFilePath);

        deleteEmptyDirectories(originVolumeAbsolutePath);

        log.debug("updateContentsFromProdVolumeAndSubPathAllFiles: Completed, err={}, vol={}", errorCount, contentVolume.getName());
        return errorCount == 0;
    }
}
