package com.tincore.retromaster.service.execute.prepare;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.service.execute.ContentExecutor;
import com.tincore.retromaster.service.execute.ExecutionContext;
import org.springframework.core.Ordered;

import java.io.IOException;

public interface ExecutionPrepareStrategy extends Ordered {
    default void doAfterExecution(ExecutionContext executionContext, boolean readOnly) throws IOException {
        // Does nothing
    }

    void doBeforeExecution(ExecutionContext executionContext, boolean readOnly) throws IOException;

    boolean isApplicable(ExecutionContext executionContext, ContentExecutor contentExecutor);
}
