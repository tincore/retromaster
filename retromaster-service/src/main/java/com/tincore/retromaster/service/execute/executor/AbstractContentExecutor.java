package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.domain.event.ContentExecutionEvent;
import com.tincore.retromaster.service.*;
import com.tincore.retromaster.service.execute.ContentExecutor;
import com.tincore.retromaster.service.execute.ExecutionContext;
import com.tincore.retromaster.service.execute.prepare.ExecutionPrepareStrategy;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.StreamTrait;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

@Slf4j
public abstract class AbstractContentExecutor implements ContentExecutor, StreamTrait, FileSystemTrait {

    public static final char EOL = '\n';

    protected static final ContentExtension[] CONTENT_EXTENSIONS_EMPTY = {};

    private final String id;
    private final String name;
    private final Set<ContentSystem> contentSystems;
    private final boolean multipartCapable;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private ContentService contentService;
    @Autowired
    private ContentMetadataItemService contentMetadataItemService;
    @Autowired
    private ProcessExecutorService processExecutorService;
    @Autowired
    private PackageFlatpakService packageFlatpakService;
    @Autowired
    private PackageSnapService packageSnapService;
    @Autowired
    private List<ExecutionPrepareStrategy> executionPrepareStrategies;

    protected AbstractContentExecutor(String id, String name, boolean multipartCapable, ContentSystem... contentSystems) {
        this.id = id;
        this.name = name;
        this.multipartCapable = multipartCapable;
        this.contentSystems = toStream(contentSystems).collect(toCollection(TreeSet::new));
    }

    public boolean canExecute(String fileName, List<ContentSystem> contentSystems) {
        if (!isFileSupported(fileName)) {
            return false;
        }
        return contentSystems.stream().anyMatch(this.contentSystems::contains);
    }

    public ExecutionContext createExecutionContext(
        ContentMetadataItem contentMetadataItem,
        UUID executionId,
        List<ContentMetadataItem> associatedItems,
        List<Pair<ContentMetadataItem, Content>> contentsMain,
        List<Pair<ContentMetadataItem, Content>> contentsAssociated) {
        return new ExecutionContext(executionId, contentMetadataItem, associatedItems, contentsMain, contentsAssociated);
    }

    public int exec(String cmd, UUID executionId) throws IOException, InterruptedException {
        applicationEventPublisher.publishEvent(new ContentExecutionEvent(executionId, String.format("exec %s", cmd)));
        var result = processExecutorService.exec(
            cmd,
            executionId,
            s -> applicationEventPublisher.publishEvent(new ContentExecutionEvent(executionId, s)));
        if (applicationEventPublisher != null) {
            applicationEventPublisher.publishEvent(new ContentExecutionEvent(executionId, String.format("exec finished result=%d", result)));
        }
        return result;
    }

    public void execute(ContentMetadataItem contentMetadataItem) throws IOException, InterruptedException {
        var executionId = UUID.randomUUID();
        var mainItemsWithContentsExisting = streamContentsWithContentMetadataItemPairsByContentMetadataIdAndFileExists(contentMetadataItem.getContentMetadata()).toList();

        List<ContentMetadataItem> associatedItems = multipartCapable ? contentMetadataItemService.getAssociatedItems(contentMetadataItem) : Collections.emptyList();
        var associatedItemsWithContentsExisting
            = associatedItems.stream().map(ContentMetadataItem::getContentMetadata).distinct().flatMap(this::streamContentsWithContentMetadataItemPairsByContentMetadataIdAndFileExists).toList();

        // Minimize different locations
        var contentMetadataItems = Stream.concat(mainItemsWithContentsExisting.stream(), associatedItemsWithContentsExisting.stream()).map(Pair::getLeft).collect(toSet());

        var contentMetadataItemsByPath = Stream.concat(mainItemsWithContentsExisting.stream(), associatedItemsWithContentsExisting.stream())
            .flatMap(p -> p.getRight().stream().map(c -> Pair.of(c, p.getLeft())))
            .filter(p -> contentMetadataItems.contains(p.getRight()))
            .collect(groupingBy(p -> p.getKey().getPathFileAbsolute(), mapping(Pair::getRight, toSet())));

        var pathsWithFileExistingSortedByUsage = contentMetadataItemsByPath.entrySet().stream().filter(e -> Files.exists(e.getKey())).sorted(Comparator.comparing(e -> -e.getValue().size())).map(Map.Entry::getKey).collect(toList());

        var contentsMain = mainItemsWithContentsExisting.stream().map(p1 -> Pair.of(p1.getLeft(), getFirstByPathPrecedence(p1.getRight(), pathsWithFileExistingSortedByUsage))).collect(toList());
        var contentsAssociated
            = associatedItemsWithContentsExisting.stream().map(p1 -> Pair.of(p1.getLeft(), getFirstByPathPrecedence(p1.getRight(), pathsWithFileExistingSortedByUsage))).collect(toList());

        var executionContext = createExecutionContext(contentMetadataItem, executionId, associatedItems, contentsMain, contentsAssociated);

        var strategy
            = executionPrepareStrategies.stream().filter(s -> s.isApplicable(executionContext, this)).findFirst().orElseThrow(() -> new InterruptedException("Could not find execution startegy cmi=" + contentMetadataItem));
        var readOnly = this.isReadOnlyExecutionCapable();
        try {
            strategy.doBeforeExecution(executionContext, readOnly);
            executionContext.setSetUpCompletedTimestamp(LocalDateTime.now());

            executeExternalApplication(executionContext);
        } catch (Exception e) { // NOPMD
            applicationEventPublisher.publishEvent(new ContentExecutionEvent(executionId, String.format("exec error %s", e.getMessage())));
            throw e;
        } finally {
            strategy.doAfterExecution(executionContext, readOnly);
        }
    }

    protected abstract void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException;

    private Optional<String> findExecutablePath() {
        return getExecutablePaths()
            .filter(
                s -> {
                    var flatpackApplicationId = StringUtils.substringAfter(s, "flatpak run").trim();
                    if (StringUtils.isNotBlank(flatpackApplicationId)) {
                        return packageFlatpakService.isInstalled(flatpackApplicationId);
                    }

                    var snapApplicationId = StringUtils.substringAfter(s, "snap run").trim();
                    if (StringUtils.isNotBlank(snapApplicationId)) {
                        return packageSnapService.isInstalled(snapApplicationId);
                    }
                    var p = Paths.get(s);
                    return Files.exists(p);
                })
            .findFirst();
    }

    public abstract Set<String> getContentExtensions();

    public Set<ContentSystem> getContentSystems() {
        return contentSystems;
    }

    public String getExecutablePath() throws IOException {
        return findExecutablePath().orElseThrow(() -> new IOException("Could not find executor in paths in " + getExecutablePaths().collect(joining(","))));
    }

    public abstract Stream<String> getExecutablePaths();

    private Content getFirstByPathPrecedence(Collection<Content> contents, List<Path> pathPrecedence) {
        for (var path : pathPrecedence) {
            for (Content content : contents) {
                if (content.getPathFileAbsolute().equals(path)) {
                    return content;
                }
            }
        }
        throw new WrappedException("Path not found in collection. This should never happen. Error in code  Contents=" + contents + " Pahs=" + pathPrecedence);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isFileSupported(String filename) {
        Set<String> contentExtensions = getContentExtensions();
        if (contentExtensions.isEmpty()) {
            return true;
        }
        return contentExtensions.contains(FilenameUtils.getExtension(filename).toLowerCase());
    }

    public boolean isFound() {
        return findExecutablePath().isPresent();
    }

    public boolean isMultipartCapable() {
        return multipartCapable;
    }

    @Override
    public boolean isProjectionExecutionCapable() {
        return true;
    }

    public boolean isReadOnlyExecutionCapable() {
        return true;
    }

    private Stream<Pair<ContentMetadataItem, List<Content>>> streamContentsWithContentMetadataItemPairsByContentMetadataIdAndFileExists(ContentMetadata contentMetadata) {
        // Should use isFileExistsAndValid to try to guarantee that files are not tampered
        return contentService.findContentsWithContentMetadataItemPairsByContentMetadataId(contentMetadata.getId())
            .stream()
            .map(p -> Pair.of(p.getLeft(), p.getRight().stream().filter(Content::isFileExists).collect(toList())))
            .filter(p -> !p.getRight().isEmpty());
    }
}
