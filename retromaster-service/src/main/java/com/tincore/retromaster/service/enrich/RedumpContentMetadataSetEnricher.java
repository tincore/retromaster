package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.retromaster.service.ContentSystemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.tincore.retromaster.domain.ContentMediaType.DISC;
import static com.tincore.retromaster.domain.ContentMetadataSet.AUTHOR_LENGTH;
import static com.tincore.retromaster.domain.ContentMetadataSource.REDUMP;

@Slf4j
@Service
@RequiredArgsConstructor
public class RedumpContentMetadataSetEnricher extends AbstractContentMetadataSetEnricher {

    private final ContentSystemService contentSystemService;
    private final RedumpContentMetadataEnricher contentMetadataEnricher;

    @Override
    public boolean canEnrich(ContentMetadataSet contentMetadataSet) {
        return REDUMP.isMatch(contentMetadataSet);
    }

    @Override
    public ContentMetadata enrichContentMetadata(ContentMetadata contentMetadata, String systemDefault) {
        contentMetadata.setMediaType(DISC); // Should eventually: Default from set
        contentMetadata.setSource(REDUMP.name());
        contentMetadata.setSystem(systemDefault);

        var title = StringUtils.substringBefore(contentMetadata.getName(), "(").trim();

        contentMetadataEnricher.enrich(title, contentMetadata);

        return contentMetadata;
    }

    @Override
    public ContentMetadataSet enrichContentMetadataSet(ContentMetadataSet contentMetadataSet) {
        contentMetadataSet.setCategory(getContentMetadataSource().toString());
        if (!contentMetadataSet.isStaging() || StringUtils.isBlank(contentMetadataSet.getRepositoryPath())) {
            var repositoryPath = getRepositoryPath(contentMetadataSet);
            contentMetadataSet.setRepositoryPath(repositoryPath);
        }
        contentMetadataSet.setAuthor(StringUtils.left(contentMetadataSet.getAuthor(), AUTHOR_LENGTH));
        return contentMetadataSet;
    }

    @Override
    public String extractSystem(ContentMetadataSet contentMetadataSet) {
        return contentSystemService.findSystemNormalized(contentMetadataSet.getName().replaceAll(" - ", " "));
    }

    @Override
    public ContentMetadataSource getContentMetadataSource() {
        return REDUMP;
    }

    @Override
    public String getRepositoryPath(ContentMetadataSet contentMetadataSet) {
        return extractSystem(contentMetadataSet) + " - " + contentMetadataSet.getVersion().trim() + " " + contentMetadataSet.getCategory();
    }
}
