package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ExecutionContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentExtension.*;

@Service
public class Pcsx2ContentExecutor extends AbstractContentExecutor {
    private static final Set<String> CONTENT_EXTENSIONS = Stream.of(iso, ciso, cso).map(Enum::name).collect(Collectors.toSet());

    private final RetromasterConfiguration.Pcsx2ExecuteConfiguration pcsx2ExecuteConfiguration;

    public Pcsx2ContentExecutor(RetromasterConfiguration retromasterConfiguration) {
        super("PCSX2", "Pcsx2", false, ContentSystem.SONY_PLAYSTATION_2);
        this.pcsx2ExecuteConfiguration = retromasterConfiguration.getExecute().getPcsx2();
    }

    @Override
    public void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException {
        var contentParam = StringUtils.wrapIfMissing(executionContext.getMainItemPath().toString(), "\"");

        var cmd = String.format("%s %s", getExecutablePath(), contentParam);
        exec(cmd, executionContext.getExecutionId());
    }

    @Override
    public Set<String> getContentExtensions() {
        return CONTENT_EXTENSIONS;
    }

    public Stream<String> getExecutablePaths() {
        return pcsx2ExecuteConfiguration.getExecutablePaths().stream();
    }
}
