package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentExtension;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ExecutionContext;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class RetroarchContentExecutor extends AbstractContentExecutor {

    private final RetromasterConfiguration.RetroarchExecuteConfiguration retroarchExecuteConfiguration;

    private final String coreName;
    private final String coreVariant;
    private final String[] coreOptions;
    private final String[] paramsExtra;
    private final Set<String> contentExtensions;


    public RetroarchContentExecutor(Configuration configuration) {
        super(
            "RETROARCH_" + configuration.getCoreName().toUpperCase() + (configuration.getCoreVariant() == null ? "" : "_" + configuration.getCoreVariant().toUpperCase()),
            "Retroarch " + (configuration.getCoreLabel() == null ? StringUtils.capitalize(StringUtils.replace(configuration.getCoreName(), "_", " ")) : configuration.getCoreLabel()),
            configuration.isMultipartCapable(),
            configuration.getContentSystems());

        this.contentExtensions = toStream(configuration.getContentExtensions()).map(Enum::name).collect(Collectors.toSet());
        this.coreName = configuration.getCoreName();
        this.coreVariant = configuration.getCoreVariant();
        this.coreOptions = configuration.getCoreOptions();
        this.paramsExtra = configuration.getParamsExtra();
        this.retroarchExecuteConfiguration = configuration.getRetroarchExecuteConfiguration();
    }

    @Override
    public void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException {
        Path m3uPath = null;
        try {
            List<String> params = new ArrayList<>();
            params.add(getRetroarchCoreParam());
            if (paramsExtra != null) {
                params.addAll(asList(paramsExtra));
            }

            if (retroarchExecuteConfiguration.isVerbose()) {
                params.add("-v");
            }

            var retroarchDataRootPath = Files.createDirectories(retroarchExecuteConfiguration.getConfigurationPath());

            var appendConfigPath = retroarchDataRootPath.resolve("retroarch.append.cfg");

            var saveStatesPath = Files.createDirectories(retroarchDataRootPath.resolve("savestates"));
            var savesPath = Files.createDirectories(retroarchDataRootPath.resolve("saves"));
            var appendConfigContent = new StringBuilder()
                .append("video_fullscreen = \"")
                .append(retroarchExecuteConfiguration.isFullscreen())
                .append('\"')
                .append(EOL)
                .append("savestate_directory = \"")
                .append(saveStatesPath)
                .append('\"')
                .append(EOL)
                .append("savefile_directory = \"")
                .append(savesPath)
                .append('\"')
                .append(EOL);

            if (coreOptions == null) {
                appendConfigContent.append("core_options_path = \"").append(retroarchDataRootPath.resolve("retroarch.core.default.cfg")).append('"').append(EOL);
            } else {
                var coreOptionsContent = new StringBuilder();
                Stream.of(coreOptions).forEach(c -> coreOptionsContent.append(c).append(EOL));

                var retromasterCoreOptionsPath = Files.write(retroarchDataRootPath.resolve("retroarch.core." + coreName + (coreVariant == null ? "" : "_" + coreVariant) + ".cfg"), coreOptionsContent.toString().getBytes());
                appendConfigContent.append("core_options_path = \"").append(retromasterCoreOptionsPath).append('"').append(EOL);
            }
            params.add("--appendconfig=" + Files.write(appendConfigPath, appendConfigContent.toString().getBytes()));

            var mainItemPath = executionContext.getMainItemPath();
            var mainItemPathExtension = FilenameUtils.getExtension(mainItemPath.toString());
            if (isMultipartCapable() && !executionContext.getOtherItemPaths().isEmpty()) {
                var playlistsPath = Files.createDirectories(retroarchDataRootPath.resolve("playlist"));
                var playlistPath = playlistsPath.resolve(executionContext.getMainItem().getContentMetadata().getName() + ".m3u");
                m3uPath = playlistPath;
                var playListContent = new StringBuilder().append(playlistsPath.relativize(mainItemPath)).append(EOL);
                var extraItemsPaths = executionContext.getOtherItemPaths().stream().sorted().toList();
                extraItemsPaths.stream().filter(p -> FilenameUtils.getExtension(p.toString()).equals(mainItemPathExtension)).forEach(p -> playListContent.append(playlistPath.relativize(p)).append(EOL));
                Files.write(m3uPath, playListContent.toString().getBytes());

                params.add(StringUtils.wrapIfMissing(m3uPath.toString(), "\""));
            } else {
                params.add(StringUtils.wrapIfMissing(mainItemPath.toString(), "\""));
            }

            var cmd = String.format("%s %s", getExecutablePath(), String.join(" ", params));
            exec(cmd, executionContext.getExecutionId());
        } finally {
            if (m3uPath != null) {
                Files.deleteIfExists(m3uPath);
            }
        }
    }

    private Optional<Path> findCoreLibPath() {
        var libName = getLibName();

        return retroarchExecuteConfiguration.getCorePaths().stream().map(p -> Paths.get(p).resolve(libName)).filter(Files::exists).findFirst();
    }

    @Override
    public Set<String> getContentExtensions() {
        return contentExtensions;
    }

    @Override
    public Stream<String> getExecutablePaths() {
        return retroarchExecuteConfiguration.getExecutablePaths().stream();
    }

    private String getLibName() {
        var libName = coreName + "_libretro.so";
        if (SystemUtils.IS_OS_MAC) {
            libName = coreName + "_libretro.dylib";
        }
        return libName;
    }

    public String getRetroarchCoreParam() {
        return findCoreLibPath().map(p -> String.format("-L %s", p)).orElseThrow(() -> new RuntimeException("Could not find any core library for " + coreName));
    }

    @Override
    public boolean isFound() {
        return super.isFound() && findCoreLibPath().isPresent();
    }

    @Data
    @Builder
    public static class Configuration {
        private boolean multipartCapable;
        private ContentSystem[] contentSystems;
        private ContentExtension[] contentExtensions;
        private String coreName;
        private String coreLabel;
        private String coreVariant;
        private String[] coreOptions;
        private String[] paramsExtra;

        private RetromasterConfiguration.RetroarchExecuteConfiguration retroarchExecuteConfiguration;

        public static class ConfigurationBuilder {
            private ContentSystem[] contentSystems; // NOPMD Builder!
            private ContentExtension[] contentExtensions = CONTENT_EXTENSIONS_EMPTY; // NOPMD Builder!
            private String[] coreOptions; // NOPMD Builder!

            public ConfigurationBuilder contentExtensions(ContentExtension... contentExtensions) {
                this.contentExtensions = contentExtensions;
                return this;
            }

            public ConfigurationBuilder contentSystems(ContentSystem... contentSystems) {
                this.contentSystems = contentSystems;
                return this;
            }

            public ConfigurationBuilder coreOptions(String... coreOptions) {
                this.coreOptions = coreOptions;
                return this;
            }

            public ConfigurationBuilder paramsExtra(String... paramsExtra) {
                this.paramsExtra = paramsExtra;
                return this;
            }
        }
    }
}
