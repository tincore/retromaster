package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.service.AbstractRepositoryServiceExtensionImpl;
import com.tincore.retromaster.config.TaskExecutorConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentSignatureChange;
import com.tincore.util.Partition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

@Slf4j
@Service
public class ContentRepositoryServiceExtensionImpl extends AbstractRepositoryServiceExtensionImpl<Content, UUID> implements ContentRepositoryServiceExtension {

    private static final int PARTITION_SIZE = 1000;
    private static final int LOG_BATCH = 20;
    private final TransactionTemplate transactionRequiresNewTemplate;
    private final TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider;

    private final ContentSignatureChangeRepositoryService contentSignatureChangeRepositoryService;

    @Autowired
    public ContentRepositoryServiceExtensionImpl(
        EntityManager entityManager,
        ContentSignatureChangeRepositoryService contentSignatureChangeRepositoryService,
        TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider,
        TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate) {
        super(Content.class, entityManager);
        this.forkJoinPoolProvider = forkJoinPoolProvider;
        this.transactionRequiresNewTemplate = transactionRequiresNewTemplate;

        this.contentSignatureChangeRepositoryService = contentSignatureChangeRepositoryService;
    }

    @Override
    public void deleteAndCreateChange(Content content) {
        deleteAndCreateChange(content, false);
    }

    @Override
    public void deleteAndCreateChange(Content content, boolean checkSignatureChangeExistence) {
        super.delete(content);
        if (!checkSignatureChangeExistence || !contentSignatureChangeRepositoryService.existsById(content.getSignature())) {
            contentSignatureChangeRepositoryService.save(new ContentSignatureChange(content.getSignature()));
        }
    }

    @Override
    public <S extends Content> List<S> saveAllAndCreateChange(Iterable<S> contents) {
        var savedContents = super.saveAll(contents);
        contentSignatureChangeRepositoryService.saveAll(savedContents.stream().map(Content::getSignature));
        return savedContents;
    }

    @Override
    public Content saveAndCreateChange(Content content) {
        var savedContent = super.save(content);
        contentSignatureChangeRepositoryService.save(new ContentSignatureChange(content.getSignature()));
        return savedContent;
    }

    @Override
    public boolean saveBatch(List<Content> contents) throws ExecutionException, InterruptedException {
        var partition = Partition.ofSize(contents, PARTITION_SIZE);
        var count = new AtomicInteger();
        log.trace("saveBatch START {}/{}", count.get(), partition.size());
        forkJoinPoolProvider.submitNow(
            () -> IntStream.range(0, partition.size())
                .parallel()
                .forEach(
                    i -> {
                        count.incrementAndGet();
                        if (count.get() % LOG_BATCH == 0) {
                            log.trace("saveBatch {}/{}", count.get(), partition.size());
                        }
                        var batch = partition.get(i);
                        transactionRequiresNewTemplate.execute(
                            ts -> {
                                saveAllAndCreateChange(batch);
                                return true;
                            });
                    }));
        log.trace("saveBatch DONE {}/{}", count.get(), partition.size());
        return true;
    }
}
