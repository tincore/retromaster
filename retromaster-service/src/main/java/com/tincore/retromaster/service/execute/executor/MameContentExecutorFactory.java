package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import static com.tincore.retromaster.domain.ContentExtension.*;
import static com.tincore.retromaster.domain.ContentSystem.*;

@Component
public class MameContentExecutorFactory { // NOPMD Cyclomatic BS

    private static final int C4 = 4;


    @Bean
    public MameContentExecutor mame32xContentExecutor() {
        return new MameContentExecutor("32x", SEGA_32X).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameA1200ContentExecutor() {
        return new MameContentExecutor("a1200", COMMODORE_AMIGA).floppyCount(1);
    }

    @Bean
    public MameContentExecutor mameA1200nContentExecutor() {
        return new MameContentExecutor("a1200n", COMMODORE_AMIGA).floppyCount(1);
    }

    @Bean
    public MameContentExecutor mameA500ContentExecutor() {
        return new MameContentExecutor("a500", COMMODORE_AMIGA).floppyCount(1);
    }

    @Bean
    public MameContentExecutor mameA500nContentExecutor() {
        return new MameContentExecutor("a500n", COMMODORE_AMIGA).floppyCount(1);
    }

    @Bean
    public MameContentExecutor mameA500pContentExecutor() {
        return new MameContentExecutor("a500p", COMMODORE_AMIGA).floppyCount(1);
    }

    @Bean
    public MameContentExecutor mameA500pnContentExecutor() {
        return new MameContentExecutor("a500pn", COMMODORE_AMIGA).floppyCount(1);
    }

    @Bean
    public MameContentExecutor mameAa540ContentExecutor() {
        return new MameContentExecutor("aa540", ACORN_ARCHIMEDES).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameAdamContentExecutor() {
        return new MameContentExecutor("adam", COLECO_COLECOVISION_ADAM).cartCount(C4).floppyCount(2).cassetteCount(2);
    }

    @Bean
    public MameContentExecutor mameApple2cContentExecutor() {
        return new MameContentExecutor("apple2c", APPLE_2).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameApple2cpContentExecutor() {
        return new MameContentExecutor("apple2cp", APPLE_2).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameApple2eContentExecutor() {
        return new MameContentExecutor("apple2e", APPLE_2).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameApple2gsContentExecutor() {
        return new MameContentExecutor("apple2gs", APPLE_2GS) {
            @Override
            public String formatContentArgument(String contentArgument) {
                if (_2mg.isExtensionOf(contentArgument)) {
                    return formatContentArgument("flop3", contentArgument);
                }
                return super.formatContentArgument(contentArgument);
            }
        }.floppyCount(C4);
    }

    @Bean
    public MameContentExecutor mameApple3ContentExecutor() {
        return new MameContentExecutor("apple3", APPLE_3).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameArchimedesContentExecutor() {
        return new MameContentExecutor("apple1", APPLE_1).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameAtari2600ContentExecutor() {
        return new MameContentExecutor("a2600", ATARI_2600).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameAtari400ContentExecutor() {
        return new MameContentExecutor("a400", ATARI_8BIT).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameAtari5200ContentExecutor() {
        return new MameContentExecutor("a5200", ATARI_5200).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameAtari7800ContentExecutor() {
        return new MameContentExecutor("a7800", ATARI_7800).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameAtari800ContentExecutor() {
        return new MameContentExecutor("a800", ATARI_8BIT).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameAtomContentExecutor() {
        return new MameContentExecutor("atom", ACORN_ATOM).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameBbcBContentExecutor() {
        return new MameContentExecutor("bbcb", ACORN_BBC).romCount(C4).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameBbcMContentExecutor() {
        return new MameContentExecutor("bbcm", ACORN_BBC).romCount(C4).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameC116ContentExecutor() {
        return new MameContentExecutor("c116", COMMODORE_C16_C116_PLUS_4).cartCount(1).floppyCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameC128ContentExecutor() {
        return new MameContentExecutor("c128", COMMODORE_C128).cartCount(1).floppyCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameC16ContentExecutor() {
        return new MameContentExecutor("c16", COMMODORE_C16_C116_PLUS_4).cartCount(1).floppyCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameC64ContentExecutor() {
        return new MameContentExecutor("c64", COMMODORE_C64).cartCount(1).floppyCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCanonV10ContentExecutor() {
        return new MameContentExecutor("canonv10", MICROSOFT_MSX_1)
            .contentExtensions(wav, tap, cas, mx1, bin, rom)
            .cartCount(2)
            .cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCanonV20ContentExecutor() {
//        return new MameContentExecutor("canonv20e", MICROSOFT_MSX_1) // UK
//        return new MameContentExecutor("canonv20f", MICROSOFT_MSX_1) // FR
//        return new MameContentExecutor("canonv20g", MICROSOFT_MSX_1) // DE
//        return new MameContentExecutor("canonv20s", MICROSOFT_MSX_1) // ES
        return new MameContentExecutor("canonv20", MICROSOFT_MSX_1)
            .contentExtensions(wav, tap, cas, mx1, bin, rom)
            .cartCount(2)
            .cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCanonV25ContentExecutor() {
        return new MameContentExecutor("canonv25", MICROSOFT_MSX_2, MICROSOFT_MSX_1).cartCount(2).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCanonV30FContentExecutor() {
        return new MameContentExecutor("canonv30f", MICROSOFT_MSX_2, MICROSOFT_MSX_1).cartCount(2).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCanonV8ContentExecutor() {
        return new MameContentExecutor("canonv8", MICROSOFT_MSX_1)
            .contentExtensions(wav, tap, cas, mx1, bin, rom)
            .cartCount(1)
            .cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCanonX07ContentExecutor() {
        return new MameContentExecutor("x07", CANON_X_07)
            .contentExtensions(wav, k7, /*lst,*/ cas, bin, rom)
            .cartCount(1)
            .cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCasioLoopyContentExecutor() {
        return new MameContentExecutor("casloopy", CASIO_LOOPY).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameColecoContentExecutor() {
        return new MameContentExecutor("coleco", COLECO_COLECOVISION).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameColecopContentExecutor() {
        return new MameContentExecutor("colecop", COLECO_COLECOVISION).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameCpc464pContentExecutor() {
        return new MameContentExecutor("cpc464p", AMSTRAD_CPCP, AMSTRAD_GX4000).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCpc6128ContentExecutor() {
        return new MameContentExecutor("cpc6128", AMSTRAD_CPC).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameCpc6128pContentExecutor() {
        return new MameContentExecutor("cpc6128p", AMSTRAD_CPCP, AMSTRAD_GX4000).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameDcContentExecutor() {
        return new MameContentExecutor("dc", SEGA_DREAMCAST).discCount(1);
    }

    @Bean
    public MameContentExecutor mameDcEuContentExecutor() {
        return new MameContentExecutor("dceu", SEGA_DREAMCAST).discCount(1);
    }

    @Bean
    public MameContentExecutor mameDcJpContentExecutor() {
        return new MameContentExecutor("dcjp", SEGA_DREAMCAST).discCount(1);
    }

    @Bean
    public MameContentExecutor mameElectronContentExecutor() {
        return new MameContentExecutor("electron", ACORN_ELECTRON).cartCount(1).floppyCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameGameGearContentExecutor() {
        return new MameContentExecutor("gamegear", SEGA_GAMEGEAR).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameGameGearJContentExecutor() {
        return new MameContentExecutor("gamegearj", SEGA_GAMEGEAR).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameGameboyAdvanceContentExecutor() {
        return new MameContentExecutor("gba", NINTENDO_GAMEBOY_ADVANCE) {
            @Override
            public String formatContentArgument(String contentArgument) {
                if (raw.isExtensionOf(contentArgument)) {
                    return formatContentArgument(getContentParamCart(), contentArgument);
                }
                return super.formatContentArgument(contentArgument);
            }
        }.cartCount(1)
            .floppyCount(2)
            .cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameGameboyColorContentExecutor() {
        return new MameContentExecutor("gbcolor", NINTENDO_GAMEBOY_COLOR).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameGameboyContentExecutor() {
        return new MameContentExecutor("gameboy", NINTENDO_GAMEBOY).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameGbpocketContentExecutor() {
        return new MameContentExecutor("gbpocket", NINTENDO_GAMEBOY_POCKET).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameGenesisContentExecutor() {
        return new MameContentExecutor("genesis", SEGA_MEGADRIVE).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameGx4000ContentExecutor() {
        return new MameContentExecutor("gx4000", AMSTRAD_GX4000).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    // intv2 Intellivision II
    @Bean
    public MameContentExecutor mameIntv2ContentExecutor() {
        return new MameContentExecutor("intv2", MATTEL_INTELLIVISION).cartCount(1).cassetteCount(1);
    }

    // intv Intellivision
    @Bean
    public MameContentExecutor mameIntvContentExecutor() {
        return new MameContentExecutor("intv", MATTEL_INTELLIVISION).cartCount(1).cassetteCount(1);
    }

    // intvecs Intellivision w/Entertainment Computer System + Intellivoice expansions
    @Bean
    public MameContentExecutor mameIntvecsContentExecutor() {
        return new MameContentExecutor("intvecs", MATTEL_INTELLIVISION).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameJaguarContentExecutor() {
        return new MameContentExecutor("jaguar", ATARI_JAGUAR).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameLynxContentExecutor() {
        return new MameContentExecutor("lynx", ATARI_LYNX).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameMegaCd2ContentExecutor() {
        return new MameContentExecutor("megacd2", SEGA_CD).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameMegaCd2JContentExecutor() {
        return new MameContentExecutor("megacd2j", SEGA_CD).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameMegaCdAContentExecutor() {
        return new MameContentExecutor("megacda", SEGA_CD).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameMegaCdContentExecutor() {
        return new MameContentExecutor("megacd", SEGA_CD).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameMegaCdJContentExecutor() {
        return new MameContentExecutor("megacdj", SEGA_CD).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameMegadrijContentExecutor() {
        return new MameContentExecutor("megadrij", SEGA_MEGADRIVE).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameMegadrivContentExecutor() {
        return new MameContentExecutor("megadriv", SEGA_MEGADRIVE).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameN64ContentExecutor() {
        return new MameContentExecutor("n64", NINTENDO_64).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameNGPCContentExecutor() {
        return new MameContentExecutor("ngpc", SNK_NEOGEO_POCKET_COLOR).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameNGPContentExecutor() {
        return new MameContentExecutor("ngp", SNK_NEOGEO_POCKET).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameNeoCdContentExecutor() {
        return new MameContentExecutor("neocd", SNK_NEOGEO_CD).discCount(1);
    }

    @Bean
    public MameContentExecutor mameNeoCdZContentExecutor() {
        return new MameContentExecutor("neocdz", SNK_NEOGEO_CD).discCount(1);
    }

    @Bean
    public MameContentExecutor mameNeoCdZJContentExecutor() {
        return new MameContentExecutor("neocdzj", SNK_NEOGEO_CD).discCount(1);
    }

    @Bean
    public MameContentExecutor mameNesContentExecutor() {
        return new MameContentExecutor("nes", NINTENDO_NES)
            .cartCount(1)
            .cassetteCount(1)
            .floppyCount(2);
    }

    @Bean
    public MameContentExecutor mameNesPalContentExecutor() {
        return new MameContentExecutor("nespal", NINTENDO_NES)
            .cartCount(1)
            .cassetteCount(1)
            .floppyCount(2);
    }

    @Bean
    public MameContentExecutor mamePcfxContentExecutor() {
        return new MameContentExecutor("pcfx", NEC_PCFX)
            .discCount(1);
    }

    @Bean
    public MameContentExecutor mamePhilipsNms801ContentExecutor() {
        return new MameContentExecutor("nms801", MICROSOFT_MSX_1)
            .contentExtensions(wav, tap, cas)
            .cartCount(0)
            .cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mamePhilipsNms8220ContentExecutor() {
        return new MameContentExecutor("nms8220", MICROSOFT_MSX_2, MICROSOFT_MSX_1).cartCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mamePhilipsNms8245ContentExecutor() {
//    nms8245           NMS 8245 (MSX2, Europe)                   (Philips, 1987)
//    nms8245f          NMS 8245F (MSX2, France)                  (Philips, 1987)
        return new MameContentExecutor("nms8245", MICROSOFT_MSX_2, MICROSOFT_MSX_1).cartCount(2).floppyCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mamePhilipsNms8250ContentExecutor() {
        return new MameContentExecutor("nms8250", MICROSOFT_MSX_2, MICROSOFT_MSX_1).cartCount(2).floppyCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mamePhilipsNms8255ContentExecutor() {
        return new MameContentExecutor("nms8255", MICROSOFT_MSX_2, MICROSOFT_MSX_1).cartCount(2).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mamePhilipsNms8260ContentExecutor() {
        return new MameContentExecutor("nms8260", MICROSOFT_MSX_2, MICROSOFT_MSX_1).cartCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mamePhilipsNms8280ContentExecutor() {
//    nms8280           NMS 8280 (MSX2, Europe)                   (Philips, 1987)
//    nms8280f          NMS 8280F (MSX2, France)                  (Philips, 1986)
//    nms8280g          NMS 8280G (MSX2, Germany)                 (Philips, 1986)
        return new MameContentExecutor("nms8280", MICROSOFT_MSX_2, MICROSOFT_MSX_1).cartCount(2).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mamePlus4ContentExecutor() {
        return new MameContentExecutor("plus4", COMMODORE_C16_C116_PLUS_4).cartCount(1).floppyCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mamePs2ContentExecutor() {
        return new MameContentExecutor("ps2", SONY_PLAYSTATION_2).discCount(1);
    }

    @Bean
    public MameContentExecutor mamePsaContentExecutor() {
        return new MameContentExecutor("psa", SONY_PLAYSTATION).discCount(1);
    }

    @Bean
    public MameContentExecutor mamePseContentExecutor() {
        return new MameContentExecutor("pse", SONY_PLAYSTATION).discCount(1);
    }

    @Bean
    public MameContentExecutor mamePsjContentExecutor() {
        return new MameContentExecutor("psj", SONY_PLAYSTATION).discCount(1);
    }

    @Bean
    public MameContentExecutor mamePsuContentExecutor() {
        return new MameContentExecutor("psu", SONY_PLAYSTATION).discCount(1);
    }

    @Bean
    public MameContentExecutor mameSaturnContentExecutor() {
        return new MameContentExecutor("saturn", SEGA_SATURN).cartCount(1).discCount(1);
    }

    @Bean
    public MameContentExecutor mameSaturnEuContentExecutor() {
        return new MameContentExecutor("saturneu", SEGA_SATURN).cartCount(1).discCount(1);
    }

    @Bean
    public MameContentExecutor mameSaturnJpContentExecutor() {
        return new MameContentExecutor("saturnjp", SEGA_SATURN).cartCount(1).discCount(1);
    }

    @Bean
    public MameContentExecutor mameSegaCd2ContentExecutor() {
        return new MameContentExecutor("segacd2", SEGA_CD).discCount(1);
    }

    @Bean
    public MameContentExecutor mameSegaCdContentExecutor() {
        return new MameContentExecutor("segacd", SEGA_CD).discCount(1);
    }

    @Bean
    public MameContentExecutor mameSg1000ContentExecutor() {
        return new MameContentExecutor("sg1000", SEGA_GAME_1000).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameSgxContentExecutor() {
        return new MameContentExecutor("sgx", NEC_SUPERGRAFX).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameSmsContentExecutor() {
        return new MameContentExecutor("sms", SEGA_MASTERSYSTEM).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameSnesContentExecutor() {
        return new MameContentExecutor("snes", NINTENDO_SNES).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameSonyHbF1XVContentExecutor() {
        return new MameContentExecutor("hbf1xv", MICROSOFT_MSX_1, MICROSOFT_MSX_2, MICROSOFT_MSX_2P).cartCount(2).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameSpectrum128ContentExecutor() {
        return new MameContentExecutor("spec128", SINCLAIR_ZX_SPECTRUM, SINCLAIR_ZX_SPECTRUM_128)
            .contentExtensions(dsk, ima, img, ufi, _360, d77, d88, _1dd, dfi, hfe, imd, ipf, mfi, mfm, td0, cqm, cqi)
            .cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameSpectrumContentExecutor() {
        return new MameContentExecutor("spectrum", SINCLAIR_ZX_SPECTRUM).cartCount(1).floppyCount(2).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameStContentExecutor() {
        return new MameContentExecutor("st", ATARI_ST).cartCount(1).floppyCount(1);
    }

    @Bean
    public MameContentExecutor mameT1000RLContentExecutor() {
        return new MameContentExecutor("t1000rl", IBM_PC)
            .floppyCount(1);
    }

    @Bean
    public MameContentExecutor mameTG16ContentExecutor() {
        return new MameContentExecutor("tg16", NEC_TURBOGRAFX).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameVboyContentExecutor() {
        return new MameContentExecutor("vboy", NINTENDO_VIRTUAL_BOY).cartCount(1);
    }

    @Bean
    public MameContentExecutor mameVectrexContentExecutor() {
        return new MameContentExecutor("vectrex", GCE_VECTREX).cartCount(1).cassetteCount(1);
    }

    @Bean
    public MameContentExecutor mameVic20ContentExecutor() {
        return new MameContentExecutor("vic20", COMMODORE_VIC20).cartCount(1).floppyCount(1).cassetteCount(1);
    }
}
