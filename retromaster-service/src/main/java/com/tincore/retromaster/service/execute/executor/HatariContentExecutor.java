package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentExtension;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ExecutionContext;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

public class HatariContentExecutor extends AbstractContentExecutor {

    private final RetromasterConfiguration.HatariExecuteConfiguration hatariExecuteConfiguration;
    private final ContentSystem mainContentSystem;

    public HatariContentExecutor(RetromasterConfiguration.HatariExecuteConfiguration hatariExecuteConfiguration, ContentSystem... contentSystems) {
        super("HATARI_" + contentSystems[0].name(), "Hatari - " + contentSystems[0].getLabel(), true, contentSystems);
        this.hatariExecuteConfiguration = hatariExecuteConfiguration;
        this.mainContentSystem = contentSystems[0];
    }

    @Override
    public void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException {
        List<String> params = new ArrayList<>();

        Optional.ofNullable(getModelParam()).ifPresent(p -> params.add("--machine " + p));

        if (ContentExtension.isDisk(executionContext.getMainItemPath()) && !executionContext.getOtherItemPaths().isEmpty()) {
            params.add("--disk-a " + StringUtils.wrapIfMissing(executionContext.getMainItemPath().toString(), "\""));
            params.add("--disk-b " + StringUtils.wrapIfMissing(executionContext.getOtherItemPaths().get(0).toString(), "\""));
        } else {
            params.add(StringUtils.wrapIfMissing(executionContext.getMainItemPath().toString(), "\""));
        }

        var cmd = String.format("%s %s", getExecutablePath(), String.join(" ", params));
        exec(cmd, executionContext.getExecutionId());
    }

    @Override
    public Set<String> getContentExtensions() {
        return Collections.emptySet();
    }

    public Stream<String> getExecutablePaths() {
        return hatariExecuteConfiguration.getExecutablePaths().stream();
    }

    private String getModelParam() {
        if (ContentSystem.ATARI_ST.equals(mainContentSystem)) {
            return "st";
        }
        if (ContentSystem.ATARI_STE.equals(mainContentSystem)) {
            return "ste";
        }
        if (ContentSystem.ATARI_MEGAST.equals(mainContentSystem)) {
            return "megast";
        }
        if (ContentSystem.ATARI_MEGASTE.equals(mainContentSystem)) {
            return "megaste";
        }
        if (ContentSystem.ATARI_TT.equals(mainContentSystem)) {
            return "tt";
        }
        if (ContentSystem.ATARI_FALCON.equals(mainContentSystem)) {
            return "falcon";
        }

        return null;
    }
}
