package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.service.AbstractRepositoryServiceExtensionImpl;
import com.tincore.retromaster.config.TaskExecutorConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.util.Partition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

@Slf4j
@Service
public class ContentMetadataItemRepositoryServiceExtensionImpl extends AbstractRepositoryServiceExtensionImpl<ContentMetadataItem, UUID> implements ContentMetadataItemRepositoryServiceExtension {

    private static final int CHUNK_SIZE = 25;
    private static final int TRACE_LOG_ITEM_COUNT = 20;

    private final TransactionTemplate transactionRequiresNewTemplate;
    private final TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider;

    @Autowired
    public ContentMetadataItemRepositoryServiceExtensionImpl(EntityManager entityManager, TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider, TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate) {
        super(ContentMetadataItem.class, entityManager);
        this.forkJoinPoolProvider = forkJoinPoolProvider;
        this.transactionRequiresNewTemplate = transactionRequiresNewTemplate;
    }

    @Override
    public boolean saveBatch(List<ContentMetadataItem> contentMetadataItems) throws ExecutionException, InterruptedException {
        var partition = Partition.ofSize(contentMetadataItems, CHUNK_SIZE);
        var count = new AtomicInteger();
        log.info("saveBatch START {}/{}", count.get(), partition.size());
        forkJoinPoolProvider.submitNow(
            () -> IntStream.range(0, partition.size())
                .parallel()
                .forEach(
                    i -> {
                        count.incrementAndGet();
                        if (count.get() % TRACE_LOG_ITEM_COUNT == 0) {
                            log.debug("saveBatch {}/{}", count.get(), partition.size());
                        } else {
                            log.trace("saveBatch {}/{}", count.get(), partition.size());
                        }
                        var batch = partition.get(i);
                        transactionRequiresNewTemplate.execute(transactionStatus -> saveAll(batch));
                    }));

        log.info("saveBatch DONE {}/{}", count.get(), partition.size());
        return true;
    }
}
