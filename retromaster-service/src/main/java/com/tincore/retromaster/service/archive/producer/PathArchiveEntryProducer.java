package com.tincore.retromaster.service.archive.producer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class PathArchiveEntryProducer implements ArchiveEntryProducer {
    private final Path path;
    private final Path basePath; // Used to relativize when creating the target archive
    private final String targetName;
    private final BasicFileAttributes basicFileAttributes;

    private InputStream inputStream;

    public PathArchiveEntryProducer(Path path, Path basePath, String targetName) throws IOException {
        this.path = path;
        this.basePath = basePath;
        this.basicFileAttributes = Files.readAttributes(path, BasicFileAttributes.class);
        this.targetName = targetName;
    }

    @Override
    public void close() throws IOException {
        if (inputStream != null) {
            inputStream.close();
            inputStream = null;
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        inputStream = Files.newInputStream(path);
        return inputStream;
    }

    @Override
    public LocalDateTime getModifiedTime() throws IOException {
        return LocalDateTime.ofInstant(Files.getLastModifiedTime(path).toInstant(), ZoneId.systemDefault());
    }

    @Override
    public String getName() {
        if (targetName != null) {
            return targetName;
        }
        var relativePath = basePath.relativize(path).toString();
        return basicFileAttributes.isRegularFile() ? StringUtils.isNotBlank(relativePath) ? relativePath : path.getFileName().toString() : relativePath;
    }

    @Override
    public long getSize() {
        return basicFileAttributes.size();
    }

    @Override
    public boolean isDirectory() {
        return basicFileAttributes.isDirectory();
    }

    @Override
    public boolean isRegularFile() {
        return basicFileAttributes.isRegularFile();
    }
}
