package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentExecuteOptionSet;
import com.tincore.retromaster.domain.ContentMetadataItemExecuteOptionSet;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ContentExecuteOptionSetService {

    private final ContentExecutorService contentExecutorService;
    private final ContentSystemService contentSystemService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;

    @Transactional(readOnly = true)
    public ContentExecuteOptionSet getContentExecuteOptionSets(Content content) {
        var contentMetadataItems = contentMetadataItemRepositoryService.findBySignature(content.getSignature());
        var items = contentMetadataItems.stream()
            .map(cmi -> {
                var contentSystems = contentSystemService.findByContentMetadata(cmi.getContentMetadata());
                return new ContentMetadataItemExecuteOptionSet(cmi, contentExecutorService.findContentExecutorsByContentMetadataItem(cmi, contentSystems));
            })
            .toList();
        return new ContentExecuteOptionSet(content, items);
    }
}
