package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.Partition;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentMetadataSetService implements StreamTrait, FileSystemTrait {

    private final ContentMetadataVolumeService contentMetadataVolumeService;
    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;
    private final TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;
    private final RetromasterConfiguration retromasterConfiguration;

    public void delete(ContentMetadataSet contentMetadataSet) {
        var contentMetadataSetFilePath = contentMetadataVolumeService.getProdPath(contentMetadataSet.getFilename());
        delete(contentMetadataSet, contentMetadataSetFilePath);
    }

    public void delete(ContentMetadataSet contentMetadataSet, Path contentMetadataSetFilePath) {
        var contentMetadataIds = contentMetadataRepositoryService.findContentMetadataIdsByContentMetadataSet(contentMetadataSet);

        log.debug("deleteContentMetadataSet name={}, contentMetadatasCount={}", contentMetadataSet.getName(), contentMetadataIds.size());
        var partition = Partition.ofSize(contentMetadataIds, retromasterConfiguration.getMetadataSetDeleteMetadataBatchSize());
        IntStream.range(0, partition.size())
            .forEach(
                i -> {
                    if (log.isInfoEnabled()) {
                        log.info("deleting contentMetadatas {}/{}", i + 1, partition.size());
                    }
                    var batch = partition.get(i);
                    transactionRequiresNewTemplate.execute(
                        ts -> {
                            batch.forEach(contentMetadataItemRepositoryService::deleteNativeByContentMetadataId);
                            return true;
                        });
                });

        log.debug("deleting contentMetadataSet name={}", contentMetadataSet.getName());
        transactionRequiresNewTemplate.execute(
            new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    contentMetadataSetRepositoryService.delete(contentMetadataSet);
                }
            });

        if (contentMetadataSetFilePath != null) {
            try {
                Files.deleteIfExists(contentMetadataSetFilePath);
            } catch (IOException e) {
                log.error("deleting contentMetadataSet. File removal failed. name={}, path={}", contentMetadataSet.getName(), contentMetadataSetFilePath, e);
            }
        }
    }

    @Transactional
    public int deleteContentMetadataSetsByMissingDatFiles() {
        try {
            log.info("Deleting ContentMetadataSets with missing dat files");
            List<Path> paths;
            try (var s = findPaths(contentMetadataVolumeService.getProdPath())) {
                paths = s.filter(this::isContentMetadataSetFileSupported).toList();
            }
            var pathFilenames = paths.stream().map(Path::getFileName).map(Path::toString).collect(Collectors.toSet());
            var count = new AtomicInteger();
            contentMetadataSetRepositoryService.findAll()
                .stream()
                .filter(c -> !pathFilenames.contains(c.getFilename()))
                .filter(c -> !c.isStaging())
                .forEach(
                    cms -> {
                        log.debug("Found contentMetadataSet with missing dat files. Should be deleted. name={}", cms.getName());
                        delete(cms, null);
                        count.incrementAndGet();
                    });
            log.info("Deleting ContentMetadataSets with missing dat files completed, count={}", count);
            return count.get();
        } catch (IOException e) {
            log.error("Deleting ContentMetadataSets with missing dat files failed", e);
            return -1;
        }
    }

    @Transactional
    public int deleteContentMetadataSetsByObsoleteVersion() {
        var count = new AtomicInteger();
        try {
            var map = contentMetadataSetRepositoryService.findAll().stream().collect(Collectors.groupingBy(c -> c.getName() + c.getCategory()));
            map.forEach(
                (key, sets) -> {
                    var multiSet = sets.size() > 1;
                    if (multiSet) {
                        var list = sets.stream().sorted(Comparator.comparing(ContentMetadataSet::getVersion)).toList();
                        for (var i = 0; i < list.size() - 1; i++) {
                            var contentMetadataSet = list.get(i);
                            log.debug("Metadata obsolete detected. Move to legacy cms={}", contentMetadataSet);
                            moveContentMetadataSetDatFileToLegacyAndDelete(contentMetadataSet);
                            count.incrementAndGet();
                        }
                    }
                });
        } catch (Exception e) { // NOPMD log. Unclear what kind of exception.
            log.error("Moving older versions failed", e);
        }
        return count.get();
    }

    public boolean isContentMetadataSetFileSupported(Path path) {
        return retromasterConfiguration.getMetadataExtensions().contains(FilenameUtils.getExtension(path.getFileName().toString()));
    }

    public boolean isFlattenable(ContentMetadataSet contentMetadataSet) {
        var contentMetadataItemCount = contentMetadataItemRepositoryService.countAllByContentMetadataSetIdAndDistinctNames(contentMetadataSet.getId());
        var contentMetadataCount = contentMetadataRepositoryService.countAllByContentMetadataSetId(contentMetadataSet.getId());
        return contentMetadataItemCount == contentMetadataCount;
    }

    public void moveContentMetadataSetDatFileToLegacy(Path path) {
        try {
            if (Files.exists(path)) {
                move(path, contentMetadataVolumeService.getLegacyPath().resolve(path.getFileName()));
            }
        } catch (IOException e) {
            log.error("moveContentMetadataSetFileToLegacy failed path={}", path, e);
        }
    }

    public void moveContentMetadataSetDatFileToLegacyAndDelete(ContentMetadataSet contentMetadataSet) {
        moveContentMetadataSetDatFileToLegacy(contentMetadataVolumeService.getProdPath(contentMetadataSet.getFilename()));
        delete(contentMetadataSet, null);
    }
}
