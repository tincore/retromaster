package com.tincore.retromaster.service.execute.prepare;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.service.ArchiveService;
import com.tincore.retromaster.service.execute.ContentExecutor;
import com.tincore.retromaster.service.execute.ExecutionContext;
import com.tincore.retromaster.service.fs.AvFsService;
import com.tincore.retromaster.service.fs.WorkFsService;
import com.tincore.util.FileSystemTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.stream.Stream;

import static com.tincore.util.lang.ThrowingConsumer.uConsumer;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectionExecutionPrepareStrategy implements ExecutionPrepareStrategy, FileSystemTrait {

    private final WorkFsService workFsService;
    private final AvFsService avFsService;
    private final ArchiveService archiveService;

    @Override
    public void doAfterExecution(ExecutionContext executionContext, boolean readOnly) throws IOException {
        if (readOnly) {
            deleteRecursive(executionContext.getTargetProjectionPath());
        } else {
            Stream.concat(executionContext.getOtherItemPaths().stream(), Stream.of(executionContext.getMainItemPath()))
                .filter(p -> p.startsWith(executionContext.getTargetProjectionPath()))
                .filter(p -> !isPathModifiedAfter(p, executionContext.getSetUpCompletedTimestamp()))
                .forEach(uConsumer(this::deleteSilent));
            deleteEmptyDirectories(executionContext.getTargetProjectionPath());
        }
    }

    @Override
    public void doBeforeExecution(ExecutionContext executionContext, boolean readOnly) throws IOException {
        var contentMetadataItem = executionContext.getMainItem();
        if (readOnly) {
            executionContext.setTargetProjectionPath(workFsService.getTemporaryPath().resolve(contentMetadataItem.getContentMetadata().getName()));
        } else {
            executionContext.setTargetProjectionPath(workFsService.getPersistentPath().resolve(contentMetadataItem.getContentMetadata().getName()));
        }

        Files.createDirectories(executionContext.getTargetProjectionPath());

        executionContext.getContentsMain()
            .stream()
            .sorted(Comparator.comparing(p -> p.getLeft().getName()))
            .forEach(
                uConsumer(
                    p -> {
                        var item = p.getLeft();
                        var path = linkOrCopyOrExtract(p.getRight(), item, executionContext);
                        if (item.getId().equals(contentMetadataItem.getId())) {
                            executionContext.setMainItemPath(path);
                        } else {
                            executionContext.getOtherItemPaths().add(path);
                        }
                    }));

        executionContext.getContentsAssociated()
            .stream()
            .sorted(Comparator.comparing(p -> p.getLeft().getName()))
            .forEach(
                uConsumer(
                    p -> {
                        var path = linkOrCopyOrExtract(p.getRight(), p.getLeft(), executionContext);
                        executionContext.getOtherItemPaths().add(path);
                    }));
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    @Override
    public boolean isApplicable(ExecutionContext executionContext, ContentExecutor contentExecutor) {
        return contentExecutor.isProjectionExecutionCapable();
    }

    private boolean isPathModifiedAfter(Path p, LocalDateTime filePreparationCompleted) {
        try {
            return getLastModifiedTime(p).isAfter(filePreparationCompleted);
        } catch (IOException e) {
            log.warn("Error checking last modified time path={}", p, e);
            return false;
        }
    }

    public Path linkOrCopyOrExtract(Content content, ContentMetadataItem item, ExecutionContext executionContext) throws IOException {
        var targetProjectionFilePath = executionContext.getTargetProjectionPath().resolve(executionContext.formatFileName(item.getName()));
        if (Files.exists(targetProjectionFilePath)) {
            return targetProjectionFilePath;
        }

        Files.createDirectories(targetProjectionFilePath.getParent());
        if (avFsService.isActive()) { // Avfs available. Compressed files can be already accessed directly
            var contentPath = avFsService.getPath(content.getVolume()).resolve(content.getPathContentRelative());
            linkOrCopy(targetProjectionFilePath, contentPath);
            return targetProjectionFilePath;
        }

        if (content.isCompressedEntry()) {
            var volumeRootPath = Paths.get(content.getVolume().getPath());
            var inputPath = volumeRootPath.resolve(content.getPathFileRelative());
            var entryName = content.getFileEntry();
            archiveService.extractToPathByName(inputPath, entryName, targetProjectionFilePath);
            return targetProjectionFilePath;
        }

        var contentPath = content.getVolume().toPath().resolve(content.getPathContentRelative());
        linkOrCopy(targetProjectionFilePath, contentPath);
        return targetProjectionFilePath;
    }
}
