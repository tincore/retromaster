package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.TaskExecutorConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.service.enrich.DummyContentMetadataSetEnricher;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.tincore.retromaster.domain.ContentLocation.isFileModifiedTimeEquals;
import static com.tincore.util.PredicateHelper.distinctByKey;
import static com.tincore.util.lang.ThrowingConsumer.uConsumer;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentUpdateAutometadataService implements StreamTrait, FileSystemTrait {

    private final ContentMetadataSetEnricherService contentMetadataSetEnricherService;
    private final ContentVolumeService contentVolumeService;
    private final ContentService contentService;
    private final ContentErrorService contentErrorService;

    private final ContentRepositoryService contentRepositoryService;
    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;
    private final ContentMetadataSetService contentMetadataSetService;
    private final ContentSignatureChangeRepositoryService contentSignatureChangeRepositoryService;
    private final ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    private final ContentVolumeRepositoryService contentVolumeRepositoryService;

    private final DummyContentMetadataSetEnricher dummyContentMetadataSetEnricher;
    private final ContentEnrichService contentEnrichService;

    private final TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;
    private final TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider;

    private ContentMetadata createContentMetadata(String fileName, Path contentMetadataContentPath, List<Content> uniqueContents, ContentMetadataSet contentMetadataSet) {
        String baseName = FilenameUtils.getBaseName(fileName);
        var contentMetadata = ContentMetadata.builder()
            .contentMetadataSet(contentMetadataSet)
            .items(new ArrayList<>())
            .title(baseName)
            .name(baseName)
            .mediaType(ContentMediaType.UNKNOWN)
            .unidentified(false)
            .alternate(false)
            .badDump(false)
            .cracked(false)
            .fixed(false)
            .hacked(false)
            .modified(false)
            .trained(false)
            .translated(false)
            .verified(false)
            .pirated(false)
            .virus(false)
            .build();

        // This should not be needed with bytecode enhancement!
        contentMetadata
            .getItems()
            .addAll(
                uniqueContents.stream().map(c -> ContentMetadataItem.builder()
                        .contentMetadata(contentMetadata)
                        .name(getContentMetadataItemName(c, contentMetadataContentPath))
                        .crc(c.getCrc())
                        .md5(c.getMd5())
                        .sha1(c.getSha1())
                        .size(c.getSize())
                        .signature(c.getSignature())
                        .build())
                    .toList());
        return contentMetadata;
    }

    public int deleteContentMetadataSetsByContentVolumeAndStagingAndMissingContentDirectory(ContentVolume contentVolume) throws IOException {
        var stageFileNames = contentVolumeService.getDirectoryPaths(contentVolume).stream()
            .map(p -> p.getFileName().toString())
            .collect(Collectors.toSet());

        log.info("deleteContentMetadataSetsByContentVolumeAndStagingAndMissingContentDirectory: Starting, vol={}", contentVolume.getName());
        var count = new AtomicInteger();
        contentMetadataSetRepositoryService.findAll()
            .stream()
            .filter(ContentMetadataSet::isStaging)
            .filter(s -> !stageFileNames.contains(s.getRepositoryPath()))
            .forEach(
                cms -> {
                    log.debug("deleteContentMetadataSetsByContentVolumeAndStagingAndMissingContentDirectory: ContentMetadataSet with missing content files. Deleting. Name={}", cms.getName());
                    contentMetadataSetService.delete(cms, null);
                    count.incrementAndGet();
                });
        log.info("deleteContentMetadataSetsByContentVolumeAndStagingAndMissingContentDirectory: Completed. vol={}, count={}", contentVolume.getName(), count);
        return count.get();
    }


    public int deleteContentMetadataSetsByStageContentVolumesAndMissingContentDirectory() throws IOException {
        var volumes = contentVolumeService.getContentVolumesByEnvironment(ContentVolumeEnvironment.STAGE);
        int count = 0;
        for (var volume : volumes) {
            count += deleteContentMetadataSetsByContentVolumeAndStagingAndMissingContentDirectory(volume);
        }
        return count;
    }

    private String getContentMetadataItemName(Content content, Path contentMetadataContentPath) {
        if (content.isCompressedEntry()) {
            return content.getFileEntry();
        }

        var contentMetadataItemRelativeSubPath = contentMetadataContentPath.relativize(content.getPathFileAbsolute()).toString();
        if (StringUtils.isNotBlank(contentMetadataItemRelativeSubPath)) { // File inside directory
            return contentMetadataItemRelativeSubPath;
        }
        // Flat file
        return contentMetadataContentPath.getFileName().toString();
    }

    private ContentMetadataSet getOrCreateContentMetadataSet(String fileName) {
        return contentMetadataSetRepositoryService
            .findByFilename(fileName)
            .orElseGet(() -> ContentMetadataSet.builder().name(fileName).filename(fileName).repositoryPath(fileName).version("0.0").checkTime(LocalDateTime.now()).staging(true).build());
    }

    public void updateContentsByContentMetadataSet(ContentMetadataSet contentMetadataSet) throws InterruptedException, ExecutionException, IOException {
        for (var contentVolume : contentVolumeService.getContentVolumesByEnvironment(ContentVolumeEnvironment.STAGE)) {
            updateContentsByContentVolumeAndSubPath(contentVolume, contentMetadataSet.getRepositoryPath(), true);
        }
    }

    public void updateContentsByContentVolume(ContentVolume contentVolume) throws IOException {
        var contentVolumePath = contentVolume.toPath();
        try (var directoryStream = Files.newDirectoryStream(contentVolumePath)) {
            StreamSupport.stream(directoryStream.spliterator(), false)
                .filter(Files::isDirectory)
                .sorted()
                .forEach(uConsumer(dp -> updateContentsByContentVolumeAndSubPath(contentVolume, contentVolume.toPath().relativize(dp).toString(), false)));
        }
        contentService.deleteContentsByVolumeAndMissingFiles(contentVolume);
    }

    public boolean updateContentsByContentVolumeAndSubPath(ContentVolume contentVolume, String contentVolumeSubPath, boolean deleteOrphans) throws IOException, ExecutionException, InterruptedException { // NOPMD
        var potentialContentMetadataSetContentsRootPath = contentVolume.toPath().resolve(contentVolumeSubPath);
        var fileName = potentialContentMetadataSetContentsRootPath.getFileName().toString();
        var contentMetadataSet = getOrCreateContentMetadataSet(fileName);

        var contentMetadataSetLastModifiedTime = getLastModifiedTime(potentialContentMetadataSetContentsRootPath);
        var fresh = Optional.ofNullable(contentMetadataSet.getCheckContentTime()).filter(t -> isFileModifiedTimeEquals(t, contentMetadataSetLastModifiedTime)).isPresent();
        if (fresh) {
            log.debug("updateContentsByContentVolumeAndSubPath: Data is Fresh. Skipping, path={}, Timestamp={}", potentialContentMetadataSetContentsRootPath, contentMetadataSetLastModifiedTime);
            return true;
        }

        var contentMetadataSetEnricher = contentMetadataSetEnricherService.findByContentMetadataSet(contentMetadataSet).orElse(dummyContentMetadataSetEnricher);
        contentMetadataSetEnricher.enrichContentMetadataSet(contentMetadataSet);

        var contentsEmpty = !Files.exists(potentialContentMetadataSetContentsRootPath) || isEmpty(potentialContentMetadataSetContentsRootPath, FileVisitOption.FOLLOW_LINKS);
        Set<String> validContentMetadataNames = new HashSet<>();
        if (contentMetadataSet.getId() == null) {
            if (contentsEmpty) {
                log.debug("updateContentsByContentVolumeAndSubPath: No filesystem contents. Skipping, path={}", potentialContentMetadataSetContentsRootPath);
                return true;
            }
            log.debug("updateContentsByContentVolumeAndSubPath: New filesystem contents, path={}", potentialContentMetadataSetContentsRootPath);
        } else {
            if (deleteOrphans) {
                log.debug("updateContentsByContentVolumeAndSubPath: Deleting contents without file, path={}", potentialContentMetadataSetContentsRootPath);
                contentService.deleteContentsByVolumeAndMetadataSetAndMissingFiles(contentVolume, contentMetadataSet);
            }
            if (contentsEmpty) {
                log.debug("updateContentsByContentVolumeAndSubPath: Existing content metadata set but no contents. Deleting all metadatas, path={}", potentialContentMetadataSetContentsRootPath);
                contentMetadataSetRepositoryService.delete(contentMetadataSet);
                return true;
            }

            log.debug("updateContentsByContentVolumeAndSubPath: Checking metadatas without real filesystem files, Path={}", potentialContentMetadataSetContentsRootPath);
            var contentMetadataIds = contentMetadataRepositoryService.findContentMetadataIdsByContentMetadataSet(contentMetadataSet);
            var count = new AtomicInteger();
            contentMetadataIds.stream()
                .parallel()
                .forEach(contentMetadataId -> transactionRequiresNewTemplate.execute(
                    ts -> {
                        count.incrementAndGet();
                        log.debug("updateContentsByContentVolumeAndSubPath: Checking, i={}/{}, id={}", count.get(), contentMetadataIds.size(), contentMetadataId);
                        contentMetadataItemRepositoryService.findFirstByContentMetadataId(contentMetadataId) // One should be enough in case of staging as its all or nothing in most cases
                            .filter(i -> {
                                var contents = contentRepositoryService.findBySignatureAndVolume(i.getSignature(), contentVolume);
                                return contents.stream()
//                                    .parallel()
                                    .filter(c -> c.getVolume().isStage()) // I think that redundant but not sure yet
                                    .filter(c -> getContentMetadataItemName(c, c.getVolume().toPath().resolve(contentMetadataSet.getRepositoryPath())).equals(i.getName()))
                                    .anyMatch(Content::isFileExistsAndValid);
                            })
                            .ifPresentOrElse(i -> {
                                    String name = contentMetadataRepositoryService.getReferenceById(contentMetadataId).getName();
                                    validContentMetadataNames.add(name);
                                    log.debug("updateContentsByContentVolumeAndSubPath: Valid, i={}/{}, id={}", count.get(), contentMetadataIds.size(), name);
                                },
                                () -> {
                                    log.debug("updateContentsByContentVolumeAndSubPath: Delete stale content metadata, i={}/{}, id={}", count.get(), contentMetadataIds.size(), contentMetadataId);
                                    contentMetadataRepositoryService.deleteById(contentMetadataId);
                                });
                        return true;
                    }));

// This is very slow! for TDC
//            transactionRequiresNewTemplate.execute(
//                ts -> {
//                    var set = contentMetadataSetRepositoryService.findById(contentMetadataSet.getId()).orElseThrow(() -> new RuntimeException("Content metadata set not found! " + contentMetadataSet));
//                    var contentMetadatas = set.getContentMetadatas();
//                    var count = new AtomicInteger();
//                    contentMetadatas.forEach(
//                        cm -> {
//                            count.incrementAndGet();
//                            log.debug("Checking content metadata contents {}/{}. Name={}", count.get(), contentMetadatas.size(), cm.getName());
//
//                            for (var i : cm.getItems()) {
//                                var contents = contentRepositoryService.findBySignatureAndVolume(i.getSignature(), contentVolume);
//                                var validFound = contents.stream()
//                                    .parallel()
//                                    .filter(c -> c.getVolume().isStage())
//                                    .filter(c -> getContentMetadataItemName(c, c.getVolume().toPath().resolve(contentMetadataSet.getRepositoryPath())).equals(i.getName()))
//                                    .anyMatch(Content::isFileExistsAndValid);
//
//                                if (validFound) {
//                                    validContentMetadataNames.add(cm.getName());
//                                    log.debug("Found valid content metadata. Name={}", cm.getName());
//                                    break; // One is enough because if it packed will compare the file. This works just for staging volumes! Normal volumes can have content distributed in different files
//                                } else {
//                                    log.debug("Delete content metadata. Name={}", cm.getName());
//                                    contentMetadataRepositoryService.delete(cm);
//                                    break;
//                                }
//                            }
//                        });
//                    return true;
//                });
        }

        contentMetadataSetRepositoryService.save(contentMetadataSet); // Make sure it has id

        var count = new AtomicInteger();

        List<Path> potentialContentMetadataContentPaths;
        try (var s = Files.list(potentialContentMetadataSetContentsRootPath)) {
            potentialContentMetadataContentPaths = s.toList();
        }
        log.debug("updateContentsByContentVolumeAndSubPath: Importing, path={}, ItemCount={}", potentialContentMetadataSetContentsRootPath, potentialContentMetadataContentPaths.size());

        var system = contentMetadataSetEnricher.extractSystem(contentMetadataSet);
        forkJoinPoolProvider.submitNow(
            () -> potentialContentMetadataContentPaths.stream()
                .parallel()
                .forEach(p -> {
                    count.incrementAndGet();
                    try {
                        var newContentMetadataFileName = p.getFileName().toString();

                        if (validContentMetadataNames.contains(newContentMetadataFileName)) {
                            log.debug("updateContentsByContentVolumeAndSubPath: Import skip already fresh, i={}/{}, path={}", count.get(), potentialContentMetadataContentPaths.size(), p);
                            return;
                        }

                        var storedContentMetadata = contentMetadataRepositoryService.findByContentMetadataSetAndName(contentMetadataSet, newContentMetadataFileName);
                        if (storedContentMetadata.isPresent()) {
                            var lastModifiedTime = getLastModifiedTime(p);
                            if (!contentService.isContentModified(contentVolume, p, lastModifiedTime)) {
                                log.debug("updateContentsByContentVolumeAndSubPath: Import skipp not modified, i={}/{}, path={}", count.get(), potentialContentMetadataContentPaths.size(), p);
                                return;
                            }
                            log.debug("updateContentsByContentVolumeAndSubPath: Import clearing old, i= {}/{}, path={}", count.get(), potentialContentMetadataContentPaths.size(), p);
                            transactionRequiresNewTemplate.execute(
                                ts -> {
                                    contentMetadataRepositoryService.delete(storedContentMetadata.get());
                                    contentRepositoryService.deleteAllByVolumeAndFileAbsolutePath(contentVolume, p);
                                    return true;
                                });
                        }
                        log.debug("updateContentsByContentVolumeAndSubPath: Import analyzing, i={}/{}, path={}", count.get(), potentialContentMetadataContentPaths.size(), p);
                        var checkTime = LocalDateTime.now();
                        transactionRequiresNewTemplate.execute(
                            ts -> {
                                try {
                                    // Will need to refactor if enable hibernate collection management enhancements
                                    //  var v = contentVolumeRepositoryService.getReferenceById(contentVolume.getId()); // Workaround for now
                                    var v = contentVolume; // Workaround for now
                                    var contentsEnriched = contentEnrichService.getContentsEnriched(v, v.toPath().resolve(p), pa -> contentErrorService.doRejectBroken(v, pa));
                                    if (contentsEnriched.isEmpty()) {
                                        throw new IOException(String.format("updateContentsByContentVolumeAndSubPath: Importing Invalid file, i=%d/%d, path=%s", count.get(), potentialContentMetadataContentPaths.size(), p));
                                    }

                                    // Under some circumstances contents may be dupe!
                                    var uniqueContents = contentsEnriched.stream()
                                        .filter(distinctByKey(c -> c.getDirectoryPath() + c.getFileName() + c.getFileEntry()))
                                        .peek(c -> c.setCheckTime(checkTime))
                                        .collect(Collectors.toList());

                                    var newContents = uniqueContents.stream().filter(c -> c.getId() == null).collect(Collectors.toList());
                                    contentRepositoryService.saveAll(newContents);

                                    var contentMetadata = createContentMetadata(newContentMetadataFileName, p, uniqueContents, contentMetadataSet);
                                    contentMetadataSetEnricher.enrichContentMetadata(contentMetadata, system);

                                    contentMetadataRepositoryService.save(contentMetadata);
                                    contentSignatureChangeRepositoryService.saveAll(contentMetadata.getItems().stream().map(ContentMetadataItem::getSignature));
                                    return true;
                                } catch (IOException e) {
                                    throw new WrappedException(e);
                                }
                            });
                        log.debug("updateContentsByContentVolumeAndSubPath: Import success, i= {}/{}, path={}", count.get(), potentialContentMetadataContentPaths.size(), p);
                    } catch (Exception e) { // NOPMD
                        log.debug("updateContentsByContentVolumeAndSubPath: Import failed, i= {}/{}, path={}", count.get(), potentialContentMetadataContentPaths.size(), p);
                        contentErrorService.doRejectBroken(contentVolume, p);
                    }
                }));

        contentMetadataSet.setCheckContentTime(getLastModifiedTime(potentialContentMetadataSetContentsRootPath));
        contentMetadataSetRepositoryService.save(contentMetadataSet);
        log.debug("updateContentsByContentVolumeAndSubPath: Completed, path={}", potentialContentMetadataSetContentsRootPath);
        return true;
    }

    public void updateContentsByStageContentVolumes() throws IOException {
        var volumes = contentVolumeService.getContentVolumesByEnvironment(ContentVolumeEnvironment.STAGE);
        for (var volume : volumes) {
            updateContentsByContentVolume(volume);
        }
    }

}
