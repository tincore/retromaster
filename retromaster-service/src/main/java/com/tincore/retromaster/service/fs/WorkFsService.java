package com.tincore.retromaster.service.fs;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.FilesystemConfiguration;
import com.tincore.util.FileSystemTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@RequiredArgsConstructor
@Service
public class WorkFsService implements FileSystemTrait {
    public static final Path PATH_SHM = Paths.get("/dev/shm/");

    private final FilesystemConfiguration filesystemConfiguration;
    private Path temporaryPath;

    public Path getPersistentPath() {
        return filesystemConfiguration.getPersistentWorkPath();
    }

    public Path getTemporaryPath() {
        return temporaryPath;
    }

    @PostConstruct
    public void postConstruct() throws IOException {
        temporaryPath = Files.exists(PATH_SHM) ? PATH_SHM.resolve("retromaster") : Files.createTempDirectory("retromaster");
    }

    @PreDestroy
    public void preDestroy() {
        try {
            deleteRecursive(temporaryPath);
        } catch (IOException e) {
            log.error("deleting work path ", e);
        }
    }
}
