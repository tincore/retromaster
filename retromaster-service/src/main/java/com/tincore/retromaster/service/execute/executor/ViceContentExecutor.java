package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentExtension;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ExecutionContext;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class ViceContentExecutor extends AbstractContentExecutor {

    private final String command;

    public ViceContentExecutor(String command, ContentSystem... contentSystems) {
        super("VICE_" + command.toUpperCase(), "Vice " + command, true, contentSystems);

        this.command = command;
    }

    @Override
    public void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException {
        Path flipNamePath = null;
        try {
            List<String> params = new ArrayList<>();

            params.add("-autostart");
            params.add(StringUtils.wrapIfMissing(executionContext.getMainItemPath().toString(), "\""));

            if (!executionContext.getAssociatedItems().isEmpty() && ContentExtension.isDisk(executionContext.getMainItemPath())) {
                flipNamePath = Files.createTempFile("vice.x64.retromaster", ".vfl");
                var config = new StringBuilder().append("# Vice fliplist file").append(EOL).append(EOL).append("UNIT 8").append(EOL).append(executionContext.getMainItemPath()).append(EOL);
                var extraItemsPaths = executionContext.getOtherItemPaths().stream().sorted().toList();
                extraItemsPaths.forEach(p -> config.append(p).append(EOL));

                var flipNameContent = config.toString();
                Files.write(flipNamePath, flipNameContent.getBytes());

                params.add("-flipname " + flipNamePath);
            }

            var cmd = String.format("%s %s", getExecutablePath(), String.join(" ", params));
            exec(cmd, executionContext.getExecutionId());
        } finally {
            if (flipNamePath != null) {
                Files.deleteIfExists(flipNamePath);
            }
        }
    }

    @Override
    public Set<String> getContentExtensions() {
        return Collections.emptySet();
    }

    @Override
    public Stream<String> getExecutablePaths() {
        return Stream.of("/usr/bin/" + command);
    }

    @Override
    public boolean isProjectionExecutionCapable() {
        return false;
    }

    // retroarch -v -L "cores\vice_x64_libretro.dll" "x64 creatures_2_a.d64"
    //
    // it makes the first disk load normally (works fine, game opening starts).
    //
    // But doing this makes retroarch crash:
    //
    // retroarch -v -L "cores\vice_x64_libretro.dll" "x64 creatures_2_a.d64 -flipname Creatures_2.vfl "
    // x64 -autostart ./Creatures_2_a.d64 -flipname ./creature2.vfl
    // The content of the Creatures_2 vfl is:
    //
    // # Vice fliplist file
    //
    // UNIT 8
    // C:\Users\Admin\Emulators\Retroarch\Creatures_2_b.d64
    // C:\Users\Admin\Emulators\Retroarch\Creatures_2_a.d64

}
