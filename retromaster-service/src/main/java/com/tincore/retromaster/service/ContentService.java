package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.TaskExecutorConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.*;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.LRUCache;
import com.tincore.util.Partition;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static com.tincore.retromaster.domain.ContentLocation.isFileModifiedTimeEquals;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentService implements FileSystemTrait {

    private static final int CHUNK_SIZE = 100;
    private static final int CACHE_SIZE = 100;
    public static final int SUB_CHUNK_SIZE = 10;

    private final ContentRepositoryService contentRepositoryService;
    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;

    private final TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;

    private final TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider;
    private final Map<Path, Boolean> recentPathExistsCache = Collections.synchronizedMap(new LRUCache<>(CACHE_SIZE));

    public void deleteContentsByMetadataIdAndMissingFiles(UUID contentMetadataId) {
        log.info("deleteContentsByMetadataIdAndMissingFiles: Starting, contentMetadataId={}", contentMetadataId);
        var signatures = contentMetadataItemRepositoryService.findByContentMetadataId(contentMetadataId).stream().map(ContentMetadataItem::getSignature).distinct().collect(toList());
        contentRepositoryService.deleteAll(contentRepositoryService.findBySignatureIn(signatures).stream().filter(c -> !c.isFileExists()).collect(toList()));
        log.info("deleteContentsByMetadataIdAndMissingFiles: Completed, contentMetadataId= {}", contentMetadataId);
    }

    public boolean deleteContentsByVolumeAndMetadataSetAndMissingFiles(ContentVolume contentVolume, ContentMetadataSet contentMetadataSet) {
        var directoryPath = contentMetadataSet.getRepositoryPath();
        try {
            log.info("deleteContentsByVolumeAndMetadataSetAndMissingFiles: Scanning, vol={}, subpath={}", contentVolume.getName(), directoryPath);
            var contents = contentRepositoryService.findByVolumeAndDirectoryPath(contentVolume, directoryPath);
            var missingContents = getContentsByMissing(contents);
            if (!missingContents.isEmpty()) {
                log.debug("deleteContentsByVolumeAndMetadataSetAndMissingFiles: Found missing. vol={}, directoryPath={}, count={}", contentVolume.getName(), directoryPath, missingContents.size());

                var partition = Partition.ofSize(missingContents, CHUNK_SIZE);
                forkJoinPoolProvider.submitNow(
                    () -> IntStream.range(0, partition.size())
                        .parallel()
                        .forEach(
                            i -> {
                                try {
                                    transactionRequiresNewTemplate.execute(
                                        ts -> {
                                            log.debug("Deleting missing contents. vol={}, directoryPath={}, i={}/{}", contentVolume.getName(), directoryPath, missingContents.size(), i + 1); // NOPMD
                                            contentRepositoryService.deleteAll(partition.get(i));
                                            return true;
                                        });
                                } catch (Exception e) { // NOPMD
                                    log.error(MessageFormat.format("deleteContentsByVolumeAndMetadataSetAndMissingFiles: Failed, contentVolume={0}, directoryPath={1}, i={2}/{3}", contentVolume.getName(), directoryPath, i + 1, partition.size()), e);
                                }
                            }));
            }
            log.info("deleteContentsByVolumeAndMetadataSetAndMissingFiles: Completed. vol={}, directoryPath={}", contentVolume.getName(), directoryPath);
            return true;
        } catch (ExecutionException | InterruptedException e) {
            log.error("deleteContentsByVolumeAndMetadataSetAndMissingFiles: Failed", e);
            return false;
        }
    }

    public boolean deleteContentsByVolumeAndMissingFiles(ContentVolume contentVolume) {
        try {
            log.info("deleteContentsByVolumeAndMissingFiles: Looking up distinct content directory paths, vol={}", contentVolume.getName());
            var directoryPaths = contentRepositoryService.findDistinctContentDirectoryPathsByContentVolume(contentVolume);
            log.info("deleteContentsByVolumeAndMissingFiles: Found paths, vol={}, count={}", contentVolume.getName(), directoryPaths.size());

            var directoryPathIndex = new AtomicInteger();
            var errorCount = new AtomicInteger();

            for (var directoryPath : directoryPaths) {
                directoryPathIndex.incrementAndGet();
                log.debug("deleteContentsByVolumeAndMissingFiles: Searching missing contents, i={}/{}, vol={}, directoryPath={}", directoryPathIndex, directoryPaths.size(), contentVolume.getName(), directoryPath);
                var contents = contentRepositoryService.findByVolumeAndDirectoryPath(contentVolume, directoryPath);
                var missingContents = getContentsByMissing(contents);
                if (!missingContents.isEmpty()) {
                    log.debug("deleteContentsByVolumeAndMissingFiles: Found missing contents, i={}/{}, vol={}, directoryPath={}, count={}", directoryPathIndex, directoryPaths.size(), contentVolume.getName(), directoryPath, missingContents.size());
                    var partition = Partition.ofSize(missingContents, Math.max(1, Math.min(CHUNK_SIZE, missingContents.size() / SUB_CHUNK_SIZE))); // Variable partition size. This makes smaller batches to take care of potential errors because of duplicate signatures in same chunk
                    forkJoinPoolProvider.submitNow(() -> IntStream.range(0, partition.size()).parallel().forEach(
                        i -> {
                            try {
                                transactionRequiresNewTemplate.execute(
                                    ts -> {
                                        if (log.isDebugEnabled()) {
                                            log.debug(
                                                "deleteContentsByVolumeAndMissingFiles: Deleting, i={}/{}, vol={}, directoryPath={}, i={}/{}",
                                                directoryPathIndex,
                                                directoryPaths.size(),
                                                contentVolume.getName(),
                                                directoryPath,
                                                missingContents.size(),
                                                i + 1);
                                        }
                                        contentRepositoryService.deleteAll(partition.get(i));
                                        return true;
                                    });
                            } catch (Exception e) { // NOPMD
                                log.error("deleteContentsByVolumeAndMissingFiles: Error, i={}/{}, {}/{}", directoryPathIndex, directoryPaths.size(), i + 1, partition.size(), e);
                                errorCount.incrementAndGet();
                                // Delete one by one!
                                partition.get(i).forEach(c -> transactionRequiresNewTemplate.execute(
                                    ts -> {
                                        log.info("deleteContentsByVolumeAndMissingFiles - Linear delete {}", c);
                                        contentRepositoryService.deleteAndCreateChange(c, true);
                                        return true;
                                    }));
                            }
                        }));
                }
            }
            log.info("deleteContentsByVolumeAndMissingFiles: Completed, vol={}, errors={}", contentVolume.getName(), errorCount.get());
            return errorCount.get() > 0;
        } catch (ExecutionException | InterruptedException e) {
            log.error("deleteContentsByVolumeAndMissingFiles: Failed", e);
            return false;
        }
    }

    public Optional<Content> findContentByContentMetadataItem(ContentMetadataItem contentMetadataItem) {
        var staging = contentMetadataItem.getContentMetadata().getContentMetadataSet().isStaging();
        var contents = contentRepositoryService.findBySignature(contentMetadataItem.getSignature());
        return contents.stream()
            .filter(c -> recentPathExistsCache.computeIfAbsent(c.getPathFileAbsolute(), p -> c.isFileExists()))
            .min(comparing(c -> staging && c.getVolume().isStage() || !staging && !c.getVolume().isStage() ? 1 : 0));
    }

    public List<Path> findContentPathsByModified(ContentVolume contentVolume, String contentVolumeSubPath) throws IOException {
        var contentsRootPath = contentVolume.toPath().resolve(contentVolumeSubPath);

        List<Path> contentPaths;
        try (var s = findPaths(contentsRootPath)) {
            contentPaths = s.toList();
        }

        var contentLocationsBySubSubPath = getContentLocationsBySubSubPath(contentVolume, contentVolumeSubPath, contentPaths.stream()
            .filter(p -> !p.equals(contentsRootPath))
            .anyMatch(Files::isDirectory));

        return contentPaths.stream()
            .filter(Files::isRegularFile)
            .filter(p -> {
                var contentSubPath = contentsRootPath.relativize(p).toString();
                var contentLocation = contentLocationsBySubSubPath.get(contentSubPath);
                try {
                    return contentLocation == null || contentLocation.getFileSize() != Files.size(p) || !isFileModifiedTimeEquals(p, contentLocation);
                } catch (IOException e) {
                    throw new UncheckedIOException("Could not retrieve content file path=" + p, e);
                }
            })
            .parallel()
            .toList();
    }

    @Transactional(readOnly = true)
    public List<Pair<ContentMetadataItem, List<Content>>> findContentsWithContentMetadataItemPairsByContentMetadataId(UUID contentMetadataId) {
        var contentMetadata = contentMetadataRepositoryService.getReferenceById(contentMetadataId);

        return contentMetadata.getItems().stream().map(i -> Pair.of(i, contentRepositoryService.findBySignature(i.getSignature()))).collect(toList());
    }

    public Content findOrCreateEmptyContent(ContentVolume volume, Path fileAbsolutePath, String fileEntry, boolean lazy) {
        var directoryPath = volume.toPath().relativize(fileAbsolutePath.getParent()).toString();
        var fileName = fileAbsolutePath.getFileName().toString();

        if (volume.getEnvironment().isContainsImported() || !lazy) {
            return contentRepositoryService.findByVolumeAndDirectoryPathAndFileNameAndFileEntry(volume, directoryPath, fileName, fileEntry).orElseGet(() -> new Content(volume, directoryPath, fileName, fileEntry));
        } else {
            return new Content(volume, directoryPath, fileName, fileEntry);
        }
    }

    public Map<String, ContentLocation> getContentLocationsBySubSubPath(ContentVolume contentVolume, String contentVolumeSubPath, boolean containsDirectory) {
        if (containsDirectory) {
            var contentMetadataSubPath = Path.of(contentVolumeSubPath);
            return contentRepositoryService.findDistinctContentLocationsByContentVolumeAndDirectoryPathStartingWith(contentVolume, contentVolumeSubPath)
                .stream()
                .collect(toMap(l -> contentMetadataSubPath.relativize(Path.of(l.getDirectoryPath())).resolve(l.getFileName()).toString(), cl -> cl));
        } else {
            return contentRepositoryService.findDistinctContentLocationsByContentVolumeAndDirectoryPath(contentVolume, contentVolumeSubPath)
                .stream()
                .collect(toMap(ContentLocation::getFileName, l -> l));
        }
    }

    public List<Content> getContentsByMissing(List<Content> contents) {
        var contentsByContentLocation = contents.stream().collect(groupingBy(Content::getPathFileAbsolute));

        return contentsByContentLocation.entrySet()
            .stream()
            .filter(e -> !Files.exists(e.getKey()))
            .flatMap(e -> e.getValue().stream())
            .toList();
    }

    public boolean isContentModified(ContentVolume contentVolume, Path contentAbsolutePath, LocalDateTime lastModifiedTime) {
        // Compare speed of these two
        // List<LocalDateTime> localDateTimes;
        // if (Files.isDirectory(contentMetadataContentAbsolutePath)) {
        // String directoryPath = volume.toPath().relativize(contentMetadataContentAbsolutePath).toString64();
        // localDateTimes = contentRepositoryService.findDistinctContentLocalDateTimeByVolumeAndDirectoryPath(volume, directoryPath);
        // } else {
        // String directoryPath = volume.toPath().relativize(contentMetadataContentAbsolutePath.getParent()).toString64();
        // String fileName = contentMetadataContentAbsolutePath.getFileName().toString64();
        // localDateTimes = contentRepositoryService.findDistinctContentLocalDateTimeByVolumeAndDirectoryPathAndFileName(volume, directoryPath, fileName);
        // }
        // return localDateTimes.isEmpty() || localDateTimes.stream().anyMatch(d -> !d.equals(lastModifiedTime));
        List<Content> contents;
        if (Files.isDirectory(contentAbsolutePath)) {
            var directoryPath = contentVolume.toPath().relativize(contentAbsolutePath).toString();
            contents = contentRepositoryService.findByVolumeAndDirectoryPath(contentVolume, directoryPath);
        } else {
            contents = contentRepositoryService.findDistinctByVolumeAndFileAbsolutePath(contentVolume, contentAbsolutePath);
        }
        return contents.isEmpty() || contents.stream().anyMatch(c -> !isFileModifiedTimeEquals(c.getModifiedTime(), lastModifiedTime));
    }
}
