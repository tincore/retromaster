package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.*;
import com.tincore.util.StreamTrait;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class RetromasterPicContentMetadataEnricher implements ContentMetadataEnricher, StreamTrait {

    @Override
    public void enrich(String title, ContentMetadata contentMetadata) { // NOPMD Complexity is justified
        contentMetadata.setSource(RetromasterPicContentMetadataSetEnricher.CONTENT_METADATA_SOURCE.name());
        var contentMetadataSetName = contentMetadata.getContentMetadataSet().getName();

        var contentMediaType = Arrays.stream(ContentMediaType.values())
            .filter(v -> StringUtils.containsIgnoreCase(contentMetadataSetName, "[" + v + "]"))
            .findFirst()
            .orElse(ContentMediaType.UNKNOWN);
        contentMetadata.setMediaType(contentMediaType);

        if (contentMetadata.getName().equals(contentMetadata.getDescription())) {
            contentMetadata.setDescription("");
        }

        var patternMediaOrder = Pattern.compile("(?<title>.*)_(?:(?<mediaOrder>[0-9]))?$");
        var matcher = patternMediaOrder.matcher(title);
        if (matcher.matches()) {
            contentMetadata.setTitle(matcher.group("title"));
            contentMetadata.setMediaOrder(matcher.group("mediaOrder"));
        } else {
            contentMetadata.setTitle(title);
        }

        var name = contentMetadata.getName().trim();

        List<String> tokensExtra = new ArrayList<>();
        if (name.indexOf('(') != -1) {
            var m = PATTERN_PARENTHESES.matcher(name);
            Set<String> parenthesesAttributes = new HashSet<>();
            while (m.find()) {
                var group = m.group(1);
                var regions = toStream(group.split(",")).map(r -> ContentRegion.MAP.get(r.trim())).filter(Objects::nonNull).collect(Collectors.toList());
                if (!regions.isEmpty()) {
                    contentMetadata.setRegion(String.join(",", regions));
                    continue;
                }
                var languages = toStream(group.split(",")).map(r -> ContentLanguage.MAP.get(r.trim())).filter(Objects::nonNull).collect(Collectors.toList());
                if (!languages.isEmpty()) {
                    contentMetadata.setLanguage(String.join(",", languages));
                    continue;
                }
                parenthesesAttributes.add(group);
            }
            tokensExtra.addAll(parenthesesAttributes);
        }

        if (tokensExtra != null) {
            contentMetadata.setTagsExtra(String.join(",", tokensExtra));
        }
    }
}
