package com.tincore.retromaster.service.dat;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.util.form.FormMapperTrait;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Remember to keep builder disabled both for javadoc and also for afterMapping to work.
 */
@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface DatFormMapper extends FormMapperTrait {

    @Mapping(source = "game.roms", target = "items")
    ContentMetadata toContentMetadata(Game game);

    default ContentMetadata toContentMetadata(Machine machine) {
        if (machine == null) {
            return null;
        }

        var contentMetadata = new ContentMetadata();

        var roms = toContentMetadataItems(machine.getRoms());
        var disks = toContentMetadataItems(machine.getDisks());

        Collection<ContentMetadataItem> items;
        if (roms == null) {
            items = disks;
        } else {
            if (disks == null) {
                items = roms;
            } else {
                items = new ArrayList<>(roms);
                items.addAll(items);
            }
        }

        contentMetadata.setItems(items);
        contentMetadata.setName(toString(machine.getName()));
        contentMetadata.setDescription(toString(machine.getDescription()));

        return contentMetadata;
    }

    Collection<ContentMetadata> toContentMetadata(Machine... machines);

    default ContentMetadataItem toContentMetadataItem(Rom rom) {
        if (rom == null) {
            return null;
        }

        var contentMetadataItem = new ContentMetadataItem();

        contentMetadataItem.setName(toString(rom.getName()));
        contentMetadataItem.setSignature(rom.getCrc(), rom.getMd5(), rom.getSha1(), rom.getSize());

        return contentMetadataItem;
    }

    default ContentMetadataItem toContentMetadataItem(Disk disk) {
        if (disk == null) {
            return null;
        }

        var contentMetadataItem = new ContentMetadataItem();

        contentMetadataItem.setName(toString(disk.getName()));
        contentMetadataItem.setSignature(disk.getCrc(), disk.getMd5(), disk.getSha1(), disk.getSize());

        return contentMetadataItem;
    }

    Collection<ContentMetadataItem> toContentMetadataItems(Rom... roms);

    Collection<ContentMetadataItem> toContentMetadataItems(Disk... disks);

    @Mapping(source = "datafile.header.name", target = "name")
    @Mapping(source = "datafile.header.description", target = "description")
    @Mapping(source = "datafile.header.category", target = "category")
    @Mapping(source = "datafile.header.version", target = "version")
    @Mapping(source = "datafile.header.author", target = "author")
    @Mapping(source = "datafile.header.email", target = "email")
    @Mapping(source = "datafile.header.url", target = "url")
    @Mapping(source = "datafile.games", target = "contentMetadatas")
    // @Mapping(source = "datafile.machines", target = "contentMetadatas")
    @Mapping(source = "filename", target = "filename")
    ContentMetadataSet toContentMetadataSet(Datafile datafile, String filename);

    @AfterMapping
    default void toContentMetadataSetAfter(Datafile source, @MappingTarget ContentMetadataSet target) { // NOPMD Linguistics
        if (target.getContentMetadatas() == null) {
            target.setContentMetadatas(new ArrayList<>());
        }

        if (source.getMachines() != null) {
            target.getContentMetadatas().addAll(toContentMetadata(source.getMachines()));
        }
        target.setVersion(Stream.of(source.getHeader().getVersion(), source.getHeader().getDate()).filter(StringUtils::isNotBlank).distinct().collect(Collectors.joining(",")));
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    class Datafile {
        private Header header;

        @JacksonXmlProperty(localName = "game")
        @JacksonXmlElementWrapper(useWrapping = false)
        private Game[] games;

        @JacksonXmlProperty(localName = "machine")
        @JacksonXmlElementWrapper(useWrapping = false)
        private Machine[] machines;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    class Header {
// Ignore these at least for now
//        private String id;
        private String name;
        private String description;
        private String category;
        private String version;
        private String date;
        private String author;
        private String email;
        private String homepage;
        private String url;
        private String comment;
        private String clrmamepro;
        private String romcenter;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    class Machine {
        @JacksonXmlProperty(isAttribute = true)
        private String name;

        private String description;

        @JacksonXmlProperty(localName = "rom")
        @JacksonXmlElementWrapper(useWrapping = false)
        private Rom[] roms;

        @JacksonXmlProperty(localName = "disk")
        @JacksonXmlElementWrapper(useWrapping = false)
        private Disk[] disks;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    class Game {
        // Ignore these at least for now
//        private String id;
//        private String cloneofid;

        @JacksonXmlProperty(isAttribute = true)
        private String name;

        private String description;

        @JacksonXmlProperty(localName = "rom")
        @JacksonXmlElementWrapper(useWrapping = false)
        private Rom[] roms;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    class Rom { // NOPMD
        @JacksonXmlProperty(isAttribute = true)
        private String name;

        @JacksonXmlProperty(isAttribute = true)
        private long size;

        @JacksonXmlProperty(isAttribute = true)
        private String crc;

        @JacksonXmlProperty(isAttribute = true)
        private String md5;

        @JacksonXmlProperty(isAttribute = true)
        private String sha1;

        @JacksonXmlProperty(isAttribute = true)
        private String status;

        @JacksonXmlProperty(isAttribute = true)
        private String serial;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    class Disk { // NOPMD
        @JacksonXmlProperty(isAttribute = true)
        private String name;

        @JacksonXmlProperty(isAttribute = true)
        private long size;

        @JacksonXmlProperty(isAttribute = true)
        private String crc;

        @JacksonXmlProperty(isAttribute = true)
        private String md5;

        @JacksonXmlProperty(isAttribute = true)
        private String sha1;

        @JacksonXmlProperty(isAttribute = true)
        private String status;

        @JacksonXmlProperty(isAttribute = true)
        private String serial;
    }
}
