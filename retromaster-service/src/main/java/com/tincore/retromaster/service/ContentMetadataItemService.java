package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentMetadataItemService {

    public static final int MATCH_SCORE_BASE = 0;
    public static final int MATCH_SCORE_BONUS_LOCALE = 100;
    public static final int MATCH_SCORE_MALUS_LOCALE = -10_000;
    public static final int MATCH_SCORE_MALUS_BAD_DUMP = -15_000;
    public static final int MATCH_SCORE_BONUS_RELEASE = 1000;
    public static final int MATCH_SCORE_BONUS_RELEASE2 = 5000;
    public static final int MATCH_SCORE_BONUS_RELEASE3 = 500;
    // To config
    private static final int PRECEDENCE_CMS_MULTIPLIER = 10;
    private static final int REGION_PRECEDENCE_US = 3;
    private static final int REGION_PRECEDENCE_EU = 2;
    private static final int REGION_PRECEDENCE_JP = 1;
    private final ContentRepositoryService contentRepositoryService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;
    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentSystemService contentSystemService;
    private final ContentMetadataSourceService contentMetadataSourceService;

    @Transactional(readOnly = true)
    public List<ContentMetadataItem> getAssociatedItems(ContentMetadataItem contentMetadataItem) {
        List<ContentMetadataItem> associatedItems = new ArrayList<>();

        var extension = FilenameUtils.getExtension(contentMetadataItem.getName());

        var contentMetadata = contentMetadataItem.getContentMetadata();
        Collection<ContentMetadataItem> items = contentMetadataItemRepositoryService.findByContentMetadataId(contentMetadata.getId());
        var sameContentMetadataAssociatedItems = items.stream()
            .filter(i -> !i.getId().equals(contentMetadataItem.getId())) // Not same
            .filter(i -> i.isExtension(extension)) // Same extension
            .filter(i -> contentRepositoryService.findBySignature(i.getSignature()).stream().anyMatch(Content::isFileExists)) // Content exists
            .toList();

        associatedItems.addAll(sameContentMetadataAssociatedItems);

        if (!ContentMediaType.UNKNOWN.equals(contentMetadata.getMediaType()) && StringUtils.isNotBlank(contentMetadata.getMediaOrder())) {
            var potentialParts
                = contentMetadataRepositoryService.findByTitleAndPublisherAndSystemAndSource(contentMetadata.getTitle(), contentMetadata.getPublisher(), contentMetadata.getSystem(), contentMetadata.getSource());

            var potentialPartsByMediaOrder = potentialParts.stream()
                .filter(cm -> !cm.equals(contentMetadata))
                .filter(cm -> cm.getMediaType().equals(contentMetadata.getMediaType()))
                .filter(cm -> cm.getMediaOrder() != null && !StringUtils.equals(cm.getMediaOrder(), contentMetadata.getMediaOrder()))
                .filter(
                    cm -> {
                        var contentMetadataItemSignatures = cm.getItems().stream().map(ContentMetadataItem::getSignature).collect(Collectors.toSet());
                        var contents = contentRepositoryService.findBySignatureIn(contentMetadataItemSignatures);
                        var contentsSignatures = contents.stream().map(Content::getSignature).collect(Collectors.toSet());
                        return contentsSignatures.equals(contentMetadataItemSignatures) && contents.stream().allMatch(Content::isFileExists);
                    })
                .collect(Collectors.groupingBy(ContentMetadata::getMediaOrder));

            var multiSetAssociatedItems = potentialPartsByMediaOrder.values()
                .stream()
                .map(
                    metadata -> {
                        var possibleMetadatas = metadata.stream().sorted(Comparator.comparing(c -> -getCompareScore(contentMetadata, c))).toList();
                        return possibleMetadatas.get(0).getItemsByExtension(extension).get(0);
                    })
                .toList();

            associatedItems.addAll(multiSetAssociatedItems);
            // from each group select the closest one
        }

        return associatedItems;
    }

    private int getCompareScore(ContentMetadata contentMetadata, ContentMetadata c) {
        return MATCH_SCORE_BASE
            + getEqualsBonus(contentMetadata.getRegion(), c.getRegion(), MATCH_SCORE_BONUS_LOCALE, MATCH_SCORE_MALUS_LOCALE)
            + getEqualsBonus(contentMetadata.getLanguage(), c.getLanguage(), MATCH_SCORE_BONUS_LOCALE, MATCH_SCORE_MALUS_LOCALE)
            + getEqualsBonus(contentMetadata.getAlternateData(), c.getAlternateData(), MATCH_SCORE_BONUS_RELEASE3, 0)
            + getEqualsBonus(contentMetadata.getCrackedData(), c.getCrackedData(), MATCH_SCORE_BONUS_RELEASE, 0)
            + getEqualsBonus(contentMetadata.getHackedData(), c.getHackedData(), MATCH_SCORE_BONUS_RELEASE, 0)
            + getEqualsBonus(contentMetadata.getTranslatedData(), c.getTranslatedData(), MATCH_SCORE_BONUS_RELEASE2, 0)
            + getEqualsBonus(contentMetadata.getTrainedData(), c.getTrainedData(), MATCH_SCORE_BONUS_RELEASE2, 0)
            + getEqualsBonus(contentMetadata.isBadDump(), false, 0, MATCH_SCORE_MALUS_BAD_DUMP);
    }

    public ContentMetadataItem getContentMetadataItemByContentAndNotStagingSortedByPrecedence(Content content) {
        return contentMetadataItemRepositoryService.findBySignature(content.getSignature())
            .stream()
            .filter(i -> !i.getContentMetadata().getContentMetadataSet().isStaging())
            .map(i -> Pair.of(i, getPrecedence(i)))
            .max(Comparator.comparing(Pair::getRight))
            .map(Pair::getLeft)
            .orElse(null);
    }

    private Collection<ContentMetadataItem> getContentMetadataItemsByExtensionMatches(ContentMetadata contentMetadata, Set<String> extensionValues) {
        return contentMetadata.getItems().stream().filter(i -> extensionValues.contains(ContentExtension.getExtensionFormatted(i.getName()))).collect(Collectors.toList());
    }

    private int getEqualsBonus(String value, String expectedBonusValue, int equalsBonus, int notEqualsMalus) {
        return StringUtils.equals(value, expectedBonusValue) ? equalsBonus : notEqualsMalus;
    }

    private int getEqualsBonus(boolean value, boolean expectedBonusValue, int equalsBonus, int notEqualsMalus) {
        return value == expectedBonusValue ? equalsBonus : notEqualsMalus;
    }

    public Collection<ContentMetadataItem> getExecutableItems(ContentMetadata contentMetadata) {
        return contentSystemService
            .findBySystemLabel(contentMetadata.getSystem())
            .map(ContentSystem::getContentExtensionValues)
            .filter(ev -> !ev.isEmpty())
            .map(ev -> getContentMetadataItemsByExtensionMatches(contentMetadata, ev))
            .orElseGet(contentMetadata::getItems);
    }

    private int getPrecedence(ContentMetadataItem contentMetadataItem) {
        var contentMetadata = contentMetadataItem.getContentMetadata();
        var regionPrecedence = getRegionPrecedence(contentMetadata.getRegion());
        return contentMetadataSourceService.getContentMetadataSource(contentMetadata.getContentMetadataSet()).getPrecedence() * PRECEDENCE_CMS_MULTIPLIER + regionPrecedence;
    }

    private int getRegionPrecedence(String region) {
        if (StringUtils.isBlank(region)) {
            return 0;
        }
        if (StringUtils.containsIgnoreCase(region, ContentRegion.US.name())) {
            return REGION_PRECEDENCE_US;
        }
        if (StringUtils.containsIgnoreCase(region, ContentRegion.EU.name())) {
            return REGION_PRECEDENCE_EU;
        }
        if (StringUtils.containsIgnoreCase(region, ContentRegion.JP.name())) {
            return REGION_PRECEDENCE_JP;
        }
        return 0;
    }
}
