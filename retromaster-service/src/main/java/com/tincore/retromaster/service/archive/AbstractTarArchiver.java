package com.tincore.retromaster.service.archive;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.service.WrappedException;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryProducer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.CompressorOutputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.compressors.lz4.BlockLZ4CompressorOutputStream;
import org.apache.commons.compress.compressors.lz4.FramedLZ4CompressorOutputStream;
import org.apache.commons.compress.compressors.lzma.LZMACompressorOutputStream;
import org.apache.commons.compress.compressors.xz.XZCompressorOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.time.ZoneOffset;
import java.util.List;

import static org.apache.commons.compress.archivers.tar.TarArchiveEntry.DEFAULT_DIR_MODE;
import static org.apache.commons.compress.archivers.tar.TarArchiveEntry.DEFAULT_FILE_MODE;
import static org.apache.commons.compress.archivers.tar.TarConstants.LF_DIR;
import static org.apache.commons.compress.archivers.tar.TarConstants.LF_NORMAL;

@Slf4j
@AllArgsConstructor
public abstract class AbstractTarArchiver implements Archiver {

    private static final String PATH_SEPARATOR = "/";

    private final RetromasterConfiguration.ArchiverConfiguration archiverConfiguration;

    public void archive(OutputStream targetOutputStream, List<? extends ArchiveEntryProducer> archiveEntryProducers) throws IOException {
        try (var out = new TarArchiveOutputStream(targetOutputStream)) {
            out.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
            out.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
            out.setAddPaxHeadersForNonAsciiNames(true);

            archiveEntryProducers.forEach(
                ae -> {
                    try {
                        if (ae.isRegularFile()) {
                            var entry = new TarArchiveEntry(ae.getName(), LF_NORMAL);
                            entry.setMode(DEFAULT_FILE_MODE);
                            entry.setModTime(ae.getModifiedTime().toInstant(ZoneOffset.UTC).toEpochMilli());
                            var size = ae.getSize();
                            entry.setSize(size);
                            out.putArchiveEntry(entry);

                            try (var in = ae.getInputStream()) {
                                IOUtils.copyLarge(in, out, new byte[archiverConfiguration.getArchiveBufferSize()]);
                            }
                            out.closeArchiveEntry();
                        } else if (ae.isDirectory()) {
                            var name = ae.getName();
                            if (StringUtils.isNotBlank(name)) {
                                var normalizedName = name.endsWith(PATH_SEPARATOR) ? name : name + PATH_SEPARATOR;
                                var entry = new TarArchiveEntry(normalizedName, LF_DIR);
                                entry.setMode(DEFAULT_DIR_MODE);
                                entry.setModTime(ae.getModifiedTime().toInstant(ZoneOffset.UTC).toEpochMilli());
                                out.putArchiveEntry(entry);
                                out.closeArchiveEntry();
                            }
                        } else {
                            log.warn("archive: Not supported, archiveEntry={}", ae);
                        }
                    } catch (IOException e) {
                        throw new WrappedException(e);
                    } finally {
                        try {
                            ae.close();
                        } catch (IOException e) {
                            log.error("archive: Closing, archiveEntry={}", ae, e);
                        }
                    }
                });
            out.finish();
        }
    }

    protected CompressorOutputStream createCompressorOutputStream(CompressorFormat compressorFormat, OutputStream outputStream) throws IOException {
        return switch (compressorFormat) {
            case LZMA -> new LZMACompressorOutputStream(outputStream);
            case BZIP2 -> new BZip2CompressorOutputStream(outputStream);
            case GZIP -> new GzipCompressorOutputStream(outputStream);
            case LZ4B -> new BlockLZ4CompressorOutputStream(outputStream);
            case LZ4F -> new FramedLZ4CompressorOutputStream(outputStream);
            case XZ -> new XZCompressorOutputStream(outputStream);
        };
    }
}
