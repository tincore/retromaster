package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.retromaster.service.ContentSystemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.tincore.retromaster.domain.ContentMediaType.UNKNOWN;
import static com.tincore.retromaster.domain.ContentMetadataSet.AUTHOR_LENGTH;
import static com.tincore.retromaster.domain.ContentMetadataSource.NOINTRO;

@Slf4j
@Service
@RequiredArgsConstructor
public class NoIntroContentMetadataSetEnricher extends AbstractContentMetadataSetEnricher {

    private final ContentSystemService contentSystemService;
    private final NoIntroContentMetadataEnricher contentMetadataEnricher;

    @Override
    public boolean canEnrich(ContentMetadataSet contentMetadataSet) {
        return NOINTRO.isMatch(contentMetadataSet);
    }

    @Override
    public ContentMetadata enrichContentMetadata(ContentMetadata contentMetadata, String systemDefault) {
        contentMetadata.setMediaType(UNKNOWN); // Should eventually: Default from set
        contentMetadata.setSource(ContentMetadataSource.NOINTRO.name());
        contentMetadata.setSystem(systemDefault);

        var title = StringUtils.substringBefore(contentMetadata.getName(), "(").trim();

        contentMetadataEnricher.enrich(title, contentMetadata);

        return contentMetadata;
    }

    @Override
    public ContentMetadataSet enrichContentMetadataSet(ContentMetadataSet contentMetadataSet) {
        contentMetadataSet.setCategory(getContentMetadataSource().toString());
        if (!contentMetadataSet.isStaging() || StringUtils.isBlank(contentMetadataSet.getRepositoryPath())) {
            var repositoryPath = getRepositoryPath(contentMetadataSet);
            contentMetadataSet.setRepositoryPath(repositoryPath);
        }
        contentMetadataSet.setAuthor(StringUtils.left(contentMetadataSet.getAuthor(), AUTHOR_LENGTH));
        return contentMetadataSet;
    }

    @Override
    public String extractSystem(ContentMetadataSet contentMetadataSet) {
        return contentSystemService.findSystemNormalized(contentMetadataSet.getName().replaceAll(" - ", " "));
    }

    @Override
    public ContentMetadataSource getContentMetadataSource() {
        return NOINTRO;
    }

    @Override
    public String getRepositoryPath(ContentMetadataSet contentMetadataSet) {
        var system = extractSystem(contentMetadataSet);
        var name = contentMetadataSet.getName();
        var extrasFormatted = Optional.ofNullable(StringUtils.substringsBetween(name, "(", ")")).map(s -> String.join(" ", s) + " ").orElse("");
        var wip = StringUtils.startsWithIgnoreCase(name, "WIP") ? "WIP " : "";
        var unofficial = StringUtils.startsWithIgnoreCase(name, "UNOFFICIAL") ? "Unofficial " : "";
        var sourceCode = StringUtils.startsWithIgnoreCase(name, "SOURCE CODE") ? "SourceCode " : "";
        var nonRedump = StringUtils.startsWithIgnoreCase(name, "NON-REDUMP") ? "NonRedump " : "";
        return system + " - " + extrasFormatted + unofficial + sourceCode + nonRedump + wip + contentMetadataSet.getVersion().trim() + " " + contentMetadataSet.getCategory();
    }
}
