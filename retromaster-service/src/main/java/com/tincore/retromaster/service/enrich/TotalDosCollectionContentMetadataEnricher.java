package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentLanguage;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.util.StreamTrait;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TotalDosCollectionContentMetadataEnricher implements ContentMetadataEnricher, StreamTrait {

    @Override
    public void enrich(String title, ContentMetadata contentMetadata) { // NOPMD Complexity is justified
        contentMetadata.setSource(ContentMetadataSource.TDC.name());

        if (contentMetadata.getName().equals(contentMetadata.getDescription())) {
            contentMetadata.setDescription("");
        }

        contentMetadata.setTitle(title);

        var name = contentMetadata.getName().trim();

        var parenthesesIndex = name.indexOf('(');
        var bracketIndex = name.indexOf('[');

        List<String> tokensExtra = null;
        if (parenthesesIndex != -1) {
            var m1 = PATTERN_PARENTHESES.matcher(name.substring(parenthesesIndex));
            List<String> parenthesesAttributes = new ArrayList<>();
            while (m1.find()) {
                parenthesesAttributes.add(m1.group(1));
            }

            if (!parenthesesAttributes.isEmpty()) {
                contentMetadata.setPublisher(parenthesesAttributes.remove(parenthesesAttributes.size() - 1));
            }
            if (!parenthesesAttributes.isEmpty()) {
                contentMetadata.setDate(parenthesesAttributes.remove(parenthesesAttributes.size() - 1));
            }

            new ArrayList<>(parenthesesAttributes)
                .stream()
                .filter(pA -> ContentLanguage.SET.contains(pA.toLowerCase()))
                .forEach(
                    pA -> {
                        contentMetadata.setLanguage(pA); // Really should append
                        parenthesesAttributes.remove(pA);
                    });

            tokensExtra = new ArrayList<>(parenthesesAttributes);
        }

        if (bracketIndex != -1) {
            List<String> bracketsAttributes = new ArrayList<>();
            var m = PATTERN_BRACKETS.matcher(name.substring(bracketIndex));
            while (m.find()) {
                bracketsAttributes.add(m.group(1));
            }
            // Always last so check first
            applyOnPrefixFound(
                bracketsAttributes,
                "!",
                data -> {
                    contentMetadata.setVerified(true);
                });
            if (!bracketsAttributes.isEmpty()) {
                // last is genre
                tokensExtra.add(bracketsAttributes.remove(bracketsAttributes.size() - 1));
            }

            applyOnPrefixFound(
                bracketsAttributes,
                "a",
                data -> {
                    contentMetadata.setAlternate(true);
                    contentMetadata.setAlternateData(data);
                });
            applyOnPrefixFound(
                bracketsAttributes,
                "b",
                data -> {
                    contentMetadata.setBadDump(true);
                    contentMetadata.setBadDumpData(data);
                });
            applyOnPrefixFound(
                bracketsAttributes,
                "cr",
                data -> {
                    contentMetadata.setCracked(true);
                    contentMetadata.setCrackedData(data);
                });
            applyOnPrefixFound(
                bracketsAttributes,
                "f",
                data -> {
                    contentMetadata.setFixed(true);
                    contentMetadata.setFixedData(data);
                });
            applyOnPrefixFound(
                bracketsAttributes,
                "h",
                data -> {
                    contentMetadata.setHacked(true);
                    contentMetadata.setHackedData(data);
                });
            applyOnPrefixFound(
                bracketsAttributes,
                "m",
                data -> {
                    contentMetadata.setModified(true);
                    contentMetadata.setModifiedData(data);
                });
            applyOnPrefixFound(
                bracketsAttributes,
                "p",
                data -> {
                    contentMetadata.setPirated(true);
                    contentMetadata.setPiratedData(data);
                });
            applyOnPrefixFound(
                bracketsAttributes,
                "tr",
                data -> {
                    contentMetadata.setTranslated(true);
                    contentMetadata.setTranslatedData(data);
                });
            applyOnPrefixFound(
                bracketsAttributes,
                "t",
                data -> {
                    contentMetadata.setTrained(true);
                    contentMetadata.setTrainedData(data);
                });

            if (tokensExtra == null) {
                tokensExtra = new ArrayList<>();
            }
            tokensExtra.addAll(bracketsAttributes);
        }

        if (tokensExtra != null) {
            contentMetadata.setTagsExtra(String.join(",", tokensExtra));
        }
    }
}
