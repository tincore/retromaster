package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSetRepositoryPath;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public interface ContentMetadataSetRepositoryService extends JpaRepository<ContentMetadataSet, UUID>, QuerydslPredicateExecutor<ContentMetadataSet>, ContentMetadataSetRepositoryServiceExtension {

    @Query("SELECT DISTINCT cms.name FROM ContentMetadataSet cms ORDER BY cms.name")
    List<String> findAllContentMetadataSetNames();

    Optional<ContentMetadataSet> findByFilename(String filename);

    List<ContentMetadataSet> findByNameAndCategory(String name, String category);

    List<ContentMetadataSet> findByNameStartsWithIgnoreCase(String nameStartsWithIgnoreCase);

    List<ContentMetadataSet> findByRepositoryPath(String repositoryPath);

    List<ContentMetadataSet> findByRepositoryPathStartsWithIgnoreCase(String repositoryPathStartsWithIgnoreCase);

    @Query("SELECT DISTINCT new com.tincore.retromaster.domain.ContentMetadataSetRepositoryPath(cms.id, cms.repositoryPath, cms.flattenable, cms.staging) FROM ContentMetadataSet cms ORDER BY cms.repositoryPath")
    List<ContentMetadataSetRepositoryPath> findContentMetadataSetRepositoryPaths();

    @Cacheable(cacheNames = "findContentMetadataSetRepositoryPathsByStringFile")
    default Map<String, ContentMetadataSetRepositoryPath> findContentMetadataSetRepositoryPathsByStringFile() {
// Should check directory conflicts while importing!
//        return findContentMetadataSetRepositoryPaths().stream().collect(Collectors.toMap(ContentMetadataSetRepositoryPath::toStringFile, s -> s));
        return findContentMetadataSetRepositoryPaths().stream().collect(Collectors.toMap(ContentMetadataSetRepositoryPath::toStringFile, s -> s, (a, b) -> a));
    }

    Optional<ContentMetadataSet> findFirstByRepositoryPath(String repositoryPath);

}
