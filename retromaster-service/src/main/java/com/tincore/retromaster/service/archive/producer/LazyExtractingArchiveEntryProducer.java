package com.tincore.retromaster.service.archive.producer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.service.archive.Archiver;
import com.tincore.util.FileSystemTrait;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;

@Slf4j
public class LazyExtractingArchiveEntryProducer implements ArchiveEntryProducer, FileSystemTrait {

    private final Path archivePath;
    private final String entryNameEquals;
    private final String targetName;
    private final long offset;
    private final Archiver archiver;

    private SeekableByteChannel archiveChannel;
    private ArchiveEntryProducer innerArchiveEntryProducer;
    private ArchiveInputStream archiveInputStream;

    public LazyExtractingArchiveEntryProducer(Path archivePath, String entryNameEquals, String targetName, long offset, Archiver archiver) {
        this.archivePath = archivePath;
        this.entryNameEquals = entryNameEquals;
        this.targetName = targetName;
        this.offset = offset;
        this.archiver = archiver;
    }

    @Override
    public void close() throws IOException {
        if (archiveInputStream != null) {
            archiveInputStream.close();
        }
        if (archiveChannel != null) {
            archiveChannel.close();
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        init();
        var inputStream = innerArchiveEntryProducer.getInputStream();
        if (offset > 0) {
            var skip = inputStream.skip(offset);
            if (skip == 0) {
                throw new EOFException("EndOfFile detected path" + archivePath);
            }
        }
        return inputStream;
    }

    @Override
    public LocalDateTime getModifiedTime() throws IOException {
        init();
        return innerArchiveEntryProducer.getModifiedTime();
    }

    @Override
    public String getName() throws IOException {
        init();
        return targetName;
    }

    @Override
    public long getSize() throws IOException {
        init();
        return innerArchiveEntryProducer.getSize();
    }

    private void init() throws IOException {
        if (innerArchiveEntryProducer != null) {
            return;
        }
        archiveChannel = Files.newByteChannel(archivePath);
        try {
            archiveInputStream = archiver.createArchiveInputStream(archiveChannel); // NOPMD
            ArchiveEntry entry;
            while ((entry = archiveInputStream.getNextEntry()) != null) {
                if (!archiveInputStream.canReadEntryData(entry)) {
                    log.warn("Unreadable entry {}", entry);
                    continue;
                }

                if (entryNameEquals.equals(entry.getName())) {
                    innerArchiveEntryProducer = new ArchiveEntryArchiveEntryProducer(entry, archiveInputStream, getLastModifiedTime(archivePath)); // NOPMD absurd
                    return;
                }
            }
        } catch (IOException e) {
            archiveChannel.close();
            throw e;
        }
        if (innerArchiveEntryProducer == null) {
            throw new IOException("Could not find expected entry " + entryNameEquals + " in " + archivePath);
        }
    }

    @Override
    public boolean isDirectory() throws IOException {
        init();
        return innerArchiveEntryProducer.isDirectory();
    }

    @Override
    public boolean isRegularFile() throws IOException {
        init();
        return innerArchiveEntryProducer.isRegularFile();
    }

    @Override
    public String toString() {
        return "LazyExtractArchiveEntryProducer " + archivePath + "/#" + entryNameEquals + " => " + targetName;
    }
}
