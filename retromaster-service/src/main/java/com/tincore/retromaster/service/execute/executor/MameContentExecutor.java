package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentExtension;
import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.service.execute.ExecutionContext;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentExtension.*;


@Accessors(fluent = true)
public class MameContentExecutor extends AbstractContentExecutor {

    private final String mameSystemId;

    @Setter
    private int floppyCount;
    @Setter
    private int cartCount;
    @Setter
    private int cassetteCount;
    @Setter
    private int romCount;
    @Setter
    private int discCount;
    private Set<String> contentExtensions = Collections.emptySet(); // NOPMD

    public MameContentExecutor(String mameSystemId, ContentSystem... contentSystems) {
        super("MAME_" + mameSystemId.toUpperCase(), "Mame " + mameSystemId, false, contentSystems);
        this.mameSystemId = mameSystemId;
    }

    public MameContentExecutor contentExtensions(ContentExtension... extensions) {
        contentExtensions = Stream.of(extensions).map(Enum::name).collect(Collectors.toSet());
        return this;
    }

    @Override
    public void executeExternalApplication(ExecutionContext executionContext) throws IOException, InterruptedException {
        var contentArgument = executionContext.getMainItemPath();
        exec(String.format("%s %s %s %s", getExecutablePath(), mameSystemId, getConfigArgs(), formatContentArgument(contentArgument.toString())), executionContext.getExecutionId());
    }

    public String formatContentArgument(String contentArgument) {

        if (isTape(contentArgument)) {
            return formatContentArgument(getContentParamTape(), contentArgument);
        }

        if (isSnap(contentArgument)) {
            return formatContentArgument(getContentParamSnap(), contentArgument);
        }

        if (isDisk(contentArgument)) {
            return formatContentArgument(getContentParamDisk(), contentArgument);
        }

        if (isDisc(contentArgument)) {
            return formatContentArgument(getContentParamDisc(), contentArgument);
        }

        if (isQuick(contentArgument)) {
            return formatContentArgument(getContentParamQuick(), contentArgument);
        }

        if (cartCount > 0 && isCart(contentArgument)) {
            return formatContentArgument(getContentParamCart(), contentArgument);
        }

        return formatContentArgument(getContentParamFirm(), contentArgument);
    }

    public String formatContentArgument(String param, String contentArgument) {
        return String.format("-%s %s", param, StringUtils.wrapIfMissing(contentArgument, "\""));
    }

    // Should eventually extract all this to config
    private String getConfigArgs() {
        return "-skip_gameinfo -window";
    }

    @Override
    public Set<String> getContentExtensions() {
        return contentExtensions;
    }

    public String getContentParamCart() {
        return String.format("cart%s", cartCount > 1 ? "1" : "");
    }

    public String getContentParamDisc() {
        return String.format("cdrm%s", discCount > 1 ? "1" : "");
    }

    public String getContentParamDisk() {
        return String.format("flop%s", floppyCount > 1 ? "1" : "");
    }

    public String getContentParamFirm() {
        return String.format("rom%s", romCount > 1 ? "1" : "");
    }

    public String getContentParamQuick() {
        return "quik";
    }

    public String getContentParamSnap() {
        return "dump";
    }

    public String getContentParamTape() {
        return String.format("cass%s", cassetteCount > 1 ? "1" : "");
    }

    @Override
    public Stream<String> getExecutablePaths() {
        return Stream.of("/usr/bin/mame", "/usr/games/mame");
    }
}
