package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.util.StreamTrait;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.TreeSet;

@Service
public class DemobaseGameboyContentMetadataEnricher implements ContentMetadataEnricher, StreamTrait {

    @Override
    public void enrich(String title, ContentMetadata contentMetadata) {
        contentMetadata.setTitle(title);

        Set<String> publishers = new TreeSet<>();
        Set<String> dates = new TreeSet<>();
        contentMetadata.getItems()
            .stream()
            .map(ContentMetadataItem::getName)
            .forEach(n -> {
                if (!StringUtils.startsWithIgnoreCase(n, title)) {
                    return;
                }

                var tokens = StringUtils.substringsBetween(n, "(", ")");
                if (tokens == null) {
                    return;
                }

                for (var token : tokens) {
                    if (StringUtils.isNumeric(token)) {
                        dates.add(token);
                    } else {
                        publishers.add(token);
                    }
                }
            });

        contentMetadata.setDate(String.join(",", dates));
        contentMetadata.setPublisher(String.join(",", publishers));
        contentMetadata.setTagsExtra("origin_demobaseGameBoy");
    }
}
