package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public interface ContentMetadataEnricher {
    Pattern PATTERN_PARENTHESES = Pattern.compile("\\((.*?)\\)");
    Pattern PATTERN_BRACKETS = Pattern.compile("\\[(.*?)\\]");

    default void applyOnPrefixFound(Collection<String> tokens, String prefix, Consumer<String> consumer) {
        new ArrayList<>(tokens)
            .stream()
            .filter(a -> StringUtils.startsWithIgnoreCase(a, prefix))
            .findFirst()
            .ifPresent(
                a -> {
                    var data = a.substring(prefix.length()).trim();
                    consumer.accept(data);
                    tokens.remove(a);
                });
    }

    void enrich(String title, ContentMetadata contentMetadata);
}
