package com.tincore.retromaster.service.execute.prepare;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.server.fuse.RetromasterFuseFsController;
import com.tincore.retromaster.service.execute.ContentExecutor;
import com.tincore.retromaster.service.execute.ExecutionContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;

import java.util.Comparator;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "tincore.retromaster.fuse.enabled", havingValue = "true")
public class RetromasterFuseFsExecutionPrepareStrategy implements ExecutionPrepareStrategy {

    private final RetromasterFuseFsController retromasterFuseFsController;

    @Override
    public void doBeforeExecution(ExecutionContext executionContext, boolean readOnly) {
        var targetPath = retromasterFuseFsController.getPath(executionContext.getMainItem().getContentMetadata(), readOnly);
        executionContext.setTargetProjectionPath(targetPath);
        executionContext.setMainItemPath(retromasterFuseFsController.getPath(executionContext.getMainItem(), readOnly));

        executionContext.getContentsAssociated()
            .stream()
            .sorted(Comparator.comparing(p1 -> p1.getLeft().getName()))
            .map(Pair::getLeft)
            .map(cmi -> retromasterFuseFsController.getPath(cmi, readOnly))
            .forEach(p -> executionContext.getOtherItemPaths().add(p));

        // All contentMetadata items are virtually projected in the filesystem with the right name.
        // You can expect that all contentMetadataItems with existing content can be accessed with the contentmetadataItem path
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Override
    public boolean isApplicable(ExecutionContext executionContext, ContentExecutor contentExecutor) {
        return retromasterFuseFsController != null && retromasterFuseFsController.isActive() && (contentExecutor.isReadOnlyExecutionCapable() || retromasterFuseFsController.isReadWriteCapable());
    }
}
