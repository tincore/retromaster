package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMediaType;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentMetadataSource;
import com.tincore.retromaster.service.ContentSystemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentMetadataSet.AUTHOR_LENGTH;
import static com.tincore.retromaster.domain.ContentMetadataSource.*;
import static com.tincore.retromaster.service.enrich.TosecContentMetadataEnricher.UNIDENTIFIED_PREFIX;

@Slf4j
@Service
@RequiredArgsConstructor
public class TosecContentMetadataSetEnricher extends AbstractContentMetadataSetEnricher implements ContentMetadataSetEnricher {

    private final ContentSystemService contentSystemService;
    private final TosecContentMetadataEnricher contentMetadataEnricher;

    @Override
    public boolean canEnrich(ContentMetadataSet contentMetadataSet) {
        return Stream.of(TOSEC, TOSEC_ISO, TOSEC_PIX).anyMatch(c -> c.isMatch(contentMetadataSet));
    }

    @Override
    public ContentMetadata enrichContentMetadata(ContentMetadata contentMetadata, String systemDefault) {
        contentMetadata.setMediaType(ContentMediaType.UNKNOWN); // Should eventually: Default from set
        contentMetadata.setSource(ContentMetadataSource.TOSEC.name());
        contentMetadata.setSystem(systemDefault); // TOSEC Forces system (only one system per set)

        var name = contentMetadata.getName();
        var parenthesesIndex = name.indexOf('(', 1);
        var demoCloseParentheses = name.indexOf(") ", parenthesesIndex);
        if (demoCloseParentheses != -1) {
            // title (demo) (date...)
            parenthesesIndex = name.indexOf('(', demoCloseParentheses);
        }
        var unknown = name.startsWith(UNIDENTIFIED_PREFIX);
        var bracketIndex = unknown ? name.indexOf('[', UNIDENTIFIED_PREFIX.length() + 1) : name.indexOf('[', 1);
        var titleStartIndex = unknown ? UNIDENTIFIED_PREFIX.length() : 0;
        var titleEndIndex = Math.min(parenthesesIndex == -1 ? name.length() : parenthesesIndex, bracketIndex == -1 ? name.length() : bracketIndex);
        var title = name.substring(titleStartIndex, titleEndIndex).trim();

        contentMetadataEnricher.enrich(title, contentMetadata);

        if (StringUtils.isBlank(contentMetadata.getTitle())) {
            log.error("No title!! {} {} ", contentMetadata.getName(), contentMetadata);
            return null;
        }

        return contentMetadata;
    }

    @Override
    public ContentMetadataSet enrichContentMetadataSet(ContentMetadataSet contentMetadataSet) {

        var category = Optional.ofNullable(contentMetadataSet.getCategory()).orElse(getContentMetadataSource().toString());
        contentMetadataSet.setCategory(category);

        var tokens = toStream(contentMetadataSet.getName().split(" - ")).map(String::trim).collect(Collectors.toList());
        tokens.remove(0);
        contentMetadataSet.setTagsExtra(String.join(",", tokens));

        if (!contentMetadataSet.isStaging() || StringUtils.isBlank(contentMetadataSet.getRepositoryPath())) {
            var repositoryPath = getRepositoryPath(contentMetadataSet);
            contentMetadataSet.setRepositoryPath(repositoryPath);
        }
        contentMetadataSet.setAuthor(StringUtils.left(contentMetadataSet.getAuthor(), AUTHOR_LENGTH));
        return contentMetadataSet;
    }

    @Override
    public String extractSystem(ContentMetadataSet contentMetadataSet) {
        var tokens = toStream(contentMetadataSet.getName().split(" - ")).map(String::trim).collect(Collectors.toList());
        var system = tokens.remove(0);
        return contentSystemService.findSystemNormalized(system);
    }

    @Override
    public ContentMetadataSource getContentMetadataSource() {
        return TOSEC;
    }

    @Override
    public String getRepositoryPath(ContentMetadataSet contentMetadataSet) {
        var tokens = toStream(contentMetadataSet.getName().split(" - ")).map(String::trim).collect(Collectors.toList());
        var systemNormalized = contentSystemService.findSystemNormalized(tokens.remove(0));
        return systemNormalized + " - " + String.join(" ", tokens) + " " + contentMetadataSet.getVersion().trim() + " " + contentMetadataSet.getCategory();
    }
}
