package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.config.TaskExecutorConfiguration;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentSignatureChange;
import com.tincore.retromaster.domain.event.ContentImportEvent;
import com.tincore.retromaster.domain.event.ContentImportEventType;
import com.tincore.util.Partition;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentSignatureChangeService {

    private final ContentSignatureChangeRepositoryService contentSignatureChangeRepositoryService;
    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentRepositoryService contentRepositoryService;

    private final TaskExecutorConfiguration.ForkJoinPoolProvider forkJoinPoolProvider;
    private final TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;
    private final ApplicationEventPublisher applicationEventPublisher;

    private final RetromasterConfiguration retromasterConfiguration;

    public String getComplete(ContentMetadata contentMetadata) {
        return contentMetadata.getContentMetadataSet().isStaging() ? ContentMetadata.COMPLETE_TOTAL : getComplete(contentMetadata.getId());
        // Alternative (SLOWER)
        // Collection<ContentMetadataItem> items = contentMetadata.getItems();
        // if (items == null || items.isEmpty()) {
        // return ContentMetadata.COMPLETE_NOITEMS;
        // } else {
        // log.debug("getComplete A {}", contentMetadata);
        // Set<String> contentMetadataItemSignatures = items.stream()
        // .map(ContentMetadataItem::getSignature)
        // .limit(20)
        // .collect(Collectors.toSet());
        // log.debug("getComplete B {}", contentMetadata);
        //
        // long count = contentRepositoryService.countDistinctSignaturesBySignatureIn(contentMetadataItemSignatures);
        // log.debug("getComplete C {}", contentMetadata);
        // if (count == 0) {
        // return ContentMetadata.COMPLETE_EMPTY;
        // } else {
        // boolean match = count == contentMetadataItemSignatures.size();
        // return match ? ContentMetadata.COMPLETE_TOTAL : ContentMetadata.COMPLETE_PARTIAL;
        // }
        // }
    }

    private String getComplete(UUID contentMetadataId) {
        var result = contentRepositoryService.findContentMetadataItemContentCountsGroupedByCount(contentMetadataId);
        if (result.length == 0) {
            return ContentMetadata.COMPLETE_NOITEMS;
        }
        var partial = result.length > 1;
        if (partial) {
            return ContentMetadata.COMPLETE_PARTIAL;
        }
        if (result[0][0] == 0 || result[0][1] == 0) {
            return ContentMetadata.COMPLETE_EMPTY;
        }
        return ContentMetadata.COMPLETE_TOTAL;
    }

    private Page<?> reindexContentSignatureBatch(AtomicInteger processedContentMetadatasCount, int contentSignatureProcessorFetchSize) {
        return transactionRequiresNewTemplate.execute(
            t -> {
                // Investigate if native query would perform better
                var pageRequest = PageRequest.of(0, contentSignatureProcessorFetchSize);
                var page = contentSignatureChangeRepositoryService.findAll(pageRequest);
                if (page.isEmpty()) {
                    return page;
                }
                log.info("reindexContentSignatureChanges remainingSignaturesCount={}", page.getTotalElements());

                var signaturesPartition = Partition.ofSize(page.get().map(ContentSignatureChange::getSignature).distinct().collect(Collectors.toList()), retromasterConfiguration.getContentSignatureProcessorBatchSize());
                try {
                    log.info("reindexContentSignatureChanges collecting affected contentMetadatas");
                    Set<UUID> contentMetadataIds = ConcurrentHashMap.newKeySet();
                    forkJoinPoolProvider.submitNow(
                        () -> IntStream.range(0, signaturesPartition.size())
                            .parallel()
                            .forEach(
                                i -> {
                                    var signatures = signaturesPartition.get(i);
                                    contentMetadataRepositoryService.findByContentMetadataIdSingaturesBySignatureIn(signatures).forEach(x -> contentMetadataIds.add(x.getContentMetadataId()));
                                }));

                    log.info("reindexContentSignatureChanges collected affected contentMetadatasCount={}", contentMetadataIds.size());

                    var count = new AtomicInteger(0); // NOPMD
                    var contentMetadataPartition = Partition.ofSize(new ArrayList<>(contentMetadataIds), retromasterConfiguration.getContentSignatureProcessorBatchSize()); // NOPMD
                    forkJoinPoolProvider.submitNow(
                        () -> IntStream.range(0, contentMetadataPartition.size())
                            .parallel()
                            .forEach(
                                i -> {
                                    var ids = contentMetadataPartition.get(i);
                                    processedContentMetadatasCount.addAndGet(ids.size());
                                    log.info("reindexContentSignatureChanges apply {}/{}", count.addAndGet(ids.size()), contentMetadataIds.size());
                                    transactionRequiresNewTemplate.execute(
                                        ts -> {
                                            ids.stream()
                                                .map(contentMetadataRepositoryService::findById) // Could anybody delete in the meanwhile??
                                                .filter(Optional::isPresent)
                                                .map(Optional::get)
                                                .forEach(
                                                    cm -> {
                                                        var complete = getComplete(cm);
                                                        if (!complete.equals(cm.getComplete())) {
                                                            cm.setComplete(complete);
                                                            contentMetadataRepositoryService.save(cm);
                                                        }
                                                    });
                                            return true;
                                        });
                                }));

                    log.info("reindexContentSignatureChanges cleaning signatures");
                    forkJoinPoolProvider.submitNow(
                        () -> IntStream.range(0, signaturesPartition.size())
                            .parallel()
                            .forEach(
                                i -> {
                                    var signatures = signaturesPartition.get(i);
                                    transactionRequiresNewTemplate.execute(
                                        ts -> {
                                            signatures.forEach(contentSignatureChangeRepositoryService::deleteById);
                                            return true;
                                        });
                                }));
                    log.info("reindexContentSignatureChanges signatures cleaned");
                } catch (ExecutionException | InterruptedException e) {
                    log.error("reindexContentSignatureChanges", e);
                    throw new WrappedException(e);
                }

                return page;
            });
    }

    public void reindexContentSignatureChanges() {
        var moreToProcess = true;
        var processedSignaturesCount = 0;
        var processedContentMetadatasCount = new AtomicInteger();
        var contentSignatureProcessorFetchSize = retromasterConfiguration.getContentSignatureProcessorFetchSize();
        while (moreToProcess) {
            var page = reindexContentSignatureBatch(processedContentMetadatasCount, contentSignatureProcessorFetchSize);
            var elementCount = page.getNumberOfElements();
            processedSignaturesCount += elementCount;
            moreToProcess = elementCount == contentSignatureProcessorFetchSize; // NOPMD
            if (processedSignaturesCount > 0) {
                log.info("reindexContentSignatureChanges moreToProcess={}, {}, {}", moreToProcess, elementCount, processedSignaturesCount);
            }
        }

        if (processedSignaturesCount > 0) {
            applicationEventPublisher.publishEvent(new ContentImportEvent(ContentImportEventType.SUCCESS, 0));
            log.info("reindexContentSignatureChanges complete signatures={}, contentMetadatas={}", processedSignaturesCount, processedContentMetadatasCount.get());
        }
    }

    public void reset() {
        contentSignatureChangeRepositoryService.deleteAll();
    }
}
