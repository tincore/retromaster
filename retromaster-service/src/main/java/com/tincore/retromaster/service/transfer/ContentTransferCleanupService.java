package com.tincore.retromaster.service.transfer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.transfer.OriginContentTransfer;
import com.tincore.retromaster.service.ArchiveService;
import com.tincore.retromaster.service.ContentRepositoryService;
import com.tincore.retromaster.service.ContentService;
import com.tincore.retromaster.service.ContentVolumeService;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.tincore.retromaster.domain.transfer.ContentTransfer.UNKOWN_PATH;
import static com.tincore.util.lang.ThrowingConsumer.uConsumer;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentTransferCleanupService implements StreamTrait, FileSystemTrait {

    private final ContentVolumeService contentVolumeService;
    private final ContentRepositoryService contentRepositoryService;
    private final ContentService contentService;
    private final ArchiveService archiveService;

    private final TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;

    public void cleanUpDisposableContents(Map<Path, List<OriginContentTransfer>> originContentTransfersByTargetFilePath) {
        transactionRequiresNewTemplate.executeWithoutResult(t -> cleanUpDisposableContentsInner(originContentTransfersByTargetFilePath));
    }

    private void cleanUpDisposableContentsInner(Map<Path, List<OriginContentTransfer>> originContentTransfers) { // NOPMD Complex
        var cleanableContentsInOriginBySignature = originContentTransfers.entrySet()
            .stream()
            .flatMap(
                e -> {
                    var path = e.getKey();
                    var transfers = e.getValue();
                    if (path.equals(UNKOWN_PATH)) {
                        return transfers.stream().filter(oct -> !oct.isManagedByTransferFlag());
                    }
                    return transfers.stream().filter(oct -> oct.isImportedFlag() && !oct.isManagedByTransferFlag());
                })
            .collect(Collectors.groupingBy(ocs -> ocs.getContent().getSignature(), Collectors.toList()));

        if (cleanableContentsInOriginBySignature.isEmpty()) {
            log.debug("cleanUpDisposableContents: Nothing to clean");
            return;
        }

        List<OriginContentTransfer> contentsInOriginToRemove = new ArrayList<>();
        List<OriginContentTransfer> contentsInOriginToKeep = new ArrayList<>();

        cleanableContentsInOriginBySignature.forEach(
            (signature, transfers) -> {
                var originContentTransfer = transfers.get(0);
                var contentsWithSameSignature = contentRepositoryService.findBySignature(originContentTransfer.getContent().getSignature());
                // When target and source are the same then do nothing because it is already stored
                // Attention, These two conditions are not exclusive when there are duplicates!
                var anyContentWithSameSignatureAndExactLocationAlreadyStored = contentsWithSameSignature.stream().filter(c -> c.isSameLocationAs(originContentTransfer.getContent())).anyMatch(Content::isFileExistsAndValid);
                if (anyContentWithSameSignatureAndExactLocationAlreadyStored && originContentTransfer.isIdentified() && !originContentTransfer.isDeleteContentFlag()) {
                    log.warn("cleanUpDisposableContents: Found known content with same signature and stored trying to be removed. Skip this one. SHOULD HAVE BEEN MARKED BY TRANSFER??? oct={}", originContentTransfer);
                } else {
                    var anyContentWithSameSignatureAndDifferentLocationAlreadyStored = contentsWithSameSignature.stream().filter(c -> !c.isSameLocationAs(originContentTransfer.getContent())).anyMatch(Content::isFileExistsAndValid);
                    if (anyContentWithSameSignatureAndDifferentLocationAlreadyStored) {
                        contentsInOriginToRemove.addAll(transfers);
                    } else {
                        contentsInOriginToKeep.add(originContentTransfer); // Keep one for each signature
                        var multiple = transfers.size() > 1;
                        if (multiple) { // Remove all other contents with same signature
                            contentsInOriginToRemove.addAll(transfers.subList(1, transfers.size()));
                        }
                    }
                }
            });

        // Remove regular files that are used
        contentsInOriginToRemove.stream()
            .filter(o -> !o.getContent().isCompressedEntry())
            .forEach(
                o -> {
                    try {
                        log.debug("cleanUpDisposableContents: Removing regular, path={}", o.getContent().getPathFileAbsolute());
                        Files.deleteIfExists(o.getContent().getPathFileAbsolute());
                        if (o.isContentDeletable()) {
                            contentRepositoryService.delete(o.getContent());
                        }
                    } catch (IOException e) {
                        log.warn("cleanUpDisposableContents: Remove failed, path={}", o.getContent().getPathFileAbsolute(), e);
                    }
                });

        // Move regular files that are unknown
        var unknownContentRootPath = contentVolumeService.getExportUnknownPath();
        contentsInOriginToKeep.stream()
            .filter(o -> !o.getContent().isCompressedEntry())
            .forEach(
                o -> {
                    try {
                        var unknownFilePath = unknownContentRootPath.resolve(o.getContent().getPathFileRelative());
                        log.debug("cleanUpDisposableContents: Moving regular, path={}, target={}", o.getContent().getPathFileAbsolute(), unknownFilePath);
                        contentService.moveToUnique(o.getContent().getPathFileAbsolute(), unknownFilePath);
                        if (o.isContentDeletable()) {
                            contentRepositoryService.delete(o.getContent());
                        }
                    } catch (IOException e) {
                        log.error("cleanUpDisposableContents: Moving Failed Staging Regular Unknown", e);
                        // Move to broken?
                    }
                });

        // Archived origin contents
        var archivedContentsToRemoveByOriginFileRelativePath
            = contentsInOriginToRemove.stream().filter(ocs -> ocs.getContent().isCompressedEntry()).collect(Collectors.groupingBy(ocs -> ocs.getContent().getPathFileRelative(), Collectors.toList()));
        var archivedContentsToKeepByOriginFileRelativePath
            = contentsInOriginToKeep.stream().filter(ocs -> ocs.getContent().isCompressedEntry()).collect(Collectors.groupingBy(ocs -> ocs.getContent().getPathFileRelative(), Collectors.toList()));

        archivedContentsToRemoveByOriginFileRelativePath
            .entrySet()
            .parallelStream()
            .forEach(
                uConsumer(
                    e -> {
                        var relativePath = e.getKey();
                        var originContentTransfer = e.getValue().get(0);
                        var originFilePath = originContentTransfer.getContent().getPathFileAbsolute(); // They all come from same origin volume so no problem relative != absolute

                        var contentsToKeep = archivedContentsToKeepByOriginFileRelativePath.remove(relativePath);
                        var nothingToKeep = contentsToKeep == null || contentsToKeep.isEmpty();
                        if (nothingToKeep) {
                            if (Files.deleteIfExists(originFilePath)) {
                                log.debug("cleanUpDisposableContents: By deleting origin, path={}", originFilePath);
                            }
                        } else { // Keep partial or all!f
                            var targetFilePath = getPathUniqueFile(unknownContentRootPath.resolve(relativePath));
                            if (originContentTransfer.getOriginContentCount() == contentsToKeep.size()) {
                                log.debug("cleanUpDisposableContents: By moving unknown, path={}, target={}", originFilePath, targetFilePath);
                                move(originFilePath, targetFilePath);
                            } else {
                                log.debug("cleanUpDisposableContents: By re-archiving to unknown, path={}, target={}", originFilePath, targetFilePath);
                                var entryNamesToKeep = contentsToKeep.stream().map(OriginContentTransfer::getContent).map(Content::getFileEntry).collect(Collectors.toList());
                                archiveService.rearchive(originFilePath, entryNamesToKeep, targetFilePath);
                                Files.deleteIfExists(originFilePath);
                            }
                            deleteContents(contentsToKeep);
                        }
                        deleteContents(e.getValue());
                    }));

        // Remaining non processed are transfers
        archivedContentsToKeepByOriginFileRelativePath
            .entrySet()
            .parallelStream()
            .forEach(
                uConsumer(
                    e -> {
                        var relativePath = e.getKey();
                        var unknownContentsToKeep = e.getValue();

                        var originContentTransfer = unknownContentsToKeep.get(0);
                        var content = originContentTransfer.getContent();
                        var originFilePath = content.getPathFileAbsolute();
                        var targetFilePath = getPathUniqueFile(unknownContentRootPath.resolve(relativePath));

                        if (originContentTransfer.getOriginContentCount() == unknownContentsToKeep.size()) {
                            log.debug("cleanUpDisposableContents: By direct moving to unknown, path={}, target={}", originFilePath, targetFilePath);
                            move(originFilePath, targetFilePath);
                        } else {
                            log.debug("cleanUpDisposableContents: By archiving to unknown, path={}, target={}", originFilePath, targetFilePath);
                            var entryNamesToKeep = unknownContentsToKeep.stream().map(OriginContentTransfer::getContent).map(Content::getFileEntry).collect(Collectors.toList());
                            archiveService.rearchive(originFilePath, entryNamesToKeep, targetFilePath);
                            Files.deleteIfExists(originFilePath);
                        }

                        deleteContents(unknownContentsToKeep);
                    }));
        // if broken move to /broken?
    }

    private void deleteContents(List<OriginContentTransfer> originContentTransfers) {
        originContentTransfers.stream().filter(OriginContentTransfer::isContentDeletable).forEach(o -> contentRepositoryService.delete(o.getContent()));
    }
}
