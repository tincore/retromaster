package com.tincore.retromaster.server.fuse.content;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.server.fuse.fs.ExtendedFuseFs;
import com.tincore.retromaster.server.fuse.fs.FuseDirectory;
import com.tincore.retromaster.server.fuse.fs.FuseEntry;
import com.tincore.retromaster.server.fuse.fs.FuseFile;
import jnr.ffi.Platform;
import jnr.ffi.Pointer;
import jnr.ffi.types.mode_t;
import jnr.ffi.types.off_t;
import jnr.ffi.types.size_t;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.serce.jnrfuse.ErrorCodes;
import ru.serce.jnrfuse.FuseFillDir;
import ru.serce.jnrfuse.FuseStubFS;
import ru.serce.jnrfuse.struct.FileStat;
import ru.serce.jnrfuse.struct.FuseFileInfo;
import ru.serce.jnrfuse.struct.Statvfs;

import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import static jnr.ffi.Platform.OS.WINDOWS;

@Slf4j
@Service
@ConditionalOnProperty(value = "tincore.retromaster.fuse.enabled", havingValue = "true")
public class ContentFuseFs extends FuseStubFS implements ExtendedFuseFs { // NOPMD God
    private static final int BLOCK_SIZE = 1024;
    private static final boolean READ_ONLY = true;

    private static final String PATH_SEPARATOR = "/";

    private final Map<String, FuseFile> openedFuseFilesByPath = new HashMap<>();
    private final FuseDirectory rootDirectory;

    public ContentFuseFs(ContentFuseFsEntryFactory contentFuseFsEntryFactory) {
        super();

        this.rootDirectory = contentFuseFsEntryFactory.createRootDirectory(getContext());
    }

    @Override
    public int create(String path, @mode_t long mode, FuseFileInfo fi) {
        if (READ_ONLY) {
            return -ErrorCodes.EACCES();
        }

        if (getFuseEntry(path) != null) {
            return -ErrorCodes.EEXIST();
        }
        var parent = getFuseEntryParent(path);
        if (parent instanceof FuseDirectory) {
            ((FuseDirectory) parent).mkfile(getLastComponent(path), getContext().uid.get(), getContext().gid.get());
            return 0;
        }
        return -ErrorCodes.ENOENT();
    }

    @Override
    public FuseEntry getFuseEntry(Path p) {
        return rootDirectory.find(toTokens(p));
    }

    private String getLastComponent(String path) {
        var p = path;
        while (p.endsWith(PATH_SEPARATOR)) {
            p = p.substring(0, p.length() - 1);
        }
        if (p.isEmpty()) {
            return "";
        }
        return p.substring(p.lastIndexOf('/') + 1);
    }

    @Override
    public int getattr(String path, FileStat stat) {
        var entry = getFuseEntry(path);
        if (entry != null) {
            entry.loadAttr(stat);
            return 0;
        }
        return -ErrorCodes.ENOENT();
    }

    @Override
    public int mkdir(String path, @mode_t long mode) {
        if (getFuseEntry(path) != null) {
            return -ErrorCodes.EEXIST();
        }
        var parent = getFuseEntryParent(path);
        if (parent instanceof FuseDirectory) {
            ((FuseDirectory) parent).mkdir(getLastComponent(path), getContext().uid.get(), getContext().gid.get());
            return 0;
        }
        return -ErrorCodes.ENOENT();
    }

    @Override
    public int open(String path, FuseFileInfo fi) {
        // log.trace("open {} {}", path, fi);
        if (!openedFuseFilesByPath.containsKey(path)) {
            var fe = getFuseEntry(path);
            if (fe instanceof FuseFile) {
                openedFuseFilesByPath.put(path, (FuseFile) fe);
            }
        }
        return super.open(path, fi);
    }

    @Override
    public int read(String path, Pointer buf, @size_t long size, @off_t long offset, FuseFileInfo fi) {
        var fuseFile = openedFuseFilesByPath.get(path);
        if (fuseFile != null) {
            return fuseFile.read(buf, size, offset);
        }

        var entry = getFuseEntry(path);
        if (entry == null) {
            return -ErrorCodes.ENOENT();
        }
        if (entry instanceof FuseDirectory) {
            return -ErrorCodes.EISDIR();
        }
        return ((FuseFile) entry).read(buf, size, offset);
    }

    @Override
    public int readdir(String path, Pointer buf, FuseFillDir filter, @off_t long offset, FuseFileInfo fi) {
        var entry = getFuseEntry(path);
        if (entry == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(entry instanceof FuseDirectory)) {
            return -ErrorCodes.ENOTDIR();
        }
        ((FuseDirectory) entry).readDir(buf, filter);
        return 0;
    }

    @Override
    public int release(String path, FuseFileInfo fi) {
        // log.trace("release {} {}", path, fi);
        openedFuseFilesByPath.remove(path);
        return super.release(path, fi);
    }

    @Override
    public int rename(String path, String newName) {
        if (READ_ONLY) {
            return -ErrorCodes.EACCES();
        }

        var entry = getFuseEntry(path);
        if (entry == null) {
            return -ErrorCodes.ENOENT();
        }
        var newParent = getFuseEntryParent(newName);
        if (newParent == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(newParent instanceof FuseDirectory)) {
            return -ErrorCodes.ENOTDIR();
        }
        return entry.rename(newName.substring(newName.lastIndexOf('/')));
//        entry.delete();
//        // ((FuseDirectory) newParent).add(entry); IMPLEMENT MOVE
//        return 0;
    }

    @Override
    public int rmdir(String path) {
        if (READ_ONLY) {
            return -ErrorCodes.EACCES();
        }

        var entry = getFuseEntry(path);
        if (entry == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(entry instanceof FuseDirectory)) {
            return -ErrorCodes.ENOTDIR();
        }
        entry.delete();
        return 0;
    }

    @Override
    public int statfs(String path, Statvfs stbuf) {
        // statfs needs to be implemented on Windows in order to allow for copying
        // data from other devices because winfsp calculates the volume size based
        // on the statvfs call.
        // see https://github.com/billziss-gh/winfsp/blob/14e6b402fe3360fdebcc78868de8df27622b565f/src/dll/fuse/fuse_intf.c#L654
        if (Platform.getNativePlatform().getOS() == WINDOWS && PATH_SEPARATOR.equals(path)) {
            stbuf.f_blocks.set(BLOCK_SIZE * BLOCK_SIZE); // total data blocks in file system
            stbuf.f_frsize.set(BLOCK_SIZE); // fs block size
            stbuf.f_bfree.set(BLOCK_SIZE * BLOCK_SIZE); // free blocks in fs
        }
        return super.statfs(path, stbuf);
    }

    public Deque<String> toTokens(Path path) {
        var normalized = path.normalize();
        var stack = new ArrayDeque<String>();
        do {
            var part = normalized.getFileName();
            if (part == null) {
                return stack;
            }

            var token = part.toString();
            if (StringUtils.isBlank(token)) {
                return stack;
            }

            stack.addFirst(token);
            normalized = normalized.getParent();
            if (normalized == null) {
                return stack;
            }
        } while (true);
    }

    @Override
    public int truncate(String path, long offset) {
        if (READ_ONLY) {
            return -ErrorCodes.EACCES();
        }

        var entry = getFuseEntry(path);
        if (entry == null) {
            return -ErrorCodes.ENOENT();
        }
        if (entry instanceof FuseDirectory) {
            return -ErrorCodes.EISDIR();
        }
        ((FuseFile) entry).truncate(offset);
        return 0;
    }

    @Override
    public int unlink(String path) {
        if (READ_ONLY) {
            return -ErrorCodes.EACCES();
        }

        var entry = getFuseEntry(path);
        if (entry == null) {
            return -ErrorCodes.ENOENT();
        }
        entry.delete();
        return 0;
    }

    @Override
    public int write(String path, Pointer buf, @size_t long size, @off_t long offset, FuseFileInfo fi) {
        if (READ_ONLY) {
            return -ErrorCodes.EACCES();
        }

        var entry = getFuseEntry(path);
        if (entry == null) {
            return -ErrorCodes.ENOENT();
        }
        if (entry instanceof FuseDirectory) {
            return -ErrorCodes.EISDIR();
        }

        return ((FuseFile) entry).write(buf, size, offset);
    }
}
