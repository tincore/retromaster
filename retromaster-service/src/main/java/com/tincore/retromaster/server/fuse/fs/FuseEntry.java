package com.tincore.retromaster.server.fuse.fs;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import ru.serce.jnrfuse.ErrorCodes;
import ru.serce.jnrfuse.struct.FileStat;

import java.io.File;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.StringJoiner;

@Slf4j
public abstract class FuseEntry {

    private final long uid;
    private final long gid;

    private final boolean writable;
    private FuseDirectory parent;

    public FuseEntry(FuseDirectory parent, long uid, long gid, boolean writable) {
        this.parent = parent;

        this.uid = uid;
        this.gid = gid;

        this.writable = writable;
    }

    public void delete() {
        if (parent != null) {
            parent.rmFile(this);
            parent = null;
        }
    }

    public abstract FuseEntry find(Deque<String> tokenStack);

    public FuseEntry findChild(String name) {
        var deque = new LinkedList<String>();
        deque.add(name);
        return find(deque);
    }

    public long getGid() {
        return gid;
    }

    public abstract String getName();

    public String getPath() {
        var tokens = new ArrayList<String>();
        var entry = this;
        while (entry != null) {
            tokens.add(entry.getName());
            entry = entry.parent;
        }
        var joiner = new StringJoiner(File.separator);
        for (var i = tokens.size() - 1; i >= 0; i--) {
            var token = tokens.get(i);
            if (StringUtils.isNotBlank(token)) {
                joiner.add(token);
            }
        }
        return File.separator + joiner;
    }

    public long getUid() {
        return uid;
    }

    public boolean isWritable() {
        return writable;
    }

    public void loadAttr(FileStat stat) {
        stat.st_uid.set(uid);
        stat.st_gid.set(gid);
    }

    public int rename(String newName) {
        log.info("rename not implemented {}", newName);
        return -ErrorCodes.EOPNOTSUPP();
    }

    // public void rename(String newName) {
    // while (newName.startsWith("/")) {
    // newName = newName.substring(1);
    // }
    // name = newName;
    // }
}
