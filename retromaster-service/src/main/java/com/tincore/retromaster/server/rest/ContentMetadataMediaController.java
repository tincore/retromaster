package com.tincore.retromaster.server.rest;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.service.ResourceNotFoundException;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMediaType;
import com.tincore.retromaster.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.FileNameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Optional;
import java.util.UUID;

import static com.tincore.util.lang.ThrowingFunction.uFunction;


@Slf4j
@Controller
@RequiredArgsConstructor
public class ContentMetadataMediaController {
    public static final Duration CACHE_DURATION = Duration.ofDays(365);

    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;
    private final ContentMetadataSearchService contentMetadataSearchService;
    private final ContentService contentService;
    private final ContentRepositoryService contentRepositoryService;

    private static MediaType toContentType(String extension) {
        return switch (extension) {
            case "pdf" -> MediaType.APPLICATION_PDF;
            case "jpg" -> MediaType.IMAGE_JPEG;
            case "png" -> MediaType.IMAGE_PNG;
            case "gif" -> MediaType.IMAGE_GIF;
            default -> throw new IllegalStateException("Unexpected value: " + extension);
        };
    }

    private static ResponseEntity<Resource> toResponseEntity(Path path) throws IOException {
        return ResponseEntity.ok()
            .cacheControl(CacheControl.maxAge(CACHE_DURATION))
            .contentType(toContentType(FileNameUtils.getExtension(path)))
            .body(new InputStreamResource(Files.newInputStream(path)));
    }

    private Optional<Path> findPath(String system, String title, ContentMediaType contentMediaType) {
        return contentMetadataSearchService.searchBySystemAndTitleAndMediaTypeAndCompleteNotEmpty(system, title, contentMediaType, PageRequest.of(0, 1)).getContent()
            .stream()
            .flatMap(cm -> cm.getItems().stream())
            .map(contentService::findContentByContentMetadataItem)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(Content::getPathFileAbsolute)
            .filter(Files::exists)
            .findFirst();
    }

    @GetMapping(value = {"/rest/contentMetadata/{id}/mediaType/{mediaType}/index/{index}", "/rest/contentMetadata/{id}/mediaType/{mediaType}"}, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_PDF_VALUE})
    @ResponseBody
    @Transactional
    public ResponseEntity<Resource> getByContentMetadataIdIdAndMediaType(@PathVariable("id") UUID id, @PathVariable("mediaType") ContentMediaType contentMediaType, @PathVariable(value = "index", required = false) Integer index) {
        return contentMetadataRepositoryService.findById(id)
            .filter(m -> StringUtils.isNotBlank(m.getTitle()))
            .flatMap(uFunction(m -> findPath(m.getSystem(), m.getTitle(), contentMediaType)))
            .map(uFunction(ContentMetadataMediaController::toResponseEntity))
            .orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + id));
    }

    @GetMapping(value = {"/rest/contentMetadataItem/{id}"}, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_PDF_VALUE})
    @ResponseBody
    @Transactional
    public ResponseEntity<Resource> getByContentMetadataItemId(@PathVariable("id") UUID id) {
        return contentMetadataItemRepositoryService.findById(id)
            .flatMap(contentService::findContentByContentMetadataItem)
            .map(Content::getPathFileAbsolute)
            .filter(Files::exists)
            .map(uFunction(ContentMetadataMediaController::toResponseEntity))
            .orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + id));
    }

    @GetMapping(value = {"/rest/contentSignature/{signature}"}, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_PDF_VALUE})
    @ResponseBody
    @Transactional
    public ResponseEntity<Resource> getByContentSignature(@PathVariable("signature") String signature) throws IOException {
        var content = contentRepositoryService.findFirstBySignature(signature)
            .orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + signature));

        var path = content.getPathFileAbsolute();
        if (Files.exists(path)) {
            return toResponseEntity(path);
        }

        log.warn("Found zombi content without matching file!! Looking up all contents with same signature, content={}", content);
        return contentRepositoryService.findBySignature(signature).stream().map(Content::getPathFileAbsolute)
            .filter(Files::exists)
            .findFirst()
            .map(uFunction(ContentMetadataMediaController::toResponseEntity))
            .orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + signature));
    }

    @GetMapping(value = {"/rest/contentMetadata/system/{system}/title/{title}/mediaType/{mediaType}/index/{index}", "/rest/contentMetadata/system/{system}/title/{title}/mediaType/{mediaType}"}, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_PDF_VALUE})
    @ResponseBody
    @Transactional
    public ResponseEntity<Resource> getBySystemAndTitleAndAndMediaType(@PathVariable("system") String system, @PathVariable("title") String title, @PathVariable("mediaType") ContentMediaType contentMediaType, @PathVariable(value = "index", required = false) Integer index) {
        return Optional.of(title)
            .filter(StringUtils::isNotBlank)
            .flatMap(uFunction(s -> findPath(system, s, contentMediaType)))
            .map(uFunction(ContentMetadataMediaController::toResponseEntity))
            .orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + system));
    }
}
