package com.tincore.retromaster.server.websocket;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import static com.tincore.retromaster.config.WebSocketConfiguration.MESSAGE_PING;
import static com.tincore.retromaster.config.WebSocketConfiguration.MESSAGE_TOPIC_ECHO;

@Controller
public class PingWebsocketController {

    @MessageMapping(MESSAGE_PING)
    @SendTo(MESSAGE_TOPIC_ECHO)
    public String onPing(String payload) {
        return "echo " + payload;
    }
}
