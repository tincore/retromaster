package com.tincore.retromaster.server.fuse.content;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.FuseConfiguration;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.server.fuse.fs.FuseDirectory;
import com.tincore.retromaster.server.fuse.fs.FuseEntry;
import com.tincore.retromaster.server.fuse.fs.FuseFile;
import com.tincore.retromaster.service.*;
import jnr.ffi.Pointer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.serce.jnrfuse.struct.FuseContext;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "tincore.retromaster.fuse.enabled", havingValue = "true")
public class ContentFuseFsEntryFactory {
    public static final String NAME_BY_SET = "bySet";
    public static final String NAME_BY_SYSTEM = "bySystem";
    public static final String NAME_BY_SET_PREFIXES = "bySetPrefixes";


    private final ContentMetadataSearchService contentMetadataSearchService;
    private final ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;
    private final ContentRepositoryService contentRepositoryService;
    private final ArchiveService archiveService;
    private final FuseConfiguration fuseConfiguration;

    public FuseRootDirectory createRootDirectory(long uid, long gid) {
        var rootDirectory = new FuseRootDirectory(uid, gid);
        rootDirectory.add(new ContentMetadataSetsFuseDirectory(rootDirectory, uid, gid, NAME_BY_SET));
        rootDirectory.add(new ContentMetadataSetsPrefixesFuseDirectory(rootDirectory, uid, gid, NAME_BY_SET_PREFIXES));
        rootDirectory.add(new ContentSystemsFuseDirectory(rootDirectory, uid, gid, NAME_BY_SYSTEM));
        return rootDirectory;
    }

    public FuseRootDirectory createRootDirectory(FuseContext context) {
        return createRootDirectory(context.uid.get(), context.gid.get());
    }

    private class ContentMetadataSetsPrefixesFuseDirectory extends FuseDirectory {
        private final String name;

        ContentMetadataSetsPrefixesFuseDirectory(FuseDirectory parent, long uid, long gid, String name) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureByWritable());
            this.name = name;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            var contentMetadataSetPrefix = tokenStack.pop();
            return new ContentMetadataSetsPrefixFuseDirectory(this, getUid(), getGid(), contentMetadataSetPrefix, fuseConfiguration.isContentMetadataSetsPrefixDirectoryContentIncludeIds())
                .find(tokenStack);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            var contentMetadataSetNames = contentMetadataSetRepositoryService.findAllContentMetadataSetNames();
            return contentMetadataSetNames.stream().map(ContentMetadataSet::getNamePrefix).distinct().sorted();
        }

        @Override
        public String getName() {
            return name;
        }
    }

    private class ContentSystemsFuseDirectory extends FuseDirectory {

        private final String name;

        ContentSystemsFuseDirectory(FuseDirectory parent, long uid, long gid, String name) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureByWritable());
            this.name = name;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            var contentSystem = tokenStack.pop();
            return new ContentSystemDirectory(this, getUid(), getGid(), contentSystem).find(tokenStack);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            return contentMetadataSearchService.searchContentMetadataSystemsByContentNotEmpty().stream();
        }

        @Override
        public String getName() {
            return name;
        }
    }

    /**
     * Contains contentMetadata prefixes for a system
     */
    private class ContentSystemDirectory extends FuseDirectory {
        private final String contentSystem;

        ContentSystemDirectory(FuseDirectory parent, long uid, long gid, String contentSystem) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureSystemsWritable());
            this.contentSystem = contentSystem;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            var contentMetadataPrefix = tokenStack.pop();
            return new ContentMetadatasPrefixByContentSystemFuseDirectory(this, getUid(), getGid(), contentSystem, contentMetadataPrefix).find(tokenStack);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            return contentMetadataSearchService.searchContentMetadataPrefixBySystemAndContentNotEmpty(contentSystem).stream();
        }

        @Override
        public String getName() {
            return contentSystem;
        }
    }

    private class ContentMetadataFuseDirectory extends FuseDirectory {
        private final String directoryName;
        private final UUID contentMetadataId;
        private final boolean staging;

        ContentMetadataFuseDirectory(FuseDirectory parent, long uid, long gid, String directoryName, UUID contentMetadataId, boolean staging) {
            super(parent, uid, gid, true);
            this.directoryName = directoryName;
            this.contentMetadataId = contentMetadataId;
            this.staging = staging;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            var stringFile = tokenStack.pop();
            var leaf = tokenStack.isEmpty();
            if (leaf) {
                var contentMetadataItems = contentMetadataItemRepositoryService.findByContentMetadataId(contentMetadataId);
                var item = contentMetadataItems.stream().filter(cmi -> cmi.getName().equals(stringFile)).findFirst();
                if (item.isPresent()) {
                    return new ContentMetadataItemFuseFile(this, getUid(), getGid(), item.get());
                }
                var directoryName = contentMetadataItems.stream().map(ContentMetadataItem::getName).filter(n -> n.contains("/")).map(n -> StringUtils.substringBefore(n, "/")).filter(n -> n.equals(stringFile)).findFirst();
                if (directoryName.isPresent()) {
                    return new ContentMetadataItemFuseDirectory(this, getUid(), getGid(), stringFile, stringFile, contentMetadataId, staging);
                }
                return null;
            } else {
                FuseDirectory contentMetadataItemDirectory = new ContentMetadataItemFuseDirectory(this, getUid(), getGid(), stringFile, stringFile, contentMetadataId, staging);
                return contentMetadataItemDirectory.find(tokenStack);
            }
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            var contentMetadataItems = contentMetadataItemRepositoryService.findByContentMetadataId(contentMetadataId);
            var directoryNames = contentMetadataItems.stream().map(ContentMetadataItem::getName).filter(n -> n.contains("/")).map(n -> StringUtils.substringBefore(n, "/")).distinct().sorted();

            var fileNames = contentMetadataItems.stream()
                .filter(
                    cmi -> {
                        if (cmi.getName().contains("/")) {
                            return false;
                        }
                        if (staging) {
                            return true;
                        }
                        return contentRepositoryService.countAllBySignature(cmi.getSignature()) > 0;
                    })
                .map(ContentMetadataItem::toStringFile);

            return Stream.concat(directoryNames, fileNames);
        }

        @Override
        public String getName() {
            return directoryName;
        }
    }

    private class ContentMetadataItemFuseDirectory extends FuseDirectory {
        private final String directoryName;
        private final String path;
        private final UUID contentMetadataId;
        private final boolean staging;

        ContentMetadataItemFuseDirectory(FuseDirectory parent, long uid, long gid, String directoryName, String path, UUID contentMetadataId, boolean staging) {
            super(parent, uid, gid, true);
            this.directoryName = directoryName;
            this.path = path;
            this.contentMetadataId = contentMetadataId;
            this.staging = staging;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            var token = tokenStack.pop();
            var leafFile = tokenStack.isEmpty();
            if (leafFile) {
                var contentMetadataItemName = path + "/" + token;
                // collapse pop to find exact name
                return contentMetadataItemRepositoryService.findFirstByContentMetadataIdAndName(contentMetadataId, contentMetadataItemName)
                    .map(cmi -> (FuseEntry) new ContentMetadataItemFuseFile(this, getUid(), getGid(), cmi))
                    .orElseGet(() -> {
                        // Or its a directory containing some contents inside or its not found leaf file!
                        int count = contentMetadataItemRepositoryService.countAllByContentMetadataIdAndNameStartsWith(contentMetadataId, contentMetadataItemName);
                        if (count > 0) {
                            return new ContentMetadataItemFuseDirectory(this, getUid(), getGid(), token, path + "/" + token, contentMetadataId, staging);
                        } else {
                            return null;
                        }
                    });
            } else {
                return new ContentMetadataItemFuseDirectory(this, getUid(), getGid(), token, path + "/" + token, contentMetadataId, staging).find(tokenStack);
            }
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            var contentMetadataItems = contentMetadataItemRepositoryService.findByContentMetadataId(contentMetadataId);
            var prefix = path + "/";
            var directoryNames = contentMetadataItems.stream()
                .map(ContentMetadataItem::getName)
                .filter(n -> n.startsWith(prefix))
                .map(n -> n.substring(prefix.length()))
                .filter(n -> n.contains("/"))
                .map(n -> StringUtils.substringBefore(n, "/"))
                .distinct()
                .sorted();

            var fileNames = contentMetadataItems.stream()
                .filter(
                    cmi -> {
                        var name = cmi.getName();
                        if (!name.startsWith(prefix)) {
                            return false;
                        }
                        var directory = name.indexOf('/', prefix.length()) > 0;
                        if (directory) {
                            return false;
                        }

                        if (staging) {
                            return true;
                        }
                        return contentRepositoryService.countAllBySignature(cmi.getSignature()) > 0;
                    })
                .map(ContentMetadataItem::toStringFile);

            return Stream.concat(directoryNames, fileNames);
        }

        @Override
        public String getName() {
            return directoryName;
        }
    }

    private class ContentMetadataItemFuseFile extends FuseFile {

        private static final int DEFAULT_BLOCK_SIZE = 1024 * 128;

        private final String fileName;
        private final String signature;
        private final long size;

        private final int blockSize;
        private List<byte[]> blocks;


        ContentMetadataItemFuseFile(FuseDirectory parent, long uid, long gid, String fileName, String signature, long size) {
            super(parent, uid, gid, true);
            blockSize = DEFAULT_BLOCK_SIZE;
            this.fileName = fileName;
            this.signature = signature;
            this.size = size;
        }

        ContentMetadataItemFuseFile(FuseDirectory parent, long uid, long gid, ContentMetadataItem contentMetadataItem) {
            this(parent, uid, gid, contentMetadataItem.toStringFile(), contentMetadataItem.getSignature(), contentMetadataItem.getSize());
        }

        @Override
        public InputStream getInputStream() throws IOException {
            var contents = contentRepositoryService.findBySignature(signature);
            if (contents.isEmpty()) {
                return null;
            }

            var content = contents.get(0);
            var inputPath = content.getPathFileAbsolute();
            if (!Files.exists(inputPath)) {
                return null;
            }

            if (!content.isCompressedEntry()) {
                var path = content.getPathFileAbsolute();
                return Files.newInputStream(path);
            }
            var entryNameEquals = content.getFileEntry();
            return archiveService.extractToLazyArchiveEntryProducerByName(inputPath, entryNameEquals, content.getFileName(), 0).getInputStream();
        }

        @Override
        public String getName() {
            return fileName;
        }

        @Override
        public long getSize() {
            return size;
        }

        @Override
        public int read(Pointer buffer, long size, long offset) {
            log.trace("Read size={], offset={}", size, offset);
            var fileContentSize = getSize();
            if (fileContentSize == 0) {
                return 0;
            }

            var contents = contentRepositoryService.findBySignature(signature);
            if (contents.isEmpty()) {
                return 0;
            }

            var content = contents.get(0);
            var inputPath = content.getPathFileAbsolute();
            if (!Files.exists(inputPath)) {
                return 0;
            }

            var totalBytesToRead = (int) Math.min(fileContentSize - offset, size);

            if (!content.isCompressedEntry()) {
                var path = content.getPathFileAbsolute();
                try (var sbc = Files.newByteChannel(path)) {
                    sbc.position(offset);
                    var bf = ByteBuffer.allocate(totalBytesToRead);
                    sbc.read(bf);
                    buffer.put(0, bf.array(), 0, totalBytesToRead);
                    return totalBytesToRead;
                } catch (IOException e) {
                    log.error("Error reading file ", e);
                    return 0;
                }
            }

            if (blocks == null) {
                try {
                    blocks = archiveService.extractEntry(inputPath, content.getFileEntry(), blockSize);
                } catch (IOException e) {
                    log.error("Could not get block", e);
                    return -1;
                }
            }

            var bufferIndex = 0;

            var blockIndex = (int) Math.min(fileContentSize, offset / blockSize);
            var blockOffset = (int) (offset % blockSize);

            var bytesToRead = totalBytesToRead;
            do {
                var block = blocks.get(blockIndex);
                var blockBytesToRead = Math.min(bytesToRead, blockSize - blockOffset);
                buffer.put(bufferIndex, block, blockOffset, blockBytesToRead);

                bufferIndex += blockBytesToRead;
                bytesToRead -= blockBytesToRead;

                blockOffset = 0;
                blockIndex++;
            } while (bytesToRead > 0);

            return totalBytesToRead;
        }

        @Override
        public int write(Pointer buffer, long bufSize, long writeOffset) {
            return 0;
        }
    }

    private class ContentMetadataSetsPrefixFuseDirectory extends FuseDirectory {
        private final String contentMetadataSetPrefix;

        private final boolean contentMetadataSetNamesIncludeId;

        ContentMetadataSetsPrefixFuseDirectory(FuseDirectory parent, long uid, long gid, String contentMetadataSetPrefix, boolean contentMetadataSetNamesIncludeId) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureWritable());
            this.contentMetadataSetPrefix = contentMetadataSetPrefix;
            this.contentMetadataSetNamesIncludeId = contentMetadataSetNamesIncludeId;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            if (contentMetadataSetNamesIncludeId) {
                var stringFileUnique = tokenStack.pop();
                var id = ContentMetadataSet.fromStringFileUnique(stringFileUnique);
                return contentMetadataSetRepositoryService.findById(id)
                    .map(cms -> new ContentMetadatasPrefixesFuseDirectory(this, getUid(), getGid(), cms.toStringFileUnique(), cms.getId(), cms.isStaging(), cms.isFlattenable()))
                    .map(fd -> fd.find(tokenStack))
                    .orElse(null);
            }

            var repositoryPathStringFile = tokenStack.pop();
            var contentMetadataSetRepositoryPath = contentMetadataSetRepositoryService.findContentMetadataSetRepositoryPathsByStringFile().get(repositoryPathStringFile);
            return new ContentMetadatasPrefixesFuseDirectory(this, getUid(), getGid(), contentMetadataSetRepositoryPath.getRepositoryPath(), contentMetadataSetRepositoryPath.getContentMetadataSetId(), contentMetadataSetRepositoryPath.isStaging(), contentMetadataSetRepositoryPath.isFlattenable())
                .find(tokenStack);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            if (contentMetadataSetNamesIncludeId) {
                return contentMetadataSetRepositoryService.findByRepositoryPathStartsWithIgnoreCase(contentMetadataSetPrefix).stream()
                    .map(ContentMetadataSet::toStringFileUnique)
                    .sorted();
            }

            return contentMetadataSetRepositoryService.findContentMetadataSetRepositoryPathsByStringFile()
                .keySet()
                .stream()
                .filter(s -> StringUtils.startsWithIgnoreCase(s, contentMetadataSetPrefix))
                .sorted();
        }

        @Override
        public String getName() {
            return contentMetadataSetPrefix;
        }
    }

    private class ContentMetadataSetsFuseDirectory extends FuseDirectory {
        private final String directoryName;

        ContentMetadataSetsFuseDirectory(FuseDirectory parent, long uid, long gid, String directoryName) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureByWritable());
            this.directoryName = directoryName;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            var repositoryPathStringFile = tokenStack.pop();
            var contentMetadataSetRepositoryPath = contentMetadataSetRepositoryService.findContentMetadataSetRepositoryPathsByStringFile().get(repositoryPathStringFile);

            return new ContentMetadataSetFuseDirectory(this, getUid(), getGid(), repositoryPathStringFile, contentMetadataSetRepositoryPath.getContentMetadataSetId(), contentMetadataSetRepositoryPath.isFlattenable(), contentMetadataSetRepositoryPath.isStaging())
                .find(tokenStack);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            return contentMetadataSetRepositoryService.findContentMetadataSetRepositoryPathsByStringFile().keySet().stream().sorted();
        }

        @Override
        public String getName() {
            return directoryName;
        }
    }

    private class ContentMetadatasPrefixesFuseDirectory extends FuseDirectory {
        private final String directoryName;
        private final UUID contentMetadataSetId;
        private final boolean staging;
        private final boolean flattenable;

        ContentMetadatasPrefixesFuseDirectory(FuseDirectory parent, long uid, long gid, String directoryName, UUID contentMetadataSetId, boolean staging, boolean flattenable) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureWritable());
            this.directoryName = directoryName;
            this.contentMetadataSetId = contentMetadataSetId;
            this.staging = staging;
            this.flattenable = flattenable;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            var contentMetadataPrefix = tokenStack.pop();
            return new ContentMetadatasPrefixByContentMetadataSetFuseDirectory(this, getUid(), getGid(), contentMetadataSetId, contentMetadataPrefix, staging, flattenable, fuseConfiguration.isContentMetadataPrefixDirectoryContentIncludeIds()).find(tokenStack);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            return contentMetadataSearchService.searchContentMetadataPrefixByContentMetadataSetIdAndCompleteNotEmpty(contentMetadataSetId).stream();
        }

        @Override
        public String getName() {
            return directoryName;
        }
    }

    private class ContentMetadataSetFuseDirectory extends FuseDirectory {
        private final String directoryName;
        private final UUID contentMetadataSetId;
        private final boolean flattenable;
        private final boolean staging;

        ContentMetadataSetFuseDirectory(FuseDirectory parent, long uid, long gid, String directoryName, UUID contentMetadataSetId, boolean flattenable, boolean staging) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureMetadatasWritable());
            this.directoryName = directoryName;
            this.contentMetadataSetId = contentMetadataSetId;
            this.flattenable = flattenable;
            this.staging = staging;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }
            var name = tokenStack.pop();
            if (flattenable) {
                return Optional.ofNullable(contentMetadataItemRepositoryService.findContentMetadataItemByStringFileByContentMetadataSetIdAndCompleteTotal(contentMetadataSetId).get(name))
                    .map(cmi -> new ContentMetadataItemFuseFile(this, getUid(), getGid(), cmi))
                    .orElse(null);
            }
            return contentMetadataRepositoryService.findByContentMetadataSetIdAndName(contentMetadataSetId, name)
                .map(cm -> new ContentMetadataFuseDirectory(this, getUid(), getGid(), cm.toStringFile(), cm.getId(), staging))
                .map(fd -> fd.find(tokenStack))
                .orElse(null);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            if (flattenable) { // Flattenable only contains 1 content item per metadata so complete total means that all content is present
                return contentMetadataItemRepositoryService.findContentMetadataItemByStringFileByContentMetadataSetIdAndCompleteTotal(contentMetadataSetId).keySet().stream().sorted();
            }
            return contentMetadataRepositoryService.findByContentMetadataSet_IdEquals(contentMetadataSetId).stream().map(ContentMetadata::getName);
        }

        @Override
        public String getName() {
            return directoryName;
        }
    }

    /**
     * Contains unique contentMetadatas By contentMetadataSet and Prefix
     */
    private class ContentMetadatasPrefixByContentMetadataSetFuseDirectory extends FuseDirectory {
        private final UUID contentMetadataSetId;
        private final String contentMetadataPrefix;

        private final boolean flattenable;
        private final boolean staging;
        private final boolean contentMetadataNamesIncludeId;

        ContentMetadatasPrefixByContentMetadataSetFuseDirectory(FuseDirectory parent, long uid, long gid, UUID contentMetadataSetId, String contentMetadataPrefix, boolean staging, boolean flattenable, boolean contentMetadataNamesIncludeId) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureMetadatasWritable());
            this.contentMetadataSetId = contentMetadataSetId;
            this.contentMetadataPrefix = contentMetadataPrefix;
            this.staging = staging;
            this.contentMetadataNamesIncludeId = contentMetadataNamesIncludeId;
            this.flattenable = flattenable;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            if (contentMetadataNamesIncludeId) {
                var contentMetadataStringFileUnique = tokenStack.pop();
                var id = ContentMetadata.fromStringFile(contentMetadataStringFileUnique);
                var contentMetadata = contentMetadataRepositoryService.findById(id);
                return contentMetadata.map(cm -> new ContentMetadataFuseDirectory(this, getUid(), getGid(), cm.toStringFileUnique(), cm.getId(), staging)).map(fd -> fd.find(tokenStack)).orElse(null);
            }

            var name = tokenStack.pop();
            if (flattenable) {
                return Optional.ofNullable(contentMetadataItemRepositoryService.findContentMetadataItemByStringFileByContentMetadataSetIdAndCompleteTotal(contentMetadataSetId).get(name))
                    .map(cmi -> new ContentMetadataItemFuseFile(this, getUid(), getGid(), cmi))
                    .orElse(null);
            }
            return contentMetadataRepositoryService.findByContentMetadataSetIdAndName(contentMetadataSetId, name)
                .map(cm -> new ContentMetadataFuseDirectory(this, getUid(), getGid(), cm.toStringFile(), cm.getId(), staging))
                .map(fd -> fd.find(tokenStack))
                .orElse(null);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            if (contentMetadataNamesIncludeId) {
                return contentMetadataSearchService
                    .searchIdsByMetadataSetIdAndPrefixAndCompleteNotEmpty(contentMetadataSetId, contentMetadataPrefix)
                    .map(i -> contentMetadataRepositoryService.findById(i).map(ContentMetadata::toStringFileUnique).orElseGet(() -> "ERROR_" + i));
            }

            if (flattenable) { // Flattenable only contains 1 content item per metadata so complete total means that all content is present
                return contentMetadataItemRepositoryService.findContentMetadataItemByStringFileByContentMetadataSetIdAndCompleteTotal(contentMetadataSetId)
                    .keySet()
                    .stream()
                    .filter(s -> StringUtils.startsWithIgnoreCase(s, contentMetadataPrefix))
                    .sorted();
            }

            return contentMetadataRepositoryService.findByContentMetadataSet_IdEquals(contentMetadataSetId).stream()
                .map(ContentMetadata::getName)
                .filter(s -> StringUtils.startsWithIgnoreCase(s, contentMetadataPrefix))
                .sorted();
        }

        @Override
        public String getName() {
            return contentMetadataPrefix;
        }
    }


    /**
     * Contains contentMetadatas by contentSystem and Prefix
     */
    private class ContentMetadatasPrefixByContentSystemFuseDirectory extends FuseDirectory {
        private final String contentSystem;
        private final String contentMetadataPrefix;

        ContentMetadatasPrefixByContentSystemFuseDirectory(FuseDirectory parent, long uid, long gid, String contentSystem, String prefix) {
            super(parent, uid, gid, fuseConfiguration.isDirectoryStructureMetadatasWritable());
            this.contentSystem = contentSystem;
            this.contentMetadataPrefix = prefix;
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }
            var contentMetadataStringFileUnique = tokenStack.pop();
            var id = ContentMetadata.fromStringFile(contentMetadataStringFileUnique);
            return contentMetadataRepositoryService.findById(id)
                .map(cm -> new ContentMetadataFuseDirectory(this, getUid(), getGid(), cm.toStringFileUnique(), cm.getId(), cm.getContentMetadataSet().isStaging()))
                .map(fd -> fd.find(tokenStack))
                .orElse(null);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            return contentMetadataSearchService.searchIdsBySystemAndPrefixAndCompleteNotEmpty(contentSystem, contentMetadataPrefix).map(i -> contentMetadataRepositoryService.findById(i).map(ContentMetadata::toStringFileUnique).orElseGet(() -> "ERROR_" + i));
        }

        @Override
        public String getName() {
            return contentMetadataPrefix;
        }
    }

    private class FuseRootDirectory extends FuseDirectory {

        private final Map<String, FuseEntry> entries = new HashMap<>();

        FuseRootDirectory(long uid, long gid) {
            super(null, uid, gid, fuseConfiguration.isDirectoryStructureRootWritable());
        }

        public void add(FuseEntry entry) {
            entries.put(entry.getName(), entry);
        }

        @Override
        public FuseEntry find(Deque<String> tokenStack) {
            if (tokenStack.isEmpty()) {
                return this;
            }

            var token = tokenStack.pop();
            return Optional.ofNullable(entries.get(token))
                .map(e -> e.find(tokenStack))
                .orElse(null);
        }

        @Override
        public Stream<String> getDirectoryEntryNames() {
            return entries.keySet().stream();
        }

        @Override
        public String getName() {
            return "";
        }

        @Override
        public void rmFile(FuseEntry entry) {
            // Does nothing
        }
    }
}
