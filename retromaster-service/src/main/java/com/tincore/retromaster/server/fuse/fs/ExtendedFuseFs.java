package com.tincore.retromaster.server.fuse.fs;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import ru.serce.jnrfuse.FuseFS;

import java.nio.file.Path;
import java.nio.file.Paths;

public interface ExtendedFuseFs extends FuseFS {
    String DIRECTORY_ENTRY_SELF = ".";
    String DIRECTORY_ENTRY_UP = "..";

    int PERMISSIONS_R = 0444; // NOPMD Real octal
    int PERMISSIONS_RW = 0666; // NOPMD Real octal
    int PERMISSIONS_RX = 0555; // NOPMD Real octal
    int PERMISSIONS_RWX = 0777; // NOPMD Real octal

    default FuseEntry getFuseEntry(String path) {
        return getFuseEntry(Paths.get(path));
    }

    FuseEntry getFuseEntry(Path path);

    default FuseEntry getFuseEntryParent(String path) {
        return getFuseEntry(Paths.get(path).getParent());
    }
}
