package com.tincore.retromaster.server.thrift;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.querydsl.core.types.Predicate;
import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.domain.event.*;
import com.tincore.retromaster.domain.search.QueryFacetFilter;
import com.tincore.retromaster.domain.search.QueryResult;
import com.tincore.retromaster.domain.search.QueryResultFacet;
import com.tincore.retromaster.service.execute.ContentExecutor;
import com.tincore.retromaster.thrift.form.*;
import com.tincore.util.form.FormMapperTrait;
import com.tincore.util.querydsl.OptionalBooleanBuilder;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentMetadata.*;
import static com.tincore.retromaster.domain.QContent.content;
import static com.tincore.retromaster.domain.QContentMetadata.contentMetadata;
import static com.tincore.retromaster.domain.QContentMetadataSet.contentMetadataSet;

// Removed builder to avoid javadoc generation issue
@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface TFormMapper extends FormMapperTrait {
    default Predicate toContentMetadataPredicate(TContentMetadataFilter filter) {
        var booleanExpression = contentMetadata.isNotNull();
        if (StringUtils.isNotBlank(filter.getQuery())) {
            booleanExpression = booleanExpression.and(
                contentMetadata.title
                    .toUpperCase()
                    .like(filter.getQuery().toUpperCase())
                    .or(contentMetadata.publisher.toUpperCase().like(filter.getQuery().toUpperCase()).or(contentMetadata.date.toUpperCase().like(filter.getQuery().toUpperCase()))));
        }
        return new OptionalBooleanBuilder(booleanExpression)
            .andWhenNotNull(contentMetadata.contentMetadataSet.id::eq, Optional.ofNullable(filter.getContentMetadataSetIdEquals()).map(this::toUuid).orElse(null))
            .andWhenNotEmpty(contentMetadata.name::equalsIgnoreCase, filter.getNameEquals())
            .andWhenNotEmpty(contentMetadata.title::equalsIgnoreCase, filter.getTitleEquals())
            .andWhenNotEmpty(contentMetadata.publisher::equalsIgnoreCase, filter.getPublisherEquals())
            .build();
    }

    default Predicate toContentMetadataSetPredicate(TContentMetadataSetFilter filter) {
        var booleanExpression = contentMetadataSet.isNotNull();
        if (StringUtils.isNotBlank(filter.getQuery())) {
            booleanExpression = booleanExpression.and(contentMetadataSet.name.likeIgnoreCase(filter.getQuery()));
        }
        return new OptionalBooleanBuilder(booleanExpression)
            .andWhenNotEmpty(contentMetadataSet.name::eq, filter.getNameEquals())
            .andWhenNotEmpty(contentMetadataSet.name::startsWithIgnoreCase, filter.getNameStartsWith())
            .andWhenNotEmpty(contentMetadataSet.category::eq, filter.getCategoryEquals())
            .build();
    }

    default Predicate toContentPredicate(TContentFilter filter) {
        var booleanExpression = content.isNotNull();
        if (StringUtils.isNotBlank(filter.getQuery())) {
            booleanExpression = booleanExpression.and(content.fileName.toUpperCase().like(filter.getQuery().toUpperCase()).or(content.fileEntry.toUpperCase().like(filter.getQuery().toUpperCase())));
        }
        return new OptionalBooleanBuilder(booleanExpression)
            .andWhenNotNull(content.volume.id::eq, toUuid(filter.getVolumeIdEquals()))
            .andWhenNotEmpty(content.directoryPath::eq, filter.getDirectoryPathEquals())
            .andWhenNotEmpty(content.fileName::eq, filter.getFileNameEquals())
            .andWhenNotEmpty(content.fileEntry::eq, filter.getFileEntryEquals())
            .andWhenNotEmptyOrZero(content.fileSize::eq, filter.getFileSizeEquals())
            .andWhenNotEmpty(content.crc::eq, filter.getCrcEquals())
            .andWhenNotEmpty(content.sha1::eq, filter.getSha1Equals())
            .andWhenNotEmpty(content.md5::eq, filter.getMd5Equals())
            .andWhenNotEmptyOrZero(content.size::eq, filter.getSizeEquals())
            .build();
    }

    List<TContentSystem> toContentSystems(Collection<ContentSystem> contentSystems);

    List<LocalDate> toLocalDates(List<String> dates);

    Map<String, String> toMapString(Map<UUID, UUID> map);

    default Pageable toPageable(TPageable pageable) {
        return PageRequest.of((int) pageable.page, (int) pageable.size, toSort(pageable));
    }

    List<QueryFacetFilter> toQueryFacetFilter(List<TQueryFacetFilter> facetFilters);

    default Sort toSort(TPageable pageable) {
        return Sort.by(Optional.ofNullable(pageable.sort).map(Collection::stream).orElse(Stream.empty()).map(this::toSortOrder).collect(Collectors.toList()));
    }

    Sort.Direction toSortDirection(TDirection direction);

    default Sort.Order toSortOrder(TOrder order) {
        return new Sort.Order(toSortDirection(Optional.of(order.direction).orElse(TDirection.ASC)), order.property);
    }

    @Mapping(source = "content.volume.id", target = "volumeId")
    TContent toTContent(Content content);

    default List<TContentDirectoryPath> toTContentDirectoryPaths(Collection<String> directoryPaths) {
        return directoryPaths.stream().map(d -> new TContentDirectoryPath().setId(d)).collect(Collectors.toList());
    }

    default TContentDirectoryPaths toTContentDirectoryPaths(Page<String> page) {
        return new TContentDirectoryPaths().setContent(toTContentDirectoryPaths(page.getContent())).setNumber(page.getNumber()).setSize(page.getSize()).setTotalElements(page.getTotalElements()).setTotalPages(page.getTotalPages());
    }

    List<TContentExecuteProcess> toTContentExecuteProcesses(Collection<ContentExecuteProcess> contentExecuteProcesses);

    TContentExecutionEvent toTContentExecutionEvent(ContentExecutionEvent event);

    default TContentExecutor toTContentExecutor(ContentExecutor contentExecutor) {
        return new TContentExecutor()
            .setId(contentExecutor.getId())
            .setName(contentExecutor.getName())
            .setFound(contentExecutor.isFound())
            .setEnabled(contentExecutor.isEnabled())
            .setSystems(toContentSystems(contentExecutor.getContentSystems()));
    }

    List<TContentExecutor> toTContentExecutors(List<ContentExecutor> collect);

    TContentImportEvent toTContentImportEvent(ContentImportEvent event);

    TContentIndexEvent toTContentIndexEvent(ContentIndexEvent event);

    TContentMediaType toTContentMediaType(ContentMediaType mediaType);

    @Mapping(source = "contentMetadata.contentMetadataSet.id", target = "contentMetadataSetId")
    @Mapping(source = "contentMetadata.contentMetadataSet.staging", target = "staging")
    TContentMetadata toTContentMetadata(ContentMetadata contentMetadata);

    default TContentMetadataComplete toTContentMetadataComplete(String complete) {
        if (complete == null) {
            return TContentMetadataComplete.UNKNOWN;
        }
        return switch (complete) {
            case COMPLETE_PARTIAL -> TContentMetadataComplete.PARTIAL;
            case COMPLETE_TOTAL -> TContentMetadataComplete.COMPLETE;
            case COMPLETE_EMPTY -> TContentMetadataComplete.EMPTY;
            case COMPLETE_NOITEMS -> TContentMetadataComplete.NOITEMS;
            default -> throw new IllegalArgumentException("Unmapped case " + complete);
        };
    }

    @Mapping(source = "contentMetadataItem.contentMetadata.id", target = "contentMetadataId")
    TContentMetadataItem toTContentMetadataItem(ContentMetadataItem contentMetadataItem);

    @Mapping(source = "contentMetadataItemExecuteOptionSet.contentMetadataItem.id", target = "contentMetadataItemId")
    @Mapping(source = "contentMetadataItemExecuteOptionSet.contentMetadataItem.name", target = "contentMetadataItemName")
    TContentMetadataItemExecuteOptionSet toTContentMetadataItemExecuteOptionsSet(ContentMetadataItemExecuteOptionSet contentMetadataItemExecuteOptionSet);

    default TContentMetadataItemExecuteOptionSet toTContentMetadataItemExecuteOptionsSet(ContentMetadataItem contentMetadataItem, List<ContentExecutor> contentExecutors) {
        return toTContentMetadataItemExecuteOptionsSet(new ContentMetadataItemExecuteOptionSet(contentMetadataItem, contentExecutors));
    }

    List<TContentMetadataItemExecuteOptionSet> toTContentMetadataItemExecuteOptionsSets(Collection<ContentMetadataItemExecuteOptionSet> contentMetadataItemExecuteOptionSets);

    List<TContentMetadataItemSummary> toTContentMetadataItemSummaries(List<ContentMetadataItem> contentMetadataItems);

    @Mapping(source = "contentMetadataItem.contentMetadata.id", target = "contentMetadataId")
    @Mapping(source = "contentMetadataItem.contentMetadata.title", target = "title")
    @Mapping(source = "contentMetadataItem.contentMetadata.date", target = "date")
    @Mapping(source = "contentMetadataItem.contentMetadata.publisher", target = "publisher")
    @Mapping(source = "contentMetadataItem.contentMetadata.mediaType", target = "mediaType")
    @Mapping(source = "contentMetadataItem.contentMetadata.verified", target = "verified")
    @Mapping(source = "contentMetadataItem.contentMetadata.region", target = "region")
    @Mapping(source = "contentMetadataItem.contentMetadata.language", target = "language")
    @Mapping(source = "contentMetadataItem.contentMetadata.system", target = "system")
    @Mapping(source = "contentMetadataItem.contentMetadata.source", target = "source")
    TContentMetadataItemSummary toTContentMetadataItemSummary(ContentMetadataItem contentMetadataItem);

    List<TContentMetadataItem> toTContentMetadataItems(List<ContentMetadataItem> contentMetadataItems);

    default TContentMetadataMedia toTContentMetadataMedia(Content content, ContentMediaType mediaType) {
        return new TContentMetadataMedia()
            .setMediaType(toTContentMediaType(mediaType))
            .setContentSignature(content.getSignature());
    }

    TContentMetadataSet toTContentMetadataSet(ContentMetadataSet contentMetadataSet);

    TContentMetadataSetManageEvent toTContentMetadataSetManageEvent(ContentMetadataSetManageEvent event);

    List<TContentMetadataSet> toTContentMetadataSets(Collection<ContentMetadataSet> contentMetadataSets);

    default TContentMetadataSets toTContentMetadataSets(Page<ContentMetadataSet> page) {
        return new TContentMetadataSets().setContent(toTContentMetadataSets(page.getContent())).setNumber(page.getNumber()).setSize(page.getSize()).setTotalElements(page.getTotalElements()).setTotalPages(page.getTotalPages());
    }

    List<TContentMetadataSummary> toTContentMetadataSummaries(Collection<ContentMetadata> contentMetadatas);

    default TContentMetadataSummaries toTContentMetadataSummaries(Page<ContentMetadata> page) {
        return new TContentMetadataSummaries().setContent(toTContentMetadataSummaries(page.getContent())).setNumber(page.getNumber()).setSize(page.getSize()).setTotalElements(page.getTotalElements()).setTotalPages(page.getTotalPages());
    }

    @Mapping(source = "contentMetadata.contentMetadataSet.id", target = "contentMetadataSetId")
    @Mapping(source = "contentMetadata.contentMetadataSet.staging", target = "staging")
    TContentMetadataSummary toTContentMetadataSummary(ContentMetadata contentMetadata);

    List<TContentMetadata> toTContentMetadatas(Collection<ContentMetadata> contentMetadatas);

    List<TContentMetadata> toTContentMetadatas(Stream<ContentMetadata> contentMetadatas);

    default TContentMetadatas toTContentMetadatas(Page<ContentMetadata> page) {
        return new TContentMetadatas().setContent(toTContentMetadatas(page.getContent())).setNumber(page.getNumber()).setSize(page.getSize()).setTotalElements(page.getTotalElements()).setTotalPages(page.getTotalPages());
    }

    TContentScanEvent toTContentScanEvent(ContentScanEvent event);

    default TContentSystem toTContentSystem(ContentSystem contentSystem) {
        return new TContentSystem().setId(contentSystem.name()).setName(contentSystem.name());
    }

    List<TContentSystem> toTContentSystems(Collection<ContentSystem> contentSystems);

    TContentVolume toTContentVolume(ContentVolume contentVolume);

    default TContentVolumeDirectory toTContentVolumeDirectory(ContentVolume contentVolume, Path path, boolean exists, LocalDateTime modifiedTime) {
        return new TContentVolumeDirectory()
            .setContentVolumeId(contentVolume.getId().toString())
            .setModifiedTime(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(modifiedTime))
            .setPath(path.toString())
            .setExists(exists);
    }

    default TContentVolumeDirectory toTContentVolumeDirectory(ContentVolume contentVolume, Path path, boolean exists, LocalDateTime modifiedTime, ContentMetadataSet contentMetadataSet, boolean fresh) {
        return new TContentVolumeDirectory()
            .setContentVolumeId(contentVolume.getId().toString())
            .setModifiedTime(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(modifiedTime))
            .setPath(path.toString())
            .setFresh(fresh)
            .setExists(exists)
            .setContentMetadataSet(toTContentMetadataSet(contentMetadataSet));
    }

    List<TContentVolume> toTContentVolumes(Collection<ContentVolume> contentVolumes);

    List<TContent> toTContents(Collection<Content> contents);

    default TContents toTContents(Page<Content> page) {
        return new TContents().setContent(toTContents(page.getContent())).setNumber(page.getNumber()).setSize(page.getSize()).setTotalElements(page.getTotalElements()).setTotalPages(page.getTotalPages());
    }

    List<TContent> toTContents(Stream<Content> contents);

    List<TContent> toTContents(List<Content> contents);

    List<TQueryFacet> toTQueryFacets(Collection<QueryResultFacet> facets);

    default TQueryResult toTQueryResult(ContentMetadata contentMetadata) {
        return new TQueryResult().setId(contentMetadata.getId().toString()).setContentMetadataSummary(toTContentMetadataSummary(contentMetadata));
    }

    default TQueryResults toTQueryResults(QueryResult<ContentMetadata> contentMetadataQueryResult, List<TQueryResult> queryResults) {
        return new TQueryResults()
            .setNumber(contentMetadataQueryResult.getNumber())
            .setTotalPages(contentMetadataQueryResult.getTotalPages())
            .setTotalElements(contentMetadataQueryResult.getTotalElements())
            .setFacets(toTQueryFacets(contentMetadataQueryResult.getFacets()))
            .setSize(queryResults.size())
            .setContent(queryResults);
    }

    default ZonedDateTime toZonedDateTime(String source) {
        return Optional.ofNullable(source).map(ZonedDateTime::parse).orElse(null);
    }

    @Mapping(ignore = true, target = "filename")
    @Mapping(ignore = true, target = "repositoryPath")
    @Mapping(ignore = true, target = "staging")
    void update(TContentMetadataSet tContentMetadataSet, @MappingTarget ContentMetadataSet contentMetadataSet);

    @Mapping(ignore = true, target = "items")
    void update(TContentMetadata tContentMetadata, @MappingTarget ContentMetadata contentMetadata);

    void update(TContentVolume tContentVolume, @MappingTarget ContentVolume stagingContentVolume);
}
