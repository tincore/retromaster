package com.tincore.retromaster.server.websocket.event;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.event.*;
import com.tincore.retromaster.server.thrift.TFormMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.thrift.TBase;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Base64;

import static com.tincore.retromaster.config.WebSocketConfiguration.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class RetromasterWebsocketEventHandler {

    private final TFormMapper formMapper;
    private final SimpMessagingTemplate template;
    private final TSerializer tSerializer;
    private final TDeserializer tDeserializer;

    private void convertAndSend(String destination, TBase base) {
        try {
            var bytes = tSerializer.serialize(base);
            var payload = Base64.getEncoder().encodeToString(bytes);
            template.convertAndSend(destination, payload);
        } catch (TException e) {
            log.error("Serialization error " + base, e);
        }
    }

    @Async
    @EventListener
    public void onContentExecutionEvent(ContentExecutionEvent event) {
        convertAndSend(MESSAGE_TOPIC_CONTENT_EXECUTION, formMapper.toTContentExecutionEvent(event));
    }

    @Async
    @EventListener
    public void onContentImportEvent(ContentImportEvent event) {
        convertAndSend(MESSAGE_TOPIC_CONTENT_IMPORT, formMapper.toTContentImportEvent(event));
    }

    @Async
    @EventListener
    public void onContentIndexEvent(ContentIndexEvent event) {
        convertAndSend(MESSAGE_TOPIC_CONTENT_INDEX, formMapper.toTContentIndexEvent(event));
    }

    @Async
    @EventListener
    public void onContentMetadataSetManageEvent(ContentMetadataSetManageEvent event) {
        convertAndSend(MESSAGE_TOPIC_CONTENT_METADATA_SET_MANAGE, formMapper.toTContentMetadataSetManageEvent(event));
    }

    @Async
    @EventListener
    public void onContentScanEvent(ContentScanEvent event) {
        convertAndSend(MESSAGE_TOPIC_CONTENT_SCAN, formMapper.toTContentScanEvent(event));
    }

    // For testing only!!
    @PostConstruct
    public void postConstruct() {
        // Timer timer = new Timer();
        // timer.scheduleAtFixedRate(new TimerTask() {
        // @Override
        // public void run() {
        // //POC
        // ContentImportEvent event = new ContentImportEvent(ContentImportEventType.START, 0);
        // convertAndSend(MESSAGE_TOPIC_CONTENT_IMPORT, formMapper.toTContentImportEvent(event));
        //
        // }
        // }, 1000, 1000);
    }
}
