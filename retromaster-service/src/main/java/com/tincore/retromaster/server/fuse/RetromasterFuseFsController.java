package com.tincore.retromaster.server.fuse;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.FuseConfiguration;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.server.fuse.content.ContentFuseFs;
import com.tincore.retromaster.server.fuse.overlay.OverlayFuseFs;
import com.tincore.retromaster.service.WrappedException;
import com.tincore.retromaster.service.fs.UnionFsService;
import jnr.ffi.Platform;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.tincore.retromaster.server.fuse.content.ContentFuseFsEntryFactory.NAME_BY_SYSTEM;

@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(value = "tincore.retromaster.fuse.enabled", havingValue = "true")
public class RetromasterFuseFsController {
    private final FuseConfiguration fuseConfiguration;
    private final ContentFuseFs contentFuseFs;
    private final UnionFsService unionFsService;

    private Path fuseFsRootPath;
    private Path unionFsMountPath;

    private OverlayFuseFs overlayFuseFs;

    private Path createFuseFsRootPath() throws IOException {
        if (Platform.getNativePlatform().getOS() == Platform.OS.WINDOWS) {
            return fuseConfiguration.getWindowsPath();
        }

        var path = fuseConfiguration.getPosixPath();
        Files.createDirectories(path);
        return path;
    }

    public Path getPath() {
        return fuseFsRootPath;
    }

    public Path getPath(ContentMetadata contentMetadata, boolean readOnly) {
        return (readOnly ? getPath() : getPathReadWrite()).resolve(NAME_BY_SYSTEM).resolve(contentMetadata.getSystem()).resolve(contentMetadata.getPrefix()).resolve(contentMetadata.toStringFileUnique());
    }

    public Path getPath(ContentMetadataItem contentMetadataItem, boolean readOnly) {
        return getPath(contentMetadataItem.getContentMetadata(), readOnly).resolve(contentMetadataItem.getName());
    }

    public Path getPathReadWrite() {
        return switch (fuseConfiguration.getReadWriteMode()) {
            case INTERNAL -> fuseFsRootPath;
            case UNIONFS -> unionFsMountPath;
            default -> null;
        };
    }

    public boolean isActive() {
        return contentFuseFs != null;
    }

    public boolean isReadOnly() {
        return FuseConfiguration.ReadWriteMode.NONE.equals(fuseConfiguration.getReadWriteMode());
    }

    public boolean isReadWriteCapable() {
        return !isReadOnly();
    }

    @PostConstruct
    public void postConstruct() throws IOException, InterruptedException {
        fuseFsRootPath = createFuseFsRootPath();
        var readWriteMode = fuseConfiguration.getReadWriteMode();
        switch (readWriteMode) {
            case NONE -> contentFuseFs.mount(fuseFsRootPath, false, fuseConfiguration.isDebug());
            case INTERNAL -> {
                Files.createDirectories(fuseConfiguration.getReadWriteInternalDataPath());
                overlayFuseFs = new OverlayFuseFs(contentFuseFs, fuseConfiguration.getReadWriteInternalDataPath());
                overlayFuseFs.mount(fuseFsRootPath, false, fuseConfiguration.isDebug());
            }
            case UNIONFS -> {
                contentFuseFs.mount(fuseFsRootPath, false, fuseConfiguration.isDebug());
                unionFsMountPath = fuseConfiguration.getReadWriteUnionFsMountPath();
                unionFsService.mount(unionFsMountPath, fuseFsRootPath, fuseConfiguration.getReadWriteUnionFsDataPath());
            }
            default -> throw new WrappedException("Unsupported ReadWriteMode mode=" + readWriteMode);
        }
    }

    @PreDestroy
    public void preDestroy() {
        if (overlayFuseFs != null) {
            overlayFuseFs.umount();
        }
        if (contentFuseFs != null) {
            contentFuseFs.umount();
        }
        try {
            if (unionFsMountPath != null) {
                unionFsService.umount(unionFsMountPath);
                unionFsMountPath = null;
            }
        } catch (IOException | InterruptedException e) {
            log.error("Umounting error", e);
        }
    }
}
