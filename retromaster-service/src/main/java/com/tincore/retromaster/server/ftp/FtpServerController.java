package com.tincore.retromaster.server.ftp;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.service.*;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryProducer;
import com.tincore.retromaster.service.archive.producer.PathArchiveEntryProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FileSystemView;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.FtpFile;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

import javax.annotation.PostConstruct;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class FtpServerController {

    private static final int BATCH_SIZE = 50;
    private static final String PATH_BY_METADATA_SET = "/byMetadataSet";
    private static final String PATH_BY_SYSTEM = "/bySystem";

    private final ArchiveService archiveService;
    private final FtpServerFactory ftpServerFactory;
    private final ContentRepositoryService contentRepositoryService;
    private final ContentMetadataSearchService contentMetadataSearchService;
    private final ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;

    private final AntPathMatcher pathMatcher = new AntPathMatcher();
    private final GenericFtpFileAdapter byMetadataSetDir = createByMetadataSetDir();
    private final GenericFtpFileAdapter bySystemDir = createBySystemsDir(PATH_BY_SYSTEM);
    private final GenericFtpFileAdapter homeDir = createHomeDir();

    private GenericFtpFileAdapter createByMetadataSetDir() {
        return new GenericFtpFileAdapter(PATH_BY_METADATA_SET, true) {
            @Override
            public List<? extends FtpFile> listFiles() {
                return contentMetadataSetRepositoryService.findAll().stream().map(cms -> createContentMetadataSetFtpFile(PATH_BY_METADATA_SET + "/" + cms.toStringFileUnique())).collect(Collectors.toList());
            }
        };
    }

    private GenericFtpFileAdapter createBySystemsDir(final String bySystemPath) {
        return new GenericFtpFileAdapter(bySystemPath, true) {
            @Override
            public List<? extends FtpFile> listFiles() {
                var systems = contentMetadataSearchService.searchContentMetadataSystemsByContentNotEmpty();
                return systems.stream().map(s -> createContentMetadataPrefixesDirBySystem(bySystemPath, s)).collect(Collectors.toList());
            }
        };
    }

    private GenericFtpFileAdapter createContentMetadataItemFile(String parentPath, String contentMetadataName, String contentMetadataItemName) {
        var id = ContentMetadata.fromStringFile(contentMetadataName);
        var item = contentMetadataItemRepositoryService.findFirstByContentMetadataIdAndName(id, contentMetadataItemName);
        return item.filter(i -> contentRepositoryService.findBySignature(i.getSignature()).stream().anyMatch(Content::isFileExists))
            .map(
                i -> new GenericFtpFileAdapter(parentPath + "/" + contentMetadataName + "/" + contentMetadataItemName, false) {
                    @Override
                    public InputStream createInputStream(long offset) throws IOException {
                        var content = contentRepositoryService.findBySignature(i.getSignature()).stream().filter(Content::isFileExists).findAny().orElseThrow(() -> new IOException("Could not find content"));
                        var volumeRootPath = content.getVolume().toPath();
                        var inputPath = volumeRootPath.resolve(content.getPathFileRelative());
                        if (content.isCompressedEntry()) { // Compressed
                            var entryNameEquals = content.getFileEntry();
                            return archiveService.extractToLazyArchiveEntryProducerByName(inputPath, entryNameEquals, i.getName(), 0).getInputStream();
                        } else { // Normal File
                            var inputStream = ((ArchiveEntryProducer) new PathArchiveEntryProducer(volumeRootPath.resolve(content.getPathFileRelative()), volumeRootPath, i.getName())).getInputStream();
                            if (offset > 0) {
                                var skipped = inputStream.skip(offset);
                                if (skipped == 0) {
                                    throw new EOFException();
                                }
                            }
                            return inputStream;
                        }
                    }

                    @Override
                    public long getSize() {
                        return i.getSize();
                    }
                })
            .orElse(null);
    }

    private GenericFtpFileAdapter createContentMetadataItemFtpFileAdapter(String contentMetadataName, String parentPath) {
        var id = ContentMetadata.fromStringFile(contentMetadataName);
        return new GenericFtpFileAdapter(parentPath + "/" + contentMetadataName, true) {
            @Override
            public List<? extends FtpFile> listFiles() {
                return contentMetadataItemRepositoryService.findByContentMetadataId(id)
                    .stream()
                    .filter(cmi -> contentRepositoryService.findBySignature(cmi.getSignature()).stream().anyMatch(Content::isFileExists))
                    .map(cmi -> createContentMetadataItemFile(parentPath, contentMetadataName, cmi.getName()))
                    .collect(Collectors.toList());
            }
        };
    }

    private GenericFtpFileAdapter createContentMetadataItemsDirByContentMetadataSetAndPrefixAndContentMetadata(String byMetadataSetPath, String metadataSet, String prefix, String contentMetadataName) {
        return createContentMetadataItemFtpFileAdapter(contentMetadataName, byMetadataSetPath + "/" + metadataSet + "/" + prefix);
    }

    private GenericFtpFileAdapter createContentMetadataItemsDirBySystemAndPrefixAndContentMetadata(final String bySystemPath, String system, String prefix, String contentMetadataName) {
        return createContentMetadataItemFtpFileAdapter(contentMetadataName, bySystemPath + "/" + system + "/" + prefix);
    }

    private GenericFtpFileAdapter createContentMetadataPrefixesDirByContentMetadataSet(String byMetadataSetPath, String metadataSet) {
        var metadataSetId = ContentMetadataSet.fromStringFileUnique(metadataSet);

        return new GenericFtpFileAdapter(byMetadataSetPath + "/" + metadataSet, true) {
            @Override
            public List<? extends FtpFile> listFiles() {
                return contentMetadataSearchService.searchContentMetadataPrefixByContentMetadataSetIdAndCompleteNotEmpty(metadataSetId)
                    .stream()
                    .map(p -> createContentMetadatasDirByContentMetadataSetAndPrefix(byMetadataSetPath, metadataSet, p))
                    .collect(Collectors.toList());
            }
        };
    }

    private GenericFtpFileAdapter createContentMetadataPrefixesDirBySystem(final String bySystemPath, String system) {
        return new GenericFtpFileAdapter(bySystemPath + "/" + system, true) {
            @Override
            public List<? extends FtpFile> listFiles() {
                return contentMetadataSearchService.searchContentMetadataPrefixBySystemAndContentNotEmpty(system).stream().map(p -> createContentMetadatasDirBySystemsAndPrefix(bySystemPath, system, p)).collect(Collectors.toList());
            }
        };
    }

    private GenericFtpFileAdapter createContentMetadataSetFtpFile(final String absolutePath) {
        return new GenericFtpFileAdapter(absolutePath, true) {};
    }

    private GenericFtpFileAdapter createContentMetadatasDirByContentMetadataSetAndPrefix(String byMetadataSetPath, String metadataSet, String prefix) {
        var metadataSetId = ContentMetadataSet.fromStringFileUnique(metadataSet);

        return new GenericFtpFileAdapter(byMetadataSetPath + "/" + metadataSet + "/" + prefix, true) {
            @Override
            public List<? extends FtpFile> listFiles() {
                var queryResultFirstPage = contentMetadataSearchService.searchByMetadataSetIdAndPrefixAndCompleteNotEmpty(metadataSetId, prefix, PageRequest.of(0, BATCH_SIZE));
                var pages
                    = IntStream.range(1, queryResultFirstPage.getTotalPages()).mapToObj(i -> contentMetadataSearchService.searchByMetadataSetIdAndPrefixAndCompleteNotEmpty(metadataSetId, prefix, PageRequest.of(i, BATCH_SIZE)));

                return Stream.concat(Stream.of(queryResultFirstPage), pages)
                    .flatMap(Streamable::get)
                    .map(cm -> createContentMetadataItemsDirByContentMetadataSetAndPrefixAndContentMetadata(byMetadataSetPath, prefix, metadataSet, cm.toStringFileUnique()))
                    .collect(Collectors.toList());
            }
        };
    }

    private GenericFtpFileAdapter createContentMetadatasDirBySystemsAndPrefix(final String bySystemPath, String system, String prefix) {
        return new GenericFtpFileAdapter(bySystemPath + "/" + system + "/" + prefix, true) {
            @Override
            public List<? extends FtpFile> listFiles() {
                var queryResultFirstPage = contentMetadataSearchService.searchBySystemAndPrefixAndCompleteNotEmpty(system, prefix, PageRequest.of(0, BATCH_SIZE));
                var pages
                    = IntStream.range(1, queryResultFirstPage.getTotalPages()).mapToObj(i -> contentMetadataSearchService.searchBySystemAndPrefixAndCompleteNotEmpty(system, prefix, PageRequest.of(i, BATCH_SIZE)));

                return Stream.concat(Stream.of(queryResultFirstPage), pages)
                    .flatMap(Streamable::get)
                    .map(cm -> createContentMetadataItemsDirBySystemAndPrefixAndContentMetadata(bySystemPath, system, prefix, cm.toStringFileUnique()))
                    .collect(Collectors.toList());
            }
        };
    }

    public GenericFtpFileAdapter createHomeDir() {
        return new GenericFtpFileAdapter("/", true) {
            @Override
            public List<? extends FtpFile> listFiles() {
                return Stream.of(byMetadataSetDir, bySystemDir).collect(Collectors.toList());
            }
        };
    }

    @PostConstruct
    public void postConstruct() throws FtpException {
        var ftpServer = ftpServerFactory.createServer();
        ftpServerFactory.setFileSystem(
            user -> new FileSystemView() {
                private FtpFile current = homeDir;

                @Override
                public boolean changeWorkingDirectory(String directory) {
                    log.debug("changeWorkingDirectory, path={}", directory);
                    // resolve if exists!
                    var rootDirectory = "/".equals(directory) || "/.".equals(directory);
                    if (rootDirectory) {
                        current = homeDir;
                        return true;
                    }
                    var dir = StringUtils.removeEnd(directory, "/");
                    if (dir.startsWith(PATH_BY_METADATA_SET)) {
                        if (PATH_BY_METADATA_SET.equals(dir)) {
                            current = byMetadataSetDir;
                            return true;
                        } else if (pathMatcher.match(PATH_BY_METADATA_SET + "/*", dir)) {
                            var metadataSet = pathMatcher.extractPathWithinPattern(PATH_BY_METADATA_SET + "/*", dir);
                            current = createContentMetadataPrefixesDirByContentMetadataSet(PATH_BY_METADATA_SET, metadataSet);
                            return true;
                        } else if (pathMatcher.match(PATH_BY_METADATA_SET + "/*/*", dir)) {
                            var vars = pathMatcher.extractUriTemplateVariables(PATH_BY_METADATA_SET + "/{set}/{prefix}", dir);
                            current = createContentMetadatasDirByContentMetadataSetAndPrefix(PATH_BY_METADATA_SET, vars.get("set"), vars.get("prefix"));
                            return true;
                        } else if (pathMatcher.match(PATH_BY_METADATA_SET + "/*/*/*", dir)) {
                            var vars = pathMatcher.extractUriTemplateVariables(PATH_BY_METADATA_SET + "/{set}/{prefix}/{contentMetadataName}", dir);
                            current = createContentMetadataItemsDirByContentMetadataSetAndPrefixAndContentMetadata(PATH_BY_METADATA_SET, vars.get("set"), vars.get("prefix"), vars.get("contentMetadataName"));
                            return true;
                        }
                    } else if (dir.startsWith(PATH_BY_SYSTEM)) {
                        if (PATH_BY_SYSTEM.equals(dir)) {
                            current = bySystemDir;
                            return true;
                        } else if (pathMatcher.match(PATH_BY_SYSTEM + "/*", dir)) {
                            var system = pathMatcher.extractPathWithinPattern(PATH_BY_SYSTEM + "/*", dir);
                            current = createContentMetadataPrefixesDirBySystem(PATH_BY_SYSTEM, system);
                            return true;
                        } else if (pathMatcher.match(PATH_BY_SYSTEM + "/*/*", dir)) {
                            var vars = pathMatcher.extractUriTemplateVariables(PATH_BY_SYSTEM + "/{system}/{prefix}", dir);
                            current = createContentMetadatasDirBySystemsAndPrefix(PATH_BY_SYSTEM, vars.get("system"), vars.get("prefix"));
                            return true;
                        } else if (pathMatcher.match(PATH_BY_SYSTEM + "/*/*/*", dir)) {
                            var vars = pathMatcher.extractUriTemplateVariables(PATH_BY_SYSTEM + "/{system}/{prefix}/{contentMetadataName}", dir);
                            current = createContentMetadataItemsDirBySystemAndPrefixAndContentMetadata(PATH_BY_SYSTEM, vars.get("system"), vars.get("prefix"), vars.get("contentMetadataName"));
                            return true;
                        }
                    }
                    return false;
                }

                @Override
                public void dispose() {
                    // Do nothing
                }

                @Override
                public FtpFile getFile(String file) {
                    log.debug("getFile, path={}", file);

                    if (pathMatcher.match(PATH_BY_SYSTEM + "/*/*/*/*", file)) {
                        var vars = pathMatcher.extractUriTemplateVariables(PATH_BY_SYSTEM + "/{system}/{prefix}/{contentMetadataName}/{contentMetadataItem}", file);
                        return createContentMetadataItemFile(PATH_BY_SYSTEM + "/" + vars.get("system") + "/" + vars.get("prefix"), vars.get("contentMetadataName"), vars.get("contentMetadataItem"));
                    }
                    if (pathMatcher.match(PATH_BY_METADATA_SET + "/*/*/*/*", file)) {
                        var vars = pathMatcher.extractUriTemplateVariables(PATH_BY_METADATA_SET + "/{set}/{prefix}/{contentMetadataName}/{contentMetadataItem}", file);
                        return createContentMetadataItemFile(PATH_BY_METADATA_SET + "/" + vars.get("set") + "/" + vars.get("prefix"), vars.get("contentMetadataName"), vars.get("contentMetadataItem"));
                    }

                    return current;
                }

                @Override
                public FtpFile getHomeDirectory() {
                    return homeDir;
                }

                @Override
                public FtpFile getWorkingDirectory() {
                    return current;
                }

                @Override
                public boolean isRandomAccessible() {
                    return false;
                }
            });

        ftpServer.start();
    }

    private class GenericFtpFileAdapter implements FtpFile {

        private static final int DIRECTORY_LINK_COUNT = 3;
        private final String absolutePath;
        private final String name;

        private final boolean directory;

        GenericFtpFileAdapter(String absolutePath, boolean directory) {
            this.absolutePath = absolutePath;
            this.name = Optional.ofNullable(Paths.get(absolutePath).getFileName()).map(Path::toString).orElse("");
            this.directory = directory;
        }

        @Override
        public InputStream createInputStream(long offset) throws IOException {
            return null;
        }

        @Override
        public OutputStream createOutputStream(long offset) {
            return null;
        }

        @Override
        public boolean delete() {
            return false;
        }

        @Override
        public boolean doesExist() {
            return true;
        }

        @Override
        public String getAbsolutePath() {
            return absolutePath;
        }

        @Override
        public String getGroupName() {
            return "retromaster";
        }

        @Override
        public long getLastModified() {
            return 0;
        }

        public int getLinkCount() {
            return directory ? DIRECTORY_LINK_COUNT : 1;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getOwnerName() {
            return "retromaster";
        }

        @Override
        public Object getPhysicalFile() {
            return name;
        }

        @Override
        public long getSize() {
            return 0;
        }

        @Override
        public boolean isDirectory() {
            return directory;
        }

        @Override
        public boolean isFile() {
            return !directory;
        }

        @Override
        public boolean isHidden() {
            return false;
        }

        @Override
        public boolean isReadable() {
            return true;
        }

        @Override
        public boolean isRemovable() {
            return false;
        }

        @Override
        public boolean isWritable() {
            return false;
        }

        @Override
        public List<? extends FtpFile> listFiles() {
            return isDirectory() ? new ArrayList<>() : null;
        }

        @Override
        public boolean mkdir() {
            return false;
        }

        @Override
        public boolean move(FtpFile destination) {
            return false;
        }

        @Override
        public boolean setLastModified(long time) {
            return false;
        }
    }
}
