package com.tincore.retromaster.server.fuse.fs;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jnr.ffi.Pointer;
import lombok.extern.slf4j.Slf4j;
import ru.serce.jnrfuse.FuseFillDir;
import ru.serce.jnrfuse.struct.FileStat;

import java.util.stream.Stream;

import static com.tincore.retromaster.server.fuse.fs.ExtendedFuseFs.DIRECTORY_ENTRY_SELF;
import static com.tincore.retromaster.server.fuse.fs.ExtendedFuseFs.DIRECTORY_ENTRY_UP;

@Slf4j
public abstract class FuseDirectory extends FuseEntry {

    public FuseDirectory(FuseDirectory parent, long uid, long gid, boolean writable) {
        super(parent, uid, gid, writable);
    }

    @Override
    public void loadAttr(FileStat stat) {
        stat.st_mode.set(getMode());
        super.loadAttr(stat);
    }

    public abstract Stream<String> getDirectoryEntryNames();

    public int getMode() {
        return FileStat.S_IFDIR | (isWritable() ? ExtendedFuseFs.PERMISSIONS_RWX : ExtendedFuseFs.PERMISSIONS_RX);
    }

    public void mkdir(String name, long uid, long gid) {
        log.warn("mkdir not implemented, name={}, uid={}, gid={}", name, uid, gid);
    }

    public void mkfile(String name, long uid, long gid) {
        log.warn("mkfile not implemented, name={}, uid={}, gid={}", name, uid, gid);
    }

    public void readDir(Pointer buf, FuseFillDir filler) {
        filler.apply(buf, DIRECTORY_ENTRY_SELF, null, 0);
        filler.apply(buf, DIRECTORY_ENTRY_UP, null, 0);
        getDirectoryEntryNames().forEach(n -> filler.apply(buf, n, null, 0));
    }

    public void rmFile(FuseEntry entry) {
        log.warn("rmFile not implemented, entry={}", entry);
    }
}
