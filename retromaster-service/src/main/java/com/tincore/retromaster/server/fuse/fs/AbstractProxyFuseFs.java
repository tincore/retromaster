package com.tincore.retromaster.server.fuse.fs;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.kenai.jffi.MemoryIO;
import jnr.ffi.Pointer;
import jnr.ffi.Runtime;
import jnr.ffi.Struct;
import jnr.ffi.types.off_t;
import jnr.ffi.types.size_t;
import ru.serce.jnrfuse.AbstractFuseFS;
import ru.serce.jnrfuse.ErrorCodes;
import ru.serce.jnrfuse.flags.FuseBufFlags;
import ru.serce.jnrfuse.struct.*;

public abstract class AbstractProxyFuseFs extends AbstractFuseFS {
    private final ExtendedFuseFs target;

    public AbstractProxyFuseFs(ExtendedFuseFs target) {
        super();
        this.target = target;
    }

    @Override
    public int access(String path, int mask) {
        return target.access(path, mask);
    }

    @Override
    public int bmap(String path, long blocksize, long idx) {
        return target.bmap(path, blocksize, idx);
    }

    @Override
    public int chmod(String path, long mode) {
        return target.chmod(path, mode);
    }

    @Override
    public int chown(String path, long uid, long gid) {
        return target.chown(path, uid, gid);
    }

    @Override
    public int create(String path, long mode, FuseFileInfo fi) {
        return target.create(path, mode, fi);
    }

    @Override
    public void destroy(Pointer initResult) {
        target.destroy(initResult);
    }

    @Override
    public int fallocate(String path, int mode, long off, long length, FuseFileInfo fi) {
        return target.fallocate(path, mode, off, length, fi);
    }

    @Override
    public int fgetattr(String path, FileStat stbuf, FuseFileInfo fi) {
        return getattr(path, stbuf);
    }

    @Override
    public int flock(String path, FuseFileInfo fi, int op) {
        return target.flock(path, fi, op);
    }

    @Override
    public int flush(String path, FuseFileInfo fi) {
        return target.flush(path, fi);
    }

    @Override
    public int fsync(String path, int isdatasync, FuseFileInfo fi) {
        return target.fsync(path, isdatasync, fi);
    }

    @Override
    public int fsyncdir(String path, FuseFileInfo fi) {
        return target.fsyncdir(path, fi);
    }

    @Override
    public int ftruncate(String path, long size, FuseFileInfo fi) {
        return target.ftruncate(path, size, fi);
    }

    public ExtendedFuseFs getTarget() {
        return target;
    }

    @Override
    public int getattr(String path, FileStat stat) {
        return target.getattr(path, stat);
    }

    @Override
    public int getxattr(String path, String name, Pointer value, long size) {
        return target.getxattr(path, name, value, size);
    }

    @Override
    public Pointer init(Pointer conn) {
        return target.init(conn);
    }

    @Override
    public int ioctl(String path, int cmd, Pointer arg, FuseFileInfo fi, long flags, Pointer data) {
        return target.ioctl(path, cmd, arg, fi, flags, data);
    }

    @Override
    public int link(String oldpath, String newpath) {
        return target.link(oldpath, newpath);
    }

    @Override
    public int listxattr(String path, Pointer list, long size) {
        return target.listxattr(path, list, size);
    }

    @Override
    public int lock(String path, FuseFileInfo fi, int cmd, Flock flock) {
        return target.lock(path, fi, cmd, flock);
    }

    @Override
    public int mkdir(String path, long mode) {
        return target.mkdir(path, mode);
    }

    @Override
    public int mknod(String path, long mode, long rdev) {
        return target.mknod(path, mode, rdev);
    }

    @Override
    public int open(String path, FuseFileInfo fi) {
        return target.open(path, fi);
    }

    @Override
    public int opendir(String path, FuseFileInfo fi) {
        return target.opendir(path, fi);
    }

    @Override
    public int poll(String path, FuseFileInfo fi, FusePollhandle ph, Pointer reventsp) {
        return target.poll(path, fi, ph, reventsp);
    }

    @Override
    public int read(String path, Pointer buf, long size, long offset, FuseFileInfo fi) {
        return target.read(path, buf, size, offset, fi);
    }

    public int readBufInner(String path, Pointer bufp, @size_t long size, @off_t long off, FuseFileInfo fi) {
        // should be implemented or null
        var vecmem = MemoryIO.getInstance().allocateMemory(Struct.size(new FuseBufvec(Runtime.getSystemRuntime())), false);
        if (vecmem == 0) {
            return -ErrorCodes.ENOMEM();
        }
        var memAdr = MemoryIO.getInstance().allocateMemory(size, false);
        if (memAdr == 0) {
            MemoryIO.getInstance().freeMemory(vecmem);
            return -ErrorCodes.ENOMEM();
        }
        var src = Pointer.wrap(Runtime.getSystemRuntime(), vecmem);
        var mem = Pointer.wrap(Runtime.getSystemRuntime(), memAdr);
        var buf = FuseBufvec.of(src);
        FuseBufvec.init(buf, size);
        buf.buf.mem.set(mem);
        bufp.putAddress(0, src.address());
        var res = read(path, mem, size, off, fi);
        if (res >= 0) {
            buf.buf.size.set(res);
        }
        return res;
    }

    // @Override
    // public int readdir(String path, Pointer buf, FuseFillDir filter, long offset, FuseFileInfo fi) {
    // return target.readdir(path, buf, filter, offset, fi);
    // }

    @Override
    public int read_buf(String path, Pointer bufp, long size, long off, FuseFileInfo fi) {
        return target.read_buf(path, bufp, size, off, fi);
    }

    @Override
    public int readlink(String path, Pointer buf, long size) {
        return target.readlink(path, buf, size);
    }

    @Override
    public int release(String path, FuseFileInfo fi) {
        return target.release(path, fi);
    }

    @Override
    public int releasedir(String path, FuseFileInfo fi) {
        return target.releasedir(path, fi);
    }

    @Override
    public int removexattr(String path, String name) {
        return target.removexattr(path, name);
    }

    @Override
    public int rename(String oldpath, String newpath) {
        return target.rename(oldpath, newpath);
    }

    @Override
    public int rmdir(String path) {
        return target.rmdir(path);
    }

    @Override
    public int setxattr(String path, String name, Pointer value, long size, int flags) {
        return target.setxattr(path, name, value, size, flags);
    }

    @Override
    public int statfs(String path, Statvfs stbuf) {
        return target.statfs(path, stbuf);
    }

    @Override
    public int symlink(String oldpath, String newpath) {
        return target.symlink(oldpath, newpath);
    }

    @Override
    public int truncate(String path, long size) {
        return target.truncate(path, size);
    }

    @Override
    public int unlink(String path) {
        return target.unlink(path);
    }

    @Override
    public int utimens(String path, Timespec[] timespec) {
        return target.utimens(path, timespec);
    }

    @Override
    public int write(String path, Pointer buf, long size, long offset, FuseFileInfo fi) {
        return target.write(path, buf, size, offset, fi);
    }

    public int writeBufInner(String path, FuseBufvec buf, @off_t long off, FuseFileInfo fi) {
        // TODO.
        // Some problem in implementation, but it not enabling by default
        int res;
        var size = (int) libFuse.fuse_buf_size(buf);
        FuseBuf flatbuf;
        var tmp = new FuseBufvec(Runtime.getSystemRuntime());
        var adr = MemoryIO.getInstance().allocateMemory(Struct.size(tmp), false);
        tmp.useMemory(Pointer.wrap(Runtime.getSystemRuntime(), adr));
        FuseBufvec.init(tmp, size);
        long mem = 0;
        if (buf.count.get() == 1 && buf.buf.flags.get() == FuseBufFlags.FUSE_BUF_IS_FD) {
            flatbuf = buf.buf;
        } else {
            res = -ErrorCodes.ENOMEM();
            mem = MemoryIO.getInstance().allocateMemory(size, false);
            if (mem == 0) {
                MemoryIO.getInstance().freeMemory(adr);
                return res;
            }
            tmp.buf.mem.set(mem);
            res = (int) libFuse.fuse_buf_copy(tmp, buf, 0);
            if (res <= 0) {
                MemoryIO.getInstance().freeMemory(adr);
                MemoryIO.getInstance().freeMemory(mem);
                return res;
            }
            tmp.buf.size.set(res);
            flatbuf = tmp.buf;
        }
        res = write(path, flatbuf.mem.get(), flatbuf.size.get(), off, fi);
        if (mem != 0) {
            MemoryIO.getInstance().freeMemory(adr);
            MemoryIO.getInstance().freeMemory(mem);
        }
        return res;
    }

    @Override
    public int write_buf(String path, FuseBufvec buf, long off, FuseFileInfo fi) {
        return target.write_buf(path, buf, off, fi);
    }
}
