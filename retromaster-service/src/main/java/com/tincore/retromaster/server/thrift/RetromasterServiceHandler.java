package com.tincore.retromaster.server.thrift;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.service.ResourceNotFoundException;
import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.service.*;
import com.tincore.retromaster.thrift.RetromasterService;
import com.tincore.retromaster.thrift.form.*;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.StreamTrait;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.thrift.TException;
import org.hibernate.search.util.common.SearchException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentLocation.isFileModifiedTimeEquals;
import static com.tincore.util.LangExtension.distinctBy;
import static com.tincore.util.lang.ThrowingFunction.uFunction;


@Slf4j
@Service
@RequiredArgsConstructor
public class RetromasterServiceHandler implements RetromasterService.Iface, StreamTrait, FileSystemTrait {

    public static final short FORBIDDEN_CODE = (short) 403;
    public static final Pageable CONTENT_METADATA_MEDIAS_PAGE = Pageable.ofSize(100);

    private final TFormMapper tFormMapper;
    private final ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    private final ContentMetadataSetService contentMetadataSetService;
    private final ContentMetadataItemService contentMetadataItemService;
    private final ContentMetadataRepositoryService contentMetadataRepositoryService;
    private final ContentSignatureChangeRepositoryService contentSignatureChangeRepositoryService;
    private final ContentExecutorService contentExecutorService;
    private final ContentService contentService;
    private final ContentRepositoryService contentRepositoryService;
    private final ContentSystemService contentSystemService;
    private final ContentVolumeService contentVolumeService;
    private final ContentVolumeRepositoryService contentVolumeRepositoryService;
    private final ProcessExecutorService processExecutorService;
    private final ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;
    private final ContentExecuteOptionSetService contentExecuteOptionSetService;
    private final RetromasterOrchestratorService retromasterOrchestratorService;
    private final ContentMetadataSearchService contentMetadataSearchService;
    private final TransactionConfiguration.TransactionReadTemplate transactionReadTemplate;

    private void assertHasAuthority(String authority) throws TGenericError {
        if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().noneMatch(a -> a.getAuthority().equals(authority))) {
            throw new TGenericError("FORBIDDEN(not(" + authority + "))", FORBIDDEN_CODE); // NOPMD
        }
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENTMETADATASET_DELETE')")
    public void deleteContentMetadataSetById(String id) throws TGenericError {
        assertHasAuthority("CONTENTMETADATASET_DELETE");
        var contentMetadataSet = contentMetadataSetRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentMetadataSet.class, id));
        contentMetadataSetService.delete(contentMetadataSet);
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENTVOLUME_DELETE')")
    public void deleteContentVolume(String id) throws TGenericError {
        assertHasAuthority("CONTENTVOLUME_DELETE");
        contentVolumeRepositoryService.deleteById(tFormMapper.toUuid(id));
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENT_EXECUTE')")
    public void doContentMetadataItemExecute(String contentMetadataItemId, String contentExecutorId) throws TException {
        assertHasAuthority("CONTENT_EXECUTE");
        var contentMetadataItem = contentMetadataItemRepositoryService
            .findById(tFormMapper.toUuid(contentMetadataItemId))
            .orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadataItem.class, contentMetadataItemId), (short) -1));
        try {
            contentExecutorService.findById(contentExecutorId).execute(contentMetadataItem);
        } catch (IOException | InterruptedException e) {
            log.error("doContentMetadataItemExecute error", e);
            throw new TGenericError(e.getMessage(), (short) -1); // NOPMD
        }
    }


    @Override
    public void doContentReorganizeRequest() throws TGenericError {
        assertHasAuthority("CONTENT_EDIT");
        retromasterOrchestratorService.doContentReorganizeRequest();
    }

    @Override
    public void doContentScanByContentMetadataId(String id) {
        var contentMetadata = contentMetadataRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentMetadata.class, id));
        retromasterOrchestratorService.doContentScanByContentMetadataRequest(contentMetadata);
    }

    @Override
    public void doContentScanByContentMetadataSetId(String id) {
        var contentMetadataSet = contentMetadataSetRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentMetadataSet.class, id));
        retromasterOrchestratorService.doContentScanByContentMetadataSetRequest(contentMetadataSet);
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENT_EDIT')")
    public void doContentScanByContentVolumeStaging() throws TGenericError {
        assertHasAuthority("CONTENT_EDIT");
        retromasterOrchestratorService.doContentScanByContentVolumeStagingRequest();
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENTVOLUME_EDIT')")
    public void doContentVolumeDirectoryScan(TContentVolumeDirectory directory) throws TGenericError {
        assertHasAuthority("CONTENTVOLUME_EDIT");
        var contentVolume = contentVolumeRepositoryService.findById(tFormMapper.toUuid(directory.getContentVolumeId())).orElseThrow(() -> new ResourceNotFoundException(ContentVolume.class, directory.contentVolumeId));
        retromasterOrchestratorService.doContentScanByVolumeAndDirectoryRequest(Pair.of(contentVolume, directory.path));
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENT_READ')")
    public TQueryResults doQuery(TQuery query, TPageable pageable) throws TGenericError {
        assertHasAuthority("CONTENT_READ");

        // Map<String, String> filters = toStream(query.getFilters()).collect(Collectors.toMap(TQueryFilter::getField, TQueryFilter::getValue)); // Not used. If needed create specific filters in TQuery
        var facetFilters = tFormMapper.toQueryFacetFilter(query.getFacetFilters());
        try {
            return transactionReadTemplate.execute(
                ts -> {
                    var contentMetadataQueryResult = contentMetadataSearchService.searchByTextPaged(query.getQuery(), facetFilters, ContentMediaType.PIC, tFormMapper.toPageable(pageable));

                    var queryResults = contentMetadataQueryResult.getContent()
                        .stream()
                        .map(tFormMapper::toTQueryResult)
                        .toList();

                    return tFormMapper.toTQueryResults(contentMetadataQueryResult, queryResults);
                });
        } catch (SearchException e) {
            log.error("doQueryFailed", e);
            throw new TGenericError(e.getMessage(), (short) -1); // NOPMD
        }
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENT_EDIT')")
    public void doReindexRequest() throws TGenericError {
        assertHasAuthority("CONTENT_EDIT");
        retromasterOrchestratorService.doReindexRequest();

    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_READ')")
    public TContent findContentById(String id) throws TGenericError {
        assertHasAuthority("CONTENT_READ");
        var content = contentRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", Content.class, id), (short) -1));
        return tFormMapper.toTContent(content);
    }

    @Override
    public TContentDirectoryPaths findContentDirectoryPathsByFilter(TContentDirectoryPathFilter filter, TPageable pageable) {
        var directoryPaths = contentRepositoryService.findDistinctContentDirectoryPaths(tFormMapper.toPageable(pageable));
        return tFormMapper.toTContentDirectoryPaths(directoryPaths);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_EXECUTE')")
    public List<TContentExecuteProcess> findContentExecuteProcessesByCurrent() throws TGenericError {
        assertHasAuthority("CONTENT_EXECUTE");
        var contentExecuteProcesses = processExecutorService.findContentExecuteProcesses();
        return tFormMapper.toTContentExecuteProcesses(contentExecuteProcesses);
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENT_EXECUTE')")
    public List<TContentExecutor> findContentExecutors(TContentExecutorFilter filter) throws TGenericError {
        assertHasAuthority("CONTENT_EXECUTE");
        return tFormMapper.toTContentExecutors(contentExecutorService.findContentExecutorsByFilter(filter.isSetEnabledEquals() ? filter.enabledEquals : null, filter.isFoundEquals() ? filter.foundEquals : null));
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENT_EXECUTE')")
    public List<TContentExecutor> findContentExecutorsByContentMetadataItemId(String id) throws TGenericError {
        assertHasAuthority("CONTENT_EXECUTE");
        var contentMetadataItem
            = contentMetadataItemRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadataItem.class, id), (short) -1));

        var contentSystems = contentSystemService.findByContentMetadata(contentMetadataItem.getContentMetadata());
        var executors = contentExecutorService.findContentExecutorsByContentMetadataItem(contentMetadataItem, contentSystems);
        return tFormMapper.toTContentExecutors(executors);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_READ')")
    public String findContentIdByContentMetadataItemId(String id) throws TException {
        assertHasAuthority("CONTENT_READ");
        var contentMetadataItem
            = contentMetadataItemRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadataItem.class, id), (short) -1));

        return contentService.findContentByContentMetadataItem(contentMetadataItem).map(c -> tFormMapper.toString(c.getId())).orElse("");
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_READ')")
    @Cacheable(cacheNames = "findContentIdsByContentMetadataId")
    public Map<String, String> findContentIdsByContentMetadataId(String id, boolean filterExecutables) throws TException {
        assertHasAuthority("CONTENT_READ");
        var contentMetadata = contentMetadataRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadata.class, id), (short) -1));

        var executableContentMetadataItems = filterExecutables ? contentMetadataItemService.getExecutableItems(contentMetadata) : contentMetadata.getItems();
        return tFormMapper.toMapString(contentRepositoryService.findContentMetadataItemIdContentIdPairs(executableContentMetadataItems));
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_READ')")
    public Map<String, String> findContentIdsByContentMetadataItemIds(List<String> ids) throws TGenericError {
        assertHasAuthority("CONTENT_READ");
        var items = ids.stream()
            .map(uFunction(id -> contentMetadataItemRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadataItem.class, id), (short) -1))))
            .collect(Collectors.toList());
        return tFormMapper.toMapString(contentRepositoryService.findContentMetadataItemIdContentIdPairs(items));
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_READ')")
    public TContentMetadata findContentMetadataById(String id) throws TGenericError {
        assertHasAuthority("CONTENTMETADATA_READ");
        var contentMetadata = contentMetadataRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentMetadata.class, id));

        var tContentMetadata = tFormMapper.toTContentMetadata(contentMetadata);

        // Sort executables 1st
        contentSystemService
            .findBySystemLabel(tContentMetadata.getSystem())
            .map(ContentSystem::getContentExtensions)
            .filter(es -> !es.isEmpty())
            .ifPresent(
                es -> tContentMetadata
                    .getItems()
                    .sort(
                        (o1, o2) -> {
                            var c1 = ContentExtension.findByFilename(o1.getName());
                            var c2 = ContentExtension.findByFilename(o2.getName());
                            var c1Ext = es.contains(c1);
                            var c2Ext = es.contains(c2);
                            if (c1Ext && !c2Ext) {
                                return -1;
                            } else if (!c1Ext && c2Ext) {
                                return 1;
                            }

                            return o1.getName().compareTo(o2.getName());
                        }));

        return tContentMetadata;
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_READ')")
    public TContentMetadataItem findContentMetadataItemById(String id) throws TGenericError {
        assertHasAuthority("CONTENTMETADATA_READ");
        var contentMetadataItem
            = contentMetadataItemRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadataItem.class, id), (short) -1));
        return tFormMapper.toTContentMetadataItem(contentMetadataItem);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_EXECUTE')")
    public TContentMetadataItemExecuteOptionSet findContentMetadataItemExecuteOptionSetByContentMetadataId(String id) throws TGenericError {
        assertHasAuthority("CONTENT_EXECUTE");

        var contentMetadata = contentMetadataRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadata.class, id), (short) -1));

        var executableContentMetadataItems = contentMetadataItemService.getExecutableItems(contentMetadata);
        var contentMetadataItemIdContentIdPairs = contentRepositoryService.findContentMetadataItemIdContentIdPairs(executableContentMetadataItems);

        return executableContentMetadataItems.stream()
            .filter(cmi -> contentMetadataItemIdContentIdPairs.containsKey(cmi.getId()))
            .findFirst()
            .map(cmi -> {
                var contentSystems = contentSystemService.findByContentMetadata(cmi.getContentMetadata());
                return tFormMapper.toTContentMetadataItemExecuteOptionsSet(cmi, contentExecutorService.findContentExecutorsByContentMetadataItem(cmi, contentSystems));
            })
            .orElseThrow(() -> new ResourceNotFoundException(TContentMetadataItemExecuteOptionSet.class, id));
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_EXECUTE')")
    public List<TContentMetadataItemExecuteOptionSet> findContentMetadataItemExecuteOptionSetsByContentId(String id) throws TGenericError {
        assertHasAuthority("CONTENT_EXECUTE");
        var content = contentRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(Content.class, id));
        var contentExecuteOptionSet = contentExecuteOptionSetService.getContentExecuteOptionSets(content);
        return tFormMapper.toTContentMetadataItemExecuteOptionsSets(contentExecuteOptionSet.getContentMetataItemExecuteOptionSets());
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_READ')")
    public List<TContentMetadataItemSummary> findContentMetadataItemSummariesByContentId(String id) throws TException {
        assertHasAuthority("CONTENTMETADATA_READ");

        var content = contentRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", Content.class, id), (short) -1));
        var contentMetadataItems = contentMetadataItemRepositoryService.findBySignature(content.getSignature());
        return tFormMapper.toTContentMetadataItemSummaries(contentMetadataItems);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_READ')")
    public List<TContentMetadataItem> findContentMetadataItemsByContentId(String id) throws TGenericError {
        assertHasAuthority("CONTENTMETADATA_READ");

        var content = contentRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", Content.class, id), (short) -1));
        var contentMetadataItems = contentMetadataItemRepositoryService.findBySignature(content.getSignature());
        return tFormMapper.toTContentMetadataItems(contentMetadataItems);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TContentMetadataMedia> findContentMetadataMediasByContentMetadataId(String id) throws TException {
        assertHasAuthority("CONTENT_READ");
        var contentMetadata = contentMetadataRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadata.class, id), (short) -1));
        return Stream.of(contentMetadata)
            .filter(m -> StringUtils.isNotBlank(m.getTitle()))
            .flatMap(m -> streamContentMetadataMediasBySystemAndTitle(m.getSystem(), m.getTitle()))
            .toList();
    }

    @Override
    public List<TContentMetadataMedia> findContentMetadataMediasBySystemAndTitle(String system, String title) throws TException {
        assertHasAuthority("CONTENT_READ");
        return streamContentMetadataMediasBySystemAndTitle(system, title).toList();
    }

    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATASET_READ')")
    public TContentMetadataSet findContentMetadataSetById(String id) throws TGenericError {
        assertHasAuthority("CONTENTMETADATASET_READ");

        var contentMetadataSet = contentMetadataSetRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentMetadataSet.class, id));
        return tFormMapper.toTContentMetadataSet(contentMetadataSet);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATASET_READ')")
    public TContentMetadataSets findContentMetadataSetsByFilter(TContentMetadataSetFilter filter, TPageable pageable) throws TGenericError {
        assertHasAuthority("CONTENTMETADATASET_READ");

        var page = contentMetadataSetRepositoryService.findAll(tFormMapper.toContentMetadataSetPredicate(filter), tFormMapper.toPageable(pageable));
        return tFormMapper.toTContentMetadataSets(page);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_READ')")
    public TContentMetadataSummaries findContentMetadataSummariesByFilter(TContentMetadataFilter filter, TPageable pageable) throws TException {
        assertHasAuthority("CONTENTMETADATA_READ");

        var page = contentMetadataRepositoryService.findAll(tFormMapper.toContentMetadataPredicate(filter), tFormMapper.toPageable(pageable));
        return tFormMapper.toTContentMetadataSummaries(page);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_READ')")
    public List<TContentMetadata> findContentMetadatasByContentId(String id) throws TGenericError {
        assertHasAuthority("CONTENTMETADATA_READ");

        var content = contentRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", Content.class, id), (short) -1));

        var contentMetadataItems = contentMetadataItemRepositoryService.findBySignature(content.getSignature());
        return tFormMapper.toTContentMetadatas(contentMetadataItems.stream().map(ContentMetadataItem::getContentMetadata).distinct());
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_READ')")
    public TContentMetadatas findContentMetadatasByFilter(TContentMetadataFilter filter, TPageable pageable) throws TGenericError {
        assertHasAuthority("CONTENTMETADATA_READ");

        var page = contentMetadataRepositoryService.findAll(tFormMapper.toContentMetadataPredicate(filter), tFormMapper.toPageable(pageable));
        return tFormMapper.toTContentMetadatas(page);
    }

    @Override
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_READ')")
    public List<TContentSystem> findContentSystems() throws TGenericError {
        assertHasAuthority("CONTENTMETADATA_READ");
        return tFormMapper.toTContentSystems(Stream.of(ContentSystem.values()).collect(Collectors.toList()));
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTVOLUME_READ')")
    public TContentVolume findContentVolumeById(String id) throws TGenericError {
        assertHasAuthority("CONTENTVOLUME_READ");
        var contentVolume = contentVolumeRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentVolume.class, id));
        return tFormMapper.toTContentVolume(contentVolume);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTVOLUME_READ')")
    public List<TContentVolumeDirectory> findContentVolumeDirectoriesByContentVolumeId(String id, boolean extended) throws TException {
        assertHasAuthority("CONTENTVOLUME_READ");

        var contentVolume = contentVolumeRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentVolume.class, id));
        try {
            var directoryPaths = contentVolumeService.getDirectoryPaths(contentVolume);
            return directoryPaths.stream().sorted().map(uFunction(p -> findContentVolumeDirectoryByContentVolumeAndPath(contentVolume, p, extended))).toList();
        } catch (IOException e) {
            log.error("doContentExecute error", e);
            throw new TGenericError(e.getMessage(), (short) -1); // NOPMD
        }
    }

    private TContentVolumeDirectory findContentVolumeDirectoryByContentVolumeAndPath(ContentVolume contentVolume, Path path, boolean extended) throws IOException {
        var targetPath = contentVolume.toPath().resolve(path);
        var exists = Files.exists(targetPath);
        var lastModifiedTime = exists ? getLastModifiedTime(targetPath) : null;
        if (extended) {
            var filename = targetPath.getFileName().toString();
            var contentMetadataSet = contentMetadataSetRepositoryService.findByRepositoryPath(filename).stream().findFirst().orElse(null);
            var fresh = lastModifiedTime != null && contentMetadataSet != null && contentMetadataSet.getCheckContentTime() != null && isFileModifiedTimeEquals(lastModifiedTime, contentMetadataSet.getCheckContentTime());
            return tFormMapper.toTContentVolumeDirectory(contentVolume, path, exists, lastModifiedTime, contentMetadataSet, fresh);
        }
        return tFormMapper.toTContentVolumeDirectory(contentVolume, path, exists, lastModifiedTime);
    }

    @Override
    public TContentVolumeDirectory findContentVolumeDirectoryByContentVolumeIdAndPath(String id, String path, boolean extended) throws TException {
        assertHasAuthority("CONTENTVOLUME_READ");

        var contentVolume = contentVolumeRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentVolume.class, id));
        try {
            return findContentVolumeDirectoryByContentVolumeAndPath(contentVolume, Path.of(path), extended);
        } catch (IOException e) {
            log.error("doContentExecute error", e);
            throw new TGenericError(e.getMessage(), (short) -1); // NOPMD
        }
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENTVOLUME_READ')")
    public List<TContentVolume> findContentVolumes() throws TGenericError {
        assertHasAuthority("CONTENTVOLUME_READ");

        return tFormMapper.toTContentVolumes(contentVolumeRepositoryService.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_READ')")
    public List<TContent> findContentsByContentMetadataId(String id) throws TGenericError {
        assertHasAuthority("CONTENT_READ");

        var contentMetadata = contentMetadataRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new ResourceNotFoundException(ContentMetadata.class, id));
        Collection<String> contentSignatures = contentMetadata.getItems().stream().map(ContentMetadataItem::getSignature).distinct().toList();
        if (contentSignatures.isEmpty()) {
            return tFormMapper.toTContents(Collections.emptyList());
        }
        var contents = contentRepositoryService.findBySignatureIn(contentSignatures);
        return tFormMapper.toTContents(contents);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_READ')")
    public List<TContent> findContentsByContentMetadataItemId(String id) throws TGenericError {
        assertHasAuthority("CONTENT_READ");

        var contentMetadataItem
            = contentMetadataItemRepositoryService.findById(tFormMapper.toUuid(id)).orElseThrow(() -> new TGenericError(String.format("Resource not found %s %s", ContentMetadataItem.class, id), (short) -1));
        var contents = contentRepositoryService.findBySignature(contentMetadataItem.getSignature());
        return tFormMapper.toTContents(contents);
    }

    @Override
    @Transactional(readOnly = true)
    // @PreAuthorize("hasAuthority('CONTENT_READ')")
    public TContents findContentsByFilter(TContentFilter filter, TPageable pageable) throws TGenericError {
        assertHasAuthority("CONTENT_READ");

        var page = contentRepositoryService.findAll(tFormMapper.toContentPredicate(filter), tFormMapper.toPageable(pageable));
        return tFormMapper.toTContents(page);
    }

    @Override
    @Transactional
    // @PreAuthorize("hasAuthority('CONTENTMETADATA_EDIT')")
    public String saveContentMetadata(TContentMetadata tContentMetadata) throws TGenericError {
        assertHasAuthority("CONTENTMETADATA_EDIT");

        var contentMetadata = contentMetadataRepositoryService.findById(tFormMapper.toUuid(tContentMetadata.getId())).orElseThrow(() -> new ResourceNotFoundException(ContentMetadata.class, tContentMetadata.getId()));

        tFormMapper.update(tContentMetadata, contentMetadata);

        contentMetadataRepositoryService.save(contentMetadata);
        contentSignatureChangeRepositoryService.saveAll(contentMetadata.getItems().stream().map(ContentMetadataItem::getSignature));

        return tContentMetadata.getId();
    }

    @Override
    @Transactional
    // @PreAuthorize("hasAuthority('CONTENTMETADATASET_EDIT')")
    public String saveContentMetadataSet(TContentMetadataSet tContentMetadataSet) throws TException {
        assertHasAuthority("CONTENTMETADATASET_EDIT");

        var contentMetadataSet
            = contentMetadataSetRepositoryService.findById(tFormMapper.toUuid(tContentMetadataSet.getId())).orElseThrow(() -> new ResourceNotFoundException(ContentMetadataSet.class, tContentMetadataSet.getId()));

        tFormMapper.update(tContentMetadataSet, contentMetadataSet);

        contentMetadataSetRepositoryService.save(contentMetadataSet);

        return tContentMetadataSet.getId();
    }

    private Stream<TContentMetadataMedia> streamContentMetadataMediasBySystemAndTitle(String system, String title) {
        return contentMetadataSearchService.searchBySystemAndTitleAndMediaImageAndCompleteNotEmpty(system, title, CONTENT_METADATA_MEDIAS_PAGE).getContent().stream()
            .flatMap(m -> m.getItems().stream()
                .map(contentService::findContentByContentMetadataItem)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(c -> tFormMapper.toTContentMetadataMedia(c, m.getMediaType()))
            )
            .filter(distinctBy(TContentMetadataMedia::getContentSignature))
            .sorted(Comparator.comparing(TContentMetadataMedia::getMediaType).reversed().thenComparing(TContentMetadataMedia::getContentSignature));
    }

    @Override
    @Transactional
    // @PreAuthorize("hasAuthority('CONTENTVOLUME_EDIT')")
    public void updateContentVolume(TContentVolume volume) throws TGenericError {
        assertHasAuthority("CONTENTVOLUME_EDIT");

        var contentVolume = contentVolumeRepositoryService.findById(tFormMapper.toUuid(volume.getId())).orElseThrow(() -> new ResourceNotFoundException(ContentVolume.class, volume.getId()));
        tFormMapper.update(volume, contentVolume);
        contentVolumeRepositoryService.save(contentVolume);
    }
}
