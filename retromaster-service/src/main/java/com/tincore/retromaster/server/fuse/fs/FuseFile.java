package com.tincore.retromaster.server.fuse.fs;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jnr.ffi.Pointer;
import lombok.extern.slf4j.Slf4j;
import ru.serce.jnrfuse.struct.FileStat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Deque;

@Slf4j
public abstract class FuseFile extends FuseEntry {

    public FuseFile(FuseDirectory parent, long uid, long gid, boolean rw) {
        super(parent, uid, gid, rw);
    }

    @Override
    public FuseEntry find(Deque<String> tokenStack) {
        if (tokenStack.isEmpty()) {
            return this;
        }
        return null;
    }

    public abstract InputStream getInputStream() throws IOException;

    private int getMode() {
        return FileStat.S_IFREG | (isWritable() ? ExtendedFuseFs.PERMISSIONS_RW : ExtendedFuseFs.PERMISSIONS_R);
    }

    public abstract long getSize();

    @Override
    public void loadAttr(FileStat stat) {
        stat.st_mode.set(getMode());
        stat.st_size.set(getSize());
        super.loadAttr(stat);
    }

    public abstract int read(Pointer buffer, long size, long offset);

    public void truncate(long size) {
        log.info("truncate unimplemented {}", getName());
    }

    public abstract int write(Pointer buffer, long bufSize, long writeOffset);
}
