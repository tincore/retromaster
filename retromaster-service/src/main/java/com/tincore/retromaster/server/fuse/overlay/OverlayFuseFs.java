package com.tincore.retromaster.server.fuse.overlay;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.server.fuse.fs.*;
import com.tincore.util.FileSystemTrait;
import jnr.ffi.Pointer;
import jnr.ffi.types.off_t;
import jnr.ffi.types.size_t;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import ru.serce.jnrfuse.ErrorCodes;
import ru.serce.jnrfuse.FuseFillDir;
import ru.serce.jnrfuse.NotImplemented;
import ru.serce.jnrfuse.struct.FileStat;
import ru.serce.jnrfuse.struct.FuseBufvec;
import ru.serce.jnrfuse.struct.FuseFileInfo;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.tincore.retromaster.server.fuse.fs.ExtendedFuseFs.*;

@Slf4j
public class OverlayFuseFs extends AbstractProxyFuseFs implements FileSystemTrait { // NOPMD Cyclomatic

    private static final String DELETED_DIRECTORY = ".del";
    private static final int MILLIS_IN_SECOND = 1000;
    private static final int MILLIS_TO_NANOS = 1_000_000;

    private final Path mutatedDataPath;
    private final long uid;
    private final long gid;

    public OverlayFuseFs(ExtendedFuseFs target, Path mutatedDataPath) {
        super(target);

        uid = getContext().uid.longValue();
        gid = getContext().gid.longValue();

        this.mutatedDataPath = mutatedDataPath;
    }

    @Override
    public int create(String path, long mode, FuseFileInfo fi) {
        log.trace("create {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return -ErrorCodes.EEXIST();
        }
        var entry = getTarget().getFuseEntry(path);
        if (entry != null && !isPathDeleteExists(dataPath)) {
            return -ErrorCodes.EEXIST();
        }

        var parent = dataPath.getParent();
        if (parent != null && parent.startsWith(mutatedDataPath)) {
            // Create file here!
            try {
                createDirectoriesParent(dataPath);
                Files.createFile(dataPath);
                return 0;
            } catch (IOException e) {
                log.error("create failed. path={} ", path, e);
                return -ErrorCodes.EIO();
            }
        }
        return -ErrorCodes.ENOENT();
    }

    @Override
    public int fsync(String path, int isdatasync, FuseFileInfo fi) {
        log.trace("fsync {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return 0;
        }

        return super.fsync(path, isdatasync, fi);
    }

    @Override
    public int fsyncdir(String path, FuseFileInfo fi) {
        log.trace("fsyncdir {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return 0;
        }

        return super.fsyncdir(path, fi);
    }

    private Path getDataPath(String path) {
        return getDataPath(Paths.get(path).normalize());
    }

    private Path getDataPath(Path path) {
        return mutatedDataPath.resolve(path.getRoot().relativize(path));
    }

    private Path getPathDeleted(Path path) {
        return path.resolveSibling(DELETED_DIRECTORY).resolve(path.getFileName());
    }

    @Override
    public int getattr(String path, FileStat stat) {
        log.trace("getattr {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            try {
                stat.st_uid.set(uid);
                stat.st_gid.set(gid);
                var lastModifiedTime = Files.getLastModifiedTime(dataPath);
                stat.st_mtim.tv_sec.set(lastModifiedTime.toMillis() / MILLIS_IN_SECOND);
                stat.st_mtim.tv_nsec.set(lastModifiedTime.toMillis() * MILLIS_TO_NANOS);
                if (Files.isDirectory(dataPath)) {
                    stat.st_mode.set(FileStat.S_IFDIR | (isWritable(dataPath) ? PERMISSIONS_RWX : PERMISSIONS_RX));
                } else {
                    stat.st_mode.set(FileStat.S_IFREG | (isWritable(dataPath) ? PERMISSIONS_RW : PERMISSIONS_R));
                    stat.st_size.set(Files.size(dataPath));
                }
                return 0;
            } catch (IOException e) {
                log.error("getattr failed. path={} ", path, e);
                return -ErrorCodes.EIO();
            }
        }

        if (isPathDeleteExists(dataPath)) {
            return -ErrorCodes.ENOENT();
        }

        // pass through
        return super.getattr(path, stat);
    }

    @Override
    public int getxattr(String path, String name, Pointer value, long size) {
        log.trace("getxattr {} {}", path, name);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return 0;
        }

        return super.getxattr(path, name, value, size);
    }

    public boolean isPathDeleteExists(Path path) {
        return Files.exists(getPathDeleted(path));
    }

    private boolean isWritable(Path path) { // NOPMD Implement list of regexes to decide with paths are writable
        return true;
    }

    @Override
    public int link(String oldpath, String newpath) {
        return super.link(oldpath, newpath);
    }

    @Override
    public int mkdir(String path, long mode) {
        log.trace("mkdir {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return -ErrorCodes.EEXIST();
        }
        var entry = getTarget().getFuseEntry(path);
        if (entry != null) {
            return -ErrorCodes.EEXIST();
        }

        var parent = dataPath.getParent();
        if (parent != null && parent.startsWith(mutatedDataPath)) {
            try {
                Files.createDirectories(dataPath);
                return 0;
            } catch (IOException e) {
                log.error("mkdir failed. path={} ", path, e);
                return -ErrorCodes.EIO();
            }
        }
        return -ErrorCodes.ENOENT();
    }

    @Override
    public int open(String path, FuseFileInfo fi) {
        log.trace("open {}", path);
        // TODO .del is should be forbidden to create
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return 0;
        }

        return super.open(path, fi);
    }

    @Override
    public int opendir(String path, FuseFileInfo fi) {
        log.trace("opendir {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return 0;
        }

        return super.opendir(path, fi);
    }

    @Override
    public int read(String path, Pointer buf, @size_t long size, @off_t long offset, FuseFileInfo fi) {
        log.trace("read {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            if (!Files.isRegularFile(dataPath)) {
                return -ErrorCodes.EISDIR();
            }

            try (var sbc = Files.newByteChannel(dataPath)) {
                var fileContentSize = Files.size(dataPath);
                var totalBytesToRead = (int) Math.min(fileContentSize - offset, size);

                sbc.position(offset);
                var bf = ByteBuffer.allocate(totalBytesToRead);
                sbc.read(bf);
                buf.put(0, bf.array(), 0, totalBytesToRead);
                return totalBytesToRead;
            } catch (IOException e) {
                log.error("read failed. path={} ", path, e);
                return -ErrorCodes.EIO();
            }
        }

        return super.read(path, buf, size, offset, fi);
    }

    @Override
    public int read_buf(String path, Pointer bufp, @size_t long size, @off_t long off, FuseFileInfo fi) {
        log.trace("read_buf {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            if (!Files.isRegularFile(dataPath)) {
                return -ErrorCodes.EISDIR();
            }

            return readBufInner(path, bufp, size, off, fi);
        }

        return super.read_buf(path, bufp, size, off, fi);
    }

    @Override
    public int readdir(String path, Pointer buf, FuseFillDir filter, long offset, FuseFileInfo fi) {
        log.trace("readdir {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            if (!Files.isDirectory(dataPath)) {
                return -ErrorCodes.ENOTDIR();
            }

            try {
                Set<String> mutateds;
                try (var dataPathsStream = Files.newDirectoryStream(dataPath)) {
                    mutateds = StreamSupport.stream(dataPathsStream.spliterator(), false)
                        .map(p -> p.getFileName().toString())
                        .filter(n -> !DELETED_DIRECTORY.equals(n))
                        .collect(Collectors.toSet());
                }

                var pathDeletes = dataPath.resolve(DELETED_DIRECTORY);
                Set<String> deletes;
                if (Files.exists(pathDeletes)) {
                    try (var pathDeletesStream = Files.newDirectoryStream(pathDeletes)) {
                        deletes = StreamSupport.stream(pathDeletesStream.spliterator(), false)
                            .map(p -> p.getFileName().toString())
                            .collect(Collectors.toSet());
                    }

                } else {
                    deletes = Collections.emptySet();
                }

                var fuseEntry = getTarget().getFuseEntry(path);

                filter.apply(buf, DIRECTORY_ENTRY_SELF, null, 0);
                filter.apply(buf, DIRECTORY_ENTRY_UP, null, 0);

                if (fuseEntry instanceof FuseDirectory) {
                    var targetFileNames = ((FuseDirectory) fuseEntry).getDirectoryEntryNames().collect(Collectors.toSet());
                    targetFileNames.removeAll(deletes);
                    targetFileNames.addAll(mutateds);

                    targetFileNames.stream().sorted().forEach(n -> filter.apply(buf, n, null, 0));
                } else {
                    mutateds.stream().sorted().forEach(n -> filter.apply(buf, n, null, 0));
                }
                return 0;
            } catch (IOException e) {
                log.error("readdir failed. path={} ", path, e);
                return -ErrorCodes.EIO();
            }
        }

        if (isPathDeleteExists(dataPath)) {
            return -ErrorCodes.ENOENT();
        }

        return getTarget().readdir(path, buf, filter, offset, fi);
    }

    @Override
    public int release(String path, FuseFileInfo fi) {
        log.trace("release {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return 0;
        }

        return super.release(path, fi);
    }

    @Override
    public int releasedir(String path, FuseFileInfo fi) {
        log.trace("releasedir {}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            return 0;
        }

        return super.releasedir(path, fi);
    }

    @Override
    public int rename(String oldpath, String newpath) { // NOPMD
        log.trace("rename {} {}", oldpath, newpath);
        var targetDataPath = getDataPath(newpath);
        if (Files.exists(targetDataPath)) {
            return -ErrorCodes.EEXIST();
        }

        var oldDataPath = getDataPath(oldpath);
        if (Files.exists(oldDataPath)) {
            // If it's in data path I might move!
            var success = oldDataPath.toFile().renameTo(targetDataPath.toFile());
            if (success) {
                return 0;
            }
            log.error("rename failed. path={}, target={}", oldpath, newpath);
            return -ErrorCodes.EIO();
        }

        var fuseEntry = getTarget().getFuseEntry(oldpath);
        if (fuseEntry == null) {
            log.warn("rename could not find entry. path={}", oldpath);
            return -ErrorCodes.ENOENT();
        }

        if (fuseEntry instanceof FuseDirectory d) {
            try {
                var sourcePath = Path.of(oldpath);
                Files.createDirectories(targetDataPath);

                var visitPending = new LinkedList<FuseEntry>();
                visitPending.add(d);
                while (!visitPending.isEmpty()) {
                    var popped = visitPending.pop();
                    var poppedPath = popped.getPath();
                    var targetAbsolutePath = targetDataPath.resolve(sourcePath.relativize(Path.of(poppedPath)));
                    if (popped instanceof FuseDirectory fd) {
                        Files.createDirectories(targetAbsolutePath);
                        fd.getDirectoryEntryNames().map(fd::findChild).forEach(visitPending::add);
                    } else if (popped instanceof FuseFile ff) {
                        try (var inputStream = ff.getInputStream()) {
                            createDirectoriesParent(targetAbsolutePath);
                            Files.copy(inputStream, targetAbsolutePath);
                        }
                    }
                }
                writeDeleteFile(oldDataPath);
                return 0;
            } catch (IOException e) {
                log.error("rename by write failed. path={}", oldDataPath, e);
                return -ErrorCodes.EIO();
            }
        }

        if (fuseEntry instanceof FuseFile f) {
            // If it's in original data and not in dataPath I might just do copy + remove
            try (var inputStream = f.getInputStream()) {
                createDirectoriesParent(targetDataPath);
                Files.copy(inputStream, targetDataPath);
                writeDeleteFile(oldDataPath);
                return 0;
            } catch (IOException e) {
                log.error("rename by write failed. path={}", oldDataPath, e);
                return -ErrorCodes.EIO();
            }
        }

        log.warn("rename could not find valid entry. path={}, target={}", oldpath, newpath);
        // Maybe only apply if I get an error from super
        return super.rename(oldpath, newpath);
    }

    @Override
    public int rmdir(String path) { // NOPMD
        log.trace("rmdir {}", path);
        var dataPath = getDataPath(path);
        try {
            if (!dataPath.startsWith(mutatedDataPath)) {
                log.error("rmdir failed. Pathg does not start with mutated. path={}, mutatedPath={}", dataPath, mutatedDataPath);
                return -ErrorCodes.EACCES();
            }

            Set<String> deletes = Collections.emptySet();
            Set<String> mutateds = Collections.emptySet();
            var exists = Files.exists(dataPath);
            if (exists) {
                if (!Files.isDirectory(dataPath)) {
                    return -ErrorCodes.ENOTDIR();
                }
                try (var pathDirectoriesStream = Files.newDirectoryStream(dataPath)) {
                    mutateds = StreamSupport.stream(pathDirectoriesStream.spliterator(), false)
                        .map(p -> p.getFileName().toString())
                        .filter(n -> !DELETED_DIRECTORY.equals(n))
                        .collect(Collectors.toSet());
                }
                var pathDeletes = dataPath.resolve(DELETED_DIRECTORY);
                if (Files.exists(pathDeletes)) {
                    try (var pathDeletesStream = Files.newDirectoryStream(pathDeletes)) {
                        deletes = StreamSupport.stream(pathDeletesStream.spliterator(), false).map(p -> p.getFileName().toString()).collect(Collectors.toSet());
                    }
                }
            }

            var fuseEntry = getTarget().getFuseEntry(path);
            if (fuseEntry == null) {
                return -ErrorCodes.ENOENT();
            }

            boolean deleteReady;
            if (fuseEntry instanceof FuseDirectory d) {
                var targetFileNames = d.getDirectoryEntryNames().collect(Collectors.toSet());
                targetFileNames.removeAll(deletes);
                targetFileNames.addAll(mutateds);
                deleteReady = targetFileNames.isEmpty();
            } else { // Is file
                deleteReady = mutateds.isEmpty();
            }

            if (deleteReady) {
                if (exists) {
                    FileUtils.deleteDirectory(dataPath.toFile());
                }
                writeDeleteFile(dataPath);
                return 0;
            }

            log.error("rmdir failed. Could not find delete ready. path={}", path);
            return -ErrorCodes.EACCES();
        } catch (IOException e) {
            log.error("rmdir failed. path={} ", path, e);
            return -ErrorCodes.EIO();
        }

    }

    @Override
    public int truncate(String path, long offset) {
        log.trace("truncate {}", path);
        var dataPath = getDataPath(path);

        try {
            if (Files.exists(dataPath)) {
                if (!Files.isRegularFile(dataPath)) {
                    return -ErrorCodes.EISDIR();
                }
                truncate(dataPath, offset);
                return 0;
            }

            var fuseEntry = getTarget().getFuseEntry(path);
            if (fuseEntry == null) {
                return -ErrorCodes.ENOENT();
            }

            if (fuseEntry instanceof FuseDirectory) {
                return -ErrorCodes.EISDIR();
            }

            if (fuseEntry instanceof FuseFile f) {
                try (var inputStream = f.getInputStream()) {
                    createDirectoriesParent(dataPath);
                    Files.copy(inputStream, dataPath);
                    truncate(dataPath, offset);
                    return 0;
                }
            }

            log.error("truncate failed. Unsupported fuse entry type??. path={}, entry={}", path, fuseEntry);
            return -ErrorCodes.EIO();
        } catch (IOException e) {
            log.error("truncate failed. path={} ", path, e);
            return -ErrorCodes.EIO();
        }

    }

    @Override
    public int unlink(String path) {
        // Called when file 'leaves' the filesystem
        log.trace("unlink {}", path);
        var dataPath = getDataPath(path);
        try {
            if (Files.exists(dataPath) && dataPath.startsWith(mutatedDataPath)) {
                if (!Files.isRegularFile(dataPath)) {
                    return -ErrorCodes.EISDIR();
                }
                Files.delete(dataPath);
                return 0;
            }

            var fuseEntry = getTarget().getFuseEntry(path);
            if (fuseEntry instanceof FuseDirectory) {
                return -ErrorCodes.EISDIR();
            }
            if (fuseEntry instanceof FuseFile) {
                writeDeleteFile(dataPath);
                return 0;
            }
            return -ErrorCodes.ENOENT();
        } catch (IOException e) {
            log.error("unlink (delete) failed. path={}", path, e);
            return -ErrorCodes.EIO();
        }
    }

    @Override
    public int write(String path, Pointer buf, @size_t long size, @off_t long offset, FuseFileInfo fi) {
        log.trace("write {}", path);
        var dataPath = getDataPath(path);
        try {
            if (Files.exists(dataPath)) {
                if (!Files.isRegularFile(dataPath)) {
                    return -ErrorCodes.EISDIR();
                }
                return writeInner(dataPath, buf, (int) size, offset);
            } else {
                var fuseEntry = getTarget().getFuseEntry(path);
                if (fuseEntry == null) {
                    return -ErrorCodes.ENOENT();
                }
                if (fuseEntry instanceof FuseDirectory) {
                    return -ErrorCodes.EISDIR();
                }
                if (fuseEntry instanceof FuseFile f) {
                    try (var inputStream = f.getInputStream()) {
                        createDirectoriesParent(dataPath);
                        Files.copy(inputStream, dataPath);
                        return writeInner(dataPath, buf, (int) size, offset);
                    }
                }
            }
        } catch (IOException e) {
            log.error("write failed. path={}", dataPath, e);
            return -ErrorCodes.EIO();
        }

        return super.write(path, buf, size, offset, fi);
    }

    public void writeDeleteFile(Path dataPath) throws IOException {
        var pathDeleted = getPathDeleted(dataPath);
        createDirectoriesParent(pathDeleted);
        Files.newByteChannel(pathDeleted, StandardOpenOption.CREATE, StandardOpenOption.WRITE).close();
    }

    public int writeInner(Path dataPath, Pointer buf, @size_t int size, @off_t long offset) throws IOException {
        try (var sbc = Files.newByteChannel(dataPath, StandardOpenOption.WRITE)) {
            var data = new byte[size];
            buf.get(0, data, 0, size);

            var bf = ByteBuffer.wrap(data);
            buf.put(0, bf.array(), 0, size);
            sbc.position(offset);
            sbc.write(bf);
            return size;
        }
    }

    @Override
    @NotImplemented
    public int write_buf(String path, FuseBufvec buf, @off_t long off, FuseFileInfo fi) {
        log.trace("write_buf path={}", path);
        var dataPath = getDataPath(path);
        if (Files.exists(dataPath)) {
            if (!Files.isRegularFile(dataPath)) {
                return -ErrorCodes.EISDIR();
            }
            return writeBufInner(path, buf, off, fi);
        }
        return super.write_buf(path, buf, off, fi);
    }
}
