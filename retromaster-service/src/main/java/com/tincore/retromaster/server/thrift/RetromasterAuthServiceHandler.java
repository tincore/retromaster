package com.tincore.retromaster.server.thrift;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.util.security.jwt.JwtTokenService;
import com.tincore.retromaster.domain.RetromasterUser;
import com.tincore.retromaster.service.UserService;
import com.tincore.retromaster.thrift.RetromasterAuthService;
import com.tincore.retromaster.thrift.form.TUser;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class RetromasterAuthServiceHandler implements RetromasterAuthService.Iface {

    public static final TUser ANONYNOUS_T_USER = new TUser().setName("").setAuthorities(Collections.emptyList());

    private final UserService userService;
    private final JwtTokenService jwtTokenService;

    @Value("${tincore.sec.jwt.claimUsername:" + Claims.SUBJECT + "}")
    private final String jwtClaimUserName = Claims.SUBJECT;

    @Override
    public String doLogin(String username, String password) {
        var user = userService.getUsers()
            .stream()
            .filter(RetromasterUser::isWeb)
            .filter(u -> u.getUsername().equals(username))
            .filter(u -> u.getPassword().equals(password))
            .findFirst();
        return user.map(
            u -> {
                Map<String, Object> claims = new HashMap<>();
                claims.put(jwtClaimUserName, username);
                claims.put("role", u.getRole());
                return jwtTokenService.createToken(claims);
            })
            .orElse("");
    }

    @Override
    // @PreAuthorize("isAuthenticated()")
    public TUser getUserMe() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication().getPrincipal())
            .filter(p -> p instanceof UserDetails)
            .map(p -> (UserDetails) p)
            .map(u -> new TUser().setName(u.getUsername()).setAuthorities(u.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())))
            .orElse(ANONYNOUS_T_USER);
    }
}
