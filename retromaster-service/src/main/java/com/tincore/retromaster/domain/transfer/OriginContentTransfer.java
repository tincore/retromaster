package com.tincore.retromaster.domain.transfer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentMetadataSourceStorageStrategy;

import java.nio.file.Path;

public class OriginContentTransfer extends ContentTransfer {

    private final Path targetFileAbsolutePath;
    private final int originContentCount;
    private final ContentMetadataSourceStorageStrategy contentMetadataSourceStorageStrategy;

    private boolean importedFlag;
    private boolean managedByTransferFlag;
    private boolean deleteContentFlag;

    public OriginContentTransfer(Content content, ContentMetadataItem contentMetadataItem, int originContentCount, Path targetFileAbsolutePath, ContentMetadataSourceStorageStrategy contentMetadataSourceStorageStrategy) {
        super(content, contentMetadataItem);
        this.targetFileAbsolutePath = targetFileAbsolutePath;
        this.originContentCount = originContentCount;
        this.contentMetadataSourceStorageStrategy = contentMetadataSourceStorageStrategy;
    }

    public OriginContentTransfer(Content content, ContentMetadataItem contentMetadataItem, int originContentCount, Path targetFileAbsolutePath, boolean deleteContentFlag, ContentMetadataSourceStorageStrategy contentMetadataSourceStorageStrategy) {
        this(content, contentMetadataItem, originContentCount, targetFileAbsolutePath, contentMetadataSourceStorageStrategy);
        this.deleteContentFlag = deleteContentFlag;
    }

    public void addDeleteContentFlag() {
        this.deleteContentFlag = true;
    }

    public void addImportedFlag() {
        this.importedFlag = true;
    }

    public void addManagedByTransferFlag() {
        managedByTransferFlag = true;
    }

    public ContentMetadataSourceStorageStrategy getContentMetadataSourceStorageStrategy() {
        return contentMetadataSourceStorageStrategy;
    }

    public int getOriginContentCount() {
        return originContentCount;
    }

    public Path getOriginFileAbsolutePath() {
        return getContent().getPathFileAbsolute();
    }

    public Path getTargetFileAbsolutePath() {
        return targetFileAbsolutePath;
    }

    public boolean isContentDeletable() {
        return isDeleteContentFlag() && getContent().getId() != null;
    }

    public boolean isDeleteContentFlag() {
        return deleteContentFlag;
    }

    public boolean isImportedFlag() {
        return importedFlag;
    }

    public boolean isManagedByTransferFlag() {
        return managedByTransferFlag;
    }
}
