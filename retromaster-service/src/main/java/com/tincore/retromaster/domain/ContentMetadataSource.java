package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.util.function.Predicate;

public enum ContentMetadataSource {
    REDUMP(
        800,
        c -> StringUtils.containsIgnoreCase(c.getAuthor(), "redump")
             || StringUtils.endsWithIgnoreCase(c.getRepositoryPath(), "REDUMP"),
        true,
        ContentMetadataSourceStorageStrategy.ARCHIVE_QUICK
    ),
    TOSEC(
        700,
        c -> StringUtils.equalsIgnoreCase(c.getCategory(), "TOSEC") || StringUtils.endsWithIgnoreCase(c.getRepositoryPath(), "TOSEC"),
        true,
        ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT
    ),
    NOINTRO(600,
        c -> StringUtils.containsIgnoreCase(c.getUrl(), "No-Intro")
             || StringUtils.equalsIgnoreCase(c.getCategory(), "NOINTRO")
             || StringUtils.endsWithIgnoreCase(c.getRepositoryPath(), "NOINTRO"),
        true,
        ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT
    ),
    TOSEC_ISO(
        500,
        c -> StringUtils.equalsIgnoreCase(c.getCategory(), "TOSEC-ISO"),
        false,
        ContentMetadataSourceStorageStrategy.ARCHIVE_QUICK
    ),
    TOSEC_PIX(
        100,
        c -> StringUtils.equalsIgnoreCase(c.getCategory(), "TOSEC-PIX"),
        false,
        ContentMetadataSourceStorageStrategy.FLAT
    ),
    OPENGOOD(
        490,
        c -> StringUtils.equalsIgnoreCase(c.getAuthor(), "OpenGood") || StringUtils.endsWithIgnoreCase(c.getRepositoryPath(), "OPENGOOD"),
        true,
        ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT
    ),
    TDC(
        450,
        c -> StringUtils.endsWithIgnoreCase(c.getRepositoryPath(), "TDC"),
        false,
        ContentMetadataSourceStorageStrategy.ARCHIVE_QUICK
    ),
    MAME(350, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    TRURIP(340, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    GOODSET(330, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    NONGOOD(320, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    EP128HU(313, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    COMPUTER_EMUZONE(312, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    VIZZED(311, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    ALEX_VAMPIRE(310, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    MAYBEINTRO(300, c -> false, true, ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT), // 2nd level derived source
    SABRETOOLS_MERGED(
        200,
        c -> StringUtils.equalsIgnoreCase(c.getCategory(), "SABRETOOLS_MERGED")
             || StringUtils.containsIgnoreCase(c.getCategory(), "SabreTools") && StringUtils.containsIgnoreCase(c.getName(), "(merged"),
        true,
        ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT
    ),
    WIZARD_OF_DATZ_MERGED(
        199,
        c -> StringUtils.equalsIgnoreCase(c.getCategory(), "WIZARD_OF_DATZ_MERGED")
             || StringUtils.containsIgnoreCase(c.getCategory(), "The Wizard of Datz") && StringUtils.containsIgnoreCase(c.getName(), "(merged"),
        true,
        ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT
    ),
    LIBRETRO_PIC(
        450,
        c -> StringUtils.endsWithIgnoreCase(c.getRepositoryPath(), "LIBRETRO_PIC"),
        false,
        ContentMetadataSourceStorageStrategy.FLAT
    ),
    RETROMASTER_PIC(
        450,
        c -> StringUtils.endsWithIgnoreCase(c.getRepositoryPath(), "RETROMASTER_PIC"),
        false,
        ContentMetadataSourceStorageStrategy.FLAT
    ),
    UNKNOWN(
        -1,
        c -> false,
        false,
        ContentMetadataSourceStorageStrategy.ARCHIVE_DEFAULT
    );

    private final Predicate<ContentMetadataSet> matchPredicate;

    private final int precedence;
    private final boolean multipleContentMetadatasCanBelongToSameTitle;
    private final ContentMetadataSourceStorageStrategy contentMetadataSourceStorageStrategy;


    ContentMetadataSource(int precedence, Predicate<ContentMetadataSet> matchPredicate, boolean multipleContentMetadatasCanBelongToSameTitle, ContentMetadataSourceStorageStrategy contentMetadataSourceStorageStrategy) {
        this.precedence = precedence;
        this.matchPredicate = matchPredicate;
        this.multipleContentMetadatasCanBelongToSameTitle = multipleContentMetadatasCanBelongToSameTitle;
        this.contentMetadataSourceStorageStrategy = contentMetadataSourceStorageStrategy;
    }


    public ContentMetadataSourceStorageStrategy getContentMetadataSourceStorageStrategy() {
        return contentMetadataSourceStorageStrategy;
    }


    public int getPrecedence() {
        return precedence;
    }

    public boolean isMatch(ContentMetadataSet contentMetadataSet) {
        return matchPredicate.test(contentMetadataSet);
    }

    public boolean isMultipleContentMetadatasCanBelongToSameTitle() {
        return multipleContentMetadatasCanBelongToSameTitle;
    }
}

// Precedence based on category precedence and name descending (1st most recent file)
