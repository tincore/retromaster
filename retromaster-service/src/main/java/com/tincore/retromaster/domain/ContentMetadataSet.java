package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.ApplicationConfiguration;
import com.tincore.util.UuidUtil;
import com.tincore.util.jpa.JpaDataTypeHelper;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.engine.backend.types.Aggregable;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static com.tincore.retromaster.config.ApplicationLuceneAnalysisConfigurer.ANALYZER_STANDARD_EDGE_N_GRAM;
import static com.tincore.retromaster.config.ApplicationLuceneAnalysisConfigurer.NORMALIZER_LOWERCASE;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(
    indexes = {@Index(name = "idx_contmetaset_name", columnList = "name"), @Index(name = "idx_contmetaset_category", columnList = "category"), @Index(name = "idx_contmetaset_filename", columnList = "filename")},
    uniqueConstraints = {
        @UniqueConstraint(
            name = "uc_contmetaset_filename",
            columnNames = {"filename"})
    })
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ContentMetadataSet {

    public static final int AUTHOR_LENGTH = 1024;
    public static final int NAME_LENGTH = 1024;
    public static final int REPOSITORY_PATH_LENGTH = 1024;
    public static final int FILENAME_LENGTH = 1024;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = JpaDataTypeHelper.TYPE_UUID)
    @ToString.Include
    @GenericField(name = "id") // Hibernate search 6 does not include Ids by default
    private UUID id;

    /**
     * Should be renamed to source.
     *
     * <p>
     * Will not be the same source as content metadatas when set is merged type.
     */
    @GenericField(name = "category", aggregable = Aggregable.YES)
    @Column(nullable = false)
    @ToString.Include
    private String category;

    @FullTextField(analyzer = ANALYZER_STANDARD_EDGE_N_GRAM)
    @KeywordField(name = "name_na", sortable = Sortable.YES, normalizer = NORMALIZER_LOWERCASE)
    @Column(nullable = false, length = NAME_LENGTH)
    @ToString.Include
    private String name;

    @Column(nullable = false)
    @ToString.Include
    private String version;

    @Column
    private String tagsExtra;

    private String description;

    @Column(length = AUTHOR_LENGTH)
    private String author;

    @Column
    private String email;

    @Column
    private String url;

    @Column(nullable = false, length = REPOSITORY_PATH_LENGTH)
    @ToString.Include
    private String repositoryPath;

    @Column(nullable = false, length = FILENAME_LENGTH)
    @ToString.Include
    private String filename;

    @OneToMany(mappedBy = "contentMetadataSet", cascade = CascadeType.REMOVE)
    @OrderBy("title ASC")
    private List<ContentMetadata> contentMetadatas;

    @GenericField
    @Column
    @ToString.Include
    @EqualsAndHashCode.Include
    private boolean staging;

    @Column
    private boolean flattenable;

    @Column
    private LocalDateTime checkTime;

    @Column
    private LocalDateTime checkContentTime;

    public static UUID fromStringFileUnique(String name) {
        var subs = StringUtils.substringsBetween(name, "[", "]");
        var value = subs[subs.length - 1];
        return UuidUtil.fromString64(value);
    }

    public static String getNamePrefix(String name) {
        return name.substring(0, 1).toUpperCase();
    }

    public static String toStringFile(String repositoryPath, boolean staging) {
        return StringUtils.replaceChars(repositoryPath, "/\\", "") + (staging ? " STAG" : "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var other = (ContentMetadataSet) o;

        return id != null && id.equals(other.id);
    }

    public String getPrefix() {
        return getNamePrefix(name);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public String toStringFile() {
        return toStringFile(getRepositoryPath(), staging);
    }

    public String toStringFileUnique() {
        return StringUtils.replaceChars(getRepositoryPath(), "/\\", "") + " [" + UuidUtil.toString64(getId()) + "]";
    }
}
