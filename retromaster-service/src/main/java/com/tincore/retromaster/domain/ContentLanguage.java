package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.tuple.Pair.of;

public enum ContentLanguage {
    ar("Arabic"),
    bg("Bulgarian"),
    bs("Bosnian"),
    cs("Czech"),
    cy("Welsh"),
    da("Danish"),
    de("German"),
    el("Greek"),
    en("English"),
    eo("Esperanto"),
    es("Spanish"),
    et("Estonian"),
    fa("Persian"),
    fi("Finnish"),
    fr("French"),
    ga("Irish"),
    gu("Gujarati"),
    he("Hebrew"),
    hi("Hindi"),
    hr("Croatian"),
    hu("Hungarian"),
    is("Icelandic"),
    it("Italian"),
    ja("Japanese"),
    ko("Korean"),
    lt("Lithuanian"),
    lv("Latvian"),
    ms("Malay"),
    nl("Dutch"),
    no("Norwegian"),
    pl("Polish"),
    pt("Portuguese"),
    ro("Romanian"),
    ru("Russian"),
    sk("Slovakian"),
    sl("Slovenian"),
    sq("Albanian"),
    sr("Serbian"),
    sv("Swedish"),
    th("Thai"),
    tr("Turkish"),
    ur("Urdu"),
    vi("Vietnamese"),
    yi("Yiddish"),
    zh("Chinese");

    public static final Set<String> SET = Stream.of(ContentLanguage.values()).map(Enum::name).collect(Collectors.toSet());

    public static final Map<String, String> MAP = Stream.of(ContentLanguage.values())
        .map(
            v -> {
                var name = v.name();
                return Stream.of(of(name, name), of(StringUtils.capitalize(name), name), of(v.description, name));
            })
        .flatMap(s -> s)
        .collect(Collectors.toMap(Pair::getKey, Pair::getValue));


    private final String description;

    ContentLanguage(String description) {
        this.description = description;
    }

    public static String normalize(String language) {
        return language.toLowerCase();
    }

    public String getDescription() {
        return description;
    }
}
