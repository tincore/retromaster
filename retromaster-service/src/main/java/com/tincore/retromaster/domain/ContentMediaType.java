package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

public enum ContentMediaType {
    UNKNOWN,
    DISC,
    DISK,
    FILE,
    PART,
    SIDE,
    TAPE,

    PIC_SNAPSHOT,
    PIC_TITLE,
    PIC_BOXART,

    DOC_MANUAL,
    DOC_MAGAZINE;


    public static final List<ContentMediaType> PIC = List.of(PIC_SNAPSHOT, PIC_TITLE, PIC_BOXART);
    public static final List<ContentMediaType> DOC = List.of(DOC_MAGAZINE, DOC_MANUAL);

}
