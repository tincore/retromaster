package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.ApplicationConfiguration;
import com.tincore.util.jpa.JpaDataTypeHelper;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(
       uniqueConstraints = {
           @UniqueConstraint(
                             name = "uc_covo_path",
                             columnNames = {"path"})
       })
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ContentVolume {

    public static final int PATH_LENGTH = 1024;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = JpaDataTypeHelper.TYPE_UUID)
    @ToString.Include
    @EqualsAndHashCode.Include
    private UUID id;

    @Column
    @ToString.Include
    @EqualsAndHashCode.Include
    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @ToString.Include
    @EqualsAndHashCode.Include
    private ContentVolumeEnvironment environment;

    @Column(nullable = false, length = PATH_LENGTH)
    @EqualsAndHashCode.Include
    private String path;

    @Column
    @EqualsAndHashCode.Include
    private int precedence;

//    @OneToMany(mappedBy = "volume", cascade = CascadeType.REMOVE, orphanRemoval = false)
    @OneToMany(mappedBy = "volume")
    private List<Content> contents;

    public ContentVolume(String name, Path path, ContentVolumeEnvironment environment) {
        this.name = name;
        this.path = path.toString();
        this.environment = environment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var other = (ContentVolume) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public boolean isStage() {
        return ContentVolumeEnvironment.STAGE.equals(this.environment);
    }

    public Path toPath() {
        return Paths.get(path);
    }
}
