package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.FilenameUtils;

import java.nio.file.Path;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentExtensionType.*;

public enum ContentExtension {
    tos(FIRM),

    bin(CART, FIRM),
    rom(CART, FIRM),

    // Cart
    _80("80", CART),
    _60("60", CART),
    _32x("32x", CART),
    _3ds("3ds", CART),
    a0(CART),
    e0(CART),
    a26(CART),
    a52(CART),
    a78(CART),
    chf(CART),
    col(CART),
    cpr(CART),
    gb(CART),
    gbc(CART),
    gba(CART),
    gbp(CART),
    j64(CART),
    jrc(CART),
    lnx(CART),
    lyx(CART),
    m5(CART),
    m7(CART),
    nes(CART),
    n64(CART),
    ndd(CART),
    pce(CART),
    sgx(CART),
    sfc(CART),
    md(CART),
    smd(CART),
    sms(CART),
    gen(CART),
    gg(CART),
    kcr(CART),
    mx1(CART),
    mx2(CART),
    vb(CART),
    wad(CART),
    ws(CART),
    wsc(CART),
    z64(CART),

    pkg(CART),

    // disk
    _1dd("1dd", DISK),
    _2mg("2mg", DISK),
    _360("360", DISK),
    _88d("88d", DISK),
    adf(DISK),
    adl(DISK),
    adm(DISK),
    ads(DISK),
    atr(DISK),
    atx(DISK),
    bbc(DISK),
    cqi(DISK),
    cqm(DISK),
    dfi(DISK),
    dim(DISK),
    dup(DISK),
    _do("do", DISK),
    d64(DISK),
    d77(DISK),
    d81(DISK),
    d88(DISK),
    dsd(DISK),
    dsk(DISK),
    dmk(DISK),
    fd(DISK),
    fdd(DISK),
    g64(DISK),
    hfe(DISK),
    imd(DISK),
    ipf(DISK),
    ima(DISK),
    img(DISK),
    mdi(DISK),
    mfi(DISK),
    mfm(DISK),
    msa(DISK),
    nib(DISK),
    pdi(DISK),
    po(DISK),
    sap(DISK),
    ssd(DISK),
    st(DISK),
    stx(DISK),
    td0(DISK),
    ufi(DISK),
    xdf(DISK),

    // tape
    cas(TAPE),
    cdt(TAPE),
    csw(TAPE),
    k7(TAPE),
    t64(TAPE, QUICK),
    tap(TAPE),
    tzx(TAPE),
    uef(TAPE),
    wav(TAPE),

    // snap
    ace(SNAP),
    sna(SNAP),
    z80(SNAP),

    chd(DISC),
    cbn(DISC),
    cue(DISC),
    ccd(DISC),
    cdi(DISC),
    cso(DISC),
    ciso(DISC),
    cdr(DISC),
    gdi(DISC),
    iso(DISC),
    isz(DISC),
    mdf(DISC),
    mds(DISC),
    nrg(DISC),
    pbp(DISC),
    toc(DISC),
    wbfs(DISC),

    cia(DISC),

    hdf(HD),
    hdm(HD),
    _2hd("2hd", HD),

    p00(QUICK),
    prg(QUICK),
    bas(QUICK),
    elf(QUICK),
    exe(QUICK),
    bat(QUICK),
    com(QUICK),
    cmd(QUICK),

    // ???
    xex(),
    prx(),


    jpg(),
    png(),

    raw(),

    m3u(LIST);

    public static final Set<ContentExtension> DISC_SET = Stream.of(ContentExtension.values()).filter(ContentExtension::isDisc).collect(Collectors.toSet());

    private static final Map<String, ContentExtension> MAP = Stream.of(ContentExtension.values()).collect(Collectors.toMap(Enum::name, Function.identity()));
    private static final Set<String> DISK_STRING_SET = toValuesSet(ContentExtension::isDisk);
    private static final Set<String> TAPE_STRING_SET = toValuesSet(ContentExtension::isTape);
    private static final Set<String> CART_STRING_SET = toValuesSet(ContentExtension::isCart);
    private static final Set<String> FIRM_STRING_SET = toValuesSet(ContentExtension::isFirm);
    private static final Set<String> SNAP_STRING_SET = toValuesSet(ContentExtension::isSnap);
    private static final Set<String> QUICK_STRING_SET = toValuesSet(ContentExtension::isSnap);
    private static final Set<String> DISC_STRING_SET = toValuesSet(ContentExtension::isDisc);
    private static final Set<String> HD_STRING_SET = toValuesSet(ContentExtension::isHd);

    private final String value;
    private final int type;

    ContentExtension(String value, ContentExtensionType... types) {
        this.value = value;
        this.type = toMask(types);
    }

    ContentExtension(ContentExtensionType... types) {
        this.value = this.name();
        this.type = toMask(types);
    }

    public static ContentExtension findByFilename(String path) {
        return MAP.get(getExtensionFormatted(path));
    }

    public static String getExtensionFormatted(String path) {
        return FilenameUtils.getExtension(path).toLowerCase();
    }

    public static boolean isCart(String path) {
        return isExtension(path, CART_STRING_SET);
    }

    public static boolean isDisc(String path) {
        return isExtension(path, DISC_STRING_SET);
    }

    public static boolean isDisk(Path path) {
        return isDisk(path.toString());
    }

    public static boolean isDisk(String path) {
        return isExtension(path, DISK_STRING_SET);
    }

    private static boolean isExtension(String path, Set<String> extensions) {
        return extensions.contains(getExtensionFormatted(path));
    }

    public static boolean isFirm(String path) {
        return isExtension(path, FIRM_STRING_SET);
    }

    public static boolean isHd(String path) {
        return isExtension(path, HD_STRING_SET);
    }

    public static boolean isQuick(String path) {
        return isExtension(path, QUICK_STRING_SET);
    }

    public static boolean isSnap(String path) {
        return isExtension(path, SNAP_STRING_SET);
    }

    public static boolean isTape(String path) {
        return isExtension(path, TAPE_STRING_SET);
    }

    private static Set<String> toValuesSet(Predicate<ContentExtension> pred) {
        return Stream.of(ContentExtension.values()).filter(pred).map(ContentExtension::getValue).collect(Collectors.toSet());
    }

    public String getValue() {
        return value;
    }

    public boolean isCart() {
        return (type & CART.getMask()) > 0;
    }

    public boolean isDisc() {
        return (type & DISC.getMask()) > 0;
    }

    public boolean isDisk() {
        return (type & DISK.getMask()) > 0;
    }

    public boolean isExtensionOf(String path) {
        return getValue().equalsIgnoreCase(getExtensionFormatted(path));
    }

    public boolean isFirm() {
        return (type & FIRM.getMask()) > 0;
    }

    public boolean isHd() {
        return (type & HD.getMask()) > 0;
    }

    public boolean isQuick() {
        return (type & QUICK.getMask()) > 0;
    }

    public boolean isSnap() {
        return (type & SNAP.getMask()) > 0;
    }

    public boolean isTape() {
        return (type & TAPE.getMask()) > 0;
    }
}
