package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.tuple.Pair.of;

public enum ContentRegion {
    WO("World", "Wo", "W"),
    UNK("Unknown", "Unk"),

    AE("United Arab Emirates"),
    AL("Albania"),
    AS("Asia", "As"),
    AT("Austria"),
    AU("Australia"),
    BA("Bosnia and Herzegovina"),
    BE("Belgium"),
    BG("Bulgaria"),
    BR("Brazil", "B"),
    CA("Canada", "C"),
    CH("Switzerland"),
    CL("Chile"),
    CN("China", "Ch"),
    CS("Serbia and Montenegro"),
    CY("Cyprus"),
    CZ("Czech Republic"),
    DE("Germany", "GER"),
    DK("Denmark"),
    EE("Estonia"),
    EG("Egypt"),
    ES("Spain", "ESP", "S"),
    EU("Europe", "EUR", "E"),
    FI("Finland"),
    FR("France", "FRA"),
    GB("United Kingdom", "UK"),
    GR("Greece", "Gr"),
    HK("Hong Kong"),
    HR("Croatia"),
    HU("Hungary"),
    ID("Indonesia"),
    IE("Ireland"),
    IL("Israel"),
    IN("India"),
    IR("Iran"),
    IS("Iceland"),
    IT("Italy", "I"),
    JO("Jordan"),
    JP("Japan", "JAP", "J"),
    KR("South Korea", "Korea"),
    LT("Lithuania"),
    LU("Luxembourg"),
    LV("Latvia"),
    MN("Mongolia"),
    MX("Mexico"),
    MY("Malaysia"),
    NL("Netherlands", "Nl"),
    NO("Norway"),
    NP("Nepal"),
    NZ("New Zealand"),
    OM("Oman"),
    PE("Peru"),
    PH("Philippines"),
    PL("Poland"),
    PT("Portugal"),
    QA("Qatar"),
    RO("Romania"),
    RU("Russia", "Ru"),
    SE("Sweden", "Sw"),
    SG("Singapore"),
    SI("Slovenia"),
    SK("Slovakia"),
    TH("Thailand"),
    TR("Turkey"),
    TW("Taiwan"),
    US("United States", "USA", "U"),
    VN("Vietnam"),
    YU("Yugoslavia"),
    ZA("South Africa");

    public static final Set<String> SET = Stream.of(ContentRegion.values()).map(Enum::name).collect(Collectors.toSet());

    public static final Map<String, String> MAP = Stream.of(ContentRegion.values())
        .map(
            v -> {
                var name = v.name();
                var base = Stream.of(of(name, name), of(v.description, name));
                return Optional.ofNullable(v.altCode).map(altCodes -> Stream.concat(base, Stream.of(altCodes).map(aC -> of(aC, name)))).orElse(base);
            })
        .flatMap(s -> s)
        .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

    private final String description;
    private final String[] altCode;

    ContentRegion(String description) {
        this(description, null);
    }

    ContentRegion(String description, String... altCode) {

        this.description = description;
        this.altCode = altCode;
    }
}
