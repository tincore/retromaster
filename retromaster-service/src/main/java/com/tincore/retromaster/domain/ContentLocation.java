package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContentLocation {
    private String directoryPath;
    private String fileName;
    private LocalDateTime modifiedTime;
    private long fileSize;

    public static boolean isFileModifiedTimeEquals(Path path, ContentLocation contentLocation) throws IOException {
        return isFileModifiedTimeEquals(path, contentLocation.getModifiedTime());
    }

    // Move to FS trait and replace wit getLastModifiedTime()
    public static boolean isFileModifiedTimeEquals(Path path, LocalDateTime modifiedTime) throws IOException {
        // Cant do equals as fileTime does not have millis on some FS
        return Files.exists(path) && isFileModifiedTimeEquals(modifiedTime, LocalDateTime.ofInstant(Files.getLastModifiedTime(path).toInstant(), ZoneId.systemDefault()));
    }

    public static boolean isFileModifiedTimeEquals(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return localDateTime1.toEpochSecond(ZoneOffset.UTC) == localDateTime2.toEpochSecond(ZoneOffset.UTC);
    }

    public boolean isFileModifiedTimeEquals(Path path) {
        try {
            return isFileModifiedTimeEquals(path, this);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
