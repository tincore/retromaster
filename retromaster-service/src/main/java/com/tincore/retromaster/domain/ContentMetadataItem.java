package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.ApplicationConfiguration;
import com.tincore.util.jpa.JpaDataTypeHelper;
import lombok.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import jakarta.persistence.*;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
@Entity
@Table(
       indexes = {
           @Index(name = "idx_contmetaitem_signature", columnList = "signature"),
       })
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ContentMetadataItem {

    public static final int NAME_LENGTH = 1024;
    private static final String MISSING = "_";

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = JpaDataTypeHelper.TYPE_UUID)
    @ToString.Include
    private UUID id;

    @Column(nullable = false, length = NAME_LENGTH)
    @ToString.Include
    private String name;

    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private long size;

    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private String sha1;

    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private String md5;

    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private String crc;

    @KeywordField
    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    @ToString.Include
    private String signature;

    @ManyToOne(optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_contmetaitem_contmeta"))
    private ContentMetadata contentMetadata;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var other = (ContentMetadataItem) o;

        return id != null && id.equals(other.id);
    }

    public String getExtension() {
        return FilenameUtils.getExtension(name);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public boolean isExtension(String extension) {
        return getExtension().equals(extension);
    }

    public void setSignature(String crc, String md5, String sha1, long size) {
        var crcFormatted = StringUtils.isNotBlank(crc) ? crc.toUpperCase() : MISSING;
        this.crc = crcFormatted;

        var md5Formatted = StringUtils.isNotBlank(md5) ? md5.toUpperCase() : MISSING;
        this.md5 = md5Formatted;

        var sha1Formatted = StringUtils.isNotBlank(sha1) ? sha1.toUpperCase() : MISSING;
        this.sha1 = sha1Formatted;

        this.size = size;
        this.signature = crcFormatted + md5Formatted + sha1Formatted + size;
    }

    public String toStringFile() {
        var separatorIndex = name.lastIndexOf('/');
        var fileName = separatorIndex < 0 ? name : name.substring(separatorIndex + 1);
        return StringUtils.replaceChars(fileName, "/\\", "");
    }
}
