package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.ApplicationConfiguration;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.jpa.JpaDataTypeHelper;
import lombok.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.engine.backend.types.Aggregable;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import jakarta.persistence.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.UUID;

import static com.tincore.retromaster.domain.ContentLocation.isFileModifiedTimeEquals;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(
    indexes = {
        @Index(name = "idx_cont_directory_path", columnList = "directoryPath"),
        @Index(name = "idx_cont_file_name", columnList = "fileName"),
        @Index(name = "idx_cont_check_time", columnList = "checkTime"),
        @Index(name = "idx_cont_signature", columnList = "signature")
    },
    uniqueConstraints = {
        @UniqueConstraint(
            name = "uc_cont",
            columnNames = {"volume_id", "directoryPath", "fileName", "fileEntry"})
    })
@Indexed
public class Content implements FileSystemTrait {

    public static final String COMPRESS_SEPARATOR = "#/";
    public static final int DIRECTORY_PATH_LENGTH = 1024;
    public static final int FILENAME_LENGTH = 1024;
    public static final int FILE_ENTRY_LENGTH = 1024;

    public static final String LONGFILE_SIGNATURE_PREFIX = "LF:";

    public static final String FIELD_DIRECTORY_PATH = "directoryPath";
    public static final String FIELD_FILE_NAME = "fileName";
    public static final String FIELD_FILE_ENTRY = "fileEntry";

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = JpaDataTypeHelper.TYPE_UUID)
    @ToString.Include
    private UUID id;

    // -----

    @ManyToOne(optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_cont_contvolu"))
    private ContentVolume volume;

    @GenericField(name = FIELD_DIRECTORY_PATH, aggregable = Aggregable.YES, sortable = Sortable.YES)
    @Column(nullable = false, length = DIRECTORY_PATH_LENGTH)
    @ToString.Include
    private String directoryPath;

    @GenericField(name = FIELD_FILE_NAME, sortable = Sortable.YES)
    @Column(nullable = false, length = FILENAME_LENGTH)
    @ToString.Include
    private String fileName;

    @GenericField(name = FIELD_FILE_ENTRY, sortable = Sortable.YES)
    @Column(length = FILE_ENTRY_LENGTH)
    @ToString.Include
    private String fileEntry;

    // -----

    @Column(nullable = false)
    private long fileSize;

    // -----

    @Column(nullable = false)
    private LocalDateTime modifiedTime;

    @Column(nullable = false)
    private LocalDateTime enrichTime;

    @Column
    private LocalDateTime checkTime;

    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private long size;

    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private String sha1;

    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private String md5;

    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private String crc;

    @GenericField
    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    @ToString.Include
    private String signature;

    // -----

    public Content(ContentVolume volume, String directoryPath, String fileName, String fileEntry) {
        this.volume = volume;

        this.directoryPath = directoryPath;
        this.fileName = fileName;
        this.fileEntry = fileEntry;
    }

    public Content(ContentVolume volume, String directoryPath, String fileName) {
        this(volume, directoryPath, fileName, null);
    }

    public Content(ContentVolume volume, String subpath) {
        this.volume = volume;

        var index = StringUtils.lastIndexOf(subpath, COMPRESS_SEPARATOR);
        if (index < 0) {
            directoryPath = FilenameUtils.getPathNoEndSeparator(subpath);
            fileName = FilenameUtils.getName(subpath);
            fileEntry = null;
        } else {
            var subpathBase = subpath.substring(0, index);
            directoryPath = FilenameUtils.getPathNoEndSeparator(subpathBase);
            fileName = FilenameUtils.getName(subpathBase);
            fileEntry = subpath.substring(index + COMPRESS_SEPARATOR.length());
        }
    }

    public static Path toPathContentRelative(String directoryPath, String fileName, String fileEntry) {
        return Paths.get(directoryPath).resolve(fileEntry == null ? fileName : fileName + COMPRESS_SEPARATOR + fileEntry);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var other = (Content) o;

        return id != null && id.equals(other.id);
    }

    public String getContentExtension() {
        return FilenameUtils.getExtension(isCompressedEntry() ? fileEntry : fileName);
    }

    public Path getPathContentRelative() {
        return toPathContentRelative(directoryPath, fileName, fileEntry);
        // return getPathDirectoryRelative().resolve(isCompressedEntry() ? fileName + COMPRESS_SEPARATOR + fileEntry : fileName);
    }

    private Path getPathDirectoryAbsolute() {
        return volume.toPath().resolve(directoryPath);
    }

    private Path getPathDirectoryRelative() {
        return Paths.get(directoryPath);
    }

    public Path getPathFileAbsolute() {
        return getPathDirectoryAbsolute().resolve(fileName);
    }

    public Path getPathFileRelative() {
        return getPathDirectoryRelative().resolve(fileName);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public boolean isCompressedEntry() {
        return fileEntry != null;
    }

    public boolean isEnriched(LocalDateTime modifiedTime) {
        return this.enrichTime != null && !this.enrichTime.isBefore(modifiedTime);
    }

    public boolean isFileExists() {
        return Files.exists(getPathFileAbsolute());
    }

    public boolean isFileExistsAndValid() {
        var absolutePath = getPathFileAbsolute();
        try {
            return Files.exists(absolutePath) && isFileModifiedTimeEquals(absolutePath, getModifiedTime());
        } catch (IOException e) {
            return false;
        }
    }

    public boolean isSameLocationAs(Content content) {
        return volume.getId().equals(content.getVolume().getId())
            && directoryPath.equals(content.directoryPath)
            && fileName.equals(content.fileName)
            && (!isCompressedEntry() && !content.isCompressedEntry() || isCompressedEntry() && fileEntry.equals(content.fileEntry));
    }

    public boolean isSameSignatureAs(Content content) {
        return getSignature().equals(content.getSignature());
    }

    public void setSignature(String crc, String md5, String sha1, long size) {
        var crcFormatted = StringUtils.isNotBlank(crc) ? crc.toUpperCase() : "_";
        this.crc = crcFormatted;

        var md5Formatted = StringUtils.isNotBlank(md5) ? md5.toUpperCase() : "_";
        this.md5 = md5Formatted;

        var sha1Formatted = StringUtils.isNotBlank(sha1) ? sha1.toUpperCase() : "_";
        this.sha1 = sha1Formatted;

        this.size = size;
        this.signature = crcFormatted + md5Formatted + sha1Formatted + size;
    }
}
