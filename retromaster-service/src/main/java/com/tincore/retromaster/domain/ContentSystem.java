package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentExtension.*;

public enum ContentSystem {
    MULTIFORMAT("Multi-format"),

    ACORN_ATOM,
    ACORN_ARCHIMEDES,
    ACORN_BBC("Acorn BBC"),
    ACORN_ELECTRON,
    ACORN_FILESTORE_E01,
    ACORN_RISC_PC,
    ACORN_RISCOS_FLASH_MEDIA("Acorn RISC OS Flash Media"),
    ACORN_SYSTEM_1("Acorn System 1"),
    ACORN("Acorn"),

    ACETRONIC_MPU_1000("Acetronic MPU-1000"),

    ACT_APRICOT("ACT Apricot"),
    ACT_APRICOT_F1("ACT Apricot F1"),
    ACT_APRICOT_PORTABLE("ACT Apricot Portable"),
    ACT_APRICOT_PC_XI("ACT Apricot PC-XI"),

    ADVANCED_COMPUTER_DESIGN_PDQ3("Advanced Computer Design PDQ-3"),

    ADC_SUPER_SIX,

    AEG_OLYMPIA_OLYTEXT_20,

    ALTOS_ACS_X86("Altos Computer Systems ACS-186, 586, 686 & 986"),
    ALTOS_ACS_8000("Altos Computer Systems ACS-8000"),
    ALTOS_ACS_8600("Altos Computer Systems ACS-8600"),
    ALTOS_SERIES_5("Altos Computer Systems Series 5"),

    ALPHASMART_PRO,

    ALSPA_COMPUTERS_ALSPA("ALSPA Computers Alspa"),

    AMERICAN_LASER_GAMES_CDROM_SYSTEM("American Laser Games CD-ROM System"),

    AQUAPLUS_PECE("Aquaplus P-ECE"),

    AMPRO_LITTLE_BOARD,

    AMSTRAD_CPC("Amstrad CPC"),
    AMSTRAD_CPCP("Amstrad CPC+"),
    AMSTRAD_GX4000("Amstrad GX4000"),
    AMSTRAD_PCW("Amstrad PCW"),
    AMSTRAD_PC1512("Amstrad PC1512"),
    AMSTRAD_PC1640("Amstrad PC1640"),
    AMSTRAD_MEGA_PC("Amstrad Mega PC"),
    AMSTRAD_NC100("Amstrad NC-100"),
    AMSTRAD_NC200("Amstrad NC-200"),
    AMSTRAD,

    ANDAMIRO_PUMP_IT_UP,

    ANALOGUE_POCKET,
    ANALOGUE_NT_MINI_NOIR,
    ANALOGUE_NT_MINI,

    ANALOGUE_SUPER_NT,
    ANALOGUE_NT,
    ANALOGUE_MEGA_SG,

    APF_IMAGINATION_MACHINE("APF Imagination Machine"),
    APF_PECOS_ONE("APF PeCos One"),
    APF_M1000("APF M-1000"),

    APPLE_1("Apple I"),
    APPLE_2("Apple II"),
    APPLE_3("Apple III"),
    APPLE_LISA("Apple Lisa"),
    APPLE_LISA_2("Apple Lisa 2"),
    APPLE_2GS("Apple IIGS"),
    APPLE_MACINTOSH,
    APPLE_PIPPIN,

    APPLIED_TECHNOLOGY_MICROBEE("Applied Technology Microbee"),

    ARDUBOY_ARDUBOY,

    ATARI_2600("Atari 2600"),
    ATARI_5200("Atari 5200"),
    ATARI_7800("Atari 7800"),
    ATARI_8BIT("Atari 8bit"),
    ATARI_JAGUAR("Atari Jaguar"),
    ATARI_JAGUAR_CD("Atari Jaguar CD"),
    ATARI_LYNX("Atari Lynx"),
    ATARI_ST("Atari ST"),
    ATARI_STE("Atari STE"),
    ATARI_MEGAST("Atari MegaST"),
    ATARI_MEGASTE("Atari MegaSTE"),
    ATARI_TT("Atari TT"),
    ATARI_FALCON("Atari Falcon"),
    ATARI("Atari"),

    AUDIO_CD,

    BALLY_ASTROCADE("Bally Astrocade"),

    BANDAI_PIPPIN,
    BANDAI_PLAYDIA,
    BANDAI_SUPER_NOTE_CLUB,
    BANDAI_SUPER_VISION_8000,
    BANDAI_WONDERSWAN("Bandai Wonderswan"),
    BANDAI_WONDERSWAN_COLOR("Bandai Wonderswan color"),
    BANDAI_DESIGN_MASTER_DENSHI_MANGAJUKU("Bandai Design Master Denshi Mangajuku"),

    BD_VIDEO,

    BENESSE_POCKET_CHALLENGE_V("Bemesse Pocket Challenge V"),
    BENESSE_POCKET_CHALLENGE_W("Bemesse Pocket Challenge W"),
    BENESSE_POCKET_CHALLENGE_V2("Bemesse Pocket Challenge V2"),

    BIT_CORP_GAMATE("Bit Corporation Gamate"),
    BIT_CORP_BIT_90("Bit Corporation BIT 90"),

    BNPO_BASHKIRIA_2M("BNPO Bashkiria-2M"),

    BONDWELL_2,
    BONDWELL_12,
    BONDWELL_14,

    BROTHER_WP230B_330MDS_335B("Brother WP-230B, 330MDS & 335B"),
    BURROUGHS_B20,
    BURROUGHS_B1000_SERIES,

    CAMBRIDGE_COMPUTER_Z88("Cambridge Computer Z88"),
    CAMPUTERS_LYNX,

    CANON_X_07,
    CANON_CAT,

    CHINTENDO_VII,

    CAPCOM_CPS_III,

    CASIO_CFX_9850,
    CASIO_LOOPY,
    CASIO_PB_1000,
    CASIO_PB_2000C,
    CASIO_PV_1000,
    CASIO_PV_2000,
    CASIO_FP_1X00("Casio FP-1000 & FP-1100"),


    CCE_MC1000("CCE MC-1000"),
    COLECO_COLECOVISION("Coleco Colecovision"),
    COLECO_COLECOVISION_ADAM("Coleco Colecovision ADAM"),

    COMMODORE_AMIGA("Commodore Amiga"),
    COMMODORE_AMIGA_1200("Commodore Amiga 1200"),
    COMMODORE_CD32("Commodore Amiga CD32"),
    COMMODORE_CDTV("Commodore Amiga CDTV"),
    COMMODORE_C64("Commodore C64"),
    COMMODORE_C65("Commodore 65"),
    COMMODORE_C64DTV("Commodore C64dtv"),
    COMMODORE_C128("Commodore C128"),
    COMMODORE_CBM_II("Commodore CBM II"),
    COMMODORE_C16_C116_PLUS_4("Commodore C16 C116 Plus 4"),
    COMMODORE_MAX("Commodore MAX"),
    COMMODORE_PET("Commodore PET"),
    COMMODORE_SUPERPET("Commodore SUPERPET"),
    COMMODORE_VIC10("Commodore VIC-10"),
    COMMODORE_VIC20("Commodore VIC-20"),
    COMMODORE_8096,
    COMMODORE_8296,
    COMMODORE_BX256_80HP("Commodore BX256-80HP"),
    COMMODORE_P500,
    COMMODORE,

    COMPUGRAPHIC_MCS_10("Compugraphic MCS-10"),

    COMX_COMX35("COMX COMX-35"),

    CONVERGENT_TECHNOLOGIES_AWS_NGEN_WORKSTATION,
    CONVERGENT_TECHNOLOGIES_MINIFRAME,

    CONITEC_PROF_180,
    CONITEC_PROF_80,

    CONTROL_DATA_CORPORATION_CDC110,

    CREATRONIC_MEGADUCK("Creatronic Mega Duck"),

    CYBIKO_XTREME("Cybiko Xtreme"),
    CYBIKO_CYBIKO("Cybiko Cybico"),
    CYBIKO("Cybiko"),

    DAI_PERSONAL_COMPUTER,

    DEC_ALPHA,
    DEC_DECSTATION,
    DEC_ALPHASERVER,

    DEC_PDP_1("DEC pdp1"),
    DEC_PDP_7("DEC pdp7"),
    DEC_PDP_8("DEC pdp8"),
    DEC_PDP_9("DEC pdp9"),
    DEC_PDP_10("DEC pdp10"),
    DEC_PDP_11("DEC pdp11"),
    DEC_PDP_12("DEC pdp12"),
    DEC_PDP_15("DEC pdp15"),
    DEC_DECPX_AXP_150,
    DEC_RAINBOW_100,
    DEC_VT_180,

    DICK_SMITH_SUPER_80R,
    DICK_SMITH_SUPER_80,

    DRAGON_DATA_ALPHA("Dragon data Alpha"),
    DRAGON_DATA_BETA("Dragon data Beta"),

    DRAGON_DATA("Dragon data"),

    DICK_SMITH_SYSTEM_80,

    DIDACT_ESSELTE_100,

    DIGITAL_MEDIA_CARTRIDGE_FIRECORE,

    DIMENSION_68000,

    DVD_VIDEO,

    DYNABYTE_MONARCH,

    EACA_EG2000("Eaca eg2000"),

    EIGHTBIT_COMMANDER_X16("8-Bit Productions Commander X16"),

    ELEKTRONIKA_BK_0010("Elektronika BK-0010"),
    ELEKTRONIKA_BK_0011("Elektronika BK-0011-411"),
    ELEKTRONIKA_MC_1502,

    ELEKTRONSKA_INDUSTRIJA("Elektronska Industrija Nis PECOM 32 & 64"),

    EMERSON_ARCADIA_2001("Emerson arcadia 2001"),

    ENTERPRISE_64("Enterprise 64 128"),

    ENTEX_ADVENTURE_VISION("Entex adventure vision"),

    EPOCH_GAME_POCKET_COMPUTER,
    EPOCH_SUPER_CASSETTE_VISION("Epoch super cassette vision"),

    EPSON_PX_4,
    EPSON_PX_8("Epson PX8 & HC-88 & Geneva"),
    EPSON_CPM,
    EPSON_QX_10,

    ETL_MARK_2("ETL mark II"),
    ETL_MARK_4("ETL mark IV"),
    ETL_MARK_4A("ETL mark IV A"),

    EXELVISION_EXETEL("Exelvision Exetel"),
    EXELVISION_EXL100("Exelvision EXL100"),
    EXELVISION_MULTI("Exelvision Exetel & EXL100"),
    EXELVISION("Exelvision"),

    EXIDY_SORCERER("Exidy Sorcerer"),

    FAIRLIGHT_CMI_SERIES_3("Fairlight CMI Series III"),
    FAIRCHILD_CHANNEL_F("Fairchild channel F"),

    FIDELITY_SENSORY_CHESS_CHALLENGER("Fidelity Sensory Chess Challenger"),
    FLEXIBLE_SYSTEMS_TASMAN_TURTLE("Flexible Systems Tasman Turtle"),

    FERGUSON_BIG_BOARD_2("Ferguson Big Board II"),

    FRANKLIN_ACE_100,
    FRANKLIN_ACE_500,
    FRANKLIN_ACE_1000,
    FRANKLIN_ACE_2000,

    FRONT_FAREAST_MAGIC_DRIVE,

    FUJI_PHOTO_FILM_FUJIC("Fuji Photo Film FUJIC"),
    FUJITSU_FMTOWNS("Fujitsu FM Towns"),
    FUJITSU_FM7("Fujitsu FM-7"),
    FUJITSU_FMR50("Fujitsu FM R-50"),
    FUJITSU_FM77AV("Fujitsu FM-77AV"),
    FUJITSU_FACOM_9450("Fujitsu Facom 9450"),
    FUJITSU_FACOM("Fujitsu Facom"),
    FUJITSU("Fujitsu"),

    FUKUTAKE_PUBLISHING_STUDYBOX,

    FUNWORLD_PHOTOPLAY,
    FUNTECH_SUPERACAN("Funtech super Acan"),

    GALAKSIJA_GALAKSIJA("Galaksija Galaksija"),
    GALAKSIJA_PLUS("Galaksija Plus"),
    GALAKSIJA("Galaksija"),

    GAMEPARK_GP32("GamePark GP32"),

    GCE_VECTREX("GCE Vectrex"),

    GOULD_K105D("Gould K105-D Logic Analyzer"),
    GOULD_K115("Gould K115 Logic Analyzer"),
    GOULD_K450("Gould K450 Logic Analyzer"),

    GRUNDY_NEWBRAIN("Grundy NewBrain"),

    GUNDAM_RX78("Gundam RX-78"),

    HARTUNG_GAME_MASTER,

    HAWTHORNE_TECHNOLOGY_HT68K,
    HAWTHORNE_TECHNOLOGY_TINYGIANT,

    HEARTKIT_H8,

    HEWLETT_PACKARD_16500A,
    HEWLETT_PACKARD_4951,
    HEWLETT_PACKARD_9000,
    HEWLETT_PACKARD_9835,
    HEWLETT_PACKARD_9845,

    HEWLETT_PACKARD_APOLLO,

    HEWLETT_PACKARD_HP48("Hewlett Packard hp48"),
    HEWLETT_PACKARD_HP49,
    HEWLETT_PACKARD_HP85,
    HEWLETT_PACKARD_HP9835A,
    HEWLETT_PACKARD_LASERJET("Hewlett Packard Laserjet"),

    HEWLETT_PACKARD_HP6400("Hewlett Packard HP 64000 Logic Development System"),

    HANIMEX_PENCIL_II,

    HITACHI_S1,
    HITACHI_BASIC_MASTER_JR,
    HITACHI_BASIC_MASTER_JR_LEVEL2,
    HITACHI_BASIC_MASTER_JR_LEVEL3,

    HOMELAB_BRAILAB("Homelab Brailab"),
    HOMELAB("Homelab"),

    IBM_PC("IBM PC", bat, exe, com, ima, cue, iso, ciso),
    IBM_PCJR("IBM PCjr"),
    IBM_1620,
    IBM_SYSTEM_360,
    IBM_DISPLAYWRITER,

    IQUE,
    IQUE_PLAYER,

    INFOCOM_Z_MACHINE("Infocom Z-Machine"),

    INTEC_INTERACT,

    INTERACT_FAMILY_COMPUTER("Interact Family Computer"),

    JUPITER_CANTAB_ACE("Jupiter Cantab Ace"),

    KAYPRO_II("Kaypro II"),

    KEIO_UNIVERSITY_K_1("Keio university K-1"),

    KOEI_PASOGO,

    KONAMI_PICNO,

    LIBRASCOPE_LGP30,
    LEAPFROG_LEAPPAD,
    LEAPFROG_LEAPSTER,
    LEAPFROG_LEAPSTER_LEARNING_GAMING_SYSTEM,

    LUXOR_ABC_80,
    LUXOR_ABC_800("Luxor ABC 800"),
    LUXOR_ABC_806,
    LUXOR_ABC_1600,
    LUXOR_VIDEO_ENTERTAINMENT_SYSTEM("Luxor video entertainment system"),

    MAGNAVOX_ODYSSEY_2("Magnavox Odyssey 2"),

    MATRA_HACHETTE_ALICE_32,
    MATRA_HACHETTE_ALICE_90,

    MATSUSHITA_JR100,
    MATSUSHITA_JR200,
    MATSUSHITA_NATIONAL_JR200,

    MATTEL_AQUARIUS,
    MATTEL_HYPERSCAN,
    MATTEL_JUICE_BOX,
    MATTEL_INTELLIVISION,
    MATTEL_INTELLIVISION_ECS,

    MEGATEL_QUARK,
    MEMOTECH_MTX("Memotech MTX"),
    MEMOTECH_MTX512("Memotech MTX512"),

    MEMOREX_VIDEO_INFORMATION_SYSTEM,

    MICHAEL_J_BAUER_DREAM6800("Michael J. Bauer DREAM-6800"),

    MGT_SAM_COUPE("MGT Sam Coupe"),

    MICROKEY_PRIMO,
    MICRONIQUE_HECTOR,

    MITS_ALTAIR_8800("MITS Altair 8800"),
    MICROSOFT_MSX_1("MSX 1", wav, tap, cas, mx1, bin, rom, dsk, dmk, d77, d88, _1dd, dfi, hfe, imd, ipf, mfi, mfm, td0, cqm, cqi),
    MICROSOFT_MSX_2("MSX 2", wav, tap, cas, mx1, mx2, bin, rom, dsk, dmk, d77, d88, _1dd, dfi, hfe, imd, ipf, mfi, mfm, td0, cqm, cqi),
    MICROSOFT_MSX_2P("MSX 2+", wav, tap, cas, mx1, mx2, bin, rom, dsk, dmk, d77, d88, _1dd, dfi, hfe, imd, ipf, mfi, mfm, td0, cqm, cqi),
    MICROSOFT_MSX_TURBO_R("MSX Turbo R", wav, tap, cas, mx1, mx2, bin, rom, dsk, dmk, d77, d88, _1dd, dfi, hfe, imd, ipf, mfi, mfm, td0, cqm, cqi),
    MICROSOFT_XBOX,
    MICROSOFT_XBOX_360,

    MITSUBISHI_MULTI_8,

    MOBILE_J2ME,
    MOBILE_POCKET_PC,
    MR_ISIZU_Z80_TV_GAME_SYSTEM("Mr. Isizu Z80 TV Game System"),
    MORROW_MPZ80,

    MOTOROLA_EXORCISER,
    MOTOROLA_VME_10,

    NAMCO_TRIFORCE,

    NASCOM("Nascom I & II"),

    NEXT,

    NCR_DECISION_MATE_V("NCR Decision mate v"),

    NEC_APC("NEC Advanced Personal Computer"),
    NEC_PC6001("NEC PC-6001"),
    NEC_PC8001("NEC PC-8001"),
    NEC_PC8201("NEC PC-8201"),
    NEC_PC8801("NEC PC-8801"),
    NEC_PC88VA("NEC PC-88va"),
    NEC_PC9801("NEC PC-9801"),
    NEC_PC9821("NEC PC-9821"),
    NEC_PCFX("NEC PC FX"),
    NEC_PCFXGA("NEC PC FXGA"),
    NEC_TURBOGRAFX("NEC PC Engine Turbografx 16"),
    NEC_TURBOGRAFX_CD("NEC PC Engine CD Turbografx 16 CD"),
    NEC_SUPERGRAFX("NEC Super Grafx"),

    NICHIBUTSU_MY_VISION,
    NINTENDO_64("Nintendo 64"),
    NINTENDO_64DD("Nintendo 64DD"),
    NINTENDO_DSI("Nintendo DSI"),
    NINTENDO_DS("Nintendo DS"),
    NINTENDO_3DS("Nintendo 3DS"),
    NINTENDO_3DS_NEW("New Nintendo 3DS"),
    NINTENDO_GAMEBOY("Nintendo Gameboy"),
    NINTENDO_GAMEBOY_COLOR("Nintendo Gameboy color"),
    NINTENDO_GAMEBOY_ADVANCE("Nintendo Gameboy advance"),
    NINTENDO_GAMEBOY_POCKET("Nintendo Gameboy pocket"),
    NINTENDO_KIOSK_VIDEO_COMPACT_FLASH("Nintendo Kiosk Video Compact Flash"),
    NINTENDO_MARIO_NO_PHOTOPI_SMARTMEDIA("Nintendo Mario no Photopi SmartMedia"),

    NINTENDO_NES("Nintendo NES"),
    NINTENDO_SNES("Nintendo SNES"),
    NINTENDO_POKEMON_MINI,
    NINTENDO_SUFAMI_TURBO,
    NINTENDO_VIRTUAL_BOY("Nintendo Virtual Boy"),
    NINTENDO_WALLPAPERS("Nintendo Wallpapers"),
    NINTENDO_WII("Nintendo Wii", iso, ciso, wbfs, wad),
    NINTENDO_WIIU("Nintendo WiiU", iso, ciso, wbfs, wad),
    NINTENDO_FAMILY_COMPUTER_DISK_SYSTEM("Nintendo FDS"),
    NINTENDO_GAMECUBE,
    NINTENDO_SATELLAVIEW,
    NINTENDO_EREADER,
    NINTENDO_PLAY_YAN,
    NINTENDO,

    NORTHSTAR_ADVANTAGE,
    NORTHSTAR_HORIZON,

    NOKIA_MIKROMIKKO,
    NOKIA_NGAGE("Nokia N-Gage"),

    NUCLEAR_DATA("Nuclear Data ND4410 & ND4420"),

    OCC_OSBORNE("OCC Osborne 1 & Osborne Executive"),

    OHIO_SC1P("Ohio Scientific Challenger 1P"),
    PG_MZSOS("Oh! MZ S-OS"),
    OLIVETTI_M20,
    OLIVETTI_PRODEST("Olivetti Prodest PC 128"),

    OPENPANDORA,

    OTRONA_ATTACHE,

    OUYA,

    PANASONIC_3DO("Panasonic 3DO"),

    PANIC_PLAYDATE,

    PALM,

    PALSON_CX_3000,

    PATISONIC_ALESTE_520EX,

    PENTAGON_1024SL,

    PEL_VARAZDIN_ORAO("PEL Varazdin Orao"),

    PETERS_PLUS_SPRINTER,

    PHILIPS_P2000("Philips P2000"),
    PHILIPS_VG5000("Philips VG 5000"),
    PHILIPS_VIDEOPAC("Philips Videopac+"),
    PHILIPS_CD_I,

    PHOTO_CD,

    PIMORONI_32BLIT,
    PIMORONI_PICOSYSTEM,
    PIONEER_LASERACTIVE("Pioneer LaserActive"),

    PLAYSTATION_GAMESHARK_UPDATES,

    PRAVETZ_8D,

    PROLOGICA,

    PSION_SERIES5("Psion Series 5"),
    PTC_SOL("PTC sol"),
    RADICA_ARCADE("Radica arcade"),
    RADIO_86RK_APOGEJ_BK01("Radio-86RK Apogej BK-01"),
    RADIO_86RK_MIKRO80("Radio-86RK Mikro-80"),
    RADIO_86RK_MIKROSHA("Radio-86RK Mikrosha"),
    RADIO_86RK_PARTNER_0101("Radio-86RK Partner-01.01"),
    RADIO_86RK_YUT_88("Radio-86RK YuT-88"),
    RADIO_86RK("Radio-86RK"),

    RASPBERRY_PI("Raspberry Foundation Raspberry Pi "),

    RAYTHEON_704,

    REGNECENTRALEN_RC700("Regnecentralen RC 700"),
    RETROLEUM_V6Z80P("Retroleum V6Z80P+"),

    ROWTRON_TELEVISION_COMPUTER_SYSTEM,

    ROBOTRON_BIC_A5105,

    ROLAND_GS_COMPATIBLES,

    RC_NIMBUS_PC,

    RCA_CHIP_8("RCA Chip-8"),
    RCA_STUDIO2("RCA Studio II"),
    RCA_SUPERCHIP("RCA Superchip"),

    RCA_MICROBOARD("RCA Microboard Computer Development System"),

    ROBOTRON_HC900("Robotron HC900"),
    ROBOTRON_KC_COMPACT("Robotron KC Compact"),
    ROBOTRON_KC85,
    ROBOTRON_Z1013("Robotron z1013"),
    ROBOTRON_Z9001("Robotron Z9001"),

    ROCKWELL_AIM("Rockwell AIM 65"),

    RM_NIMBUS_PC("RM nimbus pc"),

    SABA_VIDEOPLAY("Saba Videoplay"),

    SAGE_SAGE_II("Sage II"),

    SANYO_MBC_200("Sanyo MBC-200 & 1200"),
    SANYO_MBC_550("Sanyo MBC-550 & 555"),
    SANYO_PHC_25("Sanyo PHC-25"),

    SAMSUNG_SPC_1000,

    SEGA_32X("Sega 32x"),
    SEGA_CD("Sega CD", DISC_SET),
    SEGA_COMPUTER_3000("Sega Computer 3000"),
    SEGA_BEENA,
    SEGA_ADVANCED_PICO_BEENA("Sega Advanced Pico Beena"),
    SEGA_CHIHIRO,
    SEGA_DREAMCAST("Sega Dreamcast", DISC_SET),
    SEGA_GAME_1000("Sega Game 1000"),
    SEGA_GAMEGEAR("Sega Gamegear"),
    SEGA_MASTERSYSTEM("Sega Mastersystem"),
    SEGA_MEGADRIVE("Sega Megadrive"),
    SEGA_PICO("Sega Pico"),
    SEGA_SATURN("Sega Saturn", DISC_SET),
    SEGA_SUPERCONTROLSTATION("Sega Super Control Station"),
    SEGA_VMS("Sega Visual Memory System"),
    SEGA_WONDERMEGA("Sega WonderMega"),
    SEGA_TITAN_VIDEO,
    SEGA_SYSTEM_32,
    SEGA_NAOMI,
    SEGA_MEGAPLAY,
    SEGA_LINDBERGH,
    SEGA_RINGWIDE,
    SEGA_MODEL_2B,

    SETA_ALECK64,

    SHARP_MZ80B("Sharp MZ-80B"),
    SHARP_MZ700("Sharp MZ-700"),
    SHARP_MZ800("Sharp MZ-800 & MZ-1500"),
    SHARP_MZ2000("Sharp MZ-2000"),
    SHARP_MZ2500("Sharp MZ-2500"),
    SHARP_PCG850("Sharp PC-G850, PC-G815 & E200"),
    SHARP_X1("Sharp X1"),
    SHARP_X1TURBO("Sharp X1 Turbo"),
    SHARP_X68000("Sharp x68000"),
    SHARP_WIZARD_OZ_700("Sharp Wizard OZ-700"),


    SINCLAIR_QL("Sinclair QL"),
    SINCLAIR_ZX_SPECTRUM("Sinclair ZX Spectrum"),
    SINCLAIR_ZX_SPECTRUM_128("Sinclair ZX Spectrum 128"),

    SINCLAIR_ZX80("Sinclair ZX80"),
    SINCLAIR_ZX81("Sinclair ZX81"),

    SONY_POCKETSTATION("Sony PocketStation"),
    SONY_PLAYSTATION("Sony playstation", DISC_SET),
    SONY_PLAYSTATION_2("Sony playstation 2", DISC_SET),
    SONY_PLAYSTATION_3("Sony playstation 3", iso, ciso, pkg),
    SONY_PLAYSTATION_4("Sony playstation 4", iso, ciso, pkg),
    SONY_PLAYSTATION_5("Sony playstation 5", iso, ciso, pkg),
    SONY_PLAYSTATION_PORTABLE("Sony playstation portable", DISC_SET),
    SONY_PLAYSTATION_MOBILE("Sony playstation mobile", DISC_SET),
    SONY_PLAYSTATION_VITA,
    SONY_SMC777,
    SONY_NEWS,
    SONY,

    SPECIALIST_SPECIALIST,

    SUB_SPARC,

    SNK_NEOGEO("SNK Neo Geo"),
    SNK_NEOGEO_CD("SNK Neo Geo CD"),
    SNK_NEOGEO_POCKET("SNK Neo Geo Pocket"),
    SNK_NEOGEO_POCKET_COLOR("SNK Neo Geo Pocket Color"),

    SORD_M5("Sord M5"),

    SPECTRAVIDEO_SVI_318("Spectravideo SVI-318 & SVI-328"),

    TAB_AUSTRIA,

    TANDBERG_TDV_2324,

    TANDY_TRS_2000("Tandy Radio Shack 2000"),
    TANDY_TRS_6000("Tandy Radio Shack 6000"),
    TANDY_TRS_80_COCO("Tandy Radio Shack TRS-80 Color Computer"),
    TANDY_TRS_80_1("Tandy Radio Shack TRS-80 Model I"),
    TANDY_TRS_80_3("Tandy Radio Shack TRS-80 Model III"),
    TANDY_TRS_80_4("Tandy Radio Shack TRS-80 Model 4"),
    TANDY_TRS_80_MC_10("Tandy Radio Shack TRS-80 MC-10"),
    TANDY_TRS_80_100("Tandy Radio Shack TRS-80 Model 100"),
    TANDY_TRS_80_200("Tandy Radio Shack TRS-80 Model 200"),
    TANDY_TRS("Tandy Radio Shack"),

    TANGERINE_MICROTAN_65("Tangerine Microtan 65"),
    TANGERINE_ORIC_TELESTRAT("Tangerine Oric Telestrat"),

    TANGERINE_ORIC_MULTI("Tangerine Oric-1 & Oric Atmos"),

    TANGERINE_ORIC_1("Tangerine Oric-1"),
    TANGERINE_ORIC_ATMOS("Tangerine Oric Atmos"),
    TATUNG_EINSTEIN("Tatung Einstein TC-01"),

    TELMAC_TMC_600("Telmac TMC-600"),

    TEMIRAZOV_SOKOLOV_VECTOR_06C("Temirazov & Sokolov Vector-06C"),

    TECHNOSYS_AAMBER_PEGASUS,

    TEKTRONIX_405("Tektronix 405x"),
    TEKTRONIX_4100,
    TEKTRONIX_4400,

    TELENOVA_COMPIS,

    TELCON_ZORBA,

    TELEVIDEO_SYSTEMS_TS_802,
    TELEVIDEO_SYSTEMS_TS_803,
    TELEVIDEO_SYSTEMS_TS_806,

    TERAK_8510A("Terak 8510A & 8600"),

    TESLA_PMD_85("Tesla PMD85"),
    TESLA_ONDRA("Tesla Ondra Vili"),

    TEXAS_INSTRUMENTS_CC_40("Texas Instruments cc40"),
    TEXAS_INSTRUMENTS_TI_73("Texas Instruments ti73"),
    TEXAS_INSTRUMENTS_TI_74,
    TEXAS_INSTRUMENTS_TI_80("Texas Instruments ti80"),
    TEXAS_INSTRUMENTS_TI_81("Texas Instruments ti81"),
    TEXAS_INSTRUMENTS_TI_82("Texas Instruments ti82"),
    TEXAS_INSTRUMENTS_TI_83("Texas Instruments ti83"),
    TEXAS_INSTRUMENTS_TI_84,
    TEXAS_INSTRUMENTS_TI_85("Texas Instruments ti85"),
    TEXAS_INSTRUMENTS_TI_86("Texas Instruments ti86"),
    TEXAS_INSTRUMENTS_TI_89("Texas Instruments ti89"),
    TEXAS_INSTRUMENTS_TI_92("Texas Instruments ti92"),
    TEXAS_INSTRUMENTS_TI_95("Texas Instruments ti95"),
    TEXAS_INSTRUMENTS_TI_99_4A("Texas Instruments ti994a"),
    TEXAS_INSTRUMENTS_LANGUAGE_TUTOR,
    TEXAS_INSTRUMENTS_SPEAK_SPELL,
    TEXAS_INSTRUMENTS_SPEAK_READ,
    TEXAS_INSTRUMENTS_TOUCH_TELL,

    THOMSON_MO5("Thomson MO5"),
    THOMSON_MO6("Thomson MO6"),
    THOMSON_TO7("Thomson TO7"),
    THOMSON_TO8("Thomson TO8"),
    THOMSON_TO8D("Thomson TO8D"),
    THOMSON_TO9("Thomson TO9"),
    THOMSON_TO9_PLUS("Thomson TO9+"),
    THOMSON_TO8_MULTI("Thomson TO8, TO8D, TO9, TO9+"),

    THOMSON("Thomson"),

    TIGER_GIZMONDO("Tiger Gizmondo"),
    TIGER_GAME("Tiger game"),

    TIKI_DATA_KONTIKI_100("Tiki Data Kontiki 100"),

    TIMEX_TS_2068("Timex TS-2068"),
    TIMETOP_GAMEKING,
    TIMETOP_GAMEKING_3,

    TOMY_TUTOR("Tomy Tutor"),
    TOMY_KISS_SITE("Tomy Kiss-Site"),
    TOSHIBA_PASOPIA_7("Toshiba Pasopia 7"),
    TOSHIBA_PASOPIA("Toshiba Pasopia"),
    TOSHIBA_VISICOM,

    TRQ_VIDEO_COMPUTER_H_21,
    TRIUMPH_ADLER_ALPHATRONIC_PC("Triumph-Adler Alphatronic PC"),
    TSUKUDA_ORIGINAL_OTHELLO_MULTIVISION("Tsukuda Original Othello Multivision"),

    UNIVAC_SERIES_90,

    UNISONIC_CHAMPION,

    UNIVERSITY_OF_CAMBRIDGE_EDSAC("University of Cambridge EDSAC"),

    UNIVERSITY_OF_TOKIO_PC_1("University of Tokyo PC-1"),

    USSR_VECTOR_06C("USSR Vector-06C"),

    UZEBOX,

    VEB_MIKROELEKTRONIK_ERFURT_CM_DIAMOND("VEB Mikroelektronik Erfurt Chess-Master Diamond"),
    VEB_MIKROELEKTRONIK,

    VICTOR_9000("Victor Technologies Victor 9000 & Sirius 1"),

    VIDEO_CD,
    VIDEOBRAIN_FAMILY_COMPUTER,
    VIDEOTON_TV_COMPUTER("Videoton TV-Computer"),

    VISUAL_TECHNOLOGY_VISUAL_1050("Visual Technology Visual 1050"),

    VOLTMACE_DATABASE,

    VM_LABS_NUON,

    VTECH_CREATIVISION,
    VTECH_VSMILE("Vtech V.Smile"),
    VTECH_VFLASH("Vtech V.Flash"),
    VTECH_GENIUS,
    VTECH_GENIUS_JUNIOR_MOVIE,
    VTECH_GENIUS_LEADER_COLOR,
    VTECH_GENIUS_LEADER_2000,
    VTECH_GENIUS_LEADER_6000SL,
    VTECH_LASER,
    VTECH_LASER_200("Vtech laser 200"),
    VTECH_LASER_310("Vtech laser 310"),
    VTECH_LASER_2001("Vtech laser 2001"),
    VTECH_MOBIGO,
    VTECH_PRECOMPUTER_1000,
    VTECH_SOCRATES,

    WANG_PC,
    WANG_VS,

    WATARA_SUPERVISION("Watara Supervision"),

    WAVEMATE_BULLET,

    WELBACK_MEGA_DUCK,

    XEROX_1108,
    XEROX_1186,
    XEROX_6085,
    XEROX_820,
    XEROX_820_II,
    XEROX_860,
    XEROX_DAYBREAK,
    XEROX,

    YAMAHA_COPERA,

    ZEEBO,
    YENO_MISTERX,

    ZAPIT_GAMES_GAME_WAVE("ZAPiT Games Game Wave Family Entertainment System"),

    ZPA_NOVY_BOR_IQ_151("ZPA Novy Bor IQ-151"),

    ZILOG_MCZ_2;

    private final String label;
    private final Set<ContentExtension> contentExtensions;

    ContentSystem() {
        this.label = StringUtils.capitalize(StringUtils.replace(name(), "_", " "));
        this.contentExtensions = Collections.emptySet();
    }

    ContentSystem(String label, ContentExtension... contentExtensions) {
        this.label = label;
        this.contentExtensions = contentExtensions == null ? Collections.emptySet() : Stream.of(contentExtensions).collect(Collectors.toSet());
    }

    ContentSystem(String label, Set<ContentExtension> contentExtensions) {
        this.label = label;
        this.contentExtensions = contentExtensions;
    }

    ContentSystem(String label) {
        this.label = label;
        this.contentExtensions = Collections.emptySet();
    }

    // public static ContentSystem[] toArray(ContentSystem... contentSystems) {
    // return contentSystems;
    // }

    public Set<String> getContentExtensionValues() {
        return contentExtensions.stream().map(ContentExtension::getValue).collect(Collectors.toSet());
    }

    public Set<ContentExtension> getContentExtensions() {
        return contentExtensions;
    }

    public String getLabel() {
        return label;
    }
}
