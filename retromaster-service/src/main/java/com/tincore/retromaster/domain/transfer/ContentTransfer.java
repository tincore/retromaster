package com.tincore.retromaster.domain.transfer;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadataItem;
import org.apache.commons.io.FilenameUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public abstract class ContentTransfer {

    public static final Path UNKOWN_PATH = Paths.get("__UNKNOWN__");

    private final Content content;
    private final ContentMetadataItem contentMetadataItem;

    private String targetFileEntry;

    public ContentTransfer(Content content, ContentMetadataItem contentMetadataItem) {
        this.content = content;
        this.contentMetadataItem = contentMetadataItem;
    }

    public static String getTargetFileEntryByContentMetadataItemAndContentMetadata(ContentTransfer contentTransfer) {
        return contentTransfer.getContentMetadataItem().getContentMetadata().getName() + "/" + contentTransfer.getContentMetadataItem().getName();
    }

    public static String getTargetFileEntryByContentMetadataItemAndContentMetadataAndContentMetadataId(ContentTransfer contentTransfer) {
        return contentTransfer.getContentMetadataItem().getContentMetadata().getName() + "_" + contentTransfer.getContentMetadataItem().getContentMetadata().getId() + "/" + contentTransfer.getContentMetadataItem().getName();
    }

    public static String getTargetFileEntryByContentMetadataItemAndContentMetadataAndContentMetadataIdAndCrc(ContentTransfer contentTransfer) {
        var fileName = contentTransfer.getContentMetadataItem().getName();
        var name = FilenameUtils.getBaseName(fileName) + "_" + contentTransfer.getContent().getCrc() + FilenameUtils.EXTENSION_SEPARATOR + FilenameUtils.getExtension(fileName);

        return contentTransfer.getContentMetadataItem().getContentMetadata().getName() + "_" + contentTransfer.getContentMetadataItem().getContentMetadata().getId() + "/" + name;
    }

    public static String getTargetFileEntryByDefault(ContentTransfer contentTransfer) {
        return contentTransfer.getContentMetadataItem().getName();
    }

    public Content getContent() {
        return content;
    }

    public ContentMetadataItem getContentMetadataItem() {
        return contentMetadataItem;
    }

    public String getTargetFileEntry() {
        return targetFileEntry;
    }

    public void setTargetFileEntry(String targetFileEntry) {
        this.targetFileEntry = targetFileEntry;
    }

    public boolean isContentSignatureSameAsAnyOf(List<? extends ContentTransfer> contentSources) {
        var mySignature = content.getSignature();
        return contentSources.stream().anyMatch(o -> o.getContent().getSignature().equals(mySignature));
    }

    public boolean isIdentified() {
        return contentMetadataItem != null;
    }

    public boolean isSameContentAs(ContentTransfer contentTransfer) {
        return getContent().getId() != null && getContent().getId().equals(contentTransfer.getContent().getId());
    }
}
