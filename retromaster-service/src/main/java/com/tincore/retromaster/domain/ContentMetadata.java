package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.ApplicationConfiguration;
import com.tincore.util.UuidUtil;
import com.tincore.util.jpa.JpaDataTypeHelper;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.engine.backend.types.Aggregable;
import org.hibernate.search.engine.backend.types.Searchable;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.*;

import jakarta.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.tincore.retromaster.config.ApplicationLuceneAnalysisConfigurer.ANALYZER_STANDARD_EDGE_N_GRAM;
import static com.tincore.retromaster.config.ApplicationLuceneAnalysisConfigurer.NORMALIZER_LOWERCASE;

@Slf4j
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(
    indexes = {
        @Index(name = "idx_contmeta_title", columnList = "title"),
        @Index(name = "idx_contmeta_name", columnList = "name"),
        @Index(name = "idx_contmeta_publisher", columnList = "publisher"),
        @Index(name = "idx_contmeta_region", columnList = "region"),
    })
@Indexed
public class ContentMetadata {

    public static final String COMPLETE_PARTIAL = "P";
    public static final String COMPLETE_TOTAL = "C";
    public static final String COMPLETE_EMPTY = "E";
    public static final String COMPLETE_NOITEMS = "X";

    public static final String FIELD_SYSTEM = "system";
    public static final String FIELD_SYSTEM_RAW = "system_na";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_TITLE_RAW = "title_na";
    public static final String FIELD_SOURCE = "source";
    public static final String FIELD_LANGUAGE = "language";
    public static final String FIELD_REGION = "region";
    public static final String FIELD_COMPLETE = "complete";
    public static final String FIELD_PUBLISHER = "publisher";
    public static final String FIELD_PUBLISHER_RAW = "publisher_na";
    public static final String FIELD_PREFIX = "prefix";
    public static final String FIELD_MEDIA_TYPE = "mediaType";
    public static final String FIELD_YEAR = "year";
    public static final String FIELD_CONTENT_METADATA_SET_ID = "contentMetadataSet.id";

    public static final int NAME_LENGTH = 1024;
    public static final int TITLE_LENGTH = 1024;
    public static final int DESCRIPTION_LENGTH = 1024;
    public static final int TAGS_EXTRA_LENGTH = 254;
    private static final String YEAR_UNKNOWN = "xxxx";
    private static final Pattern YEAR_PATTERN = Pattern.compile("((19|2[0-9x])[0-9x]{2})", Pattern.MULTILINE);
    private static final int SINGLE_CHAR_NAME_LENGTH = 1;

    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = JpaDataTypeHelper.TYPE_UUID)
    private UUID id;

    @FullTextField(name = FIELD_NAME)
    @Column(nullable = false, length = NAME_LENGTH)
    @ToString.Include
    private String name;

    @FullTextField(name = FIELD_TITLE, analyzer = ANALYZER_STANDARD_EDGE_N_GRAM)
    // @FullTextField(name = FIELD_TITLE, analyzer = ANALYZER_STANDARD)
    @KeywordField(name = FIELD_TITLE_RAW, normalizer = NORMALIZER_LOWERCASE, sortable = Sortable.YES)
    @Column(nullable = false, length = TITLE_LENGTH)
    private String title;

    @FullTextField(name = FIELD_SYSTEM)
    @KeywordField(name = FIELD_SYSTEM_RAW, aggregable = Aggregable.YES, sortable = Sortable.YES)
    // DO NOT normalizer = NORMALIZER_LOWERCASE. Should PRE normalize data. HibernateSearch6 now returns NORMALIZED value instead of original.
    @Column
    @ToString.Include
    private String system;

    @GenericField(name = FIELD_SOURCE, aggregable = Aggregable.YES)
    @Column
    @ToString.Include
    private String source;

    @GenericField(name = FIELD_MEDIA_TYPE)
//    @GenericField(name = FIELD_MEDIA_TYPE, searchable = Searchable.YES)
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @ToString.Include
    private ContentMediaType mediaType;

    @GenericField
    @ToString.Include
    private String mediaOrder;

    @GenericField
    @Column(length = DESCRIPTION_LENGTH)
    private String description;

    @Column
    private String date;

    @FullTextField(name = FIELD_PUBLISHER)
    @KeywordField(name = FIELD_PUBLISHER_RAW, sortable = Sortable.YES, aggregable = Aggregable.YES)
    // DO NOT normalizer = NORMALIZER_LOWERCASE. Should PRE normalize data. HibernateSearch6 now returns NORMALIZED value instead of original.
    @Column
    @ToString.Include
    private String publisher;

    @Column
    private boolean unidentified;

    @KeywordField(name = FIELD_REGION, aggregable = Aggregable.YES)
    @Column
    @ToString.Include
    private String region;

    @KeywordField(name = FIELD_LANGUAGE, aggregable = Aggregable.YES)
    @Column
    private String language;

    @KeywordField(name = FIELD_COMPLETE, aggregable = Aggregable.YES, searchable = Searchable.YES)
    @Column
    @Builder.Default
    private String complete = ContentMetadata.COMPLETE_EMPTY;

    @GenericField
    @Column
    private boolean alternate;

    @Column
    private String alternateData;

    @GenericField
    @Column
    private boolean badDump;

    @Column
    private String badDumpData;

    @GenericField
    @Column
    private boolean cracked;

    @Column
    private String crackedData;

    @GenericField
    @Column
    private boolean fixed;

    @Column
    private String fixedData;

    @GenericField
    @Column
    private boolean hacked;

    @Column
    private String hackedData;

    @GenericField
    @Column
    private boolean modified;

    @Column
    private String modifiedData;

    @GenericField
    @Column
    private boolean trained;

    @Column
    private String trainedData;

    @GenericField
    @Column
    private boolean translated;

    @Column
    private String translatedData;

    @GenericField
    @Column
    private boolean verified;

    @GenericField
    @Column
    private boolean pirated;

    @Column
    private String piratedData;

    @GenericField
    @Column
    private boolean virus;

    @Column
    private String virusData;

    @Column
    private String tagsExtra;

    @IndexedEmbedded(includeDepth = 1)
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_contmeta_contmetaset"))
    @ManyToOne(optional = false)
    @EqualsAndHashCode.Include
    private ContentMetadataSet contentMetadataSet;

    @OneToMany(mappedBy = "contentMetadata", cascade = CascadeType.ALL, orphanRemoval = true)
    // DO NOT!!! @IndexedEmbedded(includeDepth = 3)
    @OrderBy("name ASC")
    private Collection<ContentMetadataItem> items;

    public static UUID fromStringFile(String name) {
        var subs = StringUtils.substringsBetween(name, "[", "]");
        var value = subs[subs.length - 1];
        return UuidUtil.fromString64(value);
    }

    // @Transient
    //// @IndexingDependency(derivedFrom = @ObjectPath(@PropertyValue(propertyName = "contentMetadataSet")))
    // public UUID getContentMetadataSetId() {
    // return contentMetadataSet.getId();
    // }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var other = (ContentMetadata) o;

        return id != null && id.equals(other.id);
    }

    @Transient
    public List<ContentMetadataItem> getItemsByExtension(String extension) {
        return getItems().stream().filter(i -> i.isExtension(extension)).collect(Collectors.toList());
    }

    @KeywordField(name = FIELD_PREFIX, aggregable = Aggregable.YES, sortable = Sortable.YES, searchable = Searchable.YES)
    @IndexingDependency(derivedFrom = @ObjectPath(@PropertyValue(propertyName = "name")))
    @Transient
    public String getPrefix() {
        if (StringUtils.isBlank(name)) {
            return "-";
        }
        var trimmedName = name.trim();
        if (trimmedName.length() <= 0) {
            return "-";
        }
        var chartAtZero = trimmedName.charAt(0);
        if (Character.isDigit(chartAtZero)) {
            return "0";
        }
        if (!Character.isAlphabetic(chartAtZero)) {
            return "-";
        }

        if (trimmedName.length() == SINGLE_CHAR_NAME_LENGTH) {
            return String.valueOf(Character.toUpperCase(chartAtZero));
        }
        var chartAtOne = trimmedName.charAt(1);
        if (Character.isDigit(chartAtOne)) {
            return Character.toUpperCase(chartAtZero) + "0";
        }
        if (Character.isAlphabetic(chartAtOne)) {
            return String.format("%s%s", Character.toUpperCase(chartAtZero), Character.toLowerCase(chartAtOne));
        }
        return Character.toUpperCase(chartAtZero) + "-";
    }

    @KeywordField(name = FIELD_YEAR, aggregable = Aggregable.YES, sortable = Sortable.YES)
    @IndexingDependency(derivedFrom = @ObjectPath(@PropertyValue(propertyName = "date")))
    @Column
    public String getYear() {
        if (StringUtils.isNotBlank(date)) {
            var matcher = YEAR_PATTERN.matcher(date);
            if (matcher.find()) {
                return matcher.group(0);
            }
        }

        return YEAR_UNKNOWN;
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public String toStringFileUnique() {
        return StringUtils.replaceChars(
            String.format(
                "%s (%s)(%s)%s [%s]",
                getTitle(),
                StringUtils.isNotBlank(date) ? date : "-",
                StringUtils.isNotBlank(publisher) ? publisher : "-",
                StringUtils.isNotBlank(mediaOrder) ? "(" + mediaOrder + ")" : "",
                UuidUtil.toString64(getId())),
            "/\\",
            "");
    }
    public String toStringFile() {
        return StringUtils.replaceChars(getName(), "/\\", "");
    }
}
