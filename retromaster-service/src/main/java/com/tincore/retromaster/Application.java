package com.tincore.retromaster;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Paths;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        var customConfigLocation = System.getProperty("spring.config.additional-location");

        if (customConfigLocation == null) {
            var springProfilesActive = System.getProperty("spring.profiles.active");

            var propsPath = Paths.get(System.getProperty("user.home")).resolve(".config").resolve("retromaster" + (StringUtils.isNotBlank(springProfilesActive) ? "_" + springProfilesActive : "")).resolve("application.yaml").toString();
            System.setProperty("spring.config.additional-location", "optional:" + propsPath);
        }

        // https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using-spring-boot-restart-vs-reload
        // Restart/Reload causes serious issues with parameter binding in jpa
        System.setProperty("spring.devtools.restart.enabled", "false");

        SpringApplication.run(Application.class, args);
    }
}
