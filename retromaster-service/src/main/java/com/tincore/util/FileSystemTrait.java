package com.tincore.util;
// TEMPORARILY WHILE FIXING...
/*-
 * #%L
 * tincore-lib-common
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.tincore.util.lang.ThrowingConsumer.uConsumer;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public interface FileSystemTrait {

    Predicate<Path> BY_REGULAR_FILE = Files::isRegularFile;

    int MILLISECONDS_IN_SECOND = 1000;
    boolean ATOMIC_MOVE_ENABLED = true;

    static Path getSystemUserHome() {
        return Paths.get(System.getProperty("user.home"));
    }

    default Path copy(Path source, Path target) throws IOException {
        if (source.equals(target)) {
            return target;
        }
        createDirectoriesParent(target);
        return Files.copy(source, target, REPLACE_EXISTING);
    }

    default Path copyToPath(Path targetPath, InputStream inputStream) throws IOException {
        createDirectoriesParent(targetPath);
        IOUtils.copy(inputStream, Files.newOutputStream(targetPath));
        return targetPath;
    }

    default Path createDirectoriesParent(Path filePath) throws IOException {
        var parent = filePath.getParent();
        if (parent != null) {
            Files.createDirectories(parent);
        }
        return parent;
    }

    default void deleteEmptyDirectories(Path path) throws IOException {
        getDirectoriesByEmpty(path).stream().filter(d -> !d.equals(path)).forEach(uConsumer(this::deleteSilent));
    }

    default void deleteRecursive(Path path) throws IOException {
        FileUtils.deleteDirectory(path.toFile());
    }

    default void deleteRecursiveLinks(Path path) throws IOException {
        try (var s = Files.walk(path)) {
            s.filter(Files::isSymbolicLink).forEach(uConsumer(this::deleteSilent));
        }
    }

    default void deleteSilent(Path path) throws IOException {
        Files.deleteIfExists(path);
    }

    default Stream<Path> findPaths(Path path) throws IOException {
        if (Files.notExists(path)) {
            return Stream.empty();
        }
        return Files.walk(path);
    }

    default List<Path> getDirectoriesByEmpty(Path path) throws IOException {
        if (!Files.isDirectory(path)) {
            return Collections.emptyList();
        }

        List<Path> directories = new ArrayList<>();
        Files.walkFileTree(
            path,
            new SimpleFileVisitor<>() {
                private final Stack<Path> stack = new Stack<>();

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
                    if (!stack.empty()) {
                        directories.add(stack.pop());
                    }
                    return CONTINUE;
                }

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    // if (Files.isSymbolicLink(dir)){
                    // return SKIP_SUBTREE;
                    // }
                    stack.push(dir);
                    return super.preVisitDirectory(dir, attrs);
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (!Files.isDirectory(file) || Files.isSymbolicLink(file)) {
                        stack.clear();
                    }
                    return super.visitFile(file, attrs);
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) {
                    return CONTINUE;
                }
            });

        return directories;
    }

    default FileTime getFileTimeNow() {
        return FileTime.from(System.currentTimeMillis() / MILLISECONDS_IN_SECOND, TimeUnit.SECONDS);
    }

    default LocalDateTime getLastModifiedTime(Path path) throws IOException {
        return LocalDateTime.ofInstant(Files.getLastModifiedTime(path).toInstant(), ZoneId.systemDefault());
    }

    default Path getPathUniqueFile(Path path) {
        if (!Files.exists(path)) {
            return path;
        }

        var oldFileName = path.getFileName().toString();

        var baseName = FilenameUtils.getBaseName(oldFileName);

        var extension1 = FilenameUtils.getExtension(oldFileName);
        var extension2 = FilenameUtils.getExtension(baseName);

        var nameUniqueComponent = System.currentTimeMillis();
        var newFileName = String.format("%s_%d%s%s", baseName, nameUniqueComponent, StringUtils.isNotBlank(extension2) ? "." + extension2 : "", StringUtils.isNotBlank(extension1) ? "." + extension1 : "");

        return path.resolveSibling(newFileName);
    }

    default boolean isEmpty(Path path, FileVisitOption... options) throws IOException {
        if (!Files.isDirectory(path)) {
            return false;
        }
        try (var s = Files.walk(path, 1, options)) {
            return s.allMatch(p -> p.equals(path));
        }
    }

    default void linkOrCopy(Path targetProjectionFilePath, Path contentPath) throws IOException {
        try {
            Files.createSymbolicLink(targetProjectionFilePath, contentPath);
        } catch (UnsupportedOperationException e) { // If link is not supported should copy
            Files.copy(targetProjectionFilePath, contentPath);
        }
    }

    default Path move(Path source, Path target) throws IOException {
        if (source.equals(target)) {
            return target;
        }
        createDirectoriesParent(target);
        try {
            if (ATOMIC_MOVE_ENABLED) {
                return Files.move(source, target, REPLACE_EXISTING, ATOMIC_MOVE);
            } else {
                return Files.move(source, target, REPLACE_EXISTING);
            }
        } catch (AtomicMoveNotSupportedException e) {
            return Files.move(source, target, REPLACE_EXISTING);
        }
    }

    // improve. Target exists and has same name and size maybe quick calculate crc and just remove origin
    default Path moveToUnique(Path filePath, Path targetPath) throws IOException {
        var uniqueTargetPath = getPathUniqueFile(targetPath);
        return move(filePath, uniqueTargetPath);
    }

    default Path relocateTo(Path targetRootPath, Path sourceRootPath, Path sourcePath) {
        return targetRootPath.resolve(sourceRootPath.relativize(sourcePath));
    }

    default Path touch(Path path, FileTime fileTime) throws IOException {
        return Files.setLastModifiedTime(path, fileTime);
    }

    default List<Path> touch(List<Path> paths, FileTime fileTime) throws IOException {
        for (var path : paths) {
            touch(path, fileTime);
        }
        return paths;
    }

    // DOES NOT WORK FINE
    default void touchAndCheck(Path path) throws IOException {
        var time = FileTime.from(System.currentTimeMillis() / MILLISECONDS_IN_SECOND, TimeUnit.SECONDS);
        Files.setLastModifiedTime(path, time);
        // long size = Files.size(path);
        // FileTime time2 = Files.getLastModifiedTime(path);
        // if (!time2.equals(time)) {
        // log.error("file still open?" + path);
        // }
        // log.debug("create [" + time.toMillis() + "][" + time2.toMillis() + "]" + path);
    }

    default void truncate(Path path, long size) throws IOException {
        try (var sbc = Files.newByteChannel(path, StandardOpenOption.WRITE)) {
            sbc.truncate(size);
        }
    }

    class DirectoriesFilter implements DirectoryStream.Filter<Path> {
        @Override
        public boolean accept(Path entry) {
            return Files.isDirectory(entry);
        }
    }
}
