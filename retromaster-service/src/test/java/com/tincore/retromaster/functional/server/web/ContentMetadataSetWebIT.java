package com.tincore.retromaster.functional.server.web;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.WebElement;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "Content Metadata Set", tags = "WEB")
@TestMethodOrder(MethodOrderer.MethodName.class)
public class ContentMetadataSetWebIT extends AbstractWebIT {

    @Test
    public void testContentMetadataSetPageGivenContentMetadataWhenRowClickedThenDisplaysContentMetadataDetails() {
        var contentMetadataSet = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());
        var contentMetadata = tFS.getContentMetadataRepositoryService().save(createContentMetadata(contentMetadataSet, createName()));

        setUpJwtAut();
        setUpContentMetadataSetPageDisplayed(contentMetadataSet);

        waitForVisibility(contentMetadataSetPage.contentMetadatas, contentMetadata.getId().toString());
        contentMetadataSetPage.getContentMetadataByUuid(contentMetadata.getId().toString()).click();

        var contentMetadataComponent = waitForVisibility(contentMetadataPage.component);
        assertThat(contentMetadataComponent.isDisplayed()).isTrue();
        var qualifier = waitForSemanticQualifier(contentMetadataComponent);
        assertThat(qualifier).isNotNull().isEqualTo(contentMetadata.getId().toString());
    }

    @Test
    public void testContentMetadataSetPageGivenContentMetadatasThenContentMetadatasSortedByTitle() {
        var contentMetadataSet = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());
        var contentMetadata1 = tFS.getContentMetadataRepositoryService().save(createContentMetadata(contentMetadataSet, createName()));
        var contentMetadata2 = tFS.getContentMetadataRepositoryService().save(createContentMetadata(contentMetadataSet, createName()));

        setUpJwtAut();
        setUpContentMetadataSetPageDisplayed(contentMetadataSet);
        var sortedNames = Stream.of(contentMetadata1, contentMetadata2).map(ContentMetadata::getTitle).sorted().toArray(String[]::new);

        assertThat(contentMetadataSetPage.contentMetadatas.size()).isEqualTo(sortedNames.length);
        assertThat(contentMetadataSetPage.contentMetadatasTitles.stream().map(WebElement::getText)).containsExactly(sortedNames);
    }

    @Test
    public void testContentMetadataSetPageGivenContentMetadatasThenDisplaysContentMetadatas() {
        var contentMetadataSet = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());
        var contentMetadata = tFS.getContentMetadataRepositoryService().save(createContentMetadata(contentMetadataSet, createName()));

        setUpJwtAut();

        setUpContentMetadataSetPageDisplayed(contentMetadataSet);

        assertThat(contentMetadataSetPage.getContentMetadataByTitle(contentMetadata.getTitle())).isPresent();
    }

    @Test
    public void testContentMetadataSetPageGivenNoContentMetadatasThenNotDisplayContentMetadatas() {
        var contentMetadataSet = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());

        setUpJwtAut();
        setUpContentMetadataSetPageDisplayed(contentMetadataSet);

        assertThat(contentMetadataSetPage.component.isDisplayed()).isTrue();
        assertThat(contentMetadataSetPage.contentMetadatas).isEmpty();
    }
}
