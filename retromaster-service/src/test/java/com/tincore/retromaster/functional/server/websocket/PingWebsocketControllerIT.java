package com.tincore.retromaster.functional.server.websocket;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jakarta.websocket.DeploymentException;
import org.junit.jupiter.api.Test;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.web.client.HttpClientErrorException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.*;

public class PingWebsocketControllerIT extends AbstractWebsocketControllerIT {

    private final String payload = "pingSend";

    private void assertResponseReceivedSuccessfully(CompletableFuture<byte[]> completableFuture) throws Exception {
        assertThat(completableFuture.get())
            .satisfies(
                r -> {
                    assertThat(r).isNotNull();
                    assertThat(new String(r)).isEqualTo("echo " + payload);
                });
    }

    private void setUpPingSend(StompSession stompSession) {
        stompSession.send("/retromaster/ping", payload.getBytes());
    }

    @Test
    public void testPingGivenNonAuthorizedUserThenError() {
        assertThatExceptionOfType(ExecutionException.class)
            .isThrownBy(
                () -> {
                    var stompSession = createStompSession(null);

                    final var completableFuture = new CompletableFuture<byte[]>();
                    addStompSubscription(stompSession, "/topic/echo", createStompFrameHandler(byte[].class, completableFuture));

                    setUpPingSend(stompSession);

                    waitForResponse(completableFuture);
                    assertThatThrownBy(() -> assertResponseReceivedSuccessfully(completableFuture)).hasCauseExactlyInstanceOf(HttpClientErrorException.class);
                })
            .withCauseExactlyInstanceOf(DeploymentException.class);
    }

    @Test
    public void testPingGivenSubscribedToEchoAndAdminUserWhenPingSentThenEchoReceived() throws Exception {
        var stompSession = createStompSession("ADMIN");

        final var completableFuture = new CompletableFuture<byte[]>();
        addStompSubscription(stompSession, "/topic/echo", createStompFrameHandler(byte[].class, completableFuture));

        setUpPingSend(stompSession);

        waitForResponse(completableFuture);

        assertResponseReceivedSuccessfully(completableFuture);
    }

    @Test
    public void testPingGivenSubscribedToEchoAndAnonymousAuthorizedUserWhenPingSentThenEchoReceived() throws Exception {
        var stompSession = createStompSession("NONE");

        final var completableFuture = new CompletableFuture<byte[]>();
        addStompSubscription(stompSession, "/topic/echo", createStompFrameHandler(byte[].class, completableFuture));

        setUpPingSend(stompSession);

        waitForResponse(completableFuture);

        assertResponseReceivedSuccessfully(completableFuture);
    }
}
