package com.tincore.retromaster.functional.server.web.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.selenium.AbstractPage;
import com.tincore.test.support.selenium.SeleniumPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@SeleniumPage
public class ContentMetadataSetsPage extends AbstractPage {

    @FindBy(css = "[data-sem=contentMetadataSets_component]")
    public WebElement component;

    @FindBy(css = "[data-sem=progressBar]")
    public WebElement progressBar;

    @FindBy(css = "[data-sem=back_do]")
    public WebElement back;

    @FindBy(css = "[data-sem=delete_do]")
    public WebElement delete;

    @FindBy(css = "[data-sem=menu_do]")
    public WebElement menu;

    @FindBy(css = "[data-sem=menuDisplay_do]")
    public WebElement menuDisplay;

    @FindBy(css = "[data-sem=query]")
    public WebElement query;

    @FindBy(css = "[data-sem=nameSort_do]")
    public WebElement nameSort;

    @FindBy(css = "[data-sem=contentMetadataSet_container] [data-sem=name]")
    public List<WebElement> contentMetadataSetsNames;

    @FindBy(css = "[data-sem=contentMetadataSet_container]")
    public List<WebElement> contentMetadataSets;

    @Autowired
    public PaginatorComponentPage paginatorComponentPage;

    @Autowired
    public DeleteConfirmComponentPage deleteConfirmComponentPage;

    public ContentMetadataSetsPage(@Autowired WebDriver driver) {
        super(driver);
    }

    public void doDeleteContentMetadataSetByUuid(String uuid) {
        getContentMetadataSetByUuid(uuid).findElement(By.cssSelector("[data-sem=select_do]")).click();
        delete.click();
//        getContentMetadataSetByUuid(uuid).findElement(By.cssSelector("[data-sem=delete_do]")).click();
    }

    public Optional<WebElement> getContentMetadataSetByName(String name) {
        return contentMetadataSetsNames.stream().filter(n -> name.equals(n.getText())).findFirst();
    }

    public WebElement getContentMetadataSetByUuid(String uuid) {
        return contentMetadataSets.stream().filter(w -> uuid.equals(waitForSemanticQualifier(w))).findFirst().orElseThrow(() -> new RuntimeException("Not found by UUID " + uuid));
    }

    public String getContentMetadataSetCategoryByUuid(String uuid) {
        return getContentMetadataSetByUuid(uuid).findElement(By.cssSelector("[data-sem=category]")).getText();
    }

    public String getContentMetadataSetNameByUuid(String uuid) {
        return getContentMetadataSetByUuid(uuid).findElement(By.cssSelector("[data-sem=name]")).getText();
    }

    public String getContentMetadataSetSystemByUuid(String uuid) {
        return getContentMetadataSetByUuid(uuid).findElement(By.cssSelector("[data-sem=system]")).getText();
    }

    public String getContentMetadataSetVersionByUuid(String uuid) {
        return getContentMetadataSetByUuid(uuid).findElement(By.cssSelector("[data-sem=version]")).getText();
    }

    public void waitForVisibility() {
        waitForVisibility(component);
        waitForInvisibility(progressBar);
    }
}
