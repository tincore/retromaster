package com.tincore.retromaster.functional.server.web.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.selenium.AbstractPage;
import com.tincore.test.support.selenium.SeleniumPage;
import com.tincore.test.support.selenium.SeleniumTrait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;

@SeleniumPage
public class HomePage extends AbstractPage implements SeleniumTrait {

    @FindBy(css = "[data-sem=search_component]")
    public WebElement component;

    @FindBy(css = "[data-sem=menu_do]")
    public WebElement menu;

    @FindBy(css = "[data-sem=back_do]")
    public WebElement back;

    @FindBy(css = "[data-sem=errorGeneral]")
    public WebElement generalError;

    public HomePage(@Autowired WebDriver driver) {
        super(driver);
    }

    public void doBack() {
        back.click();
    }

    public void waitForVisibility() {
        waitForVisibility(component);
    }
}
