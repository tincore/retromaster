package com.tincore.retromaster.functional.server.thrift;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.functional.support.AbstractApiIT;
import com.tincore.retromaster.thrift.RetromasterAuthService;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.THttpClient;
import org.apache.thrift.transport.TTransportException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractRetromasterAuthServiceThriftApiIT extends AbstractApiIT {

    @Autowired
    protected TProtocolFactory tProtocolFactory;

    @Value("${tincore.thrift.authPath}")
    protected String path;

    THttpClient transport;

    public RetromasterAuthService.Client clientAdmin() {
        return createClient("ADMIN");
    }

    public RetromasterAuthService.Client createClient(String role) {
        setUpSecurity(transport, role);
        transport.open();
        return new RetromasterAuthService.Client(tProtocolFactory.getProtocol(transport));
    }

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        transport = setUpThriftHttpTransport();
    }

    public void setUpSecurity(THttpClient transport, String role) {
        var token = createJwtToken(role);
        transport.setCustomHeader(securityJwtHeaderName, "Bearer " + token);
    }

    public THttpClient setUpThriftHttpTransport() throws TTransportException {
        return new THttpClient(serverProtocol + "://" + serverHost + ":" + port + path);
    }

    @AfterEach
    public void tearDown() {
        transport.close();
    }
}
