package com.tincore.retromaster.functional.server.fuse;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.FuseConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.functional.support.AbstractApiIT;
import com.tincore.retromaster.server.fuse.RetromasterFuseFsController;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.nio.file.Path;

public abstract class AbstractRetromasterFuseFsControllerIT extends AbstractApiIT {

    protected final String contentData1 = createString("someContent");
    protected final String contentData2 = createString("someContent");
    protected final String system = createString("SYSTEM_");

    @SpyBean
    public FuseConfiguration fuseConfiguration;
    @Autowired
    public RetromasterFuseFsController retromasterFuseFsController;

    protected ContentVolume productionVolume;

    public Path getFuseFsRootPath() {
        return retromasterFuseFsController.getPath();
    }

    public Path getFuseFsRootPathReadWrite() {
        return retromasterFuseFsController.getPathReadWrite();
    }

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();

        productionVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data"), true);
    }

    public ContentMetadataItem setUpPersistedContentMetadataItemWithMatchingCompressedContent(String contentMetadataItemName, String contentData) {
        var content = tFS.setUpPersistedContent(productionVolume, "somePath/" + tFS.createFilename("zip"), "testEntry.txt", true, contentData.getBytes());
        return setUpPersistedContentMetadataItemWithMatchingContent(content, contentMetadataItemName);
    }

    public ContentMetadataItem setUpPersistedContentMetadataItemWithMatchingContent(Content content, String contentMetadataItemName) {
        var contentMetadataItem = createContentMetadataItem(contentMetadataItemName, content);
        var contentMetadata = createContentMetadata(null, "cm_1", contentMetadataItem);
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(createContentMetadataSetTosec(system, createName(), contentMetadata));
        return contentMetadataSet.getContentMetadatas().get(0).getItems().iterator().next();
    }

    @AfterEach
    public void tearDown() {
        super.tearDown();
    }
}
