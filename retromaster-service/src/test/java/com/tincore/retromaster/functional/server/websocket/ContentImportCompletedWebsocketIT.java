package com.tincore.retromaster.functional.server.websocket;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.event.ContentImportEvent;
import com.tincore.retromaster.domain.event.ContentImportEventType;
import com.tincore.retromaster.server.websocket.event.RetromasterWebsocketEventHandler;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ContentImportCompletedWebsocketIT extends AbstractWebsocketControllerIT {

    private final ContentImportEvent contentImportEvent = createContentImportEvent(ContentImportEventType.SUCCESS);

    @Autowired
    private RetromasterWebsocketEventHandler retromasterWebsocketEventHandler;

    private void setUpContentImportCompletedEventSend(ContentImportEvent contentImportEvent) {
        // Flooding because there is something wrong in the test setup
        IntStream.range(0, 15).forEach(i -> retromasterWebsocketEventHandler.onContentImportEvent(contentImportEvent));
    }

    @Test
    public void testTopicContentImportCompletedGivenSubscribedAndUserHasNoPermissionsWhenEventHandlerExecutedThenNoMessageReceived() throws Exception {
        var stompSession = createStompSession("NONE");

        final var completableFuture = new CompletableFuture<byte[]>();
        addStompSubscription(stompSession, "/topic/contentImport", createStompFrameHandler(byte[].class, completableFuture));

        setUpContentImportCompletedEventSend(contentImportEvent);

        assertThatThrownBy(() -> waitForResponse(completableFuture));
    }

    @Test
    public void testTopicContentImportCompletedGivenSubscribedAndUserHasPermissionsWhenEventHandlerExecutedTwiceThenMessagesReceived() throws Exception {
        var stompSession1 = createStompSession("ADMIN");

        var completableFuture1 = new CompletableFuture<byte[]>();
        var completableFuture2 = new CompletableFuture<byte[]>();

        var subscription1 = stompSession1.subscribe(
            "/topic/contentImport",
            new StompFrameHandler() {
                @Override
                public Type getPayloadType(StompHeaders stompHeaders) {
                    return byte[].class;
                }

                @Override
                public void handleFrame(StompHeaders stompHeaders, Object o) {
                    if (!completableFuture1.isDone()) {
                        completableFuture1.complete((byte[]) o);
                    } else {
                        completableFuture2.complete((byte[]) o);
                    }
                }
            });
        stompSubscriptions.add(subscription1);

        setUpContentImportCompletedEventSend(contentImportEvent);
        setUpContentImportCompletedEventSend(contentImportEvent);

        waitForResponse(completableFuture1);
        waitForResponse(completableFuture2);
        assertThat(completableFuture1.get()).isNotNull();
        assertThat(completableFuture2.get()).isNotNull();
    }

    @Test
    public void testTopicContentImportCompletedGivenSubscribedToWhenEventHandlerExecutedThenMessageReceived() throws Exception {
        var stompSession = createStompSession("ADMIN");

        final var completableFuture = new CompletableFuture<byte[]>();
        addStompSubscription(stompSession, "/topic/contentImport", createStompFrameHandler(byte[].class, completableFuture));

        setUpContentImportCompletedEventSend(contentImportEvent);

        waitForResponse(completableFuture);
        var actual = completableFuture.get();
        assertThat(actual).isNotNull();
        assertDeserializedEquals(actual, contentImportEvent);
    }

    @Test
    public void testTopicContentImportCompletedGivenTwoSubscribedAndOneUserHasPermissionsAndOtherUserHasNotWhenEventHandlerExecutedThenMessageReceived() throws Exception {
        var stompSession1 = createStompSession("ADMIN");
        final var completableFuture1 = new CompletableFuture<byte[]>();
        addStompSubscription(stompSession1, "/topic/contentImport", createStompFrameHandler(byte[].class, completableFuture1));

        var stompSession2 = createStompSession("NONE");
        final var completableFuture2 = new CompletableFuture<byte[]>();
        addStompSubscription(stompSession2, "/topic/contentImport", createStompFrameHandler(byte[].class, completableFuture2));

        setUpContentImportCompletedEventSend(contentImportEvent);
        setUpContentImportCompletedEventSend(contentImportEvent);

        waitForResponse(completableFuture1);
        assertThat(completableFuture1.get()).isNotNull();

        assertThatThrownBy(() -> waitForResponse(completableFuture2));
    }

    @Test
    public void testTopicContentImportCompletedGivenTwoSubscribedAndUserHasPermissionsWhenEventHandlerExecutedThenMessageReceived() throws Exception {
        var stompSession1 = createStompSession("ADMIN");
        var stompSession2 = createStompSession("ADMIN");

        final var completableFuture1 = new CompletableFuture<byte[]>();
        addStompSubscription(stompSession1, "/topic/contentImport", createStompFrameHandler(byte[].class, completableFuture1));
        final var completableFuture2 = new CompletableFuture<byte[]>();
        addStompSubscription(stompSession2, "/topic/contentImport", createStompFrameHandler(byte[].class, completableFuture2));

        setUpContentImportCompletedEventSend(contentImportEvent);

        waitForResponse(completableFuture1);
        waitForResponse(completableFuture2);
        assertThat(completableFuture1.get()).isNotNull();
        assertThat(completableFuture2.get()).isNotNull();
    }
}
