package com.tincore.retromaster.functional.server.websocket;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.event.ContentImportEvent;
import com.tincore.retromaster.functional.support.AbstractApiIT;
import com.tincore.retromaster.thrift.form.TContentImportEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.thrift.TDeserializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@Slf4j
public abstract class AbstractWebsocketControllerIT extends AbstractApiIT {
    private final List<StompSession> stompSessions = new ArrayList<>();
    private final List<WebSocketStompClient> stompClients = new ArrayList<>();
    private final boolean useStandardClient = true;
    public String webSocketUrl;
    public List<StompSession.Subscription> stompSubscriptions = new ArrayList<>();
    public String endPoint = "socket";
    @Autowired
    public TDeserializer tDeserializer;

    public <T> void addStompSubscription(StompSession stompSession, String destination, StompFrameHandler stompFrameHandler) {
        var subscribe = stompSession.subscribe(destination, stompFrameHandler);
        stompSubscriptions.add(subscribe);
    }

    public void assertDeserializedEquals(byte[] data, ContentImportEvent event) {
        assertThat(data).isNotNull();

        var base = new TContentImportEvent();
        var payload = Base64.getDecoder().decode(new String(data));
        try {
            tDeserializer.deserialize(base, payload);
        } catch (Exception e) {
            log.error("Cant deserialize received {} error:{}", new String(data), e);
        }
        // Should eventually: Fix timing issue
        // assertThat(base.getExecutionId()).isEqualTo(event.getExecutionId().toString64());
        // assertThat(base.getSubscriptionId()).isEqualTo(event.getSubscriptionId().toString64());
    }

    public <T> StompFrameHandler createStompFrameHandler(Class<T> type, CompletableFuture<T> completableFuture) {
        return new StompFrameHandler() {
            @Override
            public Type getPayloadType(StompHeaders stompHeaders) {
                return type;
            }

            @Override
            public void handleFrame(StompHeaders stompHeaders, Object o) {
                completableFuture.complete((T) o);
            }
        };
    }

    protected StompSession createStompSession(String role) throws InterruptedException, ExecutionException, TimeoutException {
        var stompClient = createWebSocketStompClient();
        // Using SimpleConverter bytes <-> bytes
        // stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        stompClients.add(stompClient);

        var stompSession = stompClient.connect(webSocketUrl, createWebSocketHttpHeaders(role), createStompSessionHandler()).get(1, SECONDS);
        stompSessions.add(stompSession);

        await().atMost(1, SECONDS).until(stompSession::isConnected);

        return stompSession;
    }

    public StompSessionHandler createStompSessionHandler() {
        return new StompSessionHandler() {
            @Override
            public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
                log.trace("afterConnected {} {}", session, connectedHeaders);
            }

            @Override
            public Type getPayloadType(StompHeaders headers) {
                return String.class;
            }

            @Override
            public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
                log.trace("handleException {} {} {}", session, headers, exception);
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                log.trace("handleFrame {} {}", headers, payload);
            }

            @Override
            public void handleTransportError(StompSession session, Throwable exception) {
                log.error("handleTransportError {}  ", session, exception);
            }
        };
    }

    public WebSocketHttpHeaders createWebSocketHttpHeaders(String role) {
        var headers = new WebSocketHttpHeaders();
        if (role != null) {
            var token = createJwtToken(role);
            headers.set(securityJwtHeaderName, "Bearer " + token);
        }
        return headers;
    }

    public WebSocketStompClient createWebSocketStompClient() {
        WebSocketClient webSocketClient;
        if (useStandardClient) {
            webSocketClient = new StandardWebSocketClient();
        } else {
            webSocketClient = new SockJsClient(createWebSocketTransport());
        }
        return new WebSocketStompClient(webSocketClient);
    }

    public List<Transport> createWebSocketTransport() {
        return Stream.of(new WebSocketTransport(new StandardWebSocketClient())).collect(Collectors.toList());
    }

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        webSocketUrl = String.format("ws://localhost:%d/%s", port, endPoint);
    }

    @AfterEach
    public void tearDown() {
        super.tearDown();
        stompSubscriptions.forEach(
            s -> {
                try {
                    s.unsubscribe();
                } catch (Exception e) {
                    log.info("Error unsubscribe from stomp msg={}", e.getMessage());
                }
            });
        stompSessions.forEach(
            s -> {
                try {
                    s.disconnect();
                } catch (Exception e) {
                    log.info("Error disconnect from stomp msg={}", e.getMessage());
                }
            });
        stompClients.forEach(
            c -> {
                try {
                    c.stop();
                } catch (Exception e) {
                    log.info("Error stop stomp client msg={}", e.getMessage());
                }
            });
    }

    public <T> void waitForResponse(CompletableFuture<T> completableFuture) {
        await().atMost(15, SECONDS).pollInterval(Duration.ofMillis(500)).until(completableFuture::isDone);
    }
}
