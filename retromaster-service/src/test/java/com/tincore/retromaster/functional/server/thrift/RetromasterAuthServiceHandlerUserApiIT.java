package com.tincore.retromaster.functional.server.thrift;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(
                scenario = "Retromaster API: User",
                tags = {"API", "Thrift"})
public class RetromasterAuthServiceHandlerUserApiIT extends AbstractRetromasterAuthServiceThriftApiIT {

    @Test
    public void testGetUserMeUpdateGivenUserAdminThenUserWithAuthoritiesIsReturned() throws Exception {
        var userForm = clientAdmin().getUserMe();

        assertThat(userForm).isNotNull();
        assertThat(userForm.getName()).isEqualTo(USER_NAME);
        assertThat(userForm.getAuthorities())
            .containsExactlyInAnyOrder(
                "CONTENTMETADATASET_READ",
                "CONTENTMETADATASET_EDIT",
                "CONTENTMETADATASET_DELETE",
                "CONTENTMETADATAITEM_READ",
                "CONTENTMETADATAITEM_EDIT",
                "CONTENTMETADATAITEM_DELETE",
                "CONTENTMETADATA_READ",
                "CONTENTMETADATA_EDIT",
                "CONTENTMETADATA_DELETE",
                "CONTENTVOLUME_READ",
                "CONTENTVOLUME_EDIT",
                "CONTENTVOLUME_DELETE",
                "CONTENT_READ",
                "CONTENT_EDIT",
                "CONTENT_DELETE",
                "CONTENT_EXECUTE",
                "CONTENT_IMPORT");
    }
}
