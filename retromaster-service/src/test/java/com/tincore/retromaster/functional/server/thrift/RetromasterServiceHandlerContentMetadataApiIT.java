package com.tincore.retromaster.functional.server.thrift;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.thrift.form.TContentMetadataFilter;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(
                scenario = "Retromaster API: Content Metadata",
                tags = {"API", "Thrift"})
public class RetromasterServiceHandlerContentMetadataApiIT extends AbstractRetromasterServiceThriftApiIT {

    @Test
    public void testFindContentMetadatasByFilterGivenPersistedContentMetadatasWhenEmptyFilterThenContentMetadatasAreReturned() throws Exception {
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(tFS.createName(), "TOSEC", "2019-01-01"));
        IntStream.range(0, 39).forEach(i -> tFS.setUpPersistedContentMetadata(contentMetadataSet, tFS.createName()));

        var filter = new TContentMetadataFilter();

        var page = createTPageable(0);

        var form = clientAdmin().findContentMetadatasByFilter(filter, page);

        assertThat(form.totalElements).isEqualTo(39);
        assertThat(form.totalPages).isEqualTo(4);
        assertThat(form.size).isEqualTo(page.size);
        assertThat(form.number).isEqualTo(page.page);
        assertThat(form.content).hasSize((int) page.size);
    }
}
