package com.tincore.retromaster.functional.server.fuse;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.service.archive.producer.BytesArchiveEntryProducer;
import com.tincore.util.FileSystemTrait;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;

import static com.tincore.retromaster.domain.Content.COMPRESS_SEPARATOR;
import static com.tincore.retromaster.server.fuse.content.ContentFuseFsEntryFactory.NAME_BY_SYSTEM;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

// Attention only runs on linux when fuse is available. Create assumptions!
@Slf4j
public class RetromasterFuseFsControllerReadWriteIT extends AbstractRetromasterFuseFsControllerIT implements FileSystemTrait {

    public Path getContentMetadataPath(ContentMetadataItem contentMetadataItem) {
        return getContentMetadataPath(contentMetadataItem.getContentMetadata());
    }

    public Path getContentMetadataPath(ContentMetadata contentMetadata) {
        return getFuseFsRootPathReadWrite()
            .resolve(NAME_BY_SYSTEM)
            .resolve(contentMetadata.getSystem())
            .resolve(contentMetadata.getPrefix())
            .resolve(contentMetadata.toStringFileUnique());
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenContentMetadataItemFileDeletedAndNewFileCreatedWithSameNameThenNewFileIsCreatedSuccessfully() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        Files.delete(contentMetadataItemPath);

        FileUtils.writeStringToFile(contentMetadataItemPath.toFile(), contentData2, Charset.defaultCharset());

        assertThat(contentMetadataItemPath)
            .exists()
            .isRegularFile()
            .hasContent(contentData2);
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenContentMetadataItemFileDeletedThenFileIsNotPresent() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        Files.delete(contentMetadataItemPath);

        assertThat(contentMetadataItemPath).doesNotExist();
        assertThat(isEmpty(contentMetadataPath)).isTrue();
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenContentMetadataItemFileModifiedThenFileContainsModifications() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        FileUtils.writeStringToFile(contentMetadataItemPath.toFile(), contentData2, Charset.defaultCharset());

        var fileContent = FileUtils.readFileToString(contentMetadataItemPath.toFile(), Charset.defaultCharset());
        assertThat(fileContent).isEqualTo(contentData2);
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenNewDirectoryCreatedInSameDirectoryAsContentFileThenDirectoryIsCreatedSuccessfully() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var targetPath = contentMetadataPath.resolve("testDir");

        assertThat(Files.createDirectories(targetPath)).exists().isDirectory();
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenNewFileCreatedInSameDirectoryAsContentMetadataItemFileThenNewFileIsCreatedSuccessfully() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var targetPath = contentMetadataPath.resolve("testFile.txt");
        FileUtils.writeStringToFile(targetPath.toFile(), contentData2, Charset.defaultCharset());

        assertThat(targetPath).exists()
            .isRegularFile()
            .hasContent(contentData2);
    }

    @Test
    public void testFilesystemGivenDirectoriesOnContentMetadataDirectoryAndRenameThenDirectoriesRenamed() throws Exception {
        var compressedFileRelativePath = "somePath/" + tFS.createFilename("zip");
        var compressedFileAbsolutePath = productionVolume.toPath().resolve(compressedFileRelativePath);
        tFS.setUpFileArchived(compressedFileAbsolutePath,
            new BytesArchiveEntryProducer(contentData1.getBytes(), "testEntry1.txt"),
            new BytesArchiveEntryProducer(contentData2.getBytes(), "subDir1/subDir2/testEntry2.txt")
        );
        var lastModifiedTime = Files.exists(compressedFileAbsolutePath) ? tFS.getLastModifiedTime(compressedFileAbsolutePath) : LocalDateTime.MIN;

        var content1 = tFS.getContentRepositoryService().save(tFS.createContent(productionVolume, compressedFileRelativePath + COMPRESS_SEPARATOR + "testEntry1.txt", lastModifiedTime, contentData1.getBytes()));
        var contentMetadataItem1 = createContentMetadataItem("recognized1.ext", content1);

        var content2 = tFS.getContentRepositoryService().save(tFS.createContent(productionVolume, compressedFileRelativePath + COMPRESS_SEPARATOR + "subDir1/subDir2/testEntry2.txt", lastModifiedTime, contentData2.getBytes()));
        var contentMetadataItem2 = createContentMetadataItem("subDirA/subDirB/recognized2.ext", content2);

        var contentMetadata = createContentMetadata(null, "cm_1", contentMetadataItem1, contentMetadataItem2);
        tFS.setUpPersistedContentMetadataSet(createContentMetadataSetTosec(system, createName(), contentMetadata));

        var contentMetadataPath = getContentMetadataPath(contentMetadata);

        var contentMetadataDir1 = contentMetadataPath.resolve("subDirA");
        assertThat(contentMetadataDir1).exists().isDirectory();

        var contentMetadataDir1Moved = contentMetadataDir1.resolveSibling("subDirARename");
        assertThat(contentMetadataDir1.toFile().renameTo(contentMetadataDir1Moved.toFile())).isTrue();


        await().atMost(1, SECONDS).untilAsserted(() -> assertThat(contentMetadataDir1.toFile()).doesNotExist());
        assertThat(contentMetadataDir1Moved).exists()
            .isDirectory();


        var contentMetadataDir2 = contentMetadataDir1Moved.resolve("subDirB");
        assertThat(contentMetadataDir2).exists()
            .isDirectory();
        var contentMetadataDir2Moved = contentMetadataDir2.resolveSibling("subDirBRename");
        assertThat(contentMetadataDir2.toFile().renameTo(contentMetadataDir2Moved.toFile())).isTrue();

        await().atMost(1, SECONDS).untilAsserted(() -> assertThat(contentMetadataDir2).doesNotExist());
        assertThat(contentMetadataDir2Moved).exists()
            .isDirectory();
    }

    @Test
    public void testFilesystemGivenFilesOnContentMetadataDirectoryAndRenameThenFilesRenamed() throws Exception {
        var compressedFileRelativePath = "somePath/" + tFS.createFilename("zip");
        var compressedFileAbsolutePath = productionVolume.toPath().resolve(compressedFileRelativePath);
        tFS.setUpFileArchived(compressedFileAbsolutePath,
            new BytesArchiveEntryProducer(contentData1.getBytes(), "testEntry1.txt"),
            new BytesArchiveEntryProducer(contentData2.getBytes(), "subDir1/subDir2/testEntry2.txt")
        );
        var lastModifiedTime = Files.exists(compressedFileAbsolutePath) ? tFS.getLastModifiedTime(compressedFileAbsolutePath) : LocalDateTime.MIN;

        var content1 = tFS.getContentRepositoryService().save(tFS.createContent(productionVolume, compressedFileRelativePath + COMPRESS_SEPARATOR + "testEntry1.txt", lastModifiedTime, contentData1.getBytes()));
        var contentMetadataItem1 = createContentMetadataItem("recognized1.ext", content1);

        var content2 = tFS.getContentRepositoryService().save(tFS.createContent(productionVolume, compressedFileRelativePath + COMPRESS_SEPARATOR + "subDir1/subDir2/testEntry2.txt", lastModifiedTime, contentData2.getBytes()));
        var contentMetadataItem2 = createContentMetadataItem("subDirA/subDirB/recognized2.ext", content2);

        var contentMetadata = createContentMetadata(null, "cm_1", contentMetadataItem1, contentMetadataItem2);
        tFS.setUpPersistedContentMetadataSet(createContentMetadataSetTosec(system, createName(), contentMetadata));

        var contentMetadataPath = getContentMetadataPath(contentMetadata);

        var contentMetadataFile1 = contentMetadataPath.resolve(contentMetadataItem1.getName());
        assertThat(contentMetadataFile1).exists()
            .isRegularFile()
            .hasContent(contentData1);

        var contentMetadataFile1Moved = contentMetadataFile1.resolveSibling("renamed1.txt");
        Files.move(contentMetadataFile1, contentMetadataFile1Moved);

        await().atMost(1, SECONDS).untilAsserted(() -> assertThat(contentMetadataFile1).doesNotExist());
        assertThat(contentMetadataFile1Moved).exists()
            .isRegularFile()
            .hasContent(contentData1);


        var contentMetadataFile2 = contentMetadataPath.resolve(contentMetadataItem2.getName());
        assertThat(contentMetadataFile2).exists()
            .isRegularFile()
            .hasContent(contentData2);

        var contentMetadataFile2Moved = contentMetadataFile2.resolveSibling("renamed2.txt");
        Files.move(contentMetadataFile2, contentMetadataFile2Moved);

        await().atMost(1, SECONDS).untilAsserted(() -> assertThat(contentMetadataFile2).doesNotExist());
        assertThat(contentMetadataFile2Moved).exists()
            .isRegularFile()
            .hasContent(contentData2);
    }

}
