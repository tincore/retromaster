package com.tincore.retromaster.functional.server.thrift;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.thrift.form.TContentMetadataSet;
import com.tincore.retromaster.thrift.form.TContentMetadataSetFilter;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static com.tincore.retromaster.domain.ContentMetadataSource.NOINTRO;
import static com.tincore.retromaster.domain.ContentMetadataSource.TOSEC;
import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(
                scenario = "Retromaster API: Content Metadata Set",
                tags = {"API", "Thrift"})
public class RetromasterServiceHandlerContentMetadataSetApiIT extends AbstractRetromasterServiceThriftApiIT {

    @BeforeEach
    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testDeleteContentMetadatasSetsByIdGivenPersistedContentMetadataSetWhenMatchingIdThenContentMetadataSetsIsDeleted() throws Exception {
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(tFS.createName(), TOSEC.name(), "2019-01-01"));
        clientAdmin().deleteContentMetadataSetById(contentMetadataSet.getId().toString());
        assertThat(tFS.getContentMetadataSetRepositoryService().findById(contentMetadataSet.getId())).isNotPresent();
    }

    @Test
    public void testFindContentMetadatasSetsByFilterGivenPersistedContentMetadataSetsWhenEmptyFilterThenContentMetadataSetsAreReturned() throws Exception {
        IntStream.range(0, 39)
            .forEach(
                i -> {
                    try {
                        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(tFS.createName(), TOSEC.name(), "2019-01-01"));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });

        var filter = new TContentMetadataSetFilter();

        var page = createTPageable(0);

        var form = clientAdmin().findContentMetadataSetsByFilter(filter, page);

        assertThat(form.totalElements).isEqualTo(39);
        assertThat(form.totalPages).isEqualTo(4);
        assertThat(form.size).isEqualTo(page.size);
        assertThat(form.number).isEqualTo(page.page);
        assertThat(form.content).hasSize((int) page.size);
    }

    @Test
    public void testFindContentMetadatasSetsByFilterGivenPersistedContentMetadataSetsWhenFilterByQueryThenContentMetadataSetsWithLikeNameAreReturned() throws Exception {
        var contentMetadataSet0 = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet("xname1", TOSEC.name(), "2019-01-01"));
        var contentMetadataSet2a = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet("xname2a", TOSEC.name(), "2019-01-01"));
        var contentMetadataSet2b = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet("xname2b", TOSEC.name(), "2019-01-01"));

        var filter = new TContentMetadataSetFilter().setQuery("xname2%");

        var page = createTPageable(0);

        var form = clientAdmin().findContentMetadataSetsByFilter(filter, page);

        assertThat(form.totalElements).isEqualTo(2);
        assertThat(form.totalPages).isEqualTo(1);
        assertThat(form.size).isEqualTo(page.size);
        assertThat(form.number).isEqualTo(page.page);
        assertThat(form.content).hasSize(2);
        assertThat(form.content.stream().map(TContentMetadataSet::getId)).contains(contentMetadataSet2a.getId().toString(), contentMetadataSet2b.getId().toString());
    }

    @Test
    public void testFindContentMetadatasSetsByFilterGivenPersistedContentMetadataSetsWhenFilterBySystemThenContentMetadataSetsWithEqualSystemAreReturned() throws Exception {
        var contentMetadataSet0 = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(tFS.createName(), TOSEC.name(), "2019-01-01"));
        var contentMetadataSet2a = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(tFS.createName(), TOSEC.name(), "2019-01-01"));
        var contentMetadataSet2b = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(tFS.createName(), NOINTRO.name(), "2019-01-01"));

        var filter = new TContentMetadataSetFilter().setCategoryEquals(contentMetadataSet2a.getCategory());

        var page = createTPageable(0);

        var form = clientAdmin().findContentMetadataSetsByFilter(filter, page);

        assertThat(form.totalElements).isEqualTo(2);
        assertThat(form.totalPages).isEqualTo(1);
        assertThat(form.size).isEqualTo(page.size);
        assertThat(form.number).isEqualTo(page.page);
        assertThat(form.content).hasSize(2);
    }
}
