package com.tincore.retromaster.functional.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.Application;
import com.tincore.retromaster.support.RetromasterTestContextInitializer;
import com.tincore.retromaster.support.RetromasterTestFixtureService;
import com.tincore.retromaster.support.RetromasterTestFixtureTrait;
import com.tincore.boot.util.security.jwt.JwtTokenService;
import com.tincore.test.support.junit2bdd.junit5.BehaviourTestExtension;
import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@ExtendWith(SpringExtension.class)
@ExtendWith(BehaviourTestExtension.class)
@SpringBootTest(
                webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
                classes = {Application.class, ApplicationFunctionalITConfiguration.class})
@ContextConfiguration(initializers = RetromasterTestContextInitializer.class)
@ActiveProfiles("test")
public abstract class AbstractApiIT implements RetromasterTestFixtureTrait {

    public static final String USER_NAME = "someTestUser";

    @Autowired
    public RetromasterTestFixtureService tFS;

    @Value("${tincore.jwt.claimUsername:" + Claims.SUBJECT + "}")
    protected String jwtClaimUserName = Claims.SUBJECT;

    @Value("${tincore.jwt.claimRole:role}")
    protected String jwtClaimRole;

    @Value("${tincore.jwt.headerName}")
    protected String securityJwtHeaderName;

    @Value("${test.server.protocol:http}")
    protected String serverProtocol;

    @Value("${test.server.host:localhost}")
    protected String serverHost;

    @Value("${tincore.retromaster.homePath}")
    protected Path homePath;

    @Value("${local.server.port}")
    protected int port;

    @Value("${test.root.path}")
    protected Path testRootPath;

    @Value("${test.ftp.port}")
    protected int ftpPort;

    @Autowired
    protected JwtTokenService jwtTokenService;

    public String createJwtToken(String role) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(jwtClaimUserName, USER_NAME);
        claims.put(jwtClaimRole, role);
        return jwtTokenService.createToken(claims);
    }

    @BeforeEach
    public void setUp() throws Exception {
        tFS.clear();
    }

    @AfterEach
    public void tearDown() {
        tFS.clear();
    }
}
