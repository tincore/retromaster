package com.tincore.retromaster.functional.server.web;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebElement;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "Content Metadata Sets", tags = "WEB")
@TestMethodOrder(MethodOrderer.MethodName.class)
public class ContentMetadataSetsWebIT extends AbstractWebIT {

    @Test
    public void testContentMetadataSetsPageGivenContentMetadataSetDeleteDialogDisplayedWhenAcceptClickedThenContentMetadataSetRemoved() {
        var contentMetadataSet = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());

        setUpJwtAut();
        setUpContentMetadataSetsPageDisplayed();
        contentMetadataSetsPage.doDeleteContentMetadataSetByUuid(contentMetadataSet.getId().toString());
        deleteConfirmComponentPage.waitForVisibility();
        deleteConfirmComponentPage.accept.click();
        waitForInvisibility(deleteConfirmComponentPage.component);
        waitForInvisibility(contentMetadataSetsPage.contentMetadataSets);

        assertThat(tFS.getContentMetadataSetRepositoryService().findById(contentMetadataSet.getId())).isNotPresent();
    }

    @Test
    public void testContentMetadataSetsPageGivenContentMetadataSetThenContentMetadataSetDisplayed() {
        var contentMetadataSet = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());

        setUpJwtAut();
        setUpContentMetadataSetsPageDisplayed();

        assertThat(contentMetadataSetsPage.getContentMetadataSetByName(contentMetadataSet.getName())).isPresent();
    }

    @Test
    public void testContentMetadataSetsPageGivenContentMetadataSetWhenDeleteClickedThenDeleteDialogDisplayed() {
        var contentMetadataSet = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());

        setUpJwtAut();
        setUpContentMetadataSetsPageDisplayed();
        contentMetadataSetsPage.doDeleteContentMetadataSetByUuid(contentMetadataSet.getId().toString());

        assertThat(waitForVisibility(deleteConfirmComponentPage.component).isDisplayed()).isTrue();
    }

    @Test
    public void testContentMetadataSetsPageGivenContentMetadataSetWhenRowClickedThenContentMetadataSetPageDisplayed() {
        var contentMetadataSet = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());

        setUpJwtAut();
        setUpContentMetadataSetsPageDisplayed();
        contentMetadataSetsPage.getContentMetadataSetByUuid(contentMetadataSet.getId().toString()).click();

        contentMetadataSetPage.waitForVisibility();
        assertThat(contentMetadataSetPage.component.isDisplayed()).isTrue();
    }

    @Test
    public void testContentMetadataSetsPageGivenContentMetadataSetsThenContentMetadataSetsSortedByName() {
        var contentMetadataSet1 = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());
        var contentMetadataSet2 = tFS.getContentMetadataSetRepositoryService().save(createContentMetadataSet());

        var sortedNames = Stream.of(contentMetadataSet1, contentMetadataSet2).map(ContentMetadataSet::getName).sorted().toArray(String[]::new);

        setUpJwtAut();
        setUpContentMetadataSetsPageDisplayed();

        assertThat(contentMetadataSetsPage.contentMetadataSets.size()).isEqualTo(sortedNames.length);
        assertThat(contentMetadataSetsPage.contentMetadataSetsNames.stream().map(WebElement::getText)).containsExactly(sortedNames);
    }

    @Test
    public void testContentMetadataSetsPageGivenNoContentMetadataSetsThenNoContentMetadataSetsDisplayed() {
        setUpJwtAut();
        setUpContentMetadataSetsPageDisplayed();

        assertThat(contentMetadataSetsPage.component.isDisplayed()).isTrue();
        assertThat(contentMetadataSetsPage.contentMetadataSets.size()).isEqualTo(0);
    }
}
