package com.tincore.retromaster.functional.server.fuse;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.util.FileSystemTrait;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.tincore.retromaster.server.fuse.content.ContentFuseFsEntryFactory.NAME_BY_SYSTEM;
import static org.assertj.core.api.Assertions.assertThat;

// Attention only runs on linux when fuse is available. Create assumptions!
@ActiveProfiles(value = {"test", "testUnionFs"})
@Slf4j
public class RetromasterFuseFsWithUnionControllerReadWriteIT extends AbstractRetromasterFuseFsControllerIT implements FileSystemTrait {

    public Path getContentMetadataPath(ContentMetadataItem contentMetadataItem) {
        return getFuseFsRootPathReadWrite()
            .resolve(NAME_BY_SYSTEM)
            .resolve(contentMetadataItem.getContentMetadata().getSystem())
            .resolve(contentMetadataItem.getContentMetadata().getPrefix())
            .resolve(contentMetadataItem.getContentMetadata().toStringFileUnique());
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenContentMetadataItemFileDeletedAndNewFileCreatedWithSameNameThenNewFileIsCreatedSuccessfully() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        Files.delete(contentMetadataItemPath);

        FileUtils.writeStringToFile(contentMetadataItemPath.toFile(), contentData2, Charset.defaultCharset());

        assertThat(contentMetadataItemPath).exists()
            .isRegularFile()
            .hasContent(contentData2);
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenContentMetadataItemFileDeletedThenFileIsNotPresent() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        Files.delete(contentMetadataItemPath);

        assertThat(contentMetadataItemPath).doesNotExist();
        assertThat(isEmpty(contentMetadataPath)).isTrue();
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenContentMetadataItemFileModifiedThenFileContainsModifications() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        FileUtils.writeStringToFile(contentMetadataItemPath.toFile(), contentData2, Charset.defaultCharset());

        assertThat(contentMetadataItemPath).hasContent(contentData2);
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenNewDirectoryCreatedInSameDirectoryAsContentFileThenDirectoryIsCreatedSuccessfully() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var targetPath = contentMetadataPath.resolve("testDir");

        assertThat(Files.createDirectories(targetPath)).exists().isDirectory();
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedWhenNewFileCreatedInSameDirectoryAsContentMetadataItemFileThenNewFileIsCreatedSuccessfully() throws Exception {
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingCompressedContent("recognized.ext", contentData1);

        var contentMetadataPath = getContentMetadataPath(contentMetadataItem);

        var targetPath = contentMetadataPath.resolve("testFile.txt");
        FileUtils.writeStringToFile(targetPath.toFile(), contentData2, Charset.defaultCharset());

        assertThat(targetPath).exists().isRegularFile();

        var fileContent = FileUtils.readFileToString(targetPath.toFile(), Charset.defaultCharset());
        assertThat(fileContent).isEqualTo(contentData2);
    }
}
