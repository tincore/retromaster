package com.tincore.retromaster.functional.server.web.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.selenium.AbstractPage;
import com.tincore.test.support.selenium.SeleniumPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@SeleniumPage
public class ContentMetadataPage extends AbstractPage {

    @FindBy(css = "[data-sem=contentMetadata_component]")
    public WebElement component;

    @FindBy(css = "[data-sem=progressBar]")
    public WebElement progressBar;

    @FindBy(css = "[data-sem=back_do]")
    public WebElement back;

    @FindBy(css = "[data-sem=menu_do]")
    public WebElement menu;

    @FindBy(css = "[data-sem=details_do]")
    public WebElement details;

    @FindBy(css = "[data-sem=menuDisplay_do]")
    public WebElement menuDisplay;

    @FindBy(css = "[data-sem=contentMetadataDetails_container]")
    public WebElement contentMetadataDetailsContainer;

    @FindBy(css = "data-sem=title")
    public WebElement title;

    @FindBy(css = "data-sem=publisher")
    public WebElement publisher;

    @FindBy(css = "data-sem=system")
    public WebElement system;

    @FindBy(css = "data-sem=date")
    public WebElement date;

    @FindBy(css = "data-sem=region")
    public WebElement region;

    @FindBy(css = "data-sem=language")
    public WebElement language;

    @FindBy(css = "data-sem=mediaType")
    public WebElement mediaType;

    @FindBy(css = "data-sem=mediaOrder")
    public WebElement mediaOrder;

    @FindBy(css = "data-sem=source")
    public WebElement source;

    @FindBy(css = "data-sem=alternate")
    public WebElement alternate;

    @FindBy(css = "data-sem=alternateData")
    public WebElement alternateData;

    @FindBy(css = "data-sem=badDump")
    public WebElement badDump;

    @FindBy(css = "data-sem=badDumpData")
    public WebElement badDumpData;

    @FindBy(css = "data-sem=cracked")
    public WebElement cracked;

    @FindBy(css = "data-sem=crackedData")
    public WebElement crackedData;

    @FindBy(css = "data-sem=fixed")
    public WebElement fixed;

    @FindBy(css = "data-sem=fixedData")
    public WebElement fixedData;

    @FindBy(css = "data-sem=modified")
    public WebElement modified;

    @FindBy(css = "data-sem=modifiedData")
    public WebElement modifiedData;

    @FindBy(css = "data-sem=hacked")
    public WebElement hacked;

    @FindBy(css = "data-sem=hackedData")
    public WebElement hackedData;

    @FindBy(css = "data-sem=translated")
    public WebElement translated;

    @FindBy(css = "data-sem=translatedData")
    public WebElement translatedData;

    @FindBy(css = "data-sem=trained")
    public WebElement trained;

    @FindBy(css = "data-sem=trainedData")
    public WebElement trainedData;

    @FindBy(css = "data-sem=pirated")
    public WebElement pirated;

    @FindBy(css = "data-sem=piratedData")
    public WebElement piratedData;

    @FindBy(css = "data-sem=virus")
    public WebElement virus;

    @FindBy(css = "data-sem=virusData")
    public WebElement virusData;

    @FindBy(css = "data-sem=verified")
    public WebElement verified;

    @FindBy(css = "data-sem=tagsExtra")
    public WebElement tagsExtra;

    @FindBy(css = "[data-sem=contentMetadataItem_container]")
    public List<WebElement> contentMetadataItems;

    @FindBy(css = "[data-sem=contentMetadataItem_container] [data-sem=name]")
    public List<WebElement> contentMetadatasItemNames;

    @FindBy(css = "[data-sem=contentMetadataItem_container] [data-sem=crc]")
    public List<WebElement> contentMetadatasItemCrcs;

    @FindBy(css = "[data-sem=contentMetadataItem_container] [data-sem=md5]")
    public List<WebElement> contentMetadatasItemMd5;

    @FindBy(css = "[data-sem=contentMetadataItem_container] [data-sem=sha1]")
    public List<WebElement> contentMetadatasItemSha1;

    @FindBy(css = "[data-sem=contentMetadataItem_container] [data-sem=size]")
    public List<WebElement> contentMetadatasItemSizes;

    @FindBy(css = "[data-sem=contentMetadataItem_container] [data-sem=play_do]")
    public List<WebElement> contentMetadatasItemPlayDos;

    public ContentMetadataPage(@Autowired WebDriver driver) {
        super(driver);
    }

    public void waitForVisibility() {
        waitForVisibility(component);
        waitForInvisibility(progressBar);
    }
}
