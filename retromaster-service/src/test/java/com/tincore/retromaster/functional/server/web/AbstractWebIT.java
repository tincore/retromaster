package com.tincore.retromaster.functional.server.web;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.Application;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.functional.server.web.support.*;
import com.tincore.retromaster.functional.support.ApplicationFunctionalITConfiguration;
import com.tincore.retromaster.support.RetromasterTestContextInitializer;
import com.tincore.retromaster.support.RetromasterTestFixtureService;
import com.tincore.retromaster.support.RetromasterTestFixtureTrait;
import com.tincore.test.support.junit2bdd.junit5.BehaviourTestExtension;
import com.tincore.test.support.selenium.AbstractSeleniumFunctionalWebIT;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@ExtendWith(BehaviourTestExtension.class)
@EnableConfigurationProperties
@SpringBootTest(
                webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
                classes = {Application.class, ApplicationFunctionalITConfiguration.class, WebITConfiguration.class})
@ContextConfiguration(initializers = RetromasterTestContextInitializer.class)
public abstract class AbstractWebIT extends AbstractSeleniumFunctionalWebIT implements RetromasterTestFixtureTrait {

    @Autowired
    protected RetromasterTestFixtureService tFS;

    @Autowired
    protected DevPage devPage;

    @Autowired
    protected ErrorPage errorPage;

    @Autowired
    protected HomePage homePage;

    @Autowired
    protected ContentMetadataSetsPage contentMetadataSetsPage;

    @Autowired
    protected ContentMetadataSetPage contentMetadataSetPage;

    @Autowired
    protected ContentMetadataPage contentMetadataPage;

    @Autowired
    protected DeleteConfirmComponentPage deleteConfirmComponentPage;

    @Value("${local.server.port}")
    protected int serverPort;

    protected long userId = System.currentTimeMillis() / 1000;

    @BeforeEach
    public void setUp() {
        super.setUp();
        deleteAllCookies();
        tFS.clear();
    }

    public void setUpContentMetadataPageDisplayed(ContentMetadata contentMetadata) {
        goTo("/retromaster/contentMetadata/" + contentMetadata.getId());
    }

    public void setUpContentMetadataSetPageDisplayed(ContentMetadataSet contentMetadataSet) {
        goTo("/retromaster/contentMetadataSet/" + contentMetadataSet.getId());
    }

    public void setUpContentMetadataSetsPageDisplayed() {
        goTo("/retromaster/contentMetadataSet", contentMetadataSetsPage);
    }

    public void setUpDashboardPageDisplayed() {
        goTo("/", homePage);
    }

    public void setUpJwtAut() {
        goTo("dev", devPage);
        devPage.doJwtAut(userId, "ADMIN");

        sleep();
    }

    public void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // Should eventually Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void sleep() {
        sleep(300);
    }

    @AfterEach
    public void tearDown() {
        tFS.clear();
    }
}
