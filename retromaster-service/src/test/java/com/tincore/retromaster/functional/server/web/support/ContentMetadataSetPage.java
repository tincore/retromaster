package com.tincore.retromaster.functional.server.web.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.test.support.selenium.AbstractPage;
import com.tincore.test.support.selenium.SeleniumPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@SeleniumPage
public class ContentMetadataSetPage extends AbstractPage {

    @FindBy(css = "[data-sem=contentMetadataSet_component]")
    public WebElement component;

    @FindBy(css = "[data-sem=contentMetadataSetDetails_container]")
    public WebElement contentMetadataSetDetailsContainer;

    @FindBy(css = "[data-sem=progressBar]")
    public WebElement progressBar;

    @FindBy(css = "[data-sem=back_do]")
    public WebElement back;

    @FindBy(css = "[data-sem=menu_do]")
    public WebElement menu;

    @FindBy(css = "[data-sem=titleSort_do]")
    public WebElement titleSort;

    @FindBy(css = "[data-sem=mediaSort_do]")
    public WebElement mediaSort;

    @FindBy(css = "[data-sem=regionSort_do]")
    public WebElement regionSort;

    @FindBy(css = "[data-sem=publisherSort_do]")
    public WebElement publisherSort;

    @FindBy(css = "[data-sem=dateSort_do]")
    public WebElement dateSort;

    @FindBy(css = "[data-sem=menuDisplay_do]")
    public WebElement menuDisplay;

    @FindBy(css = "[data-sem=query]")
    public WebElement query;

    @FindBy(css = "[data-sem=contentMetadata_container] [data-sem=title]")
    public List<WebElement> contentMetadatasTitles;

    @FindBy(css = "[data-sem=contentMetadata_container]")
    public List<WebElement> contentMetadatas;

    @Autowired
    public PaginatorComponentPage paginatorComponentPage;

    public ContentMetadataSetPage(@Autowired WebDriver driver) {
        super(driver);
    }

    public Optional<WebElement> getContentMetadataByTitle(String title) {
        return contentMetadatasTitles.stream().filter(n -> title.equals(n.getText())).findFirst();
    }

    public WebElement getContentMetadataByUuid(String uuid) {
        return contentMetadatas.stream().filter(w -> uuid.equals(waitForSemanticQualifier(w))).findFirst().orElseThrow(() -> new RuntimeException("Not found by UUID " + uuid));
    }

    public String getContentMetadataDateByUuid(String uuid) {
        return getContentMetadataByUuid(uuid).findElement(By.cssSelector("[data-sem=date]")).getText();
    }

    public String getContentMetadataMediaByUuid(String uuid) {
        return getContentMetadataByUuid(uuid).findElement(By.cssSelector("[data-sem=media]")).getText();
    }

    public String getContentMetadataPublisherByUuid(String uuid) {
        return getContentMetadataByUuid(uuid).findElement(By.cssSelector("[data-sem=publisher]")).getText();
    }

    public String getContentMetadataRegionByUuid(String uuid) {
        return getContentMetadataByUuid(uuid).findElement(By.cssSelector("[data-sem=region]")).getText();
    }

    public String getContentMetadataTitleByUuid(String uuid) {
        return getContentMetadataByUuid(uuid).findElement(By.cssSelector("[data-sem=title]")).getText();
    }

    public void waitForVisibility() {
        waitForVisibility(component);
        waitForInvisibility(progressBar);
    }
}
