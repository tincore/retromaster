package com.tincore.retromaster.functional.server.fuse;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.nio.charset.Charset;

import static com.tincore.retromaster.server.fuse.content.ContentFuseFsEntryFactory.NAME_BY_SYSTEM;
import static org.assertj.core.api.Assertions.assertThat;

// Attention only runs on linux when fuse is available. Create assumptions!
@Slf4j
public class RetromasterFuseFsControllerBySystemIT extends AbstractRetromasterFuseFsControllerIT {

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentAndContentIsCompressedThenFileIsPresentAndFileHasSameNameAsContentMetadataItemAndFileInDirectoryBySystem_ContentMetadataPrefix_ContentMetadataName() throws Exception {
        var content = tFS.setUpPersistedContent(productionVolume, "somePath/" + tFS.createFilename("zip"), "testEntry.txt", true, contentData1.getBytes());

        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingContent(content, "recognized.ext");
        var systemPath = getFuseFsRootPath().resolve(NAME_BY_SYSTEM).resolve(contentMetadataItem.getContentMetadata().getSystem());
        assertThat(systemPath).exists().isDirectory();

        var contentMetadataPrefixPath = systemPath.resolve(contentMetadataItem.getContentMetadata().getPrefix());
        assertThat(contentMetadataPrefixPath).exists().isDirectory();

        var contentMetadataPath = contentMetadataPrefixPath.resolve(contentMetadataItem.getContentMetadata().toStringFileUnique());
        assertThat(contentMetadataPath).exists().isDirectory();

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        var fileContent = FileUtils.readFileToString(contentMetadataItemPath.toFile(), Charset.defaultCharset());
        assertThat(fileContent).isEqualTo(contentData1);
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentThenFileIsPresentAndFileHasSameNameAsContentMetadataItemAndFileInDirectoryBySystem_ContentMetadataPrefix_ContentMetadataName() throws Exception {
        var content = tFS.setUpPersistedContent(productionVolume, "somePath/" + tFS.createFilename("txt"), true, contentData1.getBytes());
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingContent(content, "recognized.ext");

        var systemPath = getFuseFsRootPath().resolve(NAME_BY_SYSTEM).resolve(contentMetadataItem.getContentMetadata().getSystem());
        assertThat(systemPath).exists().isDirectory();

        var contentMetadataPrefixPath = systemPath.resolve(contentMetadataItem.getContentMetadata().getPrefix());
        assertThat(contentMetadataPrefixPath).exists().isDirectory();

        var contentMetadataPath = contentMetadataPrefixPath.resolve(contentMetadataItem.getContentMetadata().toStringFileUnique());
        assertThat(contentMetadataPath).exists().isDirectory();

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        var fileContent = FileUtils.readFileToString(contentMetadataItemPath.toFile(), Charset.defaultCharset());
        assertThat(fileContent).isEqualTo(contentData1);
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithoutContentThenContentMetadataDirectoryInDirectoryBySystem_ContentMetadataPrefix_ContentMetadataNameAndDirectoryEmpty() {
        var contentMetadataItem = createContentMetadataItem("recognized.ext", contentData2.getBytes());
        var contentMetadata = createContentMetadata(null, "cm_1", contentMetadataItem);
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(createContentMetadataSetTosec("TEST_SYSTEM", createName(), contentMetadata));

        var systemPath = getFuseFsRootPath().resolve(NAME_BY_SYSTEM).resolve(contentMetadata.getSystem());
        assertThat(systemPath).exists().isDirectory();

        var contentMetadataPrefixPath = systemPath.resolve(contentMetadata.getPrefix());
        assertThat(contentMetadataPrefixPath).exists().isDirectory();

        var contentMetadataPath = contentMetadataPrefixPath.resolve(contentMetadata.toStringFileUnique());
        assertThat(contentMetadataPath).exists().isDirectory();
        assertThat(contentMetadataPath.toFile().listFiles()).isEmpty();
    }

    @Test
    public void testFilesystemGivenContentMetadataThenContentMetadataDirectoryInDirectoryBySystem_ContentMetadataPrefix_ContentMetadataNameAndDirectoryEmpty() {
        var contentMetadata = createContentMetadata(null, "cm_1");
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(createContentMetadataSetTosec("TEST_SYSTEM", createName(), contentMetadata));

        var systemPath = getFuseFsRootPath().resolve(NAME_BY_SYSTEM).resolve(contentMetadata.getSystem());
        assertThat(systemPath).exists().isDirectory();

        var contentMetadataPrefixPath = systemPath.resolve(contentMetadata.getPrefix());
        assertThat(contentMetadataPrefixPath).exists().isDirectory();

        var contentMetadataPath = contentMetadataPrefixPath.resolve(contentMetadata.toStringFileUnique());
        assertThat(contentMetadataPath).exists().isDirectory();
        assertThat(contentMetadataPath.toFile().listFiles()).isEmpty();
    }

    @Test
    public void testFilesystemGivenCreatedThenRootDirectoryContainsBySystemDirectory() {
        assertThat(getFuseFsRootPath().resolve(NAME_BY_SYSTEM)).exists().isDirectory();
    }
}
