package com.tincore.retromaster.functional.server.fuse;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.nio.charset.Charset;

import static com.tincore.retromaster.server.fuse.content.ContentFuseFsEntryFactory.NAME_BY_SET_PREFIXES;
import static org.assertj.core.api.Assertions.assertThat;

// Attention only runs on linux when fuse is available. Create assumptions!
@Slf4j
public class RetromasterFuseFsControllerBySetPrefixedIT extends AbstractRetromasterFuseFsControllerIT {

    private void setUpPrefixIncludeIdsExpectations() {
        Mockito.doReturn(true).when(fuseConfiguration).isContentMetadataPrefixDirectoryContentIncludeIds();
        Mockito.doReturn(true).when(fuseConfiguration).isContentMetadataSetsPrefixDirectoryContentIncludeIds();
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithContentThenFileIsPresentAndFileHasSameNameAsContentMetadataItemAndFileInDirectoryBySetPrefixes_ContentMetadataSetPrefix_ContentMetadataSet_ContentMetadataPrefix_ContentMetadata()
        throws Exception {

        setUpPrefixIncludeIdsExpectations();

        var content = tFS.setUpPersistedContent(productionVolume, "somePath/" + tFS.createFilename("txt"), true, contentData1.getBytes());
        var contentMetadataItem = setUpPersistedContentMetadataItemWithMatchingContent(content, "recognized.ext");

        var contentMetadataSetPrefixPath = getFuseFsRootPath().resolve(NAME_BY_SET_PREFIXES).resolve(contentMetadataItem.getContentMetadata().getContentMetadataSet().getPrefix());
        assertThat(contentMetadataSetPrefixPath).exists().isDirectory();

        var contentMetadataSetPath = contentMetadataSetPrefixPath.resolve(contentMetadataItem.getContentMetadata().getContentMetadataSet().toStringFileUnique());
        assertThat(contentMetadataSetPath).exists().isDirectory();

        var contentMetadataPrefixPath = contentMetadataSetPath.resolve(contentMetadataItem.getContentMetadata().getPrefix());
        assertThat(contentMetadataPrefixPath).exists().isDirectory();

        var contentMetadataPath = contentMetadataPrefixPath.resolve(contentMetadataItem.getContentMetadata().toStringFileUnique());
        assertThat(contentMetadataPath).exists().isDirectory();

        var contentMetadataItemPath = contentMetadataPath.resolve(contentMetadataItem.getName());
        assertThat(contentMetadataItemPath).exists().isRegularFile();

        var fileContent = FileUtils.readFileToString(contentMetadataItemPath.toFile(), Charset.defaultCharset());
        assertThat(fileContent).isEqualTo(contentData1);
    }

    @Test
    public void testFilesystemGivenContentMetadataItemWithoutContentThenContentMetadataDirectoryInDirectoryBySetPrefixes_ContentMetadataSetPrefix_ContentMetadataSet_ContentMetadataPrefix_ContentMetadataAndDirectoryEmty() {
        setUpPrefixIncludeIdsExpectations();

        var contentMetadataItem = createContentMetadataItem("recognized.ext", contentData2.getBytes());
        var contentMetadata = createContentMetadata(null, "cm_1", contentMetadataItem);
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(createContentMetadataSetTosec("TEST_SYSTEM", createName(), contentMetadata));

        var contentMetadataSetPrefixPath = getFuseFsRootPath().resolve(NAME_BY_SET_PREFIXES).resolve(contentMetadataItem.getContentMetadata().getContentMetadataSet().getPrefix());
        assertThat(contentMetadataSetPrefixPath).exists().isDirectory();

        var contentMetadataSetPath = contentMetadataSetPrefixPath.resolve(contentMetadataSet.toStringFileUnique());
        assertThat(contentMetadataSetPath).exists().isDirectory();

        var contentMetadataPrefixPath = contentMetadataSetPath.resolve(contentMetadataItem.getContentMetadata().getPrefix());
        assertThat(contentMetadataPrefixPath).exists().isDirectory();

        var contentMetadataPath = contentMetadataPrefixPath.resolve(contentMetadataItem.getContentMetadata().toStringFileUnique());
        assertThat(contentMetadataPath).exists().isDirectory();
        assertThat(contentMetadataPath.toFile().listFiles()).isEmpty();
    }

    @Test
    public void testFilesystemGivenContentMetadataThenContentMetadataDirectoryInDirectoryBySetPrefixes_ContentMetadataSetPrefix_ContentMetadataSet_ContentMetadataPrefix_ContentMetadataAndDirectoryEmty() {
        setUpPrefixIncludeIdsExpectations();

        var contentMetadata = createContentMetadata(null, "cm_1");
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(createContentMetadataSetTosec("TEST_SYSTEM", createName(), contentMetadata));

        var contentMetadataSetPrefixPath = getFuseFsRootPath().resolve(NAME_BY_SET_PREFIXES).resolve(contentMetadataSet.getPrefix());
        assertThat(contentMetadataSetPrefixPath).exists().isDirectory();

        var contentMetadataSetPath = contentMetadataSetPrefixPath.resolve(contentMetadataSet.toStringFileUnique());
        assertThat(contentMetadataSetPath).exists().isDirectory();

        var contentMetadataPrefixPath = contentMetadataSetPath.resolve(contentMetadata.getPrefix());
        assertThat(contentMetadataPrefixPath).exists().isDirectory();

        var contentMetadataPath = contentMetadataPrefixPath.resolve(contentMetadata.toStringFileUnique());
        assertThat(contentMetadataPath).exists().isDirectory();
        assertThat(contentMetadataPath.toFile().listFiles()).isEmpty();
    }

    @Test
    public void testFilesystemGivenCreatedThenRootDirectoryContainsBySetPrefixesDirectory() {
        assertThat(getFuseFsRootPath().resolve(NAME_BY_SET_PREFIXES)).exists().isDirectory();
    }
}
