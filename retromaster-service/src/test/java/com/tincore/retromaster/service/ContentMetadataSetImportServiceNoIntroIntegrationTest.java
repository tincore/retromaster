package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static com.tincore.retromaster.domain.ContentMetadataSource.NOINTRO;
import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "ContentMetadataSet")
public class ContentMetadataSetImportServiceNoIntroIntegrationTest extends AbstractContextIntegrationTest {

    @Autowired
    public ContentMetadataSetImportService contentMetadataSetImportService;

    @Autowired
    public ContentMetadataSetService contentMetadataSetService;

    @Autowired
    public ContentMetadataVolumeService contentMetadataVolumeService;

    @Autowired
    public ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;

    @Test
    public void testImportByPathGivenExistingResourceAndNameWithParenthesesAndValidThenSavesMetadata() throws IOException {
        copyToPaths("classpath:/test/dat/nointro/test_ni_3.dat", contentMetadataVolumeService.getImportPath());
        contentMetadataSetImportService.doContentMetadataSetImportByDirectory(contentMetadataVolumeService.getImportPath());

        var contentMetadataSets = contentMetadataSetRepositoryService.findAll();

        assertThat(contentMetadataSets)
            .anySatisfy(
                s -> {
                    assertThat(s.getFilename()).isEqualTo("test_ni_3.dat");
                    assertThat(s.getName()).isEqualTo("Nintendo - Wii (Digital) (Split DLC) (WAD)");
                    assertThat(s.getVersion()).isEqualTo("20190825-031431");
                    assertThat(s.getAuthor()).isEqualTo("xuom2");
                    assertThat(s.getCategory()).isEqualTo(NOINTRO.name());
                    assertThat(s.getUrl()).isEqualTo("http://www.no-intro.org");
                    assertThat(s.getRepositoryPath()).isEqualTo("NINTENDO_WII - Digital Split DLC WAD 20190825-031431 NOINTRO");
                });

        // <name>Nintendo - Wii (Digital) (Split DLC) (WAD)</name>
        // <description>Nintendo - Wii (Digital) (Split DLC) (WAD)</description>
        // <version>20190825-031431</version>
        // <author>xuom2</author>
        // <homepage>No-Intro</homepage>
        // <url>http://www.no-intro.org</url>

        assertThat(isEmpty(contentMetadataVolumeService.getImportPath())).isTrue();
        assertThat(isEmpty(contentMetadataVolumeService.getProdPath())).isFalse();
    }

    @Test
    public void testImportByPathGivenExistingResourceAndValidThenSavesMetadata() throws IOException {
        copyToPaths("classpath:/test/dat/nointro/test_ni_1.dat", contentMetadataVolumeService.getImportPath());
        contentMetadataSetImportService.doContentMetadataSetImportByDirectory(contentMetadataVolumeService.getImportPath());

        var contentMetadataSets = contentMetadataSetRepositoryService.findAll();

        assertThat(contentMetadataSets)
            .anySatisfy(
                s -> {
                    assertThat(s.getFilename()).isEqualTo("test_ni_1.dat");
                    assertThat(s.getName()).isEqualTo("Atari - 5200");
                    assertThat(s.getVersion()).isEqualTo("20130303-220600");
                    assertThat(s.getAuthor()).isEqualTo("xuom2");
                    assertThat(s.getCategory()).isEqualTo(NOINTRO.name());
                    assertThat(s.getUrl()).isEqualTo("http://www.no-intro.org");
                    assertThat(s.getRepositoryPath()).isEqualTo("ATARI_5200 - 20130303-220600 NOINTRO");
                });

        assertThat(isEmpty(contentMetadataVolumeService.getImportPath())).isTrue();
        assertThat(isEmpty(contentMetadataVolumeService.getProdPath())).isFalse();
    }
}
