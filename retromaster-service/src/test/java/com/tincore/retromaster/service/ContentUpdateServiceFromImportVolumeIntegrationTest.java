package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.service.archive.producer.BytesArchiveEntryProducer;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ContentUpdateServiceFromImportVolumeIntegrationTest extends AbstractContextIntegrationTest {

    private final byte[] contentMetadataItemAa1Payload = RandomStringUtils.randomAlphabetic(20).getBytes();
    private final byte[] contentMetadataItemAa2Payload = RandomStringUtils.randomAlphabetic(20).getBytes();
    private final byte[] unknownPayload1 = RandomStringUtils.randomAlphabetic(20).getBytes();
    private final byte[] unknownPayload2 = RandomStringUtils.randomAlphabetic(20).getBytes();

    @Autowired
    private ContentUpdateService contentUpdateService;

    private ContentVolume importVolume;
    private ContentVolume productionVolume;

    private ContentMetadataItem contentMetadataItemAa1;
    private ContentMetadataItem contentMetadataItemAa2;
    private ContentMetadata contentMetadataAa;

    @BeforeEach
    public void setUp() throws Exception {
        importVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.IMPORT, homePath.resolve("data"), true);
        productionVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data"), true);

        contentMetadataItemAa1 = createContentMetadataItem("good1.txt", contentMetadataItemAa1Payload);
        contentMetadataItemAa2 = createContentMetadataItem("good2.txt", contentMetadataItemAa2Payload);
        contentMetadataAa = createContentMetadata(null, "cmA_tit_" + RandomStringUtils.randomAlphanumeric(4), contentMetadataItemAa1, contentMetadataItemAa2);
        tFS.setUpPersistedContentMetadataSet(createContentMetadataSet("cmsA_" + RandomStringUtils.randomNumeric(4), "TOSEC", "2019-01-01", contentMetadataAa));
    }

    @Test
    public void testDoContentImportGivenImportedContentThenAddsSignatureToContentSignatureChange() throws Exception {
        tFS.createFile(importVolume.toPath(), createFileNameTxt(), contentMetadataItemAa1Payload);
        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThatContentSignatureChangeArchivedExists(productionVolume, contentMetadataItemAa1);
    }

    @Test
    public void
        testDoContentImportGivenPathsIncomingContainsArchiveAndArchiveContainsFileRecognizedAndArchiveContainsFileUnrecognizedAndTargetArchiveFileContainsFileRecognizedThenFileCompressedContainsRecognizedContentAndStoredInRepositoryAndUnrecognizedFilesStoredInUnknownAndDuplicateFilesRemoved()
            throws Exception {
        var importFilePath = tFS.setUpFileArchived(
            importVolume.toPath().resolve(createFileNameZip()),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, "blah/recognized_notrenamed.txt"),
            new BytesArchiveEntryProducer(unknownPayload2, "notrecognizedever.txt"));

        var productionFilePath = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, "recognized_in_repo.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "notrecognized_but_in_repo.txt"));
        tFS.setUpPersistedContent(productionVolume, productionFilePath, "recognized_in_repo.txt", false, contentMetadataItemAa2Payload);
        tFS.setUpPersistedContent(productionVolume, productionFilePath, "notrecognized_but_in_repo.txt", false, unknownPayload1);

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFilePath).doesNotExist();

        assertThat(productionFilePath).exists();
        assertThatArchiveFileContentContains(productionFilePath, contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(productionFilePath, contentMetadataItemAa2.getName(), contentMetadataItemAa2Payload);

        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2);

        assertThatArchiveFileContentEquals(tFS.toUnknownContentPath(productionVolume, productionFilePath), "notrecognized_but_in_repo.txt", unknownPayload1);
    }

    @Test
    public void
        testDoContentImportGivenPathsIncomingContainsArchiveAndArchiveContainsFileRecognizedAndArchiveContainsFileUnrecognizedAndTargetArchiveFileContainsUnrecognizedFilesThenFileCompressedContainsRecognizedContentAndStoredInRepositoryAndUnrecognizedFilesStoredInUnknownAndDuplicateFilesRemoved()
            throws Exception {
        var importFilePath = tFS.setUpFileArchived(
            importVolume.toPath().resolve(createFileNameZip()),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, "blah/recognized_notrenamed.txt"),
            new BytesArchiveEntryProducer(unknownPayload2, "notrecognizedever.txt"));

        var productionFilePath = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa),
            new BytesArchiveEntryProducer(unknownPayload1, "notrecognized_but_in_repo.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "notrecognized_but_in_repo_copy.txt"));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFilePath).doesNotExist();

        assertThat(productionFilePath).exists();
        assertThatArchiveFileContentEquals(productionFilePath, contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);

        var unknownContentExpectedFilePath = tFS.toUnknownContentPath(productionVolume, productionFilePath);
        assertThatArchiveFileContentEquals(unknownContentExpectedFilePath, "notrecognized_but_in_repo.txt", unknownPayload1);
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsArchiveAndArchiveContainsFileRecognizedAndTargetArchiveFileContainsUnrecognizedFileThenFileCompressedContainsRecognizedContentAndSavedAndUnrecognizedStoredInUnknown()
        throws Exception {
        var importFilePath = tFS.setUpFileArchived(importVolume.toPath().resolve(createFileNameZip()), new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, "blah/recognized_notrenamed.txt"));

        var productionFilePath = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa),
            new BytesArchiveEntryProducer(unknownPayload1, "notrecognized_but_in_repo.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "notrecognized_but_in_repo_copy.txt"));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFilePath).doesNotExist();

        assertThatArchiveFileContentEquals(productionFilePath, contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);

        assertThatArchiveFileContentEquals(tFS.toUnknownContentPath(productionVolume, productionFilePath), "notrecognized_but_in_repo.txt", unknownPayload1);
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsArchiveFileRecognizedThenFileCompressedAndStoredInRepository() throws Exception {
        var importFilePath = tFS.setUpFileArchived(importVolume.toPath().resolve(createFileNameZip()), new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, "entry.txt"));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFilePath).doesNotExist();

        assertThatArchiveFileContentEquals(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa), contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);

        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsFileNotRecognizedThenMovesToUnknownAndNoContentIsCreated() throws Exception {
        var importFile = tFS.createFile(importVolume.toPath(), createFileNameTxt(), unknownPayload2);

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFile).doesNotExist();
        assertThat(relocateTo(tFS.getUnknownContentPath(), importVolume.toPath(), importFile)).exists().hasBinaryContent(unknownPayload2);
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsFileRecognizedAndTargetArchiveFileContainsUnrecognizedFilesThenFileCompressedAndStoredInRepositoryAndUnrecognizedFilesStoredInUnknownAndDuplicateFilesRemoved() throws Exception {
        var importFile = tFS.createFile(importVolume.toPath(), createFileNameTxt(), contentMetadataItemAa1Payload);

        var productionFile = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa),
            new BytesArchiveEntryProducer(unknownPayload1, "notrecognized_but_in_repo.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk1.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk2.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk3.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk4.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk5.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk6.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk7.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk8.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunk9.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunkA.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunkB.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunkC.txt"),
            new BytesArchiveEntryProducer(unknownPayload1, "someJunkD.txt"));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFile).doesNotExist();

        assertThatArchiveFileContentEquals(productionFile, contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);

        assertThatArchiveFileContentEquals(tFS.toUnknownContentPath(productionVolume, productionFile), "notrecognized_but_in_repo.txt", unknownPayload1);
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsFileRecognizedAndTargetFileContainsUnrecognizedFileThenFileCompressedAndStoredInRepositoryAndUnrecognizedFileStoredInUnknown() throws Exception {
        var importFile = tFS.createFile(importVolume.toPath(), createFileNameTxt(), contentMetadataItemAa1Payload);

        var productionFile = tFS.setUpFileArchived(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa), new BytesArchiveEntryProducer(unknownPayload1, "someJunk.txt"));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFile).doesNotExist();

        assertThatArchiveFileContentEquals(productionFile, contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);

        assertThatArchiveFileContentEquals(tFS.toUnknownContentPath(productionVolume, productionFile), "someJunk.txt", unknownPayload1);
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsFileRecognizedThenFileCompressedAndStoredInRepository() throws Exception {
        var importFile = tFS.createFile(importVolume.toPath(), createFileNameTxt(), contentMetadataItemAa1Payload);

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFile).doesNotExist();

        assertThatArchiveFileContentEquals(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa), contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);

        assertThatContentSignatureChangeArchivedExists(productionVolume, contentMetadataItemAa1);
    }
}
