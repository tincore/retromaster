package com.tincore.retromaster.service.execute.executor;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.test.support.junit2bdd.junit5.BehaviourTestExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.tincore.retromaster.domain.ContentExtension.bin;
import static com.tincore.retromaster.domain.ContentExtension.iso;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(BehaviourTestExtension.class)
class RetroarchContentExecutorTest {

    @Test
    public void testConfigGivenBuilderWithContentExtensionsThenConfigurationHasContentExtensions() {
        assertThat(RetroarchContentExecutor.Configuration.builder().contentExtensions(bin, iso).build().getContentExtensions()).containsExactlyInAnyOrder(bin, iso);
    }

    @Test
    public void testConfigGivenBuilderWithContentSystemsThenConfigurationHasContentSystems() {
        assertThat(RetroarchContentExecutor.Configuration.builder().contentSystems(ContentSystem.NINTENDO_SNES, ContentSystem.COMMODORE_AMIGA).build().getContentSystems())
            .containsExactlyInAnyOrder(ContentSystem.NINTENDO_SNES, ContentSystem.COMMODORE_AMIGA);
    }
}
