package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ContentRepositoryServiceIntegrationTest extends AbstractContextIntegrationTest {

    private ContentVolume contentVolume;

    @Autowired
    private ContentRepositoryService contentRepositoryService;

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        contentVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data"), true);
    }

    public Content setUpPersistedContent() {
        return contentRepositoryService.save(createContent(contentVolume, "somePath/" + createFilename("txt"), NOW, CONTENT_PAYLOAD_1));
    }

    @Test
    public void testDeleteAllByVolumeAndFileAbsolutePathGivenContentThenAddsContentSignatureChange() {
        var content = setUpPersistedContent();
        tFS.clearContentSignatures();

        tFS.getTransactionRequiresNewTemplate()
            .execute(
                t -> {
                    contentRepositoryService.deleteAllByVolumeAndFileAbsolutePath(contentVolume, content.getPathFileAbsolute());
                    return true;
                });

        assertThat(tFS.getContentSignatureChangeRepositoryService().findById(content.getSignature())).isPresent();
    }

    @Test
    public void testDeleteAllByVolumeGivenContentThenAddsContentSignatureChange() {
        var content = setUpPersistedContent();
        tFS.clearContentSignatures();

        tFS.getTransactionRequiresNewTemplate()
            .execute(
                t -> {
                    contentRepositoryService.deleteAllByVolume(contentVolume);
                    return true;
                });

        assertThat(tFS.getContentSignatureChangeRepositoryService().findById(content.getSignature())).isPresent();
    }

    @Test
    public void testDeleteAllGivenContentThenAddsContentSignatureChange() {
        var content = setUpPersistedContent();
        tFS.clearContentSignatures();

        contentRepositoryService.deleteAll(Stream.of(content).collect(Collectors.toList()));

        assertThat(tFS.getContentSignatureChangeRepositoryService().findById(content.getSignature())).isPresent();
    }

    @Test
    public void testDeleteByIdGivenContentThenAddsContentSignatureChange() {
        var content = setUpPersistedContent();
        tFS.clearContentSignatures();
        contentRepositoryService.deleteById(content.getId());

        assertThat(tFS.getContentSignatureChangeRepositoryService().findById(content.getSignature())).isPresent();
    }

    @Test
    public void testDeleteGivenContentThenAddsContentSignatureChange() {
        var content = setUpPersistedContent();
        tFS.clearContentSignatures();

        contentRepositoryService.delete(content);

        assertThat(tFS.getContentSignatureChangeRepositoryService().findById(content.getSignature())).isPresent();
    }

    @Test
    public void testSaveAllGivenContentsThenAddsContentSignatureChanges() {
        var content = createContent(contentVolume, "somePath/" + createFilename("txt"), NOW, CONTENT_PAYLOAD_1);
        contentRepositoryService.saveAll(Stream.of(content).collect(Collectors.toList()));

        assertThat(tFS.getContentSignatureChangeRepositoryService().findById(content.getSignature())).isPresent();
    }

    @Test
    public void testSaveGivenContentThenAddsContentSignatureChange() {
        var content = createContent(contentVolume, "somePath/" + createFilename("txt"), NOW, CONTENT_PAYLOAD_1);
        contentRepositoryService.save(content);

        assertThat(tFS.getContentSignatureChangeRepositoryService().findById(content.getSignature())).isPresent();
    }

    @Test
    public void testSaveGivenContentWithSameSignatureThenAddsContentSignatureChange() {
        var content1 = setUpPersistedContent();
        var content2 = setUpPersistedContent();

        assertThat(tFS.getContentSignatureChangeRepositoryService().findById(content1.getSignature())).isPresent();
        assertThat(tFS.getContentSignatureChangeRepositoryService().count()).isEqualTo(1);
    }
}
