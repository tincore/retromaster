package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import static com.tincore.retromaster.service.ContentEnrichService.BUFFER_SIZE;
import static com.tincore.retromaster.service.ContentEnrichService.CRC_LENGTH;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Slf4j
public class ContentEnrichServiceIntegrationTest extends AbstractContextIntegrationTest {

    @Autowired
    private ContentEnrichService contentEnrichService;

    private final ContentVolume contentVolume = new ContentVolume();

    @Test
    public void testEnrichContentGivenInputStream2ThenCalculatesSha1AndCalculatedMd5AndCalculatesCrc32AndCalculatesSizeAndAllIsUppercase() throws Exception {
        var content = contentEnrichService.enrichContent(new Content(contentVolume, "somepath"), NOW, new ByteArrayInputStream("".getBytes()), 123L);
        assertThat(content.getCrc()).isEqualTo("00000000");
    }

    @Test
    public void testEnrichContentGivenInputStreamThenCalculatesSha1AndCalculatedMd5AndCalculatesCrc32AndCalculatesSizeAndAllIsUppercase() throws Exception {
        var content = contentEnrichService.enrichContent(new Content(contentVolume, "somepath"), NOW, new ByteArrayInputStream(CONTENT_PAYLOAD_1), 123L);
        assertThat(content.getCrc()).isEqualTo(content.getCrc().toUpperCase());
        assertThat(content.getSha1()).isEqualTo(content.getSha1().toUpperCase());
        assertThat(content.getMd5()).isEqualTo(content.getMd5().toUpperCase());
    }

    @Test
    public void testEnrichContentGivenInputStreamThenCalculatesSha1AndCalculatedMd5AndCalculatesCrc32AndCalculatesSizeAndSetsPath() throws Exception {
        var content = contentEnrichService.enrichContent(new Content(contentVolume, "somepath"), NOW, new ByteArrayInputStream(CONTENT_PAYLOAD_1), 123L);

        InputStream chain = new ByteArrayInputStream(CONTENT_PAYLOAD_2);

        var checkedInputStream = new CheckedInputStream(chain, new CRC32());
        chain = checkedInputStream;

        var md5MessageDigest = MessageDigest.getInstance("MD5");
        chain = new DigestInputStream(chain, md5MessageDigest);
        var sha1MessageDigest = MessageDigest.getInstance("SHA1");
        chain = new DigestInputStream(chain, sha1MessageDigest);

        long size = 0;
        var buf = new byte[BUFFER_SIZE];
        int read;
        while ((read = chain.read(buf)) >= 0) {
            size += read;
        }

        var crc = StringUtils.leftPad(Long.toHexString(checkedInputStream.getChecksum().getValue()).toUpperCase(), CRC_LENGTH, "0");
        var md5 = Hex.encodeHexString(md5MessageDigest.digest()).toUpperCase();
        var sha1 = Hex.encodeHexString(sha1MessageDigest.digest()).toUpperCase();

        Assertions.assertThat(content)
            .satisfies(
                v -> {
                    Assertions.assertThat(v.getCrc()).isEqualTo(CONTENT_PAYLOAD_1_CRC32.toUpperCase());
                    Assertions.assertThat(v.getSha1()).isEqualTo(CONTENT_PAYLOAD_1_SHA1.toUpperCase());
                    Assertions.assertThat(v.getMd5()).isEqualTo(CONTENT_PAYLOAD_1_MD5.toUpperCase());
                    Assertions.assertThat(v.getSize()).isEqualTo(CONTENT_PAYLOAD_1.length);
                    Assertions.assertThat(v.getPathContentRelative().toString()).isEqualTo("somepath");
                });
    }

    @Test
    public void testEnrichContentGivenPathCompressedFileWithHashThenReturnsNameEqualsCompressedFileName() throws Exception {
        var content = contentEnrichService.enrichContent(new Content(contentVolume, "somepath", "somefile.zip", "sometitle.bin"), NOW, new ByteArrayInputStream(CONTENT_PAYLOAD_1), 123L);
        assertThat(content.getDirectoryPath()).isEqualTo("somepath");
        assertThat(content.getFileName()).isEqualTo("somefile.zip");
        assertThat(content.getFileEntry()).isEqualTo("sometitle.bin");
    }

    @Test
    public void testEnrichContentGivenPathCompressedFileWithSpacesWithHashThenReturnsNameEqualsCompressedFileName() throws Exception {
        var content = contentEnrichService.enrichContent(new Content(contentVolume, "somepath", "somefile.zip", "some title.bin"), NOW, new ByteArrayInputStream(CONTENT_PAYLOAD_1), 123L);
        assertThat(content.getDirectoryPath()).isEqualTo("somepath");
        assertThat(content.getFileName()).isEqualTo("somefile.zip");
        assertThat(content.getFileEntry()).isEqualTo("some title.bin");
    }

    @Test
    public void testEnrichContentGivenPathWithFileAndSpacesThenReturnsNameEqualsFileName() throws Exception {
        var content = contentEnrichService.enrichContent(new Content(contentVolume, "some path", "some file.zip"), NOW, new ByteArrayInputStream(CONTENT_PAYLOAD_1), 123L);
        assertThat(content.getDirectoryPath()).isEqualTo("some path");
        assertThat(content.getFileName()).isEqualTo("some file.zip");
        assertThat(content.getFileEntry()).isNull();
    }

    @Test
    public void testEnrichContentGivenPathWithFileThenReturnsNameEqualsFileName() throws Exception {
        var content = contentEnrichService.enrichContent(new Content(contentVolume, "somepath", "somefile.zip"), NOW, new ByteArrayInputStream(CONTENT_PAYLOAD_1), 123L);
        assertThat(content.getDirectoryPath()).isEqualTo("somepath");
        assertThat(content.getFileName()).isEqualTo("somefile.zip");
        assertThat(content.getFileEntry()).isNull();
    }
}
