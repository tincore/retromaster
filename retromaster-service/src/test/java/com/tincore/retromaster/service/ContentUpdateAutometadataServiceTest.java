package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class ContentUpdateAutometadataServiceTest extends AbstractContextIntegrationTest {

    @Autowired
    public ContentMetadataRepositoryService contentMetadataRepositoryService;

    @Autowired
    public ContentMetadataSetService contentMetadataSetService;
    @Autowired
    public ContentMetadataVolumeService contentMetadataVolumeService;
    @Autowired
    public ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    @Autowired
    private ContentUpdateAutometadataService contentUpdateAutometadataService;

    private void setUpFileRegular(ContentVolume volume, String x) {
        tFS.setUpFileRegular(volume, x, x.getBytes());
    }

    @Test
    void test_updateContentsByStageContentVolumes_Given_stagingVolumeWithLibretroPic_Then_importsPictures() throws Exception {
        var volume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.STAGE, homePath.resolve("data"), true);
        setUpFileRegular(volume, "ATARI_ST - Title LIBRETRO_PIC/10th Frame (Europe).png");
        setUpFileRegular(volume, "ATARI_ST - Snap LIBRETRO_PIC/10th Frame (Europe).png");
        setUpFileRegular(volume, "ATARI_ST - Boxart LIBRETRO_PIC/10th Frame (Europe).png");
        setUpFileRegular(volume, "ATARI_ST - Boxart LIBRETRO_PIC/10th Frame (USA).png");
        setUpFileRegular(volume, "ATARI_ST - Boxart LIBRETRO_PIC/10th Frame (World) (Beta) (v1) (En,De).png");
        setUpFileRegular(volume, "ATARI_ST - Boxart LIBRETRO_PIC/3D Construction kit.png");
        contentUpdateAutometadataService.updateContentsByStageContentVolumes();

        tFS.getTransactionRequiresNewTemplate().executeWithoutResult(t -> {
            var contentMetadataSets = contentMetadataSetRepositoryService.findAll();
            assertThat(contentMetadataSets)
                .anySatisfy(x -> {
                    assertThat(x.getCategory()).isEqualTo("LIBRETRO_PIC");
                    assertThat(x.getName()).isEqualTo("ATARI_ST - Boxart LIBRETRO_PIC");
                    assertThat(x.getContentMetadatas()).isNotEmpty();
                })
                .anySatisfy(x -> {
                    assertThat(x.getCategory()).isEqualTo("LIBRETRO_PIC");
                    assertThat(x.getName()).isEqualTo("ATARI_ST - Title LIBRETRO_PIC");
                    assertThat(x.getContentMetadatas()).isNotEmpty();
                })
                .anySatisfy(x -> {
                    assertThat(x.getCategory()).isEqualTo("LIBRETRO_PIC");
                    assertThat(x.getName()).isEqualTo("ATARI_ST - Snap LIBRETRO_PIC");
                    assertThat(x.getContentMetadatas()).isNotEmpty();
                });
        });
    }


    @Test
    void test_updateContentsByStageContentVolumes_Given_stagingVolumeWithSnapLibretroPic_Then_importsPictures() throws Exception {
        var volume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.STAGE, homePath.resolve("data"), true);
        setUpFileRegular(volume, "ATARI_ST - Snap LIBRETRO_PIC/3D Construction kit_1.png");
        setUpFileRegular(volume, "ATARI_ST - Snap LIBRETRO_PIC/3D Construction kit_2.png");
        contentUpdateAutometadataService.updateContentsByStageContentVolumes();

        tFS.getTransactionRequiresNewTemplate().executeWithoutResult(t -> {
            var contentMetadataSets = contentMetadataSetRepositoryService.findAll();
            assertThat(contentMetadataSets)
                .anySatisfy(x -> {
                    assertThat(x.getCategory()).isEqualTo("LIBRETRO_PIC");
                    assertThat(x.getName()).isEqualTo("ATARI_ST - Snap LIBRETRO_PIC");

                    assertThat(x.getContentMetadatas())
                        .anySatisfy(cm -> {
                            assertThat(cm.getTitle()).isEqualTo("3D Construction kit");
                            assertThat(cm.getMediaOrder()).isEqualTo("1");
                        })
                        .anySatisfy(cm -> {
                            assertThat(cm.getTitle()).isEqualTo("3D Construction kit");
                            assertThat(cm.getMediaOrder()).isEqualTo("2");
                        });

                });
        });
    }
}
