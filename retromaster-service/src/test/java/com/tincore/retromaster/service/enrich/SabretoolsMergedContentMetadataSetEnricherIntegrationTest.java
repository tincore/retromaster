package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import com.tincore.util.StreamTrait;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class SabretoolsMergedContentMetadataSetEnricherIntegrationTest extends AbstractContextIntegrationTest implements StreamTrait {

    @Autowired
    private SabretoolsMergedContentMetadataSetEnricher sabretoolsMergedContentMetadataEnricher;
    private String systemDefault = "someSystem";

    @Test
    public void testCanEnrichGivenCategoryEqualsSABRETOOLS_MERGEDThenReturnsTrue() {
        assertThat(sabretoolsMergedContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "SABRETOOLS_MERGED", "2019-01-01"))).isTrue();
    }

    @Test
    public void testCanEnrichGivenCategoryNotEqualsTOSECThenReturnsFalse() {
        assertThat(sabretoolsMergedContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC", "2019-01-01"))).isFalse();
        assertThat(sabretoolsMergedContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC-ISO", "2019-01-01"))).isFalse();
        assertThat(sabretoolsMergedContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC-PIX", "2019-01-01"))).isFalse();
        assertThat(sabretoolsMergedContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "NoIntro", "2019-01-01"))).isFalse();
    }

    @Test
    public void testEnrichGivenNameWithTosecQualifierThenTitleContainsContentMetadataTitleAndDateAndPublisherFromContentMetadataItem() {
        var cm = createContentMetadata(null, "Legend of TOSEC, The [TOSEC]", createContentMetadataItem("Legend of TOSEC, The (demo) (1986)(Devstudio).bin", "c".getBytes()));
        sabretoolsMergedContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isFalse();
        assertThat(cm.getTitle()).isEqualTo("Legend of TOSEC, The");
        assertThat(cm.getDate()).isEqualTo("1986");
        assertThat(cm.getPublisher()).isEqualTo("Devstudio");
    }
}
