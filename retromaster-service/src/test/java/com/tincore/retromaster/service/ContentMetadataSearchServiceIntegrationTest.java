package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMediaType;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.search.QueryFacetFilter;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentMetadata.*;
import static com.tincore.retromaster.service.ContentMetadataSearchService.*;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ContentMetadataSearchServiceIntegrationTest extends AbstractContextIntegrationTest {

    public static final PageRequest PAGE_REQUEST = PageRequest.of(0, 100);

    public String system1 = "system" + RandomStringUtils.randomAlphanumeric(4);
    public String system2 = "system" + RandomStringUtils.randomAlphanumeric(4);

    public String titleB = "BnameBBB";
    public String titleD = "DnameDDD";

    @Autowired
    private ContentMetadataSearchService contentMetadataSearchService;

    private ContentMetadata contentMetadata1a_total;
    private ContentMetadata contentMetadata1b;

    private ContentMetadata contentMetadata2a_empty;
    private ContentMetadata contentMetadata2b_partial;
    private ContentMetadata contentMetadata2c_total;


    public QueryFacetFilter createQueryFacetFilter(String field, String... values) {
        return QueryFacetFilter.builder().field(field).values(Stream.of(values).collect(Collectors.toSet())).build();
    }

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();

        contentMetadata1a_total = tFS.createContentMetadata(null, "AnameAAA", ContentMetadata.COMPLETE_TOTAL);
        contentMetadata1b = tFS.createContentMetadata(null, titleB, COMPLETE_EMPTY);

        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system1 + " - tosecStyleSetName1", "TOSEC", "s1", contentMetadata1a_total, contentMetadata1b));

        contentMetadata2a_empty = tFS.createContentMetadata(null, "CnameCCC", COMPLETE_EMPTY);
        contentMetadata2b_partial = tFS.createContentMetadata(null, titleB, ContentMetadata.COMPLETE_PARTIAL);
        contentMetadata2c_total = tFS.createContentMetadata(null, titleD, ContentMetadata.COMPLETE_TOTAL);


        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system2 + " - tosecStyleSetName2", "TOSEC", "s1", contentMetadata2a_empty, contentMetadata2b_partial, contentMetadata2c_total));
    }

    @Test
    public void testSearchByMetadataSetIdAndCompleteGivenContentMetadataCompleteEmptyWhenMetadataSetIdEqualsMetadataSetIdAndCompletexEqualsContentMetadataSystemCompleteThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndComplete(contentMetadata2a_empty.getContentMetadataSet().getId(), COMPLETE_EMPTY, PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2a_empty.getId()));
    }

    @Test
    public void testSearchByMetadataSetIdAndCompleteGivenContentMetadataCompletePartialWhenMetadataSetIdEqualsMetadataSetIdAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndComplete(contentMetadata2b_partial.getContentMetadataSet().getId(), COMPLETE_PARTIAL, PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2b_partial.getId()));
    }

    @Test
    public void testSearchByMetadataSetIdAndCompleteGivenContentMetadataCompleteTotalWhenMetadataSetIdEqualsContentMetadataSetIdAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndComplete(contentMetadata2c_total.getContentMetadataSet().getId(), COMPLETE_TOTAL, PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2c_total.getId()));
    }

    @Test
    public void testSearchByMetadataSetIdAndCompleteGivenContentMetadataWhenMetadataSetIdEqualsMetadataSetIdAndCompleteNotEqualsContentMetadataCompleteThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndComplete(contentMetadata1a_total.getContentMetadataSet().getId(), COMPLETE_PARTIAL, PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchByMetadataSetIdAndCompleteGivenContentMetadataWhenMetadataSetIdNotEqualsMetadataSetIdThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndComplete(UUID.randomUUID(), COMPLETE_EMPTY, PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataCompleteEmptyWhenMetadataSetIdEqualsMetadataSetIdAndPrefixEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndPrefixAndCompleteNotEmpty(contentMetadata2a_empty.getContentMetadataSet().getId(), contentMetadata2a_empty.getPrefix(), PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataCompleteFullWhenMetadataSetIdEqualsContentMetadataSetIdAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndPrefixAndCompleteNotEmpty(contentMetadata2c_total.getContentMetadataSet().getId(), contentMetadata2c_total.getPrefix(), PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2c_total.getId()));
    }

    @Test
    public void testSearchByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataCompletePartialWhenMetadataSetIdEqualsMetadataSetIdAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndPrefixAndCompleteNotEmpty(contentMetadata2b_partial.getContentMetadataSet().getId(), contentMetadata2b_partial.getPrefix(), PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2b_partial.getId()));
    }

    @Test
    public void testSearchByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataWhenMetadataSetIdEqualsMetadataSetIdAndPrefixNotEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndPrefixAndCompleteNotEmpty(contentMetadata2c_total.getContentMetadataSet().getId(), "Z", PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataWhenMetadataSetIdNotEqualsMetadataSetIdAndPrefixEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchByMetadataSetIdAndPrefixAndCompleteNotEmpty(UUID.randomUUID(), contentMetadata2c_total.getPrefix(), PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataCompleteEmptyWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchBySystemAndPrefixAndCompleteNotEmpty(contentMetadata2a_empty.getSystem(), contentMetadata2a_empty.getPrefix(), PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataCompleteFullWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchBySystemAndPrefixAndCompleteNotEmpty(contentMetadata2c_total.getSystem(), contentMetadata2c_total.getPrefix(), PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2c_total.getId()));
    }

    @Test
    public void testSearchBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataCompletePartialWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchBySystemAndPrefixAndCompleteNotEmpty(contentMetadata2b_partial.getSystem(), contentMetadata2b_partial.getPrefix(), PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2b_partial.getId()));
    }

    @Test
    public void testSearchBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataCompletePartialWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadataX() {
        var contentMetadata2b_image = tFS.createContentMetadata(null, titleB, ContentMetadata.COMPLETE_PARTIAL, createContentMetadataItem("somePicture.jpg"));
        contentMetadata2b_image.setMediaType(ContentMediaType.PIC_BOXART);
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system2 + " - Snap", "LIBRETRO_PIC", "s1", contentMetadata2b_image));

        assertThat(contentMetadataSearchService.searchBySystemAndTitleAndMediaTypeAndCompleteNotEmpty(contentMetadata2b_partial.getSystem(), contentMetadata2b_partial.getTitle(), contentMetadata2b_image.getMediaType(), PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2b_image.getId()));
    }

    @Test
    public void testSearchBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataWhenSystemEqualsContentMetadataSystemAndPrefixNotEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchBySystemAndPrefixAndCompleteNotEmpty(contentMetadata2c_total.getSystem(), "Z", PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataWhenSystemNotEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchBySystemAndPrefixAndCompleteNotEmpty("NonExisting", contentMetadata2c_total.getPrefix(), PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchBySystemAndPrefixGivenContentMetadataWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchBySystemAndPrefix(contentMetadata1a_total.getSystem(), contentMetadata1a_total.getPrefix(), PAGE_REQUEST))
            .singleElement()
            .satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata1a_total.getId()));
    }

    @Test
    public void testSearchBySystemAndPrefixGivenContentMetadataWhenSystemEqualsContentMetadataSystemAndPrefixNotEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchBySystemAndPrefix(contentMetadata1a_total.getSystem(), "Z", PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchBySystemAndPrefixGivenContentMetadataWhenSystemNotEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchBySystemAndPrefix("NonExisting", contentMetadata1a_total.getPrefix(), PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchBySystemAndTitleAndMediaImageAndCompleteNotEmptyGivenMultipleImageCOntentMetadatasWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadataX() {
        var contentMetadataBoxArtImage = tFS.createContentMetadata(null, titleB, ContentMetadata.COMPLETE_PARTIAL, ContentMediaType.PIC_BOXART, createContentMetadataItem("someBoxArtPicture.jpg"));
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system2 + " - Boxart", "LIBRETRO_PIC", "s1", contentMetadataBoxArtImage));

        var contentMetadataSnapImage = tFS.createContentMetadata(null, titleB, ContentMetadata.COMPLETE_PARTIAL, ContentMediaType.PIC_SNAPSHOT, createContentMetadataItem("someSnapshotPicture.jpg"));
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system2 + " - Snap", "LIBRETRO_PIC", "s1", contentMetadataSnapImage));

        var contentMetadataTitleImage = tFS.createContentMetadata(null, titleB, ContentMetadata.COMPLETE_PARTIAL, ContentMediaType.PIC_TITLE, createContentMetadataItem("someSnapshotPicture.jpg"));
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system2 + " - Title", "LIBRETRO_PIC", "s1", contentMetadataTitleImage));

        var contentMetadataOther = tFS.createContentMetadata(null, titleB, ContentMetadata.COMPLETE_PARTIAL, ContentMediaType.DISC, createContentMetadataItem("someDisc.dsk"));
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system2 + " - tosecStyleSetName1", "TOSEC", "s1", contentMetadataOther));

        assertThat(contentMetadataSearchService.searchBySystemAndTitleAndMediaImageAndCompleteNotEmpty(contentMetadata2b_partial.getSystem(), contentMetadata2b_partial.getTitle(), PAGE_REQUEST))
            .hasSize(3)
            .anySatisfy(c -> assertThat(c.getId()).isEqualTo(contentMetadataBoxArtImage.getId()))
            .anySatisfy(c -> assertThat(c.getId()).isEqualTo(contentMetadataSnapImage.getId()))
            .anySatisfy(c -> assertThat(c.getId()).isEqualTo(contentMetadataTitleImage.getId()));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataSetWhenQueryEmptyThenReturnsAllDefaultFacets() {
        var datas = contentMetadataSearchService.searchByTextPaged("", Collections.emptyList(), ContentMediaType.PIC, PAGE_REQUEST);
        assertThat(datas.getFacets())
            .hasSize(BY_TEXT_DEFAULT_QUERY_FACET_FILTERS.size())
            .anySatisfy(f -> assertThat(f.getField()).isEqualTo(ContentMetadataSearchService.FACET_SYSTEM))
            .anySatisfy(f -> assertThat(f.getField()).isEqualTo(ContentMetadataSearchService.FACET_COMPLETE))
            .anySatisfy(f -> assertThat(f.getField()).isEqualTo(ContentMetadataSearchService.FACET_PUBLISHER))
            .anySatisfy(f -> assertThat(f.getField()).isEqualTo(ContentMetadataSearchService.FACET_YEAR))
            .anySatisfy(f -> assertThat(f.getField()).isEqualTo(ContentMetadataSearchService.FACET_SOURCE));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataSetWhenQueryThenReturnsSystemFacetSystemAndFacetSystemContainsAllSystemsCounts() {
        var datas = contentMetadataSearchService.searchByTextPaged("", Collections.emptyList(), ContentMediaType.PIC, PAGE_REQUEST);
        assertThat(datas.getFacets()).anySatisfy(f -> assertThat(f.getFacetName()).isEqualTo(ContentMetadataSearchService.FACET_SYSTEM));
        assertThat(datas.getFacets().stream().filter(f -> f.getFacetName().equals(ContentMetadataSearchService.FACET_SYSTEM)))
            .singleElement()
            .satisfies(
                f -> assertThat(f.getValues())
                    // .hasSize(2)
                    .anySatisfy(
                        qrf -> {
                            assertThat(qrf.getValue()).isEqualTo(system1);
                            assertThat(qrf.getCount()).isEqualTo(2);
                        })
                    .anySatisfy(
                        qrf -> {
                            assertThat(qrf.getValue()).isEqualTo(system2);
                            assertThat(qrf.getCount()).isEqualTo(3);
                        }));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataSetWhenQueryWithFacetFilterThenReturnsFacetSystemAndFacetSystemContainsAllSystemsCounts() {
        var queryFacetFilters = Stream.of(createQueryFacetFilter(ContentMetadataSearchService.FACET_SYSTEM, system1)).collect(Collectors.toList());
        var datas = contentMetadataSearchService.searchByTextPaged("", queryFacetFilters, ContentMediaType.PIC, PAGE_REQUEST);
        assertThat(datas.getTotalElements()).isEqualTo(2);

        assertThat(datas.getFacets()).anySatisfy(f -> assertThat(f.getFacetName()).isEqualTo(ContentMetadataSearchService.FACET_SYSTEM));
        assertThat(datas.getFacets().stream().filter(f -> f.getFacetName().equals(ContentMetadataSearchService.FACET_SYSTEM)))
            .singleElement()
            .satisfies(
                f -> assertThat(f.getValues())
                    // .hasSize(2)
                    .anySatisfy(
                        qrf -> {
                            assertThat(qrf.getValue()).isEqualTo(system1);
                            assertThat(qrf.getCount()).isEqualTo(2);
                            assertThat(qrf.isSelected()).isTrue();
                        })
                    .anySatisfy(
                        qrf -> {
                            assertThat(qrf.getValue()).isEqualTo(system2);
                            assertThat(qrf.getCount()).isEqualTo(3);
                            assertThat(qrf.isSelected()).isFalse();
                        }));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataWhenQueryEqualsContentMetadataNameAndTitleHasSingleCharacterThenReturnsMatchingContentMetadata() {
        var system = "system" + RandomStringUtils.randomAlphanumeric(4);
        var cm = tFS.createContentMetadata(null, "y").toBuilder().complete(COMPLETE_EMPTY).build();
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system + " - tosecStyleSetName1", "TOSEC", "s1", cm));

        var result = contentMetadataSearchService.searchByTextPaged("Y", Collections.emptyList(), ContentMediaType.PIC, PAGE_REQUEST);
        assertThat(result).singleElement().satisfies(c -> assertThat(c.getId()).isEqualTo(cm.getId()));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataWhenQueryEqualsContentMetadataNameThenReturnsMatchingContentMetadata() {
        var result = contentMetadataSearchService.searchByTextPaged(contentMetadata2c_total.getTitle(), Collections.emptyList(), ContentMediaType.PIC, PAGE_REQUEST);
        assertThat(result).singleElement().satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2c_total.getId()));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataWhenQueryShortButLongerThanTitleThenReturnsEmpty() {
        var system = "system" + RandomStringUtils.randomAlphanumeric(4);
        var cm = tFS.createContentMetadata(null, "z").toBuilder().complete(COMPLETE_EMPTY).build();
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system + " - tosecStyleSetName1", "TOSEC", "s1", cm));

        assertThat(contentMetadataSearchService.searchByTextPaged("Zz", Collections.emptyList(), ContentMediaType.PIC, PAGE_REQUEST)).isEmpty();
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataWhenQuerySingleCharacterThenReturnsMatchingContentMetadata() {
        var system = "system" + RandomStringUtils.randomAlphanumeric(4);
        var cm = tFS.createContentMetadata(null, "zz").toBuilder().complete(COMPLETE_EMPTY).build();
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system + " - tosecStyleSetName1", "TOSEC", "s1", cm));

        var result = contentMetadataSearchService.searchByTextPaged("Z", Collections.emptyList(), ContentMediaType.PIC, PAGE_REQUEST);
        assertThat(result).singleElement().satisfies(c -> assertThat(c.getId()).isEqualTo(cm.getId()));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataWhenQueryStartsWithContentMetadataNameThenReturnsMatchingContentMetadata() {
        var result = contentMetadataSearchService.searchByTextPaged(contentMetadata2c_total.getTitle().substring(0, 4), Collections.emptyList(), ContentMediaType.PIC, PAGE_REQUEST);
        assertThat(result).singleElement().satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2c_total.getId()));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadataWhenQueryWithNameEqualsContentMetadataNameAndFacetFilterMatchesThenReturnsMatchingContentMetadatas() {
        var queryFacetFilters = Stream.of(createQueryFacetFilter(FACET_SYSTEM, contentMetadata2c_total.getSystem()), createQueryFacetFilter(FACET_PUBLISHER)).collect(Collectors.toList());

        var result = contentMetadataSearchService.searchByTextPaged(contentMetadata2c_total.getTitle(), queryFacetFilters, ContentMediaType.PIC, PAGE_REQUEST);

        assertThat(result).singleElement().satisfies(c -> assertThat(c.getId()).isEqualTo(contentMetadata2c_total.getId()));
    }

    @Test
    public void testSearchByTextPagedGivenContentMetadatasWhenQueryEqualsContentMetadataNamesThenReturnsMatchingContentMetadatas() {
        var result = contentMetadataSearchService.searchByTextPaged(titleB, Collections.emptyList(), ContentMediaType.PIC, PAGE_REQUEST);
        assertThat(result).hasSize(2).containsExactlyInAnyOrder(contentMetadata1b, contentMetadata2b_partial);
    }

    @Test
    public void testSearchContentMetadataPrefixByContentMetadataSetIdAndContentNotEmptyGivenContentMetadataContentMetadataSetIdEqualsContentMetadataSetIdAndCompleteEmptyThenReturnsEmpty() {
        var contentMetadataSetId = "ContentMetadataSetId" + RandomStringUtils.randomAlphanumeric(4);
        var contentMetadataSet
            = tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(contentMetadataSetId + " - tosecStyleSetName1", "TOSEC", "s1", tFS.createContentMetadata(null, "titB").toBuilder().complete(COMPLETE_EMPTY).build()));

        assertThat(contentMetadataSearchService.searchContentMetadataPrefixByContentMetadataSetIdAndCompleteNotEmpty(contentMetadataSet.getId())).isEmpty();
    }

    @Test
    public void testSearchContentMetadataPrefixByContentMetadataSetIdAndContentNotEmptyGivenContentMetadatasThenReturnsPrefixes() {
        assertThat(contentMetadataSearchService.searchContentMetadataPrefixByContentMetadataSetIdAndCompleteNotEmpty(contentMetadata1a_total.getContentMetadataSet().getId())).singleElement().isEqualTo(contentMetadata1a_total.getPrefix());
    }

    @Test
    public void testSearchContentMetadataPrefixByContentMetadataSetIdAndContentNotEmptyGivenNoContentMetadataContentMetadataSetIdEqualsToContentMetadataSetIdThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchContentMetadataPrefixByContentMetadataSetIdAndCompleteNotEmpty(UUID.randomUUID())).isEmpty();
    }

    @Test
    public void testSearchContentMetadataPrefixBySystemAndContentNotEmptyGivenContentMetadataSystemEqualsSystemAndCompleteEmptyThenReturnsEmpty() {
        var system = "system" + RandomStringUtils.randomAlphanumeric(4);
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(system + " - tosecStyleSetName1", "TOSEC", "s1", tFS.createContentMetadata(null, "titB").toBuilder().complete(COMPLETE_EMPTY).build()));

        assertThat(contentMetadataSearchService.searchContentMetadataPrefixBySystemAndContentNotEmpty(system)).isEmpty();
    }

    @Test
    public void testSearchContentMetadataPrefixBySystemAndContentNotEmptyGivenContentMetadatasThenReturnsPrefixes() {
        assertThat(contentMetadataSearchService.searchContentMetadataPrefixBySystemAndContentNotEmpty(system1)).singleElement().isEqualTo(contentMetadata1a_total.getPrefix());
    }

    @Test
    public void testSearchContentMetadataPrefixBySystemAndContentNotEmptyGivenNoContentMetadataSystemEqualsToSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchContentMetadataPrefixBySystemAndContentNotEmpty("UNKNOWN_S")).isEmpty();
    }

    @Test
    public void testSearchContentMetadataSystemsByContentNotEmptyGivenContentMetadatasThenReturnsSystemsWithAnyNonEmptyContentMetadata() {
        var ContentMetadataSetId = "ContentMetadataSetId" + RandomStringUtils.randomAlphanumeric(4);
        tFS.setUpPersistedContentMetadataSet(tFS.createContentMetadataSet(ContentMetadataSetId + " - tosecStyleSetName1", "TOSEC", "s1", tFS.createContentMetadata(null, "titB").toBuilder().complete(COMPLETE_EMPTY).build()));

        assertThat(contentMetadataSearchService.searchContentMetadataSystemsByContentNotEmpty()).containsExactlyInAnyOrder(system1, system2);
    }

    @Test
    public void testSearchIdsByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataCompleteEmptyWhenMetadataSetIdEqualsContentMetadataMetadataSetIdAndPrefixEqualsContentMetadataMetadataSetIdThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchIdsByMetadataSetIdAndPrefixAndCompleteNotEmpty(contentMetadata2a_empty.getContentMetadataSet().getId(), contentMetadata2a_empty.getPrefix())).isEmpty();
    }

    @Test
    public void testSearchIdsByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataCompleteFullWhenMetadataSetIdEqualsContentMetadataMetadataSetIdAndPrefixEqualsContentMetadataMetadataSetIdThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchIdsByMetadataSetIdAndPrefixAndCompleteNotEmpty(contentMetadata2c_total.getContentMetadataSet().getId(), contentMetadata2c_total.getPrefix()))
            .singleElement()
            .isEqualTo(contentMetadata2c_total.getId());
    }

    @Test
    public void testSearchIdsByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataCompletePartialWhenMetadataSetIdEqualsContentMetadataMetadataSetIdAndPrefixEqualsContentMetadataMetadataSetIdThenReturnsContentMetadatay() {
        assertThat(contentMetadataSearchService.searchIdsByMetadataSetIdAndPrefixAndCompleteNotEmpty(contentMetadata2b_partial.getContentMetadataSet().getId(), contentMetadata2b_partial.getPrefix()))
            .singleElement()
            .isEqualTo(contentMetadata2b_partial.getId());
    }

    @Test
    public void testSearchIdsByMetadataSetIdAndPrefixAndCompleteNotEmptyGivenContentMetadataWhenMetadataSetIdNotEqualsContentMetadataMetadataSetIdAndPrefixEqualsContentMetadataMetadataSetIdThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchIdsByMetadataSetIdAndPrefixAndCompleteNotEmpty(UUID.randomUUID(), contentMetadata2c_total.getPrefix())).isEmpty();
    }

    @Test
    public void testSearchIdsBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataCompleteEmptyWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchIdsBySystemAndPrefixAndCompleteNotEmpty(contentMetadata2a_empty.getSystem(), contentMetadata2a_empty.getPrefix())).isEmpty();
    }

    @Test
    public void testSearchIdsBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataCompleteFullWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadata() {
        assertThat(contentMetadataSearchService.searchIdsBySystemAndPrefixAndCompleteNotEmpty(contentMetadata2c_total.getSystem(), contentMetadata2c_total.getPrefix())).singleElement().isEqualTo(contentMetadata2c_total.getId());
    }

    @Test
    public void testSearchIdsBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataCompletePartialWhenSystemEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsContentMetadatay() {
        assertThat(contentMetadataSearchService.searchIdsBySystemAndPrefixAndCompleteNotEmpty(contentMetadata2b_partial.getSystem(), contentMetadata2b_partial.getPrefix())).singleElement().isEqualTo(contentMetadata2b_partial.getId());
    }

    @Test
    public void testSearchIdsBySystemAndPrefixAndCompleteNotEmptyGivenContentMetadataWhenSystemNotEqualsContentMetadataSystemAndPrefixEqualsContentMetadataSystemThenReturnsEmpty() {
        assertThat(contentMetadataSearchService.searchIdsBySystemAndPrefixAndCompleteNotEmpty("NonExisting", contentMetadata2c_total.getPrefix())).isEmpty();
    }

}
