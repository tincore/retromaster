package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadataSet;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Slf4j
public class ContentServiceIntegrationTest extends AbstractContextIntegrationTest {

    @Autowired
    private ContentService contentService;

    private ContentVolume volume;
    private ContentMetadataSet contentMetadataSet;

    @BeforeEach
    public void setUp() throws Exception {
        volume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data"), true);
        contentMetadataSet = tFS.setUpPersistedContentMetadataSet(5, 2);
    }

    @Test
    public void testDeleteByContentMetadataIdAndMissingFilesGivenContentMatchingWithSetAndFileNotPresentThenContentDeleted() {
        var content = tFS.setUpPersistedContent(volume, contentMetadataSet.getRepositoryPath() + "/" + createFilename("txt"), false, createContentPayload(0, 1).getBytes());
        contentService.deleteContentsByMetadataIdAndMissingFiles(contentMetadataSet.getContentMetadatas().get(0).getId());
        assertThat(tFS.getContentRepositoryService().findById(content.getId())).isNotPresent();
    }

    @Test
    public void testDeleteByContentMetadataIdAndMissingFilesGivenContentMatchingWithSetThenContentNotDeleted() {
        var content = tFS.setUpPersistedContent(volume, contentMetadataSet.getRepositoryPath() + "/" + createFilename("txt"), true, createContentPayload(0, 1).getBytes());
        contentService.deleteContentsByMetadataIdAndMissingFiles(contentMetadataSet.getContentMetadatas().get(0).getId());
        assertThat(tFS.getContentRepositoryService().findById(content.getId())).isPresent();
    }

    @Test
    public void testDeleteByContentVolumeAndContentMetadataSetAndMissingFilesGivenContentMatchingWithSetAndFileNotPresentThenContentDeleted() {
        var content = tFS.setUpPersistedContent(volume, contentMetadataSet.getRepositoryPath() + "/" + createFilename("txt"), false, createContentPayload(1, 1).getBytes());
        contentService.deleteContentsByVolumeAndMetadataSetAndMissingFiles(volume, contentMetadataSet);
        assertThat(tFS.getContentRepositoryService().findById(content.getId())).isNotPresent();
    }

    @Test
    public void testDeleteByContentVolumeAndContentMetadataSetAndMissingFilesGivenContentMatchingWithSetThenContentNotDeleted() {
        var content = tFS.setUpPersistedContent(volume, contentMetadataSet.getRepositoryPath() + "/" + createFilename("txt"), true, createContentPayload(1, 1).getBytes());
        contentService.deleteContentsByVolumeAndMetadataSetAndMissingFiles(volume, contentMetadataSet);
        assertThat(tFS.getContentRepositoryService().findById(content.getId())).isPresent();
    }

    @Test
    @Disabled
    public void testDeleteByContentVolumeAndContentMetadataSetAndMissingFilesGivenNoContentsThenDoesNothing() {
        contentService.deleteContentsByVolumeAndMetadataSetAndMissingFiles(volume, contentMetadataSet);
        assertThat(tFS.getContentRepositoryService().findAllByVolume(volume, Pageable.unpaged()).getTotalElements()).isEqualTo(0);
    }

    @Test
    public void testDeleteByContentVolumeAndMissingFilesGivenContentMatchingWithSetAndFileNotPresentThenContentDeleted() {
        var content = tFS.setUpPersistedContent(volume, contentMetadataSet.getRepositoryPath() + "/" + createFilename("txt"), false, createContentPayload(1, 1).getBytes());
        contentService.deleteContentsByVolumeAndMissingFiles(volume);
        assertThat(tFS.getContentRepositoryService().findById(content.getId())).isNotPresent();
    }

    @Test
    public void testDeleteByContentVolumeAndMissingFilesGivenContentMatchingWithSetThenContentNotDeleted() {
        var content = tFS.setUpPersistedContent(volume, contentMetadataSet.getRepositoryPath() + "/" + createFilename("txt"), true, createContentPayload(1, 1).getBytes());
        contentService.deleteContentsByVolumeAndMissingFiles(volume);
        assertThat(tFS.getContentRepositoryService().findById(content.getId())).isPresent();
    }
}
