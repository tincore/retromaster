package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

// MAKE TEST DAT FILES AND ASSERT

@BehaviourGroup(scenario = "ContentMetadataSet")
public class ContentMetadataSetImportServiceTosecIntegrationTest extends AbstractContextIntegrationTest {

    @Autowired
    public ContentMetadataSetImportService contentMetadataSetImportService;

    @Autowired
    public ContentMetadataSetService contentMetadataSetService;

    @Autowired
    public ContentMetadataVolumeService contentMetadataVolumeService;

    private Path getImportPath() {
        return contentMetadataVolumeService.getImportPath();
    }

    @Test
    public void testCreateGivenEmptyPathThenExecutesSuccessfully() {
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
    }

    @Test
    public void testImportByPathGivenExistingNointroResourceAndValidThenSavesMetadata() throws IOException {
        copyToPaths("classpath:/test/dat/nointro/*.dat", getImportPath());
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
    }

    @Test
    public void testImportByPathGivenExistingResourceAndValid2ThenSavesMetadata() throws IOException {
        copyToPaths("classpath:/test/dat/tosec/TOSEC/test_to_2.dat", getImportPath());
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
    }

    @Test
    public void testImportByPathGivenExistingResourceAndValidThenSavesMetadata() throws IOException {
        copyToPaths("classpath:/test/dat/tosec/TOSEC/test_to_1.dat", getImportPath());
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
    }

    @Test
    public void testImportByPathGivenExistingResourceAndValidThenSavesUpdatingMetadata() throws IOException {
        copyToPaths("classpath:/test/dat/tosec/TOSEC/test_to_1.dat", getImportPath());
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
        copyToPaths("classpath:/test/dat/tosec/TOSEC/test_to_1.dat", getImportPath());
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
    }

    @Test
    public void testImportByPathGivenExistingTosecIsoResourceAndValidThenSavesMetadata() throws IOException {
        copyToPaths("classpath:/test/dat/tosec/TOSEC-ISO/*.dat", getImportPath());
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
    }
    @Test
    public void testImportByPathGivenExistingTosecPixResourceAndValidThenSavesMetadata() throws IOException {
        copyToPaths("classpath:/test/dat/tosec/TOSEC-PIX/*.dat", getImportPath());
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
    }



    @Test
    public void testImportByPathGivenExistingTosecResourceAndValidThenSavesMetadata() throws IOException {
        var paths = copyToPaths("classpath:/test/dat/tosec/TOSEC/*.dat", getImportPath());
        assertThat(contentMetadataSetImportService.doContentMetadataSetImportByDirectory(getImportPath())).isTrue();
    }
}
