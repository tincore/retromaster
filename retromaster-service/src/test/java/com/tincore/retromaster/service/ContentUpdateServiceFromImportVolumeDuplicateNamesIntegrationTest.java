package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.service.archive.producer.BytesArchiveEntryProducer;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ContentUpdateServiceFromImportVolumeDuplicateNamesIntegrationTest extends AbstractContextIntegrationTest {

    private final byte[] contentMetadataItemAPayload = RandomStringUtils.randomAlphabetic(20).getBytes();
    private final byte[] contentMetadataItemBPayload = RandomStringUtils.randomAlphabetic(20).getBytes();

    @Autowired
    private ContentUpdateService contentUpdateService;

    private ContentVolume importVolume;
    private ContentVolume productionVolume;

    private ContentMetadataItem contentMetadataItemA;
    private ContentMetadataItem contentMetadataItemB;

    private Path importFilePath1;
    private Path importFilePath2;

    public String appendFileBasenameSuffix(String fileName, String suffix) {
        return FilenameUtils.getBaseName(fileName) + suffix + FilenameUtils.EXTENSION_SEPARATOR + FilenameUtils.getExtension(fileName);
    }

    @BeforeEach
    public void setUp() throws Exception {
        importVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.IMPORT, homePath.resolve("data"), true);
        productionVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data"), true);

        contentMetadataItemA = createContentMetadataItem("name.txt", contentMetadataItemAPayload);
        contentMetadataItemB = createContentMetadataItem("name.txt", contentMetadataItemBPayload);

        importFilePath1 = tFS.setUpFileArchived(importVolume.toPath().resolve(createFileNameZip()), new BytesArchiveEntryProducer(contentMetadataItemAPayload, "blah/input1.txt"));
        importFilePath2 = tFS.setUpFileArchived(importVolume.toPath().resolve(createFileNameZip()), new BytesArchiveEntryProducer(contentMetadataItemBPayload, "blah/input2.txt"));
    }

    @Test
    public void testDoContentImportGivenImportFilesPresentAndContentMetadatasMergeAndContentMetadatasSameExactTitleThenSingleCompressedFileCreatedAndEachMetadataInSeparateDirectoryWithModifiedName() throws Exception {
        var contentMetadataTitle = "cmTitle" + RandomStringUtils.randomAlphabetic(4);
        var contentMetadataA = createContentMetadata(null, contentMetadataTitle, contentMetadataItemA);
        var contentMetadataB = createContentMetadata(null, contentMetadataTitle, contentMetadataItemB);
        tFS.setUpPersistedContentMetadataSet(createContentMetadataSet("cms_" + RandomStringUtils.randomNumeric(4), "TOSEC", "2019-01-01", contentMetadataA, contentMetadataB));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFilePath1).doesNotExist();
        assertThat(importFilePath2).doesNotExist();

        var productionFilePath = tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataA);
        assertThat(productionFilePath).exists();

        var entryNameA = contentMetadataA.getName() + "_" + contentMetadataA.getId() + "/" + contentMetadataItemA.getName();
        var entryNameB = contentMetadataB.getName() + "_" + contentMetadataB.getId() + "/" + contentMetadataItemB.getName();

        assertThatArchiveFileContentContains(productionFilePath, entryNameA, contentMetadataItemAPayload);
        assertThatArchiveFileContentContains(productionFilePath, entryNameB, contentMetadataItemBPayload);

        assertThatContentArchivedExists(productionVolume, contentMetadataItemA, entryNameA);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemB, entryNameB);
    }

    @Test
    public void testDoContentImportGivenImportFilesPresentAndContentMetadatasMergeAndContentMetadatasSameTitleAndPublisherAndYearAndContentMetadataItemSameNameThenSingleCompressedFileCreatedAndEachMetadataInSepparateDirectory()
        throws Exception {
        var contentMetadataTitle = "cmTitle" + RandomStringUtils.randomAlphabetic(4);
        var contentMetadataA = createContentMetadata(null, contentMetadataTitle + " (1980)(test)(US)", contentMetadataItemA);
        var contentMetadataB = createContentMetadata(null, contentMetadataTitle + " (1980)(test)(JP)", contentMetadataItemB);
        tFS.setUpPersistedContentMetadataSet(createContentMetadataSet("cms_" + RandomStringUtils.randomNumeric(4), "TOSEC", "2019-01-01", contentMetadataA, contentMetadataB));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFilePath1).doesNotExist();
        assertThat(importFilePath2).doesNotExist();

        var productionFilePath = tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataA);
        assertThat(productionFilePath).exists();
        var entryNameA = contentMetadataA.getName() + "/" + contentMetadataItemA.getName();
        var entryNameB = contentMetadataB.getName() + "/" + contentMetadataItemB.getName();
        assertThatArchiveFileContentContains(productionFilePath, entryNameA, contentMetadataItemAPayload);
        assertThatArchiveFileContentContains(productionFilePath, entryNameB, contentMetadataItemBPayload);

        assertThatContentArchivedExists(productionVolume, contentMetadataItemA, entryNameA);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemB, entryNameB);
    }

    @Test
    public void testDoContentImportGivenImportFilesPresentAndContentMetadatasWithTwoEntriesWithSameTitleAndPublisherAndYearAndContentMetadataItemSameNameThenSingleCompressedFileCreatedAndEachMetadataInSepparateDirectory() throws Exception {
        var contentMetadataTitle = "cmTitle" + RandomStringUtils.randomAlphabetic(4);
        var contentMetadataA = createContentMetadata(null, contentMetadataTitle + " (1980)(test)(US)", contentMetadataItemA, contentMetadataItemB);

        tFS.setUpPersistedContentMetadataSet(createContentMetadataSet("cms_" + RandomStringUtils.randomNumeric(4), "TOSEC", "2019-01-01", contentMetadataA));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFilePath1).doesNotExist();
        assertThat(importFilePath2).doesNotExist();

        var productionFilePath = tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataA);
        assertThat(productionFilePath).exists();
        var crcA = getCrc(contentMetadataItemAPayload);
        var entryNameA = contentMetadataA.getName() + "_" + contentMetadataA.getId() + "/" + appendFileBasenameSuffix(contentMetadataItemA.getName(), "_" + crcA);
        var crcB = getCrc(contentMetadataItemBPayload);
        var entryNameB = contentMetadataA.getName() + "_" + contentMetadataA.getId() + "/" + appendFileBasenameSuffix(contentMetadataItemA.getName(), "_" + crcB);
        assertThatArchiveFileContentContains(productionFilePath, entryNameA, contentMetadataItemAPayload);
        assertThatArchiveFileContentContains(productionFilePath, entryNameB, contentMetadataItemBPayload);

        assertThatContentArchivedExists(productionVolume, contentMetadataItemA, entryNameA);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemB, entryNameB);
    }
}
