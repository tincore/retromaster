package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "ContentMetadataSet")
public class ContentMetadataSetServiceIntegrationTest extends AbstractContextIntegrationTest {

    @Autowired
    public ContentMetadataSetService contentMetadataSetService;

    @Autowired
    private ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;

    @Test
    public void testDeleteGivenContentMetadataSetAndBigThenContentMetadataSetRemoved() {
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(50, 100);

        contentMetadataSetService.delete(contentMetadataSet);

        assertThat(contentMetadataSetRepositoryService.findById(contentMetadataSet.getId())).isNotPresent();
    }

    @Test
    public void testDeleteGivenContentMetadataSetAndOtherBigThenContentMetadataSetRemoved() {
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(5000, 2);

        contentMetadataSetService.delete(contentMetadataSet);

        assertThat(contentMetadataSetRepositoryService.findById(contentMetadataSet.getId())).isNotPresent();
    }

    @Test
    public void testDeleteGivenContentMetadataSetThenContentMetadataSetRemoved() {
        var contentMetadataSet = tFS.setUpPersistedContentMetadataSet(5, 10);

        contentMetadataSetService.delete(contentMetadataSet);

        assertThat(contentMetadataSetRepositoryService.findById(contentMetadataSet.getId())).isNotPresent();
    }
}
