package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentMediaType;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import com.tincore.util.StreamTrait;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class TosecContentMetadataSetEnricherIntegrationTest extends AbstractContextIntegrationTest implements StreamTrait {

    @Autowired
    private TosecContentMetadataSetEnricher tosecContentMetadataEnricher;
    private String systemDefault = "someSystem";

    @Test
    public void testCanEnrichGivenCategoryEqualsTOSECThenReturnsTrue() {
        assertThat(tosecContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC", "2019-01-01"))).isTrue();
        assertThat(tosecContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC-ISO", "2019-01-01"))).isTrue();
        assertThat(tosecContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC-PIX", "2019-01-01"))).isTrue();
    }

    @Test
    public void testCanEnrichGivenCategoryNotEqualsTOSECThenReturnsFalse() {
        assertThat(tosecContentMetadataEnricher.canEnrich(createContentMetadataSet(createName(), "NoIntro", "2019-01-01"))).isFalse();
    }

    @Test
    public void testEnrichGivenContentMetadataAndContentMetadataItemWhenItemHasLowercaseCrc32ThenItemCrc32IsChangedToUppercase() {
        var cm = tosecContentMetadataEnricher.enrichContentMetadata(createContentMetadata(null, "n", createContentMetadataItem("i", "somecontent".getBytes())), systemDefault);

        var contentMetadataItem = cm.getItems().iterator().next();

        assertThat(contentMetadataItem.getCrc()).isEqualTo(contentMetadataItem.getCrc().toUpperCase());
    }

    @Test
    public void testEnrichGivenContentMetadataAndContentMetadataItemWhenItemHasLowercaseMd5ThenItemMd5IsChangedToUppercase() {
        var cm = tosecContentMetadataEnricher.enrichContentMetadata(createContentMetadata(null, "n", createContentMetadataItem("i", "somecontent".getBytes())), systemDefault);

        var contentMetadataItem = cm.getItems().iterator().next();

        assertThat(contentMetadataItem.getMd5()).isEqualTo(contentMetadataItem.getMd5().toUpperCase());
    }

    @Test
    public void testEnrichGivenContentMetadataAndContentMetadataItemWhenItemHasLowercaseSha1ThenItemSha1IsChangedToUppercase() {
        var cm = tosecContentMetadataEnricher.enrichContentMetadata(createContentMetadata(null, "n", createContentMetadataItem("i", "somecontent".getBytes())), systemDefault);

        var contentMetadataItem = cm.getItems().iterator().next();

        assertThat(contentMetadataItem.getSha1()).isEqualTo(contentMetadataItem.getSha1().toUpperCase());
    }

    @Test
    public void testEnrichGivenNameStartWithBracketThenTitleContainsBracket() {
        var cm = createContentMetadata(null, "[D] #01 (19xx)(Dynamix)");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isFalse();
        assertThat(cm.getTitle()).isEqualTo("[D] #01");
        assertThat(cm.getDate()).isEqualTo("19xx");
        assertThat(cm.getPublisher()).isEqualTo("Dynamix");
    }

    @Test
    public void testEnrichGivenNameStartWithParenthesesThenTitleContainsParentheses() {
        var cm = createContentMetadata(null, "(B)arty-Demo (1988-08-25)(Z-Circle)");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isFalse();
        assertThat(cm.getTitle()).isEqualTo("(B)arty-Demo");
        assertThat(cm.getDate()).isEqualTo("1988-08-25");
        assertThat(cm.getPublisher()).isEqualTo("Z-Circle");
    }

    @Test
    public void testEnrichGivenNameUNKAndBracketsAndExtraTagsThenTitleContainsBracketsAndUnknownIsTrueAndTagsExtraAreSet() {
        var cm = createContentMetadata(null, "ZZZ-UNK-[compil4] Game_01a [o]");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isTrue();
        assertThat(cm.getTitle()).isEqualTo("[compil4] Game_01a");
        assertThat(cm.getTagsExtra()).contains("o");
    }

    @Test
    public void testEnrichGivenNameUNKAndBracketsThenTitleContainsBracketsAndUnknownIsTrue() {
        var cm = createContentMetadata(null, "ZZZ-UNK-[compil4] Lock_n_chase+");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isTrue();
        assertThat(cm.getTitle()).isEqualTo("[compil4] Lock_n_chase+");
    }

    @Test
    public void testEnrichGivenNameUNKWithBracketsThenTitleContainsBracketsAndUnknownIsTrue() {
        var cm = createContentMetadata(null, "ZZZ-UNK-[compil4] Frogger - Vindicator - One On One (disk 2 Of 2)");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isTrue();
        assertThat(cm.getTitle()).isEqualTo("[compil4] Frogger - Vindicator - One On One");
        assertThat(cm.getMediaType()).isEqualTo(ContentMediaType.DISK);
        assertThat(cm.getMediaOrder()).isEqualTo("2 Of 2");
    }

    @Test
    public void testEnrichGivenNameUNKWithNoMediaTypeThenMediaTypeIsUnknow() {
        var cm = createContentMetadata(null, "ZZZ-UNK-[compil3] Night & Stalker+ [o]");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getMediaType()).isEqualTo(ContentMediaType.UNKNOWN);
    }

    @Test
    public void testEnrichGivenNameUNKWithParenthesesExtraAndMediaInfoThenMediaInfoIsEnriched() {
        var cm = createContentMetadata(null, "ZZZ-UNK-[compil3] Mcs Pinball (1&3) (disk 1 Of 2)");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getMediaType()).isEqualTo(ContentMediaType.DISK);
        assertThat(cm.getMediaOrder()).isEqualTo("1 Of 2");
    }

    @Test
    public void testEnrichGivenNameWithDateThenNameAndUnknownIsFalseAndTitleIsSetAndDateIsSet() {
        var cm = createContentMetadata(null, "Somecontent (1234)");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isFalse();
        assertThat(cm.getTitle()).isEqualTo("Somecontent");
        assertThat(cm.getDate()).isEqualTo("1234");
    }

    @Test
    public void testEnrichGivenNameWithDemoParenthesesThenTitleContainsDemoParentheses() {
        var cm = createContentMetadata(null, "Legend of TOSEC, The (demo) (1986)(Devstudio)");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isFalse();
        assertThat(cm.getTitle()).isEqualTo("Legend of TOSEC, The (demo)");
        assertThat(cm.getDate()).isEqualTo("1986");
        assertThat(cm.getPublisher()).isEqualTo("Devstudio");
    }

    @Test
    public void testEnrichGivenNameWithLanguageThenLanguageIsSet() {
        var cm = createContentMetadata(null, "Legend of TOSEC, The (1986)(Devstudio)(en)");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isFalse();
        assertThat(cm.getTitle()).isEqualTo("Legend of TOSEC, The");
        assertThat(cm.getDate()).isEqualTo("1986");
        assertThat(cm.getPublisher()).isEqualTo("Devstudio");
        assertThat(cm.getLanguage()).isEqualTo("en");
    }

    @Test
    public void testEnrichGivenNameWithNoDateThenDateIsUnknown() {
        var cm = createContentMetadata(null, "Somecontent");
        cm.setDate(null);
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getDate()).isNull();
    }

    @Test
    public void testEnrichGivenNameWithNoMediaTypeThenMediaTypeIsUnknow() {
        var cm = createContentMetadata(null, "Somecontent");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getMediaType()).isEqualTo(ContentMediaType.UNKNOWN);
    }

    @Test
    public void testEnrichGivenNameWithNoPublisherThenPublisherIsUnknown() {
        var cm = createContentMetadata(null, "Somecontent");
        cm.setPublisher(null);

        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getPublisher()).isNull();
    }

    @Test
    public void testEnrichGivenNameWithRegionThenRegionIsSet() {
        var cm = createContentMetadata(null, "Legend of TOSEC, The (1986)(Devstudio)(US)");
        tosecContentMetadataEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.isUnidentified()).isFalse();
        assertThat(cm.getTitle()).isEqualTo("Legend of TOSEC, The");
        assertThat(cm.getDate()).isEqualTo("1986");
        assertThat(cm.getPublisher()).isEqualTo("Devstudio");
        assertThat(cm.getRegion()).isEqualTo("US");
    }
}
