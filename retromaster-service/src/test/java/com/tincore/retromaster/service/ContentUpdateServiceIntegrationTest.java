package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.service.archive.producer.BytesArchiveEntryProducer;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ContentUpdateServiceIntegrationTest extends AbstractContextIntegrationTest {

    public static final String ENTRY_NOT_KNOWN_ANYMORE = "notrecognized_but_in_repo.txt";

    public final byte[] contentMetadataItemAa1Payload = RandomStringUtils.randomAlphabetic(20).getBytes();
    public final byte[] contentMetadataItemAa2Payload = RandomStringUtils.randomAlphabetic(20).getBytes();
    public final byte[] unknownPayload1 = RandomStringUtils.randomAlphabetic(20).getBytes();

    private final String metadataTitle = "metadata_title" + RandomStringUtils.randomNumeric(5);
    private final String metadataSetName = "metadataSet" + RandomStringUtils.randomNumeric(5);

    @Autowired
    private ContentUpdateService contentUpdateService;

    private ContentVolume productionVolume;

    private ContentMetadataSet contentMetadataSetAOld;
    private ContentMetadataItem contentMetadataItemAa1Old;
    private ContentMetadataItem contentMetadataItemAa2Old;
    private ContentMetadata contentMetadataAaOld;

    private ContentMetadataItem contentMetadataItemAa1New;
    private ContentMetadataItem contentMetadataItemAa2New;
    private ContentMetadata contentMetadataAaNew;

    @BeforeEach
    public void setUp() throws Exception {
        productionVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data"), true);

        var contentMetadataItemAa1Name = "good1.txt";
        var contentMetadataItemAa2Name = "good2.txt";
        contentMetadataItemAa1Old = createContentMetadataItem(contentMetadataItemAa1Name, contentMetadataItemAa1Payload);
        contentMetadataItemAa2Old = createContentMetadataItem(contentMetadataItemAa2Name, contentMetadataItemAa2Payload);
        contentMetadataAaOld = createContentMetadata(null, metadataTitle, contentMetadataItemAa1Old, contentMetadataItemAa2Old);
        contentMetadataSetAOld = tFS.setUpPersistedContentMetadataSet(createContentMetadataSet(metadataSetName, "TOSEC", "1999-01-01", contentMetadataAaOld));

        contentMetadataItemAa1New = createContentMetadataItem(contentMetadataItemAa1Name, contentMetadataItemAa1Payload);
        contentMetadataItemAa2New = createContentMetadataItem(contentMetadataItemAa2Name, contentMetadataItemAa2Payload);
        contentMetadataAaNew = createContentMetadata(null, metadataTitle, contentMetadataItemAa1New, contentMetadataItemAa2New);
        tFS.setUpPersistedContentMetadataSet(createContentMetadataSet(metadataSetName, "TOSEC", "2020-01-01", contentMetadataAaNew));
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadataSetAndFilesInOldPathAndNoContentsWhenExecutedThenRecognizedFilesMovedToUpdatePathAndUnrecognizedFilesMovedToUnknown() throws Exception {
        var filePathOld = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaOld),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1Old.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));

        tFS.getContentMetadataSetRepositoryService().delete(contentMetadataSetAOld);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew), contentMetadataItemAa1New.getName(), contentMetadataItemAa1Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1New);

        assertThat(filePathOld).doesNotExist();
        assertThatContentArchivedNotExists(productionVolume, contentMetadataAaOld);
        assertThatArchiveFileContentEquals(toUnknownContentPath(filePathOld), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadataSetWithUpdateAndFilesInOldPathAndFileInNewPathWithNotRecognizedContentWhenExecutedThenRecognizedFilesMovedToUpdatePathAndUnrecognizedFilesMovedToUnknown()
        throws Exception {
        var filePathOld = tFS.setUpFileArchived(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaOld), new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1Old.getName()));
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa1Old.getName(), false, contentMetadataItemAa1Payload);

        var filePathNew = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2New.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));
        tFS.setUpPersistedContent(productionVolume, filePathNew, contentMetadataItemAa2New.getName(), false, contentMetadataItemAa2Payload);
        tFS.setUpPersistedContent(productionVolume, filePathNew, ENTRY_NOT_KNOWN_ANYMORE, false, unknownPayload1);

        tFS.getContentMetadataSetRepositoryService().delete(contentMetadataSetAOld);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa1New.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa2New.getName(), contentMetadataItemAa2Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1New);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2New);

        assertThat(filePathOld).doesNotExist();
        assertThatContentArchivedNotExists(productionVolume, contentMetadataAaOld);

        assertThatArchiveFileContentEquals(toUnknownContentPath(filePathNew), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadataSetWithUpdateAndFilesInOldPathAndNewPathAndUnknownFilesInOldAndNewWhenExecutedThenRecognizedFilesMovedToUpdatePathAndUnrecognizedFilesMovedToUnknown() throws Exception {
        var filePathOld = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaOld),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1Old.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa1Old.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePathOld, ENTRY_NOT_KNOWN_ANYMORE, false, unknownPayload1);

        var filePathNew = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2New.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));
        tFS.setUpPersistedContent(productionVolume, filePathNew, contentMetadataItemAa2New.getName(), false, contentMetadataItemAa2Payload);
        tFS.setUpPersistedContent(productionVolume, filePathNew, ENTRY_NOT_KNOWN_ANYMORE, false, unknownPayload1);

        tFS.getContentMetadataSetRepositoryService().delete(contentMetadataSetAOld);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa1New.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa2New.getName(), contentMetadataItemAa2Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1New);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2New);

        assertThat(filePathOld).doesNotExist();
        assertThatContentArchivedNotExists(productionVolume, contentMetadataAaOld);

        assertThatArchiveFileContentEquals(toUnknownContentPath(filePathOld), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
        assertThatArchiveFileContentEquals(toUnknownContentPath(filePathNew), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadataSetWithUpdateAndFilesInOldPathAndNewPathWhenExecutedThenRecognizedFilesMovedToUpdatePathAndUnrecognizedFilesMovedToUnknown() throws Exception {
        var filePathOld = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaOld),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1Old.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa1Old.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePathOld, ENTRY_NOT_KNOWN_ANYMORE, false, unknownPayload1);

        var filePathNew = tFS.setUpFileArchived(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew), new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2New.getName()));
        tFS.setUpPersistedContent(productionVolume, filePathNew, contentMetadataItemAa2New.getName(), false, contentMetadataItemAa2Payload);

        tFS.getContentMetadataSetRepositoryService().delete(contentMetadataSetAOld);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa1New.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa2New.getName(), contentMetadataItemAa2Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1New);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2New);

        assertThat(filePathOld).doesNotExist();
        assertThatContentArchivedNotExists(productionVolume, contentMetadataAaOld);

        assertThatArchiveFileContentEquals(toUnknownContentPath(filePathOld), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadataSetWithUpdateAndFilesInOldPathWhenExecutedThenRecognizedFilesMovedToUpdatePathAndUnrecognizedFilesMovedToUnknown() throws Exception {
        var filePathOld = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaOld),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1Old.getName()),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2Old.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa1Old.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa2Old.getName(), false, contentMetadataItemAa2Payload);
        tFS.setUpPersistedContent(productionVolume, filePathOld, ENTRY_NOT_KNOWN_ANYMORE, false, unknownPayload1);

        tFS.getContentMetadataSetRepositoryService().delete(contentMetadataSetAOld);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew), contentMetadataItemAa1New.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew), contentMetadataItemAa2New.getName(), contentMetadataItemAa2Payload);

        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1New);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2New);
        assertThat(filePathOld).doesNotExist();
        assertThatContentArchivedNotExists(productionVolume, contentMetadataAaOld);

        assertThatArchiveFileContentEquals(toUnknownContentPath(filePathOld), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadataSetWithUpdateAndFilesInOldPathWithFullContentAndContntsInNewPathButFilesMissingWhenExecutedThenRecognizedFilesMovedToUpdatePath() throws Exception {
        var filePathOld = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaOld),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1Old.getName()),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2Old.getName()));
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa1Old.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa2Old.getName(), false, contentMetadataItemAa2Payload);

        var filePathNew = tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew);
        tFS.setUpPersistedContent(productionVolume, filePathNew, contentMetadataItemAa1New.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePathNew, contentMetadataItemAa2New.getName(), false, contentMetadataItemAa2Payload);

        tFS.getContentMetadataSetRepositoryService().delete(contentMetadataSetAOld);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa1New.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa2New.getName(), contentMetadataItemAa2Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1New);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2New);

        assertThat(filePathOld).doesNotExist();
        assertThatContentArchivedNotExists(productionVolume, contentMetadataAaOld);
    }

    @Test
    public void
        testUpdateContentsByContentVolumeGivenContentMetadataSetWithUpdateAndFilesInOldPathWithFullContentAndFilesInNewPathWithPartialContentAndInvalidFilesWhenExecutedThenRecognizedFilesMovedToUpdatePathAndInvalidFilesMovedToUnkownPath()
            throws Exception {
        var filePathOld = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaOld),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1Old.getName()),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2Old.getName()));
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa1Old.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa2Old.getName(), false, contentMetadataItemAa2Payload);

        var filePathNew = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2New.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));
        tFS.setUpPersistedContent(productionVolume, filePathNew, contentMetadataItemAa2New.getName(), false, contentMetadataItemAa2Payload);
        tFS.setUpPersistedContent(productionVolume, filePathNew, ENTRY_NOT_KNOWN_ANYMORE, false, unknownPayload1);

        tFS.getContentMetadataSetRepositoryService().delete(contentMetadataSetAOld);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa1New.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa2New.getName(), contentMetadataItemAa2Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1New);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2New);

        assertThat(filePathOld).doesNotExist();
        assertThatContentArchivedNotExists(productionVolume, contentMetadataAaOld);

        assertThatArchiveFileContentEquals(toUnknownContentPath(filePathNew), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadataSetWithUpdateAndFilesInOldPathWithFullContentAndFilesInNewPathWithPartialContentWhenExecutedThenRecognizedFilesMovedToUpdatePath() throws Exception {
        var filePathOld = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaOld),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1Old.getName()),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2Old.getName()));
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa1Old.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePathOld, contentMetadataItemAa2Old.getName(), false, contentMetadataItemAa2Payload);

        var filePathNew = tFS.setUpFileArchived(tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAaNew), new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2New.getName()));
        tFS.setUpPersistedContent(productionVolume, filePathNew, contentMetadataItemAa2New.getName(), false, contentMetadataItemAa2Payload);

        tFS.getContentMetadataSetRepositoryService().delete(contentMetadataSetAOld);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa1New.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(filePathNew, contentMetadataItemAa2New.getName(), contentMetadataItemAa2Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1New);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2New);

        assertThat(filePathOld).doesNotExist();
        assertThatContentArchivedNotExists(productionVolume, contentMetadataAaOld);
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenEmptyVolumeThenExecutesSuccessfully() throws Exception {
        contentUpdateService.updateContentsByContentVolume(productionVolume);
    }

    public Path toUnknownContentPath(Path productionFilePath) {
        return relocateTo(tFS.getUnknownContentPath(), productionVolume.toPath(), productionFilePath);
    }
}
