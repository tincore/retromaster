package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.service.archive.ArchiverFormat;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryProducer;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static com.tincore.retromaster.service.archive.ArchiverFormat.*;

import static com.tincore.util.lang.ThrowingFunction.uFunction;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ArchiveServiceIntegrationTest extends AbstractContextIntegrationTest {

    @TempDir
    public Path testDir;

    private Path rootPath;
    private Path tmpPath;
    private Path testPath;

    @Autowired
    private ArchiveService archiveService;

    public void assertArchiveContains(Path archivePath, List<Path> filePaths) throws IOException {
        var entries = archiveService.extract(ArchiverFormat.byPath(archivePath).get(), Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);
        var expectedEntryNames = filePaths.stream().map(p -> tmpPath.relativize(p).toString()).toArray(String[]::new);
        assertThat(entries.stream().map(uFunction(ArchiveEntryProducer::getName))).containsExactlyInAnyOrder(expectedEntryNames);
    }

    public Path createTargetFilePath(ArchiverFormat archiverFormat) throws IOException {
        var targetFilePath = testPath.resolve("test." + archiverFormat.getFileExtension());
        Files.createDirectories(targetFilePath.getParent());
        return targetFilePath;
    }

    @BeforeEach
    public void setUp() {
        rootPath = testDir;
        tmpPath = rootPath.resolve("tmp");
        testPath = rootPath.resolve("test");
    }

    @Test
    public void testArchiveGiven7zAndDirectoryThenDirectoryCompressedWithContents() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"), createFilename("txt"));

        var targetFilePath = archiveService.archive(createTargetFilePath(SEVEN_ZIP), tmpPath);

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testArchiveGiven7zAndFileThenFileCompressed() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"));

        var targetFilePath = archiveService.archive(createTargetFilePath(SEVEN_ZIP), filePaths.get(0));

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testArchiveGivenTarAndDirectoryThenDirectoryCompressedWithContents() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"), createFilename("txt"));
        var targetFilePath = createTargetFilePath(TAR);

        archiveService.archive(targetFilePath, tmpPath);

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testArchiveGivenTarAndFileThenFileCompressed() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"));
        var targetFilePath = createTargetFilePath(TAR);

        archiveService.archive(targetFilePath, filePaths.get(0));

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testArchiveGivenTarLzmaAndDirectoryThenDirectoryCompressedWithContents() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"), createFilename("txt"));
        var targetFilePath = createTargetFilePath(TAR_LZMA);

        archiveService.archive(targetFilePath, tmpPath);

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testArchiveGivenTarLzmaAndFileThenFileCompressed() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"));
        var targetFilePath = createTargetFilePath(TAR_LZMA);

        archiveService.archive(targetFilePath, filePaths.get(0));

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testArchiveGivenTarXZAndDirectoryThenDirectoryCompressedWithContents() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"), createFilename("txt"));

        var targetFilePath = archiveService.archive(createTargetFilePath(TAR_XZ), tmpPath);

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testArchiveGivenZipAndDirectoryThenDirectoryCompressedWithContents() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"), createFilename("txt"));
        var targetFilePath = createTargetFilePath(ZIP);

        archiveService.archive(targetFilePath, tmpPath);

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testArchiveGivenZipAndFileThenFileCompressed() throws Exception {
        var filePaths = tFS.createFiles(tmpPath, createFilename("txt"));
        var targetFilePath = createTargetFilePath(ZIP);

        archiveService.archive(targetFilePath, filePaths.get(0));

        assertArchiveContains(targetFilePath, filePaths);
    }

    @Test
    public void testExtractGiven7zFileThenFileUncompressed() throws Exception {
        var archivePath = tFS.setUpFilesCompressed(SEVEN_ZIP, tmpPath);

        var archiveEntries = archiveService.extract(SEVEN_ZIP, Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);

        assertThat(archiveEntries).hasSize(1);
    }

    @Test
    public void testExtractGiven7zFileWithDirectoriesThenDirectoriesUncompressed() throws Exception {
        var archivePath = tFS.setUpFilesCompressedWithDirectories(SEVEN_ZIP, tmpPath);

        var archiveEntries = archiveService.extract(SEVEN_ZIP, Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);

        assertThat(archiveEntries).hasSize(1);
    }

    @Test
    public void testExtractGivenTarFileThenFileUncompressed() throws Exception {
        var archivePath = tFS.setUpFilesCompressed(TAR, tmpPath);

        var archiveEntries = archiveService.extract(TAR, Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);

        assertThat(archiveEntries).hasSize(1);
    }

    @Test
    public void testExtractGivenTarFileWithDirectoriesThenDirectoriesUncompressed() throws Exception {
        var archivePath = tFS.setUpFilesCompressedWithDirectories(TAR, tmpPath);

        var archiveEntries = archiveService.extract(TAR, Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);

        assertThat(archiveEntries).hasSize(1);
    }

    @Test
    public void testExtractGivenTarLzmaFileThenFileUncompressed() throws Exception {
        var archivePath = tFS.setUpFilesCompressed(TAR_LZMA, tmpPath);

        var archiveEntries = archiveService.extract(TAR_LZMA, Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);

        assertThat(archiveEntries).hasSize(1);
    }

    @Test
    public void testExtractGivenTarLzmaFileWithDirectoriesThenDirectoriesUncompressed() throws Exception {
        var archivePath = tFS.setUpFilesCompressedWithDirectories(TAR_LZMA, tmpPath);

        var archiveEntries = archiveService.extract(TAR_LZMA, Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);

        assertThat(archiveEntries).hasSize(1);
    }

    @Test
    public void testExtractGivenZipFileThenFileUncompressed() throws Exception {
        var archivePath = tFS.setUpFilesCompressed(ZIP, tmpPath);

        var archiveEntries = archiveService.extract(ZIP, Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);

        assertThat(archiveEntries).hasSize(1);
    }

    @Test
    public void testExtractGivenZipFileWithDirectoriesThenDirectoriesUncompressed() throws Exception {
        var archivePath = tFS.setUpFilesCompressedWithDirectories(ZIP, tmpPath);

        var archiveEntries = archiveService.extract(ZIP, Files.newByteChannel(archivePath), getLastModifiedTime(archivePath), e -> e);

        assertThat(archiveEntries).hasSize(1);
    }
}
