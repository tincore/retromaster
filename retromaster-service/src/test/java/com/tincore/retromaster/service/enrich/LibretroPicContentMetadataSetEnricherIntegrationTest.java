package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import com.tincore.util.StreamTrait;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class LibretroPicContentMetadataSetEnricherIntegrationTest extends AbstractContextIntegrationTest implements StreamTrait {

    @Autowired
    private LibretroPicContentMetadataSetEnricher libretroPicContentMetadataSetEnricher;

    @Test
    public void testCanEnrichGivenCategoryEqualsTOSECThenReturnsFalse() {
        assertThat(libretroPicContentMetadataSetEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC", "2019-01-01"))).isFalse();
        assertThat(libretroPicContentMetadataSetEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC-ISO", "2019-01-01"))).isFalse();
        assertThat(libretroPicContentMetadataSetEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC-PIX", "2019-01-01"))).isFalse();
    }

    @Test
    public void testCanEnrichGivenPathEndsWithLibRetroPicThenReturnsTrue() {
        var contentMetadataSet = createContentMetadataSet(createName(), "LIBRETRO_PIC", "");
        assertThat(libretroPicContentMetadataSetEnricher.canEnrich(contentMetadataSet)).isTrue();
    }

}
