package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import static com.tincore.retromaster.domain.ContentMetadata.*;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ContentSignatureChangeServiceIntegrationTest extends AbstractContextIntegrationTest {

    public static final PageRequest PAGE_REQUEST = PageRequest.of(0, 10);

    private ContentVolume productionVolume;

    private ContentMetadataSet contentMetadataSet;
    private ContentMetadataItem contentMetadataItem1;
    private ContentMetadataItem contentMetadataItem2;
    private ContentMetadata contentMetadata;

    @Autowired
    private ContentSignatureChangeService contentSignatureChangeService;

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();

        productionVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data"), true);

        contentMetadataItem1 = createContentMetadataItem("recognized.ext", CONTENT_PAYLOAD_1);
        contentMetadataItem2 = createContentMetadataItem("recognized2.ext", CONTENT_PAYLOAD_2);
        contentMetadata = createContentMetadata(null, "cm_1", contentMetadataItem1, contentMetadataItem2);

        contentMetadataSet = tFS.setUpPersistedContentMetadataSet(createContentMetadataSet("metadataset", "TOSEC", "2019-01-01", contentMetadata));
    }

    @Test
    public void testReindexContentSignatureChangesGivenContentMetadataAndContentMetadataItemWithContentAndContentMetadataItemWithNoContentThenContentMetadataCompleteIsCompletePartial() {
        tFS.setUpPersistedContent(productionVolume, "somePath/" + tFS.createFilename("txt"), false, CONTENT_PAYLOAD_1);
        contentSignatureChangeService.reindexContentSignatureChanges();

        var queryResult = tFS.getContentMetadataSearchService().searchByMetadataSetIdAndComplete(contentMetadataSet.getId(), COMPLETE_PARTIAL, PAGE_REQUEST);
        assertThat(queryResult).singleElement().satisfies(cm -> assertThat(cm.getId()).isEqualTo(contentMetadata.getId()));
    }

    @Test
    public void testReindexContentSignatureChangesGivenContentMetadataAndContentMetadataItemWithContentWhenContentIsRemovedThenContentMetadataCompleteIsEmpty() {
        var content = tFS.setUpPersistedContent(productionVolume, "somePath/" + tFS.createFilename("txt"), false, CONTENT_PAYLOAD_1);
        tFS.deleteContent(content);

        contentSignatureChangeService.reindexContentSignatureChanges();

        var queryResult = tFS.getContentMetadataSearchService().searchByMetadataSetIdAndComplete(contentMetadataSet.getId(), COMPLETE_EMPTY, PAGE_REQUEST);

        assertThat(queryResult).singleElement().satisfies(cm -> assertThat(cm.getId()).isEqualTo(contentMetadata.getId()));
    }

    @Test
    public void testReindexContentSignatureChangesGivenContentMetadataAndContentMetadataItemsWithContentThenContentMetadataCompleteIsComplete() {
        tFS.setUpPersistedContent(productionVolume, "somePath/" + tFS.createFilename("txt"), false, CONTENT_PAYLOAD_1);
        tFS.setUpPersistedContent(productionVolume, "somePath/" + tFS.createFilename("txt"), false, CONTENT_PAYLOAD_2);
        contentSignatureChangeService.reindexContentSignatureChanges();

        var queryResult = tFS.getContentMetadataSearchService().searchByMetadataSetIdAndComplete(contentMetadataSet.getId(), COMPLETE_TOTAL, PAGE_REQUEST);

        assertThat(queryResult).singleElement().satisfies(cm -> assertThat(cm.getId()).isEqualTo(contentMetadata.getId()));
    }

    @Test
    public void testReindexContentSignatureChangesGivenContentMetadataAndContentMetadataItemsWithNoContentThenContentMetadataCompleteIsCompleteEmpty() {
        tFS.getContentSignatureChangeRepositoryService().save(new ContentSignatureChange(contentMetadataItem1.getSignature()));
        contentSignatureChangeService.reindexContentSignatureChanges();

        var queryResult = tFS.getContentMetadataSearchService().searchByMetadataSetIdAndComplete(contentMetadataSet.getId(), COMPLETE_EMPTY, PAGE_REQUEST);

        assertThat(queryResult).singleElement().satisfies(cm -> assertThat(cm.getId()).isEqualTo(contentMetadata.getId()));
    }
}
