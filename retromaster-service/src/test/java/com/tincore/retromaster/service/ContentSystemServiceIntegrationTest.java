package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.ContentSystem;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.stream.Stream;

import static com.tincore.retromaster.domain.ContentSystem.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Slf4j
public class ContentSystemServiceIntegrationTest extends AbstractContextIntegrationTest {

    @Autowired
    private ContentSystemService contentSystemService;

    @Test
    public void testFindBySystemGivenActApricotLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("ACT Apricot")).hasValue(ACT_APRICOT);
        assertThat(contentSystemService.findBySystemLabel("ACT Apricot F1")).hasValue(ACT_APRICOT_F1);
        assertThat(contentSystemService.findBySystemLabel("ACT Apricot Portable")).hasValue(ACT_APRICOT_PORTABLE);
        assertThat(contentSystemService.findBySystemLabel("ACT Apricot PC-XI")).hasValue(ACT_APRICOT_PC_XI);
    }

    @Test
    public void testFindBySystemGivenAppleLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Apple 1")).hasValue(APPLE_1);
        assertThat(contentSystemService.findBySystemLabel("Apple I")).hasValue(APPLE_1);
        assertThat(contentSystemService.findBySystemLabel("Apple II")).hasValue(APPLE_2);
        assertThat(contentSystemService.findBySystemLabel("Apple III")).hasValue(APPLE_3);
        assertThat(contentSystemService.findBySystemLabel("Apple IIGS")).hasValue(APPLE_2GS);
    }

    @Test
    public void testFindBySystemGivenCommodoreThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Commodore Amiga")).hasValue(COMMODORE_AMIGA);
        assertThat(contentSystemService.findBySystemLabel("Commodore Amiga 1200")).hasValue(COMMODORE_AMIGA_1200);
        assertThat(contentSystemService.findBySystemLabel("Commodore Amiga CDTV")).hasValue(COMMODORE_CDTV);
        assertThat(contentSystemService.findBySystemLabel("Commodore CDTV")).hasValue(COMMODORE_CDTV);
        assertThat(contentSystemService.findBySystemLabel("CDTV")).hasValue(COMMODORE_CDTV);
        assertThat(contentSystemService.findBySystemLabel("Commodore Amiga CD32")).hasValue(COMMODORE_CD32);
        assertThat(contentSystemService.findBySystemLabel("Commodore CD32")).hasValue(COMMODORE_CD32);
    }

    @Test
    public void testFindBySystemGivenCybikoLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Cybiko Xtreme")).hasValue(CYBIKO_XTREME);
        assertThat(contentSystemService.findBySystemLabel("Cybiko - Xtreme")).hasValue(CYBIKO_XTREME);
        assertThat(contentSystemService.findBySystemLabel("Cybiko Cybiko")).hasValue(CYBIKO_CYBIKO);
        assertThat(contentSystemService.findBySystemLabel("Cybiko")).hasValue(CYBIKO);
    }

    @Test
    public void testFindBySystemGivenMastersystemThenReturnsSegaMasterSystem() {
        assertThat(contentSystemService.findBySystemLabel("Mastersystem")).hasValue(SEGA_MASTERSYSTEM);
        assertThat(contentSystemService.findBySystemLabel("Master System")).hasValue(SEGA_MASTERSYSTEM);
    }

    @Test
    public void testFindBySystemGivenMsxThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("MSX 1")).hasValue(MICROSOFT_MSX_1);
        assertThat(contentSystemService.findBySystemLabel("MSX 2")).hasValue(MICROSOFT_MSX_2);
        assertThat(contentSystemService.findBySystemLabel("MSX 2+")).hasValue(MICROSOFT_MSX_2P);
        assertThat(contentSystemService.findBySystemLabel("MSX Turbo R")).hasValue(MICROSOFT_MSX_TURBO_R);
    }

    @Test
    public void testFindBySystemGivenNecLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("NEC PC Engine TurboGrafx 16")).hasValue(NEC_TURBOGRAFX);
        assertThat(contentSystemService.findBySystemLabel("NEC PC Engine SuperGrafx")).hasValue(NEC_SUPERGRAFX);
        assertThat(contentSystemService.findBySystemLabel("NEC PC-FXGA")).hasValue(NEC_PCFXGA);
        assertThat(contentSystemService.findBySystemLabel("NEC PC-FX")).hasValue(NEC_PCFX);
        assertThat(contentSystemService.findBySystemLabel("NEC PC Engine CD")).hasValue(NEC_TURBOGRAFX_CD);
        assertThat(contentSystemService.findBySystemLabel("NEC TurboGrafx 16 CD")).hasValue(NEC_TURBOGRAFX_CD);
    }

    @Test
    public void testFindBySystemGivenNintendoLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Nintendo - Gameboy Advance")).hasValue(NINTENDO_GAMEBOY_ADVANCE);
        assertThat(contentSystemService.findBySystemLabel("Nintendo - Gameboy Color")).hasValue(NINTENDO_GAMEBOY_COLOR);
        assertThat(contentSystemService.findBySystemLabel("Nintendo Gameboy Color")).hasValue(NINTENDO_GAMEBOY_COLOR);
        assertThat(contentSystemService.findBySystemLabel("Nintendo - Gameboy")).hasValue(NINTENDO_GAMEBOY);
        assertThat(contentSystemService.findBySystemLabel("Nintendo Gameboy")).hasValue(NINTENDO_GAMEBOY);

        assertThat(contentSystemService.findBySystemLabel("Nintendo - Wii U")).hasValue(NINTENDO_WIIU);
        assertThat(contentSystemService.findBySystemLabel("Nintendo Wii U")).hasValue(NINTENDO_WIIU);
        assertThat(contentSystemService.findBySystemLabel("Nintendo WiiU")).hasValue(NINTENDO_WIIU);

        assertThat(contentSystemService.findBySystemLabel("Nintendo - Wii")).hasValue(NINTENDO_WII);
        assertThat(contentSystemService.findBySystemLabel("Nintendo Wii")).hasValue(NINTENDO_WII);

        assertThat(contentSystemService.findBySystemLabel("Nintendo 64DD")).hasValue(NINTENDO_64DD);

        assertThat(contentSystemService.findBySystemLabel("Nintendo 64")).hasValue(NINTENDO_64);

        assertThat(contentSystemService.findBySystemLabel("Nintendo DSi")).hasValue(NINTENDO_DSI);
        assertThat(contentSystemService.findBySystemLabel("Nintendo DS")).hasValue(NINTENDO_DS);

        assertThat(contentSystemService.findBySystemLabel("Nintendo FDS")).hasValue(NINTENDO_FAMILY_COMPUTER_DISK_SYSTEM);
        assertThat(contentSystemService.findBySystemLabel("Nintendo - Family Computer Disk System")).hasValue(NINTENDO_FAMILY_COMPUTER_DISK_SYSTEM);
        assertThat(contentSystemService.findBySystemLabel("Nintendo Family Disk System")).hasValue(NINTENDO_FAMILY_COMPUTER_DISK_SYSTEM);
    }

    @Test
    public void testFindBySystemGivenAmstradThenReturnsAmstrad() {
        assertThat(contentSystemService.findBySystemLabel("Amstrad CPC+")).hasValue(AMSTRAD_CPCP);
        assertThat(contentSystemService.findBySystemLabel("PC1640")).hasValue(AMSTRAD_PC1640);
        assertThat(contentSystemService.findBySystemLabel("Amstrad")).hasValue(AMSTRAD);
    }

    @Test
    public void testFindBySystemGivenRadio86RKLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Radio-86RK - Apogej BK-01")).hasValue(RADIO_86RK_APOGEJ_BK01);
        assertThat(contentSystemService.findBySystemLabel("Radio-86RK Apogej BK-01")).hasValue(RADIO_86RK_APOGEJ_BK01);
        assertThat(contentSystemService.findBySystemLabel("Radio-86RK")).hasValue(RADIO_86RK);
    }

    @Test
    public void testFindBySystemGivenSegaMegaDriveGenesisThenReturnsSegaMegadrive() {
        assertThat(contentSystemService.findBySystemLabel("Sega - Mega Drive - Genesis")).hasValue(SEGA_MEGADRIVE);
        assertThat(contentSystemService.findBySystemLabel("Genesis")).hasValue(SEGA_MEGADRIVE);
        assertThat(contentSystemService.findBySystemLabel("MegaDrive")).hasValue(SEGA_MEGADRIVE);
    }

    @Test
    public void testFindBySystemGivenSnkNeoGeoThenReturnsCorrectSystem() {
        assertThat(contentSystemService.findBySystemLabel("SNK NeoGeo")).hasValue(SNK_NEOGEO);
        assertThat(contentSystemService.findBySystemLabel("SNK Neo-Geo")).hasValue(SNK_NEOGEO);
        assertThat(contentSystemService.findBySystemLabel("SNK Neo Geo")).hasValue(SNK_NEOGEO);

        assertThat(contentSystemService.findBySystemLabel("SNK NeoGeo CD")).hasValue(SNK_NEOGEO_CD);
        assertThat(contentSystemService.findBySystemLabel("SNK Neo-Geo CD")).hasValue(SNK_NEOGEO_CD);
        assertThat(contentSystemService.findBySystemLabel("SNK Neo Geo CD")).hasValue(SNK_NEOGEO_CD);

        assertThat(contentSystemService.findBySystemLabel("SNK NeoGeo Pocket")).hasValue(SNK_NEOGEO_POCKET);
        assertThat(contentSystemService.findBySystemLabel("SNK Neo-Geo Pocket")).hasValue(SNK_NEOGEO_POCKET);
        assertThat(contentSystemService.findBySystemLabel("SNK Neo Geo Pocket")).hasValue(SNK_NEOGEO_POCKET);

        assertThat(contentSystemService.findBySystemLabel("SNK NeoGeo Pocket Color")).hasValue(SNK_NEOGEO_POCKET_COLOR);
        assertThat(contentSystemService.findBySystemLabel("SNK Neo-Geo Pocket Color")).hasValue(SNK_NEOGEO_POCKET_COLOR);
        assertThat(contentSystemService.findBySystemLabel("SNK Neo Geo Pocket Color")).hasValue(SNK_NEOGEO_POCKET_COLOR);
    }

    @Test
    public void testFindBySystemGivenSonyThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("PS1")).hasValue(SONY_PLAYSTATION);
        assertThat(contentSystemService.findBySystemLabel("PSX")).hasValue(SONY_PLAYSTATION);
        assertThat(contentSystemService.findBySystemLabel("PS2")).hasValue(SONY_PLAYSTATION_2);
        assertThat(contentSystemService.findBySystemLabel("PS3")).hasValue(SONY_PLAYSTATION_3);
        assertThat(contentSystemService.findBySystemLabel("PS4")).hasValue(SONY_PLAYSTATION_4);
        assertThat(contentSystemService.findBySystemLabel("PS5")).hasValue(SONY_PLAYSTATION_5);
        assertThat(contentSystemService.findBySystemLabel("PSP")).hasValue(SONY_PLAYSTATION_PORTABLE);
        assertThat(contentSystemService.findBySystemLabel("Sony News")).hasValue(SONY_NEWS);
        assertThat(contentSystemService.findBySystemLabel("Sony - News")).hasValue(SONY_NEWS);
        assertThat(contentSystemService.findBySystemLabel("SONY SMC-777")).hasValue(SONY_SMC777);
        assertThat(contentSystemService.findBySystemLabel("SONY")).hasValue(SONY);
    }

    @Test
    public void testFindBySystemGivenMagnavoxThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Magnavox Odyssey 2")).hasValue(MAGNAVOX_ODYSSEY_2);
    }


        @Test
    public void testFindBySystemGivenFujitsuThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Fujitsu")).hasValue(FUJITSU);


        assertThat(contentSystemService.findBySystemLabel("FUJITSU_FM77AV")).hasValue(FUJITSU_FM77AV);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM77-AV")).hasValue(FUJITSU_FM77AV);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM-77AV")).hasValue(FUJITSU_FM77AV);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM77AV")).hasValue(FUJITSU_FM77AV);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu - FM-77AV")).hasValue(FUJITSU_FM77AV);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu - FM77AV")).hasValue(FUJITSU_FM77AV);

        assertThat(contentSystemService.findBySystemLabel("FUJITSU_FM7")).hasValue(FUJITSU_FM7);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM-7")).hasValue(FUJITSU_FM7);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM7")).hasValue(FUJITSU_FM7);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu - FM-7")).hasValue(FUJITSU_FM7);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu - FM7")).hasValue(FUJITSU_FM7);

        assertThat(contentSystemService.findBySystemLabel("FUJITSU_FMTOWNS")).hasValue(FUJITSU_FMTOWNS);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM Towns")).hasValue(FUJITSU_FMTOWNS);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM-Towns")).hasValue(FUJITSU_FMTOWNS);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FMTowns")).hasValue(FUJITSU_FMTOWNS);

        assertThat(contentSystemService.findBySystemLabel("FUJITSU_FMR50")).hasValue(FUJITSU_FMR50);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM R-50")).hasValue(FUJITSU_FMR50);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FMR50")).hasValue(FUJITSU_FMR50);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM-R-50")).hasValue(FUJITSU_FMR50);
        assertThat(contentSystemService.findBySystemLabel("Fujitsu FM-R50")).hasValue(FUJITSU_FMR50);

        assertThat(contentSystemService.findBySystemLabel("Fujitsu Facom 9450")).hasValue(FUJITSU_FACOM_9450);
            assertThat(contentSystemService.findBySystemLabel("Facom 9450")).hasValue(FUJITSU_FACOM_9450);

        assertThat(contentSystemService.findBySystemLabel("FUJITSU_FACOM")).hasValue(FUJITSU_FACOM);
    }

    @Test
    public void testFindBySystemGivenThomsonThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Thomson")).hasValue(THOMSON);
        assertThat(contentSystemService.findBySystemLabel("Thomson - TO9")).hasValue(THOMSON_TO9);
        assertThat(contentSystemService.findBySystemLabel("Thomson - Thomson TO9")).hasValue(THOMSON_TO9);
        assertThat(contentSystemService.findBySystemLabel("Thomson - TO9+")).hasValue(THOMSON_TO9_PLUS);
        assertThat(contentSystemService.findBySystemLabel("Thomson - Thomson TO9+")).hasValue(THOMSON_TO9_PLUS);
    }

    @Test
    public void testFindBySystemGivenSystemLabelThenReturnsSystems() {
        Stream.of(ContentSystem.values()).forEach(c -> assertThat(contentSystemService.findBySystemLabel(c.toString())).hasValue(c));
        Stream.of(ContentSystem.values()).forEach(c -> assertThat(contentSystemService.findBySystemLabel(c.getLabel())).hasValue(c));
    }

    @Test
    public void testFindBySystemGivenVTechLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("VTech - V.Smile")).hasValue(VTECH_VSMILE);
        assertThat(contentSystemService.findBySystemLabel("VTech - Creativision")).hasValue(VTECH_CREATIVISION);
    }

    @Test
    public void testFindBySystemGivenWavemateLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("WaveMate Bullet")).hasValue(WAVEMATE_BULLET);
    }

    @Test
    public void testFindBySystemGivenXeroxLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Xerox 820 II")).hasValue(XEROX_820_II);
        assertThat(contentSystemService.findBySystemLabel("Xerox 820-II")).hasValue(XEROX_820_II);
        assertThat(contentSystemService.findBySystemLabel("Xerox")).hasValue(XEROX);
    }

    @Test
    public void testFindBySystemGivenZilogLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("Zilog MCZ-2")).hasValue(ZILOG_MCZ_2);
    }

    @Test
    public void testFindBySystemGivenZpaLabelsThenReturnsSystems() {
        assertThat(contentSystemService.findBySystemLabel("ZPA Novy Bor IQ-151")).hasValue(ZPA_NOVY_BOR_IQ_151);
    }
}