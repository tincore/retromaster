package com.tincore.retromaster.service.enrich;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import com.tincore.util.StreamTrait;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class NoIntroContentMetadataSetEnricherIntegrationTest extends AbstractContextIntegrationTest implements StreamTrait {

    @Autowired
    private NoIntroContentMetadataSetEnricher nointroContentMetadataSetEnricher;

    private String systemDefault = "someSystem";

    @Test
    public void testCanEnrichGivenCategoryEqualsTOSECThenReturnsFalse() {
        assertThat(nointroContentMetadataSetEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC", "2019-01-01"))).isFalse();
        assertThat(nointroContentMetadataSetEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC-ISO", "2019-01-01"))).isFalse();
        assertThat(nointroContentMetadataSetEnricher.canEnrich(createContentMetadataSet(createName(), "TOSEC-PIX", "2019-01-01"))).isFalse();
    }

    @Test
    public void testCanEnrichGivenUrlEqualsNoIntroThenReturnsTrue() {
        var contentMetadataSet = createContentMetadataSet(createName(), "", "2019-01-01");
        contentMetadataSet.setUrl("No-Intro");
        assertThat(nointroContentMetadataSetEnricher.canEnrich(contentMetadataSet)).isTrue();
    }

    @Test
    public void testEnrichGivenContentMetadataAndContentMetadataItemWhenItemHasLowercaseCrc32ThenItemCrc32IsChangedToUppercase() {
        var cm = nointroContentMetadataSetEnricher.enrichContentMetadata(createContentMetadata(null, "n", createContentMetadataItem("i", "somecontent".getBytes())), systemDefault);

        var contentMetadataItem = cm.getItems().iterator().next();

        assertThat(contentMetadataItem.getCrc()).isEqualTo(contentMetadataItem.getCrc().toUpperCase());
    }

    @Test
    public void testEnrichGivenContentMetadataAndContentMetadataItemWhenItemHasLowercaseMd5ThenItemMd5IsChangedToUppercase() {
        var cm = nointroContentMetadataSetEnricher.enrichContentMetadata(createContentMetadata(null, "n", createContentMetadataItem("i", "somecontent".getBytes())), systemDefault);

        var contentMetadataItem = cm.getItems().iterator().next();

        assertThat(contentMetadataItem.getMd5()).isEqualTo(contentMetadataItem.getMd5().toUpperCase());
    }

    @Test
    public void testEnrichGivenContentMetadataAndContentMetadataItemWhenItemHasLowercaseSha1ThenItemSha1IsChangedToUppercase() {
        var cm = nointroContentMetadataSetEnricher.enrichContentMetadata(createContentMetadata(null, "n", createContentMetadataItem("i", "somecontent".getBytes())), systemDefault);

        var contentMetadataItem = cm.getItems().iterator().next();

        assertThat(contentMetadataItem.getSha1()).isEqualTo(contentMetadataItem.getSha1().toUpperCase());
    }

    @Test
    public void testEnrichGivenNameAndContentWithItemWithSameNameThenRegionisItemRegion() {
        var cm = createContentMetadata(null, "NoIntro title", createContentMetadataItem("NoIntro title (USA).bin", "somedata".getBytes()));
        nointroContentMetadataSetEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getTitle()).isEqualTo("NoIntro title");
        assertThat(cm.getRegion()).isEqualTo("US");
    }

    @Test
    public void testEnrichGivenNameAndContentWithItemWithDiffernetNameThenTitleIsMetadataTitle() {
        var cm = createContentMetadata(null, "NoIntro title", createContentMetadataItem("SUBCONTENT (USA).bin", "somedata".getBytes()));
        nointroContentMetadataSetEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getTitle()).isEqualTo("NoIntro title");
    }

    @Test
    public void testEnrichGivenNameWithRegionUSAThenRegionContainsUS() {
        var cm = createContentMetadata(null, "NoIntro title (USA)");
        nointroContentMetadataSetEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getTitle()).isEqualTo("NoIntro title");
        assertThat(cm.getRegion()).isEqualTo("US");
    }

    @Test
    public void testEnrichGivenNameWithRegionsThenRegionsAreTranslated() {
        var cm = createContentMetadata(null, "NoIntro title (USA, Europe, Brazil, World)");
        nointroContentMetadataSetEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getTitle()).isEqualTo("NoIntro title");
        assertThat(cm.getRegion()).isEqualTo("US,EU,BR,WO");
    }

    @Test
    public void testEnrichGivenNameWithSomeStrangeRegionThenRegionContainsSomeStrangeRegion() {
        var cm = createContentMetadata(null, "NoIntro title (SomeStrangeRegion)");
        nointroContentMetadataSetEnricher.enrichContentMetadata(cm, systemDefault);

        assertThat(cm.getTitle()).isEqualTo("NoIntro title");
        assertThat(cm.getRegion()).isEqualTo("SomeStrangeRegion");
    }
}
