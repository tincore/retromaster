package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.service.archive.producer.BytesArchiveEntryProducer;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Path;

@Slf4j
public class ContentUpdateServiceFromUpdateProductionIntegrationTest extends AbstractContextIntegrationTest {

    public static final String ENTRY_NOT_KNOWN_ANYMORE = "notrecognized_but_in_repo.txt";

    public final byte[] contentMetadataItemAa1Payload = RandomStringUtils.randomAlphabetic(20).getBytes();
    public final byte[] contentMetadataItemAa2Payload = RandomStringUtils.randomAlphabetic(20).getBytes();
    public final byte[] unknownPayload1 = RandomStringUtils.randomAlphabetic(20).getBytes();

    private final String metadataTitle = "metadata_title" + RandomStringUtils.randomNumeric(5);
    private final String metadataSetName = "metadataSet" + RandomStringUtils.randomNumeric(5);

    @Autowired
    private ContentUpdateService contentUpdateService;

    private ContentVolume productionVolume;

    private ContentMetadataItem contentMetadataItemAa1;
    private ContentMetadataItem contentMetadataItemAa2;
    private ContentMetadata contentMetadataAa;

    @BeforeEach
    public void setUp() throws Exception {
        productionVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data" + RandomStringUtils.randomAlphanumeric(4)), false);
        contentMetadataItemAa1 = createContentMetadataItem("good1.txt", contentMetadataItemAa1Payload);
        contentMetadataItemAa2 = createContentMetadataItem("good2.txt", contentMetadataItemAa2Payload);
        contentMetadataAa = createContentMetadata(null, metadataTitle, contentMetadataItemAa1, contentMetadataItemAa2);
        tFS.setUpPersistedContentMetadataSet(createContentMetadataSet(metadataSetName, "TOSEC", "2020-01-01", contentMetadataAa));
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadatasAndContentsWhenExecutedThenRecognizedFilesInProductionPathAndUnrecognizedFilesMovedToUnknown() throws Exception {
        var filePath = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1.getName()),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));
        tFS.setUpPersistedContent(productionVolume, filePath, contentMetadataItemAa1.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePath, contentMetadataItemAa2.getName(), false, contentMetadataItemAa2Payload);
        var unknownContent = tFS.setUpPersistedContent(productionVolume, filePath, ENTRY_NOT_KNOWN_ANYMORE, false, unknownPayload1);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(filePath, contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(filePath, contentMetadataItemAa2.getName(), contentMetadataItemAa2Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2);
        assertThatContentNotExists(unknownContent);

        assertThatArchiveFileContentEquals(toUnknownContentPath(filePath), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
    }

    @Test
    public void testUpdateContentsByContentVolumeGivenContentMetadatasAndNotStoredUnknownContentWhenExecutedThenRecognizedFilesInProductionPathAndUnrecognizedFilesMovedToUnknown() throws Exception {
        var filePath = tFS.setUpFileArchived(
            tFS.toArchiveFileAbsolutePath(productionVolume, contentMetadataAa),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, contentMetadataItemAa1.getName()),
            new BytesArchiveEntryProducer(contentMetadataItemAa2Payload, contentMetadataItemAa2.getName()),
            new BytesArchiveEntryProducer(unknownPayload1, ENTRY_NOT_KNOWN_ANYMORE));
        tFS.setUpPersistedContent(productionVolume, filePath, contentMetadataItemAa1.getName(), false, contentMetadataItemAa1Payload);
        tFS.setUpPersistedContent(productionVolume, filePath, contentMetadataItemAa2.getName(), false, contentMetadataItemAa2Payload);
        tFS.setUpPersistedContent(productionVolume, filePath, ENTRY_NOT_KNOWN_ANYMORE, false, unknownPayload1);

        contentUpdateService.updateContentsByContentVolume(productionVolume);

        assertThatArchiveFileContentContains(filePath, contentMetadataItemAa1.getName(), contentMetadataItemAa1Payload);
        assertThatArchiveFileContentContains(filePath, contentMetadataItemAa2.getName(), contentMetadataItemAa2Payload);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa1);
        assertThatContentArchivedExists(productionVolume, contentMetadataItemAa2);

        assertThatArchiveFileContentEquals(toUnknownContentPath(filePath), ENTRY_NOT_KNOWN_ANYMORE, unknownPayload1);
    }

    public Path toUnknownContentPath(Path productionFilePath) {
        return relocateTo(tFS.getUnknownContentPath(), productionVolume.toPath(), productionFilePath);
    }
}
