package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import com.tincore.test.support.junit2bdd.BehaviourGroup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static com.tincore.retromaster.domain.ContentMetadataSource.SABRETOOLS_MERGED;
import static org.assertj.core.api.Assertions.assertThat;

@BehaviourGroup(scenario = "ContentMetadataSet")
public class ContentMetadataSetImportServiceSabretoolsIntegrationTest extends AbstractContextIntegrationTest {

    @Autowired
    public ContentMetadataSetImportService contentMetadataSetImportService;

    @Autowired
    public ContentMetadataSetService contentMetadataSetService;

    @Autowired
    public ContentMetadataVolumeService contentMetadataVolumeService;

    @Autowired
    public ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;

    @Test
    public void testImportByPathGivenExistingResourceAndValidThenSavesMetadata() throws IOException {
        var paths = copyToPaths("classpath:/test/dat/sabretools/test_sa_1.dat", contentMetadataVolumeService.getImportPath());
        contentMetadataSetImportService.doContentMetadataSetImportByDirectory(contentMetadataVolumeService.getImportPath());

        var contentMetadataSets = contentMetadataSetRepositoryService.findAll();

        assertThat(contentMetadataSets)
            .anySatisfy(
                s -> {
                    assertThat(s.getFilename()).isEqualTo("test_sa_1.dat");
                    assertThat(s.getName()).isEqualTo("Commodore - Plus-4 (merged)");
                    assertThat(s.getVersion()).isEqualTo("20160515140306");
                    assertThat(s.getAuthor()).isEqualTo("SabreTools");
                    assertThat(s.getCategory()).isEqualTo(SABRETOOLS_MERGED.name());
                    assertThat(s.getRepositoryPath()).isEqualTo("COMMODORE_C16_C116_PLUS_4 - 20160515140306 SABRETOOLS_MERGED");
                });

        assertThat(paths).allSatisfy(p -> assertThat(p).doesNotExist());

        assertThat(isEmpty(contentMetadataVolumeService.getProdPath())).isFalse();
    }
}
