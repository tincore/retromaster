package com.tincore.retromaster.service;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.domain.ContentVolumeEnvironment;
import com.tincore.retromaster.service.archive.producer.BytesArchiveEntryProducer;
import com.tincore.retromaster.support.AbstractContextIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@Slf4j
public class ContentUpdateServiceFromImportVolumeNoCompressionIntegrationTest extends AbstractContextIntegrationTest {

    private final byte[] contentMetadataItemAa1Payload = RandomStringUtils.randomAlphabetic(20).getBytes();
    private final byte[] contentMetadataItemAa2Payload = RandomStringUtils.randomAlphabetic(20).getBytes();
    private final byte[] unknownPayload = RandomStringUtils.randomAlphabetic(20).getBytes();

    @SpyBean
    private RetromasterConfiguration.ContentsConfiguration contentsConfigurationSpy;

    @Autowired
    private ContentUpdateService contentUpdateService;

    private ContentVolume importVolume;
    private ContentVolume productionVolume;

    private ContentMetadataItem contentMetadataItemAa1;
    private ContentMetadataItem contentMetadataItemAa2;
    private ContentMetadata contentMetadataAa;

    @BeforeEach
    public void setUp() throws Exception {
        importVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.IMPORT, homePath.resolve("data"), true);
        productionVolume = tFS.setUpPersistedContentVolume(ContentVolumeEnvironment.PROD, homePath.resolve("data"), true);

        contentMetadataItemAa1 = createContentMetadataItem("good1.txt", contentMetadataItemAa1Payload);
        contentMetadataItemAa2 = createContentMetadataItem("good2.txt", contentMetadataItemAa2Payload);
        contentMetadataAa = createContentMetadata(null, "cmA_tit_" + RandomStringUtils.randomAlphanumeric(4), contentMetadataItemAa1, contentMetadataItemAa2);
        tFS.setUpPersistedContentMetadataSet(createContentMetadataSet("cmsA_" + RandomStringUtils.randomNumeric(4), "TOSEC", "2019-01-01", contentMetadataAa));
        doReturn(true).when(contentsConfigurationSpy).isDisableArchiver();
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsArchiveAndArchiveContainsFileRecognizedAndArchiveContainsFileUnrecognizedThenFileStoredInRepositoryAndUnrecognizedFilesStoredInUnknown() throws Exception {
        var importFilePath = tFS.setUpFileArchived(
            importVolume.toPath().resolve(createFileNameZip()),
            new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, "blah/recognized_notrenamed.txt"),
            new BytesArchiveEntryProducer(unknownPayload, "notrecognizedever.txt"));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThatPlainFileContentEquals(tFS.toPlainFileAbsolutePath(productionVolume, contentMetadataItemAa1), contentMetadataItemAa1Payload);
        assertThatContentPlainExists(productionVolume, contentMetadataItemAa1);
        assertThatContentSignatureChangePlainExists(productionVolume, contentMetadataItemAa1);

        assertThatArchiveFileContentEquals(tFS.getUnknownContentPath().resolve(importFilePath.getFileName()), "notrecognizedever.txt", unknownPayload);
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsArchiveFileRecognizedAndCompressionDisabledThenFileStoredInRepository() throws Exception {
        var importFilePath = tFS.setUpFileArchived(importVolume.toPath().resolve(createFileNameZip()), new BytesArchiveEntryProducer(contentMetadataItemAa1Payload, "entry.txt"));

        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFilePath).doesNotExist();

        assertThatPlainFileContentEquals(tFS.toPlainFileAbsolutePath(productionVolume, contentMetadataItemAa1), contentMetadataItemAa1Payload);
        assertThatContentPlainExists(productionVolume, contentMetadataItemAa1);
        assertThatContentSignatureChangePlainExists(productionVolume, contentMetadataItemAa1);
    }

    @Test
    public void testDoContentImportGivenPathsIncomingContainsPlainFileRecognizedAndCompressionDisabledThenFileStoredInRepository() throws Exception {
        var importFile = tFS.createFile(importVolume.toPath(), createFileNameTxt(), contentMetadataItemAa1Payload);
        contentUpdateService.updateContentsFromImportVolume(importVolume);

        assertThat(importFile).doesNotExist();

        assertThatPlainFileContentEquals(tFS.toPlainFileAbsolutePath(productionVolume, contentMetadataItemAa1), contentMetadataItemAa1Payload);
        assertThatContentPlainExists(productionVolume, contentMetadataItemAa1);
        assertThatContentSignatureChangePlainExists(productionVolume, contentMetadataItemAa1);
    }
}
