package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.event.ContentExecutionEvent;
import com.tincore.retromaster.server.thrift.TFormMapper;
import com.tincore.retromaster.server.thrift.TFormMapperImpl;
import com.tincore.retromaster.thrift.form.TContentExecutionEvent;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TTransportException;
import org.junit.jupiter.api.Test;

import java.util.Base64;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class ThriftTest {

    private final String charset = "UTF-8";

    TFormMapper tFormMapper = new TFormMapperImpl();

    TBinaryProtocol.Factory tProtocolFactory = new TBinaryProtocol.Factory();
    TDeserializer tDeserializer = new TDeserializer(tProtocolFactory);
    TSerializer tSerializer = new TSerializer(tProtocolFactory);

    public ThriftTest() throws TTransportException {}

    @Test
    public void testSerializeGivenContextExecutionEventWhenSerializedThenIsDeserializableBack() throws TException {
        // ContentExecutionEvent event = new ContentExecutionEvent(UUID.randomUUID(),
        // "BABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABAB");
        var event = new ContentExecutionEvent(
            UUID.randomUUID(),
            "[INFO] Saving RAM type #0 to \"/Users/manuelgallego/.config/retromaster_local/emu/retroarch/saves/Final Fantasy II (1988-12-17)(Square)(JP)[h][tr en Toma - Alan Midas][mapper 2, based on Beta03-07-97].srm\".");
        // ContentExecutionEvent event = new ContentExecutionEvent(UUID.randomUUID(), "[INFO] Bla");
        var base1 = tFormMapper.toTContentExecutionEvent(event);

        // TContentExecutionEvent base1 = new
        // TContentExecutionEvent().setContentExecutionId(UUID.randomUUID().toString64()).setMessage("BABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABABBABABBABABABABABABAABAB");
        // String payload = tSerializer.toString64(base1, charset);
        var bytes1 = tSerializer.serialize(base1);
        var payloadEncoded = Base64.getEncoder().encodeToString(bytes1);

        // byte[] rev = payloadEncoded.getBytes(charset);
        // assertThat(rev).isEqualTo(bytes1);
        //
        // TContentExecutionEvent base = new TContentExecutionEvent();
        // tDeserializer.deserialize(base, bytes1);

        var baseS = new TContentExecutionEvent();
        var payloadDecoded = Base64.getDecoder().decode(payloadEncoded);
        tDeserializer.deserialize(baseS, payloadDecoded);

        assertThat(baseS.getMessage()).isEqualTo(event.getMessage());
    }
}
