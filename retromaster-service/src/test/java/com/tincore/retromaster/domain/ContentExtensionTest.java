package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.RetromasterTestFixtureTrait;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ContentExtensionTest implements RetromasterTestFixtureTrait {

    @Test
    public void testBinGivenExtensionThenIsFirmwareAndCart() {
        assertThat(ContentExtension.bin.isCart()).isTrue();
        assertThat(ContentExtension.bin.isFirm()).isTrue();

        assertThat(ContentExtension.bin.isDisc()).isFalse();
        assertThat(ContentExtension.bin.isDisk()).isFalse();
        assertThat(ContentExtension.bin.isTape()).isFalse();
        assertThat(ContentExtension.bin.isHd()).isFalse();
        assertThat(ContentExtension.bin.isQuick()).isFalse();
        assertThat(ContentExtension.bin.isSnap()).isFalse();
    }

    @Test
    public void testIsDiscGivenDiscExtensionThenReturnsTrue() {
        assertThat(ContentExtension.cue.isDisc()).isTrue();
        assertThat(ContentExtension.iso.isDisc()).isTrue();
        assertThat(ContentExtension.ciso.isDisc()).isTrue();
        assertThat(ContentExtension.nrg.isDisc()).isTrue();
    }

    @Test
    public void testIsDiscGivenNotDiscExtensionThenReturnsFalse() {
        assertThat(ContentExtension.tap.isDisc()).isFalse();
        assertThat(ContentExtension.bin.isDisc()).isFalse();
        assertThat(ContentExtension.prg.isDisc()).isFalse();
        assertThat(ContentExtension.dsk.isDisc()).isFalse();
    }
}
