package com.tincore.retromaster.domain;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.support.RetromasterTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class ContentTest implements RetromasterTestFixtureTrait {
    ContentVolume contentVolume = createContentVolume();

    @Test
    public void testSetSubpathGivenPathWithDirectoryAndEntryAndFileThenDirectoryPathIsFileDirectoryAndFileNameIsFileNameAndFileEntryIsEntry() {
        var content = new Content(contentVolume, "somepath/somesub/somefile.txt" + Content.COMPRESS_SEPARATOR + "someEntry.txt");
        assertThat(content.getDirectoryPath()).isEqualTo("somepath/somesub");
        assertThat(content.getFileName()).isEqualTo("somefile.txt");
        assertThat(content.getFileEntry()).isEqualTo("someEntry.txt");
    }

    @Test
    public void testSetSubpathGivenPathWithDirectoryAndEntryInDirectoryAndFileThenDirectoryPathIsFileDirectoryAndFileNameIsFileNameAndFileEntryIsEntry() {
        var content = new Content(contentVolume, "somepath/somesub/somefile.txt" + Content.COMPRESS_SEPARATOR + "somesub2/someEntry.txt");
        assertThat(content.getDirectoryPath()).isEqualTo("somepath/somesub");
        assertThat(content.getFileName()).isEqualTo("somefile.txt");
        assertThat(content.getFileEntry()).isEqualTo("somesub2/someEntry.txt");
    }

    @Test
    public void testSetSubpathGivenPathWithDirectoryAndFileThenDirectoryPathIsFileDirectoryAndFileNameIsFileNameAndFileEntryIsNull() {
        var content = new Content(contentVolume, "somepath/somesub/somefile.txt");
        assertThat(content.getDirectoryPath()).isEqualTo("somepath/somesub");
        assertThat(content.getFileName()).isEqualTo("somefile.txt");
        assertThat(content.getFileEntry()).isNull();
    }

    @Test
    public void testSetSubpathGivenPathWithNoDirectoryAndEntryThenDirectoryPathIsFileDirectoryAndFileNameIsFileNameAndFileEntryIsEntry() {
        var content = new Content(contentVolume, "somefile.txt" + Content.COMPRESS_SEPARATOR + "somesub2/someEntry.txt");
        assertThat(content.getDirectoryPath()).isEqualTo("");
        assertThat(content.getFileName()).isEqualTo("somefile.txt");
        assertThat(content.getFileEntry()).isEqualTo("somesub2/someEntry.txt");
    }

    @Test
    public void testSetSubpathGivenPathWithNoDirectoryAndNotCompressedThenGetPathContentRelativeReturnsSamePath() {
        var subpath = "somefile.txt";

        var content = new Content(contentVolume, subpath);

        assertThat(content.getPathContentRelative()).isEqualTo(Paths.get(subpath));
    }

    @Test
    public void testSetSubpathGivenPathWithNoDirectoryThenGetPathContentRelativeReturnsSamePath() {
        var subpath = "somefile.txt" + Content.COMPRESS_SEPARATOR + "somesub2/someEntry.txt";

        var content = new Content(contentVolume, subpath);

        assertThat(content.getPathContentRelative()).isEqualTo(Paths.get(subpath));
    }
}
