package com.tincore.retromaster.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.server.fuse.RetromasterFuseFsController;
import com.tincore.retromaster.service.fs.AvFsService;
import com.tincore.retromaster.service.fs.UnionFsService;
import com.tincore.retromaster.service.fs.WorkFsService;
import com.tincore.util.MapTrait;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.util.TestSocketUtils;

import java.io.IOException;
import java.nio.file.Files;

@Slf4j
public class RetromasterTestContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext>, MapTrait {

    @Override
    @SneakyThrows
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        var tempDirectoryPath = Files.createTempDirectory("retromaster_test");

        configurableApplicationContext.addApplicationListener(
            (ApplicationListener<ContextClosedEvent>) event -> {
                log.debug("contextClosed");

                configurableApplicationContext.getBean(RetromasterFuseFsController.class).preDestroy();
                configurableApplicationContext.getBean(AvFsService.class).preDestroy();
                configurableApplicationContext.getBean(UnionFsService.class).preDestroy();
                configurableApplicationContext.getBean(WorkFsService.class).preDestroy();

                try {
                    FileUtils.deleteDirectory(tempDirectoryPath.toFile());
                } catch (IOException e) {
                    log.error("failed trying to cleanup directory. path={}", tempDirectoryPath, e);
                }
            });

        var propertiesMap = toMap(
            "test.ftp.port",
            String.valueOf(TestSocketUtils.findAvailableTcpPort()),
            "test.root.path",
            tempDirectoryPath.toAbsolutePath().toString());

        var inlinedProperties = propertiesMap.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).toArray(String[]::new);
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(configurableApplicationContext, inlinedProperties);
    }
}
