package com.tincore.retromaster.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.Application;
import com.tincore.retromaster.config.RetromasterConfiguration;
import com.tincore.retromaster.domain.Content;
import com.tincore.retromaster.domain.ContentMetadata;
import com.tincore.retromaster.domain.ContentMetadataItem;
import com.tincore.retromaster.domain.ContentVolume;
import com.tincore.retromaster.service.ArchiveService;
import com.tincore.retromaster.service.archive.ArchiverFormat;
import com.tincore.test.support.TestFixtureTrait;
import com.tincore.test.support.junit2bdd.junit5.BehaviourTestExtension;
import com.tincore.util.FileSystemTrait;
import lombok.SneakyThrows;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tincore.retromaster.service.archive.ArchiverFormat.TAR_XZ;
import static com.tincore.test.support.TagConstants.INTEGRATION;
import static com.tincore.util.lang.ThrowingFunction.uFunction;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ExtendWith(BehaviourTestExtension.class)
@Tag(INTEGRATION)
@ContextConfiguration(
    initializers = RetromasterTestContextInitializer.class,
    classes = {Application.class})
@ActiveProfiles("test")
public abstract class AbstractContextIntegrationTest implements RetromasterTestFixtureTrait, FileSystemTrait, TestFixtureTrait {

    @Autowired
    public ResourcePatternResolver resourcePatternResolver;
    @Autowired
    public RetromasterTestFixtureService tFS;
    @Autowired
    public ArchiveService archiveService;

    @Autowired
    public RetromasterConfiguration retromasterConfiguration;

    @Value("${test.root.path}")
    protected Path testRootPath;

    @Value("${tincore.retromaster.homePath}")
    protected Path homePath;

    @Autowired
    private CacheManager cacheManager;

    @SneakyThrows
    public void assertThatArchiveFileContentContains(Path archiveFilePath, String entryName, byte[] expectedContentBytes) {
        assertThat(archiveFilePath).exists();
        var archiveFileContents = getArchiveFileContents(archiveFilePath);
        assertThatArchiveFileContentEquals(entryName, archiveFileContents, expectedContentBytes);
    }

    @SneakyThrows
    public void assertThatArchiveFileContentEquals(Path archiveFilePath, String entryName, byte[] expectedContentBytes) {
        assertThat(archiveFilePath).exists();
        var archiveFileContents = getArchiveFileContents(archiveFilePath);
        assertThat(archiveFileContents).hasSize(1);
        assertThatArchiveFileContentEquals(entryName, archiveFileContents, expectedContentBytes);
    }

    private void assertThatArchiveFileContentEquals(String expectedEntryName, List<Pair<String, byte[]>> archiveFileContents, byte[] expectedContentBytes) {
        assertThat(archiveFileContents.stream().map(Pair::getLeft)).contains(expectedEntryName);

        var entry = archiveFileContents.stream().filter(p -> p.getLeft().equals(expectedEntryName)).findFirst();
        assertThat(entry).isPresent().withFailMessage("Could not find expected entry name=" + expectedEntryName + " in " + archiveFileContents).hasValueSatisfying(e -> assertThat(e.getRight()).isEqualTo(expectedContentBytes));
    }

    public void assertThatContentArchivedExists(ContentVolume contentVolume, ContentMetadataItem contentMetadataItem) {
        assertThatContentArchivedExists(contentVolume, contentMetadataItem, contentMetadataItem.getName());
    }

    public void assertThatContentArchivedExists(ContentVolume contentVolume, ContentMetadataItem contentMetadataItem, String archivedFileEntryName) {
        assertThatContentArchivedExists(contentVolume, contentMetadataItem, TAR_XZ, archivedFileEntryName);
    }

    public void assertThatContentArchivedExists(ContentVolume contentVolume, ContentMetadataItem contentMetadataItem, ArchiverFormat archiverFormat, String archivedFileEntryName) {
        assertThatContentExists(contentVolume, contentMetadataItem.getContentMetadata().getContentMetadataSet().getRepositoryPath(), tFS.toArchiveFileName(contentMetadataItem.getContentMetadata(), archiverFormat), archivedFileEntryName);
    }

    public void assertThatContentArchivedNotExists(ContentVolume contentVolume, ContentMetadata contentMetadata) {
        assertThatContentArchivedNotExists(contentVolume, contentMetadata, TAR_XZ);
    }

    public void assertThatContentArchivedNotExists(ContentVolume contentVolume, ContentMetadata contentMetadata, ArchiverFormat archiverFormat) {
        assertThatContentNotExists(contentVolume, contentMetadata.getContentMetadataSet().getRepositoryPath(), tFS.toArchiveFileName(contentMetadata, archiverFormat));
    }

    public void assertThatContentExists(ContentVolume contentVolume, String expectedDirectoryPath, String expectedFileNameWithExtension) {
        assertThat(tFS.getContentRepositoryService().findByVolumeAndDirectoryPathAndFileName(contentVolume, expectedDirectoryPath, expectedFileNameWithExtension)).isNotEmpty();
    }

    public void assertThatContentExists(ContentVolume contentVolume, String expectedDirectoryPath, String expectedFileNameWithExtension, String fileEntryName) {
        assertThat(tFS.getContentRepositoryService().findByVolumeAndDirectoryPathAndFileNameAndFileEntry(contentVolume, expectedDirectoryPath, expectedFileNameWithExtension, fileEntryName))
            .hasValueSatisfying(c -> assertThat(c.getFileEntry()).isEqualTo(fileEntryName));
    }

    public void assertThatContentNotExists(ContentVolume productionVolume, String expectedDirectoryPath, String expectedFileNameWithExtension) {
        assertThat(tFS.getContentRepositoryService().findByVolumeAndDirectoryPathAndFileName(productionVolume, expectedDirectoryPath, expectedFileNameWithExtension)).isEmpty();
    }

    public void assertThatContentNotExists(Content content) {
        assertThat(tFS.getContentRepositoryService().findById(content.getId())).isEmpty();
    }

    public void assertThatContentPlainExists(ContentVolume contentVolume, ContentMetadataItem contentMetadataItem) {
        assertThatContentExists(contentVolume, tFS.toPlainFileDirectoryPath(contentMetadataItem), contentMetadataItem.getName());
    }

    public void assertThatContentSignatureChangeArchivedExists(ContentVolume contentVolume, ContentMetadataItem contentMetadataItem) {
        assertThatContentSignatureChangeArchivedExists(contentVolume, contentMetadataItem, TAR_XZ);
    }

    public void assertThatContentSignatureChangeArchivedExists(ContentVolume contentVolume, ContentMetadataItem contentMetadataItem, ArchiverFormat archiverFormat) {
        assertThatContentSignatureChangeArchivedExists(contentVolume, tFS.toArchiveFileName(contentMetadataItem.getContentMetadata(), archiverFormat), contentMetadataItem);
    }

    public void assertThatContentSignatureChangeArchivedExists(ContentVolume contentVolume, String fileName, ContentMetadataItem contentMetadataItem) {
        var content = tFS.getContentRepositoryService()
            .findByVolumeAndDirectoryPathAndFileNameAndFileEntry(contentVolume, contentMetadataItem.getContentMetadata().getContentMetadataSet().getRepositoryPath(), fileName, contentMetadataItem.getName())
            .get();
        var contentSignatureChanges = tFS.getContentSignatureChangeRepositoryService().findAll();
        assertThat(contentSignatureChanges).anySatisfy(csc -> assertThat(csc.getSignature()).isEqualTo(content.getSignature()));
    }

    public void assertThatContentSignatureChangeExists(ContentVolume contentVolume, String expectedDirectoryPath, String expectedFileNameWithExtension) {

        var content = tFS.getContentRepositoryService().findByVolumeAndDirectoryPathAndFileName(contentVolume, expectedDirectoryPath, expectedFileNameWithExtension).get(0);
        var contentSignatureChanges = tFS.getContentSignatureChangeRepositoryService().findAll();
        assertThat(contentSignatureChanges).anySatisfy(csc -> assertThat(csc.getSignature()).isEqualTo(content.getSignature()));
    }

    public void assertThatContentSignatureChangePlainExists(ContentVolume contentVolume, ContentMetadataItem contentMetadataItem) {
        assertThatContentSignatureChangeExists(contentVolume, tFS.toPlainFileDirectoryPath(contentMetadataItem), contentMetadataItem.getName());
    }

    public void assertThatPlainFileContentEquals(Path filePath, byte[] expectedContentBytes) {
        assertThat(filePath).exists().content().isEqualTo(new String(expectedContentBytes));
    }

    public List<Path> copyToPaths(String resourceLocationPattern, Path targetDirectory) throws IOException {
        return Stream.of(resourcePatternResolver.getResources(resourceLocationPattern))
            .map(
                uFunction(
                    r -> {
                        var path = r.getFile().toPath();
                        Files.createDirectories(targetDirectory);
                        return Files.copy(path, targetDirectory.resolve(path.getFileName()));
                    }))
            .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<Pair<String, byte[]>> getArchiveFileContents(Path archiveFilePath) {
        return archiveService.extract(archiveFilePath, uFunction(e -> Pair.of(e.getName(), IOUtils.toByteArray(e.getInputStream()))));
    }

    @BeforeEach
    public void setUp() throws Exception {
        tFS.clear();
    }

    @AfterEach
    public void tearDown() {
        tFS.clear();
        tFS.clear();
    }
}
