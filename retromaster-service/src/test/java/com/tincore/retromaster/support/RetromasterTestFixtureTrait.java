package com.tincore.retromaster.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.domain.event.ContentImportEvent;
import com.tincore.retromaster.domain.event.ContentImportEventType;
import com.tincore.test.support.TestFixtureTrait;
import com.tincore.util.StreamTrait;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import static com.tincore.retromaster.domain.ContentMetadata.COMPLETE_EMPTY;

public interface RetromasterTestFixtureTrait extends TestFixtureTrait, StreamTrait {
    DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/YYYY");

    String CONTENT_PAYLOAD_PREFIX = "content";

    byte[] CONTENT_PAYLOAD_1 = "content1".getBytes();

    String CONTENT_PAYLOAD_1_SHA1 = "105e7a844ac896f68e6f7dc0a9389d3e9be95abc";
    String CONTENT_PAYLOAD_1_MD5 = "7e55db001d319a94b0b713529a756623";
    String CONTENT_PAYLOAD_1_CRC32 = "2c2831cb";

    byte[] CONTENT_PAYLOAD_2 = "content2".getBytes();

    @SneakyThrows
    default Content createContent(ContentVolume contentVolume, String subpath, LocalDateTime localDateTime, byte[] bytes) {
        var content = new Content(contentVolume, subpath);
        content = content.toBuilder().enrichTime(localDateTime).modifiedTime(localDateTime).checkTime(localDateTime).build();
        content.setSignature(getCrc(bytes), getMd5(bytes), getSha1(bytes), bytes.length);
        return content;
    }

    default ContentImportEvent createContentImportEvent(ContentImportEventType type) {
        return ContentImportEvent.builder().type(type).errorCount(0).build();
    }

    default ContentMetadata createContentMetadata(ContentMetadataSet createContentMetadataSet, String name, ContentMetadataItem... items) {
        return createContentMetadata(createContentMetadataSet, name, COMPLETE_EMPTY, items);
    }

    default ContentMetadata createContentMetadata(ContentMetadataSet createContentMetadataSet, String name, String complete, ContentMetadataItem... items) {
        return createContentMetadata(createContentMetadataSet, name, complete, ContentMediaType.DISC, items);
    }

    default ContentMetadata createContentMetadata(ContentMetadataSet createContentMetadataSet, String name, String complete, ContentMediaType contentMediaType, ContentMetadataItem... items) {
        var contentMetadata = ContentMetadata.builder()
            .contentMetadataSet(createContentMetadataSet)
            .name(name)
            .date("1980")
            .description(name + "_des")
            .mediaType(contentMediaType)
            .publisher(name + "_pub")
            .system(name + "_sys")
            .title(name)
            .complete(complete)
            .items(toStream(items).collect(Collectors.toList()))
            .unidentified(false)
            .alternate(false)
            .badDump(false)
            .cracked(false)
            .fixed(false)
            .hacked(false)
            .modified(false)
            .trained(false)
            .translated(false)
            .verified(false)
            .pirated(false)
            .virus(false)
            .build();

        // Should not be needed with bytecode enhancement! but intellij is not using atm
        contentMetadata.getItems().stream().filter(i -> i.getContentMetadata() == null).forEach(i -> i.setContentMetadata(contentMetadata));

        return contentMetadata;
    }

    @SneakyThrows
    default ContentMetadataItem createContentMetadataItem(String name) {
        return createContentMetadataItem(name, name.getBytes());
    }

    @SneakyThrows
    default ContentMetadataItem createContentMetadataItem(String name, byte[] bytes) {
        var contentMetadataItem = ContentMetadataItem.builder().name(name).size(bytes.length).build();
        contentMetadataItem.setSignature(getCrc(bytes), getMd5(bytes), getSha1(bytes), bytes.length);
        return contentMetadataItem;
    }

    @SneakyThrows
    default ContentMetadataItem createContentMetadataItem(String name, Content matchingContent) {
        var contentMetadataItem = ContentMetadataItem.builder().name(name).size(matchingContent.getSize()).build();
        contentMetadataItem.setSignature(matchingContent.getCrc(), matchingContent.getMd5(), matchingContent.getSha1(), matchingContent.getSize());
        return contentMetadataItem;
    }

    default ContentMetadataSet createContentMetadataSet(String name, String category, String version, ContentMetadata... contentMetadatas) {
        var contentMetadataSet = ContentMetadataSet.builder()
            .author(createString("author"))
            .category(category)
            .description(createString("description"))
            .email(createString("email@"))
            .filename(name + "_file" + System.currentTimeMillis() + ".dat")
            .name(name)
            .url(createString("url"))
            .version(version)
            .repositoryPath(name + " " + version + " " + category)
            .staging(false).build();

        toStream(contentMetadatas).forEach(c -> c.setContentMetadataSet(contentMetadataSet));
        contentMetadataSet.setContentMetadatas(toStream(contentMetadatas).collect(Collectors.toList()));

        return contentMetadataSet;
    }

    default ContentMetadataSet createContentMetadataSet() {
        var name = createName();
        return createContentMetadataSetTosec("SYS_" + name.toUpperCase(), name);
    }

    default ContentMetadataSet createContentMetadataSetTosec(String system, String subName, ContentMetadata... contentMetadatas) {
        return createContentMetadataSet(system + " - " + subName, "TOSEC", "2019-01-01", contentMetadatas);
    }

    default String createContentPayload(int metadataId, int metadataItemId) {
        return CONTENT_PAYLOAD_PREFIX + metadataId + "_" + metadataItemId;
    }

    default ContentVolume createContentVolume(Path path, ContentVolumeEnvironment environment) {
        return new ContentVolume("repo", path, environment);
    }

    default ContentVolume createContentVolume() {
        return createContentVolume(Paths.get(""), ContentVolumeEnvironment.PROD);
    }

    @SneakyThrows
    default Path createFile(Path targetPath, byte[] bytes) {
        Files.createDirectories(targetPath.getParent());
        Files.write(targetPath, bytes, StandardOpenOption.CREATE);
        return targetPath;
    }

    default Path createFile(Path targetDirectoryPath, String fileName, byte[] bytes) {
        return createFile(targetDirectoryPath.resolve(fileName), bytes);
    }

    private String createFileName(String extension) {
        return "file" + RandomStringUtils.randomAlphanumeric(5) + "." + extension;
    }

    default String createFileNameTxt() {
        return createFileName("txt");
    }

    default String createFileNameZip() {
        return createFileName("zip");
    }

    default String createName() {
        return createString("name");
    }

    default String getCrc(byte[] bytes) {
        Checksum checksum = new CRC32();
        checksum.update(bytes, 0, bytes.length);
        return StringUtils.leftPad(Long.toHexString(checksum.getValue()).toUpperCase(), 8, "0");
    }

    default String getMd5(byte[] bytes) throws NoSuchAlgorithmException {
        return getMessageDigest(bytes, "MD5").toUpperCase();
    }

    default String getMessageDigest(byte[] bytes, String algorithm) throws NoSuchAlgorithmException {
        var messageDigest = MessageDigest.getInstance(algorithm);
        messageDigest.update(bytes);
        return Hex.encodeHexString(messageDigest.digest());
    }

    default String getSha1(byte[] bytes) throws NoSuchAlgorithmException {
        return getMessageDigest(bytes, "SHA1").toUpperCase();
    }

    default String toString(ZonedDateTime zonedDateTime) {
        return zonedDateTime.format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }
}
