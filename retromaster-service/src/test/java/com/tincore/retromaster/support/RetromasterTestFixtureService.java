package com.tincore.retromaster.support;

/*-
 * #%L
 * retromaster-service
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.retromaster.config.TransactionConfiguration;
import com.tincore.retromaster.domain.*;
import com.tincore.retromaster.service.*;
import com.tincore.retromaster.service.archive.ArchiverFormat;
import com.tincore.retromaster.service.archive.producer.ArchiveEntryProducer;
import com.tincore.retromaster.service.archive.producer.BytesArchiveEntryProducer;
import com.tincore.retromaster.service.transfer.OriginContentTransferService;
import com.tincore.util.FileSystemTrait;
import com.tincore.util.Partition;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.tincore.retromaster.domain.Content.COMPRESS_SEPARATOR;
import static com.tincore.retromaster.service.archive.ArchiverFormat.TAR_XZ;

@Slf4j
@Service
public class RetromasterTestFixtureService implements RetromasterTestFixtureTrait, FileSystemTrait {

    @Autowired
    public TransactionConfiguration.TransactionNewTemplate transactionRequiresNewTemplate;
    @Autowired
    private ArchiveService archiveService;
    @Autowired
    private ContentRepositoryService contentRepositoryService;
    @Autowired
    private ContentSignatureChangeRepositoryService contentSignatureChangeRepositoryService;
    @Autowired
    private ContentSignatureChangeService contentSignatureChangeService;
    @Autowired
    private ContentVolumeService contentVolumeService;
    @Autowired
    private ContentVolumeRepositoryService contentVolumeRepositoryService;
    @Autowired
    private ContentMetadataSetRepositoryService contentMetadataSetRepositoryService;
    @Autowired
    private ContentMetadataRepositoryService contentMetadataRepositoryService;
    @Autowired
    private ContentMetadataItemRepositoryService contentMetadataItemRepositoryService;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private EntityManagerFactory entityManagerFactory;
    @Autowired
    private ContentMetadataSetEnricherService contentMetadataSetEnricherService;
    @Autowired
    private CacheManager cacheManager;
    @Autowired
    private ContentMetadataSearchService contentMetadataSearchService;
    @Autowired
    private OriginContentTransferService originContentTransferService;

    public void clear() {
        // contentMetadataItemRepositoryService.deleteAll(); If modify Items will trigger search reindex of content metadata
        contentMetadataRepositoryService.deleteAll();
        contentMetadataSetRepositoryService.deleteAll();

        contentSignatureChangeRepositoryService.deleteAll();

        contentRepositoryService.deleteAll();

        contentVolumeRepositoryService.deleteAll();

        clearCache();
    }

    public void clearCache() {
        cacheManager.getCacheNames().forEach(c -> cacheManager.getCache(c).clear());
    }

    public void clearContentSignatures() {
        getContentSignatureChangeRepositoryService().deleteAll();
    }

    public void clearEntityManager() {
        getEntityManager().flush();
        getEntityManager().clear();
    }

    // @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteContent(Content content) {
        getContentRepositoryService().deleteById(content.getId());
        // getContentRepositoryService().delete(content);
        // clearEntityManager();
    }

    public void executeInTransaction(Supplier consumer) {
        var entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction tx = null;
        try {
            tx = entityManager.getTransaction();
            tx.begin();

            consumer.get();

            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw new WrappedException(e);
        } finally {
            entityManager.close();
        }
    }

    public ArchiveService getArchiveService() {
        return archiveService;
    }

    public ContentMetadataItemRepositoryService getContentMetadataItemRepositoryService() {
        return contentMetadataItemRepositoryService;
    }

    public ContentMetadataRepositoryService getContentMetadataRepositoryService() {
        return contentMetadataRepositoryService;
    }

    public ContentMetadataSearchService getContentMetadataSearchService() {
        return contentMetadataSearchService;
    }

    public ContentMetadataSetRepositoryService getContentMetadataSetRepositoryService() {
        return contentMetadataSetRepositoryService;
    }

    public ContentRepositoryService getContentRepositoryService() {
        return contentRepositoryService;
    }

    public ContentSignatureChangeRepositoryService getContentSignatureChangeRepositoryService() {
        return contentSignatureChangeRepositoryService;
    }

    public ContentSignatureChangeService getContentSignatureChangeService() {
        return contentSignatureChangeService;
    }

    public ContentVolumeRepositoryService getContentVolumeRepositoryService() {
        return contentVolumeRepositoryService;
    }

    public ContentVolumeService getContentVolumeService() {
        return contentVolumeService;
    }

    @Transactional(readOnly = true)
    public List<Content> getContentsByContentVolume(ContentVolume repositoryVolume) {
        return getContentRepositoryService().findByVolume(repositoryVolume).collect(Collectors.toList());
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public TransactionConfiguration.TransactionNewTemplate getTransactionRequiresNewTemplate() {
        return transactionRequiresNewTemplate;
    }

    public Path getUnknownContentPath() {
        return getContentVolumeService().getExportUnknownPath();
    }

    public Path setUpFileArchived(Path targetDirectory, String targetFileName, Path... paths) {
        var targetPath = targetDirectory.resolve(targetFileName);
        return setUpFileArchived(targetPath, paths);
    }

    @SneakyThrows
    public Path setUpFileArchived(Path targetPath, Path[] paths) {
        var archiverFormat = ArchiverFormat.byFilename(targetPath.getFileName().toString()).orElseThrow(() -> new RuntimeException("invalid archiver format for " + targetPath));
        Files.createDirectories(targetPath.getParent());
        getArchiveService().archive(targetPath, paths);
        return targetPath;
    }

    @SneakyThrows
    public Path setUpFileArchived(Path targetPath, ArchiveEntryProducer... archiveEntryProducers) {
        return getArchiveService().archive(targetPath, Stream.of(archiveEntryProducers).toList());
    }

    public Path setUpFileRegular(ContentVolume volume, String fileName, byte[] bytes) {
        return createFile(volume.toPath(), fileName, bytes);
    }

    public Path setUpFilesCompressed(ArchiverFormat archiverFormat, Path targetDirectoryPath) throws IOException {
        var baseFileName = RandomStringUtils.randomAlphabetic(6);
        var path = createFile(targetDirectoryPath, baseFileName + ".txt", RetromasterTestFixtureService.CONTENT_PAYLOAD_1);
        var archivePath = targetDirectoryPath.resolve(baseFileName + "." + archiverFormat.getFileExtension());
        getArchiveService().archive(archivePath, path);

        return archivePath;
    }

    public Path setUpFilesCompressedWithDirectories(ArchiverFormat archiverFormat, Path tmpPath) throws IOException {
        var filePaths = createFiles(tmpPath.resolve("dir"), createFilename("txt"));
        var archivePath = tmpPath.resolve("test." + archiverFormat.getFileExtension());
        getArchiveService().archive(archivePath, filePaths.get(0));
        return archivePath;
    }

    public Content setUpPersistedContent(ContentVolume contentVolume) {
        return setUpPersistedContent(contentVolume, "somepath/" + createFilename("txt"), false, CONTENT_PAYLOAD_1);
    }

    public Content setUpPersistedContent(ContentVolume contentVolume, String subpath, boolean writeFile, byte[] bytes) {
        var content = createContent(contentVolume, subpath, NOW, bytes);
        if (writeFile) {
            setUpFileRegular(contentVolume, subpath, bytes);
        }
        return contentRepositoryService.save(content);
    }

    public Content setUpPersistedContent(ContentVolume contentVolume, Path fileAbsolutePath, String compressedEntryName, boolean writeFile, byte[] bytes) {
        return setUpPersistedContent(contentVolume, contentVolume.toPath().relativize(fileAbsolutePath).toString(), compressedEntryName, writeFile, bytes);
    }

    @SneakyThrows
    public Content setUpPersistedContent(ContentVolume contentVolume, String compressedFileRelativePath, String compressedEntryName, boolean writeFile, byte[] bytes) {
        var compressedFileAbsolutePath = contentVolume.toPath().resolve(compressedFileRelativePath);
        if (writeFile) {
            setUpFileArchived(compressedFileAbsolutePath, new BytesArchiveEntryProducer(bytes, compressedEntryName));
        }
        var lastModifiedTime = Files.exists(compressedFileAbsolutePath) ? getLastModifiedTime(compressedFileAbsolutePath) : LocalDateTime.MIN;
        return contentRepositoryService.save(createContent(contentVolume, compressedFileRelativePath + COMPRESS_SEPARATOR + compressedEntryName, lastModifiedTime, bytes));
    }

    @Transactional
    public ContentMetadata setUpPersistedContentMetadata(ContentMetadataSet contentMetadataSet, String name) {
        return getContentMetadataRepositoryService().save(createContentMetadata(contentMetadataSet, name));
    }

    public ContentMetadataSet setUpPersistedContentMetadataSet(int metadataCount, int metadataItemCount) {
        var contentMetadatas = IntStream.range(0, metadataCount)
            .mapToObj(
                i -> {
                    var items = IntStream.range(0, metadataItemCount).mapToObj(j -> createContentMetadataItem("recognized.ext" + j, createContentPayload(i, j).getBytes())).toArray(ContentMetadataItem[]::new);
                    return createContentMetadata(null, "metadata" + i, items);
                })
            .toArray(ContentMetadata[]::new);

        return setUpPersistedContentMetadataSet(createContentMetadataSet("metadataset", "TOSEC", "2019-01-01", contentMetadatas));
    }

    public ContentMetadataSet setUpPersistedContentMetadataSet(ContentMetadataSet contentMetadataSet) {
        var contentMetadataSetEnricher = contentMetadataSetEnricherService.getByContentMetadataSet(contentMetadataSet);

        var enrichedContentMetadataSet = contentMetadataSetEnricher.enrichContentMetadataSet(contentMetadataSet);
        enrichedContentMetadataSet.setCheckTime(NOW);
        contentMetadataSetRepositoryService.saveAndFlush(contentMetadataSet);
        var systemDefault = contentMetadataSetEnricher.extractSystem(contentMetadataSet);
        var enrichedContentMetadatas = contentMetadataSetEnricher.enrichContentMetadatas(contentMetadataSet.getContentMetadatas(), contentMetadataSet, systemDefault);
        enrichedContentMetadatas.stream()
            .filter(c -> c.getContentMetadataSet() == null)
            .forEach(
                c -> {
                    c.setContentMetadataSet(contentMetadataSet);
                    if (c.getItems() != null) {
                        c.getItems().stream().filter(i -> i.getContentMetadata() == null).forEach(i -> i.setContentMetadata(c));
                    }
                }); // If HB enhanced is this needed? Intellij...

        var partition = Partition.ofSize(enrichedContentMetadatas, 20);
        IntStream.range(0, partition.size()).forEach(i -> transactionRequiresNewTemplate.execute(t -> contentMetadataRepositoryService.saveAll(partition.get(i))));
        // log.info("test_inserting {}/{}", i, partition.size());
        contentSignatureChangeService.reindexContentSignatureChanges();

        return contentMetadataSet;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ContentVolume setUpPersistedContentVolume(ContentVolumeEnvironment environment, Path rootPath, boolean reuse) throws IOException {
        if (reuse) {
            var volumes = getContentVolumeRepositoryService().findByEnvironment(environment);
            if (!volumes.isEmpty()) {
                return volumes.get(0);
            }
        }
        var contentVolume = getContentVolumeService().save(createContentVolume(rootPath.resolve(environment.getPath()), environment));
        clearEntityManager();
        return contentVolume;
    }

    public Path toArchiveFileAbsolutePath(ContentVolume contentVolume, ContentMetadata contentMetadata) {
        return toArchiveFileAbsolutePath(contentVolume, contentMetadata, TAR_XZ);
    }

    public Path toArchiveFileAbsolutePath(ContentVolume contentVolume, ContentMetadata contentMetadata, ArchiverFormat archiverFormat) {
        return toContentMetadataSetPath(contentVolume, contentMetadata.getContentMetadataSet()).resolve(toArchiveFileName(contentMetadata, archiverFormat));
    }

    public String toArchiveFileName(ContentMetadata contentMetadata, ArchiverFormat archiverFormat) {
        return originContentTransferService.getTargetFileName(contentMetadata, true) + "." + archiverFormat.getFileExtension();
    }

    public Path toContentMetadataSetPath(ContentVolume contentVolume, ContentMetadataSet contentMetadataSet) {
        return contentVolume.toPath().resolve(contentMetadataSet.getRepositoryPath());
    }

    public Path toPlainFileAbsolutePath(ContentVolume contentVolume, ContentMetadataItem contentMetadataItem) {
        return toContentMetadataSetPath(contentVolume, contentMetadataItem.getContentMetadata().getContentMetadataSet()).resolve(contentMetadataItem.getContentMetadata().getName()).resolve(contentMetadataItem.getName());
    }

    public String toPlainFileDirectoryPath(ContentMetadataItem contentMetadataItem) {
        return Path.of(contentMetadataItem.getContentMetadata().getContentMetadataSet().getRepositoryPath()).resolve(contentMetadataItem.getContentMetadata().getName()).toString();
    }

    public Path toUnknownContentPath(ContentVolume contentVolume, Path contentMetadataProductionFileAbsolutePath) {
        return relocateTo(getUnknownContentPath(), contentVolume.toPath(), contentMetadataProductionFileAbsolutePath);
    }
}
