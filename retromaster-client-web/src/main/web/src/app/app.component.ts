/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* tslint:disable:max-line-length */
import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Meta, Title } from "@angular/platform-browser";
import { NavigationEnd, Router } from "@angular/router";
import { Location } from "@angular/common";
import { AppConfig } from "./app.config";
import { LocalStorage } from "angular-web-storage";
import { WebSocketService, WebSocketSubscription } from "./modules/core/services/web-socket.service";
import { MessageBusService } from "./modules/core/services/message-bus.service";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { RouterHelperService } from "./modules/core/services/router-helper.service";
import { ApplicationMessage, ApplicationMessagesService } from "./modules/core/services/application-messages.service";
import { RetromasterService } from "./modules/retromaster/services/retromaster.service";
import { RetromasterAuthService } from "./modules/retromaster/services/retromaster-auth.service";
import { TContentImportEventType, TContentMetadataSetManageEventType, TUser } from "./modules/retromaster/services/thrift";

declare const require;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  @LocalStorage() language = "en";

  isOnline: boolean;

  constructor(
    private retromasterService: RetromasterService,
    private retromasterAuthService: RetromasterAuthService,
    private applicationMessagesService: ApplicationMessagesService,
    private messageBusService: MessageBusService,
    private webSocketService: WebSocketService,
    private translateService: TranslateService,
    private routerHelperService: RouterHelperService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private title: Title,
    private meta: Meta,
    private router: Router,
    private location: Location
  ) {
    this.isOnline = navigator.onLine;
  }

  bindRouterEvents() {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        switch (event.urlAfterRedirects) {
          case "/":
            this.meta.updateTag({
              name: "description",
              content: "Retromaster"
            });
            break;
          case "/" + AppConfig.routes.retromaster:
            this.meta.updateTag({
              name: "description",
              content: "Retromaster"
            });
            break;
        }
      }
    });
  }

  doReload() {
    window.location.reload();
  }

  ngAfterViewInit() {
    this.getUser();
  }

  ngOnDestroy() {
    this.webSocketService.disconnect();
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.translateService.setDefaultLang('en');
    this.translateService.use(this.language);
    this.translateService.setTranslation("en", require("../assets/i18n/en.json"));

    this.title.setTitle(this.translateService.instant("app"));

    this.bindRouterEvents();

    this.bindWebsocketHandlers();

    this.messageBusService.subscribe("loggedIn", m => {
      console.log("onLoggedIn");
      this.getUser();
    });

    this.messageBusService.subscribe("logout", m => {
      console.log("onLogoutRequested");
      this.doLogout();
    });

    this.messageBusService.subscribe("serviceError", m => {
      console.log("onServiceError", m);

      this.snackBar.open("errorReceived " + m, "", {
        duration: AppConfig.snackBarDuration
      });

      if (m.message && m.message.indexOf("THRIFT_ERROR") > -1) {
        console.error("Thrift error received. Reloading");
        this.doReload();
      }
    });
  }

  private doLogout() {
    this.retromasterAuthService.clearAuthToken();
    this.doReload();
  }

  private bindWebsocketHandlers() {
    // Should eventually: Add stats to event only display if significative
    this.webSocketService.add(
      new WebSocketSubscription("contentExecution", "/topic/contentExecution", msg => {
        const event = this.retromasterService.deserializeContentExecutionEvent(msg.body);
        this.applicationMessagesService.add(new ApplicationMessage(event.message, "contentExecution"));
      })
    );
    this.webSocketService.add(
      new WebSocketSubscription("contentImport", "/topic/contentImport", msg => {
        const event = this.retromasterService.deserializeContentImportCompletedEvent(msg.body);
        this.applicationMessagesService.add(new ApplicationMessage(TContentImportEventType[event.type], "contentImport"));
      })
    );
    this.webSocketService.add(
      new WebSocketSubscription("contentMetadataSetManage", "/topic/contentMetadataSetManage", msg => {
        const event = this.retromasterService.deserializeContentMetadataSetManageEvent(msg.body);
        this.applicationMessagesService.add(new ApplicationMessage(TContentMetadataSetManageEventType[event.type], "contentMetadataSetManage"));
      })
    );
    this.webSocketService.add(
      new WebSocketSubscription("contentIndex", "/topic/contentIndex", msg => {
        const event = this.retromasterService.deserializeContentIndexEvent(msg.body);
        this.applicationMessagesService.add(new ApplicationMessage(event.message, "contentIndex"));
      })
    );
    this.webSocketService.add(
      new WebSocketSubscription("contentScan", "/topic/contentScan", msg => {
        const event = this.retromasterService.deserializeContentScanEvent(msg.body);
        this.applicationMessagesService.add(new ApplicationMessage(event.message, "contentScan"));
      })
    );
  }

  private getUser() {
    const authToken = this.retromasterAuthService.getAuthToken();
    if (!authToken) {
      console.log("getUser NO TOKEN");
      this.location.go("/retromaster/auth");
      return;
    }
    console.log("getUser RETURNING");
    this.retromasterAuthService.getUser().subscribe(
      v => {
        console.log("getUser - received ", v);
        if (v.name) {
          // Not anonymous
          this.onUserReceived(v);
        } else {
          console.log("getUser - No user received - Redirect to /", authToken);
          this.retromasterAuthService.clearAuthToken();
          this.location.go("/retromaster/auth");
        }
      },
      e => {
        console.error("getUser - FAILED SHOULD CLEAR AUTH AND REDIRECT TO /");
        this.retromasterAuthService.clearAuthToken();
        this.location.go("/retromaster/auth");
      }
    );
  }

  private onUserReceived(user: TUser) {
    console.log("onUserReceived", user);
    this.snackBar.open("Welcome " + user.name, "", {
      duration: AppConfig.snackBarDuration
    });

    this.webSocketService.connect();
  }
}
