/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppConfig } from './app.config';
import { CoreConfig } from './modules/core/core.config';

const routes: Routes = [
  {
    path: AppConfig.routes.retromaster,
    loadChildren: () => import('./modules/retromaster/retromaster.module').then(m => m.RetromasterModule),
  },
  {
    path: AppConfig.routes.app,
    loadChildren: () => import('./modules/core/core.module').then(m => m.CoreModule),
  },
  // On home go to...
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: '/' + AppConfig.routes.retromaster,
  },
  // otherwise redirect to 404
  {
    path: '**',
    redirectTo: '/' + AppConfig.routes.app + '/' + CoreConfig.routes.error404,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
