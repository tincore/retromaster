/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* tslint:disable:max-line-length */
import { AfterViewChecked, AfterViewInit, Component, DoCheck, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { RetromasterService } from "../../services/retromaster.service";
import { combineLatest, fromEvent, merge } from "rxjs";
import { debounceTime, distinctUntilChanged, startWith, tap } from "rxjs/operators";
import { animate, state, style, transition, trigger } from "@angular/animations";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVBACK,
  TOOLBAR_ITEM_NAVMENU,
  ToolbarItem,
  ToolbarItemLocation
} from "../../../core/components/toolbar/toolbar.component";
import { ActivatedRoute, Router } from "@angular/router";
import { AppConfig } from "../../../../app.config";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmDeleteDialogComponent } from "../../../core/components/confirm-delete-dialog/confirm-delete-dialog.component";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { PageStateService, TableState } from "../../services/page-state.service";
import { RouterHelperService } from "../../../core/services/router-helper.service";
import { TContent } from "../../services/thrift";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SelectionModel } from "@angular/cdk/collections";

const PAGE_STATE_ID = "contentDirectoryPathPageState";

interface PageState extends TableState {
  scrollTop: number;
  query: string,
  directoryPath: string
}

@Component({
  selector: "app-content-directory-path-page",
  templateUrl: "./content-directory-path-page.component.html",
  styleUrls: ["./content-directory-path-page.component.scss"],
  animations: [
    trigger("detailExpand", [state("collapsed", style({
      height: "0px",
      minHeight: "0",
      display: "none"
    })), state("expanded", style({ height: "*" })), transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))])
  ]
})
export class ContentDirectoryPathPageComponent implements OnInit, OnDestroy, AfterViewInit, DoCheck, AfterViewChecked {
  toolbarItemDelete = new ToolbarItem("delete", "delete", "delete", true, ToolbarItemLocation.RIGHT);
  toolbarItems = [TOOLBAR_ITEM_NAVMENU, TOOLBAR_ITEM_NAVBACK, this.toolbarItemDelete, TOOLBAR_ITEM_MESSAGE, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];
  displayedColumns: string[] = ["select", "fileName", "fileEntry", "fileSize", "modifiedTime"];
  error: string;
  busy: boolean;
  selection = new SelectionModel<TContent>(true, []);

  contents: TContent[];

  totalElements: number;

  public directoryPath: string;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild("queryInput", { static: true }) queryInput: ElementRef;
  @ViewChild("mainScroll", { static: true }) private mainScroll: ElementRef<HTMLElement>;

  private pageState: PageState;
  private pageStateRestored: boolean;

  constructor(
    private retromasterService: RetromasterService,
    private pageStateService: PageStateService,
    private messageBusService: MessageBusService,
    private dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService
  ) {
    this.busy = true;
  }

  isBusy() {
    return this.busy;
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    this.mainScroll.nativeElement.addEventListener("scroll", e => {
      this.pageState.scrollTop = this.mainScroll.nativeElement.scrollTop;
    });

    fromEvent(this.queryInput.nativeElement, "keyup")
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(() => {
        const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;
        if (this.pageState?.query != query) {
          this.paginator.firstPage();
          this.findContentsPaged();
        }
      });

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        tap(() => this.findContentsPaged())
      )
      .subscribe();
  }

  ngAfterViewChecked(): void {
    if (!this.pageStateRestored && this.contents) {
      this.mainScroll.nativeElement.scrollTop = this.pageState.scrollTop;
      this.pageStateRestored = true;
    }
  }

  ngOnInit() {
    this.busy = true;

    this.directoryPath = this.activatedRoute.snapshot.paramMap.get("directoryPath");

    this.pageState = this.pageStateService.getAndClear(PAGE_STATE_ID, {
      scrollTop: 0,
      query: null
    }) as PageState;

    if (this.pageState.directoryPath != null && this.pageState.directoryPath === this.directoryPath) {
      this.queryInput.nativeElement.value = this.pageState.query;
      this.pageStateService.restoreTableState(this.pageState, this.paginator, this.sort);
      this.pageStateRestored = this.pageState.scrollTop == 0;
      const query = this.queryInput.nativeElement.value;
      if (query != null) {
        this.findContentsPaged();
      }
    } else {
      this.queryInput.nativeElement.value = null;
      this.pageStateService.resetTableState(this.pageState, this.paginator);
      this.pageStateRestored = true;
    }
  }

  ngOnDestroy(): void {
    this.pageStateService.set(PAGE_STATE_ID, this.pageState);
  }

  ngDoCheck() {
    this.toolbarItemDelete.disabled = this.selection.isEmpty();
  }

  onCellClicked(content: TContent, event) {
    if (event.view.getSelection().type !== "Range") {
      this.router.navigate([`${AppConfig.routes.retromaster}/contentDirectoryPath/${this.directoryPath}/content/${content.id}`]);
    }
  }

  onDeleteClicked() {
    let selectedItems = this.selection.selected;

    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { count: selectedItems.length };

    this.dialog.open(ConfirmDeleteDialogComponent, dialogConfig)
      .afterClosed()
      .subscribe(dr => {
        if (dr) {
          this.busy = true;
          combineLatest(selectedItems.map(i => this.retromasterService.deleteContentById(i.id)))
            .subscribe(rs => {
                console.log("Responses ", rs);
                this.busy = false;
                this.onDeleteSuccess(selectedItems.length);
              },
              e => {
                this.busy = false;
                this.onDeleteFailed(e);
              }
            );
        }
      });
  }

  onToolbarAction(action): void {
    if (action === TOOLBAR_ITEM_NAVBACK.action) {
      this.routerHelperService.doBack();
    } else if (this.toolbarItemDelete.action === action) {
      this.onDeleteClicked();
    }
  }

  onQueryClearClicked() {
    this.queryInput.nativeElement.value = "";
    this.findContentsPaged();
  }

  onSelectAllChanged() {
    if (this.isSelectAll()) {
      this.selection.clear();
    } else {
      this.contents.forEach(e => this.selection.select(e));
    }
  }

  isSelectAll() {
    const selectedCount = this.selection.selected.length;
    const itemCount = this.contents.length;
    return selectedCount === itemCount;
  }

  private findContentsPaged() {
    const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;

    this.pageState = {
      ...this.pageState,
      ...(this.pageStateService.toTableState(this.paginator, this.sort)),
      directoryPath: this.directoryPath,
      query
    };

    this.busy = true;
    let formattedQuery = query;
    if (query && query.indexOf("%") === -1) {
      formattedQuery = "%" + query + "%";
    }
    if (this.retromasterService === undefined) {
      return;
    }
    this.selection.clear();
    this.retromasterService.findContentsByDirectoryPathPaged(formattedQuery, this.directoryPath, this.paginator.pageIndex, this.paginator.pageSize, this.sort.active, this.sort.direction).subscribe(
      d => {
        this.busy = false;
        this.totalElements = d.totalElements;
        this.error = null;
        this.contents = d.content;
      },
      e => {
        console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
        this.messageBusService.publish("serviceError", e);
      }
    );
  }

  private onDeleteFailed(e) {
    console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
    this.messageBusService.publish("serviceError", e);
    this.findContentsPaged();
  }

  private onDeleteSuccess(count: number) {
    this.snackBar.open(this.translateService.instant("deleted", { count: count }), "", {
      duration: AppConfig.snackBarDuration
    });
    // Should eventually: trigger sort!
    this.findContentsPaged();
  }

}
