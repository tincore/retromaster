/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Component, OnInit } from '@angular/core';
import { LoginDialogComponent } from '../../../core/components/login-dialog/login-dialog.component';
import { RetromasterAuthService } from '../../services/retromaster-auth.service';
import { AppConfig } from '../../../../app.config';
import { Router } from '@angular/router';
import { MessageBusService } from '../../../core/services/message-bus.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-login-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss'],
})
export class AuthPageComponent implements OnInit {
  constructor(private dialog: MatDialog, private router: Router, private retromasterAuthService: RetromasterAuthService, private messageBusService: MessageBusService) {}

  ngOnInit() {
    // console.log("ngOnInit");
    Promise.resolve().then(() => {
      this.openLoginDialog();
    });
  }

  private doLogin(username: string, password: string) {
    this.retromasterAuthService.doLogin(username, password).subscribe(
      d => {
        this.retromasterAuthService.clearAuthToken();
        if (d) {
          this.retromasterAuthService.setAuthToken(d);
          // Use cookie or Bearer Header
          this.messageBusService.publish('loggedIn', {});
          this.router.navigate([`${AppConfig.routes.retromaster}`]);
        } else {
          // Login failed
          this.openLoginDialog();
        }
      },
      e => {
        console.log('onError: msg=%s, code=%s, e', e.message, e.code, e);
      }
    );
  }

  private openLoginDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(LoginDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      dr => {
        console.log('dialogResult', dr);
        if (dr) {
          this.doLogin(dr.username, dr.password);
        } else {
          // or reload?
          this.openLoginDialog();
        }
      },
      e => {
        console.log('onError: msg=%s, code=%s, e', e.message, e.code, e);
      }
    );
  }
}
