/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
// Exposes globals... I think that I should use requireds like in the bottom commented piece. But have problems using TDirection. Find out what to do in the future
module.exports = {
  Thrift: Thrift,
  RetromasterServiceClient: RetromasterServiceClient,
  RetromasterAuthServiceClient: RetromasterAuthServiceClient,

  TDirection: TDirection,
  TOrder: TOrder,
  TPageable: TPageable,
  TUser: TUser,
  TQueryResult: TQueryResult,
  TQueryFacetValue: TQueryFacetValue,
  TQueryFacet: TQueryFacet,
  TQueryResults: TQueryResults,
  TQueryFilter: TQueryFilter,
  TQueryFacetFilter: TQueryFacetFilter,
  TQuery: TQuery,
  TContentMediaType: TContentMediaType,
  TContentMetadataSet: TContentMetadataSet,
  TContentMetadataSets: TContentMetadataSets,
  TContentMetadataSetFilter: TContentMetadataSetFilter,
  TContentMetadata: TContentMetadata,
  TContentMetadataSummary: TContentMetadataSummary,
  TContentMetadataSummaries: TContentMetadataSummaries,
  TContentMetadatas: TContentMetadatas,
  TContentMetadataFilter: TContentMetadataFilter,
  TContentMetadataItem: TContentMetadataItem,
  TContentMetadataItemSummary: TContentMetadataItemSummary,
  TContentMetadataComplete: TContentMetadataComplete,
  TContentVolumeEnvironment: TContentVolumeEnvironment,
  TContentVolume: TContentVolume,
  TContentVolumeDirectory: TContentVolumeDirectory,
  TContentVolumeCreate: TContentVolumeCreate,
  TContent: TContent,
  TContents: TContents,
  TContentFilter: TContentFilter,
  TContentDirectoryPath: TContentDirectoryPath,
  TContentDirectoryPaths: TContentDirectoryPaths,
  TContentDirectoryPathFilter: TContentDirectoryPathFilter,
  TContentMetadataItemExecuteOptionSet: TContentMetadataItemExecuteOptionSet,
  TContentSystem: TContentSystem,
  TContentExecutor: TContentExecutor,
  TContentExecutorFilter: TContentExecutorFilter,
  TContentExecuteProcess: TContentExecuteProcess,
  TContentImportEventType: TContentImportEventType,
  TContentImportEvent: TContentImportEvent,
  TContentMetadataSetManageEventType: TContentMetadataSetManageEventType,
  TContentMetadataSetManageEvent: TContentMetadataSetManageEvent,
  TContentScanEventType: TContentScanEventType,
  TContentScanEvent: TContentScanEvent,
  TContentIndexEventType: TContentIndexEventType,
  TContentIndexEvent: TContentIndexEvent,
  TContentExecutionEvent: TContentExecutionEvent,
  TGenericError: TGenericError,
};

// var thriftClient = require('./thrift-client.js')
// var retromasterFormTypes = require('./retromasterForm_types.js')
// var retromasterAuthService = require('./RetromasterAuthService.js')
// var retromasterService = require('./RetromasterService.js')
//
// module.exports = {
//     Thrift: thriftClient.Thrift,
//     // TDeserializer: thriftClient.Thrift.TDeserializer,
//     RetromasterServiceClient: retromasterService.RetromasterServiceClient,
//     RetromasterAuthServiceClient: retromasterAuthService.RetromasterAuthServiceClient,
//
//     TDirection: retromasterFormTypes.TDirection,
//     TOrder: retromasterFormTypes.TOrder,
//     TPageable: retromasterFormTypes.TPageable,
//     TUser: retromasterFormTypes.TUser,
//     TQueryResult: retromasterFormTypes.TQueryResult,
//     TQueryFacetValue: retromasterFormTypes.TQueryFacetValue,
//     TQueryFacet: retromasterFormTypes.TQueryFacet,
//     TQueryResults: retromasterFormTypes.TQueryResults,
//     TQueryFilter: retromasterFormTypes.TQueryFilter,
//     TQueryFacetFilter: retromasterFormTypes.TQueryFacetFilter,
//     TQuery: retromasterFormTypes.TQuery,
//     TContentMediaType: retromasterFormTypes.TContentMediaType,
//     TContentMetadataSet: retromasterFormTypes.TContentMetadataSet,
//     TContentMetadataSets: retromasterFormTypes.TContentMetadataSets,
//     TContentMetadataSetFilter: retromasterFormTypes.TContentMetadataSetFilter,
//     TContentMetadata: retromasterFormTypes.TContentMetadata,
//     TContentMetadataSummary: retromasterFormTypes.TContentMetadataSummary,
//     TContentMetadataSummaries: retromasterFormTypes.TContentMetadataSummaries,
//     TContentMetadatas: retromasterFormTypes.TContentMetadatas,
//     TContentMetadataFilter: retromasterFormTypes.TContentMetadataFilter,
//     TContentMetadataItem: retromasterFormTypes.TContentMetadataItem,
//     TContentMetadataItemSummary: retromasterFormTypes.TContentMetadataItemSummary,
//     TContentVolumeEnvironment: retromasterFormTypes.TContentVolumeEnvironment,
//     TContentVolume: retromasterFormTypes.TContentVolume,
//     TContentVolumeDirectory: retromasterFormTypes.TContentVolumeDirectory,
//     TContentVolumeCreate: retromasterFormTypes.TContentVolumeCreate,
//     TContent: retromasterFormTypes.TContent,
//     TContents: retromasterFormTypes.TContents,
//     TContentFilter: retromasterFormTypes.TContentFilter,
//     TContentDirectoryPath: retromasterFormTypes.TContentDirectoryPath,
//     TContentDirectoryPaths: retromasterFormTypes.TContentDirectoryPaths,
//     TContentDirectoryPathFilter: retromasterFormTypes.TContentDirectoryPathFilter,
//     TContentMetadataItemExecuteOptionSet: retromasterFormTypes.TContentMetadataItemExecuteOptionSet,
//     TContentSystem: retromasterFormTypes.TContentSystem,
//     TContentExecutor: retromasterFormTypes.TContentExecutor,
//     TContentExecutorFilter: retromasterFormTypes.TContentExecutorFilter,
//     TContentExecuteProcess: retromasterFormTypes.TContentExecuteProcess,
//     TContentImportEventType: retromasterFormTypes.TContentImportEventType,
//     TContentImportEvent: retromasterFormTypes.TContentImportEvent,
//     TContentMetadataSetManageEventType: retromasterFormTypes.TContentMetadataSetManageEventType,
//     TContentMetadataSetManageEvent: retromasterFormTypes.TContentMetadataSetManageEvent,
//     TContentScanEventType: retromasterFormTypes.TContentScanEventType,
//     TContentScanEvent: retromasterFormTypes.TContentScanEvent,
//     TContentIndexEventType: retromasterFormTypes.TContentIndexEventType,
//     TContentIndexEvent: retromasterFormTypes.TContentIndexEvent,
//     TContentExecutionEvent: retromasterFormTypes.TContentExecutionEvent,
//     TGenericError: retromasterFormTypes.TGenericError
// }
//
