/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT, TOOLBAR_ITEM_MESSAGE, TOOLBAR_ITEM_NAVMENU } from '../../../core/components/toolbar/toolbar.component';
import { MessageBusService } from '../../../core/services/message-bus.service';
import { RetromasterService } from '../../services/retromaster.service';
import { TContentExecutor, TContentMetadataItem } from "../../services/thrift";

@Component({
  selector: 'app-content-executors-page',
  templateUrl: './content-executors-page.component.html',
  styleUrls: ['./content-executors-page.component.scss'],
  animations: [
    trigger('detailExpand', [state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })), state('expanded', style({ height: '*' })), transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))]),
  ],
})
export class ContentExecutorsPageComponent implements AfterViewInit {
  toolbarItems = [TOOLBAR_ITEM_NAVMENU, TOOLBAR_ITEM_MESSAGE, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];
  displayedColumns = ['name', 'systems', 'enabled', 'found'];
  error: string;
  busy: boolean;

  contentExecutors: TContentExecutor[];

  constructor(private retromasterService: RetromasterService, private messageBusService: MessageBusService) {
    this.busy = true;
  }

  isBusy() {
    return this.busy;
  }

  ngAfterViewInit() {
    this.findContentExecutors();
  }

  onCellClicked(contentExecutor: TContentExecutor, event) {
    if (event.view.getSelection().type !== "Range") {
      // Do something
    }
  }

  onToolbarAction(action) {}

  private findContentExecutors() {
    this.busy = true;
    if (this.retromasterService === undefined) {
      return;
    }
    this.retromasterService.findContentExecutors().subscribe(
      d => {
        this.busy = false;
        this.error = null;
        this.contentExecutors = d;
      },
      e => {
        console.log('onError: msg=%s, code=%s, e', e.message, e.code, e);
        this.messageBusService.publish('serviceError', e);
      }
    );
  }
}
