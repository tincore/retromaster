/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AppConfig } from "../../../app.config";
import { RetromasterAuthServiceClient, Thrift, TUser } from "./thrift";
import { CookieService } from "ngx-cookie-service";

@Injectable({
  providedIn: "root"
})
export class RetromasterAuthService {
  endpoint: string;
  private readonly COOKIE_NAME = "SO_AUTH_TOKEN";

  constructor(private cookieService: CookieService) {
    this.endpoint = AppConfig.endpoints.retromasterAuthApi;
  }

  tCallback = (o, r) => {
    if (r instanceof Thrift.TException) {
      return o.error(r);
    } else {
      return o.next(r);
    }
  };

  doLogin(username: string, password: string): Observable<string> {
    return new Observable(o => this.client().doLogin(username, password, r => this.tCallback(o, r)));
  }

  getUser(): Observable<TUser> {
    return new Observable(o => this.client().getUserMe(r => this.tCallback(o, r)));
  }

  clearAuthToken() {
    console.log("clearAuthToken");
    this.cookieService.delete(this.COOKIE_NAME, "/");
    if (this.getAuthToken()) {
      console.warn("clearAuthToken HAS NOT CLEARED YET!");
    }
  }

  setAuthToken(authToken: string) {
    // console.log('setAuthToken', authToken);
    this.cookieService.set(this.COOKIE_NAME, authToken, 1, "/");
    // console.log('setAuthToken DONE ', this.cookieService.getAll());
  }

  getAuthToken() {
    let cookies = this.cookieService.getAll();
    // console.log("COOKIES ", cookies);
    return this.cookieService.get(this.COOKIE_NAME);
  }

  private client() {
    const protocol = new Thrift.TBinaryProtocol(new Thrift.TXHRTransport(this.endpoint, { binary: true }), false, false);
    return new RetromasterAuthServiceClient(protocol, protocol);
  }
}
