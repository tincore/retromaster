/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* tslint:disable:no-non-null-assertion max-line-length */
import { animate, state, style, transition, trigger } from "@angular/animations";
import { BreakpointObserver } from "@angular/cdk/layout";
import { AfterViewInit, Component, DoCheck, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatSort } from "@angular/material/sort";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { fromEvent, merge } from "rxjs";
import { debounceTime, distinctUntilChanged, startWith, tap } from "rxjs/operators";
import { AppConfig } from "../../../../app.config";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_DETAILS,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVBACK,
  TOOLBAR_ITEM_NAVMENU,
  ToolbarItem,
  ToolbarItemLocation
} from "../../../core/components/toolbar/toolbar.component";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { RouterHelperService } from "../../../core/services/router-helper.service";
import { PageStateService, TableState } from "../../services/page-state.service";
import { RetromasterService } from "../../services/retromaster.service";
import { TContentMetadataSet, TContentMetadataSummary } from "../../services/thrift";

const PAGE_STATE_ID = "contentMetadataSetPageState";

interface PageState extends TableState {
  query: string;
  contentMetadataSetId: string;
  displayDetails: boolean;
}

@Component({
  selector: "app-content-metadata-set-page",
  templateUrl: "./content-metadata-set-page.component.html",
  styleUrls: ["./content-metadata-set-page.component.scss"],
  animations: [
    trigger("detailExpand", [state("collapsed", style({
      height: "0px",
      minHeight: "0",
      display: "none"
    })), state("expanded", style({ height: "*" })), transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))])
  ]
})
export class ContentMetadataSetPageComponent implements OnInit, OnDestroy, AfterViewInit, DoCheck {
  @ViewChild("form") formElement;
  @ViewChild("formSubmit") formSubmitElement;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild("queryInput", { static: true }) queryInput: ElementRef;

  toolbarItemSave = new ToolbarItem("save", "save", "save", true, ToolbarItemLocation.RIGHT);
  toolbarItemScanContents = new ToolbarItem("scanContents", "cached", "scanContents", false, ToolbarItemLocation.MENU);
  toolbarItems = [TOOLBAR_ITEM_NAVMENU, TOOLBAR_ITEM_NAVBACK, TOOLBAR_ITEM_DETAILS, this.toolbarItemSave, TOOLBAR_ITEM_MESSAGE, this.toolbarItemScanContents, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];

  displayedColumns = ["contentMetadata"];

  editableFields = ["name", "version", "tagsExtra", "description", "author", "email", "url", "category"];
  error: string;
  busy: boolean;

  contentMetadataSet: TContentMetadataSet;

  contentMetadataSummary: TContentMetadataSummary;
  contentMetadataSummaries: TContentMetadataSummary[];

  totalElements: number;

  private contentMetadataSetForm: UntypedFormGroup;
  private saving: boolean;

  private pageState: PageState;

  constructor(
    private retromasterService: RetromasterService,
    private pageStateService: PageStateService,
    private messageBusService: MessageBusService,
    private formBuilder: UntypedFormBuilder,
    private snackBar: MatSnackBar,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private breakpointObserver: BreakpointObserver,
    private translateService: TranslateService,
    private routerHelperService: RouterHelperService
  ) {
    this.busy = true;
    breakpointObserver.observe(["(max-width: 1279px)"]).subscribe(result => {
      this.displayedColumns = result.matches ? ["contentMetadata"] : ["title", "publisher", "date", "media", "details"];
    });

    this.contentMetadataSetForm = this.formBuilder.group({
      id: new UntypedFormControl({ value: "", disabled: true }, []),

      category: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),
      name: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),
      version: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),
      tagsExtra: new UntypedFormControl({ value: "", disabled: true }, []),
      description: new UntypedFormControl({ value: "", disabled: true }, []),
      author: new UntypedFormControl({ value: "", disabled: true }, []),
      email: new UntypedFormControl({ value: "", disabled: true }, []),
      url: new UntypedFormControl({ value: "", disabled: true }, []),
      repositoryPath: new UntypedFormControl({ value: "", disabled: true }, []),
      filename: new UntypedFormControl({ value: "", disabled: true }, []),
      staging: new UntypedFormControl({ value: "", disabled: true }, [])
    });
  }

  onQueryClearClicked() {
    this.queryInput.nativeElement.value = "";
    this.findContentMetadataSummariesPaged();
  }

  ngAfterViewInit() {
    const id = this.activatedRoute.snapshot.paramMap.get("id");
    this.retromasterService.findContentMetadataSetById(id).subscribe(v => {
      this.onContentMetadataSet(v);
    });

    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    fromEvent(this.queryInput.nativeElement, "keyup")
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(() => {
        const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;
        if (this.pageState?.query != query) {
          this.paginator.firstPage();
          this.findContentMetadataSummariesPaged();
        }
      });

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        tap(() => this.findContentMetadataSummariesPaged())
      )
      .subscribe();
  }

  ngDoCheck() {
    this.toolbarItemSave.disabled = (this.formElement && this.formElement.submitted) || !this.contentMetadataSetForm.valid || !this.pageState.displayDetails || !this.isEditable();
  }

  ngOnInit() {
    const contentMetadataSetId = this.activatedRoute.snapshot.paramMap.get("id");

    this.pageState = this.pageStateService.getAndClear(PAGE_STATE_ID, {
      query: null,
      displayDetails: false
    }) as PageState;
    if (this.pageState.contentMetadataSetId != null && this.pageState.contentMetadataSetId === contentMetadataSetId) {
      this.queryInput.nativeElement.value = this.pageState.query;
      this.pageStateService.restoreTableState(this.pageState, this.paginator, this.sort);
    } else {
      this.queryInput.nativeElement.value = null;
      this.pageStateService.resetTableState(this.pageState, this.paginator);
    }
  }

  ngOnDestroy(): void {
    this.pageStateService.set(PAGE_STATE_ID, this.pageState);
  }

  onFormSubmitted(): void {
    this.doSave();
  }

  onCellClicked(contentMetadata: TContentMetadataSummary, event) {
    if (event.view.getSelection().type !== "Range") {
      this.router.navigate([`${AppConfig.routes.retromaster}/contentMetadata/${contentMetadata.id}`]);
    }
  }

  onToolbarAction(action): void {
    if (action === TOOLBAR_ITEM_NAVBACK.action) {
      this.routerHelperService.doBack();
    } else if (action === TOOLBAR_ITEM_DETAILS.action) {
      this.onItemDetailsClicked();
    } else if (this.toolbarItemSave.action === action) {
      this.onSaveClicked();
    } else if (this.toolbarItemScanContents.action === action) {
      this.onScanContentsClicked();
    }
  }

  isDisplayDetails() {
    return this.pageState.displayDetails;
  }

  private onSaveClicked() {
    this.formSubmitElement.nativeElement.click();
  }

  private onItemDetailsClicked() {
    this.toggleDisplayDetails();
  }

  private onScanContentsClicked() {
    this.retromasterService.doContentScanByContentMetadataSetId(this.contentMetadataSet.id).subscribe(
      c => {
        this.snackBar.open(this.translateService.instant("contentScanRequested"), "", {
          duration: AppConfig.snackBarDuration
        });
      },
      (r: any) => {
        console.log("error", r, typeof r);
        this.saving = false;
        if (r.code && r.message) {
          this.error = r.message;
        }
      }
    );
  }

  private doSave() {
    if (this.saving) {
      return;
    }
    if (this.contentMetadataSetForm.valid) {
      this.saving = true;
      const formValue = this.contentMetadataSetForm.value;
      const contentMetadataSet = new TContentMetadataSet({
        ...formValue,
        id: this.contentMetadataSet.id
      });

      console.log("save", contentMetadataSet);
      this.retromasterService.saveContentMetadataSet(contentMetadataSet).subscribe(
        c => {
          this.saving = false;
          this.onSaveSuccessful();
        },
        (r: any) => {
          console.log("error", r, typeof r);
          this.saving = false;
          if (r.code && r.message) {
            this.error = r.message;
          }
        }
      );
    }
  }

  private findContentMetadataSummariesPaged() {
    const contentMetadataSetId = this.activatedRoute.snapshot.paramMap.get("id");

    const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;

    this.pageState = {
      ...this.pageState,
      ...(this.pageStateService.toTableState(this.paginator, this.sort)),
      contentMetadataSetId: contentMetadataSetId,
      query
    };

    this.busy = true;
    let formattedQuery = query;
    if (query && query.indexOf("%") === -1) {
      formattedQuery = "%" + query + "%";
    }

    if (this.retromasterService === undefined) {
      return;
    }

    this.retromasterService.findContentMetadataSummariesPagedByContentMetadataSetId(contentMetadataSetId, formattedQuery, this.paginator.pageIndex, this.paginator.pageSize, this.sort.active, this.sort.direction).subscribe(
      d => {
        this.busy = false;
        this.error = null;
        this.totalElements = d.totalElements;
        this.contentMetadataSummaries = d.content;
      },
      e => {
        this.busy = false;
        this.onError(e);
      }
    );
  }

  private onError(e) {
    console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
    this.messageBusService.publish("serviceError", e);
  }

  private isEditable(contentMetadataSet: TContentMetadataSet = this.contentMetadataSet): boolean {
    return contentMetadataSet && contentMetadataSet.staging;
  }

  private onContentMetadataSet(contentMetadataSet: TContentMetadataSet) {
    this.contentMetadataSet = contentMetadataSet;
    this.contentMetadataSetForm.setValue(contentMetadataSet);

    if (this.isEditable()) {
      this.editableFields.forEach(f => this.contentMetadataSetForm.get(f).enable());
    } else {
      this.editableFields.forEach(f => this.contentMetadataSetForm.get(f).disable());
    }
  }

  private onSaveSuccessful(): void {
    this.snackBar.open(this.translateService.instant("saved"), "", {
      duration: AppConfig.snackBarDuration
    });
  }

  private toggleDisplayDetails(): void {
    this.pageState.displayDetails = !this.pageState.displayDetails;
  }
}
