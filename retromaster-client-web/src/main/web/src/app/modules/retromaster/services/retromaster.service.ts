/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* tslint:disable:max-line-length */

import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AppConfig } from "../../../app.config";
import {
  RetromasterServiceClient,
  TContent,
  TContentDirectoryPathFilter,
  TContentDirectoryPaths,
  TContentExecutionEvent,
  TContentExecutor,
  TContentExecutorFilter,
  TContentFilter,
  TContentImportEvent,
  TContentIndexEvent,
  TContentMetadata,
  TContentMetadataFilter,
  TContentMetadataItemExecuteOptionSet,
  TContentMetadataItemSummary, TContentMetadataMedia,
  TContentMetadatas,
  TContentMetadataSet,
  TContentMetadataSetFilter,
  TContentMetadataSetManageEvent,
  TContentMetadataSets,
  TContentMetadataSummaries,
  TContents,
  TContentScanEvent,
  TContentVolume,
  TContentVolumeDirectory,
  TDirection,
  Thrift,
  TOrder,
  TPageable,
  TQuery,
  TQueryResults
} from "./thrift";

@Injectable({
  providedIn: "root"
})
export class RetromasterService {
  endpoint: string;
  tDeserializer = new Thrift.TDeserializer();

  constructor() {
    this.endpoint = AppConfig.endpoints.retromasterApi;
  }

  static base64ToArrayBuffer(base64): Uint8Array {
    const binary_string = window.atob(base64);
    const len = binary_string.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes;
  }

  tCallback = (o, r) => {
    if (r instanceof Thrift.TException) {
      return o.error(r);
    } else {
      return o.next(r);
    }
  };

  deleteContentById(id: string) {
    return null; // Implement!
    // return new Observable(o => this.tCallback(o, "DUMMY_DELETE "+id));
    // return new Observable(o => this.client().deleteContentById(id, r => this.tCallback(o, r)));
  }

  deleteContentMetadataSetById(id: string) {
    return new Observable(o => this.client().deleteContentMetadataSetById(id, r => this.tCallback(o, r)));
  }

  deserializeContentExecutionEvent(encodedData: string): TContentExecutionEvent {
    return this.deserialize(encodedData, new TContentExecutionEvent());
  }

  deserializeContentImportCompletedEvent(encodedData: string): TContentImportEvent {
    return this.deserialize(encodedData, new TContentImportEvent());
  }

  deserializeContentIndexEvent(encodedData: string): TContentIndexEvent {
    return this.deserialize(encodedData, new TContentIndexEvent());
  }

  deserializeContentMetadataSetManageEvent(encodedData: string): TContentMetadataSetManageEvent {
    return this.deserialize(encodedData, new TContentMetadataSetManageEvent());
  }

  deserializeContentScanEvent(encodedData: string): TContentScanEvent {
    return this.deserialize(encodedData, new TContentScanEvent());
  }

  doContentMetadataItemExecute(contentMetadataItemId: string, executorId: string) {
    return new Observable(o => this.client().doContentMetadataItemExecute(contentMetadataItemId, executorId, r => this.tCallback(o, r)));
  }

  doContentScanByContentMetadataId(id: string) {
    return new Observable(o => this.client().doContentScanByContentMetadataId(id, r => this.tCallback(o, r)));
  }

  doContentScanByContentMetadataSetId(id: string) {
    return new Observable(o => this.client().doContentScanByContentMetadataSetId(id, r => this.tCallback(o, r)));
  }

  doContentScanByContentVolumeStaging() {
    return new Observable(o => this.client().doContentScanByContentVolumeStaging(r => this.tCallback(o, r)));
  }

  doContentVolumeDirectoryScan(directory: TContentVolumeDirectory) {
    return new Observable(o => this.client().doContentVolumeDirectoryScan(directory, r => this.tCallback(o, r)));
  }

  doQuery(query: TQuery, page: number, size: number, sortProperty: string, sortDirection: string): Observable<TQueryResults> {
    const pageable = this.toPageableTForm(page, size, sortProperty, sortDirection);

    return new Observable(o => this.client().doQuery(query, pageable, r => this.tCallback(o, r)));
  }

  doReindexRequest() {
    return new Observable(o => this.client().doReindexRequest(r => this.tCallback(o, r)));
  }

  doContentReorganizeRequest() {
    return new Observable(o => this.client().doContentReorganizeRequest(r => this.tCallback(o, r)));
  }

  findContentById(id: string): Observable<TContent> {
    return new Observable(o => this.client().findContentById(id, r => this.tCallback(o, r)));
  }

  findContentDirectoryPathsByFilter(query: string, page: number, size: number, sortProperty: string, sortDirection: string): Observable<TContentDirectoryPaths> {
    const contentDirectoryPathFilter = new TContentDirectoryPathFilter({
      query: query
    });
    const pageable = this.toPageableTForm(page, size, "directoryPath", sortDirection);

    return new Observable(o => this.client().findContentDirectoryPathsByFilter(contentDirectoryPathFilter, pageable, r => this.tCallback(o, r)));
  }

  findContentExecutors(): Observable<TContentExecutor[]> {
    return new Observable(o => this.client().findContentExecutors(new TContentExecutorFilter(), r => this.tCallback(o, r)));
  }

  findContentExecutorsByContentMetadataItemId(id: string): Observable<TContentExecutor[]> {
    return new Observable(o => this.client().findContentExecutorsByContentMetadataItemId(id, r => this.tCallback(o, r)));
  }

  findContentMetadataById(id: string): Observable<TContentMetadata> {
    return new Observable(o => this.client().findContentMetadataById(id, r => this.tCallback(o, r)));
  }

  findContentMetadataItemExecuteOptionSetsByContentId(id: string): Observable<TContentMetadataItemExecuteOptionSet[]> {
    return new Observable(o => this.client().findContentMetadataItemExecuteOptionSetsByContentId(id, r => this.tCallback(o, r)));
  }

  findContentMetadataItemExecuteOptionSetByContentMetadataId(id: string): Observable<TContentMetadataItemExecuteOptionSet> {
    return new Observable(o => this.client().findContentMetadataItemExecuteOptionSetByContentMetadataId(id, r => this.tCallback(o, r)));
  }

  findContentMetadataItemSummariesByContentId(id: string): Observable<TContentMetadataItemSummary[]> {
    return new Observable(o => this.client().findContentMetadataItemSummariesByContentId(id, r => this.tCallback(o, r)));
  }

  findContentMetadataSetById(id: string): Observable<TContentMetadataSet> {
    return new Observable(o => this.client().findContentMetadataSetById(id, r => this.tCallback(o, r)));
  }

  findContentMetadataSetsPaged(query: string, page: number, size: number, sortProperty: string, sortDirection: string): Observable<TContentMetadataSets> {
    const contentMetadataSetFilter = new TContentMetadataSetFilter({
      query: query
    });
    const pageable = this.toPageableTForm(page, size, sortProperty, sortDirection);

    return new Observable(o => this.client().findContentMetadataSetsByFilter(contentMetadataSetFilter, pageable, r => this.tCallback(o, r)));
  }

  findContentMetadatasByContentId(id: string): Observable<TContentMetadata[]> {
    return new Observable(o => this.client().findContentMetadatasByContentId(id, r => this.tCallback(o, r)));
  }

  findContentMetadataSummariesPagedByContentMetadataSetId(id: string, query: string, page: number, size: number, sortProperty: string, sortDirection: string): Observable<TContentMetadataSummaries> {
    const contentMetadataFilter = new TContentMetadataFilter({
      contentMetadataSetIdEquals: id,
      query: query
    });
    const pageable = this.toPageableTForm(page, size, sortProperty, sortDirection);

    return new Observable(o => this.client().findContentMetadataSummariesByFilter(contentMetadataFilter, pageable, r => this.tCallback(o, r)));
  }

  findContentMetadatasPagedByQuery(query: string, page: number, size: number, sortProperty: string, sortDirection: string): Observable<TContentMetadatas> {
    const contentMetadataFilter = new TContentMetadataFilter({ query: query });
    const pageable = this.toPageableTForm(page, size, sortProperty, sortDirection);

    return new Observable(o => this.client().findContentMetadatasByFilter(contentMetadataFilter, pageable, r => this.tCallback(o, r)));
  }

  findContentsByDirectoryPathPaged(query: string, directoryPath: string, page: number, size: number, sortProperty: string, sortDirection: string): Observable<TContents> {
    const contentFilter = new TContentFilter({
      query: query,
      directoryPathEquals: directoryPath
    });
    const pageable = this.toPageableTForm(page, size, sortProperty, sortDirection);

    return new Observable(o => this.client().findContentsByFilter(contentFilter, pageable, r => this.tCallback(o, r)));
  }

  findContentsPaged(query: string, page: number, size: number, sortProperty: string, sortDirection: string): Observable<TContents> {
    const contentFilter = new TContentFilter({ query: query });
    const pageable = this.toPageableTForm(page, size, sortProperty, sortDirection);

    return new Observable(o => this.client().findContentsByFilter(contentFilter, pageable, r => this.tCallback(o, r)));
  }

  findContentVolumeById(id: string): Observable<TContentVolume> {
    return new Observable(o => this.client().findContentVolumeById(id, r => this.tCallback(o, r)));
  }

  findContentVolumes(): Observable<TContentVolume[]> {
    return new Observable(o => this.client().findContentVolumes(r => this.tCallback(o, r)));
  }

  findContentVolumeDirectoriesByContentVolumeId(id: string, extended: boolean = false): Observable<TContentVolumeDirectory[]> {
    return new Observable(o => this.client().findContentVolumeDirectoriesByContentVolumeId(id, extended, r => this.tCallback(o, r)));
  }

  findContentVolumeDirectoryByContentVolumeIdAndPath(id: string, path: string, extended: boolean = false): Observable<TContentVolumeDirectory[]> {
    return new Observable(o => this.client().findContentVolumeDirectoryByContentVolumeIdAndPath(id, path, extended, r => this.tCallback(o, r)));
  }

  findContentIdByContentMetadataItemId(id: string): Observable<string> {
    return new Observable(o => this.client().findContentIdByContentMetadataItemId(id, r => this.tCallback(o, r)));
  }

  findContentIdsByContentMetadataId(id: string): Observable<{ [k: string]: string }> {
    return new Observable(o => this.client().findContentIdsByContentMetadataId(id, true, r => this.tCallback(o, r)));
  }

  findContentMetadataMediasByContentMetadataId(id: string): Observable<TContentMetadataMedia[]> {
    return new Observable(o => this.client().findContentMetadataMediasByContentMetadataId(id, r => this.tCallback(o, r)));
  }

  findContentMetadataMediasBySystemAndTitle(system: string, title: string): Observable<TContentMetadataMedia[]> {
    return new Observable(o => this.client().findContentMetadataMediasBySystemAndTitle(system, title, r => this.tCallback(o, r)));
  }

  saveContentMetadata(contentMetadata: TContentMetadata): Observable<string> {
    return Observable.create(o => {
      this.client().saveContentMetadata(contentMetadata, r => this.tCallback(o, r));
    });
  }

  saveContentMetadataSet(contentMetadataSet: TContentMetadataSet): Observable<string> {
    return Observable.create(o => {
      this.client().saveContentMetadataSet(contentMetadataSet, r => this.tCallback(o, r));
    });
  }

  private client(): RetromasterServiceClient {
    const options = {
      binary: true,
      customHeaders: {
        // Potentially set Auth Bearer
      }
    };
    const protocol = new Thrift.TBinaryProtocol(new Thrift.TXHRTransport(this.endpoint, options), false, false);
    return new RetromasterServiceClient(protocol, protocol);
  }

  private deserialize<T>(encodedData: string, base: T): T {
    const data = RetromasterService.base64ToArrayBuffer(encodedData);
    const protocol = new Thrift.TBinaryProtocol(null, false, false);
    this.tDeserializer.deserialize(protocol, data, base);
    return base;
  }

  private toPageableTForm(page: number, size: number, property: string, direction: string) {
    return new TPageable({
      page: page,
      size: size,

      sort:
        property != null
          ? [
            new TOrder({
              property: property,

              direction: direction === "desc" ? TDirection.DESC : TDirection.ASC
            })
          ]
          : null
    });
  }
}
