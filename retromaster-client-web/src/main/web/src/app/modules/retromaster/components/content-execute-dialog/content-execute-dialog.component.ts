/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { TContentExecutor } from "../../services/thrift";

@Component({
  selector: "app-content-execute-dialog",
  templateUrl: "./content-execute-dialog.component.html"
})
export class ContentExecuteDialogComponent {
  public contentExecutors: TContentExecutor[];
  public contentName: string;

  private contentExecutor: TContentExecutor;

  constructor(private dialogRef: MatDialogRef<ContentExecuteDialogComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.contentName = data.contentName;
    this.contentExecutors = data.contentExecutors.filter(c => c.enabled && c.found);
  }

  close() {
    this.dialogRef.close();
  }

  onItemClicked(contentExecutor: TContentExecutor) {
    this.contentExecutor = contentExecutor;
    this.dialogRef.close(this.contentExecutor);
  }
}
