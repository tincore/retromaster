/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { animate, state, style, transition, trigger } from "@angular/animations";
import { AfterViewInit, Component } from "@angular/core";
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { MessagesComponent } from "../../../core/components/messages/messages.component";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVBACK,
  TOOLBAR_ITEM_NAVMENU
} from "../../../core/components/toolbar/toolbar.component";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { RouterHelperService } from "../../../core/services/router-helper.service";
import { ContentExecuteDialogComponent } from "../../components/content-execute-dialog/content-execute-dialog.component";
import { RetromasterService } from "../../services/retromaster.service";
import { TContent, TContentExecutor, TContentMetadataItemSummary } from "../../services/thrift";

@Component({
  selector: "app-content-page",
  templateUrl: "./content-page.component.html",
  styleUrls: ["./content-page.component.scss"],
  animations: [
    trigger("detailExpand", [state("collapsed", style({
      height: "0px",
      minHeight: "0",
      display: "none"
    })), state("expanded", style({ height: "*" })), transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))])
  ]
})
export class ContentPageComponent implements AfterViewInit {
  toolbarItems = [TOOLBAR_ITEM_NAVMENU, TOOLBAR_ITEM_NAVBACK, TOOLBAR_ITEM_MESSAGE, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];

  displayedColumns = ["contentMetadataItemSummary", "actions"];
  error: string;
  busy: boolean;

  content: TContent;
  contentMetadataItemSummaries: TContentMetadataItemSummary[];

  private contentForm: UntypedFormGroup;

  constructor(
    private retromasterService: RetromasterService,
    private messageBusService: MessageBusService,
    private formBuilder: UntypedFormBuilder,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService
  ) {
    this.busy = true;

    this.contentForm = this.formBuilder.group({
      id: new UntypedFormControl({ value: "", disabled: true }, []),
      volumeId: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),

      directoryPath: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),
      fileName: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),
      fileEntry: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),
      md5: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),
      sha1: new UntypedFormControl({ value: "", disabled: true }, []),
      crc: new UntypedFormControl({ value: "", disabled: true }, []),
      size: new UntypedFormControl({ value: "", disabled: true }, [Validators.required]),

      modifiedTime: new UntypedFormControl({ value: "", disabled: true }, []),
      enrichTime: new UntypedFormControl({ value: "", disabled: true }, []),
      checkTime: new UntypedFormControl({ value: "", disabled: true }, []),
      fileSize: new UntypedFormControl({ value: "", disabled: true }, [Validators.required])
    });
  }

  isBusy() {
    return this.busy;
  }

  ngAfterViewInit() {
    const id = this.activatedRoute.snapshot.paramMap.get("id");

    this.retromasterService.findContentById(id).subscribe(
      v => {
        this.content = v;
        this.contentForm.setValue(v);

        this.busy = false;
      },
      e => this.handleError(e)
    );

    this.retromasterService.findContentMetadataItemSummariesByContentId(id).subscribe(
      v => {
        this.contentMetadataItemSummaries = v;
      },
      e => this.handleError(e)
    );
  }

  onPlayClicked(contentMetadataItemSummary: TContentMetadataItemSummary) {
    this.doContentMetadataItemExecuteDialogDisplay(contentMetadataItemSummary.id);
  }

  onToolbarAction(action) {
    if (action === TOOLBAR_ITEM_NAVBACK.action) {
      this.routerHelperService.doBack();
    }
  }

  onFormSubmitted() {
    // UNIMPLEMENTED!!
  }

  private doContentMetadataItemExecuteDialogDisplay(contentMetadataItemId: string) {
    this.retromasterService.findContentExecutorsByContentMetadataItemId(contentMetadataItemId).subscribe(
      v => {
        this.openContentExecutorsDialog(contentMetadataItemId, v);
      },
      e => this.handleError(e)
    );
  }

  private handleError(e) {
    console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
    this.messageBusService.publish("serviceError", e);
  }

  private openContentExecutorsDialog(contentMetadataItemId: string, contentExecutors: TContentExecutor[]) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      contentExecutors: contentExecutors
    };
    const dialogRef = this.dialog.open(ContentExecuteDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      dr => {
        console.log("dialogResult", dr);
        if (dr) {
          // this.busy = true;
          this.retromasterService.doContentMetadataItemExecute(contentMetadataItemId, dr.id).subscribe(() => {
          });
          this.dialog.open(MessagesComponent, new MatDialogConfig());
        }
      },
      e => this.handleError(e)
    );
  }
}
