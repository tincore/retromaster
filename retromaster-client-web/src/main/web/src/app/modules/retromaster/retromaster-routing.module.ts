/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { ContentMetadataSetsPageComponent } from './pages/content-metadata-sets-page/content-metadata-sets-page.component';
import { ContentMetadataSetPageComponent } from './pages/content-metadata-set-page/content-metadata-set-page.component';
import { ContentsPageComponent } from './pages/contents-page/contents-page.component';
import { ContentPageComponent } from './pages/content-page/content-page.component';
import { ContentDirectoryPathsPageComponent } from './pages/content-directory-paths-page/content-directory-paths-page.component';
import { ContentDirectoryPathPageComponent } from './pages/content-directory-path-page/content-directory-path-page.component';
import { ContentVolumesPageComponent } from './pages/content-volumes-page/content-volumes-page.component';
import { ContentExecutorsPageComponent } from './pages/content-executors-page/content-executors-page.component';
import { ContentMetadataPageComponent } from './pages/content-metadata-page/content-metadata-page.component';
import { ContentVolumePageComponent } from './pages/content-volume-page/content-volume-page.component';
import { AuthPageComponent } from './pages/auth-page/auth-page.component';

const routes: Routes = [
  { path: 'auth', component: AuthPageComponent },
  { path: '', component: SearchPageComponent },
  { path: 'home', component: SearchPageComponent },
  { path: 'search', component: SearchPageComponent },
  { path: 'queryResult/content/:id', component: ContentPageComponent },
  {
    path: 'queryResult/contentMetadata/:id',
    component: ContentMetadataPageComponent,
  },
  { path: 'content', component: ContentsPageComponent },
  {
    path: 'contentDirectoryPath',
    component: ContentDirectoryPathsPageComponent,
  },
  {
    path: 'contentDirectoryPath/:directoryPath',
    component: ContentDirectoryPathPageComponent,
  },
  {
    path: 'contentDirectoryPath/:directoryPath/content/:id',
    component: ContentPageComponent,
  },
  { path: 'content/:id', component: ContentPageComponent },
  { path: 'contentExecutor', component: ContentExecutorsPageComponent },
  { path: 'contentMetadataSet', component: ContentMetadataSetsPageComponent },
  {
    path: 'contentMetadataSet/:id',
    component: ContentMetadataSetPageComponent,
  },
  { path: 'contentMetadata/:id', component: ContentMetadataPageComponent },
  { path: 'contentVolume', component: ContentVolumesPageComponent },
  { path: 'contentVolume/:id', component: ContentVolumePageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RetromasterRoutingModule {}
