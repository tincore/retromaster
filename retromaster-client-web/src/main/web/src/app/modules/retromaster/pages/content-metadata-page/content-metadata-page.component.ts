/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* tslint:disable:max-line-length */
import { animate, state, style, transition, trigger } from "@angular/animations";
import { BreakpointObserver } from "@angular/cdk/layout";
import { AfterViewInit, Component, DoCheck, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup } from "@angular/forms";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AppConfig } from "../../../../app.config";
import { MessagesComponent } from "../../../core/components/messages/messages.component";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVBACK,
  TOOLBAR_ITEM_NAVMENU,
  ToolbarItem,
  ToolbarItemLocation
} from "../../../core/components/toolbar/toolbar.component";
import { ApplicationMessagesService } from "../../../core/services/application-messages.service";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { RouterHelperService } from "../../../core/services/router-helper.service";
import { ContentExecuteDialogComponent } from "../../components/content-execute-dialog/content-execute-dialog.component";
import { RetromasterService } from "../../services/retromaster.service";
import {
  TContentExecutor,
  TContentMediaType,
  TContentMetadata,
  TContentMetadataItem,
  TContentMetadataMedia,
  TContentMetadataSet
} from "../../services/thrift";
import { PageStateService, TableState } from "../../services/page-state.service";
import { GalleryImage } from "../../../core/components/gallery/gallery.component";

const PAGE_STATE_ID = "contentMetadataPageState";

interface PageState extends TableState {
}

@Component({
  selector: "app-content-metadata-page",
  templateUrl: "./content-metadata-page.component.html",
  styleUrls: ["./content-metadata-page.component.scss"],
  animations: [
    trigger("detailExpand", [state("collapsed", style({
      height: "0px",
      minHeight: "0",
      display: "none"
    })), state("expanded", style({ height: "*" })), transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))])
  ]
})
export class ContentMetadataPageComponent implements AfterViewInit, OnInit, OnDestroy, DoCheck {
  toolbarItemSave = new ToolbarItem("save", "save", "save", true, ToolbarItemLocation.RIGHT);
  toolbarItemScanContents = new ToolbarItem("scanContents", "cached", "scanContents", false, ToolbarItemLocation.MENU);
  toolbarItems = [
    TOOLBAR_ITEM_NAVMENU,
    TOOLBAR_ITEM_NAVBACK,
    // TOOLBAR_ITEM_DETAILS,
    this.toolbarItemSave, TOOLBAR_ITEM_MESSAGE,
    this.toolbarItemScanContents,
    TOOLBAR_ITEM_ABOUT,
    TOOLBAR_ITEM_LOGOUT
  ];

  @ViewChild("form") formElement;
  @ViewChild("formSubmit") formSubmitElement;
  contentMetadataForm: UntypedFormGroup;

  mediaTypes = Object.values(TContentMediaType).filter(Number);

  displayedColumns = ["contentMetadataItem", "actions"];

  editableFields = [
    "name",
    "description",
    "title",
    "description",
    "publisher",
    "system",
    "date",
    "region",
    "language",
    "mediaType",
    "mediaOrder",
    "alternate",
    "alternateData",
    "badDump",
    "badDumpData",
    "cracked",
    "crackedData",
    "fixed",
    "fixedData",
    "modified",
    "modifiedData",
    "hacked",
    "hackedData",
    "translated",
    "translatedData",
    "trained",
    "trainedData",
    "pirated",
    "piratedData",
    "virus",
    "virusData",
    "verified",
    "tagsExtra"
  ];

  error: string;
  busy: boolean;

  contentMetadata: TContentMetadata;
  contentMetadataSet: TContentMetadataSet;
  contentMetadataMedias: TContentMetadataMedia[] = [];
  totalElements: number;

  private contentMetadataItemIdContentIdMap;
  private saving: boolean;
  private pageState: PageState;

  constructor(
    private retromasterService: RetromasterService,
    private messageBusService: MessageBusService,
    private applicationMessagesService: ApplicationMessagesService,
    private pageStateService: PageStateService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private formBuilder: UntypedFormBuilder,
    private breakpointObserver: BreakpointObserver,
    private translateService: TranslateService
  ) {
    this.busy = true;

    breakpointObserver.observe(["(max-width: 1279px)"]).subscribe(result => {
      this.displayedColumns = result.matches ? ["contentMetadataItem", "actions"] : ["name", "sha1", "md5", "crc", "size", "actions"];
    });
    this.contentMetadataForm = this.formBuilder.group({
      id: new UntypedFormControl({ value: "", disabled: true }, []),
      contentMetadataSetId: new UntypedFormControl({ value: "", disabled: true }, []),
      name: new UntypedFormControl({ value: "", disabled: true }, []),
      description: new UntypedFormControl({ value: "", disabled: true }, []),

      items: new UntypedFormControl({ value: "", disabled: true }, []),

      title: new UntypedFormControl({ value: "", disabled: true }, []),
      publisher: new UntypedFormControl({ value: "", disabled: true }, []),
      system: new UntypedFormControl({ value: "", disabled: true }, []),

      date: new UntypedFormControl({ value: "", disabled: true }, []),
      region: new UntypedFormControl({ value: "", disabled: true }, []),
      language: new UntypedFormControl({ value: "", disabled: true }, []),
      mediaType: new UntypedFormControl({ value: "", disabled: true }, []),
      mediaOrder: new UntypedFormControl({ value: "", disabled: true }, []),
      source: new UntypedFormControl({ value: "", disabled: true }, []),
      //
      alternate: new UntypedFormControl({ value: "", disabled: true }, []),
      alternateData: new UntypedFormControl({ value: "", disabled: true }, []),
      badDump: new UntypedFormControl({ value: "", disabled: true }, []),
      badDumpData: new UntypedFormControl({ value: "", disabled: true }, []),
      cracked: new UntypedFormControl({ value: "", disabled: true }, []),
      crackedData: new UntypedFormControl({ value: "", disabled: true }, []),
      fixed: new UntypedFormControl({ value: "", disabled: true }, []),
      fixedData: new UntypedFormControl({ value: "", disabled: true }, []),
      modified: new UntypedFormControl({ value: "", disabled: true }, []),
      modifiedData: new UntypedFormControl({ value: "", disabled: true }, []),
      hacked: new UntypedFormControl({ value: "", disabled: true }, []),
      hackedData: new UntypedFormControl({ value: "", disabled: true }, []),
      translated: new UntypedFormControl({ value: "", disabled: true }, []),
      translatedData: new UntypedFormControl({ value: "", disabled: true }, []),
      trained: new UntypedFormControl({ value: "", disabled: true }, []),
      trainedData: new UntypedFormControl({ value: "", disabled: true }, []),
      pirated: new UntypedFormControl({ value: "", disabled: true }, []),
      piratedData: new UntypedFormControl({ value: "", disabled: true }, []),
      virus: new UntypedFormControl({ value: "", disabled: true }, []),
      virusData: new UntypedFormControl({ value: "", disabled: true }, []),
      verified: new UntypedFormControl({ value: "", disabled: true }, []),
      tagsExtra: new UntypedFormControl({ value: "", disabled: true }, []),

      staging: new UntypedFormControl({ value: "", disabled: true }, []),
      unidentified: new UntypedFormControl({ value: "", disabled: true }, []),
      complete: new UntypedFormControl({ value: "", disabled: true }, [])
    });
  }

  ngOnInit() {
    this.pageState = this.pageStateService.getAndClear(PAGE_STATE_ID, {
      // displayDetails: false
    }) as PageState;
  }

  ngAfterViewInit() {
    const contentMetadataId = this.activatedRoute.snapshot.paramMap.get("id");

    this.findContentMetadataById(contentMetadataId);
  }

  ngDoCheck() {
    this.toolbarItemSave.disabled = !this.formElement
      || !this.contentMetadataForm.valid
      // || !this.pageState.displayDetails
      || !this.isEditable();
  }

  ngOnDestroy(): void {
    this.pageStateService.set(PAGE_STATE_ID, this.pageState);
  }


  onCellClicked(contentMetadataItem: TContentMetadataItem, event) {
    if (event.view.getSelection().type !== "Range") {
      console.log("onContentMetadataItemCellClicked", contentMetadataItem);
    }
  }

  getTitleSub() {
    let title = "";
    if (this.contentMetadataSet) {
      title += this.contentMetadataSet.name;
    }
    return title;
  }

  getTitle() {
    let title = "";

    if (this.contentMetadata) {
      title += this.contentMetadata.title;
    }

    if (this.isEditable()) {
      title += " [S]";
    }
    return title;
  }

  onFormSubmitted() {
    this.doSave();
  }

  onPlayClicked(contentMetadataItem: TContentMetadataItem) {
    this.doContentMetadataItemExecuteDialogDisplay(contentMetadataItem);
  }

  onPlayMainClicked() {
    if (this.contentMetadataItemIdContentIdMap != null && this.contentMetadata.items) {
      const executableItems = this.contentMetadata.items.filter(i => this.contentMetadataItemIdContentIdMap[i.id]);
      if (executableItems.length > 0) {
        this.doContentMetadataItemExecuteDialogDisplay(executableItems[0]);
      }
    }
  }


  isPlayable(contentMetadataItem: TContentMetadataItem) {
    if (this.contentMetadataItemIdContentIdMap != null) {
      return this.contentMetadataItemIdContentIdMap[contentMetadataItem.id] != null;
    }

    return false;
  }

  isPlayableMain(): boolean {
    return this.contentMetadataItemIdContentIdMap != null && Object.keys(this.contentMetadataItemIdContentIdMap).length > 0;
  }

  onToolbarAction(action) {
    if (action === TOOLBAR_ITEM_NAVBACK.action) {
      this.routerHelperService.doBack();
      // } else if (action === TOOLBAR_ITEM_DETAILS.action) {
      //   this.toggleDisplayDetails();
    } else if (this.toolbarItemSave.action === action) {
      this.formSubmitElement.nativeElement.click();
    } else if (this.toolbarItemScanContents.action === action) {
      this.retromasterService.doContentScanByContentMetadataId(this.contentMetadata.id).subscribe(
        c => {
          this.snackBar.open(this.translateService.instant("contentScanRequested"), "", {
            duration: AppConfig.snackBarDuration
          });
        },
        (r: any) => {
          console.log("error", r, typeof r);
          this.saving = false;
          if (r.code && r.message) {
            this.error = r.message;
          }
        }
      );
    }
  }

  // isDisplayDetails() {
  //   return this.pageState.displayDetails;
  // }

  getGalleryImages(): GalleryImage[] {
    return this.contentMetadataMedias
      .map((m, i) => ({
        position: i,
        alt: "",
        src: `rest/contentSignature/${m.contentSignature}`
      }));
  }

  private doContentMetadataItemExecuteDialogDisplay(contentMetadataItem: TContentMetadataItem) {
    this.retromasterService.findContentExecutorsByContentMetadataItemId(contentMetadataItem.id).subscribe(
      v => {
        this.openContentExecutorsDialog(contentMetadataItem, v);
      },
      e => this.onError(e)
    );
  }


  private doSave() {
    if (this.saving) {
      return;
    }
    if (this.contentMetadataForm.valid) {
      this.saving = true;
      const formValue = this.contentMetadataForm.value;
      const contentMetadata = new TContentMetadata({
        ...formValue,
        id: this.contentMetadata.id,
        contentMetadataSetId: this.contentMetadata.contentMetadataSetId,
        staging: this.contentMetadata.staging
      });

      console.log("save", contentMetadata);
      this.retromasterService.saveContentMetadata(contentMetadata).subscribe(
        c => {
          this.saving = false;
          this.onSaveSuccessful();
        },
        (r: any) => {
          console.log("error", r, typeof r);
          this.saving = false;
          if (r.code && r.message) {
            this.error = r.message;
          }
        }
      );
    }
  }

  private findContentMetadataById(id) {
    this.retromasterService.findContentMetadataMediasByContentMetadataId(id).subscribe(v => this.contentMetadataMedias = v);
    this.retromasterService.findContentMetadataById(id).subscribe(v => {
      this.onContentMetadata(v);
    });
  }

  private onError(e) {
    console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
    this.messageBusService.publish("serviceError", e);
  }

  private isEditable(contentMetadata = this.contentMetadata) {
    return contentMetadata && contentMetadata.staging;
  }

  private onContentMetadata(contentMetadata: TContentMetadata) {
    this.contentMetadata = contentMetadata;
    this.contentMetadataForm.setValue(contentMetadata);
    if (this.isEditable()) {
      this.editableFields.forEach(f => this.contentMetadataForm.get(f).enable());
    } else {
      this.editableFields.forEach(f => this.contentMetadataForm.get(f).disable());
    }

    if (contentMetadata.contentMetadataSetId) {
      this.retromasterService.findContentMetadataSetById(contentMetadata.contentMetadataSetId).subscribe(v => {
        this.contentMetadataSet = v;
      });
    }

    this.retromasterService.findContentIdsByContentMetadataId(contentMetadata.id).subscribe(map => {
      this.contentMetadataItemIdContentIdMap = map;
    });

    this.busy = false;
  }

  private onSaveSuccessful() {
    this.snackBar.open(this.translateService.instant("saved"), "", {
      duration: AppConfig.snackBarDuration
    });
    this.findContentMetadataById(this.contentMetadata.id);
  }

  private openContentExecutorsDialog(contentMetadataItem: TContentMetadataItem, contentExecutors: TContentExecutor[]) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      contentName: contentMetadataItem.name,
      contentExecutors: contentExecutors
    };
    this.dialog.open(ContentExecuteDialogComponent, dialogConfig)
      .afterClosed().subscribe(
      dr => {
        if (dr) {
          // this.busy = true;
          this.retromasterService.doContentMetadataItemExecute(contentMetadataItem.id, dr.id).subscribe(() => {
          });
          this.dialog.open(MessagesComponent, new MatDialogConfig());
        }
      },
      e => this.onError(e)
    );
  }
}
