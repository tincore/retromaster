/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* tslint:disable:max-line-length */
import { animate, state, style, transition, trigger } from "@angular/animations";
import { AfterViewChecked, AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { Router } from "@angular/router";
import { fromEvent, merge } from "rxjs";
import { debounceTime, distinctUntilChanged, startWith, tap } from "rxjs/operators";
import { AppConfig } from "../../../../app.config";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVMENU,
  ToolbarItem
} from "../../../core/components/toolbar/toolbar.component";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { PageStateService, TableState } from "../../services/page-state.service";
import { RetromasterService } from "../../services/retromaster.service";
import { TContentDirectoryPath } from "../../services/thrift";

const PAGE_STATE_ID = "contentDirectoryPathsPageState";

interface PageState extends TableState {
  scrollTop: number;
  query: string,
}

@Component({
  selector: "app-content-directory-paths-page",
  templateUrl: "./content-directory-paths-page.component.html",
  styleUrls: ["./content-directory-paths-page.component.scss"],
  animations: [
    trigger("detailExpand", [state("collapsed", style({
      height: "0px",
      minHeight: "0",
      display: "none"
    })), state("expanded", style({ height: "*" })), transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))])
  ]
})
export class ContentDirectoryPathsPageComponent implements OnInit, OnDestroy, AfterViewInit, AfterViewChecked {
  toolbarItems: ToolbarItem[] = [TOOLBAR_ITEM_NAVMENU, TOOLBAR_ITEM_MESSAGE, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];
  displayedColumns: string[] = ["id"];
  error: string;
  busy: boolean;

  contentDirectoryPaths: TContentDirectoryPath[];

  totalElements: number;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild("queryInput", { static: true }) queryInput: ElementRef;
  @ViewChild("mainScroll", { static: true }) private mainScroll: ElementRef<HTMLElement>;

  private pageState: PageState;
  private pageStateRestored: boolean;

  constructor(private retromasterService: RetromasterService, private pageStateService: PageStateService, private messageBusService: MessageBusService, private dialog: MatDialog, private router: Router) {
    this.busy = true;
  }

  onQueryClearClicked() {
    this.queryInput.nativeElement.value = "";
    this.findContentDirectoryPathsPaged();
  }

  isBusy() {
    return this.busy;
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    this.mainScroll.nativeElement.addEventListener("scroll", e => {
      this.pageState.scrollTop = this.mainScroll.nativeElement.scrollTop;
    });

    fromEvent(this.queryInput.nativeElement, "keyup")
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(() => {
        const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;
        if (this.pageState?.query != query) {
          this.paginator.firstPage();
          this.findContentDirectoryPathsPaged();
        }
      });

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        tap(() => this.findContentDirectoryPathsPaged())
      )
      .subscribe();
  }

  ngOnInit() {
    this.busy = true;

    this.pageState = this.pageStateService.getAndClear(PAGE_STATE_ID, {
      scrollTop: 0,
      query: null
    }) as PageState;

    this.queryInput.nativeElement.value = this.pageState.query;
    this.pageStateService.restoreTableState(this.pageState, this.paginator, this.sort);
    this.pageStateRestored = this.pageState.scrollTop == 0;

    if (this.queryInput.nativeElement.value != null) {
      this.findContentDirectoryPathsPaged();
    }
  }

  ngAfterViewChecked(): void {
    if (!this.pageStateRestored && this.contentDirectoryPaths) {
      this.mainScroll.nativeElement.scrollTop = this.pageState.scrollTop;
      this.pageStateRestored = true;
    }
  }

  ngOnDestroy(): void {
    this.pageStateService.set(PAGE_STATE_ID, this.pageState);
  }

  onCellClicked(contentDirectoryPath: TContentDirectoryPath, event) {
    if (event.view.getSelection().type !== "Range") {
      this.router.navigate([`${AppConfig.routes.retromaster}/contentDirectoryPath/${contentDirectoryPath.id}`]);
    }
  }

  onToolbarAction(action): void {
  }

  private findContentDirectoryPathsPaged() {
    const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;

    this.pageState = {
      ...this.pageState,
      ...(this.pageStateService.toTableState(this.paginator, this.sort)),
      query
    };

    this.busy = true;
    let formattedQuery = query;
    if (query && query.indexOf("%") === -1) {
      formattedQuery = "%" + query + "%";
    }
    if (this.retromasterService === undefined) {
      return;
    }
    this.retromasterService.findContentDirectoryPathsByFilter(formattedQuery, this.paginator.pageIndex, this.paginator.pageSize, this.sort.active, this.sort.direction).subscribe(
      d => {
        this.busy = false;
        this.totalElements = d.totalElements;
        this.error = null;
        this.contentDirectoryPaths = d.content;
      },
      e => {
        console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
        this.messageBusService.publish("serviceError", e);
      }
    );
  }
}
