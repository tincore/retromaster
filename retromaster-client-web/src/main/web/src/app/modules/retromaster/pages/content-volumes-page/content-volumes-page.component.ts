/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { animate, state, style, transition, trigger } from "@angular/animations";
import { AfterViewInit, Component } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { AppConfig } from "../../../../app.config";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVMENU,
  ToolbarItem,
  ToolbarItemLocation
} from "../../../core/components/toolbar/toolbar.component";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { RetromasterService } from "../../services/retromaster.service";
import { TContentVolume, TContentVolumeEnvironment } from "../../services/thrift";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-content-volumes-page",
  templateUrl: "./content-volumes-page.component.html",
  styleUrls: ["./content-volumes-page.component.scss"],
  animations: [
    trigger("detailExpand", [state("collapsed", style({
      height: "0px",
      minHeight: "0",
      display: "none"
    })), state("expanded", style({ height: "*" })), transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))])
  ]
})
export class ContentVolumesPageComponent implements AfterViewInit {
  toolbarItemReindex = new ToolbarItem("reindex", "fingerprint", "contentReindex", false, ToolbarItemLocation.MENU);
  toolbarItemContentReorganize = new ToolbarItem("contentReorganize", "sync", "contentReorganize", false, ToolbarItemLocation.MENU);
  toolbarItems = [TOOLBAR_ITEM_NAVMENU, TOOLBAR_ITEM_MESSAGE, this.toolbarItemReindex, this.toolbarItemContentReorganize, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];
  contentVolumeDisplayedColumns = ["name", "path", "actions"];
  error: string;
  busy: boolean;

  contentVolumes: TContentVolume[];

  constructor(
    private retromasterService: RetromasterService,
    private messageBusService: MessageBusService,
    private snackBar: MatSnackBar,
    private translateService: TranslateService,
    private router: Router
  ) {
    this.busy = true;
  }

  isBusy() {
    return this.busy;
  }

  public isContentVolumeScanEnabled(contentVolume: TContentVolume) {
    return contentVolume.environment === TContentVolumeEnvironment.STAGE;
  }

  ngAfterViewInit() {
    this.findContentVolumes();
  }

  public onRowContentVolumeScanClicked(contentVolume: TContentVolume) {
    this.retromasterService.doContentScanByContentVolumeStaging().subscribe(
      d => {
        this.snackBar.open("contentStagingScanRequested", "", {
          duration: AppConfig.snackBarDuration
        });
      },
      e => this.onError(e)
    );
  }

  onToolbarAction(action) {
    console.log("onToolbarAction", action);
    if (this.toolbarItemReindex.action === action) {
      this.retromasterService.doReindexRequest().subscribe(
        c => {
          this.onReindexRequestSuccessful();
        },
        e => this.onError(e)
      );
    } else if (this.toolbarItemContentReorganize.action === action) {
      this.retromasterService.doContentReorganizeRequest().subscribe(
        c => {
          this.onContentReorganizeRequestSuccessful();
        },
        e => this.onError(e)
      );
    }
  }

  onCellClicked(contentVolume: TContentVolume, event) {
    if (event.view.getSelection().type !== "Range") {
      this.router.navigate([`${AppConfig.routes.retromaster}/contentVolume/${contentVolume.id}`]);
    }
  }

  private findContentVolumes() {
    this.busy = true;
    if (this.retromasterService === undefined) {
      return;
    }
    this.retromasterService.findContentVolumes().subscribe(
      d => {
        this.busy = false;
        this.error = null;
        this.contentVolumes = d;
      },
      e => this.onError(e)
    );
  }

  private onError(e) {
    console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
    this.messageBusService.publish("serviceError", e);
  }

  private onReindexRequestSuccessful() {
    this.snackBar.open(this.translateService.instant("reindexRequested"), "", {
      duration: AppConfig.snackBarDuration
    });
  }

  private onContentReorganizeRequestSuccessful() {
    this.snackBar.open(this.translateService.instant("contentReorganizeRequested"), "", {
      duration: AppConfig.snackBarDuration
    });
  }

}
