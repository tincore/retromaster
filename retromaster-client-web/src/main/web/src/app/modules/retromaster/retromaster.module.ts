/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { NgModule } from "@angular/core";
import { MaterialModule } from "../material.module";
import { TranslateModule } from "@ngx-translate/core";
import { CommonModule } from "@angular/common";
import { ContentMetadataSetsPageComponent } from "./pages/content-metadata-sets-page/content-metadata-sets-page.component";
import { SearchPageComponent } from "./pages/search-page/search-page.component";
import { ContentMetadataSetPageComponent } from "./pages/content-metadata-set-page/content-metadata-set-page.component";
import { CoreModule } from "../core/core.module";
import { RetromasterRoutingModule } from "./retromaster-routing.module";
// import { NgxMaterialTimepickerModule } from "ngx-material-timepicker";
import { ContentsPageComponent } from "./pages/contents-page/contents-page.component";
import { ContentPageComponent } from "./pages/content-page/content-page.component";
import { ContentExecuteDialogComponent } from "./components/content-execute-dialog/content-execute-dialog.component";
import { ContentDirectoryPathPageComponent } from "./pages/content-directory-path-page/content-directory-path-page.component";
import { ContentDirectoryPathsPageComponent } from "./pages/content-directory-paths-page/content-directory-paths-page.component";
import { ContentVolumesPageComponent } from "./pages/content-volumes-page/content-volumes-page.component";
import { ContentExecutorsPageComponent } from "./pages/content-executors-page/content-executors-page.component";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { ContentMetadataPageComponent } from "./pages/content-metadata-page/content-metadata-page.component";
import { ContentVolumePageComponent } from "./pages/content-volume-page/content-volume-page.component";
import { AuthPageComponent } from "./pages/auth-page/auth-page.component";
import { MatPaginatorIntl } from "@angular/material/paginator";
import { MatPaginatorCompactIntl } from "../core/components/mat-paginator-compact/mat-paginator-compact-intl";
import { MatChipsModule } from "@angular/material/chips";
import { LazyImgDirective } from "../core/shared/LazyImgDirective";
import { MatTabsModule } from "@angular/material/tabs";

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    MaterialModule,
    CoreModule,
    ScrollingModule,
    RetromasterRoutingModule,
    MatChipsModule,
    MatTabsModule
    // NgxMaterialTimepickerModule
  ],
  declarations: [
    SearchPageComponent,
    ContentExecuteDialogComponent,
    ContentPageComponent,
    ContentsPageComponent,
    ContentDirectoryPathPageComponent,
    ContentDirectoryPathsPageComponent,
    ContentMetadataSetsPageComponent,
    ContentMetadataSetPageComponent,
    ContentMetadataPageComponent,
    ContentExecutorsPageComponent,
    ContentVolumesPageComponent,
    ContentVolumePageComponent,
    AuthPageComponent,
    LazyImgDirective,
  ],
  exports: [],
  providers: [
    { provide: MatPaginatorIntl, useValue: MatPaginatorCompactIntl() }
  ]
})
export class RetromasterModule {
}
