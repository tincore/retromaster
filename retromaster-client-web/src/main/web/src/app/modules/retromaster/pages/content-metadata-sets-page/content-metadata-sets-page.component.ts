/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* tslint:disable:max-line-length */
import { animate, state, style, transition, trigger } from "@angular/animations";
import { BreakpointObserver } from "@angular/cdk/layout";
import { AfterViewChecked, AfterViewInit, Component, DoCheck, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatSort } from "@angular/material/sort";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { combineLatest, fromEvent, merge } from "rxjs";
import { debounceTime, distinctUntilChanged, startWith } from "rxjs/operators";
import { AppConfig } from "../../../../app.config";
import { ConfirmDeleteDialogComponent } from "../../../core/components/confirm-delete-dialog/confirm-delete-dialog.component";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVMENU,
  ToolbarItem,
  ToolbarItemLocation
} from "../../../core/components/toolbar/toolbar.component";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { PageStateService, TableState } from "../../services/page-state.service";
import { RetromasterService } from "../../services/retromaster.service";
import { TContentMetadataSet } from "../../services/thrift";
import { SelectionModel } from "@angular/cdk/collections";

const PAGE_STATE_ID = "contentMetadataSetsPageState";

interface PageState extends TableState {
  scrollTop: number;
  query: string;
}

@Component({
  selector: "app-content-metadata-sets-page",
  templateUrl: "./content-metadata-sets-page.component.html",
  styleUrls: ["./content-metadata-sets-page.component.scss"],
  animations: [
    trigger("detailExpand", [state("collapsed", style({
      height: "0px",
      minHeight: "0",
      display: "none"
    })), state("expanded", style({ height: "*" })), transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))])
  ]
})
export class ContentMetadataSetsPageComponent implements OnInit, OnDestroy, AfterViewInit, AfterViewChecked, DoCheck {
  toolbarItemDelete = new ToolbarItem("delete", "delete", "delete", true, ToolbarItemLocation.RIGHT);
  toolbarItemScanContents = new ToolbarItem("scanContents", "cached", "scanContents", true, ToolbarItemLocation.RIGHT);
  toolbarItems = [TOOLBAR_ITEM_NAVMENU, this.toolbarItemScanContents, this.toolbarItemDelete, TOOLBAR_ITEM_MESSAGE, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];

  error: string;
  busy: boolean;
  contentMetadataSets: TContentMetadataSet[];
  totalElements: number;
  selection = new SelectionModel<TContentMetadataSet>(true, []);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild("queryInput", { static: true }) queryInput: ElementRef;
  @ViewChild("mainScroll", { static: true }) private mainScroll: ElementRef<HTMLElement>;

  private readonly DISPLAYED_COLUMNS_LONG = ["select", "name", "repositoryPath", "version", "category", "staging"];

  displayedColumns = this.DISPLAYED_COLUMNS_LONG;

  private readonly DISPLAYED_COLUMNS_SHORT = ["select", "contentMetadataSet"];

  private pageState: PageState;
  private pageStateRestored: boolean;

  constructor(
    private retromasterService: RetromasterService,
    private pageStateService: PageStateService,
    private messageBusService: MessageBusService,
    private dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar,
    private breakpointObserver: BreakpointObserver,
    private translateService: TranslateService
  ) {
    this.busy = true;

    breakpointObserver.observe(["(max-width: 1279px)"]).subscribe(result => {
      this.displayedColumns = result.matches ? this.DISPLAYED_COLUMNS_SHORT : this.DISPLAYED_COLUMNS_LONG;
    });
  }

  ngDoCheck() {
    this.toolbarItemDelete.disabled = this.selection.isEmpty();
    this.toolbarItemScanContents.disabled = this.selection.isEmpty();
  }

  onQueryClearClicked() {
    this.queryInput.nativeElement.value = "";
    this.findContentMetadataSetsPaged();
  }

  onSelectAllChanged() {
    if (this.isSelectAll()) {
      this.selection.clear();
    } else {
      this.contentMetadataSets.forEach(e => this.selection.select(e));
    }
  }

  isSelectAll() {
    const selectedCount = this.selection.selected.length;
    const itemCount = this.contentMetadataSets.length;
    return selectedCount === itemCount;
  }

  isBusy() {
    return this.busy;
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    this.mainScroll.nativeElement.addEventListener("scroll", e => {
      this.pageState.scrollTop = this.mainScroll.nativeElement.scrollTop;
    });

    fromEvent(this.queryInput.nativeElement, "keyup")
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(() => {
        const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;
        if (this.pageState?.query != query) {
          this.paginator.firstPage();
          this.findContentMetadataSetsPaged();
        }
      });

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({})
      )
      .subscribe(() => this.findContentMetadataSetsPaged());
  }

  ngAfterViewChecked(): void {
    if (!this.pageStateRestored && this.contentMetadataSets) {
      this.mainScroll.nativeElement.scrollTop = this.pageState.scrollTop;
      this.pageStateRestored = true;
    }
  }

  ngOnInit() {
    this.pageState = this.pageStateService.getAndClear(PAGE_STATE_ID, {
      scrollTop: 0,
      query: null
    }) as PageState;
    this.queryInput.nativeElement.value = this.pageState.query;
    this.pageStateRestored = this.pageState.scrollTop == 0;
    this.pageStateService.restoreTableState(this.pageState, this.paginator, this.sort);
  }

  ngOnDestroy(): void {
    this.pageStateService.set(PAGE_STATE_ID, this.pageState);
  }

  onCellClicked(contentMetadataSet: TContentMetadataSet, event) {
    if (event.view.getSelection().type !== "Range") {
      this.router.navigate([`${AppConfig.routes.retromaster}/contentMetadataSet/${contentMetadataSet.id}`]);
    }
  }

  onDeleteClicked() {
    let selectedItems = this.selection.selected;

    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { count: selectedItems.length };

    this.dialog.open(ConfirmDeleteDialogComponent, dialogConfig)
      .afterClosed()
      .subscribe(dr => {
        if (dr) {
          this.busy = true;
          combineLatest(selectedItems.map(e => this.retromasterService.deleteContentMetadataSetById(e.id)))
            .subscribe(n => {
                this.busy = false;
                this.onDeleteSuccess(selectedItems.length);
              },
              e => {
                this.busy = false;
                this.onDeleteFailed(e);
              }
            );
        }
      });
  }

  onToolbarAction(action) {
    if (this.toolbarItemDelete.action === action) {
      this.onDeleteClicked();
    } else if (this.toolbarItemScanContents.action === action) {
      this.onScanContentsClicked();
    }
  }

  private onScanContentsClicked() {
    let selectedItems = this.selection.selected;
    this.snackBar.open(this.translateService.instant("contentScanRequested"), "", {
      duration: AppConfig.snackBarDuration
    });
    this.selection.clear();
    combineLatest(selectedItems.map(e => this.retromasterService.doContentScanByContentMetadataSetId(e.id)))
      .subscribe(n => {
          this.busy = false;
          this.snackBar.open(this.translateService.instant("contentScanRequestAccepted"), "", {
            duration: AppConfig.snackBarDuration
          });
        },
        r => {
          this.busy = false;
          this.onError(r);
        }
      );
  }

  private findContentMetadataSetsPaged() {
    const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;

    this.pageState = {
      ...this.pageState,
      ...(this.pageStateService.toTableState(this.paginator, this.sort)),
      query
    };

    let formattedQuery = query;
    if (query && query.indexOf("%") === -1) {
      formattedQuery = "%" + query + "%";
    }
    this.busy = true;
    if (this.retromasterService === undefined) {
      return;
    }
    this.selection.clear();
    this.retromasterService.findContentMetadataSetsPaged(formattedQuery, this.paginator.pageIndex, this.paginator.pageSize, this.sort.active, this.sort.direction).subscribe(
      d => {
        this.busy = false;
        this.totalElements = d.totalElements;
        this.error = null;
        this.contentMetadataSets = d.content;
      },
      e => {
        this.busy = false;
        this.onError(e);
      }
    );
  }

  private onDeleteFailed(e) {
    this.onError(e);
    this.findContentMetadataSetsPaged();
  }

  private onError(e) {
    console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
    this.messageBusService.publish("serviceError", e);
  }

  private onDeleteSuccess(count: number) {
    this.snackBar.open(this.translateService.instant("deleted", { count: count }), "", {
      duration: AppConfig.snackBarDuration
    });
    // Should eventually: trigger sort!
    this.findContentMetadataSetsPaged();
  }

}
