/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { animate, state, style, transition, trigger } from "@angular/animations";
import { AfterViewChecked, AfterViewInit, Component, DoCheck, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { AppConfig } from "../../../../app.config";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVBACK,
  TOOLBAR_ITEM_NAVMENU,
  ToolbarItem,
  ToolbarItemLocation
} from "../../../core/components/toolbar/toolbar.component";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { RouterHelperService } from "../../../core/services/router-helper.service";
import { RetromasterService } from "../../services/retromaster.service";
import { TContentVolume, TContentVolumeDirectory } from "../../services/thrift";
import { combineLatest } from "rxjs";
import { SelectionModel } from "@angular/cdk/collections";
import { TranslateService } from "@ngx-translate/core";
import { PageStateService } from "../../services/page-state.service";

interface PageState {
  scrollTop: number;
}

const PAGE_STATE_ID = "contentVolumePageState";

@Component({
  selector: "app-content-volume-page",
  templateUrl: "./content-volume-page.component.html",
  styleUrls: ["./content-volume-page.component.scss"],
  animations: [
    trigger("detailExpand", [state("collapsed", style({
      height: "0px",
      minHeight: "0",
      display: "none"
    })), state("expanded", style({ height: "*" })), transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))])
  ]
})
export class ContentVolumePageComponent implements OnInit, OnDestroy, DoCheck, AfterViewInit, AfterViewChecked {
  toolbarItemScanContents = new ToolbarItem("scanContents", "cached", "scanContents", true, ToolbarItemLocation.RIGHT);
  toolbarItems = [TOOLBAR_ITEM_NAVMENU, TOOLBAR_ITEM_NAVBACK, this.toolbarItemScanContents, TOOLBAR_ITEM_MESSAGE, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];
  contentVolumeDirectoriesDisplayedColumns = ["select", "path", "fresh", "contentMetadataSetName"];
  error: string;
  busy: boolean;
  selection = new SelectionModel<TContentVolumeDirectory>(true, []);

  contentVolume: TContentVolume;
  contentVolumeDirectories: TContentVolumeDirectory[];

  @ViewChild("mainScroll", { static: true }) private mainScroll: ElementRef<HTMLElement>;

  private pageState: PageState;
  private pageStateRestored: boolean;

  constructor(
    private retromasterService: RetromasterService,
    private messageBusService: MessageBusService,
    private snackBar: MatSnackBar,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private translateService: TranslateService,
    private pageStateService: PageStateService
  ) {
    this.busy = true;
  }

  isBusy() {
    return this.busy;
  }

  ngOnInit() {
    this.busy = true;

    this.pageState = this.pageStateService.getAndClear(PAGE_STATE_ID, {
      scrollTop: 0
    }) as PageState;

    this.pageStateRestored = this.pageState.scrollTop == 0;
  }

  ngAfterViewInit() {
    this.mainScroll.nativeElement.addEventListener("scroll", e => {
      this.pageState.scrollTop = this.mainScroll.nativeElement.scrollTop;
    });

    const contentVolumeId = this.activatedRoute.snapshot.paramMap.get("id");
    this.findContentVolume(contentVolumeId);
    this.findContentVolumeDirectories(contentVolumeId);
  }

  ngAfterViewChecked(): void {
    if (!this.pageStateRestored && this.contentVolumeDirectories) {
      this.mainScroll.nativeElement.scrollTop = this.pageState.scrollTop;
      this.pageStateRestored = true;
    }
  }

  ngDoCheck() {
    this.toolbarItemScanContents.disabled = this.selection.isEmpty();
  }

  ngOnDestroy(): void {
    this.pageStateService.set(PAGE_STATE_ID, this.pageState);
  }

  onSelectAllChanged() {
    if (this.isSelectAll()) {
      this.selection.clear();
    } else {
      this.contentVolumeDirectories.forEach(e => this.selection.select(e));
    }
  }

  isSelectAll() {
    const selectedCount = this.selection.selected.length;
    const itemCount = this.contentVolumeDirectories.length;
    return selectedCount === itemCount;
  }

  onCellClicked(directory: TContentVolumeDirectory, event) {
    if (event.view.getSelection().type !== 'Range') {
      this.router.navigate([`${AppConfig.routes.retromaster}/contentDirectoryPath/${directory.path}`]);
    }
  }

  onScanContentsClicked() {
    if (this.isSelectAll()) {
      this.retromasterService.doContentScanByContentVolumeStaging().subscribe(
        d => {
          this.snackBar.open("contentStagingScanRequested", "", {
            duration: AppConfig.snackBarDuration
          });
        },
        e => this.onError(e)
      );
      return;
    }

    let selectedItems = this.selection.selected;
    this.snackBar.open(this.translateService.instant("contentScanRequested"), "", {
      duration: AppConfig.snackBarDuration
    });
    this.selection.clear();
    combineLatest(selectedItems.map(e => this.retromasterService.doContentVolumeDirectoryScan(e)))
      .subscribe(n => {
          this.busy = false;
          this.error = null;
          this.snackBar.open(this.translateService.instant("contentScanRequestAccepted"), "", {
            duration: AppConfig.snackBarDuration
          });
        },
        e => {
          this.busy = false;
          this.onError(e);
        }
      );
  }

  onToolbarAction(action) {
    if (action === TOOLBAR_ITEM_NAVBACK.action) {
      this.routerHelperService.doBack();
    } else if (this.toolbarItemScanContents.action === action) {
      this.onScanContentsClicked();
    }
  }


  private findContentVolume(id: string) {
    this.retromasterService!.findContentVolumeById(id).subscribe(
      d => {
        this.busy = false;
        this.error = null;
        this.contentVolume = d;
      },
      e => this.onError(e)
    );
  }


  private findContentVolumeDirectories(id: string) {
    this.retromasterService!.findContentVolumeDirectoriesByContentVolumeId(id, true).subscribe(
      ds => {
        this.busy = false;
        this.error = null;
        this.contentVolumeDirectories = ds;
      },
      e => this.onError(e)
    );
  }

  private onError(e) {
    console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
    this.messageBusService.publish("serviceError", e);
  }

}
