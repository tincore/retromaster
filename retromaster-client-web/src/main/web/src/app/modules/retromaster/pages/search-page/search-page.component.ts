/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { AfterViewChecked, AfterViewInit, Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild } from "@angular/core";
// import {TQuery, TQueryFacet, TQueryFacetFilter, TQueryResult, TQueryResults} from '../../services/thrift';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSidenav } from "@angular/material/sidenav";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { fromEvent, merge } from "rxjs";
import { debounceTime, distinctUntilChanged, startWith, tap } from "rxjs/operators";
import { AppConfig } from "../../../../app.config";
import {
  TOOLBAR_ITEM_ABOUT,
  TOOLBAR_ITEM_LOGOUT,
  TOOLBAR_ITEM_MESSAGE,
  TOOLBAR_ITEM_NAVMENU,
  ToolbarItem,
  ToolbarItemLocation
} from "../../../core/components/toolbar/toolbar.component";
import { MessageBusService } from "../../../core/services/message-bus.service";
import { WebSocketService } from "../../../core/services/web-socket.service";
import { PageStateService, TableState } from "../../services/page-state.service";
import { RetromasterService } from "../../services/retromaster.service";

import {
  TContentExecutor,
  TContentMetadataComplete,
  TQuery,
  TQueryFacet,
  TQueryFacetFilter,
  TQueryFacetValue,
  TQueryResult,
  TQueryResults
} from "../../services/thrift";
import { ContentExecuteDialogComponent } from "../../components/content-execute-dialog/content-execute-dialog.component";
import { MessagesComponent } from "../../../core/components/messages/messages.component";

interface PageState extends TableState {
  query?: string;
  facetSelectionMap: {};
  facetUsed: {};
  scrollTop: number;
  sideNavOpen: boolean;
}

const PAGE_STATE_ID = "searchPageState";

@Component({
  selector: "app-search-page",
  templateUrl: "./search-page.component.html",
  styleUrls: ["./search-page.component.scss"]
})
export class SearchPageComponent implements OnInit, OnDestroy, AfterViewInit, AfterViewChecked {
  toolbarItemFilter = new ToolbarItem("filter", "filter_list", "filter", false, ToolbarItemLocation.RIGHT);
  toolbarItems = [TOOLBAR_ITEM_NAVMENU, this.toolbarItemFilter, TOOLBAR_ITEM_MESSAGE, TOOLBAR_ITEM_ABOUT, TOOLBAR_ITEM_LOGOUT];
  displayedColumns = ["description"];
  error: string;

  queryResults: TQueryResult[];
  facets: TQueryFacet[];

  facetQueryFilter = {};
  facetItemCount = {};

  totalElements: number;

  busy: boolean;
  busyRequest: boolean;


  readonly facetItemDisplayMinCount = 6;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild("queryInput", { static: true }) queryInput: ElementRef;
  @ViewChild("sidenav", { static: true }) sideNav: MatSidenav;
  @ViewChild("mainScroll", { static: true }) private mainScroll: ElementRef<HTMLElement>;

  private pageState: PageState;
  private pageStateRestored: boolean;

  constructor(
    private retromasterService: RetromasterService,
    private messageBusService: MessageBusService,
    private webSocketService: WebSocketService,
    private pageStateService: PageStateService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private translateService: TranslateService,
    private router: Router
  ) {
  }

  getFacetItemCount(facet: TQueryFacet) {
    return this.facetItemCount[facet.field];
  }

  getFacetItemSize(facet: TQueryFacet) {
    return facet.field === "complete" ? 4 : 30;
  }

  getFacetSelectedCount(facet: TQueryFacet) {
    return this.pageState.facetSelectionMap[facet.field].length;
  }

  getFacetSelectedSummary(facet: TQueryFacet) {
    const selectedCount = this.getFacetSelectedCount(facet);
    const itemCount = this.getFacetItemCount(facet);

    return selectedCount > 0 ? selectedCount + "/" + itemCount : itemCount;
  }

  getFacetValues(facet: TQueryFacet): TQueryFacetValue[] {
    return facet.values;
  }

  getFacetValuesByNotSelected(facet: TQueryFacet): TQueryFacetValue[] {
    let queryFilter = this.facetQueryFilter[facet.field];
    if (queryFilter) {
      let searchString = queryFilter.toLowerCase();
      return this.getFacetValues(facet).filter(v => !this.isFacetSelected(facet, v.value) && v.value.toLowerCase().indexOf(searchString) != -1);
    }

    return this.getFacetValues(facet)
      .filter(v => !this.isFacetSelected(facet, v.value))
      .splice(0, Math.max(0, Math.min(this.getFacetItemCount(facet), this.facetItemDisplayMinCount - this.getFacetSelectedCount(facet))));
  }

  getFacetValuesBySelected(facet: TQueryFacet): TQueryFacetValue[] {
    let queryFilter = this.facetQueryFilter[facet.field];
    if (queryFilter) {
      let searchString = queryFilter.toLowerCase();
      return this.getFacetValues(facet).filter(v => this.isFacetSelected(facet, v.value) && v.value.toLowerCase().indexOf(searchString) != -1);
    }

    return this.getFacetValues(facet).filter(v => this.isFacetSelected(facet, v.value));
  }

  getFacetItemHiddenCount(facet: TQueryFacet): number {
    return Math.max(0, this.getFacetItemCount(facet) - Math.max(this.getFacetSelectedCount(facet), this.facetItemDisplayMinCount));
  }

  getFacetSelectedTooltip(field: string) {
    return this.pageState.facetSelectionMap[field].join(", ");
  }

  getFacetValueTranslatePrefix(facet: TQueryFacet) {
    if (facet.field === "complete") {
      return "complete.";
    } else {
      return null;
    }
  }

  isBusy() {
    return this.busy;
  }

  isFacetUsed(facet: TQueryFacet) {
    return this.pageState.facetUsed[facet.field];
  }

  ngAfterViewInit() {
    this.mainScroll.nativeElement.addEventListener("scroll", e => {
      this.pageState.scrollTop = this.mainScroll.nativeElement.scrollTop;
    });

    fromEvent(this.queryInput.nativeElement, "keyup")
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(() => {
        const query = this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : null;
        if (this.pageState?.query != query) {
          this.paginator.firstPage();
          this.findQueryResultsPaged();
        }
      });
    // merge(this.sort.sortChange, this.paginator.page)
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        tap(() => {
          // console.log('pageChange', this.paginator);
          this.findQueryResultsPaged();
        })
      )
      .subscribe();
  }

  ngAfterViewChecked(): void {
    if (!this.pageStateRestored && this.queryResults) {
      this.mainScroll.nativeElement.scrollTop = this.pageState.scrollTop;
      this.pageStateRestored = true;
    }
  }

  ngOnInit() {
    this.pageState = this.pageStateService.getAndClear(PAGE_STATE_ID, {
      query: null,
      scrollTop: 0,
      facetSelectionMap: {},
      facetUsed: {},
      sideNavOpen: true
    }) as PageState;

    this.queryInput.nativeElement.value = this.pageState.query;
    this.pageStateRestored = this.pageState.scrollTop == 0;
    this.pageStateService.restoreTableState(this.pageState, this.paginator);
    this.pageState.sideNavOpen ? this.sideNav.open() : this.sideNav.close();
  }

  onCellClicked(queryResult: TQueryResult, event) {
    if (event.view.getSelection().type !== "Range") {
      this.router.navigate([`${AppConfig.routes.retromaster}/queryResult/contentMetadata/${queryResult.contentMetadataSummary.id}`]);
    }
  }
  onFacetClosed(facet: TQueryFacet) {
    this.pageState.facetUsed[facet.field] = false;
  }

  onFacetOpened(facet: TQueryFacet) {
    this.pageState.facetUsed[facet.field] = true;
  }

  onFacetShowMore(facet: TQueryFacet, facetSelectDialogTemplateRef: TemplateRef<any>) {
    let dialogRef = this.dialog.open(facetSelectDialogTemplateRef, {
      data: facet
    });
    dialogRef.afterClosed().subscribe(dr => {
      console.log("onFacetShowMore Dialog closed", dr, facet);
    });
  }

  ngOnDestroy(): void {
    this.pageStateService.set(PAGE_STATE_ID, this.pageState);
  }

  onFacetSelection(field, value) {
    let items = this.pageState.facetSelectionMap[field];

    let index = items.indexOf(value);
    if (index === -1) {
      items.push(value);
    } else {
      items.splice(index, 1);
    }
    // console.log("onFacetSelection", field, value, items);
    this.findQueryResultsPaged();
  }

  isFacetSelected(facet: TQueryFacet, value: string): boolean {
    let items = this.pageState.facetSelectionMap[facet.field];
    return items !== undefined && items.indexOf(value) >= 0;
  }

  onFacetSelectionClear($event, facet: TQueryFacet) {
    $event.stopPropagation();
    if (this.pageState.facetSelectionMap[facet.field]) {
      this.pageState.facetSelectionMap[facet.field] = [];
      this.findQueryResultsPaged();
    }
  }

  getFacetQueryFilterValue(facet: TQueryFacet) {
    return this.facetQueryFilter[facet.field];
  }

  onFacetQueryClearClicked(facet: TQueryFacet) {
    // console.log('onFacetQueryClearClicked', facet);
    this.facetQueryFilter[facet.field] = null;
  }

  onQueryClearClicked() {
    this.queryInput.nativeElement.value = "";
    this.findQueryResultsPaged();
  }

  onToolbarAction(action): void {
    console.log("onToolbarAction", action);
    if (this.toolbarItemFilter.action === action) {
      this.sideNav.toggle();
      this.pageState.sideNavOpen = this.sideNav.opened;
    }
  }

  getImageUrl(queryResult: TQueryResult) {
    return `/rest/contentMetadata/system/${queryResult.contentMetadataSummary?.system}/title/${queryResult.contentMetadataSummary?.title}/mediaType/PIC_BOXART`;
    // return `/rest/contentMetadata/${queryResult.contentMetadataSummary?.id}/mediaType/PIC_BOXART`;
  }

  isPlayable(queryResult: TQueryResult) {
    return queryResult.contentMetadataSummary.complete === TContentMetadataComplete.PARTIAL || queryResult.contentMetadataSummary.complete === TContentMetadataComplete.COMPLETE;
  }

  onPlayClicked(e, ev) {
    ev.stopPropagation();
    console.log("onPlayClicked", e, ev);
    this.retromasterService.findContentMetadataItemExecuteOptionSetByContentMetadataId(e.contentMetadataSummary.id).subscribe(
      v => {
        console.log("HEY", v);
        this.openContentExecutorsDialog(v.contentMetadataItemId, v.contentMetadataItemName, v.contentExecutors);
      },
      e => this.onError(e)
    );
    return false;
  }

  private openContentExecutorsDialog(contentMetadataItemId: string, contentName: string, contentExecutors: TContentExecutor[]) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      contentName: contentName,
      contentExecutors: contentExecutors
    };
    this.dialog.open(ContentExecuteDialogComponent, dialogConfig)
      .afterClosed().subscribe(
      dr => {
        if (dr) {
          // this.busy = true;
          this.retromasterService.doContentMetadataItemExecute(contentMetadataItemId, dr.id).subscribe(() => {
          });
          this.dialog.open(MessagesComponent, new MatDialogConfig());
        }
      },
      e => this.onError(e)
    );
  }


  private findQueryResultsPaged() {
    if (this.busy && this.queryResults != null) {
      this.busyRequest = true;
      console.log("busy...");
      return;
    }

    const query = this.queryInput.nativeElement.value;
    this.pageState = {
      ...this.pageState,
      ...(this.pageStateService.toTableState(this.paginator, null)),
      query
    };

    this.busy = true;
    this.busyRequest = false;

    if (this.retromasterService === undefined) {
      return;
    }
    const facetFilters = [];
    for (const entry in this.pageState.facetSelectionMap) {
      const values = this.pageState.facetSelectionMap[entry];
      facetFilters.push(new TQueryFacetFilter({ field: entry, values: values }));
    }
    const tQuery = new TQuery({
      query: query,
      facetFilters: facetFilters
    });
    this.retromasterService.doQuery(tQuery, this.paginator.pageIndex, this.paginator.pageSize, "title", "desc").subscribe(
      d => this.onQueryResults(d),
      e => {
        this.busy = false;
        this.onError(e);
      }
    );
  }

  private onError(e) {
    console.log("onError: msg=%s, code=%s, e", e.message, e.code, e);
    this.messageBusService.publish("serviceError", e);
  }

  private onQueryResults(queryResults: TQueryResults) {
    console.log("onQueryResults", queryResults);
    this.error = null;
    this.busy = false;
    if (this.busyRequest) {
      console.log("was busy so search now...");
      this.findQueryResultsPaged();
    } else {
      this.totalElements = queryResults.totalElements;
      this.queryResults = queryResults.content;
      this.facets = queryResults.facets;
      this.facets.forEach(f => {
        let selectedValues = f.values.filter(v => v.selected).map(v => v.value);
        this.pageState.facetSelectionMap[f.field] = selectedValues;
        this.facetItemCount[f.field] = f.values.length;
      });
    }
  }
}
