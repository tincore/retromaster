/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Injectable } from "@angular/core";
import { LocalStorageService } from "angular-web-storage";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort, SortDirection } from "@angular/material/sort";

const pageSizeOptions = [15, 25, 50, 100, 500];

export interface TableState {
  paginatorPageIndex?: number,
  paginatorPageSize?: number,
  sortActive?: string,
  sortDirection?: SortDirection
}

@Injectable({
  providedIn: "root"
})
export class PageStateService {
  constructor(
    private localStorageService: LocalStorageService
  ) {
  }

  public get(key: string, defaultValue: any = undefined): any {
    const value = this.localStorageService.get(key);
    return value != null ? value : defaultValue;
  }

  public getAndClear(key: string, defaultValue: any = undefined): any {
    const value = this.localStorageService.get(key);
    this.remove(key);
    return value != null ? value : defaultValue;
  }

  public set(key: string, value: any) {
    this.localStorageService.set(key, value);
  }


  remove(key: string) {
    this.localStorageService.remove(key);
  }

  public resetTableState(tableState: TableState, matPaginator: MatPaginator) {
    if (matPaginator) {
      matPaginator.firstPage();
      matPaginator.pageSize = tableState?.paginatorPageSize ? tableState.paginatorPageSize : pageSizeOptions[0];
      matPaginator.pageSizeOptions = pageSizeOptions;
    }
  }

  public restoreTableState(tableState: TableState, matPaginator: MatPaginator, sort: MatSort = null) {
    if (matPaginator) {
      if (tableState.paginatorPageIndex) {
        matPaginator.pageIndex = tableState.paginatorPageIndex;
      } else {
        matPaginator.firstPage();
      }
      matPaginator.pageSize = tableState.paginatorPageSize ? tableState.paginatorPageSize : pageSizeOptions[0];
      matPaginator.pageSizeOptions = pageSizeOptions;
    }
    if (sort) {
      if (tableState.sortActive) {
        sort.active = tableState.sortActive;
      }
      if (tableState.sortDirection) {
        sort.direction = tableState.sortDirection;
      }
    }
  }


  public toTableState(paginator: MatPaginator, sort: MatSort): TableState {
    return {
      paginatorPageIndex: paginator != null ? paginator.pageIndex : null,
      paginatorPageSize: paginator != null ? paginator.pageSize : null,
      sortActive: sort != null ? sort.active : null,
      sortDirection: sort != null ? sort.direction : null
    };
  }
}
