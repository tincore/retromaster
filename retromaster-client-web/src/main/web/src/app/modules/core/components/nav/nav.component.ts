/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* tslint:disable:max-line-length */
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MessageBusService } from '../../services/message-bus.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

export class NavItem {
  constructor(public action: string, public icon?: string, public label?: string, public param?: string, public disabled?: boolean, public location?: string, public semantic: string = action + param + '_do') {}
}

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit, OnDestroy {
  @ViewChild(MatSidenav, { static: true }) matSidenav: MatSidenav;

  private itemsHome: NavItem[] = [new NavItem('route', 'search', 'search', '/', false, null, 'search_display_do')];

  private itemsData: NavItem[] = [
    // new NavItem('route', 'description', 'contents', '/retromaster/content', false, null, 'content_display_do'),
    // new NavItem('route', 'description', 'contentDirectoryPaths', '/retromaster/contentDirectoryPath', false, null, 'contentDirectoryPath_display_do'),
    new NavItem('route', 'class', 'contentMetadataSet', '/retromaster/contentMetadataSet', false, null, 'contentMetadataSet_display_do'),
    new NavItem('route', 'storage', 'contentVolume', '/retromaster/contentVolume', false, null, 'contentVolume_display_do'),
  ];

  private itemsConfig: NavItem[] = [
    new NavItem('route', 'play_circle_filled', 'contentExecutor', '/retromaster/contentExecutor', false, null, 'executor_display_do'),
    // new NavItem('route', 'list', 'contentMetadataVolume', '/retromaster/contentMetadataVolume', false, null, 'contentMetadataVolume_display_do'),
  ];

  private messageBusToggleNavSubscription: Subscription;

  constructor(private messageBusService: MessageBusService, private router: Router) {}

  getItemsHome(): NavItem[] {
    return this.itemsHome;
  }

  getItemsData(): NavItem[] {
    return this.itemsData;
  }

  getItemsConfig(): NavItem[] {
    return this.itemsConfig;
  }

  ngOnDestroy() {
    this.messageBusToggleNavSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.messageBusToggleNavSubscription = this.messageBusService.subscribe('navToggle', () => {
      this.matSidenav.toggle();
    });
  }

  onItemClicked(item: NavItem): void {
    this.closeNav();

    if (item.action === 'route') {
      console.log('onItemClicked ', item);
      this.router.navigate([item.param]);
    }
  }

  onNavCloseClicked(): void {
    this.closeNav();
  }

  private closeNav() {
    this.matSidenav.close();
  }
}
