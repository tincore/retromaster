/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs';
import { Subscription } from 'rxjs';

interface Message {
  topic: string;
  payload: any;
}

type Consumer = (payload: any) => void;

@Injectable({
  providedIn: 'root',
})
export class MessageBusService {
  private handler = new Subject<Message>();

  publish(topic: string, payload: any) {
    this.handler.next({ topic: topic, payload });
  }

  subscribe(topic: string, consumer: Consumer) {
    return this.handler
      .filter(message => message.topic === topic)
      .map(message => message.payload)
      .subscribe(consumer);
  }
}
