/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { AfterViewChecked, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ApplicationMessage, ApplicationMessagesService } from '../../services/application-messages.service';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent implements AfterViewChecked, OnInit, OnDestroy {
  public messages: ApplicationMessage[];

  @ViewChild('messageList') private messageList: ElementRef;

  private autoScroll = true;
  private subscription: Subscription;

  constructor(private applicationMessagesService: ApplicationMessagesService, private dialogRef: MatDialogRef<MessagesComponent>) {
    this.messages = applicationMessagesService.messages;
  }

  public close() {
    this.dialogRef.close();
  }

  public ngAfterViewChecked() {
    this.doScrollToBottom();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
    this.subscription = this.applicationMessagesService.subscribe(m => {
      this.messages = this.applicationMessagesService.messages;
      this.doScrollToBottom();
    });
  }

  public onScroll() {
    const element = this.messageList.nativeElement;
    const atBottom = element.scrollHeight - element.scrollTop === element.clientHeight;
    if (this.autoScroll && atBottom) {
      this.autoScroll = true;
    } else {
      this.autoScroll = false;
    }
  }

  private doScrollToBottom() {
    if (!this.autoScroll) {
      return;
    }
    try {
      this.messageList.nativeElement.scrollTop = this.messageList.nativeElement.scrollHeight;
    } catch (err) {}
  }
}
