/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import { Subject, Subscription } from 'rxjs';

type Consumer = (message: ApplicationMessage) => void;

export class ApplicationMessage {
  constructor(public message: string, public type = 'type', public time = new Date()) {}
}

@Injectable({
  providedIn: 'root',
})
export class ApplicationMessagesService {
  messages: ApplicationMessage[];
  private handler = new Subject<ApplicationMessage>();

  constructor() {
    this.messages = [];
  }

  add(message: ApplicationMessage) {
    this.messages.push(message);
    const maxMessages = 100;
    if (this.messages.length > maxMessages) {
      this.messages = this.messages.slice(this.messages.length - maxMessages, this.messages.length);
    }

    this.handler.next(message);
  }

  subscribe(consumer: Consumer): Subscription {
    return this.handler.subscribe(consumer);
  }
}
