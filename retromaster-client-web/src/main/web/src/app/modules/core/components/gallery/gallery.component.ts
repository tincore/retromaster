/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { BehaviorSubject, Observable, Subscription } from "rxjs";
import { MatDialog } from "@angular/material/dialog";
import { MediaChange, MediaObserver } from "@angular/flex-layout";
import { GalleryModalComponent } from "../gallery-modal/gallery-modal.component";
import { filter, map } from "rxjs/operators";

export interface GalleryImage {
  src: string;
  position: number;
  alt: string;
}

export interface GalleryImageSelectionChange {
  image: GalleryImage;
  first: boolean;
  last: boolean;
}

@Component({
  selector: "app-gallery",
  templateUrl: "./gallery.component.html",
  styleUrls: ["./gallery.component.scss"]
})
export class GalleryComponent implements OnInit, OnDestroy {
  subscription: Subscription[] = [];

  @Input() columns = 5;
  @Input() images: GalleryImage[] = [];

  private images$: BehaviorSubject<GalleryImage[]> = new BehaviorSubject(this.images);
  private imageSelectionChange$: BehaviorSubject<GalleryImageSelectionChange> = new BehaviorSubject(undefined);

  constructor(
    public dialog: MatDialog,
    public mediaObserver: MediaObserver
  ) {
  }

  onImageClicked(position: number): void {
    this.selectImage(position);

    let dialogConfig = {
      maxWidth: "95vw",
      maxHeight: "95vh",
      height: "95%",
      width: "95%",
      data: {
        gallery: this
      }
    };
    this.dialog.open(GalleryModalComponent, dialogConfig);
  }

  ngOnInit(): void {
    this.subscription.push(
      this.mediaObserver.asObservable()
        .pipe(
          filter((changes: MediaChange[]) => changes.length > 0),
          map((changes: MediaChange[]) => changes[0])
        ).subscribe((change: MediaChange) => {
        switch (change.mqAlias) {
          case "xs": {
            this.columns = 1;
            break;
          }
          case "sm": {
            this.columns = 2;
            break;
          }
          case "md": {
            this.columns = 3;
            break;
          }
          case "lg": {
            this.columns = 5;
            break;
          }
          default: {
            this.columns = 6;
            break;
          }
        }
      })
    );
    this.subscription.push(
      this.images$.asObservable().subscribe(g => this.images = g)
    );
  }

  ngOnDestroy(): void {
    this.subscription.forEach(subscription => subscription.unsubscribe());
  }

  onImageSelectionChange(): Observable<GalleryImageSelectionChange> {
    return this.imageSelectionChange$.asObservable();
  }

  selectImageRelative(position: number, displacement: number): void {
    const newPosition = position + displacement;
    if (newPosition < 0) {
      this.selectImage(this.images.length - 1);
    } else if (newPosition >= this.images.length) {
      this.selectImage(0);
    } else {
      this.selectImage(newPosition);
    }
  }


  selectImage(position: number): void {
    if (position >= 0 && position < this.images.length) {
      let galleryImage = this.images[position];
      this.imageSelectionChange$.next({
        image: galleryImage,
        first: position == 0,
        last: position == this.images.length - 1
      });
    }
  }
}
