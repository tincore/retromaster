/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { ErrorHandler, NgModule } from "@angular/core";
import { MaterialModule } from "../material.module";
import { TranslateModule } from "@ngx-translate/core";

import { Error401PageComponent } from "./pages/error401-page/error401-page.component";
import { Error404PageComponent } from "./pages/error404-page/error404-page.component";
import { ConfirmDeleteDialogComponent } from "./components/confirm-delete-dialog/confirm-delete-dialog.component";
import { NavComponent } from "./components/nav/nav.component";
import { ToolbarComponent } from "./components/toolbar/toolbar.component";
import { AboutComponent } from "./components/about/about.component";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { MessagesComponent } from "./components/messages/messages.component";
import { LoginDialogComponent } from "./components/login-dialog/login-dialog.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CoreRoutingModule } from "./core-routing.module";
import { LayoutModule } from "@angular/cdk/layout";
import { GalleryComponent } from "./components/gallery/gallery.component";
import { GalleryModalComponent } from "./components/gallery-modal/gallery-modal.component";
import { GlobalErrorHandler } from "./services/global-error-handler";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LayoutModule,
    MaterialModule,
    RouterModule,
    TranslateModule,
    CoreRoutingModule
  ],
  declarations: [
    AboutComponent,
    ConfirmDeleteDialogComponent,
    GalleryComponent,
    GalleryModalComponent,
    LoginDialogComponent,
    MessagesComponent,
    NavComponent,
    ToolbarComponent,
    Error401PageComponent,
    Error404PageComponent
  ],
  exports: [
    AboutComponent,
    ConfirmDeleteDialogComponent,
    GalleryComponent,
    GalleryModalComponent,
    LoginDialogComponent,
    MessagesComponent,
    NavComponent,
    ToolbarComponent,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    }
  ]
})
export class CoreModule {
}
