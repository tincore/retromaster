/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AboutComponent } from '../about/about.component';

import { MessageBusService } from '../../services/message-bus.service';
import { MessagesComponent } from '../messages/messages.component';
import { MatDialog } from '@angular/material/dialog';

export enum ToolbarItemLocation {
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
  MENU = 'MENU',
}

export class ToolbarItem {
  constructor(public action: string, public icon?: string, public label?: string, public disabled = false, public location = ToolbarItemLocation.RIGHT, public semantic: string = action + '_do') {}
}

export const TOOLBAR_ITEM_NAVMENU = new ToolbarItem('sidenavToggle', 'menu', 'menu', false, ToolbarItemLocation.LEFT);
export const TOOLBAR_ITEM_NAVBACK = new ToolbarItem('navBack', 'arrow_back', 'back', false, ToolbarItemLocation.LEFT);
export const TOOLBAR_ITEM_ABOUT = new ToolbarItem('about', 'info_outline', 'about', false, ToolbarItemLocation.MENU);
export const TOOLBAR_ITEM_LOGOUT = new ToolbarItem('logout', 'logout', 'logout', false, ToolbarItemLocation.MENU);
export const TOOLBAR_ITEM_DETAILS = new ToolbarItem('details', 'details', 'details', false, ToolbarItemLocation.RIGHT);
export const TOOLBAR_ITEM_MESSAGE = new ToolbarItem('message', 'message', 'message', false, ToolbarItemLocation.RIGHT);

@Component({
  selector: 'app-toolbar',
  styleUrls: ['./toolbar.component.scss'],
  templateUrl: './toolbar.component.html',
})
export class ToolbarComponent {
  @Input() title = '';
  @Input() titleSub = '';

  @Input() busy = false;

  @Output() action = new EventEmitter<string>();
  @Output() toggleSidenav = new EventEmitter<void>();

  @Input() items: ToolbarItem[];

  constructor(private dialog: MatDialog, private messageService: MessageBusService) {
    if (!this.items) {
      this.items = [];
    }
  }

  getItemsByLeft() {
    return this.getItemsByLocation(ToolbarItemLocation.LEFT);
  }

  getItemsByMenu() {
    return this.getItemsByLocation(ToolbarItemLocation.MENU);
  }

  getItemsByRight() {
    return this.getItemsByLocation(ToolbarItemLocation.RIGHT);
  }

  onItemClicked(item: ToolbarItem) {
    if (item.action === TOOLBAR_ITEM_ABOUT.action) {
      this.dialog.open(AboutComponent);
    } else if (item.action === TOOLBAR_ITEM_LOGOUT.action) {
      this.messageService.publish('logout', {});
    } else if (item.action === TOOLBAR_ITEM_NAVMENU.action) {
      this.messageService.publish('navToggle', {});
    } else if (item.action === TOOLBAR_ITEM_MESSAGE.action) {
      this.dialog.open(MessagesComponent);
    } else {
      this.action.emit(item.action);
    }
  }

  private getItemsByLocation(location: ToolbarItemLocation) {
    return this.items.filter(i => location === i.location);
  }
}
