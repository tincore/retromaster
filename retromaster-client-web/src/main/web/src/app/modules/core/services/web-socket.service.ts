/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Injectable } from '@angular/core';
import { Client, IMessage, StompSubscription } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { AppConfig } from '../../../app.config';

export class WebSocketSubscription {
  public stompSubscription: StompSubscription;

  constructor(public id: string, public topic: string, public onMessage: (message: IMessage) => void) {
    // within message callback
    //         if (message.headers['content-type'] === 'application/octet-stream') {
    //             // message is binary
    //             // call message.binaryBody
    //         } else {
    //             // message is text
    //             // call message.body
    //         }
  }
}

@Injectable({
  providedIn: 'root',
})
export class WebSocketService {
  private verbose = false;

  private readonly endpoint: string;

  private stompClient: Client;
  private connected: boolean;

  private webSocketSubscriptions: WebSocketSubscription[] = [];

  constructor() {
    this.endpoint = AppConfig.endpoints.websocket;
  }

  add(webSocketSubscription: WebSocketSubscription) {
    const found = this.webSocketSubscriptions.find(w => w.id == webSocketSubscription.id);
    if (found) {
      console.log('subscription already added', webSocketSubscription);
      return;
    }
    this.webSocketSubscriptions.push(webSocketSubscription);

    if (this.isConnected()) {
      this.subscribeAll();
    }
  }

  connect() {
    console.log('websocket connect requested');
    this.init();
    this.stompClient.activate();
  }

  disconnect() {
    console.log('websocket disconnect requested');
    if (this.stompClient != null) {
      this.stompClient.deactivate();
    }
  }

  isConnected() {
    return this.connected;
  }

  public publish(destination: string, body: string) {
    console.log('publish ', destination);
    this.init();
    this.stompClient.publish({
      destination: destination,
      // Alternative... binaryData type should be Uint8Array
      // binaryBody: binaryData,
      // headers: { 'content-type': 'application/octet-stream' },

      // skipContentLengthHeader: true,
      body: body,
    });
  }

  private init() {
    if (this.stompClient != null) {
      return;
    }
    console.log('initStomp ', this.endpoint);
    this.stompClient = new Client({
      brokerURL: this.endpoint,
      // connectHeaders: {
      //     login: 'user',
      //     passcode: 'password',
      // },
      reconnectDelay: 5000,
      heartbeatIncoming: 4000,
      heartbeatOutgoing: 4000,
    });
    this.stompClient.webSocketFactory = () => {
      let endpoint = this.endpoint;
      return new SockJS(endpoint);
    };
    this.stompClient.onConnect = () => {
      console.log('onConnect received');
      this.connected = true;
      this.subscribeAll();
    };
    this.stompClient.onDisconnect = () => {
      console.log('onDisconnect received');
      this.connected = false;
    };

    if (this.verbose) {
      this.stompClient.logRawCommunication = true;
      this.stompClient.debug = function (str) {
        console.log('stompClient', str);
      };
    } else {
      this.stompClient.debug = function (str) {};
    }

    this.stompClient.onStompError = function (frame) {
      // Will be invoked in case of error encountered at Broker
      // Bad login/passcode typically will cause an error
      // Complaint brokers will set `message` header with a brief message. Body may contain details.
      // Compliant brokers will terminate the connection after any error
      console.log('Broker reported error: ' + frame.headers['message']);
      console.log('Additional details: ' + frame.body);
    };
  }

  private subscribeAll() {
    this.webSocketSubscriptions.forEach(w => {
      console.log('subscribe', w);
      if (w.stompSubscription == null) {
        w.stompSubscription = this.stompClient.subscribe(w.topic, w.onMessage);
      }
    });
  }
}
