/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { ErrorHandler, Injectable, NgZone } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { of } from "rxjs";
import { RetromasterAuthService } from "../../retromaster/services/retromaster-auth.service";
import { Thrift } from "../../retromaster/services/thrift";

export class Error {
  constructor(public error: string, public message: string, public path: string, public status: number, public timestamp: string) {
  }
}

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(
    private retromasterAuthService: RetromasterAuthService,
    private zone: NgZone
  ) {
  }

  // Temporary...
  public static handleErrorCustom<T>(operation = "operation", result?: T) {
    return (error: any) => {
      // Should eventually: send the error to remote logging infrastructure
      console.error("handleError", error); // log to console instead
      if (error.status >= 400) {
        throw error;
      }

      return of(result as T);
    };
  }

  handleError(error: any) {
    if (!(error instanceof HttpErrorResponse)) {
      error = error.rejection; // get the error object
    }

    // this.zone.run(() => console.error("Error from global error handler zone", error));
    // In the worst of the cases leave some global variable with last thrift error and read it
    if (Thrift.lastResponseStatus == 401 || Thrift.lastResponseStatus == 403) {
      console.error("Thrift error detected! Clearing all auth and Reloading", error, Thrift.lastResponseStatus);
      Thrift.lastResponseStatus = 0;
      this.retromasterAuthService.clearAuthToken();
      window.location.reload();
    }

  }

}

