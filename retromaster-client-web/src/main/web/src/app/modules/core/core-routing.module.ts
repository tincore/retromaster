/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Error401PageComponent } from './pages/error401-page/error401-page.component';
import { Error404PageComponent } from './pages/error404-page/error404-page.component';
import { CoreConfig } from './core.config';

const routes: Routes = [
  { path: CoreConfig.routes.error401, component: Error401PageComponent },
  { path: CoreConfig.routes.error404, component: Error404PageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule {}
