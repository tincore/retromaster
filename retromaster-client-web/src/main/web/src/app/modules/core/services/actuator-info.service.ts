/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AppConfig } from "../../../app.config";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { GlobalErrorHandler } from "./global-error-handler";

export class ActuatorInfo {
  constructor(public build: ActuatorInfoBuild) {
  }
}

export class ActuatorInfoBuild {
  constructor(public version: string, public group: string, public artifact: string) {
  }
}

@Injectable({
  providedIn: "root"
})
export class ActuatorInfoService {
  endpoint: string;

  constructor(private http: HttpClient) {
    this.endpoint = AppConfig.endpoints.actuator + "/info";
  }

  get(): Observable<ActuatorInfo> {
    return this.http.get<ActuatorInfo>(this.endpoint).pipe(catchError(GlobalErrorHandler.handleErrorCustom("get", null)));
  }
}
