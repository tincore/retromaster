/*-
 * #%L
 * retromaster-client-web
 * %%
 * Copyright (C) 2021 - 2023 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { GalleryComponent, GalleryImage, GalleryImageSelectionChange } from "../gallery/gallery.component";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { postcss } from "@angular-devkit/build-angular/src/webpack/plugins/postcss-cli-resources";

@Component({
  selector: "app-gallery-modal",
  templateUrl: "./gallery-modal.component.html",
  styleUrls: ["./gallery-modal.component.scss"]
})
export class GalleryModalComponent implements OnInit, OnDestroy {

  public image: GalleryImage;
  public first: boolean;
  public last: boolean;

  private subscription: Subscription;
  private gallery: GalleryComponent;

  constructor(
    private ref: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.gallery = data.gallery;
  }


  ngOnInit(): void {
    this.subscription = this.gallery
      .onImageSelectionChange()
      .subscribe((galleryImageSelectionChange: GalleryImageSelectionChange) => {
        this.image = galleryImageSelectionChange.image;
        this.first = galleryImageSelectionChange.first;
        this.last = galleryImageSelectionChange.last;
        this.ref.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSwipeLeft() {
    this.gallery.selectImageRelative(this.image.position, -1);
  }

  onSwipeRight() {
    this.gallery.selectImageRelative(this.image.position, 1);
  }


  onNextClicked() {
    this.gallery.selectImageRelative(this.image.position, 1);
  }

  onPrevClicked() {
    this.gallery.selectImageRelative(this.image.position, -1);
  }

  isFirstEnabled() {
    return true;
  }

  isLastEnabled() {
    return true;
  }
}
