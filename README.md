# Tincore RetroMaster

This project stores, manages and serves in a space efficient manner massive amounts of rom files.

## Usage

### Execution

Open a terminal where retromaster-service.jar is placed and execute:
 
```
java -jar retromaster-service.jar
```

This will create database and needed directories automatically.

### Importing new data files and content

1. Place dat files in ${user.home}/retromaster/meta/import
2. New dat files will be imported
3. Updated dat files will be updated
4. Place rom files (compressed or uncompresed) in ${user.home}/retromaster/data/import
5. Recognized files are deduplicated and recompressed into ${user.home}/retromaster/data/prod
6. Unrecognized files are deduplicated and compressed into ${user.home}/retromaster/data/export

### Indexing custom content

Create new directory inside folder

```
 ${user.home}/retromaster/meta/stage/
```

For instance

```
 ${user.home}/retromaster/meta/stage/IBM_PC
```

This will create a staging dat file called "IBM_PC" where each sub directory or compressed file will be considered
a rom.   


### Accessing content

#### Web UI

Files can be browsed with embedded Web based UI

```
 http://localhost:8085/
```

Default username/password are admin/admin

Roms can be executed if configured emulators are detected on system. 

#### FUSE

Files are then transparently accessible through fuse or ftp (for instance).

```
 ${user.home}/.config/retromaster/mnt/retro
```


#### Dev mode

When enabled devmode for authorization and other utils can be reached at

```
http://localhost:8085/dev
```

Authorize with name admin and claims ROLE=admin

#### H2 Console

When enabled in props

```
http://localhost:8085/h2-console
```

Usual database location
```
jdbc:h2:file:~/.config/retromaster/db
```

User/Pass
```
retromaster:retromaster
```


## Development


### Preconditions

Use Linux

Make sure that you have essential libraries installed.

```
apt-get install -y -qq build-essential libz-dev zlib1g-dev && apt-get install -y flex bison
```

If using chromedriver on tests

```
apt-get install chromium
```

For some fuse tests

```
apt-get install avfs unionfs-fuse
```


### Javascript Client (retromaster-client)

Retromaster TS/JS Frontend. 
Based on **Angular 14** 

It is served directly from retromaster-service

#### Development

** IT LOOKS LIKE NG does not like GraalVM **

Change to a different JDK. For instance:
```
sdk install java 11.0.11.hs-adpt
```

Make sure Angular CLI is installed globally

```
sudo npm install -g @angular/cli --unsafe-perm=true --allow-root
```

To remove...
```
sudo npm uninstall -g @angular/cli
```

To provide some mock data (if needed) you may start json server.

```
npm run json-server
```

```
npm run ng serve
```

##### Maven

There are a number of maven profiles to facilitate development

Will compile the project quicker when all dependencies are already present
```
cd retromaster-client-web
mvn install -Pquick
```

To start server (ng serve)
Served content available at  http://localhost:4200/app

```                                                                        
cd retromaster-client-web
mvn package -Pbuild_dev
```                                                                        
which is essentially doing

```                                                                        
npm run build_dev
```                                                                        


This will need some sort of backend. For that:
Json server (provides data) can be started by
```
mvn package -P dev-server
```  

Updates dependencies to latest versions

`mvn validate -PupdateLatestDependencies -U`

---
---

### IDE

Be careful with some editors like intellij. Currently if you modify any hibernate pojo it will recompile it WITHOUT 
bytecode enhancements and the tests will fail if run from intellij (Maven is fine). You can avoid this by enabling
in Settings>...>Maven>Runner delegate build options into maven but then you would need to add a lot of properties
to skip useless steps that make the development build slower.

For instance
```
maven.install.skip      true
it.skip	                true
coverage.skip	        true
spring-boot.skip        true
checkstyle.skip	        true
pmd.skip                true
spotbugs.skip	        true
source.skip             true
source.jar.skip	        true
source.test-jar.skip	true
javadoc.skip	        true
javadoc.jar.skip        true
javadoc.test-jar.skip	true
docker.skip             true
```


### Database

#### Export import
script SIMPLE to '~/contents.sql' table CONTENTS;
RUNSCRIPT FROM 'test.sql'
```
java -cp h2*.jar org.h2.tools.RunScript -url jdbc:h2:~/test -script test.sql
```

RunScript.execute(conn, new FileReader("test.sql"));

```
java -cp ~/.m2/repository/com/h2database/h2/1.4.197/h2-1.4.197.jar org.h2.tools.RunScript -url jdbc:h2:~/test -script test.sql
```

```
java -cp ~/.m2/repository/com/h2database/h2/1.4.197/h2-1.4.197.jar org.h2.tools.Recover
```

---

## Emulators

##### DosBox-X

###### Install
sudo snap install dosbox-x


##### FS-UAE

###### Install
sudo add-apt-repository ppa:fengestad/stable

##### REICAST

###### Install
sudo snap install reicast --edge

---

# Run options
To do mass import and indexing helps giving enough memory.

-Xms128m -Xmx4096m -XX:MaxPermSize=350m -XX:ReservedCodeCacheSize=64m -XX:+UseCodeCacheFlushing -XX:+UseCompressedOops
-Xms1G -Xmx5G -XX:+UseStringDeduplication -XX:+HeapDumpOnOutOfMemoryError 

---

## TODO

Fuse-Java on windows. Check. Add auto unit selection for tests. 
Add assumes on tests to avoid running on Windows. 

At some time try to move to new hibernate-search.

Create users and secure accessing from
