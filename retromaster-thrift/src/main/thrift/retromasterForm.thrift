namespace java com.tincore.retromaster.thrift.form

enum TDirection {
    ASC = 0,
    DESC = 1;
}

struct TOrder {
    1:  required string property;
    2:  optional TDirection direction;
}

struct TPageable {
    1:  required i64 page;
    2:  required i64 size;
    3:  optional list<TOrder> sort;
}

struct TUser {
    1:  required string name;
    2:  required list<string> authorities;
}

struct TQueryResult {
    1: required string id;
    2: optional TContentMetadataSummary contentMetadataSummary;
}

struct TQueryFacetValue {
    1: required string value;
    2: optional i64 count;
    3: optional bool selected;
}

struct TQueryFacet {
    1: required string field;
    2: optional list<TQueryFacetValue> values;
}

struct TQueryResults {
    1: required i64 number;
    2: required i64 size;
    3: required i64 totalElements;
    4: required i64 totalPages;
    5: optional list<TQueryResult> content;
    6: optional list<TQueryFacet> facets;
}

struct TQueryFilter {
    1: required string field;
    2: required string value;
}

struct TQueryFacetFilter {
    1: required string field;
    2: required list<string> values;
    3: optional bool includeZero;
    4: optional i32 valuesMax;
}

struct TQuery {
    1: optional string query;
    2: optional list<TQueryFilter> filters;
    6: optional list<TQueryFacetFilter> facetFilters;
}

enum TContentMediaType {
    UNKNOWN = 0,
    DISC = 1,
    DISK = 2,
    FILE = 3,
    PART = 4,
    SIDE = 5,
    TAPE = 6;

    PIC_SNAPSHOT = 7;
    PIC_TITLE = 8;
    PIC_BOXART = 9;

    DOC_MANUAL = 10;
    DOC_MAGAZINE = 11,
}

struct TContentMetadataSet {
    1: required string id;
    2: required string category;
    3: required string name;
    4: optional string version;
    5: optional string description;
    6: optional string tagsExtra;
    7: optional string author;
    8: optional string email;
    9: optional string url;
    10: optional string filename;
    11: optional string repositoryPath;
    12: optional bool staging;
    13: optional string checkTime;
    14: optional string checkContentTime;
}

struct TContentMetadataSets {
    1: required i64 number;
    2: required i64 size;
    3: required i64 totalElements;
    4: required i64 totalPages;
    5: optional list<TContentMetadataSet> content;
}

struct TContentMetadataSetFilter {
    1: optional string query;
    2: optional string categoryEquals;
    3: optional string nameEquals;
    4: optional string nameStartsWith;
}

enum TContentMetadataComplete {
    EMPTY = 0,
    PARTIAL = 1,
    COMPLETE = 2,
    NOITEMS = 3;
    UNKNOWN = 4;
}

struct TContentMetadata {
    1: required string id;
    2: required string contentMetadataSetId;
    3: required string name;
    4: required string title;
    5: optional string description;
    6: optional string date;
    7: optional string publisher;
    8: optional TContentMediaType mediaType;
    9: optional string mediaOrder;
    10: optional bool verified;
    11: optional bool alternate;
    12: optional string alternateData;
    13: optional bool badDump;
    14: optional string badDumpData;
    15: optional bool cracked;
    16: optional string crackedData;
    17: optional bool fixed;
    18: optional string fixedData;
    19: optional bool hacked;
    20: optional string hackedData;
    21: optional bool modified;
    22: optional string modifiedData;
    23: optional bool trained;
    24: optional string trainedData;
    25: optional bool translated;
    26: optional string translatedData;
    27: optional bool pirated;
    28: optional string piratedData;
    29: optional bool virus;
    30: optional string virusData;
    31: optional string tagsExtra;
    32: optional list<TContentMetadataItem> items;
    33: optional string region;
    34: optional string language;
    35: optional string system;
    36: optional string source;

    37: required bool staging;

    38: optional bool unidentified;
    39: optional TContentMetadataComplete complete;
}

struct TContentMetadataSummary {
    1: required string id;
    2: required string contentMetadataSetId;
    3: required string name;
    4: required string title;
    5: optional string description;
    6: optional string date;
    7: optional string publisher;
    8: optional TContentMediaType mediaType;
    9: optional string mediaOrder;
    10: optional bool verified;
    11: optional bool alternate;
    12: optional string alternateData;
    13: optional bool badDump;
    14: optional string badDumpData;
    15: optional bool cracked;
    16: optional string crackedData;
    17: optional bool fixed;
    18: optional string fixedData;
    19: optional bool hacked;
    20: optional string hackedData;
    21: optional bool modified;
    22: optional string modifiedData;
    23: optional bool trained;
    24: optional string trainedData;
    25: optional bool translated;
    26: optional string translatedData;
    27: optional bool pirated;
    28: optional string piratedData;
    29: optional bool virus;
    30: optional string virusData;
    31: optional string tagsExtra;
    33: optional string region;
    34: optional string language;
    35: optional string system;
    36: optional string source;

    37: required bool staging;

    38: optional bool unidentified;
    39: optional TContentMetadataComplete complete;
}


struct TContentMetadataSummaries {
    1: required i64 number;
    2: required i64 size;
    3: required i64 totalElements;
    4: required i64 totalPages;
    5: optional list<TContentMetadataSummary> content;
}

struct TContentMetadatas {
    1: required i64 number;
    2: required i64 size;
    3: required i64 totalElements;
    4: required i64 totalPages;
    5: optional list<TContentMetadata> content;
}
struct TContentMetadataFilter {
    1: optional string query;
    2: optional string contentMetadataSetIdEquals;
    3: optional string nameEquals;
    4: optional string titleEquals;
    5: optional string publisherEquals;
}

struct TContentMetadataItem {
    1: required string id;
    2: required string contentMetadataId;
    3: required string name;
    4: optional string crc;
    5: optional string sha1;
    6: optional string md5;
    7: optional i64 size;
}

struct TContentMetadataItemSummary {
    1: required string id;
    2: required string contentMetadataId;
    3: required string name;
    4: optional i64 size;
    5: required string title;
    6: optional string date;
    7: optional string publisher;
    8: optional TContentMediaType mediaType;
    9: optional string mediaOrder;
    10: optional bool verified;
    11: optional string region;
    12: optional string language;
    13: optional string system;
    14: optional string source;
    15: optional string contentId;
}

struct TContentMetadataMedia {
    1: required string contentSignature;
    2: required TContentMediaType mediaType;
}

enum TContentVolumeEnvironment {
    PROD = 0;
    STAGE = 1;
    IMPORT = 2;
}

struct TContentVolume {
    1: required string id;
    2: required string name;
    3: required string path;
    4: required TContentVolumeEnvironment environment;
    5: optional i32 precedence;
}

struct TContentVolumeDirectory {
    1: required string contentVolumeId;
    2: required string path;
    3: optional string modifiedTime;
    4: optional bool exists;
    5: optional TContentMetadataSet contentMetadataSet;
    6: optional bool fresh;
}

struct TContentVolumeCreate {
    1: required string name;
    2: required string path;
    3: required TContentVolumeEnvironment environment;
    4: optional i32 precedence;
}

struct TContent {
    1: required string id;
    2: required string volumeId;
    3: required string directoryPath;
    4: required string fileName;
    5: optional string fileEntry;
    6: required string fileSize;
    7: optional string crc;
    8: optional string sha1;
    9: optional string md5;
    10: optional i64 size;
    11: optional string enrichTime;
    12: optional string modifiedTime;
    13: optional string checkTime;
}

struct TContents {
    1: required i64 number;
    2: required i64 size;
    3: required i64 totalElements;
    4: required i64 totalPages;
    5: optional list<TContent> content;
}

struct TContentFilter {
    1: optional string query;
    2: optional string volumeIdEquals;
    3: optional string directoryPathEquals;
    4: optional string fileNameEquals;
    5: optional string fileEntryEquals;
    6: optional i64 fileSizeEquals;
    7: optional string crcEquals;
    8: optional string sha1Equals;
    9: optional string md5Equals;
    10: optional i64 sizeEquals;
}

struct TContentDirectoryPath {
    1: required string id;
}

struct TContentDirectoryPaths {
    1: required i64 number;
    2: required i64 size;
    3: required i64 totalElements;
    4: required i64 totalPages;
    5: optional list<TContentDirectoryPath> content;
}

struct TContentDirectoryPathFilter {
    1: optional string query;
    2: optional string volumeIdEquals;
}

struct TContentMetadataItemExecuteOptionSet {
    1: required string contentMetadataItemId;
    2: required string contentMetadataItemName;
    3: optional list<TContentExecutor> contentExecutors;
}

struct TContentSystem {
    1: required string id;
    2: required string name;
}

struct TContentExecutor {
    1: required string id;
    2: required string name;
    3: required bool enabled;
    4: optional bool found;
    5: optional list<TContentSystem> systems;
}

struct TContentExecutorFilter {
    1: optional string idEquals;
    2: optional bool enabledEquals;
    3: optional bool foundEquals;
}

struct TContentExecuteProcess {
    1: required string id;
    2: required string contentExecutorId;
    3: required string command;
}

enum TContentImportEventType {
    START = 0,
    SUCCESS = 1,
    FAIL = -1,
    PROGRESS = 2,
}

struct TContentImportEvent {
    1: required TContentImportEventType type;
    2: i32 errorCount;
}

enum TContentMetadataSetManageEventType {
    IMPORT_START = 100,
    IMPORT_PROGRESS = 102,
    IMPORT_SUCCESS = 101,
    IMPORT_FAIL = -101,

    REIMPORT_START = 200,
    REIMPORT_SUCCESS = 201,
    REIMPORT_FAIL = -201,

    METADATAS_MODIFIED = 1000,
}

struct TContentMetadataSetManageEvent {
    1: required TContentMetadataSetManageEventType type;
    2: string message
    3: i32 count;
}

enum TContentScanEventType {
    START = 0,
    SUCCESS = 1,
    FAIL = -1,
    PROGRESS_CONTENT_METADATA_SET_CREATED = 2,
}

struct TContentScanEvent {
    1: required TContentScanEventType type;
    2: string message
    3: i32 errorCount;
}

enum TContentIndexEventType {
    START = 0,
    SUCCESS = 1,
    FAIL = -1,
    PROGRESS = 2,
}

struct TContentIndexEvent {
    1: required TContentIndexEventType type;
    2: string message
    3: i32 count;
    4: i32 total;
}

struct TContentExecutionEvent {
    1: required string contentExecutionId;
    2: required string message;
}

exception TGenericError {
    1: string message,
    2: i16 code,
}
