include "retromasterForm.thrift"

namespace java com.tincore.retromaster.thrift


service RetromasterAuthService {

    string doLogin(1: string username, 2: string password) throws (1:retromasterForm.TGenericError error);

    retromasterForm.TUser getUserMe() throws (1:retromasterForm.TGenericError error);
}