include "retromasterForm.thrift"

namespace java com.tincore.retromaster.thrift


service RetromasterService {

# ---
    void doContentReorganizeRequest() throws (1:retromasterForm.TGenericError error);
    void doReindexRequest() throws (1:retromasterForm.TGenericError error);

    void doContentScanByContentVolumeStaging() throws (1:retromasterForm.TGenericError error);
    void doContentScanByContentMetadataSetId(1:  string id) throws (1:retromasterForm.TGenericError error);
    void doContentScanByContentMetadataId(1:  string id) throws (1:retromasterForm.TGenericError error);
# ---
    retromasterForm.TContentMetadataSet findContentMetadataSetById(1:  string id) throws (1:retromasterForm.TGenericError error);
    retromasterForm.TContentMetadataSets findContentMetadataSetsByFilter(1:  retromasterForm.TContentMetadataSetFilter filter, 2: retromasterForm.TPageable pageable) throws (1:retromasterForm.TGenericError error);

    string saveContentMetadataSet(1:  retromasterForm.TContentMetadataSet contentMetadataSet) throws (1:retromasterForm.TGenericError error);
    void deleteContentMetadataSetById(1:  string id) throws (1:retromasterForm.TGenericError error);

# ---
    retromasterForm.TContentMetadataSummaries findContentMetadataSummariesByFilter(1:  retromasterForm.TContentMetadataFilter filter, 2: retromasterForm.TPageable pageable) throws (1:retromasterForm.TGenericError error);

# ---
    retromasterForm.TContentMetadata findContentMetadataById(1:  string id) throws (1:retromasterForm.TGenericError error);
    retromasterForm.TContentMetadatas findContentMetadatasByFilter(1:  retromasterForm.TContentMetadataFilter filter, 2: retromasterForm.TPageable pageable) throws (1:retromasterForm.TGenericError error);
    list<retromasterForm.TContentMetadata> findContentMetadatasByContentId(1: string id) throws (1:retromasterForm.TGenericError error);

    string saveContentMetadata(1:  retromasterForm.TContentMetadata contentMetadata) throws (1:retromasterForm.TGenericError error);

# ---
    retromasterForm.TContentMetadataItem findContentMetadataItemById(1: string id) throws (1:retromasterForm.TGenericError error);
    list<retromasterForm.TContentMetadataItem> findContentMetadataItemsByContentId(1: string id) throws (1:retromasterForm.TGenericError error);
    list<retromasterForm.TContentMetadataItemSummary> findContentMetadataItemSummariesByContentId(1: string id) throws (1:retromasterForm.TGenericError error);

# ---
    retromasterForm.TContent findContentById(1:  string id) throws (1:retromasterForm.TGenericError error);
    retromasterForm.TContents findContentsByFilter(1:  retromasterForm.TContentFilter filter, 2: retromasterForm.TPageable pageable) throws (1:retromasterForm.TGenericError error);
    list<retromasterForm.TContent> findContentsByContentMetadataId(1: string id) throws (1:retromasterForm.TGenericError error);
    list<retromasterForm.TContent> findContentsByContentMetadataItemId(1: string id) throws (1:retromasterForm.TGenericError error);

# ---
    retromasterForm.TContentVolume findContentVolumeById(1: string id) throws (1:retromasterForm.TGenericError error);
    list<retromasterForm.TContentVolumeDirectory> findContentVolumeDirectoriesByContentVolumeId(1: string id, 2: bool extended) throws (1:retromasterForm.TGenericError error);
    retromasterForm.TContentVolumeDirectory findContentVolumeDirectoryByContentVolumeIdAndPath(1: string id, 2: string path, 3: bool extended) throws (1:retromasterForm.TGenericError error);

    void doContentVolumeDirectoryScan(1: retromasterForm.TContentVolumeDirectory directory) throws (1:retromasterForm.TGenericError error);

    list<retromasterForm.TContentVolume> findContentVolumes() throws (1:retromasterForm.TGenericError error);
    void updateContentVolume(1: retromasterForm.TContentVolume volume) throws (1:retromasterForm.TGenericError error);
    void deleteContentVolume(1: string id) throws (1:retromasterForm.TGenericError error);

# ---
    list<retromasterForm.TContentExecutor> findContentExecutors(1: retromasterForm.TContentExecutorFilter filter) throws (1:retromasterForm.TGenericError error);
    list<retromasterForm.TContentExecutor> findContentExecutorsByContentMetadataItemId(1: string id) throws (1:retromasterForm.TGenericError error);
# ---
    retromasterForm.TContentDirectoryPaths findContentDirectoryPathsByFilter(1:  retromasterForm.TContentDirectoryPathFilter filter, 2: retromasterForm.TPageable pageable) throws (1:retromasterForm.TGenericError error);

# ---
    list<retromasterForm.TContentSystem> findContentSystems() throws (1:retromasterForm.TGenericError error);

# ---
    string findContentIdByContentMetadataItemId(1: string id) throws (1:retromasterForm.TGenericError error);
    map<string,string> findContentIdsByContentMetadataItemIds(1: list<string> ids) throws (1:retromasterForm.TGenericError error);
    map<string,string> findContentIdsByContentMetadataId(1: string id, 2: bool filterExecutables) throws (1:retromasterForm.TGenericError error);

    list<retromasterForm.TContentMetadataItemExecuteOptionSet> findContentMetadataItemExecuteOptionSetsByContentId(1: string id) throws (1:retromasterForm.TGenericError error);
    retromasterForm.TContentMetadataItemExecuteOptionSet findContentMetadataItemExecuteOptionSetByContentMetadataId(1: string id) throws (1:retromasterForm.TGenericError error);

    void doContentMetadataItemExecute(1: string contentMetadataItemId, 2: string contentExecutorId) throws (1:retromasterForm.TGenericError error);

    list<retromasterForm.TContentExecuteProcess> findContentExecuteProcessesByCurrent() throws (1:retromasterForm.TGenericError error);

# ---
   list<retromasterForm.TContentMetadataMedia> findContentMetadataMediasByContentMetadataId(1:  string id) throws (1:retromasterForm.TGenericError error);
   list<retromasterForm.TContentMetadataMedia> findContentMetadataMediasBySystemAndTitle(1:  string system, 2: string title) throws (1:retromasterForm.TGenericError error);

# ---
    retromasterForm.TQueryResults doQuery(1: retromasterForm.TQuery query, 2: retromasterForm.TPageable pageable) throws (1:retromasterForm.TGenericError error);

}